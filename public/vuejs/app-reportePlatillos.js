var app = new Vue({
  el: '#reportePlatillos',
  created: function() {
      this.obtenerCategorias();
  },
  //las variables que comienzan con "r" son para las rutas, si seguido tienen una "g" son para get y 'p' para Post.
  //varibales que comienzan con "a" son arreglos
  data: {
    fechaHasta:null,
    fechaDesde:null,
    mostrarbotonDescarga:false,
    isLoading:false,
    categorias:null,
    subcategorias:null,
    productos:null,
    totalsubcategorias:null,
    totalcategorias:null,
      totalgeneral:null,
      categoriasMSel:[],
      categoriasmSeleccionadas:null,
      sinregistro:null


  },
  methods: {

      obtenerCategorias:function(){
          var getCat="/reportes/platillo/Categorias";
          axios.get(getCat).then(response=>{
              this.categoriasMSel=response.data.categorias;
          });
      },

      obtenerPlatillos:function(){
        var rOplatillos="/reportes/platillo/generar";
        var fechas={
         'inicio':this.fechaDesde,
         'termino':this.fechaHasta,
         'categorias':this.categoriasmSeleccionadas
     }
     this.isLoading=true;
     axios.post(rOplatillos,fechas).then(response=>{

         this.productos=response.data.productos;
         this.categorias=response.data.categorias;
         this.categorias=response.data.categorias;
         this.subcategorias=response.data.subcategorias;
         this.totalsubcategorias=response.data.totalsubcategorias;
         this.totalcategorias=response.data.totalcategorias;
         this.totalgeneral=response.data.totalgeneral;
         this.isLoading=false;
         if (this.productos==null) {
          this.sinregistro=true;
      }else{
          this.sinregistro=false;
      }
  });
 },

  getCategorias:function({cCatFolio,cCatDescripcion,cCatCodigo}){ //[${cCatCodigo}]-
      return `${cCatDescripcion}`

  },

  porcentajesubcategoria:function(totalcant,cantidadProd){
    var cantidadPorcentaje=(parseFloat(cantidadProd)/parseFloat(totalcant))*100;
    cantidadPorcentaje=cantidadPorcentaje.toFixed(2);
    cantidadPorcentaje=this.formateardosDecimales(cantidadPorcentaje);
    return cantidadPorcentaje;
},

porcentajecategoria:function(totalcant,cantidadProd){
    var cantidadPorcentaje=(parseFloat(cantidadProd)/parseFloat(totalcant))*100;
    cantidadPorcentaje=cantidadPorcentaje.toFixed(2);
    cantidadPorcentaje=this.formateardosDecimales(cantidadPorcentaje);
    return cantidadPorcentaje;
},

porcentajeTotal:function(totalcant,cantidadProd){
    var cantidadPorcentaje=(parseFloat(cantidadProd)/parseFloat(totalcant))*100;
    cantidadPorcentaje=cantidadPorcentaje.toFixed(2);
    cantidadPorcentaje=this.formateardosDecimales(cantidadPorcentaje);
    return cantidadPorcentaje;
},

formatearMoneda: function(value){
  var formatear = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2,
});
  return formatear.format(value);
},

formateardosDecimales: function(values){
  var cant=values;
  cant=parseFloat(cant);
  var cantidad= cant.toFixed(2);
  return cantidad;
},

      //PDF PRODUCTOS
generarPDF:function(){

       var hoy = new Date();
       var fecha = hoy.getDate() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getFullYear();
       var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
       var fechaYHora = fecha + ' ' + hora;
       var roTIcketDes='/reportes/platillo/PDF';
       var data_reporte={
          'inicio':this.fechaDesde,
          'termino':this.fechaHasta,
          'categorias':this.categoriasmSeleccionadas
      }

      axios({
          method:'post',
          url:roTIcketDes,
          responseType:'blob'
          ,data:data_reporte
      }).then(response=>{
          let blob= new Blob([response.data],{type: 'application/pdf'});
          let link=document.createElement('a')
          link.href=window.URL.createObjectURL(blob)
          link.download='ReportePlatillo'+fechaYHora+'.pdf'
          link.click()
      });
  },

   generarReporteExcel:function(){
      var oRArticulosEXCEL='/reportes/platillo/EXCEL'
      var hoy = new Date();
      var fecha = hoy.getDate() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getFullYear();
      var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
      var fechaYHora = fecha + ' ' + hora;
      var fecArr={
          'inicio':this.fechaDesde,
          'termino':this.fechaHasta,
          'categorias':this.categoriasmSeleccionadas
      }

         axios({
          method:'post',
          url:oRArticulosEXCEL,
          responseType:'blob'
          ,data:fecArr
      }).then(response=>{
          let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
          let link=document.createElement('a')
          link.href=window.URL.createObjectURL(blob)
             link.download='ReportePlatillo'+fechaYHora+'.csv'
          link.click()
      });

  }


},
components:{
  Multiselect: window.VueMultiselect.default,
}

});

//
