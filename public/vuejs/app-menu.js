 var app = new Vue({
    el: '#configuracionmenu',
    created: function() {
    	this.getmenu();
    },
    //las variables que comienzan con "r" son para las rutas, si seguido tienen una "o" son para obtener informacion.
    data: {
     menuarray:null,
     nuevo_menu:{
        Folio:null,
        id:null,
        nombre:null,
        folPadre:null,
        URL:null,
        nodHijos:null,
        nuevo:null,
        nombreIcono:null,
    },
    folio_eliminar:null
},
computed:
{
    disabledButton:function()
        {/*
            if(this.selected_Zona!=80)
            {
                return false;
            }else
            {

                return true;
            }*/

        }
    },
    methods: {

        getmenu:function(){
        	var roMenu="/menu/configuracion/lista";
        	axios.get(roMenu).then(response=>{
        		this.menuarray=response.data.listamenu;
        	});
        },

        mAgregar:function(){
        	$('#agregarMenu').modal('show');
        },
        fNuevoMenu:function(){
            rpNMenu="/menu/nuevoItem";
            axios.post(rpNMenu,this.nuevo_menu).then(response=>{
                this.reiniciarNuevoMenu();
                this.getmenu();
                $('#agregarMenu').modal('hide');
                toastr.success("Nuevo elemento agregado al menú");
            });
        },
        reiniciarNuevoMenu:function(){
            this.nuevo_menu.nombre=null;
            this.nuevo_menu.folPadre=null;
            this.nuevo_menu.URL=null;
            this.nuevo_menu.nodHijos=null;
            this.nuevo_menu.nuevo=null;
            this.nuevo_menu.nombreIcono=null;
            this.nuevo_menu.id=null;
        },
        editarMenu:function(value){
            this.nuevo_menu.Folio=value.cMenFolio
            this.nuevo_menu.nombre=value.cMenDescripcion;
            this.nuevo_menu.folPadre=value.cMenFolPadre;
            this.nuevo_menu.URL=value.cMenURL;
            this.nuevo_menu.nodHijos=value.cMenHijos;
            this.nuevo_menu.nuevo=0;
            this.nuevo_menu.nombreIcono=value.cMenIcono;
            this.nuevo_menu.id=value.cMenOrden;
            $('#editarM').modal('show');
        },
        actualizarMenu:function(){
            var MenuAct='/menu/ActualizarMenu';
            axios.post(MenuAct,this.nuevo_menu).then(response=>{
               $('#editarM').modal('hide');  
               toastr.success("Menu actualizado con exito");
           });
        },
        confirmacionDeleteMen:function(value){
            this.folio_eliminar=value;
            $('#deleteMenu').modal('show');
        },
        eliminarMenu:function(){
            var elimMenu='/menu/EliminarMenu';
            var arr_folio={
                'Folio':this.folio_eliminar
            }
            axios.post(elimMenu,arr_folio).then(response=>{
                this.getmenu();
                $('#deleteMenu').modal('hide');
                 toastr.success("Elemento del menu eliminado con  exito");
            });
             
        }

    }
});