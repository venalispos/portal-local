var app = new Vue({
	el: '#peticionTraspaso',
	created: function() {
this.FunBuscarCentrosCosto();
this.FunBuscarproductos();
this.FunObtenerPeticionesnoFinalizadas();
	},
	data: {
centroCostos:null,
productosdata:null,
registroEditar:null,
productoSelected:{
  producto:null,
  cantidad:null,
  CentroCosto:null
},
productosSinFinalizar:null
	},
	computed:{
	},
	methods: {

              FunReiniciarVariables:function(){
                this.productoSelected.producto= null;
                this.productoSelected.cantidad = null;
                this.productoSelected.CentroCosto = null;
              },

              FunObtenerPeticionesnoFinalizadas:function(){
                route='/traspasos/get/dataAPIV';
                axios.get(route).then(response=>{
                    this.productosSinFinalizar=response.data;
                });
              },
              FunBuscarCentrosCosto:function(){
                axios.get('/traspasos/get/centrosCostos').then(response=>{
                  this.centroCostos= response.data;
                });
              },

              FunBuscarproductos:function(){
                route='/catalogo/getProductos';
                axios.get(route).then(response=>{
                  this.productosdata= response.data.productos;
                });
              },

              FunEnviarDatosControlador:function(){
                route='/traspasos/post/dataAPIV';
                data={
                  'data':this.productoSelected
                }
                axios.post(route,data).then(response=>{
                  this.FunObtenerPeticionesnoFinalizadas();
                  this.FunReiniciarVariables();
                });
              },

              FunModalEliminar:function(value){
                this.registroEditar=value;
                $('#meliminar').modal('show');
              },
              FunEliminarRegistro:function(){
                route='/traspasos/post/dataAPIV/delete';
                var data={
                  'registro':this.registroEditar
                }
                axios.post(route,data).then(response=>{
                  toastr.success("registro eliminado correctamente");
                  this.FunObtenerPeticionesnoFinalizadas();
                  $('#meliminar').modal('hide');
                });
              },

              FunActualizarFinalizarRegistros:function(){
                route='/traspasos/post/dataAPIV/update';
                data={
                  'registros':0
                }
                axios.post(route,data).then(response=>{
                  this.FunObtenerPeticionesnoFinalizadas();
                  toastr.success("Se ha creado la API para enviar a admintotal correctamente");
                });
              }


			     },
           components:{
             Multiselect: window.VueMultiselect.default,
         }



		});
