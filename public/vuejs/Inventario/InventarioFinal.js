var app = new Vue({
	el: '#Inventario',
	created: function() {
this.FunObtenerProductosAltaInicial();
this.FunObtenerprivilegios();
this.FunObtenerHabilitarCaptura();
	},
	data: {
		dataHabilitar:null,
codigoProducto:null,
cantidadProducto:null,
productosInicialDiario:null,
productoSeleccionado:null,
privilegios:null,
Insertar:0,
modificar:0,
eliminar:0,
reporte:0,
	},
	computed:{
	},
	methods: {
						FunObtenerHabilitarCaptura:function(){
							route='/inventario/verificar/envioadmintotal';
							axios.get(route).then(response=>{
								this.dataHabilitar = response.data;
							});
						},
						FunObtenerprivilegios :function(){
							var route='/inventario/privilegios/get';
							axios.get(route).then(response=>{
								this.privilegios=response.data;
								this.FunRecorrerPrivilegios();
							});
						},
						FunEnfocarCantidad:function(){
							 document.getElementById("cantidadP").focus();
						},
						FunEnfocarCodigo:function(){
							 document.getElementById("codigoP").focus();
						},
						FunRecorrerPrivilegios:function(){

							this.privilegios.forEach(privilegio=>{
							if (privilegio== 1) {
								this.Insertar=privilegio;
							}
							if (privilegio== 2) {
								this.modificar=privilegio;
							}
							if (privilegio== 3) {
								this.eliminar=privilegio;
							}
							if (privilegio== 4) {
								this.reporte=privilegio;
							}
							});
						},

            FunSepararEnviar:function(){
              if (this.codigoProducto.includes("*")) {
                var _cantidad=1;
                var _partescodigo=this.codigoProducto.split("*");
                try {
                  _cantidad=parseInt(_partescodigo[0]);
                } catch (e) {
                _cantidad=1;
                }
                  this.FunAgregarProducto(_partescodigo[1] , _cantidad);
              }else{
                this.FunAgregarProducto(this.codigoProducto,1);
              }
            },

            FunAgregarProducto:function(_codigo,cantidad){
							if (this.dataHabilitar == 0) {
							this.FunEnfocarCodigo();
              route ='/inventario/Inicial/agregarProductos';
              data= {
                'cantidad':cantidad,
                'codigo':_codigo,
								'tipo':4,
								'comentario':'entrada'
              }
                  axios.post(route,data).then(response=>{
										if (response.data== "0") {
                    toastr.success("Producto ingresado correctamente");
                      this.FunObtenerProductosAltaInicial();
                  }else if (response.data== "3") {
                  	toastr.error("No existe una entrada de este producto");
                  }else if (response.data== "4") {
                  	toastr.error("LA CAPTURA ES MAYOR A LO DECLARADO");
                  }else{
                    toastr.error("El código del producto no existe");
                  }

                  });
              this.FunreiniciarVariables();
						}else{
							toastr.error("Ya generaste los archivos para enviar a admintotal, no puedes realizar mas acciones en este turno");
						}
              },

              FunreiniciarVariables:function(){
              this.codigoProducto=null;
              this.cantidadProducto=null;

              },

              FunObtenerProductosAltaInicial:function(){
                route='/inventario/Inicial/ObtenerProductoInventarioDiario';
								data= {
									'tipo':4
								}
                axios.post(route,data).then(response=>{
                  this.productosInicialDiario=response.data;
                });
              },

              FunAbrirModalModificar:function(values){
                this.productoSeleccionado=values;
                $('#mmodificar').modal('show');
              },

              FunGuardarModificadoCerrarModal:function(){
                route='/inventario/Inicial/GuardarProductoModificado';
                data= {
                  'producto':this.productoSeleccionado,
									'tipo':4
                }
                axios.post(route,data).then(response=>{
                    toastr.success("Producto actualizado con exito");
                      $('#mmodificar').modal('hide');
                      this.FunreiniciarVariables();
                      this.FunObtenerProductosAltaInicial();
                });
              },

              FunAbrirModalEliminar:function(values){
                this.productoSeleccionado=values;
                $('#meliminar').modal('show');
              },

              FunEliminarProductoCerrarModal:function(){
                route='/inventario/Inicial/eliminarProducto';
                data= {
                  'producto':this.productoSeleccionado,
									'tipo':4
                }
                axios.post(route,data).then(response=>{
                    toastr.success("Producto eliminado con exito");
                      $('#meliminar').modal('hide');
                      this.FunreiniciarVariables();
                      this.FunObtenerProductosAltaInicial();
                });
              }

			},


		});
