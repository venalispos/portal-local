var app = new Vue({
                	el: '#Inventario',
                	created: function() {
                    this.FunObtenerprivilegios();
                	},
                	data: {
                  dfechaReporte:null,
                  iTipoReporte:null,
                  reporte:null,
                  aEntradas:null,
                  aDevoluciones:null,
                  aMermas:null,
                  aFinal:null
                	},
                	computed:{
                	},
                        	methods: {

                            FunObtenerprivilegios :function(){
                              var route='/inventario/privilegios/get';
                              axios.get(route).then(response=>{
                                this.privilegios=response.data;
                                this.FunRecorrerPrivilegios();
                              });
                            },
                            FunRecorrerPrivilegios:function(){

                              this.privilegios.forEach(privilegio=>{
                              if (privilegio== 1) {
                                this.Insertar=privilegio;
                              }
                              if (privilegio== 2) {
                                this.modificar=privilegio;
                              }
                              if (privilegio== 3) {
                                this.eliminar=privilegio;
                              }
                              if (privilegio== 4) {
                                this.reporte=privilegio;
                              }
                              });
                            },
                            FunObtenerInventarioFisico:function(){
                              route='/inventario/Reportes/Generar/porTipo';
                              data={
                                'fecha':this.dfechaReporte
                              }
                              axios.post(route,data).then(response=>{
                                this.aEntradas = response.data.entradas;
                                this.aMermas = response.data.mermas;
                                this.aDevoluciones = response.data.devoluciones;
                                this.aFinal = response.data.final;
                              });
                            },
                            FunGenerarPDF:function(){

                              route='/inventario/Inicial/reporte/PDF/movimientos';
                              data= {
                                'fecha':this.dfechaReporte
                              }
                              axios({
                                method:'post',
                                url:route,
                                responseType:'blob'
                                ,data:data
                              }).then(response=>{
                                let blob= new Blob([response.data],{type: 'application/pdf'});
                                let link=document.createElement('a')
                                link.href=window.URL.createObjectURL(blob)
                                link.download='ReporteInventario.pdf'
                                link.click()
                              });
                            },
                            FunGenerarExcel:function(){
                              route='/inventario/Inicial/reporte/PDF/movimientos';
                              data= {
                                'fecha':this.fechaReporteInventario
                              }
                              axios({
                               method:'post',
                               url:route,
                               responseType:'blob'
                               ,data:data
                           }).then(response=>{
                               let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                               let link=document.createElement('a')
                               link.href=window.URL.createObjectURL(blob)
                                  link.download='InventarioDiario.csv'
                               link.click()
                           });
                         }
                			     },


		          });
