var app = new Vue({
                	el: '#Inventario',
                	created: function() {
                    this.FunObtenerprivilegios();
                	},
                	data: {
                fechaReporteInventario:null,
                reporteInventario:null,
                privilegios:null,
                Insertar:0,
                modificar:0,
                eliminar:0,
                reporte:0,
                totalventa:0,
                totalVentaReal:0,
                totalVentaRealCantidad:null,
                totalVentaRealTeorica:null,
                totalVentaRealTeoricaCantidad:null,
                totalMonto:null,
                totalInicial:null,
                totalEntrada:null,
                totalDevolucion:null,
                totalMerma:null,
                totalFinal:null
                	},
                	computed:{
                	},
                        	methods: {

                            FunObtenerprivilegios :function(){
                              var route='/inventario/privilegios/get';
                              axios.get(route).then(response=>{
                                this.privilegios=response.data;
                                this.FunRecorrerPrivilegios();
                              });
                            },
                            FunRecorrerPrivilegios:function(){

                              this.privilegios.forEach(privilegio=>{
                              if (privilegio== 1) {
                                this.Insertar=privilegio;
                              }
                              if (privilegio== 2) {
                                this.modificar=privilegio;
                              }
                              if (privilegio== 3) {
                                this.eliminar=privilegio;
                              }
                              if (privilegio== 4) {
                                this.reporte=privilegio;
                              }
                              });
                            },
                            FunBuscarInventario:function(){
                              route='/inventario/Inicial/BuscarReporteInventario';
                              data= {
                                'fecha':this.fechaReporteInventario
                              }
                              axios.post(route,data).then(response=>{
                                this.reporteInventario=response.data.datosInventario;
                                this.totalventa=response.data.totalventa;
                                this.totalVentaReal=response.data.totalVentareal;
                                this.totalVentaRealCantidad=response.data.totalventaCantidad;
                                this.totalVentaRealTeorica=response.data.totalventateoricacantidad;
                                this.totalMonto=response.data.montototal;
                                this.totalInicial=response.data.totalInicial;
                                this.totalDevolucion=response.data.totalDevolucion;
                                this.totalMerma=response.data.totalMerma;
                                this.totalFinal=response.data.totalFinal;
                                this.totalEntrada=response.data.totalEntrada;
                                this.FuncionGuardarTotales();
                              });

                            },

                            FunFormatearModena:function(value){
                              var formatear = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                minimumFractionDigits: 2,
                            });
                              return formatear.format(value);
                            },

                            FunFormatearDosDecimales:function(values){
                              var cant=values;
                              cant=parseFloat(cant);
                              var cantidad= cant.toFixed(2);
                              return cantidad;
                            },

                            FuncionGuardarTotales:function(){
                              route='/inventario/Inicial/GuardarTotales';
                              data ={
                                'data':this.reporteInventario,
                                'fecha':this.fechaReporteInventario
                              }
                              axios.post(route,data).then(response=>{

                              });
                            },

                            FunRealizarInventarioFinal:function(inicial,entrada,devolucion,merma,venta ){
                              var resultado = (parseFloat(inicial)+parseFloat(entrada))-parseFloat(devolucion)-parseFloat(merma)-parseFloat(venta);
                              return resultado;
                            },

                            FunGenerarPDF:function(){
                              route='/inventario/Inicial/BuscarReporteInventarioPDF';
                              data= {
                                'fecha':this.fechaReporteInventario
                              }
                              axios({
                    						method:'post',
                    						url:route,
                    						responseType:'blob'
                    						,data:data
                    					}).then(response=>{
                    						let blob= new Blob([response.data],{type: 'application/pdf'});
                    						let link=document.createElement('a')
                    						link.href=window.URL.createObjectURL(blob)
                    						link.download='ReporteInventario.pdf'
                    						link.click()
                    					});
                            },
                            FunGenerarExcel:function(){
                              route='/inventario/Inicial/FunBuscarReporteInventarioEXCEL';
                              data= {
                                'fecha':this.fechaReporteInventario
                              }
                              axios({
                               method:'post',
                               url:route,
                               responseType:'blob'
                               ,data:data
                           }).then(response=>{
                               let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                               let link=document.createElement('a')
                               link.href=window.URL.createObjectURL(blob)
                                  link.download='InventarioDiario.csv'
                               link.click()
                           });
                         },

                         FunGenerarProductosAdmintotalAPI:function(){
                           route='/inventario/prepararDatos/envioadmintotal';
                           data= {
                             'fecha':this.fechaReporteInventario
                           }
                           axios.post(route,data).then(response=>{
                              toastr.success("Productos preparados para enviar a admintotal con exito");
                           });
                         },
                         FunGenerarArchivosEnviarCorreo:function(){
                           var route='/inventario/GenerarEnviar/porCorreo';
                           data= {
                             'fecha':this.fechaReporteInventario
                           }
                           axios.post(route,data).then(response=>{
                                toastr.success("Se ha enviado el correo a sus destinatarios de manera automática");
                           });
                         },



                			     },


		          });
