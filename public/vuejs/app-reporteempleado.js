var app = new Vue({
  el: '#reporteEmpleado',
  created: function() {
      this.getSucursales();
      this.fechaDefault();
  },
  data: {
      fechaDesde : null,
      fechaHasta : null,
      tiendas_selected: [],
      lista_sucursales: [],
      reporte_resultados: null,
      reporte_informacion: null,

    errors: null,
    isLoading: 0
  },
  computed: {

  },
  methods: {
      getSucursales: function(){
          var sucursalesURL = '/recursos/sucursales/reportes';
          axios.get(sucursalesURL).then(response => {
                  this.lista_sucursales = response.data.lista_sucursales;
                  //this.tiendas_selected.push(response.data.lista_sucursales[0]);
          });
      },
      generarReporte: function(){
          this.isLoading = 1;
          var generarReporteURL = '/reportes/empleado/generar';
          data_reporte = {
              'fechaDesde' : this.fechaDesde,
              'fechaHasta' : this.fechaHasta,
              'tiendas_selected' : this.tiendas_selected
          };
          axios.post(generarReporteURL, data_reporte).then(response => {
              this.reporte_resultados = response.data.reporte;
              this.reporte_informacion = response.data.informacion;
              this.isLoading = 0;
          });
      },
      fechaDefault: function() {
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1;
          var yyyy = today.getFullYear();
          if(dd<10)
          {
              dd='0'+dd;
          }

          if(mm<10)
          {
              mm='0'+mm;
          }

          today = yyyy+'-'+mm+'-'+dd;
          this.fechaHasta = today;
      },
      formatoMoneda(value){
          var formatear = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
          });
          return formatear.format(value);
      }
  },

});
