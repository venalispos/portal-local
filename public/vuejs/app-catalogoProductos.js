 var app = new Vue({
 	el: '#catalogoProductos',
 	created: function() {
 		this.getProducts();
 	},
 	data: {
 		arrProducts:null,
 		arrCategorias:null,
 		arrSubCategorias:null,
 		configuracion:null,
 		clasificacion:null,

 		productoModificar:{
            cProInterCodigo:null,
            cProDesTicket:null,
            cProPrecio:null,
            cProCosto:null,
            cProImpuesto1:null,
            cProImpuesto2:null,
            cProImpuesto3:null,
            cProImpuesto4:null,
            cProCategoria:null,
            cProSubCategoria:null,
            cProClasificacion:null,
            cProImprime:null,
            cProActivo:null,
            cProVisible:null
        },
        _data_response:null
    },
    methods: { 

 		//modales
 		mEditarProducto:function(value){
 			$('#productoEdit').modal('show');
 		},
        //funciones
        getProducts:function(){
        	var rGetProd="/catalogo/getProductos";
        	axios.get(rGetProd).then(response=>{
        		this.arrProducts=response.data.productos;
        		this.arrCategorias=response.data.categorias;
        		this.arrSubCategorias=response.data.subcategorias;
        		this.configuracion=response.data.configuracion;
        		this.clasificacion=response.data.clasificacion;
                this.FunCalcularVerdaderoFalso();
            });

        	setTimeout(function(){  $('#example').DataTable({
              "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
                "pagingType": "full_numbers",
                "processing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros encontrados"
            },
            "bDestroy": true
        });
        }, 3000);
        },

        FunCalcularVerdaderoFalso:function(){

            if(this.configuracion[0].cConImpuesto1Activo== 1){    
                this.configuracion[0].cConImpuesto1Activo=true;
            }else{
                this.configuracion[0].cConImpuesto1Activo=false;  
            }

            if(this.configuracion[0].cConImpuesto2Activo== 1){    
                this.configuracion[0].cConImpuesto2Activo=true;
            }else{
                this.configuracion[0].cConImpuesto2Activo=false;  
            }

            if(this.configuracion[0].cConImpuesto3Activo== 1){    
                this.configuracion[0].cConImpuesto3Activo=true;
            }else{
                this.configuracion[0].cConImpuesto3Activo=false;  
            }

            if(this.configuracion[0].cConImpuesto4Activo== 1){    
                this.configuracion[0].cConImpuesto4Activo=true;
            }else{
                this.configuracion[0].cConImpuesto4Activo=false;  
            }

        },
        FunValidarCodigoProducto:function(){

            var _data={
                '_codigo_validar' :this.productoModificar.cProInterCodigo
            };

            axios.post('/catalogo/Validar_codigo_producto',_data).then(response=>{
                this._data_response=response.data.datos;

            });

        },
    }
});  