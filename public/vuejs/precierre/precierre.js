var app = new Vue({
  el: '#cortesPrincipal',
  created: function(){
    this.dataCortes();
    this.obtenerData();
  },
  data:{
    precierres:[],
    precierreDetalle:[],
    configNube:[],
    tipoPago:[],
    subtipoPago:[],
    configuracion:[],
    precierre:{
      'cfechaprecierre':null,
      'cidsucursal':0,
      'ctotalprecierre':0,
      'cestatusprecierre':0,
      'cdescripciongasto':null,
      'ctotalgasto':0,
      'cusuarioprecierre':0,
    },
    bterminal1:{
      'cidtipopago':1,
      'cdenominacion':null,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':1,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    bterminal2:{
      'cidtipopago':1,
      'cdenominacion':null,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':2,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    sterminal1:{
      'cidtipopago':2,
      'cdenominacion':null,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':3,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    sterminal2:{
      'cidtipopago':2,
      'cdenominacion':null,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':4,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    billetes1:{
      'cidtipopago':3,
      'cdenominacion':1000,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    billetes2:{
      'cidtipopago':3,
      'cdenominacion':500,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    billetes3:{
      'cidtipopago':3,
      'cdenominacion':200,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    billetes4:{
      'cidtipopago':3,
      'cdenominacion':100,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    billetes5:{
      'cidtipopago':3,
      'cdenominacion':50,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    billetes6:{
      'cidtipopago':3,
      'cdenominacion':20,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    monedas1:{
      'cidtipopago':3,
      'cdenominacion':10,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    monedas2:{
      'cidtipopago':3,
      'cdenominacion':5,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    monedas3:{
      'cidtipopago':3,
      'cdenominacion':2,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    monedas4:{
      'cidtipopago':3,
      'cdenominacion':1,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    monedas5:{
      'cidtipopago':3,
      'cdenominacion':0.50,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    monedas6:{
      'cidtipopago':3,
      'cdenominacion':0.20,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    monedas7:{
      'cidtipopago':3,
      'cdenominacion':0.10,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    cheque:{
      'cidtipopago':4,
      'cdenominacion':0,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares1:{
      'cidtipopago':5,
      'cdenominacion':100,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares2:{
      'cidtipopago':5,
      'cdenominacion':50,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares3:{
      'cidtipopago':5,
      'cdenominacion':20,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares4:{
      'cidtipopago':5,
      'cdenominacion':10,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares5:{
      'cidtipopago':5,
      'cdenominacion':5,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares6:{
      'cidtipopago':5,
      'cdenominacion':1,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares7:{
      'cidtipopago':5,
      'cdenominacion':0.50,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares8:{
      'cidtipopago':5,
      'cdenominacion':0.25,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares9:{
      'cidtipopago':5,
      'cdenominacion':0.10,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares10:{
      'cidtipopago':5,
      'cdenominacion':0.05,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    dolares11:{
      'cidtipopago':5,
      'cdenominacion':0.01,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    aplicacion:{
      'cidtipopago':6,
      'cdenominacion':0,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    uber:{
      'cidtipopago':7,
      'cdenominacion':0,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':0,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    amexterminal1:{
      'cidtipopago':8,
      'cdenominacion':null,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':5,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    amexterminal2:{
      'cidtipopago':8,
      'cdenominacion':null,
      'ccantidad':0,
      'ctotal':0,
      'cidsubtipopago':6,
      'cnoreferencia':null,
      'cidprecierre':0,
    },
    banamexTotal:0,
    scotiabankTotal:0,
    nacionalTotal:0,
    dolaresTotal:0,
    tipoCambio:0,
    dolaresCambio:0,
    amexpressTotal:0,
    totalBan:0,
    totalSco:0,
    totalNac:0,
    totalDol:0,
    totalAmex:0,
    totalTran:0,
    totalApp:0,
    totalUb:0,
    total:0,
  },
  methods:{
    dataCortes:function(){
      axios.get('/precierre/precierrePrincipal/obtenerPrecierres').then(response=>{
        this.precierres = response.data.precierre;
        setTimeout(function(){$('#tableCortes').DataTable({
              "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
                "pagingType": "full_numbers",
                "processing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros encontrados"
              },
            });
        }); }, 3000).catch(function (error){
              toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
              console.log(error);
        });
    },
    obtenerData:function(){
			 axios.get('/precierre/precierrePrincipal/obtenerData').then(response=>{
         this.configNube = response.data.confignube;
         this.tipoPago = response.data.tipopago;
         this.subtipoPago = response.data.subtipopago;
         this.configuracion = response.data.configuracion;
		 });
    },
    abrirModalVer:function(idprecierre){
      this.tipoCambio=this.configuracion.cConDolarParidad;
      var idprecierre={'idprecierre':idprecierre};
      var url_data = "/precierre/precierrePrincipal/dataPrecierre";
      axios.post(url_data, idprecierre).then(response=>{
        this.precierreDetalle=response.data.precierredetalle;
        this.totalesVer();
       $('#VerCorte').modal('show');
    });
    },
    abrirModalAgregar:function(){
      this.precierre.cidsucursal=this.configNube.cIDTienda;
      this.tipoCambio=this.configuracion.cConDolarParidad;
      this.formatoFechaHoy();
      $('#AgregarCorte').modal('show');
    },
    abrirModalConfirmar:function(){
      this.totalCorte();
      $('#confirmarCorte').modal('show');
    },
    agregarCorte:function(){
      var precierreCompleto = {
        'precierre':this.precierre,
        'bterminal1': this.bterminal1,
        'bterminal2': this.bterminal2,
        'sterminal1': this.sterminal1,
        'sterminal2': this.sterminal2,
        'billetes1': this.billetes1,
        'billetes2': this.billetes2,
        'billetes3': this.billetes3,
        'billetes4': this.billetes4,
        'billetes5': this.billetes5,
        'billetes6': this.billetes6,
        'monedas1': this.monedas1,
        'monedas2': this.monedas2,
        'monedas3': this.monedas3,
        'monedas4': this.monedas4,
        'monedas5': this.monedas5,
        'monedas6': this.monedas6,
        'monedas7': this.monedas7,
        'cheque': this.cheque,
        'dolares1': this.dolares1,
        'dolares2': this.dolares2,
        'dolares3': this.dolares3,
        'dolares4': this.dolares4,
        'dolares5': this.dolares5,
        'dolares6': this.dolares6,
        'dolares7': this.dolares7,
        'dolares8': this.dolares8,
        'dolares9': this.dolares9,
        'dolares10': this.dolares10,
        'dolares11': this.dolares11,
        'aplicacion': this.aplicacion,
        'uber': this.uber,
        'amexterminal1': this.amexterminal1,
        'amexterminal2': this.amexterminal2,
      };
      var url_datosAgregar = "/precierre/precierrePrincipal/agregarPrecierre";
			 axios.post(url_datosAgregar, precierreCompleto).then(response=>{
				 toastr.success('El precierre se ha agregado correctamente');
				 $("#tableCortes").dataTable().fnDestroy();
				 this.dataCortes();
				 this.dataLimpiar();
				 $('#confirmarCorte').modal('hide');
				 $('#AgregarCorte').modal('hide');
		 });
    },
    totalBanamex:function(){
      this.banamexTotal=parseFloat(this.bterminal1.ctotal)+parseFloat(this.bterminal2.ctotal);
    },
    totalScotiabank:function(){
      this.scotiabankTotal=parseFloat(this.sterminal1.ctotal)+parseFloat(this.sterminal2.ctotal);
    },
    totalNacional:function(){
      this.billetes1.ctotal=parseFloat(this.billetes1.cdenominacion)*parseFloat(this.billetes1.ccantidad);
      this.billetes2.ctotal=parseFloat(this.billetes2.cdenominacion)*parseFloat(this.billetes2.ccantidad);
      this.billetes3.ctotal=parseFloat(this.billetes3.cdenominacion)*parseFloat(this.billetes3.ccantidad);
      this.billetes4.ctotal=parseFloat(this.billetes4.cdenominacion)*parseFloat(this.billetes4.ccantidad);
      this.billetes5.ctotal=parseFloat(this.billetes5.cdenominacion)*parseFloat(this.billetes5.ccantidad);
      this.billetes6.ctotal=parseFloat(this.billetes6.cdenominacion)*parseFloat(this.billetes6.ccantidad);
			this.monedas1.ctotal=parseFloat(this.monedas1.cdenominacion)*parseFloat(this.monedas1.ccantidad);
      this.monedas2.ctotal=parseFloat(this.monedas2.cdenominacion)*parseFloat(this.monedas2.ccantidad);
      this.monedas3.ctotal=parseFloat(this.monedas3.cdenominacion)*parseFloat(this.monedas3.ccantidad);
      this.monedas4.ctotal=parseFloat(this.monedas4.cdenominacion)*parseFloat(this.monedas4.ccantidad);
      this.monedas5.ctotal=parseFloat(this.monedas5.cdenominacion)*parseFloat(this.monedas5.ccantidad);
      this.monedas6.ctotal=parseFloat(this.monedas6.cdenominacion)*parseFloat(this.monedas6.ccantidad);
      this.monedas7.ctotal=parseFloat(this.monedas7.cdenominacion)*parseFloat(this.monedas7.ccantidad);
      this.nacionalTotal=parseFloat(this.billetes1.ctotal)+parseFloat(this.billetes2.ctotal)+parseFloat(this.billetes3.ctotal)+parseFloat(this.billetes4.ctotal)+parseFloat(this.billetes5.ctotal)+parseFloat(this.billetes6.ctotal)+parseFloat(this.monedas1.ctotal)+parseFloat(this.monedas2.ctotal)+parseFloat(this.monedas3.ctotal)+parseFloat(this.monedas4.ctotal)+parseFloat(this.monedas5.ctotal)+parseFloat(this.monedas6.ctotal)+parseFloat(this.monedas7.ctotal);
		},
    totalDolares:function(){
      this.dolares1.ctotal=parseFloat(this.dolares1.cdenominacion)*parseFloat(this.dolares1.ccantidad);
      this.dolares2.ctotal=parseFloat(this.dolares2.cdenominacion)*parseFloat(this.dolares2.ccantidad);
      this.dolares3.ctotal=parseFloat(this.dolares3.cdenominacion)*parseFloat(this.dolares3.ccantidad);
      this.dolares4.ctotal=parseFloat(this.dolares4.cdenominacion)*parseFloat(this.dolares4.ccantidad);
      this.dolares5.ctotal=parseFloat(this.dolares5.cdenominacion)*parseFloat(this.dolares5.ccantidad);
      this.dolares6.ctotal=parseFloat(this.dolares6.cdenominacion)*parseFloat(this.dolares6.ccantidad);
      this.dolares7.ctotal=parseFloat(this.dolares7.cdenominacion)*parseFloat(this.dolares7.ccantidad);
      this.dolares8.ctotal=parseFloat(this.dolares8.cdenominacion)*parseFloat(this.dolares8.ccantidad);
      this.dolares9.ctotal=parseFloat(this.dolares9.cdenominacion)*parseFloat(this.dolares9.ccantidad);
      this.dolares10.ctotal=parseFloat(this.dolares10.cdenominacion)*parseFloat(this.dolares10.ccantidad);
      this.dolares11.ctotal=parseFloat(this.dolares11.cdenominacion)*parseFloat(this.dolares11.ccantidad);
      this.dolaresTotal=parseFloat(this.dolares1.ctotal)+parseFloat(this.dolares2.ctotal)+parseFloat(this.dolares3.ctotal)+parseFloat(this.dolares4.ctotal)+parseFloat(this.dolares5.ctotal)+parseFloat(this.dolares6.ctotal)+parseFloat(this.dolares7.ctotal)+parseFloat(this.dolares8.ctotal)+parseFloat(this.dolares9.ctotal)+parseFloat(this.dolares10.ctotal)+parseFloat(this.dolares11.ctotal);
      this.dolaresCambio=this.dolaresTotal*parseFloat(this.tipoCambio);
    },
    totalAmexpress:function(){
      this.amexpressTotal=parseFloat(this.amexterminal1.ctotal)+parseFloat(this.amexterminal2.ctotal);
    },
    totalCorte:function(){
      this.precierre.ctotalprecierre=parseFloat(this.banamexTotal)+parseFloat(this.scotiabankTotal)+parseFloat(this.nacionalTotal)+parseFloat(this.dolaresCambio)+parseFloat(this.amexpressTotal)+parseFloat(this.cheque.ctotal)+parseFloat(this.aplicacion.ctotal)+parseFloat(this.uber.ctotal);
		},
    totalesVer:function(){
      for(var i=0; i<this.precierreDetalle.length; i++){
				if(this.precierreDetalle[i].cidtipopago==1){
					this.totalBan+=parseFloat(this.precierreDetalle[i].ctotal);
				}
        if(this.precierreDetalle[i].cidtipopago==2){
					this.totalSco+=parseFloat(this.precierreDetalle[i].ctotal);
				}
        if(this.precierreDetalle[i].cidtipopago==3){
					this.totalNac+=parseFloat(this.precierreDetalle[i].ctotal);
				}
        if(this.precierreDetalle[i].cidtipopago==4){
					this.totalTran+=parseFloat(this.precierreDetalle[i].ctotal);
				}
        if(this.precierreDetalle[i].cidtipopago==5){
					this.totalDol+=parseFloat(this.precierreDetalle[i].ctotal);
				}
        if(this.precierreDetalle[i].cidtipopago==6){
					this.totalApp+=parseFloat(this.precierreDetalle[i].ctotal);
				}
        if(this.precierreDetalle[i].cidtipopago==7){
					this.totalUb+=parseFloat(this.precierreDetalle[i].ctotal);
				}
        if(this.precierreDetalle[i].cidtipopago==8){
					this.totalAmex+=parseFloat(this.precierreDetalle[i].ctotal);
				}
			}
      this.dolaresCambio= parseFloat(this.tipoCambio)*parseFloat(this.totalDol);
      this.total = this.totalBan+this.totalSco+this.totalNac+this.totalTran+this.dolaresCambio+this.totalApp+this.totalUb+this.totalAmex;

      let valts1 = (this.totalBan/1).toFixed(2).replace(',', '.');
      this.totalBan= valts1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      let valts2 = (this.totalSco/1).toFixed(2).replace(',', '.');
      this.totalSco = valts2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      let valts3 = (this.totalNac/1).toFixed(2).replace(',', '.');
      this.totalNac = valts3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      let valts4 = (this.dolaresCambio/1).toFixed(2).replace(',', '.');
      this.dolaresCambio= valts4.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      let valts6 = (this.totalDol/1).toFixed(2).replace(',', '.');
      this.totalDol= valts6.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      let valts5 = (this.totalAmex/1).toFixed(2).replace(',', '.');
      this.totalAmex = valts5.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      let valts7 = (this.total/1).toFixed(2).replace(',', '.');
      this.total = valts7.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

      for(var i=0; i<this.precierreDetalle.length; i++){
        let valts8 = (this.precierreDetalle[i].ctotal/1).toFixed(2).replace(',', '.');
        this.precierreDetalle[i].ctotal = valts8.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    },
    formatoFechaHoy:function(){
      var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth()+1;
			var day = date.getDate();
			if(day<10){
        day='0'+day;
      }
      if(month<10){
        month='0'+month;
      }
			this.precierre.cfechaprecierre = year + "-" + month + "-" + day;
    },
    dataLimpiar:function(){
      this.bterminal1={
	      'cidtipopago':1,
	      'cdenominacion':null,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':1,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.bterminal2={
	      'cidtipopago':1,
	      'cdenominacion':null,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':2,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.sterminal1={
	      'cidtipopago':2,
	      'cdenominacion':null,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':3,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.sterminal2={
	      'cidtipopago':2,
	      'cdenominacion':null,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':4,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.billetes1={
	      'cidtipopago':3,
	      'cdenominacion':1000,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.billetes2={
	      'cidtipopago':3,
	      'cdenominacion':500,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.billetes3={
	      'cidtipopago':3,
	      'cdenominacion':200,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.billetes4={
	      'cidtipopago':3,
	      'cdenominacion':100,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.billetes5={
	      'cidtipopago':3,
	      'cdenominacion':50,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.billetes6={
	      'cidtipopago':3,
	      'cdenominacion':20,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.monedas1={
	      'cidtipopago':3,
	      'cdenominacion':10,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.monedas2={
	      'cidtipopago':3,
	      'cdenominacion':5,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.monedas3={
	      'cidtipopago':3,
	      'cdenominacion':2,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	   this. monedas4={
	      'cidtipopago':3,
	      'cdenominacion':1,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.monedas5={
	      'cidtipopago':3,
	      'cdenominacion':0.50,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.monedas6={
	      'cidtipopago':3,
	      'cdenominacion':0.20,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.monedas7={
	      'cidtipopago':3,
	      'cdenominacion':0.10,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.cheque={
	      'cidtipopago':4,
	      'cdenominacion':0,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares1={
	      'cidtipopago':5,
	      'cdenominacion':100,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares2={
	      'cidtipopago':5,
	      'cdenominacion':50,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares3={
	      'cidtipopago':5,
	      'cdenominacion':20,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares4={
	      'cidtipopago':5,
	      'cdenominacion':10,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares5={
	      'cidtipopago':5,
	      'cdenominacion':5,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares6={
	      'cidtipopago':5,
	      'cdenominacion':1,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	     'cidprecierre':0,
	    };
	    this.dolares7={
	      'cidtipopago':5,
	      'cdenominacion':0.50,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares8={
	      'cidtipopago':5,
	      'cdenominacion':0.25,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares9={
	      'cidtipopago':5,
	      'cdenominacion':0.10,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares10={
	      'cidtipopago':5,
	      'cdenominacion':0.05,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.dolares11={
	      'cidtipopago':5,
	      'cdenominacion':0.01,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.aplicacion={
	      'cidtipopago':6,
	      'cdenominacion':0,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.uber={
	      'cidtipopago':7,
	      'cdenominacion':0,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':0,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.amexterminal1={
	      'cidtipopago':8,
	      'cdenominacion':null,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':5,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
	    this.amexterminal2={
	      'cidtipopago':8,
	      'cdenominacion':null,
	      'ccantidad':0,
	      'ctotal':0,
	      'cidsubtipopago':6,
	      'cnoreferencia':null,
	      'cidprecierre':0,
	    };
			this.banamexTotal=0;
	    this.scotiabankTotal=0;
	    this.nacionalTotal=0;
	    this.dolaresTotal=0;
	    this.tipoCambio=0;
	    this.dolaresCambio=0;
	    this.amexpressTotal=0;
      this.totalBan=0,
      this.totalSco=0;
      this.totalNac=0;
      this.totalDol=0;
      this.totalAmex=0;
      this.totalTran=0;
      this.totalApp=0;
      this.totalUb=0;
      this.total=0;
    },
  },
});
