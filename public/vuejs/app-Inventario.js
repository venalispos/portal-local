var app = new Vue({
	el: '#IdInventarioInicial',
	created: function() {
		this.FuncionVerificarCapturaInicialDiario();
		this.FuncionVerificarMermasDevoluciones();
		this.FuncionVerificarCapturaUsuario();
		this.FuncionMostrarDatosInventario();
		this.FuncionMostrarDatosInventarioSalida();
		this.FuncionMostrarDatosInventarioSalidaPorUsuario();
		this.FuncionVerificarInventarioReportes();
	},
	data: {
		vProductoBuscar:null,
		dDatosProducto:null,
		vStatusInventario:null,
		vADatosInventario:null,
		vADatosInventarioSalida:null,
		vADatosInventarioSalidaMermaDevolucion:null,
		dProductoEditar:null,
		fechaReporteInventario:null,
		datosReporteInventario:null,
		_opcionseleccionada:null,
		statusInventarioReporte	:null,
		_vIMensaje:0,
		vSCapturaInicialFinalizada:0,
		vSCapturaMermasDevoluciones:0,
		vSCapturaFinalUsuario:0,
		vSCapturaFinalizadaFinal:0,
		permisoReprote:null

	},
	computed:{


	},
	methods: {

		FuncionBuscarProducto:function(){


			var RBuscarProducto='/Inventario/Buscarproducto';
			var AcodigoProducto={
				'codigo':this.vProductoBuscar
			}
			axios.post(RBuscarProducto,AcodigoProducto).then(response=>{
				this.dDatosProducto=response.data;
				if (this.dDatosProducto == []) {
					toastr.error("EL PRODUCTO NO EXISTE");
					this.dDatosProducto=null;
				}
			});
			setTimeout(function(){ document.getElementById("inputCantidadEntrada").focus(); }, 1000);


		},



		FuncionAgregarInventario:function(_datosProducto){
			var RGagregarEntrada="/Inventario/guardar/producto";
			var VADatosProducto={
				'producto':_datosProducto
			}
			axios.post(RGagregarEntrada,VADatosProducto).then(response=>{
				this.FuncionInicializarValores();
				this.FuncionMostrarDatosInventario();
				toastr.success("PRODUCTO AGREGADO CORRECTAMENTE");
				setTimeout(function(){ document.getElementById("InputBuscarProducto").focus(); }, 500);
			});

		},


		FuncionInicializarValores:function(){
			this.vProductoBuscar=null;
			this.dDatosProducto=null;
		},


		FuncionMostrarDatosInventario(){
			var rOdatosInventario="/Inventario/mostrar/inventario/productos/agregadosenfecha";
			axios.get(rOdatosInventario).then(response=>{
				this.vADatosInventario=response.data.datosInventario;
			});
		},


		FuncionMostrarModalEliminar:function(_datosProductoEditar){
			this.dProductoEditar=_datosProductoEditar;
			$('#mmodificar').modal("show");
		},

		FuncionModalEliminarProducto:function(_datosProductoEditar){
			this.dProductoEditar=_datosProductoEditar;
			$('#_idEliminarP').modal("show");
		},


		FuncionNuevaCantidadProducto:function(){
			var route='/Inventario/modificar/producto/cantidad';
			axios.post(route,this.dProductoEditar).then(response=>{
				toastr.success("Producto modificado con exito ");
				this.FuncionInicializarValores();
				this.FuncionMostrarDatosInventario();
				$('#mmodificar').modal("hide");
			});
		},


		FuncionEliminarProducto:function(){
			var route='/Inventario/eliminar/producto';
			axios.post(route,this.dProductoEditar).then(response=>{
				toastr.success("Producto eliminado con exito ");
				this.FuncionInicializarValores();
				this.FuncionMostrarDatosInventario();
				$('#_idEliminarP').modal("hide");
			});
		},

		FuncionBuscarInventario:function(){
			route="/Inventario/buscar/productos/detalles";
			var vAFecha= {
				'fecha':this.fechaReporteInventario}
				axios.post(route,vAFecha).then(response=>{
					this.datosReporteInventario=response.data.informacionInventario;
					this.statusInventarioReporte=response.data.status;
					this._vIMensaje=response.data.mensaje;
					this.FuncionFinalizarInventario();
				});
			},


			FUncionIngresarSalidas:function(_datosProducto){
				route="/Inventario/guardar/producto/salida";
				var vADatos={
					'producto':_datosProducto,
					'seleccion':this._opcionseleccionada
				}
				axios.post(route,vADatos).then(response=>{
					if (this._opcionseleccionada == 0) {
						toastr.success("Producto agregado a merma");
					}else if (this._opcionseleccionada== 1) {
						toastr.success("Producto agregado a devolución");
					}
					this.FuncionMostrarDatosInventarioSalida();
					this.FuncionInicializarValores();
				});
			},


			FuncionMostrarDatosInventarioSalida(){
				var rOdatosInventario="/Inventario/mostrar/inventario/productos/agregadosenfechaSalida";
				axios.get(rOdatosInventario).then(response=>{
					this.vADatosInventarioSalidaMermaDevolucion=response.data.datosInventario;
				});
			},

			FuncionModalElimnarSalida:function(_datosProductoEditar){
				this.dProductoEditar=_datosProductoEditar;
				$("#_idEliminarSalida").modal("show");
			},


			FuncionEliminarProductoSalida:function(){

				var route='/Inventario/eliminar/productoSalida';
				axios.post(route,this.dProductoEditar).then(response=>{
					toastr.success("Producto eliminado con exito ");
					this.FuncionInicializarValores();
					this.FuncionMostrarDatosInventarioSalida();
					$("#_idEliminarSalida").modal("hide");
				});


			},


			FuncionNuevaCantidadProductoSalida:function(){

				var route='/Inventario/modificar/producto/cantidadSalida';
				axios.post(route,this.dProductoEditar).then(response=>{
					toastr.success("Producto modificado con exito ");
					this.FuncionInicializarValores();
					this.FuncionMostrarDatosInventarioSalida();
					$('#_modalmodificarSalida').modal("hide");
				});

			},

			FuncionAbrirModalModificarSalida:function(_datosProductoEditar){
				this.dProductoEditar=_datosProductoEditar;
				$("#_modalmodificarSalida").modal("show");
			},

			FuncionOperacionInventario:function(_inicial,_entrada,_salidam,_salidad,_venta){
				if (_inicial == null) {
					_inicial=0;
				}

				if (_entrada == null) {
					_entrada=0;
				}
				if (_salidam == null) {
					_salidam=0;
				}
				if (_salidad == null) {
					_salidad=0;
				}
				if (_venta == null) {
					_venta	=0;
				}

				var resultado = parseFloat(_inicial)+parseFloat(_entrada) - parseFloat(_salidam) - parseFloat(_salidad) -parseFloat(_venta);

				return this.FuncionformateardosDecimales(resultado);
			},

			FuncionCalcularOperacionVentaCapturada:function(_inicial,_entrada,_salidam,_salidad,_venta,_CantidadFInal){
				if (_inicial == null) {
					_inicial=0;
				}

				if (_entrada == null) {
					_entrada=0;
				}
				if (_salidam == null) {
					_salidam=0;
				}
				if (_salidad == null) {
					_salidad=0;
				}
				if (_venta == null) {
					_venta	=0;
				}

				var resultado = parseFloat(_inicial)+parseFloat(_entrada) - parseFloat(_salidam) - parseFloat(_salidad) -parseFloat(_venta)-parseFloat(_CantidadFInal);

				return this.FuncionformateardosDecimales(resultado);
			},

			FuncionCalcularOperacionVentaCapturadaefectivo:function(_inicial,_entrada,_salidam,_salidad,_salidausaurio){
				if (_inicial == null) {
					_inicial=0;
				}

				if (_entrada == null) {
					_entrada=0;
				}
				if (_salidam == null) {
					_salidam=0;
				}
				if (_salidad == null) {
					_salidad=0;
				}


				var resultado = parseFloat(_inicial)+parseFloat(_entrada) - parseFloat(_salidam) - parseFloat(_salidad)-parseFloat(_salidausaurio);

				return this.FuncionformateardosDecimales(resultado);
			},


			FuncionformateardosDecimales: function(values){
				var cant=values;
				cant=parseFloat(cant);
				var cantidad= cant.toFixed(2);
				return cantidad;
			},

			FuncionFinalizarInventario:function(){
				route ="/Inventario/guardar/productos/totales";
				var _ArrDatos={
					'productos':this.datosReporteInventario
				}
				axios.post(route,_ArrDatos).then(response=>{

				});
			},


			FuncionGenerarPDFInventario:function(){
				var route='/Inventario/generar/pdf/reporte/inventario';
				var vAFecha= {
					'fecha':this.fechaReporteInventario}
					axios({
						method:'post',
						url:route,
						responseType:'blob'
						,data:vAFecha
					}).then(response=>{
						let blob= new Blob([response.data],{type: 'application/pdf'});
						let link=document.createElement('a')
						link.href=window.URL.createObjectURL(blob)
						link.download='ReporteInventario.pdf'
						link.click()
					});
				},

				FUncionIngresarSalidasPorUsuario:function(_datosProducto){
					route="/Inventario/mostrar/inventario/productos/usuario/agregadosenfechaSalida";
					var vADatos={
						'producto':_datosProducto
					}
					axios.post(route,vADatos).then(response=>{
						toastr.success("Registro final del producto ingresado correctamente");
						this.FuncionInicializarValores();
						this.FuncionMostrarDatosInventarioSalidaPorUsuario();

					});
				},


				FuncionMostrarDatosInventarioSalidaPorUsuario(){
					var rOdatosInventario="/Inventario/guardar/producto/usuario/salida";
					axios.get(rOdatosInventario).then(response=>{
						this.vSCapturaFinalUsuario=response.data.datosInventario;
					});
				},
				FuncionFinalizarCapturaInventario:function(){
					$('#_idFinalizarCaptura').modal('show');
				},

				FuncionEliminarCapturaInventarioinicial:function(){
					var route='/Inventario/finalizar/Inventario/caputar/inicial';
					var data={
						'data':this.vADatosInventario
					}
					axios.post(route,data).then(response=>{
						toastr.success("Captura inicial finalizada correctamente");
						$('#_idFinalizarCaptura').modal('hide');
						this.FuncionVerificarCapturaInicialDiario();
					});
				},

				FuncionVerificarCapturaInicialDiario:function(){
					route='/Inventario/Verificar/Inventario/Capturado/fechaactual';
					axios.get(route).then(response=>{
						this.vSCapturaInicialFinalizada=response.data;
					});
				},

				FuncionFinalizarMermasDevoliciones:function(){
					$('#_idFinalizarMermasDevoliciones').modal('show');
				},

				FuncionFinalizarMermasDevoluciones:function(){
					var route='/Inventario/finalizar/Inventario/MermasDevoluciones/capturadas/fechaactual';
					var data={
						'data':this.vADatosInventarioSalidaMermaDevolucion
					}
					axios.post(route,data).then(response=>{
						toastr.success("Captura inicial finalizada correctamente");
						$('#_idFinalizarMermasDevoliciones').modal('hide');
						this.FuncionVerificarMermasDevoluciones();
					});
				},

				FuncionmodalEliminarSalidaUsuario:function(_datosProducto){

					this.dProductoEditar=_datosProducto;
					$('#_idEliminarSalidaUser').modal('show')
				},
				FuncionEliminarProductolistUsuario:function(){

					var route='/Inventario/eliminar/productoSalida';
					axios.post(route,this.dProductoEditar).then(response=>{
						toastr.success("Producto eliminado con exito ");
						this.FuncionInicializarValores();
						this.FuncionMostrarDatosInventarioSalidaPorUsuario();
						$("#_idEliminarSalidaUser").modal("hide");
					});
				},
				FuncionAbrirModalModificarSalidaReproteUsuario:function(_datosProducto){
					this.dProductoEditar=_datosProducto;
					$('#_modalmodificarreporteFinalUsuario').modal('show');
				},

				FuncionNuevaCantidadReporteUsuario:function(){

				var route='/Inventario/modificar/producto/cantidadSalida';
				axios.post(route,this.dProductoEditar).then(response=>{
					toastr.success("Producto modificado con exito ");
					this.FuncionInicializarValores();
					this.FuncionMostrarDatosInventarioSalidaPorUsuario();
					$('#_modalmodificarreporteFinalUsuario').modal("hide");
				});

			},


				FuncionVerificarMermasDevoluciones:function(){
					route='/Inventario/verificar/Inventario/MermasDevoluciones/capturadas/fechaactual';
					axios.get(route).then(response=>{
						this.vSCapturaMermasDevoluciones=response.data;
					});
				},

				FuncionFinalizarCapturaUsuario:function(){
					var route='/Inventario/finalizar/Inventario/InventarioFinal/capturadas/fechaactual';
					var data={
						'data':this.vSCapturaFinalUsuario
					}
					axios.post(route,data).then(response=>{
						toastr.success("Captura inicial finalizada correctamente");
						$('#idFinalizarCapturaUsuario').modal('hide');
						this.FuncionVerificarCapturaUsuario();
					});
				},

				FuncionVerificarCapturaUsuario:function(){
					route='/Inventario/verificar/Inventario/InventarioFinal/capturadas/fechaactual';
					axios.get(route).then(response=>{
						this.vSCapturaFinalizadaFinal=response.data;
					});
				},

				FuncionConfirmarFinalizarCapturaUsuario:function(){
					$('#idFinalizarCapturaUsuario').modal('show');
				},

				FuncionVerificarInventarioReportes:function(){
					route ='/Inventario/verificar/Inventario/reportes/capturar/finalizados';
					axios.get(route).then(response => {
						this.permisoReprote=response.data;
					});
				}




			},


		});
