var app = new Vue({
 el: '#reportedesglose',
 created: function() {
 },
   //las variables que comienzan con "r" son para las rutas, si seguido tienen una "o" son para obtener informacion.
   data: {
     fechaDesde:null,
     fechaHasta:null,
     mostrarbotonDescarga:null,
     reporteMesasHis:null,
     isLoading:null,
     sinRegistros:null,
     reporteProductos:null,
     totalProductos:null,
     formasdepago:null,
     mostrarbotonDescarga:null,
     cargandoPDF: false,
   },
   computed:
   {
     disabledButton:function()
       {/*
           if(this.selected_Zona!=80)
           {
               return false;
           }else
           {

               return true;
           }*/

       }
   },
   methods: {
     generarReporte:function(){
       var roTIcketDes='/reportes/desgloseTP/generar';
       var fechas={
         'inicio':this.fechaDesde,
         'termino':this.fechaHasta
       }
       axios.post(roTIcketDes,fechas).then(response=>{
       if (response.data == 0) {
                   toastr.warning("Has tratado de ingresar una fecha no valida");
                   this.isLoading = 0;
               } else {
         this.reporteMesasHis=response.data.mesas;
         this.reporteProductos=response.data.productos;
         this.totalProductos=response.data.totalProductos;
         this.formasdepago=response.data.formasdepago;
         this.mostrarbotonDescarga=true;
       }
       });
     },
     formatearMoneda: function(value){
       var formatear = new Intl.NumberFormat('en-US', {
         style: 'currency',
         currency: 'USD',
         minimumFractionDigits: 2,
       });
       return formatear.format(value);
     },
     totalProductosIva:function(value,value2){
       var subtotal=value;
       var iva= value2;
       var resultado=parseFloat(subtotal)+parseFloat(iva);
       resultado=this.formatearMoneda(resultado);
       return resultado;
     },
     formatearFecha: function(fecha){
       var year = fecha.toString().substr(0,4);
       var month = fecha.toString().substr(4,2);
       var day = fecha.toString().substr(6,2);

       return year+'/'+month+'/'+day;
     },
     descargarPDFdesglose:function(){
       var fecha = new Date();
       console.log(fecha);
       var roTIcketDes='/reportes/desgloseTP/generarPDF';
       data_reporte = {
         'inicio' : this.fechaDesde,
         'termino' : this.fechaHasta
       };

       axios({
         method:'post',
         url:roTIcketDes,
         responseType:'blob'
         ,data:data_reporte
       }).then(response=>{
         let blob= new Blob([response.data],{type: 'application/pdf'});
         let link=document.createElement('a')
         link.href=window.URL.createObjectURL(blob)
         link.download='DesglosePDF.pdf'
         link.click()
       });
     },

     descargarPDFdesgloseEN:function(){
        this.cargandoPDF = true;
        var fecha = new Date();
        console.log(fecha);
        var roTIcketDes='/reportes/desgloseTP/generarPDF/en';
        data_reporte = {
          'inicio' : this.fechaDesde,
          'termino' : this.fechaHasta
        };

        axios({
          method:'post',
          url:roTIcketDes,
          responseType:'blob'
          ,data:data_reporte
        }).then(response=>{
          let blob= new Blob([response.data],{type: 'application/pdf'});
          let link=document.createElement('a')
          link.href=window.URL.createObjectURL(blob)
          link.download='reportProduct.pdf'
          link.click()
        })
        .finally(() => {
          this.cargandoPDF = false;
        });
      },

     GenerarExcelDesglose:function(){

       data_reporte = {
         'inicio' : this.fechaDesde,
         'termino' : this.fechaHasta
       };
       axios({
         method:'post',
         url:'/reportes/desgloseTP/generarEXCEL',
         responseType:'blob'
         ,data:data_reporte
       }).then(response=>{
         let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
         let link=document.createElement('a')
         link.href=window.URL.createObjectURL(blob)
         link.download='ReporteVentasProductos.csv'
         link.click()
       });

     }

   }
});
