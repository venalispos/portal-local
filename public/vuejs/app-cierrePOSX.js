 var app = new Vue({
    el: '#cierre',
    created: function() {
        this.obtenerFormasPago();
    },
    //las variables que comienzan con "r" son para las rutas, si seguido tienen una "o" son para obtener informacion.
    data: {
    formaspagoArr:null
},
computed:
{
    disabledButton:function(){

        }
    },
    methods: {

      obtenerFormasPago:function(){
        var routeGetFormas="/cierre/getFormasPago";
        axios.get(routeGetFormas).then(response=>{
            this.formaspagoArr=response.data.formas;
        });
      },

      cambiarestatus:function(index){
        this.formaspagoArr[index]['cCatEstatus']=1;
      }

    }
});