var app = new Vue({
 el: '#catalogoProductos',
 created: function(){
   this.obtenerProductos();
 },
 data: {
   DatosProducto: {},
   Ticket:[],
   Categorias:[],
   Clasificacion:[],
   ComCategorias:[],
   ComProductos:[],
   ComSubCategorias:[],
   Configuracion:[],
   SubCategorias:[],
   CatModificadores:[],
   Productos:[],
   DescTicket: [],
   Precio: null,
   Cargando: 1,
   Indice: 0,
   ProductoA: {},
 },
 methods: {
   limpiarProducto:function(){
     this.DatosProducto = {'A0': 0, 'A1':0, 'A2':0, 'A3':0, 'A4':0, 'A5':0, 'A6':0, 'F0': '0000', 'F1':'0000', 'F2':'0000', 'F3':'0000', 'F4':'0000', 'F5':'0000', 'F6':'0000', 'I0': '0000', 'I1':'0000', 'I2':'0000', 'I3':'0000', 'I4':'0000', 'I5':'0000', 'I6':'0000','P0': 0.0, 'P1':0.0, 'P2':0.0, 'P3':0.0, 'P4':0.0, 'P5':0.0, 'P6':0.0, 'cComCat1':0, 'cComCat2':0, 'cComCat3':0, 'cComCat4':0, 'cComCat5':0, 'cComCat6':0, 'cComCat1Cant':0, 'cComCat2Cant':0, 'cComCat3Cant':0, 'cComCat4Cant':0, 'cComCat5Cant':0, 'cComCat6Cant':0, 'cCombo':0, 'cProActivo':0, 'cProAutoModi':0, 'cProCategoria':0, 'cProClasificacion':0, 'cProCodigo':0, 'cProComPrecio':0, 'cProCosto':0, 'cProDesBoton':"", 'cProDesTicket':"", 'cProDescripcion':"", 'cProFechaFin':99999999, 'cProFechaInicio':0, 'cProFolio':1, 'cProFraccion':0, 'cProImprime':0, 'cProImpuesto1':1, 'cProImpuesto2':1, 'cProImpuesto3':1, 'cProImpuesto4':1, 'cProInterCodigo':0, 'cProMod':0, 'cProModCant':0, 'cProPrecio':0, 'cProPuntos':0, 'cProSubCategoria':0, 'cProVisible':0}
   },
   obtenerDatos:function(){
     axios.get('/catalogo/productos/obtenerDatos').then(response=>{
        this.Ticket = response.data.Ticket;
        this.Categorias = response.data.Categorias;
        this.Clasificacion = response.data.Clasificacion;
        this.ComCategorias = response.data.ComCategorias;
        this.ComProductos = response.data.ComProductos;
        this.ComSubCategorias = response.data.ComSubCategorias;
        this.Configuracion = response.data.Configuracion;
        this.SubCategorias = response.data.SubCategorias;
      }).catch(function (error){
              toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
              console.log(error);
          });
   },
   obtenerProductos:function(){
     this.limpiarProducto()
     axios.get('/catalogo/productos/obtenerProductos').then(response=>{
        this.Productos = response.data.Productos;
        setTimeout(function(){$('#table_productos').DataTable({
              "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
                "pagingType": "full_numbers",
                "processing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros encontrados"
              },
            });
        }); }, 3000).catch(function (error){
              toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
              console.log(error);
          });
   },
   cambiarUno:function(valor){
     var uno = valor
     if(uno===1){
       uno = true
     }
     else {
       uno = false
     }
     return uno
   },

   FormatearDescMayusculas:function(){
     this.DatosProducto.cProDescripcion = this.DatosProducto.cProDescripcion.toUpperCase();
   },
   FormatearDescBMayusculas:function(){
     this.DatosProducto.cProDesBoton = this.DatosProducto.cProDesBoton.toUpperCase();
   },
   FormatearDescTMayusculas:function(){
     this.DatosProducto.cProDesTicket = this.DatosProducto.cProDesTicket.toUpperCase();
   },
   cambiarCheck: function(valor){
     var Boleano = valor
     if(Boleano){
       Boleano=1;
     }else{
       Boleano=0;
     }
     return Boleano
   },
   cambiarPuntos: function(valor){
     var Hora = valor.replace(':', '');
     return Hora
   },
   agregarPuntos: function(valor){
     var HoraI = valor.substring(0, 2);
     var HoraF = valor.substring(2, 4);
     return HoraI+':'+HoraF
   },
   CambiarCheckBox:function(){
     this.DatosProducto.cProActivo = this.cambiarUno(this.DatosProducto.cProActivo)
     this.DatosProducto.cProVisible = this.cambiarUno(this.DatosProducto.cProVisible)
     this.DatosProducto.cProAutoModi = this.cambiarUno(this.DatosProducto.cProAutoModi)
     this.DatosProducto.cCombo = this.cambiarUno(this.DatosProducto.cCombo)
     this.DatosProducto.cProImpuesto1 = this.cambiarUno(this.DatosProducto.cProImpuesto1)
     this.DatosProducto.cProImpuesto2 = this.cambiarUno(this.DatosProducto.cProImpuesto2)
     this.DatosProducto.cProImpuesto3 = this.cambiarUno(this.DatosProducto.cProImpuesto3)
     this.DatosProducto.cProImpuesto4 = this.cambiarUno(this.DatosProducto.cProImpuesto4)
     this.DatosProducto.A0 = this.cambiarUno(this.DatosProducto.A0)
     this.DatosProducto.A1 = this.cambiarUno(this.DatosProducto.A1)
     this.DatosProducto.A2 = this.cambiarUno(this.DatosProducto.A2)
     this.DatosProducto.A3 = this.cambiarUno(this.DatosProducto.A3)
     this.DatosProducto.A4 = this.cambiarUno(this.DatosProducto.A4)
     this.DatosProducto.A5 = this.cambiarUno(this.DatosProducto.A5)
     this.DatosProducto.A6 = this.cambiarUno(this.DatosProducto.A6)

     this.DatosProducto.I0 = this.agregarPuntos(this.DatosProducto.I0)
     this.DatosProducto.I1 = this.agregarPuntos(this.DatosProducto.I1)
     this.DatosProducto.I2 = this.agregarPuntos(this.DatosProducto.I2)
     this.DatosProducto.I3 = this.agregarPuntos(this.DatosProducto.I3)
     this.DatosProducto.I4 = this.agregarPuntos(this.DatosProducto.I4)
     this.DatosProducto.I5 = this.agregarPuntos(this.DatosProducto.I5)
     this.DatosProducto.I6 = this.agregarPuntos(this.DatosProducto.I6)
     this.DatosProducto.F0 = this.agregarPuntos(this.DatosProducto.F0)
     this.DatosProducto.F1 = this.agregarPuntos(this.DatosProducto.F1)
     this.DatosProducto.F2 = this.agregarPuntos(this.DatosProducto.F2)
     this.DatosProducto.F3 = this.agregarPuntos(this.DatosProducto.F3)
     this.DatosProducto.F4 = this.agregarPuntos(this.DatosProducto.F4)
     this.DatosProducto.F5 = this.agregarPuntos(this.DatosProducto.F5)
     this.DatosProducto.F6 = this.agregarPuntos(this.DatosProducto.F6)
   },

   CambiarCheckBoxB:function(){
     this.DatosProducto.cProActivo = this.cambiarCheck(this.DatosProducto.cProActivo)
     this.DatosProducto.cProVisible = this.cambiarCheck(this.DatosProducto.cProVisible)
     this.DatosProducto.cProAutoModi = this.cambiarCheck(this.DatosProducto.cProAutoModi)
     this.DatosProducto.cCombo = this.cambiarCheck(this.DatosProducto.cCombo)
     this.DatosProducto.cProImpuesto1 = this.cambiarCheck(this.DatosProducto.cProImpuesto1)
     this.DatosProducto.cProImpuesto2 = this.cambiarCheck(this.DatosProducto.cProImpuesto2)
     this.DatosProducto.cProImpuesto3 = this.cambiarCheck(this.DatosProducto.cProImpuesto3)
     this.DatosProducto.cProImpuesto4 = this.cambiarCheck(this.DatosProducto.cProImpuesto4)
     this.DatosProducto.A0 = this.cambiarCheck(this.DatosProducto.A0)
     this.DatosProducto.A1 = this.cambiarCheck(this.DatosProducto.A1)
     this.DatosProducto.A2 = this.cambiarCheck(this.DatosProducto.A2)
     this.DatosProducto.A3 = this.cambiarCheck(this.DatosProducto.A3)
     this.DatosProducto.A4 = this.cambiarCheck(this.DatosProducto.A4)
     this.DatosProducto.A5 = this.cambiarCheck(this.DatosProducto.A5)
     this.DatosProducto.A6 = this.cambiarCheck(this.DatosProducto.A6)
     this.DatosProducto.I0 = this.cambiarPuntos(this.DatosProducto.I0)
     this.DatosProducto.I1 = this.cambiarPuntos(this.DatosProducto.I1)
     this.DatosProducto.I2 = this.cambiarPuntos(this.DatosProducto.I2)
     this.DatosProducto.I3 = this.cambiarPuntos(this.DatosProducto.I3)
     this.DatosProducto.I4 = this.cambiarPuntos(this.DatosProducto.I4)
     this.DatosProducto.I5 = this.cambiarPuntos(this.DatosProducto.I5)
     this.DatosProducto.I6 = this.cambiarPuntos(this.DatosProducto.I6)
     this.DatosProducto.F0 = this.cambiarPuntos(this.DatosProducto.F0)
     this.DatosProducto.F1 = this.cambiarPuntos(this.DatosProducto.F1)
     this.DatosProducto.F2 = this.cambiarPuntos(this.DatosProducto.F2)
     this.DatosProducto.F3 = this.cambiarPuntos(this.DatosProducto.F3)
     this.DatosProducto.F4 = this.cambiarPuntos(this.DatosProducto.F4)
     this.DatosProducto.F5 = this.cambiarPuntos(this.DatosProducto.F5)
     this.DatosProducto.F6 = this.cambiarPuntos(this.DatosProducto.F6)
   },
   ModalBotonDescB:function(){
     this.FormatearPrecio();
     $("#VerBotonDescB").modal("show");
   },
   FormatearPrecio:function(){
     let valts = (this.DatosProducto.cProPrecio/1).toFixed(2).replace(',', '.');
	   this.Precio = valts.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   },
   ModalBotonDescT:function(){
     this.FormatearPrecio();
     this.ConfiguracionTicket();
     $("#VerBotonDescT").modal("show");
   },
   ConfiguracionTicket:function(){
     var cantprecio = this.Precio.length;
     var cantdesc = this.DatosProducto.cProDesTicket.length;
     var precio = this.Precio;
     var descripcion = this.DatosProducto.cProDesTicket;
     var totaldesc = parseFloat(this.Ticket.cTicCantCaracteres) - 14;
     var total = totaldesc - cantdesc;

     if(cantdesc>=totaldesc){
       descripcion = descripcion.slice(0,total);
     }

     for(var i=cantprecio; i<7; i++){
       precio = "_" + precio;
     }
     console.log(precio);
     for(var j=cantdesc; j<totaldesc; j++){
       descripcion = descripcion + "_";
     }
     var descticket = "00.00__" + descripcion + "$_" + precio;
     this.DescTicket = descticket.split('_');
   },
   cerrarModal:function(Modal){
     $('#'+Modal).modal("hide");
     this.CambiarCheckBoxB();
   },
   abrirModal:function(Modal, Datos){
     console.log(Datos);
     if(Datos===0){
       this.limpiarProducto()
     }
     else{
       this.DatosProducto = Datos
     }
     this.obtenerDatos()
     this.CambiarCheckBox()
     $('#'+Modal).modal("show");
   },
   validarCategoriasCombo: function(){
     if(this.DatosProducto.cCombo){
       if(this.validarCombos()){
         return true
       }
       else {
         return false
       }
     }
     else{
       return false
     }
   },
   validarCombos:function () {
     var comprobar = false;

     switch (this.DatosProducto.cComCat1) {
      case this.DatosProducto.cComCat2:
        comprobar = true;
        break;
      case this.DatosProducto.cComCat3:
        comprobar = true;
        break;
      case this.DatosProducto.cComCat4:
        comprobar = true;
        break;
    }
      switch (this.DatosProducto.cComCat2) {
       case this.DatosProducto.cComCat3:
         comprobar = true;
         break;
       case this.DatosProducto.cComCat4:
         comprobar = true;
         break;
     }
       switch (this.DatosProducto.cComCat3) {
        case this.DatosProducto.cComCat4:
          comprobar = true;
          break;
      }

      return comprobar;
   },
   editarProducto:function(){
     if(this.validarCategoriasCombo()){
       toastr.warning('Categorias repetidas');
     }else{
     this.CambiarCheckBoxB();
     var ruta = "editarProducto"
     var datos = this.DatosProducto
     var modal = "EditarProducto"
     var accion = "modificado"
     this.conexion(ruta, datos, modal, accion)
   }
   },
   ProductoAgregar:function(){
     if(this.validarCategoriasCombo()){
       toastr.warning('Categorias repetidas');
     }else{
     this.CambiarCheckBoxB();
     var ruta = "agregarProducto"
     var datos = this.DatosProducto
     var modal = "AgregarProducto"
     var accion = "agregado"
     this.conexion(ruta, datos, modal, accion)
   }
   },
   descargarExcel:function(){
     data_reporte = {
     };
     axios({
       method:'post',
       url:'/catalogo/productos/generarExcel',
       responseType:'blob'
       ,data:data_reporte
     }).then(response=>{
       let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
       let link=document.createElement('a')
       link.href=window.URL.createObjectURL(blob)
       link.download='Productos.csv'
       link.click()
     });
   },
   conexion:function(ruta, datos, modal, accion){
     var url = "/catalogo/productos/"+ruta;
      axios.post(url, datos).then(response=>{
        var estado = response.data;
        if(estado === 0)
        {
           $("#table_productos").dataTable().fnDestroy();
           this.obtenerProductos();
           toastr.success('El producto se ha '+ accion +' correctamente.');
  				 $('#'+modal).modal("hide");
        }
        else{
          toastr.warning('Error','El codigo del produto está repetido.');
        }
      }).catch(function (error){
         toastr.warning('Error','Ha ocurrido un error.');
         console.log(error);
     });
   },
 },
});
