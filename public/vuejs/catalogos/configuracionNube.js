var app = new Vue({
  el: '#configuracionNube',
  created: function(){
    this.iniciarDatosConfigNube();
    this.ObtenerConfigNube();
  },
  data:{
    DatosConfigNube: {}
  },
  methods:{
    iniciarDatosConfigNube: function(){
      this.DatosConfigNube = {'cBDSincroActiva': 0, 'cBDHost': null, 'cBDUser': null, 'cBDPass': null, 'cIDTienda': 0,
      'cBDName': null, 'cDBHostAdminTotal': null, 'cDbAdminTotalAlmacen': null, 'cBDAppPedidos': 0, 'cBDAppCheckIt': 0,
      'cBDBitacora': 0, 'cBDIndicadores': 0, 'cBDPuestos': 0, 'cBDProductosNubeToLocal': 0, 'cBDRegistrarFacturas':0,
      'cBDMesasAbiertasNube': 0, 'cBDContenidoMesasAbiertasNube': 0, 'cBDMesasNube': 0, 'cBDProductosNube': 0,
      'cBDRegAsistencia': 0, 'cBDAdminTotal': 0, 'cBDSincronizarCorte': 0, 'cBDEmpleados': 0, 'cDBQUerys': 0}
    },
    cambiarCheck: function(){
      this.DatosConfigNube.cBDAppPedidos = this.cambiarBooleano(this.DatosConfigNube.cBDAppPedidos);
      this.DatosConfigNube.cBDAppCheckIt = this.cambiarBooleano(this.DatosConfigNube.cBDAppCheckIt);
      this.DatosConfigNube.cBDBitacora = this.cambiarBooleano(this.DatosConfigNube.cBDBitacora);
      this.DatosConfigNube.cBDIndicadores = this.cambiarBooleano(this.DatosConfigNube.cBDIndicadores);
      this.DatosConfigNube.cBDPuestos = this.cambiarBooleano(this.DatosConfigNube.cBDPuestos);
      this.DatosConfigNube.cBDProductosNubeToLocal = this.cambiarBooleano(this.DatosConfigNube.cBDProductosNubeToLocal);
      this.DatosConfigNube.cBDRegistrarFacturas = this.cambiarBooleano(this.DatosConfigNube.cBDRegistrarFacturas);
      this.DatosConfigNube.cBDMesasAbiertasNube = this.cambiarBooleano(this.DatosConfigNube.cBDMesasAbiertasNube);
      this.DatosConfigNube.cBDMesasNube = this.cambiarBooleano(this.DatosConfigNube.cBDMesasNube);
      this.DatosConfigNube.cBDProductosNube = this.cambiarBooleano(this.DatosConfigNube.cBDProductosNube);
      this.DatosConfigNube.cBDRegAsistencia = this.cambiarBooleano(this.DatosConfigNube.cBDRegAsistencia);
      this.DatosConfigNube.cBDAdminTotal = this.cambiarBooleano(this.DatosConfigNube.cBDAdminTotal);
      this.DatosConfigNube.cBDSincronizarCorte = this.cambiarBooleano(this.DatosConfigNube.cBDSincronizarCorte);
      this.DatosConfigNube.cBDEmpleados = this.cambiarBooleano(this.DatosConfigNube.cBDEmpleados);
      this.DatosConfigNube.cDBQUerys = this.cambiarBooleano(this.DatosConfigNube.cDBQUerys);
    },
    cambiarCheckB: function(){
      this.DatosConfigNube.cBDAppPedidos = this.cambiarNumero(this.DatosConfigNube.cBDAppPedidos);
      this.DatosConfigNube.cBDAppCheckIt = this.cambiarNumero(this.DatosConfigNube.cBDAppCheckIt);
      this.DatosConfigNube.cBDBitacora = this.cambiarNumero(this.DatosConfigNube.cBDBitacora);
      this.DatosConfigNube.cBDIndicadores = this.cambiarNumero(this.DatosConfigNube.cBDIndicadores);
      this.DatosConfigNube.cBDPuestos = this.cambiarNumero(this.DatosConfigNube.cBDPuestos);
      this.DatosConfigNube.cBDProductosNubeToLocal = this.cambiarNumero(this.DatosConfigNube.cBDProductosNubeToLocal);
      this.DatosConfigNube.cBDRegistrarFacturas = this.cambiarNumero(this.DatosConfigNube.cBDRegistrarFacturas);
      this.DatosConfigNube.cBDMesasAbiertasNube = this.cambiarNumero(this.DatosConfigNube.cBDMesasAbiertasNube);
      this.DatosConfigNube.cBDMesasNube = this.cambiarNumero(this.DatosConfigNube.cBDMesasNube);
      this.DatosConfigNube.cBDProductosNube = this.cambiarNumero(this.DatosConfigNube.cBDProductosNube);
      this.DatosConfigNube.cBDRegAsistencia = this.cambiarNumero(this.DatosConfigNube.cBDRegAsistencia);
      this.DatosConfigNube.cBDAdminTotal = this.cambiarNumero(this.DatosConfigNube.cBDAdminTotal);
      this.DatosConfigNube.cBDSincronizarCorte = this.cambiarNumero(this.DatosConfigNube.cBDSincronizarCorte);
      this.DatosConfigNube.cBDEmpleados = this.cambiarNumero(this.DatosConfigNube.cBDEmpleados);
      this.DatosConfigNube.cDBQUerys = this.cambiarNumero(this.DatosConfigNube.cDBQUerys);
    },
    ObtenerConfigNube:function(){
      axios.get('/catalogo/configuracionNube/obtenerConfigNube').then(response=>{
         this.DatosConfigNube = response.data.ConfigNube;
         this.cambiarCheck()
         console.log(this.DatosConfigNube);
       }).catch(function (error){
         toastr.warning('Error','Ha ocurrido un error al obtener los datos');
         console.log(error);
      });
    },
    cambiarBooleano:function(bool){
      if(bool==="1"){
        return true;
      }
      else{
        return false;
      }
    },
    cambiarNumero:function(bool){
      if(bool===true){
        return 1;
      }
      else{
        return 0;
      }
    },
    abrirModal:function(Modal){
      this.ObtenerConfigNube();
      $('#'+Modal).modal("show");
    },
    cerrarModal:function(Modal){
      $('#'+Modal).modal("hide");
    },
    editarConNube:function(){
      this.cambiarCheckB()
      var ruta = "editarConfigNube"
      var datos = this.DatosConfigNube
      var modal = "EditarConfigNube"
      var accion = "editado"
      this.conexion(ruta, datos, modal, accion)
    },
    agregarConNube:function(){
      this.cambiarCheckB()
      var ruta = "agregarConfigNube"
      var datos = this.DatosConfigNube
      var modal = "AgregarConfigNube"
      var accion = "agregado"
      this.conexion(ruta, datos, modal, accion)
    },
    conexion:function(ruta, datos, modal, accion){
      var url = "/catalogo/configuracionNube/"+ruta;
       axios.post(url, datos).then(response=>{
          this.ObtenerConfigNube();
          toastr.success('Configuración '+ accion+' correctamente');
          $('#'+modal).modal("hide");
       }).catch(function (error){
          toastr.warning('Error','Ha ocurrido un error al '+accion+' configuración');
          console.log(error);
      });
    },
  },
});
