app = new Vue({
	el: '#clientesPrincipal',
	created:function(){
    this.obtenerClientes();
	},
	data:{
    dataCliente:{
      'cnombrecliente':null,
      'crfccliente':null,
      'ccurpcliente':null,
      'cdomiciliocomercial':null,
      'crepesentantelegal':null,
      'cciudadcliente':null,
      'cestadocliente':null,
      'ccodigopostal':null,
      'cpaiscliente':null,
      'ccorreoelectronico':null,
      'ctelefonocliente':null,
      'cnotascliente':null,
      'cestatuscliente':1,
      'cfechaalta':null,
      'cfechainactivo':null,
      'ccodigocliente':0,
			'ccodigoadmintotal':null,
    },
    clientes:[],
	},
  components: {
  },
	methods:{
    abrirModalAgregarCliente:function(){
      this.hoyFecha();
      $('#agregarClientesModal').modal('show');
    },
    abrirModalEditarCliente:function(idcliente){
      var cliente={'idcliente':idcliente};
			axios.post('/catalogos/clientes/dataeditar', cliente).then(response=>{
			this.dataCliente=response.data.clientes;
			$('#editarClientesModal').modal('show');
			});
    },
		abrirModalEliminarCliente:function(cliente){
			this.dataCliente=cliente;
      $('#eliminarClientesModal').modal('show');
    },
		abrirModalVerCliente:function(cliente){
			this.dataCliente=cliente;
      $('#verClientesModal').modal('show');
    },
    obtenerClientes:function(){
      axios.get('/catalogos/clientes/obtenerClientes').then(response=>{
         this.clientes = response.data.clientes;
         setTimeout(function(){$('#clientesTabla').DataTable({
               "language": {
                 "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
                 "pagingType": "full_numbers",
                 "processing": "Procesando...",
                 "sLengthMenu": "Mostrar _MENU_ registros encontrados"
               },
             });
         }); }, 3000).catch(function (error){
               toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
               console.log(error);
        });
    },
    agregarCliente:function(){
      var urlAgregar= "/catalogos/clientes/agregar";
			 axios.post(urlAgregar, this.dataCliente).then(response=>{
         toastr.success('El cliente se ha agregado correctamente');
         $("#clientesTabla").dataTable().fnDestroy();
         this.obtenerClientes();
         $('#agregarClientesModal').modal('hide');
         this.eliminarData();
		 });
    },
    editarCliente:function(){
      var urlEditar= "/catalogos/clientes/editar";
       axios.post(urlEditar, this.dataCliente).then(response=>{
         toastr.success('El cliente se ha modificado correctamente');
         $("#clientesTabla").dataTable().fnDestroy();
         this.obtenerClientes();
         $('#editarClientesModal').modal('hide');
         this.eliminarData();
     });
    },
		eliminarClientes:function(){
			var urlEliminar= "/catalogos/clientes/eliminar";
			 axios.post(urlEliminar, this.dataCliente).then(response=>{
				 toastr.success('El cliente se ha eliminado correctamente');
				 $("#clientesTabla").dataTable().fnDestroy();
				 this.obtenerClientes();
				 $('#eliminarClientesModal').modal('hide');
				 this.eliminarData();
		 });
		},
    hoyFecha:function(){
      var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth()+1;
			var day = date.getDate();
			if(day<10){
        day='0'+day;
      }
      if(month<10){
        month='0'+month;
      }
			this.dataCliente.cfechaalta = year + "-" + month + "-" + day;
    },
    eliminarData:function(){
      this.dataCliente={
        'cnombrecliente':null,
        'crfccliente':null,
        'ccurpcliente':null,
        'cdomiciliocomercial':null,
        'crepresentantelegal':null,
        'cciudadcliente':null,
        'cestadocliente':null,
        'ccodigopostal':null,
        'cpaiscliente':null,
        'ccorreoelectronico':null,
        'ctelefonocliente':null,
        'cnotascliente':null,
        'cestatuscliente':1,
        'cfechaalta':null,
        'cfechainactivo':null,
        'ccodigocliente':null
      };
    },
	},
});
