var app = new Vue({
  el: '#catalogoSubcategorias',
  created: function(){
    this.ObtenerCategorias();
    this.ObtenerSubCategorias();
  },
  data:{
    DatosSubCategoria: {},
    Categorias: [],
    Subcategorias: [],
  },
  methods:{
    iniciarSubCategoria:function(){
      this.DatosSubCategoria = {'cSCatCodigo':0, 'cSCatDescripcion': null, 'cSCatPadre': 0}
    },
    ModalAgregarSubcategoria:function(){
      $("#AgregarSubcategoria").modal("show");
    },
    ObtenerCategorias:function(){
      axios.get('/catalogo/categorias/obtenerCategorias').then(response=>{
         this.Categorias = response.data.Categorias;
       }).catch(function (error){
               toastr.warning('Error','Ha ocurrido un error al obtener las categorías');
               console.log(error);
      });
    },
    FormatearDescMayusculas:function(){
      this.DatosSubCategoria.cSCatDescripcion = this.DatosSubCategoria.cSCatDescripcion.toUpperCase();
    },
    agregarSubcategoria:function(){
      var ruta = "agregarSubcategoria";
      var modal = "AgregarSubcategoria";
      var accion = "agregada";
      this.conexion(ruta, this.DatosSubCategoria, modal,accion);
    },
    ObtenerSubCategorias:function(){
      axios.get('/catalogo/subcategorias/obtenerSubCategorias').then(response=>{
         this.Subcategorias = response.data.SubCategorias;
         setTimeout(function(){$('#table_subcategorias').DataTable({
               "language": {
                 "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
                 "pagingType": "full_numbers",
                 "processing": "Procesando...",
                 "sLengthMenu": "Mostrar _MENU_ registros encontrados"
               },
             });
         }); }, 3000).catch(function (error){
               toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
               console.log(error);
        });
    },
    VerModalEditar:function(subcategoria){
      this.DatosSubCategoria=subcategoria;
      $("#EditarSubcategoria").modal("show");
    },
    VerModalEliminar:function(subcategoria){
      this.DatosSubCategoria=subcategoria;
      $("#EliminarSubcategoria").modal("show");
    },
    eliminarSubategoria:function(){
      var ruta = "eliminarSubcategoria";
      var modal = "EliminarSubcategoria";
      var accion = "eliminada";
      this.conexion(ruta, this.DatosSubCategoria, modal,accion)
    },
    editarSubcategoria:function(){
      var ruta = "editarSubcategoria";
      var modal = "EditarSubcategoria";
      var accion = "editada";
      this.conexion(ruta, this.DatosSubCategoria, modal,accion)
    },
    conexion:function(ruta, datos, modal, accion){
      var url = "/catalogo/subcategorias/"+ruta;
       axios.post(url, datos).then(response=>{
         var estado = response.data;
         if(estado === 1)
         {
           toastr.warning('Error','El codigo de la subcategoria está repetido.');
        }
        else if (estado === 2) {
          toastr.warning('Error','Existen productos con esta categoria.');
        }
        else{
          $("#table_subcategorias").dataTable().fnDestroy();
          this.ObtenerSubCategorias();
          toastr.success('Subcategoria '+ accion+' correctamente');
          $('#'+modal).modal("hide");
          this.iniciarSubCategoria()
        }
       }).catch(function (error){
          toastr.warning('Error','Ha ocurrido un error al '+accion+' subcategoria');
          console.log(error);
      });
    },
  },
});
