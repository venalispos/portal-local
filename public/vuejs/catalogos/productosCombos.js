var app = new Vue({
  el: '#botonesCombos',
  created: function(){
    this.obtenerDatos()
    this.iniciardatosComProductos()
  },
  data:{
    ComProductos:[],
    dataComProductos:{},
    ComSubCategorias:[],
    Productos:[]
  },
  methods:{
    iniciardatosComProductos: function(){
      this.dataComProductos={cComProdIdPadre:'',cComProdIdProd:''}
    },
    obtenerDatos:function(){
    axios.get('/catalogo/comboProductos/obtenerComProductos').then(response=>{
     this.ComProductos = response.data.ComProductos;
     this.ComSubCategorias = response.data.ComSubCategorias;
     this.Productos = response.data.Productos;
     setTimeout(function(){$('#table_productoscombo').DataTable({
           "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
             "pagingType": "full_numbers",
             "processing": "Procesando...",
             "sLengthMenu": "Mostrar _MENU_ registros encontrados"
           },
         });
     }); }, 3000).catch(function (error){
           toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
           console.log(error);
  });
  },
  abrirModal:function(Modal, Datos){
    if(Datos===0){
      this.iniciardatosComProductos()
    }
    else{
      this.dataComProductos=Datos;
    }
    $('#'+Modal).modal("show");
  },
  cerrarModal:function(Modal){
    $('#'+Modal).modal("hide");
  },
  editarProductosCombos:function(){
    var ruta = "editarComProductos"
    var datos = this.dataComProductos
    var modal = "EditarComProductos"
    var accion = "editada"
    this.conexion(ruta, datos, modal, accion)
  },
  agregarProductoCombos:function(){
    var ruta = "agregarComProductos"
    var datos = this.dataComProductos
    var modal = "AgregarComProductos"
    var accion = "agregada"
    this.conexion(ruta, datos, modal, accion)
  },
  eliminarProductosCombos:function(){
    var ruta = "eliminarComProductos"
    var datos = this.dataComProductos
    var modal = "EliminarComProducto"
    var accion = "eliminada"
    this.conexion(ruta, datos, modal, accion)
  },
  conexion:function(ruta, datos, modal, accion){
    var url = "/catalogo/comboProductos/"+ruta;
     axios.post(url, datos).then(response=>{
       $("#table_productoscombo").dataTable().fnDestroy();
        this.obtenerDatos();
        toastr.success('Producto '+ accion+' correctamente');
        $('#'+modal).modal("hide");
     }).catch(function (error){
        toastr.warning('Error','Ha ocurrido un error');
        console.log(error);
    });
  },
  },
});
