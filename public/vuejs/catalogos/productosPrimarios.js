var app = new Vue({
 el: '#ProductosPrimarios',
 created: function(){
    this.obtenerDatos();
 },
 data: {
   listaProdPrimarios: [],
   idProdPrimario: 0,
   productoPrimario: {cProPriDescripcion: '', cProPriDetEquivalencia:''},
   numeroRegistros: 10,
   offset: 2,
   pagination: {
       'total': 0,
       'current_page': 1,
       'per_page': 0,
       'last_page': 0,
       'from': 0,
       'to': 0,
   },
   Cargando: 1,
 },
 computed: {
     isActived: function() {
         return this.pagination.current_page;
     },

     pagesNumber: function(){
         if(!this.pagination.to){
             return [];
         }

         var from = this.pagination.current_page - this.offset; // Desde qué # página mostrará botón
         if ( from < 1 ){
             from = 1;
         }

         var to = from + (this.offset * 2);                    // Hasta qué # página mostrará botón
         if( to >= this.pagination.last_page ){
             to = this.pagination.last_page;
         }
         var pagesArray = [];
         while( from <= to){
             pagesArray.push( from );
             from++;
         }
         return pagesArray;
     },
 },
 methods: {

   changePage: function (page){
         this.pagination.current_page = page;
         this.obtenerDatos(page);
     },

   abrirModal:function(){
       $('#AgregarProductoPrimario').modal("show");
     },

   cerrarModal:function(){
       $('#AgregarProductoPrimario').modal("hide");
       this.limpiarDatos();
     },

   agregarProductoPrimario: function() {
     var url = "/Catalogos/ProductosPrimarios/agregarProductosPrimarios";
     var datos = {
       'data':this.productoPrimario,
       'id':this.idProdPrimario,
     }

      axios.post(url, datos).then(response=>{
        this.validarProdPrimario(response.data);
        this.limpiarDatos();
      }).catch(function (error){
         toastr.warning('Error','Ha ocurrido un error ');
         console.log(error);
     });
   },

   limpiarDatos:function() {
     this.idProdPrimario = 0;
     this.productoPrimario.cProPriDescripcion = '';
     this.productoPrimario.cProPriDetEquivalencia = '';
   },

   obtenerDatos:function(){
     var page = this.pagination.current_page;
     this.isLoading = 1;

     var UrlListaProductosPrimarios = '/Catalogos/ProductosPrimarios/obtenerDatos?page='+page+'&registros='+this.numeroRegistros;
     // var UrlListaProductosPrimarios = '/Catalogos/ProductosPrimarios/obtenerDatos';
     axios.get(UrlListaProductosPrimarios).then(response=>{
       // this.listaProdPrimarios = response.data;
       this.isLoading = 0;
       this.listaProdPrimarios = response.data.listaProdPrimarios.data;
       this.pagination = response.data.paginate;
     });
 },

   abrirModalEditarProdPrimario:function(producto){
     this.idProdPrimario = producto.cProPriFolio;
     this.productoPrimario = {'cProPriDescripcion': producto.cProPriDescripcion, 'cProPriDetEquivalencia': producto.cProPriDetEquivalencia};
     $('#EditarProductoPrimario').modal("show");
   },

   cerrarModalEditarProdPrimario:function(){
     $('#EditarProductoPrimario').modal("hide");
     this.limpiarDatos();
   },

   editarProductoPrimario:function(){
     var url = "/Catalogos/ProductosPrimarios/editarProductosPrimarios";
     var datos = {
       'data':this.productoPrimario,
       'id':this.idProdPrimario,
     }
      axios.post(url, datos).then(response=>{
        if(response.data === 0) { this.cerrarModalEditarProdPrimario(); }
        this.validarProdPrimario(response.data);
      }).catch(function (error){
         toastr.warning('Error','Ha ocurrido un error ');
         console.log(error);
     });
   },

   eliminarProductoPrimario:function(){
     var url = "/Catalogos/ProductosPrimarios/eliminarProductosPrimarios";
     var datos = {
       'id':this.idProdPrimario,
     }
      axios.post(url, datos).then(response=>{
        toastr.success("Se elimino correctamente.");
        this.cerrarModalEliminarProdPrimario();
        this.obtenerDatos();
      }).catch(function (error){
         toastr.warning('Error','Ha ocurrido un error ');
         console.log(error);
     });
   },

   abrirModalEliminarProdPrimario:function(producto){
     this.idProdPrimario = producto.cProPriFolio;
     this.productoPrimario = {'cProPriDescripcion': producto.cProPriDescripcion};
     $('#EliminarProductoPrimario').modal("show");
   },

   cerrarModalEliminarProdPrimario:function(){
     $('#EliminarProductoPrimario').modal("hide");
     this.limpiarDatos();
   },

   validarProdPrimario:function(producto) {
     if(producto === 0) {
       toastr.success("Se ha registrado correctamente.");
       this.obtenerDatos();
     } else {
       toastr.warning("El nombre del producto primario, ya se encuentra registrado.");
     }
   },

 },

 components: {
        Multiselect: window.VueMultiselect.default,
}
});
