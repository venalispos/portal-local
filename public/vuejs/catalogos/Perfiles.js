var app = new Vue({
  el: '#catalogoPerfiles',
  created: function(){
    this.obtenerDatos();
    this.iniciardatosPerfiles();
    this.FunObtenerDatosMenu();
  },
  data:{
    datosPerfil:{},
    Perfiles:[],
    Acciones:[],
    agregarPermisos:[],
    InventarioPermisos:[],
    AccionesSeleccionadas:[],
    menu:null,
    permisosPortalSeleccionados:[],
    datosInventario:[
      {
        'descripcion':"Agregar",
        'valor':1
      },
      {
        'descripcion':"Editar",
        'valor':2
      },
      {
        'descripcion':"Eliminar",
        'valor':3
      },
      {
        'descripcion':"Reportes",
        'valor':4
      }
    ]

  },
  methods:{
    iniciardatosPerfiles: function(){
      this.datosPerfil={cPerAccesPortal: '0',cPerAccesoClave: 0,cPerAccion: '',cPerAutDividir: 0,cPerAutExtra: 0,cPerAutTransfer: 0,cPerAutorizaSalida: 0,cPerCategorias: ' ',cPerDesc: "NO",cPerFolio: 0,cPerId: 0,cPerLiquida: 0,cPerProductos: ' ',cPerReImpTicket: 0,cPerRecibeCuentas: 0,cPerServicio: 0,cPerSubCategorias: ' '}
    },
    obtenerDatos:function(){
    axios.get('/catalogo/perfiles/obtenerPerfiles').then(response=>{
     this.Perfiles = response.data.Perfiles;
     this.Acciones = response.data.Acciones;
     setTimeout(function(){$('#table_catalogoperfiles').DataTable({
           "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
             "pagingType": "full_numbers",
             "processing": "Procesando...",
             "sLengthMenu": "Mostrar _MENU_ registros encontrados"
           },
         });
     }); }, 3000).catch(function (error){
           toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
           console.log(error);
  });
  },
  cambiarUno:function(valor){
    var uno = valor
    if(uno===1){
      uno = true
    }
    else {
      uno = false
    }
    return uno
  },
  cambiarCheck: function(valor){
    var Boleano = valor
    if(Boleano){
      Boleano=1;
    }else{
      Boleano=0;
    }
    return Boleano
  },
  FunObtenerDatosMenu:function(){
    route='/menu/configuracion/lista';
    axios.get(route).then(response=>{
      this.menu=response.data.listamenu;
    });
  },
  CambiarCheckBox:function(){
    this.datosPerfil.cPerAccesoClave = this.cambiarUno(this.datosPerfil.cPerAccesoClave)
    this.datosPerfil.cPerAutDividir = this.cambiarUno(this.datosPerfil.cPerAutDividir)
    this.datosPerfil.cPerAutExtra = this.cambiarUno(this.datosPerfil.cPerAutExtra)
    this.datosPerfil.cPerAutTransfer = this.cambiarUno(this.datosPerfil.cPerAutTransfer)
    this.datosPerfil.cPerAutorizaSalida = this.cambiarUno(this.datosPerfil.cPerAutorizaSalida)
    this.datosPerfil.cPerLiquida = this.cambiarUno(this.datosPerfil.cPerLiquida)
    this.datosPerfil.cPerReImpTicket = this.cambiarUno(this.datosPerfil.cPerReImpTicket)
    this.datosPerfil.cPerRecibeCuentas = this.cambiarUno(this.datosPerfil.cPerRecibeCuentas)
    this.datosPerfil.cPerServicio = this.cambiarUno(this.datosPerfil.cPerServicio)
    var _this = this
    if(this.datosPerfil.Permisos!=null){
    this.datosPerfil.Permisos.forEach(function(permiso) {
      permiso.activo = _this.cambiarUno(permiso.activo)
    });
    }
  },
  CambiarCheckBoxB:function(){
    this.datosPerfil.cPerAccesoClave = this.cambiarCheck(this.datosPerfil.cPerAccesoClave)
    this.datosPerfil.cPerAutDividir = this.cambiarCheck(this.datosPerfil.cPerAutDividir)
    this.datosPerfil.cPerAutExtra = this.cambiarCheck(this.datosPerfil.cPerAutExtra)
    this.datosPerfil.cPerAutTransfer = this.cambiarCheck(this.datosPerfil.cPerAutTransfer)
    this.datosPerfil.cPerAutorizaSalida = this.cambiarCheck(this.datosPerfil.cPerAutorizaSalida)
    this.datosPerfil.cPerLiquida = this.cambiarCheck(this.datosPerfil.cPerLiquida)
    this.datosPerfil.cPerReImpTicket = this.cambiarCheck(this.datosPerfil.cPerReImpTicket)
    this.datosPerfil.cPerRecibeCuentas = this.cambiarCheck(this.datosPerfil.cPerRecibeCuentas)
    this.datosPerfil.cPerServicio = this.cambiarCheck(this.datosPerfil.cPerServicio)
    var _this = this
    var perAccion = ""
    this.datosPerfil.Permisos.forEach(function(permiso) {
      permiso.activo = _this.cambiarCheck(permiso.activo)
  });
  },

  FormatearDescMayusculas:function(){
    this.datosPerfil.cPerDesc = this.datosPerfil.cPerDesc.toUpperCase();
  },
  abrirModal:function(Modal, Datos){
    if(Datos === 0){
        this.datosPerfil=Datos;
      this.iniciardatosPerfiles()
      this.InventarioPermisos= [];
      this.AccionesSeleccionadas= [];
      this.permisosPortalSeleccionados =[];
    }
    else{

      this.datosPerfil=Datos;
      this.permisosPortalSeleccionados=   this.datosPerfil['cPerAccesPortal'].split(",");
      this.InventarioPermisos =this.datosPerfil['cPerInventario'].split(",");
      this.FunAgregarNuevoCampoArrayPortal();
    }
    this.CambiarCheckBox()
    $('#'+Modal).modal("show");
  },
  FunCambiarActivo:function(value){
    value.activo=1;
  },
  cerrarModal:function(Modal){
    this.CambiarCheckBoxB()
    $('#'+Modal).modal("hide");
  },
  cerrarModalN:function(Modal){
    $('#'+Modal).modal("hide");
  },
  editarPerfil:function(){
    var ruta = "editarPerfil"
    var datos = {
      'data':this.datosPerfil,
      'inventario':this.InventarioPermisos,
      'acciones':this.AccionesSeleccionadas,
      'portal':this.permisosPortalSeleccionados
    }
    var modal = "EditarPerfil"
    var accion = "editada"
    this.CambiarCheckBoxB()
    this.conexion(ruta, datos, modal, accion)
  },
  agregarPerfil:function(){
    var ruta = "agregarPerfil"
    var datos = {
      'data':this.datosPerfil,
      'inventario':this.InventarioPermisos,
      'acciones':this.AccionesSeleccionadas,
      'portal':this.permisosPortalSeleccionados
    }
    var modal = "AgregarPerfil"
    var accion = "agregada"
    datos.cPerAccion = this.agregarPermisos
    console.log(datos);
    this.conexion(ruta, datos, modal, accion)
  },
  eliminarPerfiles:function(){
    var ruta = "eliminarPerfil"
    var datos = this.datosPerfil
    var modal = "EliminarPerfil"
    var accion = "eliminada"
    this.conexion(ruta, datos, modal, accion)
  },
  AccionesAgregar:function(ID){
    var index = this.agregarPermisos.indexOf(ID);
    if(index !== -1){
      this.agregarPermisos.splice(index, 1)
    }
    else{
      this.agregarPermisos.push(ID);
    }
  },
  conexion:function(ruta, datos, modal, accion){
    var url = "/catalogo/perfiles/"+ruta;
     axios.post(url, datos).then(response=>{
       $("#table_catalogoperfiles").dataTable().fnDestroy();
        this.obtenerDatos();
        toastr.success('Pefil '+ accion+' correctamente');
        $('#'+modal).modal("hide");
     }).catch(function (error){
        toastr.warning('Error','Ha ocurrido un error');
        console.log(error);
    });
  },

  FunAgregarNuevoCampoArrayPortal:function(){
    this.menu.forEach( function(elemento) {
    elemento.activo=0;
});
  }

  },
});
