var app = new Vue({
  el: '#botonesProducto',
  created: function(){
    this.obtenerDatos();
  },
  data:{
    BotonProductos:[],
    Productos:[],
    BotSubCategorias:[],
    datosBotonProducto:{},
  },
  methods:{
    iniciardatosBotonProductos: function(){
      this.datosBotonProducto={cBotProIdPadre:'', cBotProIdProd:''}
    },
    obtenerDatos:function(){
      this.iniciardatosBotonProductos()
    axios.get('/catalogo/botonProductos/obtenerBotonProductos').then(response=>{
     this.BotonProductos = response.data.BotProductos;
     this.Productos = response.data.Productos;
     this.BotSubCategorias = response.data.BotSubCategorias;
     setTimeout(function(){$('#table_botonproductos').DataTable({
           "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
             "pagingType": "full_numbers",
             "processing": "Procesando...",
             "sLengthMenu": "Mostrar _MENU_ registros encontrados"
           },
         });
     }); }, 3000).catch(function (error){
           toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
           console.log(error);
  });
  },
  abrirModal:function(Modal, Datos){
    if(Datos===0){
      this.iniciardatosBotonProductos()
    }
    else{
      this.datosBotonProducto=Datos;
    }
    $('#'+Modal).modal("show");
  },
  cerrarModal:function(Modal){
    $('#'+Modal).modal("hide");
  },
  editarBotonProductos:function(){
    var ruta = "editarBotonProductos"
    var datos = this.datosBotonProducto
    var modal = "EditarBotonProductos"
    var accion = "editada"
    this.conexion(ruta, datos, modal, accion)
  },
  agregarBotonProductos:function(){
    var ruta = "agregarBotonProductos"
    var datos = this.datosBotonProducto
    var modal = "AgregarBotonProductos"
    var accion = "agregada"
    this.conexion(ruta, datos, modal, accion)
  },
  eliminarBotonProducto:function(){
    var ruta = "eliminarBotonProductos"
    var datos = this.datosBotonProducto
    var modal = "EliminarBotonProducto"
    var accion = "eliminada"
    this.conexion(ruta, datos, modal, accion)
  },
  conexion:function(ruta, datos, modal, accion){
    var url = "/catalogo/botonProductos/"+ruta;
     axios.post(url, datos).then(response=>{
       $("#table_botonproductos").dataTable().fnDestroy();
        this.obtenerDatos();
        toastr.success('Categoria '+ accion+' correctamente');
        $('#'+modal).modal("hide");
     }).catch(function (error){
        toastr.warning('Error','Ha ocurrido un error al '+accion+' categoria');
        console.log(error);
    });
  },
  },
});
