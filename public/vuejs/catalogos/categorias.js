var app = new Vue({
  el: '#catalogoCategorias',
  created: function(){
    this.ObtenerCategorias();
  },
  data:{
    DatosCategorias: {},
    Categorias: [],
  },
  methods:{
    ModalAgregarCategoria:function(){
      $("#AgregarCategoria").modal("show");
    },
    iniciarCategoria:function(){
      this.DatosCategorias = {'cCatFolio': 0, 'cCatCodigo': 0, 'cCatDescripcion': null}
    },
    FormatearDescMayusculas:function(){
      this.DatosCategorias.cCatDescripcion = this.DatosCategorias.cCatDescripcion.toUpperCase();
    },
    agregarCategoria:function(){
      var ruta = "agregarCategoria";
      var modal = "AgregarCategoria";
      var accion = "agregada";
      this.crearTabla(ruta, this.DatosCategorias, modal,accion)
    },
    ObtenerCategorias:function(){
      this.iniciarCategoria()
      axios.get('/catalogo/categorias/obtenerCategorias').then(response=>{
         this.Categorias = response.data.Categorias;
         setTimeout(function(){$('#table_categorias').DataTable({
               "language": {
                 "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
                 "pagingType": "full_numbers",
                 "processing": "Procesando...",
                 "sLengthMenu": "Mostrar _MENU_ registros encontrados"
               },
             });
         }); }, 3000).catch(function (error){
               toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
               console.log(error);
        });
    },
    ModalEditarCategoria:function(Categoria){
      this.DatosCategorias = Categoria;
      $("#EditarCategoria").modal("show");
    },

    crearTabla:function(ruta, datos, modal, accion){
      var url = "/catalogo/categorias/"+ruta;
       axios.post(url, datos).then(response=>{
         var estado = response.data;
         if(estado === 1)
         {
           toastr.warning('Error','El codigo de la categoria está repetido.');
        }
        else if (estado === 2) {
          toastr.warning('Error','Existen subcategorias con este codigo.');
        }
        else{
          $("#table_categorias").dataTable().fnDestroy();
          this.ObtenerCategorias();
          toastr.success('Categoria '+ accion+' correctamente');
          $('#'+modal).modal("hide");
          this.iniciarCategoria()
        }
       }).catch(function (error){
          toastr.warning('Error','Ha ocurrido un error al '+accion+' Categoria');
          console.log(error);
      });
    },
    editarCategoria:function(){
      var ruta = "editarCategoria";
      var modal = "EditarCategoria";
      var accion = "editada";
      this.crearTabla(ruta, this.DatosCategorias, modal,accion)
    },
    ModalEliminarCategoria:function(Categoria){
      this.DatosCategorias = Categoria;
      $("#EliminarCategoria").modal("show");
    },
    eliminarCategoria:function(){
      console.log("llegue");
      var ruta = "eliminarCategorias";
      var modal = "EliminarCategoria";
      var accion = "eliminada";
      this.crearTabla(ruta, this.DatosCategorias, modal,accion)
    }
  },
});
