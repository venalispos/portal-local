var app = new Vue({
  el: '#botonesSubcategorias',
  created: function(){
    this.obtenerDatos();
    this.iniciardatosBotonSubCategorias()
  },
  data:{
    BotonSubCategorias:[],
    BotonCategorias:[],
    datosBotonSubCategorias:{},
  },
  methods:{
    iniciardatosBotonSubCategorias: function(){
      this.datosBotonSubCategorias={cBotSubCatDescripcion:"", cBotSubCatIdPadre:0}
    },
    obtenerDatos:function(){
    axios.get('/catalogo/botonSubcategorias/obtener').then(response=>{
     this.BotonSubCategorias = response.data.BotSubCategorias;
     this.BotonCategorias = response.data.BotCategorias;
     setTimeout(function(){$('#table_botonsubcategorias').DataTable({
           "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
             "pagingType": "full_numbers",
             "processing": "Procesando...",
             "sLengthMenu": "Mostrar _MENU_ registros encontrados"
           },
         });
     }); }, 3000).catch(function (error){
           toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
           console.log(error);
  });
  },
  abrirModal:function(Modal, Datos){
    if(Datos===0){
      this.iniciardatosBotonSubCategorias()
    }
    else{
      this.datosBotonSubCategorias=Datos;
    }
    $('#'+Modal).modal("show");
  },
  cerrarModal:function(Modal){
    $('#'+Modal).modal("hide");
  },
  editarBotonSubcategoria:function(){
    var ruta = "editarBotonSubCategorias"
    var datos = this.datosBotonSubCategorias
    var modal = "EditarBotonSubcategorias"
    var accion = "editada"
    this.conexion(ruta, datos, modal, accion)
  },
  agregarBotonSubcategoria:function(){
    var ruta = "agregarBotonSubCategorias"
    var datos = this.datosBotonSubCategorias
    var modal = "AgregarBotonSubcategorias"
    var accion = "agregada"
    this.conexion(ruta, datos, modal, accion)
  },
  eliminarBotonSubcategoria:function(){
    var ruta = "eliminarBotonSubCategorias"
    var datos = this.datosBotonSubCategorias
    var modal = "EliminarBotonSubcategorias"
    var accion = "eliminada"
    this.conexion(ruta, datos, modal, accion)
  },
  conexion:function(ruta, datos, modal, accion){
    var url = "/catalogo/botonSubcategorias/"+ruta;
     axios.post(url, datos).then(response=>{
       $("#table_botonsubcategorias").dataTable().fnDestroy();
        this.obtenerDatos();
        toastr.success('Categoria '+ accion+' correctamente');
        $('#'+modal).modal("hide");
     }).catch(function (error){
        toastr.warning('Error','Ha ocurrido un error al '+accion+' categoria');
        console.log(error);
    });
  },
  FormatearMayusculas:function(){
    this.datosBotonSubCategorias.cBotSubCatDescripcion = this.datosBotonSubCategorias.cBotSubCatDescripcion.toUpperCase();
  },
  },
});
