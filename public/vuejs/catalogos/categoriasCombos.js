var app = new Vue({
  el: '#categoriasCombos',
  created: function(){
    this.ObtenerCategoriasComb();
  },
  data:{
    CategoriasComb:  [],
    DatosCatCombo: {},
  },
  methods:{
    iniciarDatosCatCombo:function(){
      this.DatosCatCombo = {'cComCatDesc': null}
    },
    ObtenerCategoriasComb:function(){
      axios.get('/catalogo/comcategorias/obtenerComCategorias').then(response=>{
         this.CategoriasComb = response.data.ComCategorias;
         setTimeout(function(){$('#table_categoriasCombos').DataTable({
               "language": {
                 "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
                 "pagingType": "full_numbers",
                 "processing": "Procesando...",
                 "sLengthMenu": "Mostrar _MENU_ registros encontrados"
               },
             });
         }); }, 3000).catch(function (error){
               toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
               console.log(error);
        });
    },
    ModalAgregarComCategoria:function(){
      $("#AgregarCatCombo").modal("show");
    },
    ModalEditarComCategoria:function(catcombos){
      this.DatosCatCombo = catcombos;
      $("#EditarCatCombo").modal("show");
    },
    ModalEliminarComCategoria:function(catcombos){
      this.DatosCatCombo = catcombos;
      $("#EliminarCatCombo").modal("show");
    },
    FormatearDescMayusculas:function(){
      this.DatosCatCombo.cComCatDesc = this.DatosCatCombo.cComCatDesc.toUpperCase();
    },
    agregarCatCombo:function(){
      var ruta = "agregarComCategorias";
      var modal = "AgregarCatCombo";
      var accion = "agregada";
      this.crearTabla(ruta, this.DatosCatCombo, modal,accion);
    },
    editarCatCombo:function(){
      var ruta = "editarComCategorias";
      var modal = "EditarCatCombo";
      var accion = "modificada";
      this.crearTabla(ruta, this.DatosCatCombo, modal,accion);
    },
    eliminarCatCombo:function(){
      var ruta = "eliminarComCategorias";
      var modal = "EliminarCatCombo";
      var accion = "eliminada";
      this.crearTabla(ruta, this.DatosCatCombo, modal,accion);
    },
    crearTabla:function(ruta, datos, modal, accion){
      var url = "/catalogo/comcategorias/"+ruta;
       axios.post(url, datos).then(response=>{
          $("#table_categoriasCombos").dataTable().fnDestroy();
          this.ObtenerCategoriasComb();
          toastr.success('Categoria '+ accion+' correctamente');
          $('#'+modal).modal("hide");
       }).catch(function (error){
          toastr.warning('Error','Ha ocurrido un error al '+accion+' Categoria');
          console.log(error);
      });
    },
    cerrarModal:function(Modal){
      this.iniciarDatosCatCombo();
      $('#'+Modal).modal("hide");
    },
  },
});
