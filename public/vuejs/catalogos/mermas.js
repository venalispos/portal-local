var app = new Vue({
  el: '#mermas',
  created: function(){
    this.obtener_categorias();
  },
  data:{
      lista_mermas:[],
      lista_categorias:[],

      date_search:'',
      date_waste:'',

      selected_categoria:1,
      waste_quantity:1

  },


  methods:{

    obtener_categorias(){
      var url_categorias = '/catalogo/mermas/categorias';

      axios.get(url_categorias).then(response => {
          this.lista_categorias = response.data
      });
    },

    obtener_mermas:function(){
      var urlMermas = "/catalogos/mermas/obtener_mermas";

      data_search = {
        'date_waste':this.date_search,
      }

      axios.post(urlMermas, data_search).then(response => {
          this.lista_mermas = response.data
      });

    },

    ver_agregar_merma(){

      $('#agregar_merma').modal('show');

      var urlObtenerFecha = "/catalogos/mermas/obtener_fecha";

      axios.get(urlObtenerFecha).then(response => {
        this.date_waste = response.data
      });
    },

    agregar_merma(){

      var urlAgregarMerma = '/catalogos/mermas/agregar_merma';

      let response = '';

      data_add = {
        'cCategoria':this.selected_categoria,
        'cCantidad':this.waste_quantity
      }

      axios.post(urlAgregarMerma, data_add).then(response => {

        response = response.data.response;
        if(response == 0){
        toastr.error("You cant't add the waste, the waste already exist!");
      }else if(response == 1){
        toastr.success("The waste was added");
        $("#agregar_merma").modal("hide");
      }

      }).catch(error => {
        toastr.error("Ocurrió un error al agregar la merma");
      });
    }
  },


});
