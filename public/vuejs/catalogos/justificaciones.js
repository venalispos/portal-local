var app = new Vue({
  el: '#Justificaciones',
  created: function(){
    this.getJustificaciones();
 },
  data: {
    justificaciones: [],
    justificacion: {
      'cJusCategoria': 0,
      'cJusDescripcion': '',
    },
    fillJustificacion: {
      'cJusFolio': 0,
      'cJusID': 0,
      'cJusCategoria': 0,
      'cJusDescripcion': '',
    },
    descuento: {
      'cDescIdPadre': 0,
      'cDescDescripcion': '',
      'cDescCantidad': 0,
    },
    fillDescuento: {
      'cDescFolio': 0,
      'cDescIdPadre': 0,
      'cDescDescripcion': '',
      'cDescCantidad': 0,
    }
 },
 computed: {

 },
 methods: {
   getJustificaciones: function() {
       var url = '/catalogos/justificaciones/obtenerJustificaciones';
       axios.get(url).then(response => {
           this.justificaciones = response.data;
       });
   },

   abrirModal: function() {
     $('#AgregarJustificacion').modal("show");
   },

   cerrarModal: function() {
     this.limpiarDatos();
     $('#AgregarJustificacion').modal("hide");
   },

   abrirModalEditar: function(justificacion) {
     this.fillJustificacion = justificacion;
     this.descuentos = justificacion.descuentos;
     $("#EditarJustificacion").modal("show");
  },

   cerrarModalEditar: function() {
    this.limpiarDatos();
    $('#EditarJustificacion').modal("hide");
  },

   abrirModalEliminar: function(justificacion) {
    this.fillJustificacion = justificacion;
    this.descuentos = justificacion.descuentos;
    $("#EliminarJustificacion").modal("show");
 },

   cerrarModalEliminar: function() {
   this.limpiarDatos();
   $('#EliminarJustificacion').modal("hide");
 },

   limpiarDatos: function() {
    this.justificacion = {
      'cJusCategoria': 0,
      'cJusDescripcion': '',
    };

    this.fillJustificacion = {
      'cJusFolio': 0,
      'cJusID': 0,
      'cJusCategoria': 0,
      'cJusDescripcion': ''
    };

    this.descuento = {
      'cDescIdPadre': 0,
      'cDescDescripcion': '',
      'cDescCantidad': 0,
    };

    this.fillDescuento = {
      'cDescFolio': 0,
      'cDescIdPadre': 0,
      'cDescDescripcion': '',
      'cDescCantidad': 0,
    };
  },

   agregarJustificacion: function() {
   var url = '/catalogos/justificaciones/agregarJustificacion';
   var data = {
     'justificacion': this.justificacion
   };

    axios.post(url, data).then(response => {
      if (response.data === 0) { this.cerrarModal(); }
      toastr.success("Nueva justificación creada con éxito..");
      this.getJustificaciones();
      // this.cerrarModal();
    }).catch(function (error) {
       toastr.warning('Error','Ha ocurrido un error ');
       console.log(error);
   });
 },

   editarJustificacion: function() {
    var url = "/catalogos/justificaciones/editarJustificacion";
    var data = {
      'justificacion': this.fillJustificacion
    };

    axios.post(url, data).then(response => {
       if (response.data === 0) { this.cerrarModalEditar(); }
       toastr.success("Justificación actualizada con éxito.");
       this.getJustificaciones();
       this.cerrarModalEditar();
     }).catch(function (error) {
        toastr.warning('Error','Ha ocurrido un error ');
        console.log(error);
    });
  },

   eliminarJustificacion: function() {
   var url = "/catalogos/justificaciones/eliminarJustificacion";
   var data = {
     'justificacion': this.fillJustificacion
   };

   axios.post(url, data).then(response => {
      if (response.data === 0) { this.cerrarModalEditar(); }
      toastr.success("Eliminado correctamente.");
      this.getJustificaciones();
      this.cerrarModalEliminar();
    }).catch(function (error) {
       toastr.warning('Error','Ha ocurrido un error ');
       console.log(error);
   });
 },

   abrirModalDescuento: function() {
     $('#AgregarDescuento').modal("show");
   },

   cerrarModalDescuento: function() {
     // this.limpiarDatos();
     $('#AgregarDescuento').modal("hide");
   },

   abrirModalEditarDesc: function(descuento) {
     this.fillDescuento = descuento;
     $('#EditarDescuento').modal("show");
   },

   cerrarModalEditarDesc: function() {
     this.fillDescuento = {
       'cDescFolio': 0,
       'cDescIdPadre': 0,
       'cDescDescripcion': '',
       'cDescCantidad': 0,
     };
     $('#EditarDescuento').modal("hide");
   },

   abrirModalEliminarDesc: function(descuento) {
     this.fillDescuento = descuento;
     $('#EliminarDescuento').modal("show");
   },

   cerrarModalEliminarDesc: function() {
     this.fillDescuento = {
       'cDescFolio': 0,
       'cDescIdPadre': 0,
       'cDescDescripcion': '',
       'cDescCantidad': 0,
     };
     $('#EliminarDescuento').modal("hide");
   },

   agregarDescuento: function() {
   var url = '/catalogos/justificaciones/agregarDescuento';
   var data = {
     'descuento': this.descuento
   };

    axios.post(url, data).then(response => {
      if (response.data === 0) { this.cerrarModalDescuento(); }
      toastr.success("Nuevo descuento creado con éxito.");
      this.getJustificaciones();
      this.cerrarModalDescuento();
    }).catch(function (error) {
       toastr.warning('Error','Ha ocurrido un error ');
       console.log(error);
   });
 },

   editarDescuento: function() {
     var url = "/catalogos/justificaciones/editarDescuento";
     var data = {
       'descuento': this.fillDescuento
     };

     axios.post(url, data).then(response => {
        if (response.data === 0) { this.cerrarModalEditarDesc(); }
        toastr.success("Descuento actualizado con éxito.");
        this.getJustificaciones();
        this.cerrarModalEditarDesc();
      }).catch(function (error) {
         toastr.warning('Error','Ha ocurrido un error ');
         console.log(error);
     });
  },

   eliminarDescuento: function() {
    var url = "/catalogos/justificaciones/eliminarDescuento";
    var data = {
      'descuento': this.fillDescuento
    };

    axios.post(url, data).then(response => {
       if (response.data === 0) { this.cerrarModalEliminarDesc(); }
       toastr.success("Eliminado correctamente.");
       this.getJustificaciones();
       this.cerrarModalEliminarDesc();
       this.cerrarModalEditar();
     }).catch(function (error) {
        toastr.warning('Error','Ha ocurrido un error ');
        console.log(error);
    });
  },
},
components: {

  Multiselect: window.VueMultiselect.default
}

});
