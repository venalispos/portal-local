var app = new Vue({
  el: '#subcategoriasCombos',
  created: function(){
    this.ObtenerSubcategoriasComb();
    this.ObtenerCategoriasComb();
  },
  data:{
    SubcategoriasComb:  [],
    DatosSubcatCombo: {},
    categorias: [],
  },
  methods:{
    iniciarDatos:function(){
      this.DatosSubcatCombo = {'cComSubCatDescripcion': null, 'cComSubCatIdPadre': 0}
    },
    ObtenerSubcategoriasComb:function(){
      axios.get('/catalogo/comsubcategorias/obtenerComSubCategorias').then(response=>{
         this.SubcategoriasComb = response.data.ComSubCategorias;
         setTimeout(function(){$('#table_subcategoriasCombos').DataTable({
               "language": {
                 "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
                 "pagingType": "full_numbers",
                 "processing": "Procesando...",
                 "sLengthMenu": "Mostrar _MENU_ registros encontrados"
               },
             });
         }); }, 3000).catch(function (error){
               toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
               console.log(error);
        });
    },
    ObtenerCategoriasComb:function(){
      axios.get('/catalogo/comcategorias/obtenerComCategorias').then(response=>{
         this.categorias = response.data.ComCategorias;
       }).catch(function (error){
               toastr.warning('Error','Ha ocurrido un error al obtener las categorías');
               console.log(error);
      });
    },
    ModalAgregarComSubcategoria:function(){
      $("#AgregarSubcatCombo").modal("show");
    },
    FormatearDescMayusculas:function(){
      this.DatosSubcatCombo.cComSubCatDescripcion = this.DatosSubcatCombo.cComSubCatDescripcion.toUpperCase();
    },
    agregarSubcatCombo:function(){
      var ruta = "agregarComSubcategorias";
      var modal = "AgregarSubcatCombo";
      var accion = "agregada";
      this.crearTabla(ruta, this.DatosSubcatCombo, modal,accion);
    },
    editarSubcatCombo:function(){
      var ruta = "editarComSubcategorias";
      var modal = "EditarSubcatCombo";
      var accion = "modificada";
      this.crearTabla(ruta, this.DatosSubcatCombo, modal,accion);
    },
    eliminarSubcatCombo:function(){
      var ruta = "eliminarComSubcategorias";
      var modal = "EliminarSubcatCombo";
      var accion = "eliminada";
      this.crearTabla(ruta, this.DatosSubcatCombo, modal,accion);
    },
    crearTabla:function(ruta, datos, modal, accion){
      var url = "/catalogo/comsubcategorias/"+ruta;
      console.log(url);
       axios.post(url, datos).then(response=>{
          $("#table_subcategoriasCombos").dataTable().fnDestroy();
          this.ObtenerSubcategoriasComb();
          toastr.success('Categoria '+ accion+' correctamente');
          $('#'+modal).modal("hide");
          this.iniciarDatos()
       }).catch(function (error){
          toastr.warning('Error','Ha ocurrido un error al '+accion+' Categoria');
          console.log(error);
      });
    },
    cerrarModal:function(Modal){
      this.iniciarDatos();
      $('#'+Modal).modal("hide");
    },
    ModalEditarComSubcategoria:function(subcategoriascombo){
      this.DatosSubcatCombo=subcategoriascombo;
      $("#EditarSubcatCombo").modal("show");
    },
    ModalEliminarComSubcategoria:function(subcategoriascombo){
      this.DatosSubcatCombo=subcategoriascombo;
      $("#EliminarSubcatCombo").modal("show");
    },
  },
});
