var app = new Vue({
  el: '#configuracionTerminales',
  created: function(){
    this.obtenerDatos();
  },
  data:{
    datosTerminales:{},
    Terminales:[],
  },
  methods:{
    iniciardatosTerminales: function(){
      this.datosTerminales={cTerDescargas:"0",cTerIdTeamViewer:"0",cTerImpBarra:"",cTerImpCocina:"", cTerImpTickets:"", cTerLiquida:0, cTerSerie:""}
    },
    obtenerDatos:function(){
    axios.get('/catalogo/configuracionTerminales/obtenerTerminales').then(response=>{
     this.Terminales = response.data.Terminales;
     setTimeout(function(){$('#table_configterminales').DataTable({
           "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
             "pagingType": "full_numbers",
             "processing": "Procesando...",
             "sLengthMenu": "Mostrar _MENU_ registros encontrados"
           },
         });
     }); }, 3000).catch(function (error){
           toastr.warning('Error','Ha ocurrido un error al obtener las terminales.');
           console.log(error);
  });
  },
  abrirModal:function(Modal, Datos){
    if(Datos===0){
      this.iniciardatosTerminales()
    }
    else{
      this.datosTerminales=Datos;
    }
    $('#'+Modal).modal("show");
  },
  cerrarModal:function(Modal){
    $('#'+Modal).modal("hide");
  },
  editarConfigTerminal:function(){
    var ruta = "editarConfigTerminal"
    var datos = this.datosTerminales
    var modal = "EditarConfigTerminal"
    var accion = "editado"
    this.conexion(ruta, datos, modal, accion)
  },
  agregarConfigTerminal:function(){
    var ruta = "agregarConfigTerminal"
    var datos = this.datosTerminales
    var modal = "AgregarConfigTerminal"
    var accion = "agregado"
    this.conexion(ruta, datos, modal, accion)
  },
  eliminarConfigTerminal:function(){
    var ruta = "eliminarConfigTerminal"
    var datos = this.datosTerminales
    var modal = "EliminarConfigTerminal"
    var accion = "eliminado"
    this.conexion(ruta, datos, modal, accion)
  },
  conexion:function(ruta, datos, modal, accion){
    var url = "/catalogo/configuracionTerminales/"+ruta;
     axios.post(url, datos).then(response=>{
       $("#table_configterminales").dataTable().fnDestroy();
        this.obtenerDatos();
        toastr.success('Terminal '+ accion+' correctamente');
        $('#'+modal).modal("hide");
     }).catch(function (error){
        toastr.warning('Error','Ha ocurrido un error al '+accion+' terminal');
        console.log(error);
    });
  },
  FormatearBMayusculas:function(){
    this.datosTerminales.cTerImpBarra = this.datosTerminales.cTerImpBarra.toUpperCase();
  },
  FormatearTMayusculas:function(){
    this.datosTerminales.cTerImpTickets = this.datosTerminales.cTerImpTickets.toUpperCase();
  },
  FormatearCMayusculas:function(){
    this.datosTerminales.cTerImpCocina = this.datosTerminales.cTerImpCocina.toUpperCase();
  },
  }
});
