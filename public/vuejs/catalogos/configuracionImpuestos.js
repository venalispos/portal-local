var app = new Vue({
  el: '#configuracionImpuestos',
  created: function(){
    this.ObtenerConfigImpuestos();
  },
  data:{
    DatosConfigImpuestos: {}
  },
  methods:{
    iniciarDatosConfigImpuestos: function(){
      this.DatosConfigImpuestos = {'cConImpuesto1Activo':0, 'cConImpuesto1Nombre':"NO", 'cConImpuesto1Cantidad':0.00, 'cConImpuesto1Tipo':2, 'cConImpuesto2Activo':0, 'cConImpuesto2Nombre':"NO", 'cConImpuesto2Cantidad':0.00, 'cConImpuesto2Tipo':2, 'cConImpuesto3Activo':0, 'cConImpuesto3Nombre':"NO", 'cConImpuesto3Cantidad':0.00, 'cConImpuesto3Tipo':2, 'cConImpuesto4Activo':0, 'cConImpuesto4Nombre':"NO", 'cConImpuesto4Cantidad':0.00, 'cConImpuesto4Tipo':2}
    },
    ObtenerConfigImpuestos:function(){
      axios.get('/configuracion/impuestos/obtenerImpuestos').then(response=>{
         this.DatosConfigImpuestos = response.data.Impuestos;
         console.log(this.DatosConfigImpuestos);
       }).catch(function (error){
         toastr.warning('Error','Ha ocurrido un error al obtener los datos');
         console.log(error);
      });
    },
    abrirModal:function(Modal){
      $('#'+Modal).modal("show");
    },
    cerrarModal:function(Modal){
      this.ObtenerConfigImpuestos();
      $('#'+Modal).modal("hide");
    },
    editarConImpuestos:function(){
      var ruta = "editarImpuesto"
      var datos = this.DatosConfigImpuestos
      var modal = "AgregarConImpuestos"
      var accion = "editado"
      this.conexion(ruta, datos, modal, accion)
    },
    agregarConfigImpuestos:function(){
      var ruta = "agregarImpuesto"
      var datos = this.DatosConfigImpuestos
      var modal = "AgregarConImpuestos"
      var accion = "editado"
      this.conexion(ruta, datos, modal, accion)
    },
    conexion:function(ruta, datos, modal, accion){
      var url = "/configuracion/impuestos/"+ruta;
       axios.post(url, datos).then(response=>{
          this.ObtenerConfigImpuestos();
          toastr.success('Configuración '+ accion+' correctamente');
          $('#'+modal).modal("hide");
       }).catch(function (error){
          toastr.warning('Error','Ha ocurrido un error al '+accion+' configuración');
          console.log(error);
      });
    },
    FormatearMayusculas1:function(){
      this.DatosConfigImpuestos.cConImpuesto1Nombre = this.DatosConfigImpuestos.cConImpuesto1Nombre.toUpperCase();
    },
    FormatearMayusculas2:function(){
      this.DatosConfigImpuestos.cConImpuesto2Nombre = this.DatosConfigImpuestos.cConImpuesto2Nombre.toUpperCase();
    },
    FormatearMayusculas3:function(){
      this.DatosConfigImpuestos.cConImpuesto3Nombre = this.DatosConfigImpuestos.cConImpuesto3Nombre.toUpperCase();
    },
    FormatearMayusculas4:function(){
      this.DatosConfigImpuestos.cConImpuesto4Nombre = this.DatosConfigImpuestos.cConImpuesto4Nombre.toUpperCase();
    },
  },
});
