var app = new Vue({
  el: '#Equivalencias',
  created: function(){

    this.getListaProductos();
    this.getListaProdPrimarios();
    this.getListaEquivalencias();
 },
  data: {

    listaProductos: [],
    listaProdPrimarios: [],
    producto: [],
    prodPrimario: { cProPriFolio: 0, cProPriDescripcion: '', cProPriDetEquivalencia: '' },
    equivalencia: { cFolioProPri: 0, cCodigoProd: 0, cEquivalencia: 1 },
    listaEquivalencias: [],
    idEquivalencia: 0,
    numeroRegistros: 10,
    offset: 2,
    pagination: {
      'total': 0,
      'current_page': 1,
      'per_page': 0,
      'last_page': 0,
      'from': 0,
      'to': 0,
    },
 },
 computed: {

     isActived: function() {
         return this.pagination.current_page;
     },

     pagesNumber: function() {
         if(!this.pagination.to){
             return [];
         }

         var from = this.pagination.current_page - this.offset; // Desde quÃ© # pÃ¡gina mostrarÃ¡ botÃ³n
         if ( from < 1 ){
             from = 1;
         }

         var to = from + (this.offset * 2);                    // Hasta quÃ© # pÃ¡gina mostrarÃ¡ botÃ³n
         if( to >= this.pagination.last_page ){
             to = this.pagination.last_page;
         }
         var pagesArray = [];
         while( from <= to){
             pagesArray.push( from );
             from++;
         }
         return pagesArray;
     },
 },
 methods: {

   changePage: function(page) {
     this.pagination.current_page = page;
     this.getListaEquivalencias(page);
   },

   getListaProductos: function() {
     var UrlListaProductos = '/Catalogos/Equivalencias/obtenerProductos';
     axios.get(UrlListaProductos).then(response=> {
       this.listaProductos = response.data;
     });
   },

   getListaProdPrimarios: function() {
     var UrlListaProdPrimarios = '/Catalogos/Equivalencias/obtenerProdPrimarios';
     axios.get(UrlListaProdPrimarios).then(response=> {
       this.listaProdPrimarios = response.data;
     });
   },

   abrirModal: function() {
     $('#AgregarEquivalencia').modal("show");
   },

   cerrarModal: function() {
     $('#AgregarEquivalencia').modal("hide");
     this.limpiarDatos();
   },

   getNombreProdCodigo:function({ cProCodigo, cProDescripcion }) {
     return `[${cProCodigo}] - ${cProDescripcion}`;
   },

   agregarEquivalencias: function() {
     var url = '/Catalogos/Equivalencias/agregarEquivalencias';

     this.equivalencia.cFolioProPri = this.prodPrimario.cProPriFolio;
     this.equivalencia.cCodigoProd = this.producto.cProCodigo;

      axios.post(url, this.equivalencia).then(response=> {
        response.data;
        toastr.success("Se ha registrado correctamente.");
        this.limpiarDatos();
        this.getListaEquivalencias();

      }).catch(function (error) {
         toastr.warning('Error','Ha ocurrido un error ');
         console.log(error);
     });
   },

   getListaEquivalencias: function() {
     var page = this.pagination.current_page;
     this.isLoading = 1;

     // var UrlListaEquivalencias = '/Catalogos/Equivalencias/obtenerEquivalencias';
     // axios.get(UrlListaEquivalencias).then(response=> { this.listaEquivalencias = response.data; });

     var UrlListaEquivalencias = '/Catalogos/Equivalencias/obtenerEquivalencias?page='+page+'&registros='+this.numeroRegistros;
     axios.get(UrlListaEquivalencias).then(response=>{
       this.isLoading = 0;
       this.listaEquivalencias = response.data.ListaEquivalencias.data;
       this.pagination = response.data.paginate;
     });
   },

   limpiarDatos: function() {

     this.idEquivalencia = 0;
     this.equivalencia= { cFolioProPri: 0, cCodigoProd: 0, cEquivalencia: 1 };
     this.prodPrimario = [];
     this.producto = [];
   },

   abrirModalEditar: function(producto) {
     console.log(producto);
     this.idEquivalencia = producto.cEquivaFolio;
     this.equivalencia = { 'cFolioProPri': producto.cFolioProPri, 'cCodigoProd': producto.cCodigoProd, 'cEquivalencia': producto.cEquivalencia };
     this.prodPrimario = { 'cProPriDescripcion': producto.producto_primario.cProPriDescripcion, 'cProPriDetEquivalencia': producto.producto_primario.cProPriDetEquivalencia };
     this.producto = { 'cProCodigo': producto.cCodigoProd, 'cProDescripcion': producto.producto.cProDescripcion };

     $('#EditarEquivalencias').modal("show");
   },

   cerrarModalEditar: function() {
     $('#EditarEquivalencias').modal("hide");
     this.limpiarDatos();
   },

   editarEquivalencias: function() {
     var url = "/Catalogos/Equivalencias/editarEquivalencias";
     this.equivalencia.cCodigoProd = this.producto.cProCodigo;
     this.equivalencia.cFolioProPri = this.prodPrimario.cProPriFolio;

     var datos = { 'data': this.equivalencia, 'id': this.idEquivalencia, }

     axios.post(url, datos).then(response=> {
        if (response.data === 0) { this.cerrarModalEditar(); }
        toastr.success("Se ha registrado correctamente.");
        this.getListaEquivalencias();
        this.cerrarModalEditar();

      }).catch(function (error) {
         toastr.warning('Error','Ha ocurrido un error ');
         console.log(error);
     });
   },

   abrirModalEliminar: function(producto) {
     this.idEquivalencia = producto.cEquivaFolio;
     this.prodDescripcion = producto.producto.cProDescripcion;
     this.prodPrimario = { 'cProPriDescripcion': producto.producto_primario.cProPriDescripcion };
     this.producto = { 'cProDescripcion': producto.producto.cProDescripcion };

     $('#EliminarEquivalencias').modal("show");
   },

   cerrarModalEliminar: function() {
     $('#EliminarEquivalencias').modal("hide");
     this.limpiarDatos();
   },

   eliminarEquivalencias: function() {
     var url = "/Catalogos/Equivalencias/eliminarEquivalencias";
     var datos = { 'id': this.idEquivalencia, }

     axios.post(url, datos).then(response=> {
        toastr.success("Se ha eliminado correctamente.");
        this.getListaEquivalencias();
        this.cerrarModalEliminar();

      }).catch(function (error) {
         toastr.warning('Error','Ha ocurrido un error ');
         console.log(error);
     });
   },

   descargarExcel:function(){
     data_reporte = {
     };
     axios({
       method:'post',
       url:'/catalogo/productos/generarExcel',
       responseType:'blob'
       ,data:data_reporte
     }).then(response=>{
       let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
       let link=document.createElement('a')
       link.href=window.URL.createObjectURL(blob)
       link.download='Equi.csv'
       link.click()
     });
   },
},
components: {

  Multiselect: window.VueMultiselect.default
}

});
