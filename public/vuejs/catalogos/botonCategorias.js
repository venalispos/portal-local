var app = new Vue({
  el: '#botonesCategorias',
  created: function(){
    this.obtenerDatos()
  },
  data:{
    BotonCategorias:[],
    datosBotonCategorias:{}
  },
  methods:{
    iniciardatosBotonCategorias: function(){
      this.datosBotonCategorias={cBotCatDescripcion:""}
    },
    obtenerDatos:function(){
    axios.get('/catalogo/botonCategorias/obtenerBotCategorias').then(response=>{
     this.BotonCategorias = response.data.BotCategorias;
     setTimeout(function(){$('#table_botoncategorias').DataTable({
           "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json" ,
             "pagingType": "full_numbers",
             "processing": "Procesando...",
             "sLengthMenu": "Mostrar _MENU_ registros encontrados"
           },
         });
     }); }, 3000).catch(function (error){
           toastr.warning('Error','Ha ocurrido un error al obtener los datos.');
           console.log(error);
  });
  },
  abrirModal:function(Modal, Datos){
    if(Datos===0){
      this.iniciardatosBotonCategorias()
    }
    else{
      this.datosBotonCategorias=Datos;
    }
    $('#'+Modal).modal("show");
  },
  cerrarModal:function(Modal){
    $('#'+Modal).modal("hide");
  },
  editarBotonCategoria:function(){
    var ruta = "editarBotonCategorias"
    var datos = this.datosBotonCategorias
    var modal = "EditarBotonCategoria"
    var accion = "editada"
    this.conexion(ruta, datos, modal, accion)
  },
  agregarBotonCategoria:function(){
    var ruta = "agregarBotonCategorias"
    var datos = this.datosBotonCategorias
    var modal = "AgregarBotonCategoria"
    var accion = "agregada"
    this.conexion(ruta, datos, modal, accion)
  },
  eliminarBotonCategorias:function(){
    var ruta = "eliminarBotonCategorias"
    var datos = this.datosBotonCategorias
    var modal = "EliminarBotonCategorias"
    var accion = "eliminada"
    this.conexion(ruta, datos, modal, accion)
  },
  conexion:function(ruta, datos, modal, accion){
    var url = "/catalogo/botonCategorias/"+ruta;
     axios.post(url, datos).then(response=>{
       $("#table_botoncategorias").dataTable().fnDestroy();
        this.obtenerDatos();
        toastr.success('Categoria '+ accion+' correctamente');
        $('#'+modal).modal("hide");
     }).catch(function (error){
        toastr.warning('Error','Ha ocurrido un error al '+accion+' categoria');
        console.log(error);
    });
  },
  FormatearMayusculas:function(){
    this.datosBotonCategorias.cBotCatDescripcion = this.datosBotonCategorias.cBotCatDescripcion.toUpperCase();
  },
  },
});
