var app = new Vue({
  el: '#correos',
  created: function(){
    this.FunobtenerCorreos();

  },
  data:{
    dataCorreos:null,
    nuevoCorreo:{
      'correo':null,
      'estatus':1
    },
    correoSelected:null
  },
  methods:{


    FunobtenerCorreos:function(){
      route='/correos/getCorreos';
      axios.get(route).then(response=>{
        this.dataCorreos= response.data.correos;
      });
    },

    FunModalNuevoCorreo:function(){
    $('#mNuevoCorreo').modal("show");
  },
  FunAgregarNuevoCorreoCerrarModal:function(){
    route='/correos/NuevoCorreo';
    var data ={
      'data':this.nuevoCorreo
    }
    axios.post(route,data).then(response=>{
      this.FunobtenerCorreos();
  $('#mNuevoCorreo').modal("hide");
  toastr.success("CORREO AGREGADO CORRECTAMENTE");
    });
  },

  FunAbrirModalActualizar:function(value){
      this.correoSelected=value;
  $('#mActualizarCorreo').modal("show");
  },

FunModificarCorreos:function(){
  route='/correos/ModificarCorreos';
  data={
    'data':this.correoSelected
  }
  axios.post(route,data).then(response=>{
    this.FunobtenerCorreos();
  $('#mActualizarCorreo').modal("hide");
  toastr.success("CORREO MODIFICADO CORRECTAMENTE");
  });
},

FunModalEliminarCorreo:function(value){
      this.correoSelected=value;
  $('#mEliminarCorreo').modal("show");
},
FunEliminarCorreo:function(){
  route='/correos/EliminarCorreo';
  data={
    'data':this.correoSelected
  }
  axios.post(route,data).then(response=>{
    this.FunobtenerCorreos();
  $('#mEliminarCorreo').modal("hide");
  toastr.success("CORREO ELIMINADO CORRECTAMENTE");
  });
}

  },
});
