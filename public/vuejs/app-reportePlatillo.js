var app = new Vue({
    el: '#reportePlatillos',
    created: function() {
    },
  //las variables que comienzan con "r" son para las rutas, si seguido tienen una "g" son para get y 'p' para Post.
  //varibales que comienzan con "a" son arreglos
  data: {
    fechaHasta:null,
    fechaDesde:null,
    mostrarbotonDescarga:false,
    isLoading:false,
      reporteArticulos:null,
      totalesArticulos:null

  },
  methods: {
      obtenerventaporArticulos:function(){
          var oRArticulos="/reportes/obtenerArticulos";
          var fecArr={
              'inicio':this.fechaDesde,
              'termino':this.fechaHasta
          }
          this.isLoading=true;
          axios.post(oRArticulos,fecArr).then(response=>{
              if (response.data == 0) {
                  toastr.warning("Has tratado de ingresar una fecha no valida");
                  this.isLoading = 0;
              } else {
              this.totalesArticulos=response.data.totales;
              this.reporteArticulos=response.data.Articulos;
              this.isLoading=false;
              if (this.totalesArticulos == null) {
                  this.mostrarbotonDescarga = false;
              }else{
                  this.mostrarbotonDescarga = true;
              }
          }
          });
      },
      formatearMoneda: function(value){
          var formatear = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
        });
          return formatear.format(value);
      },

      formateardosDecimales: function(values){
          var cant=values;
          cant=parseFloat(cant);
          var cantidad= cant.toFixed(2);
          return cantidad;
      },
      calcularProcentaje:function(value){
          var cantidadArt=parseFloat(value);
          var porcentajeCantidad=(cantidadArt/parseFloat(this.totalesArticulos[0].precio))*100;
          var porciento=this.formateardosDecimales(porcentajeCantidad);
          return porciento;
      },
      generarReportePDF:function(){

       var hoy = new Date();
       var fecha = hoy.getDate() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getFullYear();
       var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
       var fechaYHora = fecha + ' ' + hora;
       var oRArticulos="/reportes/obtenerArticulosPDF";
       var fecArr={
          'inicio':this.fechaDesde,
          'termino':this.fechaHasta
      }

      axios({
          method:'post',
          url:oRArticulos,
          responseType:'blob'
          ,data:fecArr
      }).then(response=>{
          let blob= new Blob([response.data],{type: 'application/pdf'});
          let link=document.createElement('a')
          link.href=window.URL.createObjectURL(blob)
          link.download='ReporteArticulos'+fechaYHora+'.pdf'
          link.click()
      });
  },

  generarReporteExcel:function(){
      oRArticulosEXCEL='/reportes/obtenerArticulosEXCEL'
      var hoy = new Date();
      var fecha = hoy.getDate() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getFullYear();
      var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
      var fechaYHora = fecha + ' ' + hora;
      var oRArticulos="/reportes/obtenerArticulosPDF";
      var fecArr={
          'inicio':this.fechaDesde,
          'termino':this.fechaHasta
      }

         axios({
          method:'post',
          url:oRArticulosEXCEL,
          responseType:'blob'
          ,data:fecArr
      }).then(response=>{
          let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
          let link=document.createElement('a')
          link.href=window.URL.createObjectURL(blob)
             link.download='ReporteArticulos'+fechaYHora+'.csv'
          link.click()
      });

  }
}

});

//
