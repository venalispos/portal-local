var app = new Vue({
 el: '#resumen',
 created: function() {

 },
   data: {
     fechaHasta:null,
     fechaDesde:null,
     mostrarbotonDescarga:false,
     isLoading:false,
     reporteResumen:null,
     reporteFormasPago:null,
     sinRegistros:false,
     Subtotal:null,
     Iva:null,
     Cancelaciones:null,
     Descuentos:null,
     Cortesias:null,
     Monto:null,
     totalResumen:null,
     totalTotales:null,
     mostrarbotonDescarga:false
   },
   methods: {
     generarReporteResumen:function(){
       rpResumen="/reportes/generarResumenrespuesta";
       var aFechas={
         'inicio':this.fechaDesde,
         'termino':this.fechaHasta
       }
       this.isLoading=true;
       this.sinRegistros=false;
       this.mostrarbotonDescarga=false;
       axios.post(rpResumen,aFechas).then(response=>{
         this.reporteResumen=response.data.HistorialCategorias,
         this.reporteFormasPago=response.data.HistorialFormasPagos,
         this.Subtotal=response.data.Subtotal,
         this.Iva=response.data.Iva,
         this.Cancelaciones=response.data.Cancelaciones,
         this.Descuentos=response.data.Descuentos,
         this.Cortesias=response.data.Cortesias,
         this.Monto=response.data.Monto,
         this.totalResumen=response.data.totalResumen,
         this.totalTotales=response.data.totales
         this.isLoading=false;
         if (this.reporteResumen==null) {
           this.sinRegistros=true;
         }
         this.mostrarbotonDescarga=true;
       });
     },
     formatearMoneda: function(value){
       var formatear = new Intl.NumberFormat('en-US', {
         style: 'currency',
         currency: 'USD',
         minimumFractionDigits: 2,
       });
       return formatear.format(value);
     },
     descargarPDFresumen:function(){

       var rutaPDfGenerales='/reportes/generarResumenPDF';
       data_reporte = {
         'inicio' : this.fechaDesde,
         'termino' : this.fechaHasta
       };

       axios({
         method:'post',
         url:rutaPDfGenerales,
         responseType:'blob'
         ,data:data_reporte
       }).then(response=>{
         let blob= new Blob([response.data],{type: 'application/pdf'});
         let link=document.createElement('a')
         link.href=window.URL.createObjectURL(blob)
         link.download='Venta por categorias Resumen.pdf'
         link.click()
       });

     },
     GenerarExcelResumen:function(){
       data_reporte = {
         'inicio' : this.fechaDesde,
         'termino' : this.fechaHasta
       };
       axios({
         method:'post',
         url:'/reportes/generarResumenExcel',
         responseType:'blob'
         ,data:data_reporte
       }).then(response=>{
         let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
         let link=document.createElement('a')
         link.href=window.URL.createObjectURL(blob)
         link.download='Ventas por categorias Resumen.csv'
         link.click()
       });
     }

   }
});

//
