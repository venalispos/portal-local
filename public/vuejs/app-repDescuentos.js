 var app = new Vue({
 	el: '#reporteDescuento',
 	created: function() {
 	},
    //las variables que comienzan con "r" son para las rutas, si seguido tienen una "o" son para obtener informacion.
    data: {
    	fechaDesde:null,
    	fechaHasta:null,
    	isloading:null,
    	array_Descuento:null
    },
    computed:
    {


    }, 
    methods : {
    	repDescuentos:function(){
    		this.true=null;
    		var roDescuentos="/reportes/descuentos/generar";
    		var fecArr={
    			'inicio':this.fechaDesde,
    			'termino' : this.fechaHasta
    		}
    		axios.post(roDescuentos,fecArr).then(response=>{
    			this.array_Descuento=response.data.productos;
    			this.isloading=null;
    		});
    	}
    }
}); 