const ClientTable = VueTables.ClientTable
const Event = VueTables.Event // import eventbus

console.log(VueTables);
Vue.use(ClientTable)

var app = new Vue({
    el: '#reporteporticket',
    created: function() {
    },
    data: {
        columnas: ['tHisFechaRep','tHisFolioTicket', 'tHisDesMesa','Factura', 'Subtotal', 'IVA', 'Total'],
        options: {
            pagination: false,
            headings: {
                tHisFechaRep: 'Fecha',
                tHisFolioTicket: 'Ticket',
                tHisDesMesa: 'Mesa'
            },
            perPage: 20000,
            sortable: []
        },

        fechaDesde : null,
        fechaHasta : null,
        tienda_selected: 0,
        lista_sucursales: null,
        reporte_resultados: null,
        totalTotales: 0,
        selected_filtro: 'Ticket',
        mostrarbotonDescarga:null,
        errors: null,
        isLoading: 0,
        reporteproductos:null,
        totalproductos:null,
        categorias_productos:null,
        totalesCategorias:null,
        totalFinal:null,
        subtotalticket:null,
        cajas:null,
        totalImpuesto:null,
        botonPresionadoTicket:0
    },
    mounted(){

        window.addEventListener("keyup", function(e) {

            if (e.keyCode== 16) {
               $('#Buscarcmezcla').hide();
           }
       });

        window.addEventListener("keydown", function(e) {
            if (e.keyCode== 16) {
                $('#Buscarcmezcla').show();
            }
        });
    },
    methods: {
        statusReporte:function(value){
            this.botonPresionadoTicket=value;
        },
        generarReporte: function(filtro){
            if(filtro){
            } else {
                this.selected_filtro = 'Ticket';
            }
            this.isLoading = 1;
            var generarReporteURL = '/reportes/ticketGenerar';
            data_reporte = {
                'fechaDesde' : this.fechaDesde,
                'fechaHasta' : this.fechaHasta,
                'filtro' : filtro,
                'reporte':this.botonPresionadoTicket
            };
            axios.post(generarReporteURL, data_reporte).then(response => {
                if (response.data == 0) {
                    toastr.warning("Has tratado de ingresar una fecha no valida");
                    this.isLoading = 0;
                } else {
                this.reporte_resultados = response.data.reporte;
                this.totalTotales = response.data.totalTotales;
                this.subtotalticket = response.data.subtotal;
                this.totalImpuesto = response.data.impuesto;
                this.isLoading = 0;
                if (this.totalTotales!=0) {
                    this.mostrarbotonDescarga=1;
                }
            }
            });
        },
        formatearMoneda: function(value){
            var formatear = new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
              minimumFractionDigits: 2,
          });
            return formatear.format(value);
        },
        formatearHora: function(fecha){
          var Hora = fecha.toString().substr(11,2);
          var minutos = fecha.toString().substr(14,2);

          return Hora+':'+minutos;
        },
        formatearFecha: function(fecha){
          var year = fecha.toString().substr(0,4);
          var month = fecha.toString().substr(4,2);
          var day = fecha.toString().substr(6,2);

          return year+'/'+month+'/'+day;
        },
        formatearCierreH: function(value){
            var datatime = value;
            return datatime.substring(11);
        },
        formatearCierreF: function(value){
            var datatime = value;
            return datatime.substring(0,10);
        },
        aplicarFiltro: function(value) {
            this.selected_filtro = value;
            this.generarReporte(value);
        },
        descargarPDF : function(){
            var rutaPDfGenerales='/reportes/generarPDFIticket';
            data_reporte = {
                'fechaDesde' : this.fechaDesde,
                'fechaHasta' : this.fechaHasta,
                'reporte':this.botonPresionadoTicket
            };

            axios({
                method:'post',
                url:rutaPDfGenerales,
                responseType:'blob'
                ,data:data_reporte
            }).then(response=>{
                let blob= new Blob([response.data],{type: 'application/pdf'});
                let link=document.createElement('a')
                link.href=window.URL.createObjectURL(blob)
                link.download='Reporte de ventas Ticket.pdf'
                link.click()
            });
        },

        generarReporteproductos: function(){
            var routePDF='/reportes/productosGenerar';
            this.isLoading = 1;
            data_reporte = {
                'fechaDesde' : this.fechaDesde,
                'fechaHasta' : this.fechaHasta
            };
            axios.post(routePDF,data_reporte).then(response=>{
                if (response.data == 0) {
                    toastr.warning("Has tratado de ingresar una fecha no valida");
                    this.isLoading = 0;
                } else {
                this.reporteproductos=response.data.productos;
                this.categorias_productos=response.data.categorias;
                this.totalesCategorias=response.data.totalesCategorias;
                this.totalFinal=response.data.final;
                this.cajas = response.data.ListaCajas;
                this.isLoading = 0;
                if (this.reporteproductos!= "[]") {
                    this.mostrarbotonDescarga=1;
                }
            }
            });
            },

            descargarPDFproducto : function(){
                var rutaPDfGenerales='/reportes/generarPDFProd';
                data_reporte = {
                    'fechaDesde' : this.fechaDesde,
                    'fechaHasta' : this.fechaHasta
                };

                axios({
                    method:'post',
                    url:rutaPDfGenerales,
                    responseType:'blob'
                    ,data:data_reporte
                }).then(response=>{
                    let blob= new Blob([response.data],{type: 'application/pdf'});
                    let link=document.createElement('a')
                    link.href=window.URL.createObjectURL(blob)
                    link.download='Reporte de ventas Productos.pdf'
                    link.click()
                });
            },

            formateardosDecimales: function(values){
                var cant=values;
                cant=parseFloat(cant);
                var cantidad= cant.toFixed(2);
                return cantidad;
            },

            GenerarExcelproductos:function()
            {

              data_reporte = {
                'fechaDesde' : this.fechaDesde,
                'fechaHasta' : this.fechaHasta
            };
            axios({
                method:'post',
                url:'/reportes/excelProductos',
                responseType:'blob'
                ,data:data_reporte
            }).then(response=>{
                let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                let link=document.createElement('a')
                link.href=window.URL.createObjectURL(blob)
                link.download='ReporteVentasProductos.csv'
                link.click()
            });

        },

        GenerarExcelTicket:function()
        {

          data_reporte = {
            'fechaDesde' : this.fechaDesde,
            'fechaHasta' : this.fechaHasta,
                'reporte':this.botonPresionadoTicket
        };
        axios({
            method:'post',
            url:'/reportes/excelTicket',
            responseType:'blob'
            ,data:data_reporte
        }).then(response=>{
            let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            let link=document.createElement('a')
            link.href=window.URL.createObjectURL(blob)
            link.download='ReporteVentasTicket.csv'
            link.click()
        });

    }
}
});
