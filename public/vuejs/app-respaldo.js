 var app = new Vue({
 	el: '#respaldosBasededatos',
 	created: function() {

 	},
    //las variables que comienzan con "r" son para las rutas, si seguido tienen una "g" son para get y 'p' para Post.
    //varibales que comienzan con "a" son arreglos 
    data: {
        loading:null,
        respuesta:null
    },
    methods: {
    	generarRespaldo:function (){
            this.loading=true;
            axios.get('/respaldo/generar').then(response=>{ 
                this.loading=false;
                toastr.success("Respaldo Generado");
            });
        }

    }
});

 //