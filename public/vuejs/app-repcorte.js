var app = new Vue({
    el: '#reporteporticket',
    created: function() {
    },
    data: {
        cargapdf: 0,
        cargaexcel: 0,
        isLoading: 0,
        fechaDesde : '2021-04-15',
        fechaHasta : '2021-04-15',
        reporte: null,
    },
    mounted(){
    },
    methods: {

        generarReporte: function(){
            this.isLoading = 1;
            url = '/reportes/corte/generar'
            data = {
                'fechaDesde': this.fechaDesde,
                'fechaHasta': this.fechaHasta
            }
            axios.post(url, data).then((response) => {
                if (response.data == 0) {
                    toastr.warning("Has tratado de ingresar una fecha no valida");
                    this.isLoading = 0;
                } else {
                this.reporte = response.data;
                this.isLoading = 0;
            }
            }).catch(function (error) {
                toastr.warning("Error", "Ha ocurrido un error ");
                console.log(error);
            });
        },
        descargarPDFCorte:function(){
            this.cargapdf = 1;
            data = {
                'fechaDesde': this.fechaDesde,
                'fechaHasta': this.fechaHasta
            }
    		var rutaPDfGenerales='/reportes/corte/generar/pdf';

    		axios({
    			method:'post',
    			url:rutaPDfGenerales,
    			responseType:'blob'
    			,data:data
    		}).then(response=>{
                this.cargapdf = 0;
    			let blob= new Blob([response.data],{type: 'application/pdf'});
    			let link=document.createElement('a')
    			link.href=window.URL.createObjectURL(blob)
    			link.download='Reporte Corte.pdf'
    			link.click()
    		});

    	},
        GenerarExcelCorte:function(){
            this.cargaexcel = 1;
    		data_reporte = {
                'fechaDesde': this.fechaDesde,
                'fechaHasta': this.fechaHasta
    		};
    		axios({
    			method:'post',
    			url:'/reportes/corte/generar/excel',
    			responseType:'blob'
    			,data:data_reporte
    		}).then(response=>{
                this.cargaexcel = 0;
    			let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
    			let link=document.createElement('a')
    			link.href=window.URL.createObjectURL(blob)
    			link.download='ReporteExcel.csv'
    			link.click()
    		});
    	},
        formatPrice: function (value) {
            var val = (value / 1).toFixed(2).replace(",", ".");
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }


    }

});
