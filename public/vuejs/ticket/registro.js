var app = new Vue({
   el: '#registro',
   created: function() {
   },
   data: {
     Buscar:{Ticket:null},
     Mesa:null,
     Productos:null,
     Estatus:null,
     Escaneos:null,
     Cargando:0,
},
computed:
{
   },
   methods: {
     buscarTicket:function(){
       this.Cargando=1;
       var ruta='/ticket/registro/buscar';
       axios.post(ruta,this.Buscar).then(response=>{
         this.Mesa=response.data.Mesa;
         this.Estatus=response.data.Estatus;
         this.Escaneos=response.data.Escaneos;
         this.Productos=response.data.Productos;
         this.Cargando=0;
       }).catch(function (error) {
           toastr.warning("Error", "Ha ocurrido un error ");
           console.log(error);
           this.Cargando=0;
       });
       this.Buscar={Ticket:null};
     },
     formatoPrecio: function(value) {
            var val = (value/1).toFixed(2).replace(',', '.');
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        },

   }
});
