var app = new Vue({
	el: '#IdInventarioInicial',
	created: function() {
		this.verificarStatusInventario();
		this.obtenerProductosfilttrados();
		this.obtenerProductos();
		this.obtenerreporteProductos();
	},
	data: {
		dataproductos:null,
		dataCatagorias:null,
		dataSubcategorias:null,
		dataProductosCambiados:[],
		Categoriaseleccionada:0,
		subcategoriaseleccionada:0,
		deshabilitarBotonAgregar:0,
		statusInventario:0,
		reporteInventario:null,
		productos:null,
		historial:null
	},
	computed:{

		disableButton:function(){
			if (this.deshabilitarBotonAgregar == 1) {
				return true;
			}else{
				return false;
			}
		}
	},
	methods: {
		verificarStatusInventario:function(){
			routestatus='/Inventario/verificarStatus';
			axios.get(routestatus).then(response=>{
				this.statusInventario=response.data
			});
		},

		obtenerProductos:function(){
			console.log("obtenerProductos");
			var routeProductos="/catalogo/getProductos";
			axios.get(routeProductos).then(response=>{
				this.dataCatagorias=response.data.categorias;
				this.dataSubcategorias=response.data.subcategorias;
			}).catch(function (error) {
          toastr.warning("Error", "Ha ocurrido un error ");
          console.log(error);
      });
		},
		cambiaropcionasubcategoria:function(){
			this.subcategoriaseleccionada=0;
		},

		buscarproductoconFiltros:function(){
			var routeproductoFiltros="/producto/productobycategoriasubcategoria";
			var data={
				'categoria':this.Categoriaseleccionada,
				'subcategoria':this.subcategoriaseleccionada
			}
			axios.post(routeproductoFiltros,data).then(response=>{
				this.verificarStatusInventario();
				this.dataproductos=response.data.productos;
				this.dataProductosCambiados=response.data.productosinventario;
			}).catch(function (error) {
          toastr.warning("Error", "Ha ocurrido un error ");
          console.log(error);
      });
		},
		agregarItemtoinventario:function(dataF){
			this.deshabilitarBotonAgregar=1;
			var dataProducto=dataF;
			var data={
				'producto': dataProducto
			}
			var routeaddItem="/Inventario/agregarproductosInventario";
			axios.post(routeaddItem,data).then(response=>{
				toastr.success("Producto agregado al inventario de esté día correctamente");
				this.verificarStatusInventario();
				this.obtenerProductos();
				this.buscarproductoconFiltros();
				this.deshabilitarBotonAgregar=0;

			}).catch(function (error) {
          toastr.warning("Error", "Ha ocurrido un error ");
          console.log(error);
      });
		},
		obtenerProductosfilttrados:function(){
			var routeproductosFilttrados="/producto/filtradoporinventario";
			axios.get(routeproductosFilttrados).then(response=>{
				this.dataproductos=response.data.productos;
				this.dataProductosCambiados=response.data.productosInventario;

			}).catch(function (error) {
          toastr.warning("Error", "Ha ocurrido un error ");
          console.log(error);
      });
		},
		eliminarproductodeinventario:function(dataDelete){
			var data={
				'producto': dataDelete
			}
			var routedeleteproducto="/Inventario/eliminarproductodeInventario";
			axios.post(routedeleteproducto,data).then(response=>{
				toastr.success("Producto eliminado del inventario de esté día");
				this.buscarproductoconFiltros();

			}).catch(function (error) {
          toastr.warning("Error", "Ha ocurrido un error ");
          console.log(error);
      });
		},
		modalconfirmacionfinalizarinventario:function(){
			$("#finalizarinventario").modal("show");
		},
		terminarInventarioDiario:function(){
			$("#finalizarinventario").modal("hide");
			axios.get("/Inventario/finalizarInventarioDiario").then(response=>{
				toastr.success("Gracias por llenar el inventario");
				this.verificarStatusInventario();
			}).catch(function (error) {
          toastr.warning("Error", "Ha ocurrido un error ");
          console.log(error);
      });
		},



		/// Aquí comienza la fase del reporteo

		obtenerreporteProductos:function(){
			var routeReporte='/Inventario/obtenerreporteproductoinventario';
			axios.get(routeReporte).then(response=>{
					this.reporteInventario=response.data;

			}).catch(function (error) {
          toastr.warning("Error", "Ha ocurrido un error ");
          console.log(error);
      });
		},
		descargarPDFInventario:function(){

		var hoy = new Date();
         var fecha = hoy.getDate() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getFullYear();
         var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
         var fechaYHora = fecha + ' ' + hora;
         var RUTApdf="/Inventario/obtenerreporteproductoinventarioPDF";

        axios({
            method:'post',
            url:RUTApdf,
            responseType:'blob'
            ,data:null
        }).then(response=>{
            let blob= new Blob([response.data],{type: 'application/pdf'});
            let link=document.createElement('a')
            link.href=window.URL.createObjectURL(blob)
            link.download='inventario Diario'+fecha+'.pdf'
            link.click()
        });
		},
		descargarInventarioEXCEL:function(){
			 var hoy = new Date();
        var fecha = hoy.getDate() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getFullYear();
        var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
        var fechaYHora = fecha + ' ' + hora;
        var oRArticulos="/Inventario/obtenerreporteproductoinventarioEXCEL";


           axios({
            method:'post',
            url:oRArticulos,
            responseType:'blob'
            ,data:null
        }).then(response=>{
            let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            let link=document.createElement('a')
            link.href=window.URL.createObjectURL(blob)
               link.download='inventario Diario'+fecha+'.csv'
            link.click()
        });
		}

	}
});
