var app = new Vue({
    el: '#catalogoempleados',
    created: function() {
        this.buscarEmpleado();
    },
    data: {
     usuarios:null,
     status:null,
     isLoading:true,
     usuarioSeleccionado:null,
     nuevoEmpleadoSel:{
       cUsuCodigo:null,
       cUsuNombre:null,
       cUsuApellido:null,
       cUsuActivo:1,
       cUsuClave:null,
       cUsuPuesto1Activo:1,
       cUsuPuesto1Desc:null,
       cUsuPuesto1Perfil:null,
       cUsuPuesto2Activo:0,
       cUsuPuesto2Desc:"",
       cUsuPuesto2Perfil:0,
       cUsuPuesto3Activo :0,
       cUsuPuesto3Desc:"",
       cUsuCuentaBancaria:"",
       cUsuNumRuta:"",
       cUsuPuesto3Perfil:0,
       cUsuFPData:null,
       cUsuNumSeguro:null,
       cUsuDireccion :null,
       cUsuTelefono:null,
       cUsuFechNac:null,
       cUsuAccesoAdmin:1,
       cUsuAdminPass:null,
       remember_token:null,
       cUsuPermisosPortal:null,
       tUsuReiniciar:0,
       cUsuAutoCovid:0
   },
   perfiles:null,
   perfilSeleccionado:null
},
methods: {

        //modales
        mEditarUsuario:function(value){
            this.usuarioSeleccionado=value;
            this.usuarioSeleccionado.cUsuFechNac=this.cambiarFormatoFecha(this.usuarioSeleccionado.cUsuFechNac)
            $('#mEditarUsuario').modal('show');
        },

        mNuevoEMpleado:function(){
            $('#mNuevoUsuario').modal('show');
        },
         mNuevoEMpleadoclose:function(){
            $('#mNuevoUsuario').modal('hide');
            this.buscarEmpleado();
        },

        mEditEmployeeclose:function(){
            $('#mEditarUsuario').modal('hide');
        },
        //funciones
        buscarEmpleado:function(){
          var rGetEMpleado="/catalogo/obtenerusuarios";
          axios.get(rGetEMpleado).then(response=>{
            this.usuarios=response.data.usuarios;
            this.status=response.data.activo;
            this.perfiles=response.data.perfiles;
            this.isLoading=false;
            });


			},
      actualizarUsuario:function(){
        var routeUpdateEmployee = "/catalogo/updateEmployee";
        axios.post(routeUpdateEmployee,this.usuarioSeleccionado).then(response=>{
          this.mEditEmployeeclose();
          toastr.success("Empleado actualizado");
        });
      },
      agregarNuevoEmpleado:function(){
        var shareRoute="/catalogo/shareEmploye";
        axios.post(shareRoute,this.nuevoEmpleadoSel).then(response=>{
        toastr.success("Empleado guardado exitosamente");
		     this.buscarEmpleado();
         this.mNuevoEMpleadoclose();
        this.reiniciarNuevoEmpleado();
        });

      },
      eliminarEmpleadoModal:function(InformacionUsuario){
        this.usuarioSeleccionado=InformacionUsuario;
        $('#Idempleadoeliminarmodal').modal('show');
      },

      eliminarEmpleados:function(){
        var routeDelete="/catalogo/eliminarEmpleado";
        var data={
          'usuario':this.usuarioSeleccionado
        }
        axios.post(routeDelete,data).then(response=>{
           this.buscarEmpleado();
          toastr.success("Empleado eliminado correctamente");
            $('#Idempleadoeliminarmodal').modal('hide');
        });
      },

      cambiarFormatoFecha:function(value){
        var fecha = moment(value).format("YYYY-MM-DD");
        return fecha;
        },

     reiniciarNuevoEmpleado:function(){
      this.nuevoEmpleadoSel.cUsuCodigo=null,
       this.nuevoEmpleadoSel.cUsuNombre=null,
       this.nuevoEmpleadoSel.cUsuApellido=null,
       this.nuevoEmpleadoSel.cUsuActivo=1,
       this.nuevoEmpleadoSel.cUsuClave=null,
       this.nuevoEmpleadoSel.cUsuPuesto1Activo=1,
       this.nuevoEmpleadoSel.cUsuPuesto1Desc=null,
       this.nuevoEmpleadoSel.cUsuPuesto1Perfil=null,
       this.nuevoEmpleadoSel.cUsuPuesto2Activo=null,
       this.nuevoEmpleadoSel.cUsuPuesto2Desc=null,
       this.nuevoEmpleadoSel.cUsuPuesto2Perfil=null,
       this.nuevoEmpleadoSel.cUsuPuesto3Activo =null,
       this.nuevoEmpleadoSel.cUsuPuesto3Desc=null,
       this.nuevoEmpleadoSel.cUsuPuesto3Perfil=null,
       this.nuevoEmpleadoSel.cUsuFPData=null,
       this.nuevoEmpleadoSel.cUsuNumSeguro=null,
       this.nuevoEmpleadoSel.cUsuDireccion =null,
       this.nuevoEmpleadoSel.cUsuTelefono=null,
       this.nuevoEmpleadoSel.cUsuFechNac=null,
       this.nuevoEmpleadoSel.cUsuAccesoAdmin=1,
       this.nuevoEmpleadoSel.cUsuAdminPass=null,
       this.nuevoEmpleadoSel.remember_token=null,
      this.nuevoEmpleadoSel.cUsuPermisosPortal=null,
       this.nuevoEmpleadoSel.tUsuReiniciar=0,
       this.nuevoEmpleadoSel.cUsuAutoCovid=0,
       this.nuevoEmpleadoSel.cUsuCuentaBancaria="",
         this.nuevoEmpleadoSel.cUsuNumRuta=""
     }

}
});
