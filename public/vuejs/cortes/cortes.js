var app = new Vue({
  el: '#cortesPrincipal',
  created: function(){
    this.obtenerConfiguracion();
  // this.FunObtenerInformacionCortes();
  // this.FunObtenerItemFormaPago();
  },
  data:{
    configuracion: null,
    fecha:null,
  dataInformacionCortes:null,
  dataMesasAbiertas:null,
  dataFechaAceptada:null,
  dataInformacionItem:null,
  formaPagoSelected:null,
  dataGastos:null,
  dataDevoluciones:null,
  gastoSelected:null,
  devolucionSelected:null,
  gastoAgregar:{
    descripcion:null,
    gasto:null
  },
  devolucionAgregar:{
    descripcion:null,
    gasto:null
  },
  corteRealizadoConsulta: null,
  guardarCorte:false,
  finalizarCorte:false,
  inicio:true,
  errors: [],
  dataTotalGasto:null,
  dataTotaDevoluciones:null,
  dataTotalFormasPago:null,
  dataTotalIngresado:null,
  dataCorteFinalizado:null,
  num_papeleta:null,
  num_bolsa:null
  },
  methods:{
    obtenerConfiguracion: function(){
      var url = "/configuracion/obtener";
      axios.get(url).then(response => {
          this.configuracion = response.data;
      }).catch(function (error) {
          toastr.warning("Error", "Ha ocurrido un error ");
          console.log(error);
      });
    },

    obtenerEstatusCorte: function(){
      var url = "/cortes/cortesPrincipal/verEstatusCorte";
      this.inicio=false;
      var data={
        'fecha':this.fecha
      }
      axios.post(url,data).then(response=>{
        this.guardarCorte = response.data.corteGuardado.cCorDetFinalizado;
        this.finalizarCorte = response.data.cosrteFinalizado.cCorDetFinalizado;
      }).catch(function (error) {
          toastr.warning("Error", "Ha ocurrido un error ");
          console.log(error);
      });
    },

    FunObtenerInformacionCortes:function(){
      var route='/cortes/cortesPrincipal/obtenerCortes';
      var data={
        'fecha':this.fecha
      }
      this.guardarCorte=true;
      this.finalizarCorte=false;
      axios.post(route,data).then(response=>{
        this.obtenerEstatusCorte();
        this.dataInformacionCortes=response.data.formaspago;
        this.dataGastos=response.data.gastos;
        this.dataTotalGasto=response.data.totalGasto;

        this.corteRealizadoConsulta=response.data.totalIngresado;

        this.dataDevoluciones=response.data.devoluciones;
        this.dataTotaDevoluciones=response.data.totalDevoluciones;

        this.dataTotalFormasPago=response.data.totalesFormasPago;
        this.dataTotalIngresado=response.data.totalIngresado;
        this.dataCorteFinalizado=response.data.corteFinalizado;
        this.dataMesasAbiertas=response.data.permitircorte;
        this.dataFechaAceptada=response.data.permitircorteFecha;
        this.FunObtenerItemFormaPago();
      });
    },

    FunObtenerItemFormaPago:function(){
      var route='/cortes/cortesPrincipal/obtenerItems';
      var data ={
        'fecha':this.fecha
      }
      axios.post(route,data).then(response=>{
        this.dataInformacionItem=response.data
      });
    },

    FunFormatearMoneda:function(value){
      var values=parseFloat(value);
        var formatear = new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: 'USD',
          minimumFractionDigits: 2,
        });

      return formatear.format(values);
    },

    FunObtenerItemsAbrirModal:function(value){
      this.formaPagoSelected= value;
      $('#idModificarCorte').modal('show');
    },

    FunGuardarCantidadItemCerrarModal:function (){
      var route='/cortes/cortesPrincipal/GuardarCantidadItem';
      var data={
        'fecha':this.fecha,
        'item':this.dataInformacionItem,
        'formaPagoSelected':this.formaPagoSelected,
        'num_papeleta':this.num_papeleta,
        'num_bolsa':this.num_bolsa
      }
      axios.post(route,data).then(response=>{
        this.FunObtenerInformacionCortes();
        this.FunObtenerItemFormaPago();
        toastr.success("Monto agregado correctamente");
          $('#idModificarCorte').modal('hide');
      });
    },

    FuncalcularDiferencia:function(pos,ingresado,gastos,devolucion){
      var dpos =parseFloat(pos);
      var dingresado =parseFloat(ingresado);
      var dgastos =parseFloat(gastos);
      var dDevolucion =parseFloat(devolucion);
      var dtotalingresado=dingresado+dgastos+dDevolucion;
      if (dpos == dtotalingresado ) {
        return "El sistema no registra diferencias en el corte  ¡ES SEGURO CONTINUAR! ";
      }else{
      return "El sistema  registra diferencias en el corte ¿continuar de todas maneras?";
      }
    },

    FunMostrarModalModificar:function(value){
      this.gastoSelected=value;
        $('#modificarGasto').modal('show');
    },

    FunMostrarModalAutorizar:function(){
        $('#autorizarCorte').modal('show');
    },

    FunMostrarModalModificarDevolucion:function(value){
      this.devolucionSelected=value;
        $('#modificarDevolucion').modal('show');
    },
    //cortes/cortesPrincipal/guardarDevolucion
    FunGuardarGastoExistente:function(){
        var route='/cortes/cortesPrincipal/guardarGastoModificado';
      var data={
        'gasto':this.gastoSelected
      }
      axios.post(route,data).then(response=>{
        this.FunObtenerInformacionCortes();
        this.FunObtenerItemFormaPago();
        toastr.success("Gasto corregido correctamente");
          $('#modificarGasto').modal('hide');
      });
    },

    FunGuardarDevolucionExistente:function(){
        var route='/cortes/cortesPrincipal/guardarDevolucion';
      var data={
        'gasto':this.devolucionSelected
      }
      axios.post(route,data).then(response=>{
        this.FunObtenerInformacionCortes();
        this.FunObtenerItemFormaPago();
        toastr.success("Devolución corregida correctamente");
          $('#modificarDevolucion').modal('hide');
      });
    },

    FunAgregarGasto:function(){
      $('#agregarGasto').modal('show');
    },

    FunGuardarGasto:function(){
      var route='/cortes/cortesPrincipal/guardarGasto';
      var data={
        'fecha':this.fecha,
        'agregarGasto':this.gastoAgregar
      }
      axios.post(route,data).then(response=>{
        this.FunObtenerInformacionCortes();
        this.FunObtenerItemFormaPago();
        this.FunLimpiarGasto();
        toastr.success("Gasto agregado correctamente");
          $('#agregarGasto').modal('hide');
      });
    },

    FunAgregarDevolucion:function(){
      $('#agregarDevolucion').modal('show');
    },

    FunGuardarDevolucion:function(){
      var route='/cortes/cortesPrincipal/guardarDevolucionAgregar';
      var data={
        'fecha':this.fecha,
        'agregarGasto':this.devolucionAgregar
      }
      axios.post(route,data).then(response=>{
        this.FunObtenerInformacionCortes();
        this.FunObtenerItemFormaPago();
        this.FunLimpiarGasto();
        toastr.success("Devolucion agregada correctamente");
          $('#agregarDevolucion').modal('hide');
      });
    },

    FunLimpiarGasto:function(){
      this.gastoAgregar.gasto=0;
      this.gastoAgregar.descripcion="";
      this.devolucionAgregar.gasto=0;
      this.devolucionAgregar.descripcion="";
    },

    FunEliminarGastoModal:function(values){
      this.gastoSelected = values;
      $('#eliminarGato').modal('show');
    },

    FunEliminarDevolucionModal:function(values){
      this.devolucionSelected = values;
      $('#eliminarDevolucion').modal('show');
    },

    FunEliminarGasto:function(){
      var route='/cortes/cortesPrincipal/eliminarGasto';
    var data={
      'gasto':this.gastoSelected
    }
    axios.post(route,data).then(response=>{
      this.FunObtenerInformacionCortes();
      this.FunObtenerItemFormaPago();
      toastr.success("Gasto eliminado correctamente");
        $('#eliminarGato').modal('hide');
    });
    },

    FunEliminarDevolucion:function(){
      var route='/cortes/cortesPrincipal/eliminarGasto';
    var data={
      'gasto':this.devolucionSelected
    }
    axios.post(route,data).then(response=>{
      this.FunObtenerInformacionCortes();
      this.FunObtenerItemFormaPago();
      toastr.success("Devolucion eliminada correctamente");
        $('#eliminarDevolucion').modal('hide');
    });
    },

    checkForm: function () {
      if (this.num_papeleta && this.num_bolsa) {
        return true;
      }

      this.errors = [];

      if (!this.num_bolsa) {
        toastr.warning("Error", "El # de bolsa es obligatorio. ");
        this.errors.push('El # de bolsa es obligatorio.');
      }
      if (!this.num_papeleta) {
          toastr.warning("Error", "El # de papeleta es obligatorio. ");
        this.errors.push('El # de papeleta es obligatoria.');
      }
    },

    FunModalFinalizarCorte:function(){
      $('#finalizarCorte').modal('show');
    },

    FunFinalizarCorte:function(){
      var route='/cortes/cortesPrincipal/finalizarCorte';
      var data={
        'fecha':this.fecha,
        'num_papeleta':this.num_papeleta,
        'num_bolsa':this.num_bolsa
      }
      if (this.checkForm()) {
        axios.post(route,data).then(response=>{
          if(response.data){
            this.FunObtenerInformacionCortes();
            this.FunObtenerItemFormaPago();
            toastr.success("Corte finalizado correctamente");
            $('#finalizarCorte').modal('hide');
          } else {
            toastr.warning("Tienes Mesas Abiertas","No puedes realizar corte");
            $('#finalizarCorte').modal('hide');
          }
        });
      }
    },

    FunGuardarCorte:function(){
      var route='/cortes/cortesPrincipal/guardarCorte';
      var data={
        'fecha':this.fecha,
        'num_papeleta':this.num_papeleta,
        'num_bolsa':this.num_bolsa
      }
      if (this.checkForm()) {
          axios.post(route,data).then(response=>{
            if (response.data == 0) {
              toastr.warning("Existen mesas abiertas");
            }else {
              this.guardarCorte = true;
              this.num_papeleta=null;
              this.num_bolsa=null;
              toastr.success("Corte guardado correctamente");
              $('#autorizarCorte').modal('hide');
            }

        });
      }
    },

    FunDescargarArqueoVenta:function(){
          var route='/cortes/cortesPrincipal/arqueoVentaPDF';
          var data={
            'fecha':this.fecha
        };
        axios({
          method:'post',
          url:route,
          responseType:'blob'
          ,data:data
        }).then(response=>{
          let blob= new Blob([response.data],{type: 'application/pdf'});
          let link=document.createElement('a')
          link.href=window.URL.createObjectURL(blob)
          var win = window.open('', '_blank');
        win.location.href = window.URL.createObjectURL(blob)
        });
    },

    FunDescargarCorteCaja:function(){
          var route='/cortes/cortesPrincipal/corteCajaPDf';
          var data={
            'fecha':this.fecha
        };
        axios({
          method:'post',
          url:route,
          responseType:'blob'
          ,data:data
        }).then(response=>{
          let blob= new Blob([response.data],{type: 'application/pdf'});
          let link=document.createElement('a')
          link.href=window.URL.createObjectURL(blob)
          var win = window.open('', '_blank');
        win.location.href = window.URL.createObjectURL(blob)
        });
    },

    FunDescargarbanamex:function(){
            var route='/cortes/cortesPrincipal/corteDescargaBanamex';
            var data={
              'fecha':this.fecha
          };
          axios({
            method:'post',
            url:route,
            responseType:'blob'
            ,data:data
          }).then(response=>{
            let blob= new Blob([response.data],{type: 'application/pdf'});
            let link=document.createElement('a')
            link.href=window.URL.createObjectURL(blob)
            // link.download='fichadesposito.pdf'
            // link.click()
            var win = window.open('', '_blank');
          win.location.href = window.URL.createObjectURL(blob)
          });

    },

    FunDescargarDolar:function(){
            var route='/cortes/cortesPrincipal/corteDescargarDolar';
            var data={
              'fecha':this.fecha
          };
          axios({
            method:'post',
            url:route,
            responseType:'blob'
            ,data:data
          }).then(response=>{
            let blob= new Blob([response.data],{type: 'application/pdf'});
            let link=document.createElement('a')
            link.href=window.URL.createObjectURL(blob)
            var win = window.open('', '_blank');
           win.location.href = window.URL.createObjectURL(blob)
          });
    }
  }
});
