var app = new Vue({
 el: '#sales_report',
 created: function() {
 },
 data: {
   date_since:"",
   date_to:"",
   Categorias:0,
   Apps:0,
   Horarios:[],
   TotalesSinCategorias:0,
   HoraSInCategoriaTotal:0,
   TotalHorarios:0,
   isLoading:0
 },

   methods: {

     obtener_ventas(){
       this.isLoading = 1;
       this.Categorias=0
       var url_post = '/reportes/sales_report/ventas'

       data_post = {
         'date_since': this.date_since,
         'date_to': this.date_to
       }

       axios.post(url_post, data_post).then(response => {
           this.Total = response.data.Total
           this.Categorias = response.data.Categorias
           this.SinCategoria = response.data.SinCategoria
           this.TotalesSinCategorias = response.data.TotalesSinCategorias
           this.SubtotalFinal = response.data.SubtotalFinal
           this.Impuestos1Final = response.data.Impuestos1Final
           this.Impuestos2Final = response.data.Impuestos2Final
           this.Impuestos3Final = response.data.Impuestos3Final
           this.Impuestos4Final = response.data.Impuestos4Final
           this.CanselacionesFinal = response.data.CanselacionesFinal
           this.CortesiasFinal = response.data.CortesiasFinal
           this.TotalHorarios = response.data.TotalHorarios
           this.TotalFinalHorario = response.data.TotalFinalHorario
           this.FormaPagos = response.data.FormaPagos
           this.HoraMesa = response.data.HoraMesa
           this.MermaFinal = response.data.MermaFinal
           this.Horarios = response.data.Horarios
           this.HoraSInCategoriaTotal = response.data.HoraSInCategoriaTotal
           this.Impuesto1 = response.data.Impuesto1;
           this.Impuesto2 = response.data.Impuesto2;
           this.Impuesto3 = response.data.Impuesto3;
           this.Impuesto4 = response.data.Impuesto4;
           this.Apps = response.data.Apps;

           this.isLoading = 0;

       }).catch(error => {
         toastr.error("Ocurrió un error al generar el reporte");
         console.log(error);
         this.isLoading = 0;
       });
     },

     generar_pdf_reporte : function(){
         var rutaPDfGenerales='/reportes/sales_report/pdf';

         data_reporte = {
             'date_since' : this.date_since,
             'date_to' : this.date_to
         };

         let string_nombre = 'Sales report';

         axios({
             method:'post',
             url:rutaPDfGenerales,
             responseType:'blob'
             ,data:data_reporte
         }).then(response=>{
             let blob= new Blob([response.data],{type: 'application/pdf'});
             let link=document.createElement('a')
             link.href=window.URL.createObjectURL(blob)
             link.download = string_nombre + '.pdf'
             link.click()
         });
     },


     formatoVenta(valor) {
       let val = (valor/1).toFixed(2).replace(',', '.');
       return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
     },
   }
});
