var app = new Vue({
 el: '#reportEquivalencias',
 created: function() {

   this.getListaProdPrimarios();
 },
 data: {
   consulta: { fechaDesde: null, fechaHasta: null, prodPrimarioSelected: [] },
   listaProdPrimarios: [],
   reporte_resultados: null,
   isLoading: 0,
   checked: false,
   detallado: true,
 },

methods: {

  getListaProdPrimarios: function() {
    var UrlListaProdPrimarios = '/reportes/equivalencias/obtenerProdPrimarios';
    axios.get(UrlListaProdPrimarios).then(response=> {
      this.listaProdPrimarios = response.data;
    });
  },

  seleccionarTodo: function() {
    if (this.checked) {
      this.consulta.prodPrimarioSelected = this.listaProdPrimarios;
    } else {
      this.consulta.prodPrimarioSelected = [];
    }

  },

  generarReporte: function() {
      this.isLoading = 1;
      var generarReporteURL = '/reportes/equivalencias/generar';
      var data_reporte = {
          'fechaDesde' : this.consulta.fechaDesde,
          'fechaHasta' : this.consulta.fechaHasta,
          'prodPrimarioSelected' : this.consulta.prodPrimarioSelected
      };
      axios.post(generarReporteURL, data_reporte).then(response => {
          this.reporte_resultados = response.data;
          this.isLoading = 0;
          console.log('Hecho');
      });
  },

    formatoVenta(valor) {
      let val = (valor/1).toFixed(2).replace(',', '.');
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },

  },
  components: {
    Multiselect: window.VueMultiselect.default,
  }

});
