var app = new Vue({
    el: "#rastrillo",
    created: function () {
        this.getCatFormasPago();
    },
    data: {
        catFormasPago: [],
        consulta: {
            fechaDesde: null,
            fechaHasta: null,
            formaPago: 0,
        },
        carga: false,
        cargaPDF: false,
        reporte: null,
        resumenPDF: false,
        resumenExcel: false,
    },

    methods: {

        getCatFormasPago: function() {
            var url = '/reportes/rastrillo/catformaspago';

            axios.get(url).then(response => {
                this.catFormasPago = response.data;
            }).catch(function(error) {
                toastr.warning("Error", "Ha ocurrido un error ");
                console.log(error);
            });
        },

        generarReporte: function() {
            this.carga = true;
            var url = '/reportes/rastrillo/generar';
            var data = this.consulta;

            axios.post(url, data).then(response => {
                this.reporte = response.data;
                this.carga = false;
            }).catch(function(error) {
                toastr.warning("Error", "Ha ocurrido un error ");
                console.log(error);
            });
        },

        check: function(item) {
            var id = item.cHisMesFolio;

            var url ="/reportes/rastrillo/actualizar/" + id;
            var data = {
                'cHisMesFacturado' : item.cHisMesFacturado,
                'cHisMesVisible' : item.cHisMesVisible
            };

            axios.post(url, data).then(() => {
                toastr.success("Ticket actualizado con éxito");
            }).catch(function(error) {
                toastr.warning("Error", "Ha ocurrido un error ");
                console.log(error);
            });
        },

        exportarPDF: function() {
            this.cargaPDF = true;
            var url = '/reportes/rastrillo/pdf';
            var data = this.consulta;

            axios({ method:'post', url:url, responseType:'blob',data:data }).then(response => {
                let blob = new Blob([response.data], { type: 'application/pdf' });
                let link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = 'ReporteTicket.pdf';
                link.click();
                this.cargaPDF = false;
            }).catch(function(error) {
                toastr.warning("Error", "Ha ocurrido un error ");
                console.log(error);
            });
        },

        reporteResumenPDF: function() {
            this.resumenPDF = true;
            var url = '/reportes/rastrillo/resumen/pdf';
            var data = this.consulta;

            axios({ method:'post', url:url, responseType:'blob',data:data }).then(response => {
                let blob = new Blob([response.data], { type: 'application/pdf' });
                let link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = 'Venta por categorias Resumen.pdf';
                link.click();
                this.resumenPDF = false;
            }).catch(function(error) {
                toastr.warning("Error", "Ha ocurrido un error ");
                console.log(error);
            });
        },


        reporteResumenExcel:function(){
          this.resumenExcel = true;
          data = this.consulta;
          axios({
            method:'post',
            url:'/reportes/rastrillo/resumen/excel',
            responseType:'blob'
            ,data:data
          }).then(response=>{
            let blob= new Blob([response.data],{type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            let link=document.createElement('a')
            link.href=window.URL.createObjectURL(blob)
            link.download='resumen.csv'
            link.click();
            this.resumenExcel = false;
          });
        },

        formatPrice: function(value) {
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2
              });
            return formatter.format(value);
        },

        formatDate: function(fecha){
            var year = fecha.toString().substr(0,4);
            var month = fecha.toString().substr(4,2);
            var day = fecha.toString().substr(6,2);

            return day + '/' + month + '/' + year;
        },

    },
});
