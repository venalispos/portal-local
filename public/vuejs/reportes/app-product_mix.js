var app = new Vue({
 el: '#sales_report',
 created: function() {


 },
 data: {
   date_since:"",
   date_to:"",
   SinCategoria:[],
   Total:0,
   TotalesSinCategorias:0,
   Impuesto1:null,
   Impuesto2:null,
   Impuesto3:null,
   Impuesto4:null,
   Categorias:[],
   isLoading:false,
   CantidadFinal:0,
   PrecioFinal:0,
   SubtotalFinal:0,
   Impuestos1Final:0,
   Impuestos2Final:0,
   Impuestos3Final:0,
   Impuestos4Final:0,
 },

   methods: {

     obtener_reporte(){
       var url_post = '/reportes/product_mix/ventas';

       data_post = {
         'date_since': this.date_since,
         'date_to': this.date_to
       }
       this.Categorias = 0
       this.isLoading = true;
       axios.post(url_post, data_post).then(response => {

         this.Categorias = response.data.Categorias;
         this.SinCategoria = response.data.SinCategoria;
         this.Total = response.data.Total;
         this.TotalesSinCategorias = response.data.TotalesSinCategorias;
         this.Impuesto1 = response.data.Impuesto1;
         this.Impuesto2 = response.data.Impuesto2;
         this.Impuesto3 = response.data.Impuesto3;
         this.Impuesto4 = response.data.Impuesto4;
         this.CantidadFinal = response.data.CantidadFinal
         this.PrecioFinal = response.data.PrecioFinal
         this.SubtotalFinal = response.data.SubtotalFinal
         this.Impuestos1Final = response.data.Impuestos1Final
         this.Impuestos2Final = response.data.Impuestos2Final
         this.Impuestos3Final = response.data.Impuestos3Final
         this.Impuestos4Final = response.data.Impuestos4Final

         this.isLoading = false

       }).catch(error => {
         toastr.error("Ocurrió un error al generar el reporte");
         console.log(error);
         this.isLoading = false;
       });
     },

     generar_pdf_reporte : function(){
         var rutaPDfGenerales = '/reportes/product_mix/pdf';

         data_reporte = {
             'date_since' : this.date_since,
             'date_to' : this.date_to
         };

         let string_nombre = 'Product mix';

         axios({
             method:'post',
             url:rutaPDfGenerales,
             responseType:'blob'
             ,data:data_reporte
         }).then(response=>{
             let blob= new Blob([response.data],{type: 'application/pdf'});
             let link=document.createElement('a')
             link.href=window.URL.createObjectURL(blob)
             link.download = string_nombre + '.pdf'
             link.click()
         });
     },


     formatoVenta(valor) {
       let val = (valor/1).toFixed(2).replace(',', '.');
       return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
     },
   }
});
