
<?php

$textosTitulos=DB::table('tPortaltitulos')->get();
$textosTitulos=json_decode(json_encode($textosTitulos),true);
$_perfil_1 = auth()->user()->cUsuPuesto1Perfil;
$_perfil_1_Activo = auth()->user()->cUsuPuesto1Activo;
$_perfil_2 = auth()->user()->cUsuPuesto2Perfil;
$_perfil_2_Activo = auth()->user()->cUsuPuesto2Activo;
$_perfil_3 = auth()->user()->cUsuPuesto3Perfil;
$_perfil_3_Activo = auth()->user()->cUsuPuesto3Activo;
$_perfilesActivos = NULL;


if($_perfil_1_Activo == 1)
  $_perfilesActivos = $_perfilesActivos.$_perfil_1.",";
if($_perfil_2_Activo == 1)
  $_perfilesActivos = $_perfilesActivos.$_perfil_2.",";
if($_perfil_3_Activo == 1)
  $_perfilesActivos = $_perfilesActivos.$_perfil_3.",";

$_perfilesActivos = substr($_perfilesActivos,0,-1);


$_perfilesActuales = explode(",", $_perfilesActivos);
$accesosPerfiles = DB::table('tPerfiles')->select('cPerAccesPortal')->whereIn('cPerId',$_perfilesActuales)->get();

$accesosPerfiles=json_decode($accesosPerfiles,true);

$_permisosasginados = NULL;
foreach ($accesosPerfiles as $key ) {

  $_permisosasginados= $_permisosasginados.$key['cPerAccesPortal'].",";
  $_permisosasginados = substr($_permisosasginados,0,-1);

}

       $enlace_actual = $_SERVER['REQUEST_URI'];//'http://'.$_SERVER['HTTP_HOST'].
       $gFolio = DB::table('tMenu')
       ->select('cMenFolio')
       ->where('cMenURL',$enlace_actual)
       ->get();
       try {

        $Folio=json_decode(json_encode($gFolio),true);

        if (strpos($_permisosasginados,$Folio[0]['cMenFolio'])) {

        }else{

        }
      } catch (Exception $e) {
        //

     }


     ?>

     <!DOCTYPE html>
     <html lang="{{ app()->getLocale() }}">
     <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>Venalis</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css')}}">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css')}}">
      <!-- Ionicons -->
      <link rel="stylesheet" href="{{ asset('/css/ionicons.min.css')}}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ asset('/AdminLTE/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="{{ asset('/AdminLTE/dist/css/skins/skin-green.min.css')}}">
   <!-- Morris chart -->
   <link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/morris.js/morris.css')}}">
   <!-- jvectormap -->
   <link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/jvectormap/jquery-jvectormap.css')}}">
   <!-- Date Picker -->
   <link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="{{  asset('/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

   <link rel="stylesheet" href="{{  asset('/css/toastr.min.css')}}">
   <link rel="stylesheet" href="/css/vue-multiselect.min.css">
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green sidebar-mini">
  <div class="wrapper">
    @include('header')
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      @include('sidebar')
      <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->


    <!-- /.content-wrapper -->
    <footer class="main-footer">
      @include('footer')
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="{{ asset('/AdminLTE/bower_components/jquery/dist/jquery.min.js')}}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ asset('/AdminLTE/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <!-- Morris.js charts -->
  <script src="{{ asset('/AdminLTE/bower_components/raphael/raphael.min.js')}}"></script>
  <script src="{{ asset('/AdminLTE/bower_components/morris.js/morris.min.js')}}"></script>
  <!-- Sparkline -->
  <script src="{{ asset('/AdminLTE/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
  <!-- jvectormap -->
  <script src="{{ asset('/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{ asset('/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- jQuery Knob Chart -->
  <script src="{{ asset('/AdminLTE/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
  <!-- daterangepicker -->
  <script src="{{ asset('/AdminLTE/bower_components/moment/min/moment.min.js')}}"></script>
  <script src="{{ asset('/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
  <!-- datepicker -->
  <script src="{{ asset('/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="{{ asset('/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
  <!-- Slimscroll -->
  <script src="{{ asset('/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
  <!-- FastClick -->
  <script src="{{ asset('/AdminLTE/bower_components/fastclick/lib/fastclick.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('/AdminLTE/dist/js/adminlte.min.js')}}"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="{{ asset('/AdminLTE/dist/js/pages/dashboard.js')}}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('/AdminLTE/dist/js/demo.js')}}"></script>
  <script src="{{ asset('/js/toastr.min.js')}}"></script>
  <script>
    toastr.options = {
      "positionClass": "toast-top-right"
    }
  </script>
</body>
</html>
