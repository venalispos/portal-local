@extends('Master')
@section('titulo','Reporte de inventario')
@section('content')
<style>
#formulario:hover {
	background-color: #BCD9D7 !important;

}
</style>
<div id="IdInventarioInicial">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<a class="btn btn-app">
					<i class="fa fa-download" @click='descargarPDFInventario'></i> PDF
				</a>

				<!-- <a class="btn btn-app">
					<i class="fa fa-download" @click='descargarInventarioEXCEL'></i> EXCEL
				</a> -->
			</div> 
		</div>
	</div>
	<div class="row">
		
		<div class=" col-lg-10  col-md-10  col-sm-12 col-xs-12">
			<table class="table table-condensed">
				<thead>
					<th>CÓDIGO</th>
					<th>DESCRIPCIÓN</th>
					<th align="right" style="text-align: right;">INICIAL</th>
					<th align="right" style="text-align: right;">CANTIDAD VENDIDA</th>
					<th align="right" style="text-align: right;">DIFERENCIA</th>
					<th align="right" style="text-align: right;">ESTATUS</th>

				</thead>
				<tbody>
					<template v-for='producto in reporteInventario'>
						<tr>
							<td>@{{producto.cProInterCodigo}}</td>
							<td >@{{producto.cProDesTicket}}</td>
							<td align="right" style="text-align: right;">  @{{producto.tInvIniValorMinimo}}
							</td>
							<td  align="right" style="text-align: right;">@{{producto.cHisProCantidad}}</td>
							<td v-if='producto.DIFERENCIA' align="right" style="text-align: right;">@{{producto.DIFERENCIA}}</td>
							<td  v-if='producto.DIFERENCIA == null && producto.cHisProCantidad == 0 && producto.tInvIniValorMinimo != 0' align="right">@{{producto.tInvIniValorMinimo}}</td>
							<td  v-if='producto.DIFERENCIA == null && producto.tInvIniValorMinimo == 0 && producto.cHisProCantidad != 0' align="right">- @{{producto.cHisProCantidad}}</td>
							<td  v-if='producto.DIFERENCIA == null && producto.tInvIniValorMinimo == 0 && producto.cHisProCantidad == 0' align="right">0.00</td>
							<template v-if='producto.DIFERENCIA == null && producto.cHisProCantidad == 0 && producto.tInvIniValorMinimo != 0' align="right">
								<td align="right"><small class="label label-success">El almacen está completo <i class="fa fa-check"></i> </small></td>	
							</template>
							<template v-if='producto.DIFERENCIA == null && producto.tInvIniValorMinimo == 0 && producto.cHisProCantidad != 0' align="right">
								<td align="right"><small class="label label-warning" title="No está registrado en almacen">No está registrado en almacen <i class="fa fa-exclamation-triangle"></i></small></td>	
							</template>
							<template v-if='producto.DIFERENCIA'>
								<td align="right"><small class="label label-info" title="">Requieres mas de este producto <i class="fa fa-check"></i> </small></td>	
							</template>
							<template v-if='producto.DIFERENCIA == null && producto.tInvIniValorMinimo == 0 && producto.cHisProCantidad == 0' align="right">
								<td align="right"><small class="label label-danger">El producto no se registro ni se vendió <i class="fa fa-times"></i> </small></td>	
							</template>
						</tr>
					</template>
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-InventarioInicial.js"></script>
@endsection
