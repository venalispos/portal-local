<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<div>
		<table width="100%" style="font-family: courier; ">
			<tr>
				<th width="20%" align="left"><br>CÓDIGO|</th>
				<th width="40%"><br>DESCRIPCIÓN|</th>
				<th align="right" style="text-align: right;"><br>INICIAL|</th>
				<th align="right" style="text-align: right;">CANTIDAD <br> VENTA|</th>
				<th align="right" style="text-align: right;"><br>DIFERENCIA|</th>

			</tr>
			
				@foreach($dataGPDF['productos'] as $producto )
				@if($producto['DIFERENCIA'] != NULL )
				<tr style="background-color: #EDE125">
				@else
				<tr>	
				@endif
				@if($producto['DIFERENCIA'] == NULL && $producto['cHisProCantidad'] != 0 && $producto['tInvIniValorMinimo']== 0  )
				<tr style="background-color: #BD4346">
				@endif
				<td width="20%">{{$producto['cProInterCodigo']}}</td>
				<td  width="40%">{{$producto['cProDesTicket']}}</td>
				<td align="right" style="text-align: right;">   <?php echo number_format($producto['tInvIniValorMinimo'],2) ?></td>
				<td  align="right" style="text-align: right;"><?php echo number_format($producto['cHisProCantidad'],2) ?></td>
				@if($producto['DIFERENCIA'] != NULL )
				<td  align="right" style="text-align: right;"><?php echo number_format($producto['DIFERENCIA'],2) ?></td>
				@endif
				
				@if($producto['DIFERENCIA'] == NULL && $producto['cHisProCantidad'] == 0 && $producto['tInvIniValorMinimo'] != 0 )
				<td align="right"> <?php echo number_format($producto['tInvIniValorMinimo'],2) ?></td>
				@endif
				@if($producto['DIFERENCIA'] == NULL && $producto['cHisProCantidad'] != 0 && $producto['tInvIniValorMinimo']== 0  )
				<td align="right"><?php echo number_format($producto['cHisProCantidad'],2) ?></td>
				@endif
				@if($producto['DIFERENCIA'] == NULL && $producto['cHisProCantidad'] == 0 && $producto['tInvIniValorMinimo']== 0  )
				<td align="right">0.00</td>
				@endif
			</tr>
				@endforeach

				<!-- <template v-for='producto in reporteInventario'>
					<tr>

						<td  v-if='producto.DIFERENCIA == null && producto.tInvIniValorMinimo == 0 && producto.cHisProCantidad == 0' align="right">0.00</td>
						<template v-if='producto.DIFERENCIA == null && producto.cHisProCantidad == 0 && producto.tInvIniValorMinimo != 0' align="right">
							<td align="right"><small class="label label-success">El almacen está completo <i class="fa fa-check"></i> </small></td>	
						</template>
						<template v-if='producto.DIFERENCIA == null && producto.tInvIniValorMinimo == 0 && producto.cHisProCantidad != 0' align="right">
							<td align="right"><small class="label label-warning" title="No está registrado en almacen">No está registrado en almacen <i class="fa fa-exclamation-triangle"></i></small></td>	
						</template>
						<template v-if='producto.DIFERENCIA'>
							<td align="right"><small class="label label-info" title="">Requieres mas de este producto <i class="fa fa-check"></i> </small></td>	
						</template>
						<template v-if='producto.DIFERENCIA == null && producto.tInvIniValorMinimo == 0 && producto.cHisProCantidad == 0' align="right">
							<td align="right"><small class="label label-danger">El producto no se registro ni se vendió <i class="fa fa-times"></i> </small></td>	
						</template>
					</tr>
				</template> -->
		</table>
	</div>
</body>
</html>