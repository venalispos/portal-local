<form  @submit.prevent="FuncionNuevaCantidadProducto()">
	{{ csrf_field() }}
	<div class="modal fade" id="mmodificar" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">

				<template v-if='dProductoEditar'>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
						
					</div >
					<div class="modal-body">
						<div class="panel panel-primary">
					<div class="panel-heading">
						<h4>Modificar <strong>@{{dProductoEditar.cProDesTicket}}</strong> </h4>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label> Cantidad actual @{{dProductoEditar.cantidad}}</label>
						</div>
						<div class="form-group">
							<input type="" class="form-control" name="" placeholder="Nueva cantidad" v-model='dProductoEditar.nuevaCantidad'>
						</div>
					</div>
				</div>
					</div>
					<div class="modal-footer">
						<div class="pull-right">
							<button class="btn btn-primary" type="submit" >Guardar</button>
						</div>
					</div>
				</template>
			</div>
		</div>
	</div>
</form>