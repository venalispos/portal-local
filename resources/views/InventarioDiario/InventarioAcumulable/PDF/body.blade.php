<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div>
		<table width="100%" style="font-family: courier; ">

			<thead   style="background:#C5EEC3">
				<tr borde='2'>
					<th borde='2' colspan="3" style="border: #C5EEC3 5px solid;">PRODUCTO</th>
					<th borde='2' colspan="9"  style="border: #C5EEC3 5px solid;">CANTIDAD</th>
					<th borde='2' colspan="6"  style="border: #C5EED1 6px solid;">VALOR MONETARIO</th>
				</tr>
				<th align="left"  >CODIGO|</th>
				<th align="left" >DESCRIPCION|</th>
				<th  align="left" >PRECIO|</th>
				<th   align="right">INICIAL|</th>
				<th   align="right">ENTRADAS|</th>
				<th   align="right">MERMAS|</th>
				<th   align="right">DEVOLUCION|</th>
				<th   align="right" >VENTA <br> CALCULADA| </th>
				<th  align="right" style="background:#C5EEC3">VENTA <br> CAPTURADA| </th>
				<th  align="right">EXISTENCIA <br> CALCULADA| </th>
				<th   align="right">EXISTENCIA <br> CAPTURADA| </th>
				<th   align="right">DIFERENCIAS| </th>
				<th  align="right">VENTA <br>CALCULADA|</th>
				<th  align="right">VENTA <br>CAPTURADA| </th>
				<th align="right">DIFERENCIAS| </th>
				<th  align="right">EXISTENCIA <br>CALCULADA|</th>
				<th  align="right">EXISTENCIA <br>CAPTURADA|</th>
				<th align="right">DIFERENCIAS|</th>
			</thead>
		@foreach($dataGPDF['informacionInventario'] as $Inventario)

		<?php

		$total=number_format(($Inventario['Inicial'])+floatval($Inventario['ENTRADA'])-floatval($Inventario['SALIDAM'])-floatval($Inventario['SALIDAD'])-floatval($Inventario['cantidadventa']),2);
		?>

		@if($total > $Inventario['SALIDADUSUARIO'])
		<tr style="background: #C5EEC3;">
			@else
					@if($total < $Inventario['SALIDADUSUARIO'])
				<tr  style="background: orange;">
				@else
				<tr>
			@endif
				@endif
				<td alig="left" width="10%" align="left" style="text-align: left;"><?php echo $Inventario['codigo']; ?></td>
				<td alig="left" width="30%" align="left"><?php echo $Inventario['cProDesTicket']; ?></td>
				<td alig="right" width="10%" align="right"><?php echo number_format($Inventario['cProPrecio'],2); ?></td>
				<td style="text-align:right" alig="right" width="10%" align="right"><?php echo number_format($Inventario['Inicial'],2); ?></td>
				<td style="text-align:right" alig="right" width="10%" align="right"><?php echo number_format($Inventario['ENTRADA'],2); ?></td>
				<td style="text-align:right" alig="right" width="10%" align="right"><?php echo number_format($Inventario['SALIDAM'],2); ?></td>
				<td style="text-align:right" alig="right" width="10%" align="right"><?php echo number_format($Inventario['SALIDAD'],2); ?></td>
				@if($Inventario['cantidadventa'] != NULL)
				<td style="text-align:right" alig="right" width="10%" align="right"><?php echo number_format($Inventario['cantidadventa'],2); ?></td>
				@else
				<td style="text-align:right" alig="right" width="10%" align="right">0.00</td>
				@endif
				<!-- Venta generada por venta capturada -->
		 				<?php $totalVentaCapturada= number_format(floatval($Inventario['Inicial'])+floatval($Inventario['ENTRADA'])-floatval($Inventario['SALIDAM'])-floatval($Inventario['SALIDAD'])-floatval($Inventario['SALIDADUSUARIO']),2); ?>
				<td style="text-align:right" alig="right" width="10%" align="right"><?php echo $totalVentaCapturada; ?></td>
				<!-- Existencia calculada -->
				<?php $ExistenciaCalculada= number_format(floatval($Inventario['Inicial'])+floatval($Inventario['ENTRADA'])-floatval($Inventario['SALIDAM'])-floatval($Inventario['SALIDAD'])-floatval($Inventario['cantidadventa']),2); ?>
				<td style="text-align:right" alig="right"><?php echo $ExistenciaCalculada; ?></td>
				<!-- existencia capturada -->
				<td style="text-align:right" alig="right"><?php echo number_format($Inventario['SALIDADUSUARIO'],2); ?></td>
				<!-- diferencias  de las existencias-->
				<td style="text-align:right" alig="right"><?php echo number_format($ExistenciaCalculada - $Inventario['SALIDADUSUARIO'],2) ?></td>

				<!-- Venta calculada en valor monetario -->
				<td style="text-align:right" alig="right"><?php echo number_format(floatval($Inventario['cantidadventa'])*floatval($Inventario['cProPrecio']),2); ?></td>
				<!-- venta capturada -->
				<?php $totalVentaCapturada=number_format(floatval($Inventario['cProPrecio'])*(floatval($Inventario['Inicial'])+floatval($Inventario['ENTRADA'])-floatval($Inventario['SALIDAM'])-floatval($Inventario['SALIDAD'])-floatval($Inventario['SALIDADUSUARIO'])),2); ?>
				<td style="text-align:right" alig="right"><?php  echo $totalVentaCapturada; ?></td>
				<!-- DIferencias de ventas siempre y cuando la venta capturada sea mayor a la del sistema -->
				@if($totalVentaCapturada > floatval($Inventario['cantidadventa'])*floatval($Inventario['cProPrecio']))
				<td style="text-align:right" alig="right">0.00</td>
				@else
				<?php $DiferenciaVentasCapturadaCalculada=number_format((floatval($Inventario['cProPrecio'])*floatval($Inventario['cantidadventa']))-($totalVentaCapturada),2); ?>
				<td style="text-align:right" alig="right"><?php echo $DiferenciaVentasCapturadaCalculada; ?></td>
				@endif
				<!-- Existente en el sistema monetario-->
				<td style="text-align:right" alig="right"> <?php echo number_format(floatval($Inventario['cProPrecio'])* $ExistenciaCalculada,2); ?></td>
				<!-- existencia capturada monetario-->
				<td style="text-align:right" alig="right"> <?php echo number_format(floatval($Inventario['cProPrecio'])*floatval( $Inventario['SALIDADUSUARIO']),2); ?></td>
				@if( number_format(floatval($Inventario['cProPrecio'])* $ExistenciaCalculada,2) < number_format(floatval($Inventario['cProPrecio'])*floatval( $Inventario['SALIDADUSUARIO']),2) )
				<td style="text-align:right" alig="right"><?php echo   number_format(number_format(floatval($Inventario['cProPrecio'])* $ExistenciaCalculada,2) -  number_format(floatval($Inventario['cProPrecio'])*floatval( $Inventario['SALIDADUSUARIO']),2),2); ?></td>
				@else
				<td style="text-align:right" alig="right">0.00</td>
				@endif
			</tr>
			@endforeach
		</table></div>
	</body>
	</html>
