<form  @submit.prevent="FuncionEliminarProductoSalida()">
	{{ csrf_field() }}
	<div class="modal fade" id="_idEliminarSalida" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">

				<template v-if='dProductoEditar'>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
						<h4 v-if='dProductoEditar.tInvIniStatus == 2'>¿Desea realmente eliminar el producto de la lista de salidas por merma?</h4>
						<h4  v-if='dProductoEditar.tInvIniStatus == 3'>¿Desea realmente eliminar el producto de la lista de salidas por devoluciones?</h4>
					</div >
					<div class="modal-body">
						<h3><strong>@{{dProductoEditar.cProDesTicket}}</strong></h3>
					</div>
					<div class="modal-footer">
						<div class="pull-right">
							<button class="btn btn-warning" type="submit" >Si, deseo eliminar</button>
						</div>
					</div>
				</template>
			</div>
		</div>
	</div>
</form>