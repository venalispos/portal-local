@extends('Master')
@section('titulo',' CAPTURAR PEDIDOS DIARIOS')
@section('content')
<style>
	#formulario:hover {
		background-color: #BCD9D7 !important; 

	}
</style>
<div id="IdInventarioInicial">
  <template v-if='vSCapturaInicialFinalizada == 0'>
  <div class="row">
    <template v-if='vSCapturaInicialFinalizada == 0'>
     <div class=" col-lg-12">
      <div class="box box-default">
       <div class="box-header with-border">
        <h3 class="box-title">INGRESAR PRODUCTO</h3>

        <div class="box-tools pull-right">
                    <!--  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
                      <button class="btn btn-warning btn pull-right" @click='FuncionFinalizarCapturaInventario()'>Finalizar captura de inventario</button>
                    </div>
                  </div>
                  <!-- /.box-header --> 
                  <div class="box-body">
                    <div class=col-lg-4>
                      <form @submit.prevent="FuncionBuscarProducto()">
                        <div class="form-group">
                          <input type="text" class="form-control" id="InputBuscarProducto" v-model='vProductoBuscar' required="" title="escanear código" placeholder="Ingrese código del producto">
                        </div>
                        <div class="form-group">
                          <button class="btn btn-primary btn-block">BUSCAR</button>
                        </div>
                      </form>
                    </div>
                    <div class="col-lg-8">
                      <div class="form-group">

                        <template v-if='dDatosProducto'>
                         <div class="panel panel-primary">
                          <div class="panel-heading">Información</div>
                          <div class="panel-body">
                           <template v-if='dDatosProducto'>
                             <form @submit.prevent="FuncionAgregarInventario(dDatosProducto[0])">
                              <table class="table table-hover">
                                <tr><td><small>CODIGO </small><strong>@{{dDatosProducto[0].cProInterCodigo}}</strong></td>
                                  <td><small>DESCRIPCIÓN </small><strong>@{{dDatosProducto[0].cProDesTicket}}</strong></td>
                                  <td><small>CANTIDAD A AGREGAR </small> <input type="number" class="form-control" v-model='dDatosProducto[0].cProCantidad' id="inputCantidadEntrada" required="" placeholder="Ingrese una cantidad"></td>
                                </tr>
                                <tr>
                                  <td colspan="3"><button class="btn btn-primary btn pull-right" type="submit">AGREGAR</button></td>
                                </tr>
                              </table>
                            </form> 
                          </template>
                        </div>
                      </div>
                    </template>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix">

              </div>
              <!-- /.box-footer -->
            </div>

          </div>
        </template>
      </div>
      <template v-if='vADatosInventario'>
        <div class="row">
          <div class="col-lg-12">
           <div class="box box-default">
            <div class="box-header with-border">
             <h3 class="box-title">INVENTARIO INICIAL</h3>

             <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
           <div class="table-responsive">
            <table class="table no-margin">
             <thead>
              <th>Código producto</th>
              <th>Descripción</th>
              <th>Cantidad Inicial Diaria</th>
              <th>Acciones</th>
            </thead>
            <tbody>
              <template v-for='data in vADatosInventario'>
                <tr>
                  <td>@{{data.cProInterCodigo}}</td>
                  <td>@{{data.cProDesTicket}}</td>
                  <td>@{{data.cantidad}}</td>
                  <td v-if='vSCapturaInicialFinalizada== 0'><button class="btn btn-primary" @click='FuncionMostrarModalEliminar(data)'><i class="fa fa-edit"></i></button> <button class="btn btn-primary" @click='FuncionModalEliminarProducto(data)'> <i class="fa fa-trash"></i></button></td>
                  <td v-else>Opciones restringidas</td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">

      </div>
      <!-- /.box-footer -->
    </div>

  </div>
</div>
</template>
</template>
<template v-else><p><small>Captura de pedidos iniciales finalizada</small></p></template>
@include('InventarioDiario.InventarioAcumulable._modalmodificar')
@include('InventarioDiario.InventarioAcumulable._modalEliminar')
@include('InventarioDiario.InventarioAcumulable._modalFinalizarInventarioDiario')
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-Inventario.js"></script>
@endsection
