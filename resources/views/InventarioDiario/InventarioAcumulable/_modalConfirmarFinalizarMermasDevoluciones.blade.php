<form  @submit.prevent="FuncionFinalizarMermasDevoluciones()">
	{{ csrf_field() }}
	<div class="modal fade" id="_idFinalizarMermasDevoliciones" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">


					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" >&times;</button>
						<h4>¿Desea finalizar las salidas por mermas y devolución?</h4>
					</div >
					<div class="modal-body">
						
					</div>
					<div class="modal-footer">
						<div class="pull-right">
							<button class="btn btn-warning" type="submit" >Si, deseo Finalizar</button>
							<button type="reset" class="btn btn-primary" data-dismiss="modal" > Cancelar</button>
						</div>
					</div>
				
			</div>
		</div>
	</div>
</form>