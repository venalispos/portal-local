@extends('Master')
@section('titulo','Reporte de Inventario')
@section('content')

<div id="IdInventarioInicial">
	<template v-if='permisoReprote == 0'>
		<div class="row">
		<div class="col-lg-12">
				<form @submit.prevent='FuncionBuscarInventario()'>
						<div class="row">
							<div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
								<div class="form-group">
									<label>Seleccione una fecha para ver  detalles del inventario </label>
									<input type="date" class="form-control" v-model='fechaReporteInventario' name="">
								</div>
								<div class="form-group">
									<button class="btn btn-primary btn-block">BUSCAR <i class="fa fa-search"></i></button>
								</div>
								<div class="form-group">
									<template v-if='datosReporteInventario '>
									<button class="btn btn-app" @click='FuncionGenerarPDFInventario()'> <i class="fa fa-save"></i>Descargar PDF</button>
									</template>

								</div>
							</div>

						</div>
					</form>
		</div>
	</div>
	<template v-if='datosReporteInventario'>
	<div class="row">
		 <div class="col-lg-12">
		 	<template >
		 	<div class="table-responsive">
		 		<table class="table table-hover" >
					<thead borde='2'>
						<th colspan="3" style="border: #C5EEC3 5px solid;">PRODUCTO</th>
						<th colspan="9"  style="border: #C5EEC3 5px solid;">CANTIDAD</th>
						<th colspan="6"  style="border: #C5EEC3 5px solid;">VALOR MONETARIO</th>
					</thead>
		 			<thead style="background:#C5EEC3">
		 				<th align="left"  >CÓDIGO</th>
		 				<th align="left" >DESCRIPCIÓN</th>
		 				<th align="left" >PRECIO</th>
		 				<th  align="right">EXISTENCIA <br>INICIAL	</th>
		 				<th  align="right">ENTRADAS <br> DIARIA	</th>
		 				<th  align="right">MERMAS</th>
		 				<th  align="right">DEVOLUCIÓN</th>
		 				<th  align="right" >VENTA <br> CALCULADA </th>
						<th align="right" style="background:#C5EEC3">VENTA <br> CAPTURADA </th>
		 				<th >EXISTENCIA <br> (calculada) </th>
		 				<th  >EXISTENCIA <br> (capturada) </th>
		 				<th  >DIFERENCIAS </th>
						<th >VENTA <br> (calculada)</th>
						<th >VENTA <br> (capturada) </th>
						<th>DIFERENCIAS </th>
						<th >EXISTENCIA <br>(calculada)</th>
						<th >EXISTENCIA <br> (Capturado) </th>
						<th>DIFERENCIAS</th>
		 			</thead>
		 			<tbody>
		 				<template v-for='datosProductos in datosReporteInventario'>
		 					<tr>
								<!-- Descripción del código -->
		 						<td title="CODIGO DEL PRODUCTO" align="left">@{{datosProductos.codigo}}</td>
								<!-- Descripción del ticket -->
		 						<td title="DESCRIPCIÓN DEL PRODUCTO" align="left">@{{datosProductos.cProDesTicket}}</td>
								<!-- Precio de producto -->
		 						<td title="PRECIO DEL PRODUCTO" align="left">@{{datosProductos.cProPrecio}}</td>
								<!-- Inventario inicial día anterior -->
		 						<td title="INVENTARIO INICIAL">@{{FuncionformateardosDecimales(datosProductos.Inicial)}}</td>
								<!-- Inventario inicial Diario -->
		 						<td title="ENTRADAS DIARIAS">@{{FuncionformateardosDecimales(datosProductos.ENTRADA)}}</td>
								<!-- Salidas por mermas -->
		 						<td title="SALIDAS POR MERMAS">@{{FuncionformateardosDecimales(datosProductos.SALIDAM)}}</td>
								<!-- Salidas por devolución -->
		 						<td title="DIFERENCIA VENTA CAPTURADA/CALCULADA">@{{FuncionformateardosDecimales(datosProductos.SALIDAD)}}</td>
								<!-- Venta generada por inventario capturado -->
		 						<td title="VENTA CALCULADA POR EL SISTEMA"  v-if='datosProductos.cantidadventa'>@{{FuncionformateardosDecimales(datosProductos.cantidadventa)}}</td>
		 						<td title="VENTA CALCULADA POR EL SISTEMA"  v-else>0.00</td>
								 <!-- Venta generada por venta capturada -->
								<td title="VENTA GENERADA POR EXISTENCIA CAPTURADA">@{{FuncionCalcularOperacionVentaCapturada(datosProductos.Inicial,datosProductos.ENTRADA,datosProductos.SALIDAM,datosProductos.SALIDAD,datosProductos.cantidadventa,datosProductos.SALIDADUSUARIO)}}</td>
								<!-- Existencia por venta calculada -->
		 						<td title="EXISTENCIA FINAL CALCULADA">@{{FuncionOperacionInventario(datosProductos.Inicial,datosProductos.ENTRADA,datosProductos.SALIDAM,datosProductos.SALIDAD,datosProductos.cantidadventa)}}</td>
								<!-- Existencia por venta Capturada -->
		 						<td title="EXISTENCIA FINAL CAPTURADA" >@{{FuncionformateardosDecimales(datosProductos.SALIDADUSUARIO)}}</td>
								<!-- Diferencia venta calculada y venta capturada en Cantidad -->
		 						<td title="DIFERENCIA VENTA CAPTURADA/CALCULADA EN CANTIDAD" style="background:#D7F1D6">@{{FuncionformateardosDecimales(FuncionOperacionInventario(datosProductos.Inicial,datosProductos.ENTRADA,datosProductos.SALIDAM,datosProductos.SALIDAD,datosProductos.cantidadventa) - FuncionformateardosDecimales(datosProductos.SALIDADUSUARIO) )}}</td>
								<!-- Venta Calculada en valor -->
								<td title="VENTA CALCULADA EN VALOR MONETARIO">@{{FuncionformateardosDecimales(datosProductos.cProPrecio * datosProductos.cantidadventa)}}</td>
								<!-- venta capturada en valor -->
								<td title="VENTA CAPTURADA VALOR MONETARIO">@{{FuncionformateardosDecimales(datosProductos.cProPrecio *  FuncionCalcularOperacionVentaCapturadaefectivo(datosProductos.Inicial,datosProductos.ENTRADA,datosProductos.SALIDAM,datosProductos.SALIDAD,datosProductos.SALIDADUSUARIO))}}</td>
								<!-- Venta capturada -calculada -->
								<td title="DIFERENCIA VENTA CAPTURADA/CALCULADA" style="background:#D7F1D6" v-if='(datosProductos.cProPrecio * datosProductos.cantidadventa)-datosProductos.cProPrecio *  FuncionCalcularOperacionVentaCapturadaefectivo(datosProductos.Inicial,datosProductos.ENTRADA,datosProductos.SALIDAM,datosProductos.SALIDAD,datosProductos.SALIDADUSUARIO) >0'> @{{(datosProductos.cProPrecio * datosProductos.cantidadventa)-datosProductos.cProPrecio *  FuncionCalcularOperacionVentaCapturadaefectivo(datosProductos.Inicial,datosProductos.ENTRADA,datosProductos.SALIDAM,datosProductos.SALIDAD,datosProductos.SALIDADUSUARIO)}}</td>
								<td title="DIFERENCIA VENTA CAPTURADA/CALCULADA EN VALOR MONETARIO" style="background:#D7F1D6" v-else>0.00</td>
								<!-- Existente en el sistema -->
								<td title="EXISTENCIA CALCULADA EN VALOR MONETARIO">@{{FuncionformateardosDecimales(datosProductos.cProPrecio * FuncionOperacionInventario(datosProductos.Inicial,datosProductos.ENTRADA,datosProductos.SALIDAM,datosProductos.SALIDAD,datosProductos.cantidadventa) )}}</td>
								<!-- Existente venta por captura -->
								<td  title="EXISTENCIA CAPTURADA EN VALOR MONETARIO">@{{ FuncionformateardosDecimales(datosProductos.cProPrecio * datosProductos.SALIDADUSUARIO)}}</td>
								<!-- diferencias de existencia	 existencia calculada y autogeenerada-->
								<td  v-if='(datosProductos.cProPrecio * datosProductos.SALIDADUSUARIO)-(datosProductos.cProPrecio * FuncionOperacionInventario(datosProductos.Inicial,datosProductos.ENTRADA,datosProductos.SALIDAM,datosProductos.SALIDAD,datosProductos.cantidadventa)) >0' title="DIFERENCIA DE EXISTENCIA" style="background:#D7F1D6">@{{FuncionformateardosDecimales((datosProductos.cProPrecio * datosProductos.SALIDADUSUARIO)-(datosProductos.cProPrecio * FuncionOperacionInventario(datosProductos.Inicial,datosProductos.ENTRADA,datosProductos.SALIDAM,datosProductos.SALIDAD,datosProductos.cantidadventa)))}}</td>
								<td  v-else title="DIFERENCIA DE EXISTENCIA" style="background:#D7F1D6;">0.00</td>

		 					</tr>
		 				</template>
		 			</tbody>
		 		</table>
		 	</div>
		 	</template>
		 	<template v-if='_vIMensaje'>
		 		<template v-if='_vIMensaje == 1'>
		 		<span>Resultados no encontrados.</span>
		 	</template>
		 	</template>

		 </div>
	</div>
	</template>
	</template>
	<template v-else>
		<div><p><small>Por favor finaliza las capturas de entradas,salidas y reporte final para poder generar un reporte.</small></p></div>
	</template>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-Inventario.js"></script>
@endsection
