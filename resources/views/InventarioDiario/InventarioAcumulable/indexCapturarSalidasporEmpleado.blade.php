@extends('Master')
@section('titulo',' CAPTURAR REPORTE FINAL')
@section('content')
<style>
	#formulario:hover {
		background-color: #BCD9D7 !important; 

	}



</style>
<div id="IdInventarioInicial">
      <template v-if='vSCapturaFinalizadaFinal == 0'>
	<div class="row">
		<div class=" col-lg-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">INGRESAR PRODUCTO</h3>
                              <button class="btn btn-warning btn pull-right" @click='FuncionConfirmarFinalizarCapturaUsuario'>Finalizar inventario</button>
            			</div>
            			<!-- /.box-header -->
            			<div class="box-body">
            				<div class=col-lg-4>
            					<form @submit.prevent="FuncionBuscarProducto()">
            						<div class="form-group">
            							<input type="text" class="form-control" id="InputBuscarProducto" v-model='vProductoBuscar' required="" title="escanear código" placeholder="Ingrese código del producto">
            						</div>
            						<div class="form-group">
            							<button class="btn btn-default btn-block">BUSCAR</button>
            						</div>
            					</form>
            				</div>
            				<div class="col-lg-8">
            					<div class="panel panel-default">
            						<div class="panel-heading">Información</div>
            						<div class="panel-body">
            							<template v-if='dDatosProducto'> 
            								
            								<form @submit.prevent="FUncionIngresarSalidasPorUsuario(dDatosProducto[0])">
            									<table class="table table-hover">
            										<tr><td><small>CODIGO </small><strong>@{{dDatosProducto[0].cProInterCodigo}}</strong></td>
            											<td><small>DESCRIPCIÓN </small><strong>@{{dDatosProducto[0].cProDesTicket}}</strong></td>
            											<td><small>CANTIDAD A AGREGAR </small> <input type="number" class="form-control" v-model='dDatosProducto[0].cProCantidad' id="inputCantidadEntrada" required="" placeholder="Ingrese una cantidad"></td>
            										</tr>
            										<tr>
            											<td colspan="3"><button class="btn btn-primary btn pull-right" type="submit">AGREGAR</button></td>
            										</tr>
            									</table>
            								</form>
            							</template>
            						</div>
            					</div>
            				</div>
            			</div>
            			<!-- /.box-body -->
            			<div class="box-footer clearfix">

            			</div>
            			<!-- /.box-footer -->
            		</div>

            	</div>
            </div>
            <div class="row">
            	<div class="col-lg-12">
            		<div class="box box-default">
            			<div class="box-header with-border">
            				<h3 class="box-title">INVENTARIO INICIAL</h3>

            			</div>
            			<!-- /.box-header -->
            			<div class="box-body">
            				<div class="table-responsive">
            					<table class="table no-margin">
            						<thead>
            							<th>Código producto</th>
            							<th>Descripción</th>
            							<th>Cantidad</th>
            							<th>Status</th>
            							<th>Acciones</th>
            						</thead>
            						<tbody>
            							<template v-for='data in vSCapturaFinalUsuario'>
            								<tr>
            									<td>@{{data.cProInterCodigo}}</td>
            									<td>@{{data.cProDesTicket}}</td>
            									<td>@{{data.cantidad}}</td>
            									<td v-if='data.tInvIniStatus ==4'><span class="label label-success">Inventario final</span></td>
            									<td v-if='vSCapturaFinalizadaFinal == 0'><button class="btn btn-primary" @click='FuncionAbrirModalModificarSalidaReproteUsuario(data)'><i class="fa fa-edit"></i></button> <button class="btn btn-primary" @click='FuncionmodalEliminarSalidaUsuario(data)'> <i class="fa fa-trash"></i></button></td>
                                                                  <td v-else> Opciones restringidas</td>
            								</tr>
            							</template>
            						</tbody>
            					</table>
            				</div>
            			</div>
            			<!-- /.box-body -->
            			<div class="box-footer clearfix">

            			</div>
            			<!-- /.box-footer -->
            		</div>

            	</div>
            </div>
            </template>
            <template v-else>
                  <div> <p><small>Captura final de usuario Finalizada</small></p></div>
            </template>
            @include('InventarioDiario.InventarioAcumulable._modaleliminarSalida')
            @include('InventarioDiario.InventarioAcumulable._modalmodificarSalida')
            @include('InventarioDiario.InventarioAcumulable._modalfinalizarCapturaFinal')
            @include('InventarioDiario.InventarioAcumulable._modaleliminarSalidasPorUsuario')
            @include('InventarioDiario.InventarioAcumulable._modalModificarReporteFinalUsuario')
</div>

        @endsection
        @section('scripts')
        <script src="/js/axios.min.js"></script>
        <script src="/js/vue.js"></script>
        <script src="/vuejs/app-Inventario.js"></script>
        @endsection
