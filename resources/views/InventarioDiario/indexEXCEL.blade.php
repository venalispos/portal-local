<?php 
$fecha="";
$fecha = date('d/m/Y H:m');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<div>
				
	<table width="100%" style="font-family:Courier;">
				<tr>
					<th  align="center" colspan="3" style=" font-family:Courier;font-style: bold; font-size:27px; text-align: center; ">
						@php print_r($info_encabezado['empresaSeleccionada'][0]['cConEmpresa']); @endphp
					</th>
				</tr>	
					<tr>
						<td align="left" width="180" style="font-family:Courier; font-size:20px" >
							@php print_r($info_encabezado['empresaSeleccionada'][0]['cConSucursal']); @endphp 
						</td>
						<td  align="center" style="font-family:Courier; font-size:26px; text-align: center; ">
							REPORTE INVENTARIO DIARIO
						</td>
						<td style="font-family:Courier ; font-size:18px"  width="180">{{$fecha}}</td>
					</tr>
					<tr>
						<td></td>
						<td align="center" style="font-family:Courier ; font-size:26px;" >
							{{$info_encabezado['fecha']}}</td>
						<td>
						</td>
					</tr>
					<tr >
						<td colspan="3" style="padding-bottom: 15px;"><hr></td>
					</tr>
			</table>
	</div>
	<div>
		<table width="100%" style="font-family: courier; ">
			<tr>
				<th width="20%" align="left"><br>CODIGO|</th>
				<th width="40%"><br>DESCRIPCION|</th>
				<th align="right" style="text-align: right;"><br>INICIAL|</th>
				<th align="right" style="text-align: right;">CANTIDAD <br> VENTA|</th>
				<th align="right" style="text-align: right;"><br>DIFERENCIA|</th>

			</tr>
			
				@foreach($dataGPDF['productos'] as $producto )
				@if($producto['DIFERENCIA'] != NULL )
				<tr style="background-color: #EDE125">
				@else
				<tr>	
				@endif
				
				<td width="20%">{{$producto['cProInterCodigo']}}</td>
				<td  width="40%">{{$producto['cProDesTicket']}}</td>
				<td align="right" style="text-align: right;">   <?php echo number_format($producto['tInvIniValorMinimo'],2) ?></td>
				<td  align="right" style="text-align: right;"><?php echo number_format($producto['cHisProCantidad'],2) ?></td>
				@if($producto['DIFERENCIA'] != NULL )
				<td  align="right" style="text-align: right;"><?php echo number_format($producto['DIFERENCIA'],2) ?></td>
				@endif
				
				@if($producto['DIFERENCIA'] == NULL && $producto['cHisProCantidad'] == 0 && $producto['tInvIniValorMinimo'] != 0 )
				<td align="right"> <?php echo number_format($producto['tInvIniValorMinimo'],2) ?></td>
				@endif
				@if($producto['DIFERENCIA'] == NULL && $producto['cHisProCantidad'] != 0 && $producto['tInvIniValorMinimo']== 0  )
				<td align="right"><?php echo number_format($producto['cHisProCantidad'],2) ?></td>
				@endif
				@if($producto['DIFERENCIA'] == NULL && $producto['cHisProCantidad'] == 0 && $producto['tInvIniValorMinimo']== 0  )
				<td align="right">0.00</td>
				@endif
			</tr>
				@endforeach
		</table>
	</div>
</body>
</html>