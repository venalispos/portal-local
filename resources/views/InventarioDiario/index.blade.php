@extends('Master')
@section('titulo','Inventario Inicial')
@section('content')
<style>
#formulario:hover {
	background-color: #BCD9D7 !important;

}
</style>
<div id="IdInventarioInicial">
	<template v-if='statusInventario == 1'>
	<div class="row">
		<form @submit.prevent="buscarproductoconFiltros">
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="form-group">
					SELECCIONE UNA CATEGORIA
					<select name="" id="" class="form-control" v-model='Categoriaseleccionada' @change="cambiaropcionasubcategoria">
						<option value="0">TODAS</option>
						<template v-for='datacategoria in dataCatagorias' >
							<option :value="datacategoria.cCatCodigo">@{{datacategoria.cCatDescripcion}}</option>
						</template>
					</select>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="form-group">
					SELECCIONE UNA SUBCATEGORIA
					<select name="" id="" class="form-control" v-model='subcategoriaseleccionada'>
						<option value="0">TODAS</option>
						<template v-for='datasubcategoria in dataSubcategorias' v-if='datasubcategoria.cSCatPadre == Categoriaseleccionada'>
							<option :value="datasubcategoria.cSCatCodigo">@{{datasubcategoria.cSCatDescripcion}}</option>
						</template>
					</select>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="form-group">
					<br>
					<button class="btn btn-default"> <i class="fa fa-search"></i> BUSCAR</button>
				</div>
			</div>
		</form>
		<div class="col-lg-3">
			<br>
			<button class="btn btn-primary pull-right" @click='modalconfirmacionfinalizarinventario'>FINALIZAR INVENTARIO <i class="fa fa-arrow-right"></i></button>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Productos a inventariar </div>
				<div class="panel-body">
					<template v-for='data in dataproductos'>
						<form @submit.prevent="agregarItemtoinventario(data)" id="formulario">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
										@{{data.cProInterCodigo}}- @{{data.cProDesTicket}}
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<input type="number" v-model='data.cantidad' class="form-control">
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
										<button class="btn btn-default btn pull-right" :disabled='disableButton'><i class="fa fa-arrow-right"  aria-hidden="true"></i></button>
									</div>
								</div>
							</div>
						</form>
					</template>
				</div>
				<!-- <div class="panel-footer"><button class="btn btn-warning">MOVER TODOS</button></div> -->
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Productos con inventario</div>
				<div class="panel-body">
					<template v-for='productosinventariados in dataProductosCambiados'>
						<form @submit.prevent="eliminarproductodeinventario(productosinventariados)">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<button class="btn btn-danger btn pull-left"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										@{{productosinventariados.cProInterCodigo}}- @{{productosinventariados.cProDesTicket}}
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<input type="number" v-model='productosinventariados.tInvIniValorMinimo' disabled="" class="form-control">
									</div>
								</div>
							</div>
						</form>
					</template>
				</div>
				<div class="panel-footer"></div>
			</div>
		</div>
	</div>
	</template>
	<template v-else>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<span>Solo puedes llenar el inventario una ves al día</span>
			</div>
		</div>
	</template>

	@include('InventarioDiario._modalConfirmarIngresarInventario')
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-InventarioInicial.js"></script>
@endsection
