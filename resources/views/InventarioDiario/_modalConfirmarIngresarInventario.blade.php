<form  @submit.prevent="terminarInventarioDiario">
    {{ csrf_field() }}
    <div class="modal fade" id="finalizarinventario" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" >&times;</button>
                    <h4><strong>FINALIZAR INVENTARIO DEL DÍA</strong></h4>
                    <div id="mensaje_validacion" class="alert alert-default" role="alert">¿Seguro que deséa finalizar el inventario diario?</div>
                </div >
                <div class="modal-body">

              </div>
              <div class="modal-footer">
               <div class="pull-right">
                <button class="btn btn-primary" type="submit"   >Si, Deseo continuar</button>
            </div>
        </div>
    </div>
</div>
</div>
</form>