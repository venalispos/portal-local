<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<style>
	@font-face {
		font-family: 'Ubuntu';
		src: {{ resource_path('assets/fonts/ubuntu/ubuntu-regular-webfont.ttf') }} format('truetype');
		font-weight: normal;
		font-style: normal;

	}

	@font-face {
		font-family: 'Ubuntu';
		src: {{ resource_path('assets/fonts/ubuntu/ubuntu-medium-webfont.ttf') }} format('truetype');
		font-weight: bold;
		font-style: normal;

	}
	body {


		font-family: 'courier';
	}
</style>
</head> 
<body > 
	<table width="100%"  >
		
		@foreach($dataGPDF['categorias'] as $categoria)

		<tr>
			<td><?php echo "<p  style='font-weight:bold !important; font-size:23px !important;'>".$categoria['cCatDescripcion']."</p>"; ?></td>
			 
			<table    width="100%" cellpadding="0" cellspacing="0">
				@foreach($dataGPDF['subcategorias'] as $subcategoria) 
				@if( $subcategoria['cSCatPadre'] == $categoria['cCatCodigo'])
				<tr>
					<td width="25%"><strong><?php echo '<p style="font-style: bold !important; font-size:18px !important;padding-left: 2%; ">'.$subcategoria["cSCatDescripcion"].'</p>'; ?></strong></td>
					<table width="100%" >
						<thead>
							<?php echo "<th width='50%' align='left'  style='padding-left: 5%'>DESCRIPCION</th>"; ?>
							<th width="10%" style="text-align: right;">CANTIDAD</th>
							<th width="15%" style="text-align: right;">PORCENTAJE SUBCATEGORIA</th> 
							<th width="15%" style="text-align: right;">PORCENTAJE CATEGORIA</th>
							<th width="15%" style="text-align: right;">PORCENTAJE GENERAL</th>
						</thead> 
						<tbody>
							@foreach($dataGPDF['productos'] as $producto)
							@if($subcategoria['cSCatCodigo']== $producto['cHisProSubCategoria'])
							<tr>
								<?php echo "<td width='50%' >".$producto['cHisProDesTicket']."</td>"; ?>
								<td width="10%" style="text-align: right;"><?php  echo number_format($producto['cantidad'],2); ?></td>
								@foreach($dataGPDF['totalsubcategorias'] as $totalsubcategoria)
								@if($subcategoria['cSCatCodigo'] == $totalsubcategoria['cHisProSubCategoria'])
								<td width="15%" style="text-align: right;"> <?php echo number_format(($producto['cantidad']/$totalsubcategoria['cantidad'])*100,2); ?> %</td>
								@endif
								@endforeach
								@foreach( $dataGPDF['totalcategorias'] as $totalcat)
								@if($totalcat['categoria'] == $producto['cHisProCategoria'])
								<td width="15%" style="text-align: right;"><?php echo number_format(($producto['cantidad']/$totalcat['cantidad'])*100,2); ?> %</td>
								@endif
								@endforeach  
								@foreach($dataGPDF['totalgeneral'] as $general)
								<td align="right"> <?php echo number_format(($producto['cantidad']/$general['cantidad'])*100,2); ?> %</td>
								@endforeach
							</tr>
							@endif
							@endforeach
						</tbody>
						<tr>
							<th style=" text-align: right;" width="45%">TOTAL {{$subcategoria['cSCatDescripcion']}} </th>
							@foreach($dataGPDF['totalsubcategorias'] as $totalcat)
							@if($totalcat['cHisProSubCategoria'] == $subcategoria['cSCatCodigo'])
							<td style="text-align: right;"><strong> <?php echo number_format($totalcat['cantidad'],2); ?> </strong></td>
							@foreach($dataGPDF['totalsubcategorias'] as $totalsubcat)
							@if($totalsubcat['cHisProSubCategoria'] == $subcategoria['cSCatCodigo'])
							<td width="15%" style="text-align: right;"><strong><?php echo number_format(($totalcat['cantidad']/ $totalsubcat['cantidad'])*100,2); ?> % </strong></td>
							@endif
							@endforeach

							@foreach($dataGPDF['totalcategorias']  as $totalcate)
							@if($totalcate['categoria'] == $subcategoria['cSCatPadre'])
							<td width="15%" style="text-align: right;"><strong><?php echo number_format(($totalcat['cantidad'] / $totalcate['cantidad'])*100,2); ?> % </strong></td>
							@endif
							@endforeach
							@foreach($dataGPDF['totalgeneral'] as $general)
							<td width="15%" style="text-align: right;"><strong><?php echo number_format(( $totalcat['cantidad'] / $general['cantidad'])*100,2); ?> % </strong></td>
							@endforeach
							@endif
							@endforeach
						</tr>
					</table>
				</tr>
				@endif
				@endforeach
			</table>
		</tr>
		<table width="100%">
			<tr>
			<th width="45%"><strong><?php echo '<p width="10%" style="font-style: bold !important; font-size:22px !important; text-align:left;">TOTAL '.$categoria['cCatDescripcion'].'</p>'; ?> </strong></th>
			@foreach($dataGPDF['totalcategorias'] as $totalcategoria)
			@if( $totalcategoria['categoria']  == $categoria['cCatCodigo'])
			<td  style="text-align: right;"><strong> <?php echo '<p width="90%" style="font-style: bold !important;text-align:left; font-size:23px !important; ">'.number_format($totalcategoria['cantidad'],2).'</p>'; ?></strong></td>
			@endif
			@endforeach
		</tr>
		</table>

		@endforeach
		<table width="50%"  align="right">
			<tr>
			<td style="background-color: beige;" width="30%" style="font-size:24px !important;"><strong>TOTAL GENERAL</strong></td>
			@foreach( $dataGPDF['totalgeneral'] as $general)
			<td  width="20%" style="background-color: beige;"><strong><?php echo '<p width="10%" style="font-style: bold !important; font-size:24px !important; text-align:left;background-color: beige;"> '.number_format($general['cantidad'],2).'</p>'; ?></strong></td>
			@endforeach
		</tr>
		</table>
	</table> 
</body>
</html>