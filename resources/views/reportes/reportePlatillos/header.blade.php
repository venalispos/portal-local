<?php
$fecha="";
$fecha = date('d/m/Y H:m');
?>
<!DOCTYPE html>
<html lang="en">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<div id = 'formatos' >

	<table width="100%" style="font-family:Courier;">
				<thead>
					<th  align="center" colspan="3" style=" font-family:Courier;font-style: bold; font-size:27px; text-align: center; ">
						@php print_r($datosencabezado['empresaSeleccionada'][0]['cConEmpresa']); @endphp
					</th>
				</thead>
				<tbody>
					<tr>
						<td align="left" width="180" style="font-family:Courier; font-size:20px" >
							@php print_r($datosencabezado['empresaSeleccionada'][0]['cConSucursal']); @endphp
						</td>
						<td  align="center" style="font-family:Courier; font-size:26px; text-align: center; ">
							REPORTE POR PLATILLO
						</td>
						<td style="font-family:Courier ; font-size:18px"  width="180">{{$fecha}}</td>
					</tr>
					<tr>
						<td></td>
						<td align="center" style="font-family:Courier ; font-size:26px;" >
							{{$datosencabezado['fecha']}}</td>
						<td>
						</td>
					</tr>
					<tr >
						<td colspan="3" style="padding-bottom: 15px;"><hr></td>
					</tr>
				</tbody>
			</table>
</div>
</body>
</html>
