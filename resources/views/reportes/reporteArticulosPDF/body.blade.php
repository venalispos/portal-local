<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
	<div>
		<table width="100%" style="font-family: courier; ">
			<tr>
				<td  width="25%" aign="left" style="text-align: left;"><strong>PRODUCTO</strong></td>
				<td width="15%" align="right" style="text-align: right;"><strong>CANTIDAD</strong></td>
				<td width="20%" align="right" style="text-align: right;"><strong>VENTA TOTAL CON IVA</strong></td>
				<td width="20%" align="right" style="text-align: right;"><strong>PORCENTAJE</strong></td>
			</tr>
			@foreach($dataGPDF['Articulos'] as $articulos)
			<tr>
				<td width="25%" aign="left" style="text-align: left;">{{$articulos['cProDescripcion']}}</td>
				<td width="15%" align="right" style="text-align: right;">@php echo number_format($articulos['cantidadProductos'],2) @endphp</td>
				<td width="20%" align="right" style="text-align: right;">@php echo number_format($articulos['precio'],2) @endphp</td>
				<?php 

				$porcentajeArtEfc=($articulos['precio']/$dataGPDF['totales'][0]['precio'])*100;
				?>
				<td width="20%" align="right" style="text-align: right;">@php echo number_format($porcentajeArtEfc,2) @endphp %</td>
			</tr>
			@endforeach
			<tr>
				<td width="20%" align="right" style="text-align: right;"><strong>TOTALES</strong></td>
				<td width="15%" align="right" style="text-align: right;"><strong>@php echo number_format($dataGPDF['totales'][0]['cantidadProductos'],2) @endphp</strong></td>
				<td width="20%" align="right" style="text-align: right;"><strong>@php echo number_format($dataGPDF['totales'][0]['precio'],2) @endphp</strong></td>
				<td width="20%" align="right" style="text-align: right;"><strong>100%</strong></td>
			</tr>
		</table>
	</div>
</body>
</html>   