@extends('Master')
@section('titulo','Reportes por productos')
@section('content')
<div id="reporteporticket">
	<div class="row">
		<div class="col-xs-12 ">
			<form @submit.prevent="generarReporteproductos()" >
				<div class="box box-primary">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Filtros de búsqueda</h3>
					</div>

					<div class="box-body">
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaDesde">Desde *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaDesde" v-model="fechaDesde" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaHasta">Hasta *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaHasta" v-model="fechaHasta" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<br>
							<button type="submit" class="btn btn-primary btn-sm ">Mostrar reporte</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 ">
			<div class="box box-primary">
				<div class="box-header with-border ">
					<h3 class="box-title">Reporte</h3>
					<div class="row">
						<template v-if='mostrarbotonDescarga'>
							<div class="col-md-4">
								<a class="btn btn-app" @click='descargarPDFproducto()'>
									<i class="fa fa-download" aria-hidden="true"></i> PDF
								</a>
								<a class="btn btn-app" @click='GenerarExcelproductos()'>
									<i class="fa fa-download" aria-hidden="true"></i> EXCEL
								</a>
							</div>
						</template>

					</div>
				</div>

				<div class="box-body">
					<div class="col-xs-12">
						<template v-if='reporteproductos'>
							<div class="box-body">
								<div class="table-responsive">
									<template v-for='(categoria, indexCategoria) in categorias_productos'>
										<table class="table table-hover">
											<thead>
												<tr>
													<td > <strong> <span style="font-size:21px;">@{{categoria}}</span></strong>
														<br>
														<div class="table-responsive">
															<table class="table table-hover" width="100%">
																<thead>
																	<tr>
																		<th>CÓDIGO</th>
																		<th width="30%">DESCRIPCIÓN</th>
																		<th width="15%" style="text-align: center;">CANTIDAD</th>
																		<th width="15%" style="text-align: right;">SUBTOTAL</th>
																		<th width="20%" style="text-align: right;">IVA</th>
																		<th width="20%" style="text-align: right;">TOTAL</th>
																	</tr>
																</thead>
																<tbody>
																	<template v-for='productos in reporteproductos' v-if='productos.COMPARAR==categoria'>
																		<tr>
																			<td>@{{productos.cHisProCodigo}}</td>
																			<td width="30%" >@{{productos.cProDescripcion}}</td>
																			<td width="15%" style="text-align: center;">@{{formateardosDecimales(productos.cantidad)}}</td>
																			<td width="15%" style="text-align: right;">@{{formatearMoneda(productos.subtotal)}}</td>
																			<td width="20%" style="text-align: right;">@{{formatearMoneda(productos.cHisProImp1)}}</td>
																			<td width="20%" style="text-align: right;">@{{formatearMoneda(productos.totalsuma)}}</td>
																		</tr>
																	</template>
																	<tr>
																			<th style="text-align: right;" colspan="2" align="right"><strong>TOTALES</strong></td>
																			<th style="text-align: center;" align="center">@{{formateardosDecimales(totalesCategorias[indexCategoria].Cantidad)}}</th>
																			<th style="text-align: right;"  align="right">@{{formatearMoneda(totalesCategorias[indexCategoria].subtotal)}}</th>
																			<th style="text-align: right;"  align="right">@{{formatearMoneda(totalesCategorias[indexCategoria].impuesto)}}</th>
																			<th style="text-align: right;"  align="right">@{{formatearMoneda(totalesCategorias[indexCategoria].total)}}</th>
																	</tr>
																</tbody>
															</table>
														</div>
													</td>
												</tr>
											</thead>
										</table>
									</template>
								</div>
							</div>
							<template v-for='(caja, index) in cajas'>
								<div class="box-body">
									<h1 align="center" style="font-size:21px;"><strong>RESUMEN @{{caja.cHisProSerieLiq}}</strong></h1>
									<div class="table-responsive">
										<table class="table table-hover" width="100%">
											<thead>
												<th width="15%" >CAJA</th>
												<th width="15%" >CATEGORIA</th>
												<th width="10%" style="text-align: center;">CANTIDAD</th>
												<th width="20%" style="text-align: right;">SUBTOTAL</th>
												<th width="20%" style="text-align: right;">IVA</th>
												<th width="20%" style="text-align: right;">TOTAL</th>
											</thead>
											<tbody>
												<template v-for='totales in totalesCategorias'>
													<template v-if="totales.cHisProSerieLiq == caja.cHisProSerieLiq">
													<tr>
														<td width="10%" >@{{totales.cHisProSerieLiq}}</td>
														<td width="10%" >@{{totales.cCatDescripcion}}</td>
														<td width="20%" style="text-align: center;">@{{formateardosDecimales(totales.Cantidad)}}</td>
														<td width="20%" style="text-align: right;">@{{formatearMoneda(totales.subtotal)}}</td>
														<td width="20%" style="text-align: right;">@{{formatearMoneda(totales.impuesto)}}</td>
														<td width="20%"  style="text-align: right;">@{{formatearMoneda(totales.total)}}</td>
													</tr>
												</template>
											</template>
												<template v-for='finales in totalFinal[index] '>
													<th width="20%" align="right" style="text-align: right;">TOTALES</td>
													<th></th>
													<th width="20%" style="text-align: center;">@{{finales.Cantidad}}</th>
													<th width="20%" style="text-align: right;">@{{formatearMoneda(finales.subtotal)}}</th>
													<th width="20%" style="text-align: right;">@{{formatearMoneda(finales.impuesto)}}</th>
													<th width="20%"  style="text-align: right;">@{{formatearMoneda(finales.total)}}</th>
												</template>
											</tbody>
										</table>
									</div>
								</div>
							</template>
						</template>
						<template v-else>
							<template v-if="isLoading">
								<p class="text-center">
									<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
									<span>Cargando...</span>
								</p>
							</template>
							<template v-else>
								<p class="text-center">
									<small>Complete los campos para mostrar un reporte1</small>
								</p>
							</template>
						</template>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-tables-2.min.js"></script>
<script src="/vuejs/app-repticket.js"></script>
@endsection
