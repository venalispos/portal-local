<?php 
$fecha="";
$fecha = date('d/m/Y H:m');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Document</title>
</head>
<body>
	<div>
		<table width="100%" style="font-family:Courier;">
			<tbody>
				<tr>
					<td  align="center" colspan="3" style=" font-family:Courier;font-style: bold; font-size:27px; text-align: center; ">
						@php print_r($info_encabezado['empresaSeleccionada'][0]['cConEmpresa']); @endphp
					</td>

					<td align="left" width="180" style="font-family:Courier; font-size:20px" >
						@php print_r($info_encabezado['empresaSeleccionada'][0]['cConSucursal']); @endphp 
					</td>
				</tr>
				<tr>
					<td>
						REPORTE POR ARTICULO
					</td>
					<td style="font-family:Courier ; font-size:18px"  width="180">{{$fecha}}</td>
					<td></td>
					<td align="center" style="font-family:Courier ; font-size:26px;" >
					{{$info_encabezado['fecha']}}						</td>
					<td>
					</td>
				</tr>
				<tr >
					<td colspan="3" style="padding-bottom: 15px;"><hr></td>
				</tr>
			</tbody>
		</table>
	</div>
	<br>
	<br>
	<div>
		<table width="100%" style="font-family: courier;">
			<tr>
				<th  width="30%" aign="left" style="text-align: left;"><strong>PRODUCTO</strong></th>
				<th width="20%" align="right" style="text-align: right;"><strong>CANTIDAD</strong></th>
				<th width="20%" align="right" style="text-align: right;"><strong>PRECIO</strong></th>
				<th width="20%" align="right" style="text-align: right;"><strong>PORCENTAJE</strong></th>
			</tr>
			@foreach($dataGPDF['Articulos'] as $articulos)
			<tr>
				<td width="30%" aign="left" style="text-align: left;">{{$articulos['cProDescripcion']}}</td>
				<td width="20%" align="right" style="text-align: right;">@php echo number_format($articulos['cantidadProductos'],2) @endphp</td>
				<td width="20%" align="right" style="text-align: right;">@php echo number_format($articulos['precio'],2) @endphp</td>
				<?php 

				$porcentajeArtEfc=($articulos['precio']/$dataGPDF['totales'][0]['precio'])*100;
				?>
				<td width="20%" align="right" style="text-align: right;">@php echo number_format($porcentajeArtEfc,2) @endphp %</td>
			</tr>
			@endforeach
			<tr>
				<td width="20%" align="right" style="text-align: right;"><strong>TOTALES</strong></td>
				<td width="20%" align="right" style="text-align: right;"><strong>@php echo number_format($dataGPDF['totales'][0]['cantidadProductos'],2) @endphp</strong></td>
				<td width="20%" align="right" style="text-align: right;"><strong>@php echo number_format($dataGPDF['totales'][0]['precio'],2) @endphp</strong></td>
				<td width="20%" align="right" style="text-align: right;"><strong>100%</strong></td>
			</tr>
		</table>
	</div>
</body>
</html>