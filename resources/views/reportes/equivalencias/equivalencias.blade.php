@extends('Master')
@section('titulo','Reporte Equivalencias')
@section('content')
<div id ="reportEquivalencias">
  <div class="row">
    <div class="col-xs-12 col-sm-3">
      <form @submit.prevent="generarReporte" >
        <div class="box box-success">
          <div class="box-header with-border text-center">
            <h3 class="box-title">Filtros de búsqueda</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label for="fechaDesde">Desde *</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input name="fechaDesde" v-model="consulta.fechaDesde" type="date" class="form-control" required>
              </div>
            </div>
            <div class="form-group">
              <label for="fechaHasta">Hasta *</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input name="fechaHasta" v-model="consulta.fechaHasta" type="date" class="form-control" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Producto Primario</label>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" v-model="checked" @change="seleccionarTodo()">
                <label class="form-check-label" for="seleccionarTodo">&nbsp; Seleccionar todo</label>
              </div>
              <!--<select class="form-control" v-model="consulta.prodPrimarioSelected" required>
                <option value="0">TODOS</option>
                <option v-for="producto in listaProdPrimarios" :value="producto.cProPriFolio">@{{producto.cProPriDescripcion}}</option>
              </select>-->
              <multiselect
              v-model="consulta.prodPrimarioSelected"
              placeholder="Seleccionar producto..."
              label="cProPriDescripcion"
              track-by="cProPriFolio"
              :options="listaProdPrimarios"
              :multiple="true"
              :limit="3"
              :close-on-select="false"
              :allow-empty="false"
              select-label="Agregar"
              selected-label="Ya agregado"
              deselect-label="Quitar"
              open-direction="bottom">
              </multiselect>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" v-model="detallado" @change="" checked="true">
                <label class="form-check-label" for="Detallado">&nbsp; Detallado</label>
              </div>
            </div>
            <button type="submit" class="btn btn-success btn-sm btn-block">Mostrar reporte</button>
          </div>
        </div>
      </form>
    </div>
    <div class="col-xs-12 col-sm-9">
      <div class="box box-success">
        <div class="box-header with-border text-center">
          <h3 class="box-title">Reporte</h3>
          <div v-if="reporte_resultados != null" class="form-group">
            <button class="btn btn-app btn pull-right" @click=''>
              <i aria-hidden="true" class="fa fa-file-excel-o"></i>
              Excel
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-xs-12">
              <template v-if="reporte_resultados != null">
                <template v-if="detallado == true">
                  <template v-for = "producto in reporte_resultados">
                    <div class="box box_primary">
                      <div class="box-header with-border text-center">
                        <h4>@{{ producto.Descripcion }}</h4>
                      </div>
                      <div class="box-body">
                        <table class="table table-stripped table-hover">
                          <thead>
                            <tr>
                              <th style="text-align:right">Código</th>
                              <th style="text-align:right">Producto</th>
                              <th style="text-align:right">Equivalencia</th>
                              <th style="text-align:right">Cantidad</th>
                              <th style="text-align:right">Total</th>
                              <th style="text-align:right">Detalle</th>
                            </tr>
                          </thead>
                          <tbody>
                            <template v-if="producto.Producto == 0">
                              <tr>
                                <td colspan="6">Sin resultados</td>
                              </tr>
                            </template>
                            <template v-else>
                              <tr v-for="reporte in producto.Producto">
                                <td align="right"> @{{ reporte.cCodigoProd }} </td>
                                <td align="right"> @{{ reporte.cProDescripcion }} </td>
                                <td align="right"> @{{ formatoVenta(reporte.cEquivalencia) }} </td>
                                <td align="right"> @{{ formatoVenta(reporte.Cantidad) }} </td>
                                <td align="right"> @{{ formatoVenta(reporte.Total) }} </td>
                                <td align="right"> @{{ reporte.cProPriDetEquivalencia }} </td>
                              </tr>
                              <tr>
                                <td ></td>
                                  <template>
                                    <td colspan="3" align="right"><b>Total Final</b></td>
                                    <td align="right"><b> @{{ formatoVenta(producto.TotalFinal) }} </b></td>
                                  </template>
                              </tr>
                            </template>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </template>
                </template>
                <template>
                  <div class="col-sm-6">
                    <div class="box box-success">
                      <div class="box-header with-border text-center">
                        <h4>RESUMEN</h4>
                      </div>
                      <div class="box-body">
                        <table class = "table table-stripped table-hover">
                          <thead>
                            <th style="text-align:right">Producto Primario</th>
                            <th style="text-align:right">Total</th>
                          </thead>
                          <tbody>
                            <tr v-for = "totales in reporte_resultados">
                              <td align="right"><b>@{{totales.Descripcion}}</b></td>
                              <td align="right"><b>@{{formatoVenta(totales.TotalFinal)}}</b></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </template>
              </template>
              <template v-else>
                <template v-if="isLoading">
                  <p class="text-center">
                    <i class="fa fa-refresh fa-spin"></i><br>
                    <span>Cargando...</span>
                  </p>
                </template>
                <template v-else>
                  <p class="text-center">
                    <small>Complete los campos para mostrar el reporte</small>
                  </p>
                </template>
              </template>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-tables-2.min.js"></script>
<script src="/vuejs/vue-multiselect.js"></script>
<script src="/vuejs/reportes/app-reportEquivalencias.js"></script>
@endsection
