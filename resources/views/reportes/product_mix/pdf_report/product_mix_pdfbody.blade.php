<?php

$impuesto_1 = null;
$impuesto_2 = null;
$impuesto_3 = null;
$impuesto_4 = null;


$impuesto_1 = $reportes['Impuesto1'];
$impuesto_2 = $reportes['Impuesto2'];
$impuesto_3 = $reportes['Impuesto3'];
$impuesto_4 = $reportes['Impuesto4'];


 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
  @foreach($reportes['Categorias'] as $Categoria)
    <p style="font-family: courier;"><b>{{$Categoria['cCatDescripcion']}}</b></p>
      <table width="100%" style="font-family: courier; ">
        <thead>
            <th style = "text-align:right"> Item num</th>
            <th style = "text-align:right"> Item name</th>
            <th style = "text-align:right"> Num sold</th>
            <th style = "text-align:right"> Price sold</th>
            <th style = "text-align:right"> Amount</th>
            @if($impuesto_1)
              <th style = "text-align:right">{{$impuesto_1['cConImpuesto1Nombre']}}</th>
            @endif
            @if($impuesto_2)
              <th style = "text-align:right">{{$impuesto_2['cConImpuesto2Nombre']}}</th>
            @endif
            @if($impuesto_3)
              <th style = "text-align:right">{{$impuesto_3['cConImpuesto3Nombre']}}</th>
            @endif
            @if($impuesto_4)
              <th style = "text-align:right" >{{$impuesto_4['cConImpuesto4Nombre']}}</th>
            @endif
              <th style = "text-align:right"> Grs sales</th>
          </thead>
          <tbody>
            @foreach($Categoria['Productos'] as $product_mix)
                <tr>
                    <td align = "right">{{$product_mix['cHisProCodigo']}}</td>
                    <td align = "right">{{$product_mix['cHisProDesGeneral']}}</td>
                    <td align = "right">{{number_format($product_mix['Cantidad']  ,2,'.','')}}</td>
                    <td align = "right">{{number_format($product_mix['cHisProPrecio']  ,2,'.','')}}</td>
                    <td align = "right">{{number_format($product_mix['Subtotal']  ,2,'.','')}}</td>
                    @if($impuesto_1)
                    <td align = "right">{{number_format($product_mix['Impuesto1'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_2)
                    <td align = "right">{{number_format($product_mix['Impuesto2'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_3)
                    <td align = "right">{{number_format($product_mix['Impuesto3'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_4)
                    <td align = "right">{{number_format($product_mix['Impuesto4'] ,2,'.','')}}</td>
                    @endif

                    <td align = "right">{{number_format($product_mix['Total'], 2,'.','')}}</td>

                  </tr>
              @endforeach
                        <tr>
                            <td>  </td>
                            <td align = "right"><b>Totals</b></td>
                            <td align = 'right'><b>{{$Categoria['Totales']['Cantidad']}}</b></td>
                            <td align = 'right'><b> {{number_format($Categoria['Totales']['Precio'])}} </b></td>
                            <td align = 'right'><b> {{number_format($Categoria['Totales']['Subtotal'])}} </b></td>
                            @if($impuesto_1)
                            <td align = 'right'><b>{{number_format($Categoria['Totales']['Impuesto1'] ,2,'.','')}}</b></td>
                            @endif
                            @if($impuesto_2)
                            <td align = 'right'><b>{{number_format($Categoria['Totales']['Impuesto2'] ,2,'.','')}}</b></td>
                            @endif
                            @if($impuesto_3)
                            <td align = 'right'><b>{{number_format($Categoria['Totales']['Impuesto3'] ,2,'.','')}}</b></td>
                            @endif
                            @if($impuesto_4)
                            <td align = 'right'><b>{{number_format($Categoria['Totales']['Impuesto4'] ,2,'.','')}}</b></td>
                            @endif

                            <td align = 'right'><b>{{number_format($Categoria['Totales']['Total'] ,2,'.','')}}</b></td>
                        </tr>
              </tbody>
            </table>
             @endforeach

             @if($reportes['TotalesSinCategorias'] != 0)
    <p style="font-family: courier;"><b>WITHOUT CATEGORY</b></p>
      <table width="100%" style="font-family: courier; ">
        <thead>
            <th style = "text-align:right"> Item num</th>
            <th style = "text-align:right"> Item name</th>
            <th style = "text-align:right"> Num sold</th>
            <th style = "text-align:right"> Price sold</th>
            <th style = "text-align:right"> Amount</th>
            @if($impuesto_1)
              <th style = "text-align:right">{{$impuesto_1['cConImpuesto1Nombre']}}</th>
            @endif
            @if($impuesto_2)
              <th style = "text-align:right">{{$impuesto_2['cConImpuesto2Nombre']}}</th>
            @endif
            @if($impuesto_3)
              <th style = "text-align:right">{{$impuesto_3['cConImpuesto3Nombre']}}</th>
            @endif
            @if($impuesto_4)
              <th style = "text-align:right" >{{$impuesto_4['cConImpuesto4Nombre']}}</th>
            @endif
              <th style = "text-align:right"> Grs sales</th>
          </thead>
          <tbody>
            @foreach($reportes['SinCategoria'] as $product_mix)
                <tr>
                    <td align = "right">{{$product_mix['cHisProCodigo']}}</td>
                    <td align = "right">{{$product_mix['cHisProDesGeneral']}}</td>
                    <td align = "right">{{number_format($product_mix['Cantidad']  ,2,'.','')}}</td>
                    <td align = "right">{{number_format($product_mix['cHisProPrecio']  ,2,'.','')}}</td>
                    <td align = "right">{{number_format($product_mix['Subtotal']  ,2,'.','')}}</td>
                    @if($impuesto_1)
                    <td align = "right">{{number_format($product_mix['Impuesto1'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_2)
                    <td align = "right">{{number_format($product_mix['Impuesto2'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_3)
                    <td align = "right">{{number_format($product_mix['Impuesto3'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_4)
                    <td align = "right">{{number_format($product_mix['Impuesto4'] ,2,'.','')}}</td>
                    @endif

                    <td align = "right">{{number_format($product_mix['Total'], 2,'.','')}}</td>

                  </tr>
              @endforeach
                        <tr>
                            <td>  </td>
                            <td align = "right"><b>Totals</b></td>
                            <td align = 'right'><b>{{$reportes['TotalesSinCategorias']['Cantidad']}}</b></td>
                            <td align = 'right'><b> {{number_format($reportes['TotalesSinCategorias']['Precio'])}} </b></td>
                            <td align = 'right'><b> {{number_format($reportes['TotalesSinCategorias']['Subtotal'])}} </b></td>
                            @if($impuesto_1)
                            <td align = 'right'><b>{{number_format($reportes['TotalesSinCategorias']['Impuesto1'] ,2,'.','')}}</b></td>
                            @endif
                            @if($impuesto_2)
                            <td align = 'right'><b>{{number_format($reportes['TotalesSinCategorias']['Impuesto2'] ,2,'.','')}}</b></td>
                            @endif
                            @if($impuesto_3)
                            <td align = 'right'><b>{{number_format($reportes['TotalesSinCategorias']['Impuesto3'] ,2,'.','')}}</b></td>
                            @endif
                            @if($impuesto_4)
                            <td align = 'right'><b>{{number_format($reportes['TotalesSinCategorias']['Impuesto4'] ,2,'.','')}}</b></td>
                            @endif

                            <td align = 'right'><b>{{number_format($reportes['TotalesSinCategorias']['Total'] ,2,'.','')}}</b></td>
                        </tr>
              </tbody>
            </table>
             @endif
             <p style="font-family: courier;"><b>SUMMARY</b></p>
      <table width="100%" style="font-family: courier; ">
        <thead>
            <th style = "text-align:right"> Category</th>
            <th style = "text-align:right"> Num sold</th>
            <th style = "text-align:right"> Price sold</th>
            <th style = "text-align:right"> Amount</th>
            @if($impuesto_1)
              <th style = "text-align:right">{{$impuesto_1['cConImpuesto1Nombre']}}</th>
            @endif
            @if($impuesto_2)
              <th style = "text-align:right">{{$impuesto_2['cConImpuesto2Nombre']}}</th>
            @endif
            @if($impuesto_3)
              <th style = "text-align:right">{{$impuesto_3['cConImpuesto3Nombre']}}</th>
            @endif
            @if($impuesto_4)
              <th style = "text-align:right" >{{$impuesto_4['cConImpuesto4Nombre']}}</th>
            @endif
              <th style = "text-align:right"> Total</th>
          </thead>
          <tbody>
            @foreach($reportes['Categorias'] as $Categoria)
                <tr>
                    <td align = "right">{{$Categoria['cCatDescripcion']}}</td>
                    <td align = "right">{{number_format($Categoria['Totales']['Cantidad']  ,2,'.','')}}</td>
                    <td align = "right">{{number_format($Categoria['Totales']['Precio']  ,2,'.','')}}</td>
                    <td align = "right">{{number_format($Categoria['Totales']['Subtotal']  ,2,'.','')}}</td>
                    @if($impuesto_1)
                    <td align = "right">{{number_format($Categoria['Totales']['Impuesto1'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_2)
                    <td align = "right">{{number_format($Categoria['Totales']['Impuesto2'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_3)
                    <td align = "right">{{number_format($Categoria['Totales']['Impuesto3'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_4)
                    <td align = "right">{{number_format($Categoria['Totales']['Impuesto4'] ,2,'.','')}}</td>
                    @endif

                    <td align = "right">{{number_format($Categoria['Totales']['Total'], 2,'.','')}}</td>

                  </tr>
              @endforeach
              @if($reportes['TotalesSinCategorias'] != 0)
                <tr>
                    <td align = "right">WITHOUT CATEGORY</td>
                    <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Cantidad']  ,2,'.','')}}</td>
                    <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Precio']  ,2,'.','')}}</td>
                    <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Subtotal']  ,2,'.','')}}</td>
                    @if($impuesto_1)
                    <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Impuesto1'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_2)
                    <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Impuesto2'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_3)
                    <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Impuesto3'] ,2,'.','')}}</td>
                    @endif
                    @if($impuesto_4)
                    <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Impuesto4'] ,2,'.','')}}</td>
                    @endif

                    <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Total'], 2,'.','')}}</td>

                  </tr>
              @endif
                  <tr>
                      <td align = "right"><b>Totals</b></td>
                      <td align = 'right'><b>{{$reportes['CantidadFinal']}}</b></td>
                      <td align = 'right'><b> {{number_format($reportes['PrecioFinal'])}} </b></td>
                      <td align = 'right'><b> {{number_format($reportes['SubtotalFinal'])}} </b></td>
                      @if($impuesto_1)
                      <td align = 'right'><b>{{number_format($reportes['Impuestos1Final'] ,2,'.','')}}</b></td>
                      @endif
                      @if($impuesto_2)
                      <td align = 'right'><b>{{number_format($reportes['Impuestos2Final'] ,2,'.','')}}</b></td>
                      @endif
                      @if($impuesto_3)
                      <td align = 'right'><b>{{number_format($reportes['Impuestos3Final'] ,2,'.','')}}</b></td>
                      @endif
                      @if($impuesto_4)
                      <td align = 'right'><b>{{number_format($reportes['Impuestos4Final'] ,2,'.','')}}</b></td>
                      @endif

                      <td align = 'right'><b>{{number_format($reportes['Total'] ,2,'.','')}}</b></td>
                  </tr>
              </tbody>
            </table>
           </body>
    </html>
