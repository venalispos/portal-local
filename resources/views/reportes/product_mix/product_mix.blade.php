<?php
/*if ($Version=="01.0.0") {
  echo "correcto";
} else {
  echo "Actualiza";
}
*/
 ?>
@extends('Master')
@section('titulo','Product mix report')
@section('content')
<div id =   "sales_report">
  <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border text-center">
            Choose dates of report
          </div>
          <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="">Since</label>
                  <input type="date" name="" value="" class = "form-control" v-model = "date_since">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="">To</label>
                  <input type="date" name="" value="" class = "form-control" v-model = "date_to">
                </div>
            </div>
              <button type="button" name="button" class = "btn btn-primary pull-right btn-sm" @click = "obtener_reporte">
                <span class = "fa fa-search" ></span> Search
              </button>
          </div>
        </div>
      </div>
    </div>
    <div v-if = "Categorias != 0" class="row">
      <a @click = "generar_pdf_reporte" class="btn btn-app">
          <i class="fa fa-file"></i> PDF
      </a>
    </div>
    <template v-if = "Categorias != 0" class="row">
      <div class="row">
        <div class="col-md-12">
        <template v-for = "Categoria in Categorias">
            <div class="box box_primary">
              <div class="box-header with-border text-center">
                @{{Categoria.cCatDescripcion}}
              </div>
                <div class="box-body">
                  <table class = "table table-stripped table-hover">
                    <thead>
                      <th style = "text-align:right"> Item num</th>
                      <th style = "text-align:right"> Item name</th>
                      <th style = "text-align:right"> Num sold</th>
                      <th style = "text-align:right"> Price sold</th>
                      <th style = "text-align:right"> Amount</th>
                      <th style = "text-align:right" v-if = "Impuesto1" >@{{Impuesto1.cConImpuesto1Nombre}}</th>
                      <th style = "text-align:right" v-if = "Impuesto2" >@{{Impuesto2.cConImpuesto2Nombre}}</th>
                      <th style = "text-align:right" v-if = "Impuesto3" >@{{Impuesto3.cConImpuesto3Nombre}}</th>
                      <th style = "text-align:right" v-if = "Impuesto4" >@{{Impuesto4.cConImpuesto4Nombre}}</th>
                      <th style = "text-align:right"> Grs sales</th>
                    </thead>
                    <tbody>
                      <tr v-for = "(Producto, indexP) in Categoria.Productos">
                        <td align = "right">@{{Producto.cHisProCodigo}}</td>
                        <td align = "right">@{{Producto.cHisProDesGeneral}}</td>
                        <td align = "right">@{{formatoVenta(Producto.Cantidad)}}</td>
                        <td align = "right">@{{formatoVenta(Producto.cHisProPrecio)}}</td>
                        <td align = "right">@{{formatoVenta(Producto.Subtotal)}}</td>
                        <td align = "right" v-if = "Impuesto1">@{{formatoVenta(Producto.Impuesto1)}}</td>
                        <td align = "right" v-if = "Impuesto2">@{{formatoVenta(Producto.Impuesto2)}}</td>
                        <td align = "right" v-if = "Impuesto3">@{{formatoVenta(Producto.Impuesto3)}}</td>
                        <td align = "right" v-if = "Impuesto4">@{{formatoVenta(Producto.Impuesto4)}}</td>
                        <td align = "right">@{{formatoVenta(Producto.Total)}}</td>
                      </tr>
                      <tr>
                        <td ></td>
                          <template>
                            <td align = "right"><b>Totals</b></td>
                            <td align = 'right'><b>@{{formatoVenta(Categoria.Totales.Cantidad)}}</b></td>
                            <td align = 'right'><b>@{{formatoVenta(Categoria.Totales.Precio)}}</b></td>
                            <td align = 'right'><b> @{{formatoVenta(Categoria.Totales.Subtotal)}} </b></td>
                            <td align = 'right' v-if = "Impuesto1"><b>@{{formatoVenta(Categoria.Totales.Impuesto1)}}</b></td>
                            <td align = 'right' v-if = "Impuesto2"><b>@{{formatoVenta(Categoria.Totales.Impuesto2)}}</b></td>
                            <td align = 'right' v-if = "Impuesto3"><b>@{{formatoVenta(Categoria.Totales.Impuesto3)}}</b></td>
                            <td align = 'right' v-if = "Impuesto4"><b>@{{formatoVenta(Categoria.Totales.Impuesto4)}}</b></td>

                            <td align = 'right'><b>@{{formatoVenta(Categoria.Totales.Total)}}</b></td>
                          </template>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </template>
          <template v-if = "TotalesSinCategorias != 0">
            <div class="box box-warning">
              <div class="box-header with-border text-center">
               WITHOUT CATEGORY
              </div>
                <div class="box-body">
                  <table class = "table table-stripped table-hover">
                    <thead>
                      <th style = "text-align:right"> Item num</th>
                      <th style = "text-align:right"> Item name</th>
                      <th style = "text-align:right"> Num sold</th>
                      <th style = "text-align:right"> Price sold</th>
                      <th style = "text-align:right"> Amount</th>
                      <th style = "text-align:right" v-if = "Impuesto1" >@{{Impuesto1.cConImpuesto1Nombre}}</th>
                      <th style = "text-align:right" v-if = "Impuesto2" >@{{Impuesto2.cConImpuesto2Nombre}}</th>
                      <th style = "text-align:right" v-if = "Impuesto3" >@{{Impuesto3.cConImpuesto3Nombre}}</th>
                      <th style = "text-align:right" v-if = "Impuesto4" >@{{Impuesto4.cConImpuesto4Nombre}}</th>
                      <th style = "text-align:right"> Grs sales</th>
                    </thead>
                    <tbody>
                      <tr v-for = "Producto in SinCategoria">
                        <td align = "right">@{{Producto.cHisProCodigo}}</td>
                        <td align = "right">@{{Producto.cHisProDesGeneral}}</td>
                        <td align = "right">@{{formatoVenta(Producto.Cantidad)}}</td>
                        <td align = "right">@{{formatoVenta(Producto.cHisProPrecio)}}</td>
                        <td align = "right">@{{formatoVenta(Producto.Subtotal)}}</td>
                        <td align = "right" v-if = "Impuesto1">@{{formatoVenta(Producto.Impuesto1)}}</td>
                        <td align = "right" v-if = "Impuesto2">@{{formatoVenta(Producto.Impuesto2)}}</td>
                        <td align = "right" v-if = "Impuesto3">@{{formatoVenta(Producto.Impuesto3)}}</td>
                        <td align = "right" v-if = "Impuesto4">@{{formatoVenta(Producto.Impuesto4)}}</td>
                        <td align = "right">@{{formatoVenta(Producto.Total)}}</td>
                      </tr>
                      <tr>
                        <td ></td>
                          <template>
                            <td align = "right"><b>Totals</b></td>
                            <td align = 'right'><b>@{{formatoVenta(TotalesSinCategorias.Cantidad)}}</b></td>
                            <td align = 'right'><b>@{{formatoVenta(TotalesSinCategorias.Precio)}}</b></td>
                            <td align = 'right'><b> @{{formatoVenta(TotalesSinCategorias.Subtotal)}} </b></td>
                            <td align = 'right' v-if = "Impuesto1"><b>@{{formatoVenta(TotalesSinCategorias.Impuesto1)}}</b></td>
                            <td align = 'right' v-if = "Impuesto2"><b>@{{formatoVenta(TotalesSinCategorias.Impuesto2)}}</b></td>
                            <td align = 'right' v-if = "Impuesto3"><b>@{{formatoVenta(TotalesSinCategorias.Impuesto3)}}</b></td>
                            <td align = 'right' v-if = "Impuesto4"><b>@{{formatoVenta(TotalesSinCategorias.Impuesto4)}}</b></td>

                            <td align = 'right'><b>@{{formatoVenta(TotalesSinCategorias.Total)}}</b></td>
                          </template>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </template>
          <div class="box box-primary">
            <div class="box-header with-border text-center">
              SUMMARY
            </div>
            <div class="box box-body">
              <table class = "table table-stripped table-hover">
                <thead>
                  <th style = "text-align:right">Category</th>
                  <th style = "text-align:right">Num sold</th>
                  <th style = "text-align:right">Price sold</th>
                  <th style = "text-align:right">Amount</th>
                  <th style = "text-align:right" v-if = "Impuesto1" >@{{Impuesto1.cConImpuesto1Nombre}}</th>
                  <th style = "text-align:right" v-if = "Impuesto2" >@{{Impuesto2.cConImpuesto2Nombre}}</th>
                  <th style = "text-align:right" v-if = "Impuesto3" >@{{Impuesto3.cConImpuesto3Nombre}}</th>
                  <th style = "text-align:right" v-if = "Impuesto4" >@{{Impuesto4.cConImpuesto4Nombre}}</th>
                  <th  style = "text-align:right">Total</th>
                </thead>
                <tbody>
                  <tr v-for = "totales in Categorias">
                    <td align = 'right'><b>@{{totales.cCatDescripcion}}</b></td>
                    <td align = 'right'><b>@{{formatoVenta(totales.Totales.Cantidad)}}</b></td>
                    <td align = 'right'><b> @{{formatoVenta(totales.Totales.Precio)}} </b></td>
                    <td align = 'right'><b> @{{formatoVenta(totales.Totales.Subtotal)}} </b></td>
                    <td align = 'right' v-if = "Impuesto1"><b>@{{formatoVenta(totales.Totales.Impuesto1)}}</b></td>
                    <td align = 'right' v-if = "Impuesto2"><b>@{{formatoVenta(totales.Totales.Impuesto2)}}</b></td>
                    <td align = 'right' v-if = "Impuesto3"><b>@{{formatoVenta(totales.Totales.Impuesto3)}}</b></td>
                    <td align = 'right' v-if = "Impuesto4"><b>@{{formatoVenta(totales.Totales.Impuesto4)}}</b></td>
                    <td align = 'right'><b>@{{formatoVenta(totales.Totales.Total)}}</b></td>
                  </tr>
                  <tr v-if = "TotalesSinCategorias != 0">
                    <td align = 'right'><b>WITHOUT CATEGORY</b></td>
                    <td align = 'right'><b>@{{formatoVenta(TotalesSinCategorias.Cantidad)}}</b></td>
                    <td align = 'right'><b> @{{formatoVenta(TotalesSinCategorias.Precio)}} </b></td>
                    <td align = 'right'><b> @{{formatoVenta(TotalesSinCategorias.Subtotal)}} </b></td>
                    <td align = 'right' v-if = "Impuesto1"><b>@{{formatoVenta(TotalesSinCategorias.Impuesto1)}}</b></td>
                    <td align = 'right' v-if = "Impuesto2"><b>@{{formatoVenta(TotalesSinCategorias.Impuesto2)}}</b></td>
                    <td align = 'right' v-if = "Impuesto3"><b>@{{formatoVenta(TotalesSinCategorias.Impuesto3)}}</b></td>
                    <td align = 'right' v-if = "Impuesto4"><b>@{{formatoVenta(TotalesSinCategorias.Impuesto4)}}</b></td>
                    <td align = 'right'><b>@{{formatoVenta(TotalesSinCategorias.Total)}}</b></td>
                  </tr>
                  <tr>
                    <td align = 'right'><b>Totals</b></td>
                    <td align = 'right'><b>@{{formatoVenta(CantidadFinal)}}</b></td>
                    <td align = 'right'><b> @{{formatoVenta(PrecioFinal)}} </b></td>
                    <td align = 'right'><b> @{{formatoVenta(SubtotalFinal)}} </b></td>
                    <td align = 'right' v-if = "Impuesto1"><b>@{{formatoVenta(Impuestos1Final)}}</b></td>
                    <td align = 'right' v-if = "Impuesto2"><b>@{{formatoVenta(Impuestos2Final)}}</b></td>
                    <td align = 'right' v-if = "Impuesto3"><b>@{{formatoVenta(Impuestos3Final)}}</b></td>
                    <td align = 'right' v-if = "Impuesto4"><b>@{{formatoVenta(Impuestos4Final)}}</b></td>
                    <td align = 'right'><b>@{{formatoVenta(Total)}}</b></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </template>
    <template v-else>
      <template v-if="isLoading">
        <p class="text-center">
          <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
          <span>Cargando...</span>
        </p>
      </template>
      <template v-else>
        <p class="text-center">
          <small>Complete los campos para mostrar el reporte</small>
        </p>
      </template>
    </template>
</div>
@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-tables-2.min.js"></script>
<script src="/vuejs/reportes/app-product_mix.js"></script>
@endsection
