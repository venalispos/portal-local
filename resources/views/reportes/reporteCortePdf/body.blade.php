<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Document</title>
</head>
<style>

table {
  border-collapse: collapse;
  width: 60%;
}
td {
  width: 50% ;
}
th {
  height: 70px;
}
</style>
<body>
    <div>
        <h3>Caja</h3>
        <table>
            <tbody>
                <tr>
                    <td style="font-family:Courier;">EFECTIVO</td>
                    <td style="font-family:Courier;">${{number_format($dataGPDF['efectivo'], 2)}}</td>
                </tr>
                @foreach($dataGPDF['caja'] as $caja)
                <tr>
                    <td style="font-family:Courier;">{{$caja['cCatAgForDescripcion']}}</td>
                    <td style="font-family:Courier;">${{number_format($caja['suma'], 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                @foreach($dataGPDF['totalesysubtotales'] as $totales)
                <tr>
                    <td style="font-family:Courier;;">SUBTOTAL</td>
                    <td style="font-family:Courier;">${{number_format($totales['Subtotal'], 2)}}</td>
                </tr>
                <tr>
                    <td style="font-family:Courier;;">Impuesto</td>
                    <td style="font-family:Courier;">${{number_format($totales['Impuesto'], 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <td style="font-family:Courier;">TOTAL</td>
                    <td style="font-family:Courier;">${{number_format($dataGPDF['totalcaja'], 2)}}</td>
                </tr>
            </tbody>
        </table>
        <h3>Forma de Pago Ventas</h3>
        <table>
            <tbody>
                <tr>
                    <td style="font-family:Courier;">EFECTIVO</td>
                    <td style="font-family:Courier;">${{number_format($dataGPDF['efectivo'], 2)}}</td>
                </tr>
                @foreach($dataGPDF['formapago'] as $formapago)
                <tr>
                    <td style="font-family:Courier;">{{$formapago['categoriaformapago']['cCatForDesc']}}</td>
                    <td style="font-family:Courier;">${{number_format($formapago['Cantidad'])}}</td>
                </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                @foreach($dataGPDF['totalesysubtotales'] as $totales)

                <tr>
                    <td style="font-family:Courier;;">SUBTOTAL</td>
                    <td style="font-family:Courier;">${{number_format($totales['Subtotal'], 2)}}</td>
                </tr>
                <tr>
                    <td style="font-family:Courier;;">IMPUESTO</td>
                    <td style="font-family:Courier;">${{number_format($totales['Impuesto'], 2)}}</td>
                </tr>
                <tr>
                    <td style="font-family:Courier;;">TOTAL</td>
                    <td style="font-family:Courier;">${{ number_format($totales['Total'], 2)}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <h3>Descuentos y Cortesias</h3>
        <table>
            <tbody>
                <tr>
                    <td style="font-family:Courier;">COMENSALES</td>
                    <td style="font-family:Courier;">{{$dataGPDF['comensales']['CantidadClientes']}}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <br>
        <table>
            <tbody>
                <tr>
                    <td class="text-right">
                        <h4>Descuentos</h4>
                    </td>
                </tr>
                @foreach($dataGPDF['deducciones'] as $deducciones)
                <tr>

                    <td style="font-family:Courier;">{{ $deducciones['categorias']['cCatDescripcion'] }}</td>
                    <td style="font-family:Courier;">${{number_format($deducciones['Descuentos'], 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <td style="font-family:Courier;">TOTAL</td>
                    <td style="font-family:Courier;">${{number_format($dataGPDF['totaldescuentos']['Descuentos'], 2)}}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tbody>
                <tr>
                    <td class="text-right">
                        <h4>Cortesias</h4>
                    </td>
                </tr>
                @foreach($dataGPDF['deducciones'] as $deducciones)
                <tr>
                    <td style="font-family:Courier;">{{ $deducciones['categorias']['cCatDescripcion'] }}</td>
                    <td style="font-family:Courier;">${{number_format($deducciones['Cortesias'], 2)}}</td>
                </tr>
                @endforeach
                <tr>
                    <td style="font-family:Courier;">TOTAL</td>
                    <td style="font-family:Courier;">${{number_format($dataGPDF['totalcortesias']['Cortesias'], 2)}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>