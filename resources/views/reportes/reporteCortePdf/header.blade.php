<?php
$fecha = "";
$fecha = date('d/m/Y H:m');
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>
    <div id='formatos'>
        <table width="100%" style="font-family:Courier;">
            <thead>
                <th></th>
            </thead>
            <tbody>
                <tr>
                    <td align="center" style="font-family:Courier; font-size:26px; text-align: center; ">
                        Reporte Corte
                    </td>
                    <td style="font-family:Courier ; font-size:12px" width="30%">{{$fecha}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" style="font-family:Courier ; font-size:14px;">
                        {{$datosencabezado['fecha']}}
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-bottom: 15px;">
                        <hr>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>