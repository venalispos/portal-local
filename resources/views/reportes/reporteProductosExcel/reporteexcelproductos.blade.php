<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Document</title>
</head>
<body>
	<tr>
		<td>@php print_r($info_encabezado['empresaSeleccionada'][0]['cConEmpresa']); @endphp</td>
	</tr>
	<tr>
		<td>SUCURSAl</td>
		<td>@php print_r($info_encabezado['empresaSeleccionada'][0]['cConSucursal']); @endphp </td>
	</tr>
		<tr>
			<td>REPORTE DE VENTA POR PRODUCTOS</td>
		</tr>
		<tr>
			<td>FECHA {{$info_encabezado['fecha']}}</td>
		</tr>

	<tr></tr>
	<tr></tr>
		<br>
		@foreach($dataGPDF['categorias'] as $categoria)
				<br><tr><td>{{$categoria}}</td></tr>
							<tr>
								<td style="font-family:Courier;font-size:14px;">CODIGO</td>
								<td width="25%" style="font-family:Courier;font-size:14px;">DESCRIPCION</td>
								<td width="15%" style="text-align: center;font-family:Courier;font-size:14px;" ><strong>CANTIDAD</strong></td>
								<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;" ><strong>SUBTOTAL</strong></td>
								<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>IVA</strong></td>
								<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>TOTAL</strong></td>
							</tr>
							@foreach($dataGPDF['productos'] as $productos )
								@if($productos['COMPARAR']==$categoria)
									<tr>
										<td style="font-family:Courier;font-size:14px;">{{$productos['cHisProCodigo']}}</td>
										<td width="25%" style="font-family:Courier;font-size:14px;">{{$productos['cProDescripcion']}}</td>
										<td width="15%" style="text-align: center;font-family:Courier;font-size:14px;">@php echo number_format($productos['cantidad'],2) @endphp</td>
										<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;"> @php echo number_format($productos['subtotal'],2) @endphp</td>
										<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;"> @php echo number_format($productos['cHisProImp1'],2) @endphp</td>
										<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;"> @php echo number_format($productos['totalsuma'],2) @endphp</td>
									</tr>
								@endif
							@endforeach
							@foreach($dataGPDF['totalesCategorias'] as $totales)
								@if($totales['cCatDescripcion']==$categoria)
									<tr>
										<td colspan="2"  style="text-align: center;font-family:Courier;font-size:14px;"><strong>TOTALES </strong></td>
										<td   style="text-align: center;font-family:Courier;font-size:14px;"><strong>@php echo number_format($totales['Cantidad'],2) @endphp </strong> </td>
										<td  style="text-align: right;font-family:Courier;font-size:14px;"><strong>@php echo number_format($totales['subtotal'],2) @endphp </strong> </td>
										<td  style="text-align: right;font-family:Courier;font-size:14px;"><strong>@php echo number_format($totales['impuesto'],2) @endphp </strong></td>
										<td   style="text-align: right;font-family:Courier;font-size:14px;"><strong>@php echo number_format($totales['total'],2) @endphp </strong> </td>
									</tr>
								@endif
							@endforeach

		@endforeach

		<tr>
			<br><td align="center" style="font-family:Courier;font-size:21px;"><strong>RESUMEN</strong></td><br>
		</tr>
		<table class="table table-hover" width="100%">
			<tr>
				<td width="25%" style="font-family:Courier;font-size:14px;"><strong>CAJA</strong></td>
				<td width="40%" style="font-family:Courier;font-size:14px;"><strong>DESCRIPCION</strong></td>
				<td width="20%" style="text-align: center;font-family:Courier;font-size:14px;" ><strong>CANTIDAD</strong></td>
				<td width="20%" style="text-align: right;font-family:Courier;font-size:14px;" ><strong>SUBTOTAL</strong></td>
				<td width="20%" style="text-align: right;font-family:Courier;font-size:14px;" ><strong>IVA</strong></td>
				<td width="20%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>TOTAL</strong></td>
			</tr>
			<tbody>
				@foreach($dataGPDF['totalesCategorias'] as $totales)
					<tr>
						<td width="25%" style="font-family:Courier;font-size:14px;">{{$totales['cHisProSerieLiq']}}</td>
						<td width="40%" style="font-family:Courier;font-size:14px;">{{$totales['cCatDescripcion']}}</td>
						<td width="20%" style="text-align: center;font-family:Courier;font-size:14px;">@php echo number_format($totales['Cantidad'],2) @endphp</td>
						<td width="20%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($totales['subtotal'],2) @endphp</td>
						<td width="20%" style="text-align: center;font-family:Courier;font-size:14px;">@php echo number_format($totales['impuesto'],2) @endphp</td>
						<td width="20%"  style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($totales['total'],2) @endphp</td>
					</tr>
					@endforeach
					@foreach($dataGPDF['final'] as $totales)
					<tr>
						<td width="40%" style="font-family:Courier;font-size:14px;"> <strong>TOTALES </strong></td>
						<td></td>
						<td width="20%" style="text-align: center;font-family:Courier;font-size:14px;"> <strong>@php echo number_format($totales['Cantidad'],2) @endphp </strong></td>
						<td width="20%" style="text-align: right;font-family:Courier;font-size:14px;"> <strong>@php echo number_format($totales['subtotal'],2) @endphp </strong></td>
						<td width="20%" style="text-align: center;font-family:Courier;font-size:14px;"> <strong>@php echo number_format($totales['impuesto'],2) @endphp </strong></td>
						<td width="20%"  style="text-align: right;font-family:Courier;font-size:14px;"> <strong>@php echo number_format($totales['total'],2) @endphp </strong></td>
					</tr>
					@endforeach
			</tbody>

</body>
</html>
