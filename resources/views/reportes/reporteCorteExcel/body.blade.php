<p>Reporte Corte </p>
<table>
    <tr>
        <td><strong>FECHA</strong></td>
    </tr>
    <tr>
        <td>{{$info_encabezado['fecha']}}</td>
    </tr>
</table>
<br><br>
<table border="0" width="60%">
    <tbody>
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">EFECTIVO</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($dataGPDF['efectivo'], 2)}}</td>
        </tr>
        @foreach($dataGPDF['caja'] as $caja)
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">{{$caja['cCatAgForDescripcion']}}</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($caja['suma'], 2)}}</td>
        </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
        </tr>
        @foreach($dataGPDF['totalesysubtotales'] as $totales)
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left;">Subtotal</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{number_format($totales['Subtotal'], 2)}}</td>
        </tr>
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left;">Impuesto</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{number_format($totales['Impuesto'], 2)}}</td>
        </tr>
        @endforeach
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">Total</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($dataGPDF['totalcaja'], 2)}}</td>
        </tr>
    </tbody>
</table>
<h3>Forma de Pago Ventas</h3>
<table border="0" width="60%">
    <tbody>
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">EFECTIVO</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($dataGPDF['efectivo'], 2)}}</td>
        </tr>
        @foreach($dataGPDF['formapago'] as $formapago)
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">{{$formapago['categoriaformapago']['cCatForDesc']}}</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($formapago['Cantidad'], 2) }}</td>
        </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
        </tr>
        @foreach($dataGPDF['totalesysubtotales'] as $totales)
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left;">Subtotal</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{number_format($totales['Subtotal'], 2)}}</td>
        </tr>
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left;">Impuesto</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{number_format($totales['Impuesto'], 2)}}</td>
        </tr>
        @endforeach
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">Total</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($dataGPDF['totalformapago'], 2)}}</td>
        </tr>
    </tbody>
</table>
<h3>Descuentos y Cortesias</h3>
<table border="0" width="60%">
    <tbody>
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">Comensales</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">{{ $dataGPDF['comensales']['CantidadClientes']}}</td>
        </tr>
    </tbody>
</table>
<br>
<br>
<table border="0" width="60%">
    <tbody>
        <tr>
            <td class="text-right">
                <h4>Descuentos</h4>
            </td>
        </tr>
        @foreach($dataGPDF['deducciones'] as $deducciones)
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">{{ $deducciones['categorias']['cCatDescripcion'] }}</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($deducciones['Descuentos'], 2)}}</td>
        </tr>
        @endforeach
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">Total</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($dataGPDF['totaldescuentos']['Descuentos'], 2)}}</td>
        </tr>
    </tbody>
</table>
<br>
<table border="0" width="60%">
    <tbody>
        <tr>
            <td class="text-right">
                <h4>Cortesias</h4>
            </td>
        </tr>
        @foreach($dataGPDF['deducciones'] as $deducciones)
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">{{ $deducciones['categorias']['cCatDescripcion'] }}</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($deducciones['Cortesias'], 2)}}</td>
        </tr>
        @endforeach
        <tr>
            <td style="font-family:Courier;font-size:14px;text-align:left">Total</td>
            <td style="font-family:Courier;font-size:14px;text-align:left">${{ number_format($dataGPDF['totalcortesias']['Cortesias'], 2) }}</td>
        </tr>
    </tbody>
</table>