<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<tr>
		<td>@php print_r($info_encabezado['empresaSeleccionada'][0]['cConEmpresa']); @endphp</td>
	</tr>
	<tr>
		<td>SUCURSAl</td>
		<td>@php print_r($info_encabezado['empresaSeleccionada'][0]['cConSucursal']); @endphp </td>
	</tr>
	<p>Reporte de ventas por Ticket</p>
		<table>
			<tr><td><strong>FECHA</strong></td></tr>
			<tr>
				<td>{{$info_encabezado['fecha']}}</td>
			</tr>
		</table>
		<table>
			<tr>
				<td>TICKET</td>
				<td>FECHA</td>
				<td>MESA</td>
				<td>SUBTOTAL</td>
				<td>IVA</td>
				<td>TOTAL</td>
			</tr>
			@foreach($dataGPDF['reporte'] as $reporte)
			<tr>
				<td>{{$reporte->cHisMesTicket}}</td>
				<td>{{$reporte->cHisMesFechaRep}}</td>
				<td>{{$reporte->cHisMesNombre}}</td>
				<td>{{$reporte->cHisMesSubTotal}}</td>
				<td>{{$reporte->cHisMesTotalImpuesto1}}</td>
				<td>{{$reporte->cHisMesTotal}}</td>
			</tr>
			@endforeach	
			<tr>
				<td></td>
				<td></td>
				<td align="right"  style="text-align: right;font-family:Courier;font-size:14px;"><strong>TOTALES</strong></td>
				<td style="text-align: right;font-family:Courier;font-size:14px;"><strong> @php echo number_format($dataGPDF['subtotal'],2)  @endphp </strong></td>
				<td style="text-align: right;font-family:Courier;font-size:14px;"><strong>  @php echo number_format($dataGPDF['impuesto'],2)  @endphp </strong></td>
				<td style="text-align: right;font-family:Courier;font-size:14px;"><strong>  @php echo number_format($dataGPDF['totalTotales'],2)  @endphp </strong></td>
			</tr>
		</table>
		
</body>
</html>