
<tr>
		<td>@php print_r($info_encabezado['empresaSeleccionada'][0]['cConEmpresa']); @endphp</td>
	</tr>
	<tr>
		<td>@php print_r($info_encabezado['empresaSeleccionada'][0]['cConSucursal']); @endphp </td>
	</tr>
	<p>Reporte desglose </p>
		<table>
			<tr><td><strong>FECHA</strong></td></tr>
			<tr>
				<td>{{$info_encabezado['fecha']}}</td>
			</tr>
		</table>
		<br><br>

	@foreach($dataGPDF['mesas'] as $mesas)
	<table  width="100%" style="font-family: courier; ">
		<tr>
			<td colspan="5">TICKET # {{$mesas['cHisMesTicket']}}</td>
		</tr>
		<tr >
			<td>CANTIDAD</td>
			<td>CODIGO</td>
			<td align="left" style="text-align: left;">DESCRIPCION</td>
			<td align="right" style="text-align: right;">SUBTOTAL</td>
			<td align="right" style="text-align: right;">IMPUESTO</td>
			<td align="right" style="text-align: right;">TOTAL</td>
		</tr>
		@foreach($dataGPDF['productos'] as $productos)
			@if($productos['cHisProTicket'] == $mesas['cHisMesTicket'])
			<tr>
				<td width="20%" >{{$productos['cHisProCantidad']}}</td>
				<td width="20%" >{{$productos['cHisProCodigo']}}</td>
				<td width="50%">{{$productos['cHisProDesTicket']}}</td>
				<td width="10%" align="right" style="text-align: right;">@php echo number_format($productos['cHisProSubTotal'],2) @endphp</td>
				<td width="10%" align="right" style="text-align: right;">@php echo number_format($productos['cHisProImp1'],2) @endphp</td>
				@php
				$totalProd=$productos['cHisProImp1']+$productos['cHisProSubTotal'];
				@endphp
				<td width="10%" align="right" style="text-align: right;"> @php echo number_format($totalProd,2) @endphp</td>
			</tr>
			@endif
		@endforeach
		@foreach($dataGPDF['totalProductos'] as $total)

			@if($total['cHisProTicket'] == $mesas['cHisMesTicket'])
			<tr>

			<td colspan="3" align="right" style="text-align: right;"><strong>TOTAL</strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Subtotal'],2) @endphp </strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Impuesto'],2) @endphp </strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Total'],2) @endphp </strong></td>
			</tr>
			@endif

		@endforeach
		<tr>
			<th>FORMA DE PAGO</th>
			<th> MONTO</th>
		</tr>
		@foreach($dataGPDF['formasdepago'] as $pago)
			@if($pago['cHisForTicket'] == $mesas['cHisMesTicket'])
				@if($pago['cHisForId']==1)
				<tr>
					<td >EFECTIVO</td>
					<td style="text-align: center;">NA</td>
				</tr>
				@else
				<td>@php echo $pago['cHisForDes'] @endphp</td>
				<td style="text-align: center;">@php echo number_format($pago['cHisForCant'],2) @endphp</td>
				@endif
			@endif
		@endforeach
	</table>
	<br><br>
	@endforeach
