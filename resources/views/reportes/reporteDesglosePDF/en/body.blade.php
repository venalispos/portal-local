<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
	@foreach($dataGPDF['mesas'] as $mesas)
	<table class="table table-hover" width="100%" style="font-family: courier; ">
		<tr>

		</tr>
		<th align="left" colspan="5">TICKET # {{$mesas['cHisMesTicket']}}</th>
		<tr >
			<th align="left">QUANTITY</th>
			<th align="left">ITEM</th>
			<th align="left" style="text-align: left;">NAME</th>
			<th align="right" style="text-align: right;">SUBTOTAL</th>
			<th align="right" style="text-align: right;">TAX</th>
			<th align="right" style="text-align: right;">MB TAX</th>
			<th align="right" style="text-align: right;">TOTAL</th>
		</tr>
		@foreach($dataGPDF['productos'] as $productos)
		@if($productos['cHisProTicket'] == $mesas['cHisMesTicket'])
		<tr>
			<td width="10%" >{{$productos['cHisProCantidad']}}</td>
			<td width="10%" >{{$productos['cHisProCodigo']}}</td>
			<td width="40%">{{$productos['cHisProDesTicket']}}</td>
			<td align="right" style="text-align: right;">@php echo number_format($productos['cHisProSubTotal'],2) @endphp</td>
			<td align="right" style="text-align: right;">@php echo number_format($productos['cHisProImp2'],2) @endphp</td>
			<td align="right" style="text-align: right;">@php echo number_format($productos['cHisProImp3'],2) @endphp</td>
			@php
			$totalProd=$productos['cHisProImp1']+$productos['cHisProImp2']+$productos['cHisProImp3']+$productos['cHisProImp4']+$productos['cHisProSubTotal'];
			@endphp
			<td align="right" style="text-align: right;"> @php echo number_format($totalProd,2) @endphp</td>
		</tr>
		@endif
		@endforeach
		@foreach($dataGPDF['totalProductos'] as $total)
			@if($total['cHisProTicket'] == $mesas['cHisMesTicket'])
			<tr>
			<td colspan="3" align="right" style="text-align: right;"><strong>TOTAL</strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Subtotal'],2) @endphp </strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Impuesto2'],2) @endphp </strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Impuesto3'],2) @endphp </strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Total'],2) @endphp </strong></td>
			</tr>
			@endif
		@endforeach
		<tr>
			<th>PAYMENT METHODS</th>
			<th>AMOUNT</th>
		</tr>
		@foreach($dataGPDF['formasdepago'] as $pago)
			@if($pago['cHisForTicket'] == $mesas['cHisMesTicket'])
            <td>@php echo $pago['cHisForDes'] @endphp</td>
            <td style="text-align: center;">@php echo number_format($pago['cHisForCant'],2) @endphp</td>
			@endif
		@endforeach
	</table>
	<br><br><br><br>
	@endforeach
</body>
</html>
