<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
	@foreach($dataGPDF['mesas'] as $mesas)
	<table class="table table-hover" width="100%" style="font-family: courier; ">
		<tr>

		</tr>
		<th align="left" colspan="5">TICKET # {{$mesas['cHisMesTicket']}}</th>
		<tr >
			<th align="left">CANTIDAD</th>
			<th align="left">CODIGO</th>
			<th align="left" style="text-align: left;">DESCRIPCION</th>
			<th align="right" style="text-align: right;">SUBTOTAL</th>
			<th align="right" style="text-align: right;">IMPUESTO</th>
			<th align="right" style="text-align: right;">TOTAL</th>
		</tr>
		@foreach($dataGPDF['productos'] as $productos)
		@if($productos['cHisProTicket'] == $mesas['cHisMesTicket'])
		<tr>
			<td width="20%" >{{$productos['cHisProCantidad']}}</td>
			<td width="20%" >{{$productos['cHisProCodigo']}}</td>
			<td width="50%">{{$productos['cHisProDesTicket']}}</td>
			<td width="10%" align="right" style="text-align: right;">@php echo number_format($productos['cHisProSubTotal'],2) @endphp</td>
			<td width="10%" align="right" style="text-align: right;">@php echo number_format($productos['cHisProImp1'],2) @endphp</td>
			@php
			$totalProd=$productos['cHisProImp1']+$productos['cHisProSubTotal'];
			@endphp
			<td width="10%" align="right" style="text-align: right;"> @php echo number_format($totalProd,2) @endphp</td>
		</tr>
		@endif
		@endforeach
		@foreach($dataGPDF['totalProductos'] as $total)
			@if($total['cHisProTicket'] == $mesas['cHisMesTicket'])
			<tr>
			<td colspan="3" align="right" style="text-align: right;"><strong>TOTAL</strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Subtotal'],2) @endphp </strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Impuesto'],2) @endphp </strong></td>
			<td  align="right" style="text-align: right;"><strong>@php echo number_format($total['Total'],2) @endphp </strong></td>
			</tr>
			@endif
		@endforeach
		<tr>
			<th>FORMA DE PAGO</th>
			<th> MONTO</th>
		</tr>
		@foreach($dataGPDF['formasdepago'] as $pago)
			@if($pago['cHisForTicket'] == $mesas['cHisMesTicket'])
				@if($pago['cHisForId']==1)
				<tr>
					<td >EFECTIVO</td>
					<td style="text-align: center;">NA</td>
				</tr>
				@else
				<td>@php echo $pago['cHisForDes'] @endphp</td>
				<td style="text-align: center;">@php echo number_format($pago['cHisForCant'],2) @endphp</td>
				@endif
			@endif
		@endforeach
	</table>
	<br><br><br><br>
	@endforeach
</body>
</html>
