@extends('Master')
@section('titulo','Desglose de ticket')
@section('content')
<div id="reportedesglose">
	<div class="row">
		<div class="col-xs-12 ">
			<form @submit.prevent="generarReporte()" >
				<div class="box box-success">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Filtros de búsqueda</h3>
					</div>

					<div class="box-body">
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaDesde">Desde *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaDesde" v-model="fechaDesde" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaHasta">Hasta *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaHasta" v-model="fechaHasta" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<br>
							<button type="submit" class="btn btn-success btn-sm ">Mostrar reporte</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 ">
			<div class="box box-success">
				<div class="box-header with-border " align="center" style="text-align: center;">
					<h3 class="box-title" >Reporte</h3>
					<div class="row">
						<template v-if='mostrarbotonDescarga'>
							<div class="col-md-4">
								<a class="btn btn-app" @click='descargarPDFdesglose'>
									<i class="fa fa-download" aria-hidden="true"></i> PDF
								</a>
								<a class="btn btn-app" @click='GenerarExcelDesglose'>
									<i class="fa fa-download" aria-hidden="true"></i> EXCEL
								</a>
                                <a @click.prevent="descargarPDFdesgloseEN()" class="btn btn-app btn-danger" :disabled="cargandoPDF">
                                    <i :class="!cargandoPDF
                                        ? 'fa fa-download'
                                        : 'fa fa-spinner fa-pulse fa-1x fa-fw'">
                                    </i>
                                    <span> PDF US </span>
                                </a>
							</div>
						</template>

					</div>
				</div>

				<div class="box-body">
					<div class="col-xs-12">
						<template v-if='reporteMesasHis'>
							<div class="table-responsive">
								<template v-for='mesas in reporteMesasHis'>
									<table class="table table-hove table table-bordered table-striped dataTable" width="100%">
										<thead>
											<tr >
												<th colspan="4">TICKET  # @{{mesas.cHisMesTicket}}</th>
												<th colspan="2">FECHA VENTA: @{{formatearFecha(mesas.cHisMesFechaRep)}}</th>

											</tr>
											<tr>
												<th>CANTIDAD</th>
												<th>HORA INGRESO</th>
												<th>CODIGO</th>
												<th>DESCRIPCION</th>
												<th align="right" style="text-align: right;">SUBTOTAL</th>
											<th align="right" style="text-align: right;">IMPUESTO</th>
											<th align="right" style="text-align: right;">TOTAL</th>
											</tr>
										</thead>
										<tbody>
											<template v-for='productos in reporteProductos' v-if='productos.cHisProTicket == mesas.cHisMesTicket'>
												<tr>
													<td width="10%" >@{{productos.cHisProCantidad}}</td>
													<td width="10%" >@{{productos.hora}}</td>
													<td width="20%" >@{{productos.cHisProCodigo}}</td>
													<td width="50%">@{{productos.cHisProDesTicket}}</td>
													<td width="10%" align="right" style="text-align: right;">@{{formatearMoneda(productos.cHisProSubTotal)}}</td>
													<td width="10%" align="right" style="text-align: right;">@{{formatearMoneda(productos.cHisProImp1)}}</td>
													<td width="10%" align="right" style="text-align: right;">@{{totalProductosIva(productos.cHisProSubTotal,productos.cHisProImp1)}}</td>
												</tr>
											</template>
											<template v-for='total in totalProductos' v-if='total.cHisProTicket == mesas.cHisMesTicket'>
												<td colspan="4" align="right" style="text-align: right;"><strong>TOTAL</strong></td>
												<td align="right" style="text-align: right;"><strong>@{{formatearMoneda(total.Subtotal)}}</strong></td>
												<td align="right" style="text-align: right;"><strong>@{{formatearMoneda(total.Impuesto)}}</strong></td>
												<td align="right" style="text-align: right;"><strong>@{{formatearMoneda(total.Total)}}</strong></td><br><br>
											</template>
											<tr>
												<th>FORMA DE PAGO</th>
												<th> MONTO</th>
											</tr>
											<template v-for='formaspago in formasdepago' v-if='formaspago.cHisForTicket == mesas.cHisMesTicket'>
												<template v-if='formaspago.cHisForId==1'>
													<tr>
														<td >EFECTIVO</td>
														<td >NA</td>
													</tr>
												</template>
												<template v-else>
													<tr>
														<td>@{{formaspago.cHisForDes}}</td>
														<td>@{{formatearMoneda(formaspago.cHisForCant)}}</td>
													</tr>
												</template>
											</template>
										</tbody>
									</table><br><br>
								</template>
							</div>
						</template>
						<template v-else>
							<template v-if="isLoading">
								<p class="text-center">
									<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
									<span>Cargando...</span>
								</p>
							</template>
							<template v-if='sinRegistros'>
								<p class="text-center">
									<small>No existen Registros en el rango de fecha @{{fechaDesde}} / @{{fechaHasta}}</small>
								</p>
							</template>
							<template v-else>
								<p class="text-center">
									<small>Complete los campos para mostrar un resumen</small>
								</p>
							</template>
						</template>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-tables-2.min.js"></script>
<script src="/vuejs/app-reporteDesglose.js"></script>
@endsection
