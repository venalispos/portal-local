<?php
$Impuesto1 = null;
$Impuesto2 = null;
$Impuesto3 = null;
$Impuesto4 = null;

$Impuesto1 = $reportes['Impuesto1'];
$Impuesto2 = $reportes['Impuesto2'];
$Impuesto3 = $reportes['Impuesto3'];
$Impuesto4 = $reportes['Impuesto4'];
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
	<div>
    <p  style="font-family: courier;"><b>Sales by category</b></p>
    <br>
		<table width="100%" style="font-family: courier; ">
			<thead>
        <thead>
          <th style = "text-align: right;"> Sales categories</th>
          <th style = "text-align: right;"> Net sls</th>
          <th style = "text-align: right;"> Comps</th>
          <th style = "text-align: right;"> Voids</th>
          <th style = "text-align: right;"> Waste </th>
        @if($Impuesto1)
          <th style = "text-align: right;">{{$Impuesto1['cConImpuesto1Nombre']}}</th>
        @endif
        @if($Impuesto2)
          <th style = "text-align: right;">{{$Impuesto2['cConImpuesto2Nombre']}}</th>
        @endif
        @if($Impuesto3)
          <th style = "text-align: right;">{{$Impuesto3['cConImpuesto3Nombre']}}</th>
        @endif
        @if($Impuesto4)
          <th style = "text-align: right;" >{{$Impuesto4['cConImpuesto4Nombre']}}</th>
        @endif
          <th style = "text-align: right;">Grs sales</th>
        </thead>
        <tbody>
          @foreach($reportes['Categorias'] as $sale)
          <tr>
            <td align = 'right'>{{$sale['cCatDescripcion']}}</td>
            <td align = 'right'>{{number_format($sale['Totales']['Subtotal'],2, '.', '')}}</td>
            <td align = 'right'>{{number_format($sale['Totales']['Cortesias'],2, '.', '')}}</td>
            <td align = 'right'>{{number_format($sale['Totales']['Cancelaciones'],2, '.', '')}}</td>
            <td align = 'right'>{{number_format($sale['Totales']['Merma'],2, '.', '')}}</td>
            @if($Impuesto1)
            <td align = 'right'>{{number_format($sale['Totales']['Impuesto1'],2, '.', '')}}</td>
            @endif
            @if($Impuesto2)
            <td align = 'right'>{{number_format($sale['Totales']['Impuesto2'],2, '.', '')}}</td>
            @endif
            @if($Impuesto3)
            <td align = 'right'>{{number_format($sale['Totales']['Impuesto3'],2, '.', '')}}</td>
            @endif
            @if($Impuesto4)
            <td align = 'right'>{{number_format($sale['Totales']['Impuesto4'],2, '.', '')}}</td>
            @endif

            <td align = 'right'>{{number_format($sale['Totales']['Total'],2, '.', '')}}</td>
          </tr>
          @endforeach
          @if($reportes['TotalesSinCategorias'] != 0)
          <tr>
            <td align = 'right'>WITHOUT CATEGORY</td>
            <td align = 'right'>{{number_format($reportes['TotalesSinCategorias']['Subtotal'],2, '.', '')}}</td>
            <td align = 'right'>{{number_format($reportes['TotalesSinCategorias']['Cortesias'],2, '.', '')}}</td>
            <td align = 'right'>{{number_format($reportes['TotalesSinCategorias']['Cancelaciones'],2, '.', '')}}</td>
            <td align = 'right'>{{number_format($reportes['TotalesSinCategorias']['Merma'],2, '.', '')}}</td>
            @if($Impuesto1)
            <td align = 'right'>{{number_format($reportes['TotalesSinCategorias']['Impuesto1'],2, '.', '')}}</td>
            @endif
            @if($Impuesto2)
            <td align = 'right'>{{number_format($reportes['TotalesSinCategorias']['Impuesto2'],2, '.', '')}}</td>
            @endif
            @if($Impuesto3)
            <td align = 'right'>{{number_format($reportes['TotalesSinCategorias']['Impuesto3'],2, '.', '')}}</td>
            @endif
            @if($Impuesto4)
            <td align = 'right'>{{number_format($reportes['TotalesSinCategorias']['Impuesto4'],2, '.', '')}}</td>
            @endif

            <td align = 'right'>{{number_format($reportes['TotalesSinCategorias']['Total'],2, '.', '')}}</td>
          </tr>
          @endif

          <tr>
            <td align = 'right'><b>Totals</b></td>
            <td align = 'right'><b>{{number_format($reportes['SubtotalFinal'],2, '.', '')}}</b></td>
            <td align = 'right'><b>{{number_format($reportes['CortesiasFinal'],2, '.', '')}}</b></td>
            <td align = 'right'><b>{{number_format($reportes['CanselacionesFinal'],2, '.', '')}}</b></td>
            <td align = 'right'><b>{{number_format($reportes['MermaFinal'],2, '.', '')}}</b></td>
            @if($Impuesto1)
            <td align = 'right'><b>{{number_format($reportes['Impuestos1Final'],2, '.', '')}}</b></td>
            @endif
            @if($Impuesto2)
            <td align = 'right'><b>{{number_format($reportes['Impuestos2Final'],2, '.', '')}}</b></td>
            @endif
            @if($Impuesto3)
            <td align = 'right'><b>{{number_format($reportes['Impuestos3Final'],2, '.', '')}}</b></td>
            @endif
            @if($Impuesto4)
            <td align = 'right'><b>{{number_format($reportes['Impuestos4Final'],2, '.', '')}}</b></td>
            @endif
            <td align = 'right'><b>{{number_format($reportes['Total'],2, '.', '')}}</b></td>
          </tr>

      </thead>
		</table>

    <br>
    <p style="font-family: courier;"><b>Sales by day part by cat</b></p>
    <br>
    <table width="70%" style="font-family: courier;">
      <thead>
        <th style = "text-align: right;">Category</th>
        @foreach($reportes['Horarios'] as $Horario)
          <th style = "text-align: right;">{{$Horario['cDescParteDia']}}</th>
        @endforeach
        <th style = "text-align: right;">Total</th>
      </thead>
      <tbody>
        @foreach($reportes['Categorias'] as $Categoria)
        <tr>
          <td align = "right">{{$Categoria['cCatDescripcion']}}</td>
          @foreach($Categoria['Hora']['Horarios'] as $Horario)
            <td align = "right">{{number_format($Horario['Total'],2, '.', '')}}</td>
          @endforeach
          <td align = "right">{{number_format($Categoria['Hora']['Total'],2, '.', '')}}</td>
        </tr>
        @endforeach
        @if($reportes['HoraSInCategoriaTotal'] != 0)
        <tr>
          <td align = "right">WITHOUT CATEGORY</td>
          @foreach($reportes['HoraSInCategoriaTotal']['Horarios'] as $Horario)
            <td align = "right">{{number_format($Horario['Total'],2, '.', '')}}</td>
          @endforeach
          <td align = "right">{{number_format($reportes['HoraSInCategoriaTotal']['Total'],2, '.', '')}}</td>
        </tr>
        @endif
        <tr>
          <td align = "right"><b>Totals</b></td>
          @foreach($reportes['TotalHorarios'] as $Horario)
            <td align = "right">{{number_format($Horario,2, '.', '')}}</td>
          @endforeach
          <td align = "right"><b>{{number_format($reportes['TotalFinalHorario'],2, '.', '')}}</b></td>
        </tr>
      </tbody>
    </table>
    <br>
    <p style="font-family: courier;"><b>Payments</b></p>
    <br>
    <table width="100%" style="font-family: courier;">
      <thead>
        <th style = "text-align: right;">Payment type</th>
        <th style = "text-align: right;">Qry Trans</th>
        <th style = "text-align: right;">Amount</th>
        <th style = "text-align: right;">Charge Tip</th>
        <th style = "text-align: right;">AutoGratui</th>
        <th style = "text-align: right;">Sales</th>
      </thead>
      <tbody>
        @foreach($reportes['FormaPagos']['FormaDePago'] as $Pago)
        <tr>
          <td align = "right">{{$Pago['cCatForDesc']}}</td>
          <td align = "right">{{number_format($Pago['Cantidad'],2,'.','')}}</td>
          <td align = "right">{{number_format($Pago['Subtotal'],2,'.','')}}</td>
          <td align = "right">{{number_format($Pago['Cargo'],2,'.','')}}</td>
          <td align = "right">{{number_format($Pago['Propina'],2,'.','')}}</td>
          <td align = "right">{{number_format($Pago['Total'],2,'.','')}}</td>
        </tr>
        @endforeach
        <tr>
          <td align = "right"><b>Totals</b><br> </td>
          <td align = "right"><b>{{$reportes['FormaPagos']['TotalCantidad']}}</b></td>
          <td align = "right"><b>{{number_format($reportes['FormaPagos']['TotalSubtotal'],2,'.','')}}</b></td>
          <td align = "right"><b>{{number_format($reportes['FormaPagos']['TotalCargo'],2,'.','')}}</b></td>
          <td align = "right"><b>{{number_format($reportes['FormaPagos']['TotapPropina'],2,'.','')}}</b></td>
          <td align = "right"><b>{{number_format($reportes['FormaPagos']['TotalFinal'],2,'.','')}}</b></td>
        </tr>
      </tbody>
    </table>
    @if($Impuesto1)
    <br>
    <p style="font-family: courier;"><b>{{$Impuesto1['cConImpuesto1Nombre']}}</b></p>
    <br>
    <table width="30%" style="font-family: courier;">
      <thead>
        <th style = "text-align: right;">Sales categories</th>
        <th style = "text-align: right;">{{$Impuesto1['cConImpuesto1Cantidad']*100}}%</th>
      </thead>
      <tbody>
        @foreach($reportes['Categorias'] as $mb_tax)
        <tr>
          <td align = "right">{{$mb_tax['cCatDescripcion']}}</td>
          <td align = "right">{{number_format($mb_tax['Totales']['Impuesto1'],2,'.','')}}</td>
        </tr>
        @endforeach
        @if($reportes['TotalesSinCategorias'] != 0)
        <tr>
          <td align = "right">WITHOUT CATEGORY</td>
          <td align = "right">{{number_format($mb_tax['TotalesSinCategorias']['Impuesto1'],2,'.','')}}</td>
        </tr>
        @endif
        <tr>
          <td align = "right"><b>Totals</b></td>
          <td align = "right">
            <b>{{number_format($reportes['Impuestos1Final'],2,'.','')}}</b>
          </td>
        </tr>
      </tbody>
    </table>
    @endif
    @if($Impuesto2)
    <br>
    <p style="font-family: courier;"><b>{{$Impuesto2['cConImpuesto2Nombre']}}</b></p>
    <br>
    <table width="30%" style="font-family: courier;">
      <thead>
        <th style = "text-align: right;">Sales categories</th>
        <th style = "text-align: right;">{{$Impuesto2['cConImpuesto2Cantidad']*100}}%</th>
      </thead>
      <tbody>
        @foreach($reportes['Categorias'] as $mb_tax)
        <tr>
          <td align = "right">{{$mb_tax['cCatDescripcion']}}</td>
          <td align = "right">{{number_format($mb_tax['Totales']['Impuesto2'],2,'.','')}}</td>
        </tr>
        @endforeach
        @if($reportes['TotalesSinCategorias'] != 0)
        <tr>
          <td align = "right">WITHOUT CATEGORY</td>
          <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Impuesto2'],2,'.','')}}</td>
        </tr>
        @endif
        <tr>
          <td align = "right"><b>Totals</b></td>
          <td align = "right">
            <b>{{number_format($reportes['Impuestos2Final'],2,'.','')}}</b>
          </td>
        </tr>
      </tbody>
    </table>
    @endif
    @if($Impuesto3)
    <br>
    <p style="font-family: courier;"><b>{{$Impuesto3['cConImpuesto3Nombre']}}</b></p>
    <br>
    <table width="30%" style="font-family: courier;">
      <thead>
        <th style = "text-align: right;">Sales categories</th>
        <th style = "text-align: right;">{{$Impuesto3['cConImpuesto3Cantidad']*100}}%</th>
      </thead>
      <tbody>
        @foreach($reportes['Categorias'] as $mb_tax)
        <tr>
          <td align = "right">{{$mb_tax['cCatDescripcion']}}</td>
          <td align = "right">{{number_format($mb_tax['Totales']['Impuesto3'],2,'.','')}}</td>
        </tr>
        @endforeach
        @if($reportes['TotalesSinCategorias'] != 0)
        <tr>
          <td align = "right">WITHOUT CATEGORY</td>
          <td align = "right">{{number_format($reportes['TotalesSinCategorias']['Impuesto3'],2,'.','')}}</td>
        </tr>
        @endif
        <tr>
          <td align = "right"><b>Totals</b></td>
          <td align = "right">
            <b>{{number_format($reportes['Impuestos3Final'],2,'.','')}}</b>
          </td>
        </tr>
      </tbody>
    </table>
    @endif
    @if($Impuesto4)
    <br>
    <p style="font-family: courier;"><b>{{$Impuesto4['cConImpuesto4Nombre']}}</b></p>
    <br>
    <table width="30%" style="font-family: courier;">
      <thead>
        <th style = "text-align: right;">Sales categories</th>
        <th style = "text-align: right;">{{$Impuesto4['cConImpuesto4Cantidad']*100}}%</th>
      </thead>
      <tbody>
        @foreach($reportes['Categorias'] as $mb_tax)
        <tr>
          <td align = "right">{{$mb_tax['cCatDescripcion']}}</td>
          <td align = "right">{{number_format($mb_tax['Totales']['Impuesto4'],2,'.','')}}</td>
        </tr>
        @endforeach
        @if($reportes['TotalesSinCategorias'] != 0)
        <tr>
          <td align = "right">WITHOUT CATEGORY</td>
          <td align = "right">{{number_format($mb_tax['TotalesSinCategorias']['Impuesto4'],2,'.','')}}</td>
        </tr>
        @endif
        <tr>
          <td align = "right"><b>Totals</b></td>
          <td align = "right">
            <b>{{number_format($reportes['Impuestos4Final'],2,'.','')}}</b>
          </td>
        </tr>
      </tbody>
    </table>
    @endif
    <br>
    <p style="font-family: courier;"><b>Tax by Tax ID</b></p>
    <br>
    <table width="30%" style="font-family: courier;">
      <thead>
        <th style = "text-align: right;">Sales tax</th>
        <th style = "text-align: right;">Total</th>
      </thead>
      <tbody>
        <tr>
          @if($Impuesto1)
          <td align = "right">{{$Impuesto1['cConImpuesto1Nombre']}}</td>
          @endif
          @if($Impuesto1)
          <td align = "right">{{number_format($reportes['Impuestos1Final'],2,'.','')}}</td>
          @endif
        </tr>
        <tr>
          @if($Impuesto2)
          <td align = "right" >{{$Impuesto2['cConImpuesto2Nombre']}}</td>
          @endif
          @if($Impuesto2)
          <td align = "right">{{number_format($reportes['Impuestos2Final'],2,'.', '')}}</td>
          @endif
        </tr>
        <tr>
          @if($Impuesto3)
          <td align = "right">{{$Impuesto3['cConImpuesto3Nombre']}}</td>
          @endif
          @if($Impuesto3)
          <td align = "right">{{number_format($reportes['Impuestos3Final'],2,'.','')}}</td>
          @endif
        </tr>
        <tr>
          @if($Impuesto4)
          <td align = "right">{{$Impuesto4['cConImpuesto4Nombre']}}</td>
          <td align = "right">{{number_format($reportes['Impuestos4Final'],2,'.','')}}</td>
          @endif
        </tr>
        <tr>
          <td align = "right"><b>Totals</b></td>
          <td align = "right">{{number_format($reportes['Impuestos1Final']+$reportes['Impuestos2Final']+$reportes['Impuestos3Final']+$reportes['Impuestos4Final'],2,'.','')}}</td>
        </tr>
      </tbody>
    </table>
    <br>
    <p style="font-family: courier;"><b>Count by Day Part</b></p>
    <br>
    <table  width="30%" style="font-family: courier;">
      <thead>
        <th style = "text-align: right;">Category</th>
        <th style = "text-align: right;">Guest</th>
        <th style = "text-align: right;">Check</th>
      </thead>
      <tbody>
        @foreach($reportes['HoraMesa']['Mesas'] as $Horario)
        <tr>
          <td style = "text-align: right;">{{$Horario['cDescParteDia']}}</td>
          <td style = "text-align: right;">{{$Horario['TotalClientes']}}</td>
          <td style = "text-align: right;">{{$Horario['TotalTicket']}}</td>
        </tr>
        @endforeach
        <tr>
          <td align = "right"><b>Totals</b></td>
          <td align = "right">{{$reportes['HoraMesa']['TotalClientes']}}</td>
          <td align = "right">{{$reportes['HoraMesa']['TotalTicket']}}</td>
        </tr>
      </tbody>
    </table>

    <br>
    @foreach($reportes['Apps'] as $App)
      <p style="font-family: courier;"><b>{{$App['Descripcion']}}</b></p>
      <table  width="30%" style="font-family: courier;" >
        <thead>
          <th></th>
          <th style = "text-align:right">Total</th>
        </thead>
        <tbody>
          <tr>
            <td>Net sales</td>
            <td align = "right"> {{number_format($App['Subtotal'], 2, '.', '')}}</td>
        </tr>
        <tr>
          @if($Impuesto1)
          <td>{{$Impuesto1['cConImpuesto1Nombre']}}</td>
          <td align = "right">{{number_format($App['Impuesto1'],2,'.','')}}</td>
          @endif
        </tr>
        <tr>
        @if($Impuesto2)
          <td>{{$Impuesto2['cConImpuesto2Nombre']}}</td>
          <td align = "right">{{number_format($App['Impuesto2'], 2, '.', '')}}</td>
        @endif
        </tr>
        <tr>
          @if($Impuesto3)
          <td>{{$Impuesto3['cConImpuesto3Nombre']}}</td>
          <td align = "right">{{number_format($App['Impuesto3'], 2, '.', '')}}</td>
          @endif
        </tr>
        <tr>
          @if($Impuesto4)
          <td>{{$Impuesto4['cConImpuesto4Nombre']}}</td>
          <td align = "right">{{number_format($App['Impuesto4'], 2, '.', '')}}</td>
          @endif
        </tr>
        <tr>
          <td><b>Fee</b></td>
          <td align = "right">{{number_format($App['Fee'],2,'.','')}}</td>
        </tr>
          <tr>
            <td><b>Total</b></td>
            <td align = "right">{{number_format($App['Total'], 2, '.', '')}}</td>
          </tr>
        </tbody>
      </table>
    @endforeach
	</div>
</body>
</html>
