@extends('Master')
@section('titulo','Sales report')
@section('content')
<div id =   "sales_report">
  <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border text-center">
            Choose dates of report
          </div>
            <div class="box-body">
              <form  method="post">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">Since</label>
                    <input type="date" name="" value="" class = "form-control" v-model = "date_since">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="">To</label>
                    <input type="date" name="" value="" class = "form-control" v-model = "date_to">
                  </div>
                </div>
                <button type="button" name="button" class = "btn btn-primary pull-right btn-sm" @click = "obtener_ventas">
                  <span class = "fa fa-search" ></span> Search
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div v-if = "Categorias" class="row">
        <a @click = "generar_pdf_reporte" class="btn btn-app">
            <i class="fa fa-file"></i> PDF
        </a>
      </div>
      <template v-if="Categorias != 0">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border text-center">
              Sales by category
            </div>
            <div class="box-body">
              <table class = "table table-stripped table-hover">
                <thead>
                  <th> Sales categories</th>
                  <th style = "text-align:right;"> Net sls</th>
                  <th style = "text-align:right;"> Comps</th>
                  <th style = "text-align:right;"> Voids</th>
                  <th style = "text-align:right;">Waste</th>
                  <th style = "text-align:right;" v-if = "Impuesto1">@{{Impuesto1.cConImpuesto1Nombre}}</th>
                  <th style = "text-align:right;" v-if = "Impuesto2">@{{Impuesto2.cConImpuesto2Nombre}}</th>
                  <th style = "text-align:right;" v-if = "Impuesto3">@{{Impuesto3.cConImpuesto3Nombre}}</th>
                  <th style = "text-align:right;" v-if = "Impuesto4">@{{Impuesto4.cConImpuesto4Nombre}}</th>
                  <th style = "text-align:right;">Grs sales</th>
                </thead>
                <tbody>
                  <tr v-for = "Categoria in Categorias">
                    <td>@{{Categoria['cCatDescripcion']}}</td>
                    <td align = "right">@{{formatoVenta(Categoria.Totales.Subtotal)}}</td>
                    <td align = "right">@{{formatoVenta(Categoria.Totales.Cortesias)}}</td>
                    <td align = "right">@{{formatoVenta(Categoria.Totales.Cancelaciones)}}</td>
                    <td align = "right">@{{formatoVenta(Categoria.Totales.Merma)}}</td>
                    <td align = "right" v-if = "Impuesto1">@{{formatoVenta(Categoria.Totales.Impuesto1)}}</td>
                    <td align = "right" v-if = "Impuesto2">@{{formatoVenta(Categoria.Totales.Impuesto2)}}</td>
                    <td align = "right" v-if = "Impuesto3">@{{formatoVenta(Categoria.Totales.Impuesto3)}}</td>
                    <td align = "right" v-if = "Impuesto4">@{{formatoVenta(Categoria.Totales.Impuesto4)}}</td>
                    <td align = "right">@{{formatoVenta(Categoria.Totales.Total)}}</td>
                  </tr>
                  <tr v-if = "TotalesSinCategorias != 0">
                    <td>WITHOUT CATEGORY</td>
                    <td align = "right">@{{formatoVenta(TotalesSinCategorias.Subtotal)}}</td>
                    <td align = "right">@{{formatoVenta(TotalesSinCategorias.Cortesias)}}</td>
                    <td align = "right">@{{formatoVenta(TotalesSinCategorias.Cancelaciones)}}</td>
                    <td align = "right">@{{formatoVenta(TotalesSinCategorias.Merma)}}</td>
                    <td align = "right" v-if = "Impuesto1">@{{formatoVenta(TotalesSinCategorias.Impuesto1)}}</td>
                    <td align = "right" v-if = "Impuesto2">@{{formatoVenta(TotalesSinCategorias.Impuesto2)}}</td>
                    <td align = "right" v-if = "Impuesto3">@{{formatoVenta(TotalesSinCategorias.Impuesto3)}}</td>
                    <td align = "right" v-if = "Impuesto4">@{{formatoVenta(TotalesSinCategorias.Impuesto4)}}</td>
                    <td align = "right">@{{formatoVenta(TotalesSinCategorias.Total)}}</td>
                  </tr>
                  <tr>
                    <td align = "right"><b>Totals</b></td>
                    <td align = "right"><b>@{{formatoVenta(SubtotalFinal)}}</b></td>
                    <td align = "right"><b>@{{formatoVenta(CortesiasFinal)}}</b></td>
                    <td align = "right"><b>@{{formatoVenta(CanselacionesFinal)}}</b></td>
                    <td align = "right"><b>@{{formatoVenta(MermaFinal)}}</b></td>
                    <td align = "right" v-if = "Impuesto1"><b>@{{formatoVenta(Impuestos1Final)}}</b></td>
                    <td align = "right" v-if = "Impuesto2"><b>@{{formatoVenta(Impuestos2Final)}}</b></td>
                    <td align = "right" v-if = "Impuesto3"><b>@{{formatoVenta(Impuestos3Final)}}</b></td>
                    <td align = "right" v-if = "Impuesto4"><b>@{{formatoVenta(Impuestos4Final)}}</b></td>
                    <td align = "right"><b>@{{formatoVenta(Total)}}</b></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="box box-primary">
                  <div class="box-header with-border text-center">
                    Sales by day part by cat
                  </div>
                  <div class="box-body">
                    <table class = "table table-stripped table-hover">
                      <thead>
                        <th>Category</th>
                        <template v-for= "Horario in Horarios">
                          <th style = "text-align:right;">@{{Horario.cDescParteDia}}</th>
                        </template>
                        <th style = "text-align:right;">Total</th>
                      </thead>
                      <tbody>
                        <tr v-for = "Categoria in Categorias">
                          <td>@{{Categoria['cCatDescripcion']}}</td>
                          <template v-for = "(Horario, indexP) in Categoria.Hora.Horarios">
                            <td align = "right">@{{formatoVenta(Horario.Total)}}</td>
                          </template>
                          <td align = "right">@{{formatoVenta(Categoria.Hora.Total)}}</td>
                        </tr>
                        <tr v-if = "HoraSInCategoriaTotal != 0">
                          <td>WITHOUT CATEGORY</td>
                          <template v-for = "(Horario, indexP) in HoraSInCategoriaTotal.Horarios">
                            <td align = "right">@{{formatoVenta(Horario.Total)}}</td>
                          </template>
                          <td align = "right">@{{formatoVenta(HoraSInCategoriaTotal.Total)}}</td>
                        </tr>
                        <tr>
                          <td><b>Totals</b></td>
                          <template v-for = "Horario in TotalHorarios">
                            <td align = "right"><b>@{{formatoVenta(Horario)}}</b></td>
                          </template>
                          <td align = "right"><b>@{{formatoVenta(TotalFinalHorario)}}</b></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="box box-primary">
                  <div class="box-header with-border text-center">
                    Payments
                  </div>
                  <div class="box-body">
                    <table class = "table table-stripped table-hover">
                      <thead>
                        <th>Payment type</th>
                        <th style = "text-align:right;">Qry Trans</th>
                        <th style = "text-align:right;">Amount</th>
                        <th style = "text-align:right;">Charge Tip</th>
                        <th style = "text-align:right;">AutoGratui</th>
                        <th style = "text-align:right;">Sales</th>
                      </thead>
                      <tbody>
                        <tr v-for = "Pago in FormaPagos.FormaDePago">
                          <td>@{{Pago.cCatForDesc}}</td>
                          <td align = "right">@{{formatoVenta(Pago.Cantidad)}}</td>
                          <td align = "right">@{{formatoVenta(Pago.Subtotal)}}</td>
                          <td align = "right">@{{formatoVenta(Pago.Cargo)}}</td>
                          <td align = "right">@{{formatoVenta(Pago.Propina)}}</td>
                          <td align = "right">@{{formatoVenta(Pago.Total)}}</td>
                        </tr>
                        <tr>
                          <td><b>Totals</b></td>
                          <td align = "right"><b>@{{formatoVenta(FormaPagos.TotalCantidad)}}</b></td>
                          <td align = "right"><b>@{{formatoVenta(FormaPagos.TotalSubtotal)}}</b></td>
                          <td align = "right"><b>@{{formatoVenta(FormaPagos.TotalCargo)}}</b></td>
                          <td align = "right"><b>@{{formatoVenta(FormaPagos.TotapPropina)}}</b></td>
                          <td align = "right"><b>@{{formatoVenta(FormaPagos.TotalFinal)}}</b></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <template v-if = "Impuesto1">
                <div class="col-md-6">
                  <div class="box box-primary">
                    <div class="box-header with-border text-center">
                      @{{Impuesto1.cConImpuesto1Nombre}}
                    </div>
                    <div class="box-body">
                      <table class = "table table-stripped table-hover">
                        <thead>
                          <th >Sales categories</th>
                          <th style = "text-align:right;">@{{Impuesto1.cConImpuesto1Cantidad*100}}%</th>
                        </thead>
                        <tbody>
                          <tr v-for = 'Categoria in Categorias'>
                            <td>@{{Categoria['cCatDescripcion']}}</td>
                            <td align = "right">@{{formatoVenta(Categoria.Totales.Impuesto1)}}</td>
                          </tr>
                          <tr v-if = "TotalesSinCategorias != 0">
                            <td>WITHOUT CATEGORY</td>
                            <td align = "right" v-if = "Impuesto1">@{{formatoVenta(TotalesSinCategorias.Impuesto1)}}</td>
                          </tr>
                          <tr>
                            <td><b>Totals</b></td>
                            <td align = "right">
                              <b>@{{formatoVenta(Impuestos1Final)}}</b>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </template>

              <template v-if = "Impuesto2">
                <div class="col-md-6">
                  <div class="box box-primary">
                    <div class="box-header with-border text-center">
                      @{{Impuesto2.cConImpuesto2Nombre}}
                    </div>
                    <div class="box-body">
                      <table class = "table table-stripped table-hover">
                        <thead>
                          <th >Sales categories</th>
                          <th style = "text-align:right;">@{{Impuesto2.cConImpuesto2Cantidad*100}}%</th>
                        </thead>
                        <tbody>
                          <tr v-for = 'Categoria in Categorias'>
                            <td>@{{Categoria['cCatDescripcion']}}</td>
                            <td align = "right">@{{formatoVenta(Categoria.Totales.Impuesto2)}}</td>
                          </tr>
                          <tr v-if = "TotalesSinCategorias != 0">
                            <td>WITHOUT CATEGORY</td>
                            <td align = "right" v-if = "Impuesto2">@{{formatoVenta(TotalesSinCategorias.Impuesto2)}}</td>
                          </tr>
                          <tr>
                            <td><b>Totals</b></td>
                            <td align = "right">
                              <b>@{{formatoVenta(Impuestos2Final)}}</b>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </template>

              <template v-if = "Impuesto3">
                <div class="col-md-6">
                  <div class="box box-primary">
                    <div class="box-header with-border text-center">
                      @{{Impuesto3.cConImpuesto3Nombre}}
                    </div>
                    <div class="box-body">
                      <table class = "table table-stripped table-hover">
                        <thead>
                          <th >Sales categories</th>
                          <th style = "text-align:right;">@{{Impuesto3.cConImpuesto3Cantidad*100}}%</th>
                        </thead>
                        <tbody>
                          <tr v-for = 'Categoria in Categorias'>
                            <td>@{{Categoria['cCatDescripcion']}}</td>
                            <td align = "right">@{{formatoVenta(Categoria.Totales.Impuesto3)}}</td>
                          </tr>
                          <tr v-if = "TotalesSinCategorias != 0">
                            <td>WITHOUT CATEGORY</td>
                            <td align = "right" v-if = "Impuesto3">@{{formatoVenta(TotalesSinCategorias.Impuesto3)}}</td>
                          </tr>
                          <tr>
                            <td><b>Totals</b></td>
                            <td align = "right">
                              <b>@{{formatoVenta(Impuestos3Final)}}</b>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </template>

              <template v-if = "Impuesto4">
                <div class="col-md-6">
                  <div class="box box-primary">
                    <div class="box-header with-border text-center">
                      @{{Impuesto4.cConImpuesto4Nombre}}
                    </div>
                    <div class="box-body">
                      <table class = "table table-stripped table-hover">
                        <thead>
                          <th >Sales categories</th>
                          <th style = "text-align:right;">@{{Impuesto4.cConImpuesto4Cantidad*100}}%</th>
                        </thead>
                        <tbody>
                          <tr v-for = 'Categoria in Categorias'>
                            <td>@{{Categoria['cCatDescripcion']}}</td>
                            <td align = "right">@{{formatoVenta(Categoria.Totales.Impuesto4)}}</td>
                          </tr>
                          <tr v-if = "TotalesSinCategorias != 0">
                            <td>WITHOUT CATEGORY</td>
                            <td align = "right" v-if = "Impuesto4">@{{formatoVenta(TotalesSinCategorias.Impuesto4)}}</td>
                          </tr>
                          <tr>
                            <td><b>Totals</b></td>
                            <td align = "right">
                              <b>@{{formatoVenta(Impuestos4Final)}}</b>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </template>

              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border text-center">
                    Tax by Tax ID
                  </div>
                  <div class="box-body">
                    <table class = "table table-stripped table-hover">
                      <thead>
                        <th>Sales tax</th>
                        <th style = "text-align:right;">Total</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td v-if = "Impuesto1">@{{Impuesto1.cConImpuesto1Nombre}}</td>
                          <td align = "right" v-if = "Impuesto1">@{{formatoVenta(Impuestos1Final)}}</td>
                        </tr>
                        <tr>
                          <td v-if = "Impuesto2">@{{Impuesto2.cConImpuesto2Nombre}}</td>
                          <td align = "right" v-if = "Impuesto2">@{{formatoVenta(Impuestos2Final)}}</td>
                        </tr>
                        <tr>
                          <td v-if = "Impuesto3">@{{Impuesto3.cConImpuesto3Nombre}}</td>
                          <td align = "right" v-if = "Impuesto3">@{{formatoVenta(Impuestos3Final)}}</td>
                        </tr>
                        <tr>
                          <td v-if = "Impuesto4">@{{Impuesto4.cConImpuesto4Nombre}}</td>
                          <td align = "right" v-if = "Impuesto4">@{{formatoVenta(Impuestos4Final)}}</td>
                        </tr>
                        <tr>
                          <td><b>Totals</b></td>
                          <td align = "right">@{{formatoVenta(Impuestos1Final+Impuestos2Final+Impuestos3Final+Impuestos4Final)}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="box box-primary">
                  <div class="box-header with-border text-center">
                   Count by Day Part
                  </div>
                  <div class="box-body">
                    <table class = "table table-stripped table-hover">
                      <thead>
                        <th>Category</th>
                        <th style = "text-align:right;">Guest</th>
                        <th style = "text-align:right;">Check</th>
                      </thead>
                      <tbody>
                        <template v-for = "(Horario, index) in HoraMesa.Mesas">
                          <tr>
                            <td>@{{Horario.cDescParteDia}}</td>
                            <td align = "right">@{{Horario.TotalClientes}}</td>
                            <td align = "right">@{{Horario.TotalTicket}}</td>
                          </tr>
                        </template>
                        <tr>
                          <td><b>Totals</b></td>
                          <td align = "right"><strong>@{{formatoVenta(HoraMesa.TotalClientes)}}</strong></td>
                          <td align = "right"><strong>@{{formatoVenta(HoraMesa.TotalTicket)}}</strong></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <template v-for = "(App, index) in Apps">
                <div class="col-md-6">
                  <div class="box box-primary">
                    <div class="box-header with-border text-center">
                      @{{App.Descripcion}}
                    </div>
                    <div class="box-body">
                      <table class = "table table-stripped table-hover">
                        <thead>
                          <th></th>
                          <th style = "text-align:right;">Total</th>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Net sales</td>
                            <td align = "right" >@{{formatoVenta(App.Subtotal)}}</td>
                          </tr>
                          <tr>
                            <td v-if = "Impuesto1">@{{Impuesto1.cConImpuesto1Nombre}}</td>
                            <td align = "right" v-if = "Impuesto1">@{{formatoVenta(App.Impuesto1)}}</td>
                          </tr>
                          <tr>
                            <td v-if = "Impuesto2">@{{Impuesto2.cConImpuesto2Nombre}}</td>
                            <td align = "right" v-if = "Impuesto2">@{{formatoVenta(App.Impuesto2)}}</td>
                          </tr>
                          <tr>
                            <td v-if = "Impuesto3">@{{Impuesto3.cConImpuesto3Nombre}}</td>
                            <td align = "right" v-if = "Impuesto3">@{{formatoVenta(App.Impuesto3)}}</td>
                          </tr>
                          <tr>
                            <td v-if = "Impuesto4">@{{Impuesto4.cConImpuesto4Nombre}}</td>
                            <td align = "right" v-if = "Impuesto4">@{{formatoVenta(App.Impuesto4)}}</td>
                          </tr>
                          <tr>
                            <td>Fee</td>
                            <td align = "right">@{{formatoVenta(App.Fee)}}</td>
                          </tr>
                          <tr>
                            <td><b>Totals</b></td>
                            <td align = "right">@{{formatoVenta(App.Total)}}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </template>
            </div>
          </div>
        </div>
      </template>
      <template v-else>
        <template v-if="isLoading">
          <p class="text-center">
            <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
            <span>Cargando...</span>
          </p>
        </template>
        <template v-else>
          <p class="text-center"><small>Completa los campos para mostrar un reporte</small></p>
        </template>
      </template>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-tables-2.min.js"></script>
<script src="/vuejs/reportes/app-sales_report.js"></script>
@endsection
