@extends('Master') @section('titulo','Reporte de venta por empleado') @section('styles')
<link rel="stylesheet" href="/css/toastr.css">
<link rel="stylesheet" href="/css/vue-multiselect.min.css"> @endsection @section('content')

<div id="reporteEmpleado">
  <div class="row">
    <div class="col-xs-12 col-sm-3 hidden-print">
      <form @submit.prevent="generarReporte">
        <div class="box box-primary">
          <div class="box-header with-border text-center">
            <h3 class="box-title">Filtros de búsqueda</h3>
          </div>

          <div class="box-body">
            <div class="form-group">
              <label for="fechaDesde">Desde *</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input id="datePicker" name="fechaDesde" v-model="fechaDesde" type="date" class="form-control" required>
              </div>
            </div>
            <div class="form-group">
              <label for="fechaHasta">Hasta *</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input name="fechaHasta" v-model="fechaHasta" type="date" class="form-control" required>
              </div>
            </div>
            <button type="submit" class="btn btn-primary btn-sm btn-block">Mostrar reporte</button>
          </div>
        </div>
      </form>
    </div>

    <div class="col-xs-12 col-sm-9">
      <div class="box box-primary">
        <div class="box-header with-border text-center">
          <h3 class="box-title">Reporte</h3>
          <template v-if="reporte_informacion && reporte_resultados != ''">
	                	<div class="text-center">
	                		DESDE <b>@{{ reporte_informacion.fechaDesde }}</b> HASTA <b>@{{ reporte_informacion.fechaHasta }}</b>
	                		<button class="btn btn-primary hidden-print pull-right" onclick="javascript:window.print()">
					            <i class="fa fa-fw fa-print"></i> Imprimir
				          	</button>
							<br>
	                	</div>
	                </template>
        </div>

        <div class="box-body">
          <div class="col-xs-12">
            <template v-if="isLoading == 0">
		            		<template v-if="reporte_resultados">
		            			<template v-if="reporte_resultados == ''">
		            				<p class="text-center"><small>No se han obtenido resultados con los parámetros seleccionados...</small></p>
		            			</template>
            <template v-else>
				            		<div class="table-responsive">
					            		<table class="table table-hover table-striped">
                            <thead>
                              <tr>
                                <th class="text-center"> Posición </th>
                                <th class="text-center"style="width:60%"> Empleado </th>
                                <th class = "text-center"> Subtotal</th>
                                <th class = "text-center"> Total</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr v-for="(reporte, index) in reporte_resultados">
                                <td class="text-center">  @{{ index+1 }} </td>
                                <td class="text-left">  <small>@{{ reporte.cHisProEmpNum }} </small> | @{{ reporte.cHisProEmpNom }} </td>
                                <td class="text-center">  @{{ formatoMoneda(reporte.Subtotal) }} </td>
                                <td class="text-center">  @{{ formatoMoneda(reporte.Total) }} </td>
                              </tr>
                            </tbody>
					            		</table>
				            		</div>
			            		</template>
            </template>
            <template v-else>
		            			<p class="text-center"><small>Completa los campos para mostrar un reporte</small></p>
		            		</template>
            </template>
            <template v-else-if="isLoading == 1">
		            		<p class="text-center">
		            			<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
								<span>Cargando...</span>
		            		</p>
		            	</template>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection @section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-reporteempleado.js?ver=1.0.2"></script> {{-- Script VueJS principal. --}} @endsection
