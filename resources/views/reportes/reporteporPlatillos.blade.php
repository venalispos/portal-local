@extends('Master')
@section('titulo','Reporte platillos')
@section('content')
<div id="reportePlatillos">
	<div class="row">
		<div class="col-xs-12 ">
			<form @submit.prevent="obtenerPlatillos()" > 
				<div class="box box-success">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Filtros de búsqueda</h3>
					</div>

					<div class="box-body">
						<div class="row">
							<div class="col-sm-4 col-md-3" 	>
								<div class="form-group" >
									<label for="">Categorias*</label>
									<multiselect 
									placeholder='Seleccione una categoria' 
									v-model='categoriasmSeleccionadas'
									:options='categoriasMSel' 
									label='cCatCodigo'
									:multiple="true"
									:custom-label='getCategorias'> 
								</multiselect>
							</div>
						</div>
						<div class="col-sm-3 col-md-3" >
							<div class="form-group" >
								<label for="fechaDesde">Desde *</label>
								<div class="input-group"  > 
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaDesde" style="padding-left: 30%;" v-model="fechaDesde" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-md-3">
							<div class="form-group" >
								<label for="fechaHasta">Hasta *</label>
								<div class="input-group" >
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaHasta" style="padding-left: 30%;" v-model="fechaHasta" type="date" class="form-control" required>
								</div>
							</div>
							
						</div>	
						<div class="col-sm-2">
							<br>
							<div class="form-group">
								<button type="submit" class="btn btn-success btn-sm pull-right">Mostrar reporte</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div> 
<div class="row">
	<template v-if='productos'>
		<div class="col-md-12">
			<div class="form-group">
				<a class="btn btn-app" @click='generarPDF()'>
					<i class="fa fa-download"></i> PDF
				</a>
				<a class="btn btn-app" @click='generarReporteExcel()'> 
					<i class="fa fa-download"></i> EXCEL
				</a>
			</div>
			<div class="table-responsive">
				<table class="table  table-striper">
					<thead>
					</thead>
					<tbody>
						<template v-for='categoria in categorias'>
							<tr>
								<td style="font-style: bold;"><h5><strong>@{{categoria.cCatDescripcion}}</strong></h5></td>
								<template >
									<table class="table  table-striper" width="100%">
										<thead> 
											<th></th>
											<tbody>
												<template v-for='subcategoria in subcategorias' v-if='subcategoria.cSCatPadre==categoria.cCatCodigo' >
													<tr>
														<td width="10%">@{{subcategoria.cSCatDescripcion}}</td>
														<template>
															<table class="table table-hover table-striper" width="100%">
																<thead>
																	<th width="40%">DESCRIPCION</th>
																	<th width="15%" style="text-align: center;">CANTIDAD</th>
																	<th width="15%" style="text-align: right;">PORCENTAJE SUBCATEGORIA</th>
																	<th width="15%" style="text-align: right;">PORCENTAJE CATEGORIA</th>
																	<th width="15%" style="text-align: right;">PORCENTAJE GENERAL</th>
																</thead> 
																<tbody>
																	<template v-for='producto in productos' v-if='subcategoria.cSCatCodigo == producto.cHisProSubCategoria'>
																		<tr>
																			<td width="40%">@{{producto.cHisProDesTicket}}</td>
																			<td width="15%" style="text-align: center;">@{{formateardosDecimales(producto.cantidad)}}</td>

																			<template v-for='totalsubcat in totalsubcategorias' v-if='subcategoria.cSCatCodigo == totalsubcat.cHisProSubCategoria'>
																				<td width="15%" style="text-align: right;">@{{porcentajesubcategoria(totalsubcat.cantidad,producto.cantidad)}} %</td>
																			</template>
																			
																			<template v-for='totalcat in totalcategorias' v-if='totalcat.categoria == producto.cHisProCategoria'>
																				<td width="15%" style="text-align: right;">@{{porcentajecategoria(totalcat.cantidad,producto.cantidad)}} %</td>
																			</template>
																			<template v-for='general in totalgeneral'>
																				<td align="right"> @{{porcentajeTotal(general.cantidad,producto.cantidad)}} %</td>
																			</template>
																		</tr> 
																	</template>
																</tbody>
																<tr>
																	<td style="text-align: left;" width="40%"><h5><strong>TOTAL @{{subcategoria.cSCatDescripcion}} </strong></h5></td>
																	<template v-for='totalcat in totalsubcategorias' v-if='subcategoria.cSCatCodigo == totalcat.cHisProSubCategoria'>
																		<td style="text-align: center;"><strong>@{{formateardosDecimales(totalcat.cantidad)}}</strong></td>

																		<template v-for='totalsubcat in totalsubcategorias' v-if='subcategoria.cSCatCodigo == totalsubcat.cHisProSubCategoria'>
																			<td width="15%" style="text-align: right;"><strong>@{{porcentajesubcategoria(totalsubcat.cantidad,totalcat.cantidad)}} % </strong></td>
																		</template>

																		<template v-for='totalcate in totalcategorias' v-if='totalcate.categoria == subcategoria.cSCatPadre'>
																			<td width="15%" style="text-align: right;"><strong>@{{porcentajecategoria(totalcate.cantidad,totalcat.cantidad)}} % </strong></td>
																		</template>
																		
																		<template v-for='general in totalgeneral'>
																			<td align="right"><strong> @{{porcentajeTotal(general.cantidad,totalcat.cantidad)}} % </strong></td>
																		</template>
																	</template>
																</tr>
															</table>
														</template>
													</tr>
												</template>
											</tbody>
											<th></th>
										</thead>
									</table>
								</template>
							</tr>
							<tr>
								<th ><h5><strong>TOTAL @{{categoria.cCatDescripcion}} </strong></h5></th>
								<template v-for='totalcategoria in totalcategorias' v-if='totalcategoria.categoria == categoria.cCatCodigo'>
									<td ><h5><strong>@{{formateardosDecimales(totalcategoria.cantidad)}}</strong></h5></td>
									<td><br></td>
								</template>
							</tr>
						</template>
						<tr  style="background-color: beige;">
							<td><strong><h5>TOTAL GENERAL</h5></strong></td>
							<template v-for='general in totalgeneral'>
								<td ><strong><h5>@{{general.cantidad}}</h5></strong></td>
							</template>

						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</template>
		<template v-else>
			<template v-if="isLoading">
				<p class="text-center"> 
					<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
					<span>Cargando...</span>
				</p>
			</template>
			<template v-if='sinregistro'> 
				<h3 align="center"><small>No existe un reporte para mostrar</small></h3>
			</template>
			<template v-else>
				<p class="text-center"> 
					<small>Complete los campos para mostrar un reporte</small>
				</p>
			</template>
		</template>
	</div>
</div>
@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
	
	
<script src="/vuejs/app-reportePlatillos.js"></script>
@endsection
