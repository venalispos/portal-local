@extends('Master')
@section('titulo','Reporte por descuentos')
@section('content')
<div id="reporteDescuento">
	<div class="row">
		<div class="col-xs-12 ">
			<form @submit.prevent="repDescuentos()" >
				<div class="box box-success">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Filtros de búsqueda</h3>
					</div>

					<div class="box-body">
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaDesde">Desde *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaDesde" v-model="fechaDesde" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaHasta">Hasta *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaHasta" v-model="fechaHasta" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<br>
							<button type="submit" class="btn btn-success btn-sm ">Mostrar reporte</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Ticket</th>
							<th>Codigo</th>
							<th>Descripcion</th>
							<th>precio</th>
							<th>Descuento</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<template v-for='descuento in array_Descuento'>
							<tr>
								td
							</tr>
						</template>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-tables-2.min.js"></script>
<script src="/vuejs/app-repDescuentos.js"></script>
@endsection
