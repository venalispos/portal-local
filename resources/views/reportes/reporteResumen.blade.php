@extends('Master')
@section('titulo','Resumen')
@section('content')
<div id="resumen">
	<div class="row">
		<div class="col-xs-12 ">
			<form @submit.prevent="generarReporteResumen()">
				<div class="box box-success">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Filtros de búsqueda</h3>
					</div>

					<div class="box-body">
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaDesde">Desde *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaDesde" v-model="fechaDesde" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaHasta">Hasta *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaHasta" v-model="fechaHasta" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<br>
							<button type="submit" class="btn btn-success btn-sm ">Mostrar reporte</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 ">
			<div class="box box-success">
				<div class="box-header with-border " align="center" style="text-align: center;">
					<h3 class="box-title">Reporte</h3>
					<div class="row">
						<template v-if='mostrarbotonDescarga'>
							<div class="col-md-4">
								<a class="btn btn-app" @click='descargarPDFresumen()'>
									<i class="fa fa-download" aria-hidden="true"></i> PDF
								</a>
								<a class="btn btn-app" @click='GenerarExcelResumen()'>
									<i class="fa fa-download" aria-hidden="true"></i> EXCEL
								</a>
							</div>
						</template>

					</div>
				</div>

				<div class="box-body">
					<div class="col-xs-12">
						<template v-if='reporteResumen'>
							<div class="box-body">
								<div class="table-responsive">

								</div>
							</div>
							<div class="box-body">
								<h1 align="center" style="font-size:21px;"><strong>VENTAS POR CATEGORIAS</strong></h1>
								<h1 align="center" style="font-size:21px;">@{{fechaDesde}} / @{{fechaHasta}}</h1>
								<div class="row">
									<div class=" form-group">
										<div class="table-responsive">
											<table class="table table-hover">
												<thead>
													<th>CAJA</th>
													<th>CATEGORIAS</th>
													<th align="right" style="text-align: right;">SUBTOTAL</th>
													<th align="right" style="text-align: right;">CORTESIAS</th>
													<th align="right" style="text-align: right;">DESCUENTOS</th>
													<th align="right" style="text-align: right;">CANCELACIONES</th>
													<th align="right" style="text-align: right;">IVA</th>
													<th align="right" style="text-align: right;">TOTAL</th>
												</thead>
												<tbody>
													<template v-for='data in reporteResumen'>
														<tr>
															<td>@{{data.cHisProSerieLiq}}</td>
															<td>@{{data.cCatDescripcion}}</td>
															<td align="right" style="text-align: right;">@{{formatearMoneda(data.Subtotal)}}</td>
															<td align="right" style="text-align: right;">@{{formatearMoneda(data.Cortesias)}}</td>
															<td align="right" style="text-align: right;">@{{formatearMoneda(data.Descuento)}}</td>
															<td align="right" style="text-align: right;">@{{formatearMoneda(data.Cancelaciones)}}</td>
															<td align="right" style="text-align: right;">@{{formatearMoneda(data.Iva)}}</td>
															<template v-for='total in totalResumen' v-if='total.cCatDescripcion == data.cCatDescripcion && total.cHisProSerieLiq == data.cHisProSerieLiq'>
																<td align="right" style="text-align: right;">@{{formatearMoneda(total.Total)}}</td>
															</template>
														</tr>
													</template>
													<tr>
														<th align="right" style="text-align: right;"><strong>TOTAL</strong></th>
														<th></th>
														<th align="right" style="text-align: right;">@{{formatearMoneda(Subtotal)}}</th>
														<th align="right" style="text-align: right;">@{{formatearMoneda(Cortesias)}}</th>
														<th align="right" style="text-align: right;">@{{formatearMoneda(Descuentos)}}</th>
														<th align="right" style="text-align: right;">@{{formatearMoneda(Cancelaciones)}}</th>
														<th align="right" style="text-align: right;">@{{formatearMoneda(Iva)}}</th>
														<th align="right" style="text-align: right;">@{{formatearMoneda(totalTotales)}}</th>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<h3 align="center" width="50%" style="width: 50%;"><strong>FORMAS DE PAGO</strong></h3>
										<div class="table-responsive">
											<table class="table table-hover" width="50%" style="width: 50%;">
												<thead>
													<th>FORMAS DE PAGO</th>

													<th align="right" style="text-align: right;">MONTO</th>
												</thead>
												<tbody>
													<tr>
														<td>EFECTIVO</td>
														<td align="right" style="text-align: right;">@{{ formatearMoneda(Efectivo) }} </td>
													</tr>
													<template v-for='metodoPago in reporteFormasPago'>
														<tr>
															<td>@{{metodoPago.cCatForDesc}}</td>
															<td align="right" style="text-align: right;">@{{formatearMoneda(metodoPago.monto)}}</td>
														</tr>
													</template>
													<tr>
														<th align="right" style="text-align: right;"><strong>TOTAL</strong></th>
														<th align="right" style="text-align: right;">@{{formatearMoneda(Monto)}}</th>

													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</template>
						<template v-else>
							<template v-if="isLoading">
								<p class="text-center">
									<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
									<span>Cargando...</span>
								</p>
							</template>
							<template v-if='sinRegistros'>
								<p class="text-center">
									<small>No existen Registros en el rango de fecha @{{fechaDesde}} / @{{fechaHasta}}</small>
								</p>
							</template>
							<template v-else>
								<p class="text-center">
									<small>Complete los campos para mostrar un resumen</small>
								</p>
							</template>
						</template>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-tables-2.min.js"></script>
<script src="/vuejs/app-repResumen.js"></script>
@endsection