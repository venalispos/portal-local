@extends('Master')
@section('titulo','Reportes Corte')
@section('content')
<div id="reporteporticket">
    <div class="row">
        <div class="col-xs-12 ">
            <form @submit.prevent="generarReporte()">
                <div class="box box-primary">
                    <div class="box-header with-border text-center">
                        <h3 class="box-title">Filtros de búsqueda</h3>
                    </div>

                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fechaDesde">Desde *</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="fechaDesde" v-model="fechaDesde" type="date" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fechaHasta">Hasta *</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="fechaHasta" v-model="fechaHasta" type="date" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary btn-sm ">Mostrar reporte</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 ">
            <div class="box box-primary">
                <div class="box-header with-border ">
                    <div class="col-md-2">
                        <h3 class="box-title">Reporte</h3>
                    </div>
                    <template v-if="reporte">
                        <div class="col-md-10">
                            <template v-if="cargapdf == 0">
                                <a class="btn btn-app" style="background-color: red;color: white" @click='descargarPDFCorte()'>
                                    <i style="color: white;" class="fa fa-edit"></i> PDF
                                </a>
                            </template>
                            <template v-else>
                                <button type="submit" class="btn btn-small btn-dark float-right" disabled>
                                    <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
                                    <span> PDF </span>
                                </button>
                            </template>
                            <template v-if="cargaexcel == 0">
                                <a class="btn btn-app" style="background-color: green;color: white" @click='GenerarExcelCorte()'>
                                    <i style="color: white;" class="fa fa-edit"></i> EXCEL
                                </a>
                            </template>
                            <template v-else>
                                <button type="submit" class="btn btn-small btn-dark" disabled>
                                    <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
                                    <span> Excel </span>
                                </button>
                            </template>
                        </div>
                    </template>
                </div>
                <div class="box-body">
                    <div class="col-xs-8">
                        <template v-if="isLoading == 0">
                            <template v-if="reporte">

                                <h3>Caja</h3>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">

                                            </th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">EFECTIVO</td>
                                            <td align="right"> $@{{ formatPrice(reporte.efectivo) }} </td>
                                        </tr>
                                        <template v-for="report in reporte.caja">
                                            <tr>
                                                <td align="center"> @{{ report.cCatAgForDescripcion }} </td>
                                                <td align="right"> $@{{ formatPrice(report.suma) }} </td>
                                            </tr>
                                        </template>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <template v-for="totales in reporte.totalesysubtotales">
                                            <tr>
                                                <td class="text-center">Subtotal</td>
                                                <td class="text-right">$@{{ formatPrice(totales.Subtotal) }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Impuesto</td>
                                                <td class="text-right">$@{{ formatPrice(totales.Impuesto) }}</td>
                                            </tr>
                                        </template>
                                        <tr>
                                            <td align="right">Total</td>
                                            <td align="right"> $@{{ formatPrice(reporte.totalcaja) }} </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h3>Forma de Pago Ventas</h3>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">

                                            </th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center">EFECTIVO</td>
                                            <td align="right"> $@{{ formatPrice(reporte.efectivo) }} </td>
                                        </tr>
                                        <template v-for="report in reporte.formapago">
                                            <tr>
                                                <td align="center"> @{{ report.categoriaformapago.cCatForDesc }} </td>
                                                <td align="right"> $@{{ formatPrice(report.Cantidad) }} </td>
                                            </tr>
                                            <tr></tr>
                                        </template>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <template v-for="totales in reporte.totalesysubtotales">
                                            <tr>
                                                <td class="text-center">Subtotal</td>
                                                <td class="text-right">$@{{ formatPrice(totales.Subtotal) }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Impuesto</td>
                                                <td class="text-right">$@{{ formatPrice(totales.Impuesto) }}</td>
                                            </tr>
                                        </template>
                                        <tr>
                                            <td align="right">Total</td>
                                            <td align="right"> $@{{ formatPrice(reporte.totalformapago) }} </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h3>Descuentos y Cortesias</h3>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Comensales</th>
                                            <th class="text-right">@{{ reporte.comensales.CantidadClientes }}</th>
                                        </tr>

                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-right"><h4>Cortesias</h4></th>
                                        </tr>
                                        <template v-for="deducciones in reporte.deducciones">
                                            </tr>
                                            <tr>
                                                <th class="text-center">@{{ deducciones.categorias.cCatDescripcion }}</th>
                                                <th class="text-right">$@{{ formatPrice(deducciones.Cortesias) }}</th>
                                            </tr>
                                        </template>
                                        <tr>
                                            <th class="text-right">Total</th>
                                            <th class="text-right"> $@{{ formatPrice(reporte.totalcortesias.Cortesias) }} </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-right"><h4>Descuentos</h4></th>
                                        </tr>
                                        <template v-for="deducciones in reporte.deducciones">

                                            <tr>
                                                <th class="text-center">@{{ deducciones.categorias.cCatDescripcion }}</th>
                                                <th class="text-right">$@{{ formatPrice(deducciones.Descuentos) }}</th>
                                            </tr>
                                        </template>
                                        <tr>
                                            <th class="text-right">Total</th>
                                            <th class="text-right"> $@{{ formatPrice(reporte.totaldescuentos.Descuentos) }} </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </template>
                        </template>
                        <template v-else-if="isLoading == 1">
                            <td colspan="10">
                                <p class="text-center">
                                    <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
                                    <span>Cargando...</span>
                                </p>
                            </td>
                        </template>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-tables-2.min.js"></script>
<script src="/vuejs/app-repcorte.js"></script>
@endsection