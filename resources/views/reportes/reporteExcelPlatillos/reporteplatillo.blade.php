<?php 
$fecha="";
$fecha = date('d/m/Y H:m');
?>

		<tr></tr>
		<tr></tr>
	<table width="100%" style="font-family:Courier;">
				<tr>
					<th  align="center" colspan="3" style=" font-family:Courier;font-style: bold; font-size:27px; text-align: center; ">
						@php print_r($info_encabezado['empresaSeleccionada'][0]['cConEmpresa']); @endphp
					</th>
				</tr>	
				<tbody>
					<tr>
						<td align="left" width="180" style="font-family:Courier; font-size:20px" >
							@php print_r($info_encabezado['empresaSeleccionada'][0]['cConSucursal']); @endphp 
						</td>
						<td  align="center" style="font-family:Courier; font-size:26px; text-align: center; ">
							REPORTE POR PLATILLO
						</td>
						<td style="font-family:Courier ; font-size:18px"  width="180">{{$fecha}}</td>
					</tr>
					<tr>
						<td></td>
						<td align="center" style="font-family:Courier ; font-size:26px;" >
							{{$info_encabezado['fecha']}}						</td>
						<td>
						</td>
					</tr>
					<tr >
						<td colspan="3" style="padding-bottom: 15px;"><hr></td>
					</tr> 
				</tbody>
			</table>
<tr>
	
</tr>
<tr></tr>
<tr></tr>


	<table width="100%"  >
		
		<tr>
			<td></td>
		</tr>
		@foreach($dataGPDF['categorias'] as $categoria)

		<tr>
			<td><?php echo "<span  style='font-weight:bold !important; font-size:23px !important;'>".$categoria['cCatDescripcion']."</span>"; ?></td>
			 <tr>
			 	<td></td> 
			 </tr>
			<table    width="100%" cellpadding="0" cellspacing="0">
				@foreach($dataGPDF['subcategorias'] as $subcategoria) 
				@if( $subcategoria['cSCatPadre'] == $categoria['cCatCodigo'])
				<tr>
					<td width="25%"><strong><?php echo '<span style="font-style: bold !important; font-size:18px !important;padding-left: 2%;background-color:orange !important; ">'.$subcategoria["cSCatDescripcion"].'</span>'; ?></strong></td>
					<tr >
						<thead>
							<?php echo "<th width='50%' align='left'  style='padding-left: 5%'>DESCRIPCION</th>"; ?>
							<th width="10%" style="text-align: right;">CANTIDAD</th>
							<th width="15%" style="text-align: right;">PORCENTAJE SUBCATEGORIA</th> 
							<th width="15%" style="text-align: right;">PORCENTAJE CATEGORIA</th>
							<th width="15%" style="text-align: right;">PORCENTAJE GENERAL</th>
						</thead> 
						<tbody>
							@foreach($dataGPDF['productos'] as $producto)
							@if($subcategoria['cSCatCodigo']== $producto['cHisProSubCategoria'])
							<tr>
								<?php echo "<td width='50%' >".$producto['cHisProDesTicket']."</td>"; ?>
								<td width="10%" style="text-align: right;"><?php  echo number_format($producto['cantidad'],2); ?></td>
								@foreach($dataGPDF['totalsubcategorias'] as $totalsubcategoria)
								@if($subcategoria['cSCatCodigo'] == $totalsubcategoria['cHisProSubCategoria'])
								<td width="15%" style="text-align: right;"> <?php echo number_format(($producto['cantidad']/$totalsubcategoria['cantidad'])*100,2); ?> %</td>
								@endif
								@endforeach
								@foreach( $dataGPDF['totalcategorias'] as $totalcat)
								@if($totalcat['categoria'] == $producto['cHisProCategoria'])
								<td width="15%" style="text-align: right;"><?php echo number_format(($producto['cantidad']/$totalcat['cantidad'])*100,2); ?> %</td>
								@endif
								@endforeach  
								@foreach($dataGPDF['totalgeneral'] as $general)
								<td align="right"> <?php echo number_format(($producto['cantidad']/$general['cantidad'])*100,2); ?> %</td>
								@endforeach
							</tr>
							@endif
							@endforeach
						</tbody>
						<tr>
							<th style=" text-align: right;" width="45%">TOTAL {{$subcategoria['cSCatDescripcion']}} </th>
							@foreach($dataGPDF['totalsubcategorias'] as $totalcat)
							@if($totalcat['cHisProSubCategoria'] == $subcategoria['cSCatCodigo'])
							<td style="text-align: right;"><strong> <?php echo number_format($totalcat['cantidad'],2); ?> </strong></td>
							@foreach($dataGPDF['totalsubcategorias'] as $totalsubcat)
							@if($totalsubcat['cHisProSubCategoria'] == $subcategoria['cSCatCodigo'])
							<td width="15%" style="text-align: right;"><strong><?php echo number_format(($totalcat['cantidad']/ $totalsubcat['cantidad'])*100,2); ?> % </strong></td>
							@endif
							@endforeach

							@foreach($dataGPDF['totalcategorias']  as $totalcate)
							@if($totalcate['categoria'] == $subcategoria['cSCatPadre'])
							<td width="15%" style="text-align: right;"><strong><?php echo number_format(($totalcat['cantidad'] / $totalcate['cantidad'])*100,2); ?> % </strong></td>
							@endif
							@endforeach
							@foreach($dataGPDF['totalgeneral'] as $general)
							<td width="15%" style="text-align: right;"><strong><?php echo number_format(( $totalcat['cantidad'] / $general['cantidad'])*100,2); ?> % </strong></td>
							@endforeach
							@endif
							@endforeach
						</tr>
					</tr>
				</tr>
				<tr>
					<td></td>
				</tr>
				@endif
				@endforeach
			</table>
		</tr>
		<tr>
			<td></td>
		</tr>
		<table width="100%">
			<tr>
			<th width="45%"><strong><?php echo '<span width="10%" style="font-style: bold !important; font-size:22px !important; text-align:left;">TOTAL '.$categoria['cCatDescripcion'].'</span>'; ?> </strong></th>
			@foreach($dataGPDF['totalcategorias'] as $totalcategoria)
			@if( $totalcategoria['categoria']  == $categoria['cCatCodigo'])
			<td  style="text-align: right;"><strong> <?php echo '<span width="90%" style="font-style: bold !important;text-align:left; font-size:23px !important; ">'.number_format($totalcategoria['cantidad'],2).'</span>'; ?></strong></td>
			@endif
			@endforeach
		</tr>
		</table>

		@endforeach
		<table width="50%"  align="right">
			<tr>
			<td style="background-color: beige;" width="30%" style="font-size:24px !important;"><strong>TOTAL GENERAL</strong></td>
			@foreach( $dataGPDF['totalgeneral'] as $general)
			<td  width="20%" style="background-color: beige;"><strong><?php echo '<span width="10%" style="font-style: bold !important; font-size:24px !important; text-align:left;background-color: beige;"> '.number_format($general['cantidad'],2).'</span>'; ?></strong></td>
			@endforeach
		</tr>
		</table>
	</table> 
</body>
</html>