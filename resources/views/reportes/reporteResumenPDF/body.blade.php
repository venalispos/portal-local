<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Document</title>
</head>

<body>
	<div>
		<table width="100%">
			<tr style="font-family:Courier;font-size:14px;">
				<th>CAJA</th>
				<td><strong>CATEGORIAS</strong></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"> <strong>SUBTOTAL</strong></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>CORTESIAS</strong></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>DESCUENTOS</strong></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>CANCELACIONES</strong></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>IVA</strong></td>
				<td width="20%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>TOTAL</strong></td>
			</tr>
			@foreach($dataGPDF['HistorialCategorias'] as $historial)
			<tr style="font-family:Courier;font-size:14px;">
				<td>{{$historial['cHisProSerieLiq']}}</td>
				<td>{{$historial['cCatDescripcion']}}</td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($historial['Subtotal'],2) @endphp</td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($historial['Cortesias'],2) @endphp</td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($historial['Descuento'],2) @endphp</td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($historial['Cancelaciones'],2) @endphp</td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($historial['Iva'],2) @endphp</td>
				@foreach($dataGPDF['totalResumen'] as $total)
				@if($total['cCatDescripcion'] == $historial['cCatDescripcion'] && $total['cHisProSerieLiq'] == $historial['cHisProSerieLiq'])
				<td width="20%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($total['Total'],2) @endphp</td>
				@endif
				@endforeach
			</tr>
			@endforeach
			<tr>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>TOTAL</strong></td>
				<td></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>@php echo number_format($dataGPDF['Subtotal'],2) @endphp</strong></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>@php echo number_format($dataGPDF['Cortesias'],2) @endphp</strong></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>@php echo number_format($dataGPDF['Descuentos'],2) @endphp</strong></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>@php echo number_format($dataGPDF['Cancelaciones'],2) @endphp</strong></td>
				<td width="14%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>@php echo number_format($dataGPDF['Iva'],2) @endphp</strong></td>
				<td width="20%" style="text-align: right;font-family:Courier;font-size:14px;"><strong>@php echo number_format($dataGPDF['totales'],2) @endphp</strong></td>
			</tr>
		</table>
	</div>
	<div>
		<br><br><br>
		<div>
			<table width="50%">
				<tr style="font-family:Courier;font-size:14px;">
					<td><strong>FORMAS DE PAGO</strong></td>
					<td align="right" style="text-align: right;"><strong>MONTO</strong></td>
				</tr>
				<tr style="font-family:Courier;font-size:14px;">
					<td>EFECTIVO</td>
					<td align="right" style="text-align: right;">@php echo number_format($dataGPDF['Efectivo'],2 )@endphp</td>
				</tr>
				@foreach($dataGPDF['HistorialFormasPagos'] as $pagos)
				<tr style="font-family:Courier;font-size:14px;">
					<td>{{$pagos['cCatForDesc']}}</td>
					<td style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($pagos['monto'],2) @endphp</td>
				</tr>
				@endforeach
				<tr style="font-family:Courier;font-size:14px;">
					<th align="right" style="text-align: right;"><strong>TOTAL</strong></th>
					<th align="right" style="text-align: right;">@php echo number_format($dataGPDF['Monto'],2 )@endphp</th>

				</tr>
			</table>
		</div>
	</div>
</body>

</html>