<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Document</title>
</head>

<body>
    <div>
        <table border="0" width="100%">
            <tr>
                <td style="font-family:Courier;font-size:14px;"><strong>TICKET</strong></td>
                <td style="font-family:Courier;font-size:14px;"><strong>FECHA</strong></td>
                <td style="font-family:Courier;font-size:14px;"><strong>MESA</strong></td>
                <td style="text-align: right;font-family:Courier;font-size:14px;"><strong>SUBTOTAL</strong></td>
                <td style="text-align: right;font-family:Courier;font-size:14px;"><strong>IVA</strong></td>
                <td style="text-align: right;font-family:Courier;font-size:14px;"><strong>TOTAL</strong></td>
            </tr>
            @foreach($dataPDF['reporte'] as $ticket)
            @if ($ticket->cHisMesVisible == 1)
                <tr>
                    <td style="font-family:Courier;font-size:14px;">{{$ticket->cHisMesTicket}}</td>
                    <td style="font-family:Courier;font-size:14px;">{{$ticket->cHisMesFechaRep}}</td>
                    <td style="font-family:Courier;font-size:14px;">{{$ticket->cHisMesNombre}}</td>
                    <td style="text-align: right;font-family:Courier;font-size:14px;"> $@php echo
                        number_format($ticket->cHisMesSubTotal,2) @endphp</td>
                    <td style="text-align: right;font-family:Courier;font-size:14px;"> $@php echo
                        number_format($ticket->cHisMesTotalImpuesto1,2) @endphp</td>
                    <td style="text-align: right;font-family:Courier;font-size:14px;"> $@php echo
                        number_format($ticket->cHisMesTotal,2) @endphp</td>
                </tr>
            @endif
            @endforeach
            <tr>
                <td align="right" colspan="3" style="text-align: right;font-family:Courier;font-size:14px;">
                    <strong>TOTALES</strong></td>
                <td style="text-align: right;font-family:Courier;font-size:14px;"><strong> $@php echo
                        number_format($dataPDF['subtotal'],2) @endphp </strong></td>
                <td style="text-align: right;font-family:Courier;font-size:14px;"><strong> $@php echo
                        number_format($dataPDF['iva'],2) @endphp </strong></td>
                <td style="text-align: right;font-family:Courier;font-size:14px;"><strong> $@php echo
                        number_format($dataPDF['total'],2) @endphp </strong></td>
            </tr>
        </table>
    </div>
</body>

</html>
