@extends('Master')
@section('titulo','Reporte')
@section('content')
<div id="rastrillo">
    <div class="row">
        <div class="col-xs-12 ">
            <form @submit.prevent="generarReporte()">
                <div class="box box-success">
                    <div class="box-header with-border text-center">
                        <h3 class="box-title"> Filtros de búsqueda </h3>
                    </div>

                    <div class="box-body">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="fechaDesde"> Desde * </label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="fechaDesde" v-model="consulta.fechaDesde" type="date"
                                        class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="fechaHasta"> Hasta * </label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="fechaHasta" v-model="consulta.fechaHasta" type="date"
                                        class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="FormaPago"> Forma de Pago </label>
                                <select class="form-control" v-model="consulta.formaPago">
                                    <option value="0"> TODAS </option>
                                    <option v-for="item in catFormasPago" :value="item.cCatForId">
                                        @{{ item.cCatForDesc }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <br>
                            <button type="submit" class="btn btn-success btn-sm "> Mostrar reporte </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 ">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Reporte</h3>
                </div>
                <div class="box-body">
                    <template v-if="carga">
                        <center>
                            <div class="mx-auto">
                                <p>
                                    <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
                                    <span> Cargando...</span>
                                </p>
                            </div>
                        </center>
                    </template>
                    <template v-else>
                        <template v-if="reporte">
                            <div class="row">
                                <div class="col-sm-6">
                                    <template v-if="cargaPDF">
                                        <button type="submit" class="btn btn-danger btn-sm" disabled>
                                            <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
                                            <span> Exportar PDF </span>
                                        </button>
                                    </template>
                                    <template v-else>
                                        <button type="submit" class="btn btn-danger btn-sm"
                                            v-on:click.prevent="exportarPDF()">
                                            <i class="fa fa-file-pdf-o"></i>
                                            <span> Exportar PDF </span>
                                        </button>
                                    </template>
                                    <template v-if="resumenPDF">
                                        <button type="submit" class="btn btn-danger btn-sm" disabled>
                                            <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
                                            <span> Reporte Resumen </span>
                                        </button>
                                    </template>
                                    <template v-else>
                                        <button type="submit" class="btn btn-danger btn-sm"
                                            v-on:click.prevent="reporteResumenPDF()">
                                            <i class="fa fa-file-pdf-o"></i>
                                            <span> Reporte Resumen </span>
                                        </button>
                                    </template>
                                    <template v-if="resumenExcel">
                                        <button type="submit" class="btn btn-success btn-sm" disabled>
                                            <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
                                            <span> Reporte Resumen </span>
                                        </button>
                                    </template>
                                    <template v-else>
                                        <button type="submit" class="btn btn-success btn-sm"
                                            v-on:click.prevent="reporteResumenExcel()">
                                            <i class="fa fa-file-excel-o"></i>
                                            <span> Reporte Resumen </span>
                                        </button>
                                    </template>
                                </div>
                            </div>
                            <br>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="text-align:center"> Ticket </th>
                                        <th style="text-align:center"> Fecha </th>
                                        <th style="text-align:center"> Mesa </th>
                                        <th style="text-align:center"> Formas de Pago </th>
                                        <th style="text-align:center"> Total </th>
                                        <th style="text-align:center"> Facturado </th>
                                        <th style="text-align:center"> Visible </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <template v-if="reporte != 0">
                                        <tr v-for="item in reporte">
                                            <td style="text-align:center"> @{{ item.cHisMesTicket }} </td>
                                            <td style="text-align:center"> @{{ formatDate(item.cHisMesFechaRep) }} </td>
                                            <td style="text-align:center"> @{{ item.cHisMesNombre }} </td>
                                            <td style="text-align:left">
                                                <p v-for="forma in item.formaspago">
                                                    @{{ formatPrice(forma.cHisForCant) }} -
                                                    <span>@{{ forma.cHisForDes }}</span> <br>
                                                </p>
                                            </td>
                                            <td style="text-align:center"> @{{ formatPrice(item.cHisMesTotal) }} </td>
                                            <td style="text-align:center">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox"
                                                        v-model="item.cHisMesFacturado" @change="check(item)">
                                                </div>
                                            </td>
                                            <td style="text-align:center">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox"
                                                        v-model="item.cHisMesVisible" @change="check(item)">
                                                </div>
                                            </td>
                                        </tr>
                                    </template>
                                    <template v-else>
                                        <td colspan="12">
                                            <p style="text-align:center">No hay resultados que mostrar.</p>
                                        </td>
                                    </template>
                                </tbody>
                            </table>
                        </template>
                    </template>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/reportes/app-rastrillo.js"></script>
@endsection
