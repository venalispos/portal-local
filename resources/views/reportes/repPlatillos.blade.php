@extends('Master')
@section('titulo','Reporte por articulo')
@section('content')
<div id="reportePlatillos">
	<div class="row">
		<div class="col-xs-12 ">
			<form @submit.prevent="obtenerventaporArticulos()" > 
				<div class="box box-success">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Filtros de búsqueda</h3>
					</div>

					<div class="box-body">
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaDesde">Desde *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaDesde" v-model="fechaDesde" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaHasta">Hasta *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaHasta" v-model="fechaHasta" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<br>
							<button type="submit" class="btn btn-success btn-sm ">Mostrar reporte</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div> 
	<div class="row">
						<template v-if='mostrarbotonDescarga'>
							<div class="col-md-4">
							<a class="btn btn-app" @click='generarReportePDF()'>
								<i class="fa fa-edit"></i> PDF
							</a>
							<a class="btn btn-app" @click='generarReporteExcel()'>
								<i class="fa fa-edit"></i> EXCEL
							</a>
						</div>
						</template>
					</div>
	<div class="row">
		<template v-if='mostrarbotonDescarga'>
			<div class="col-md-sm-12">
				<div class="table-responsive">
					<table class="table table-hover table-striped" width="100%">
						<thead>
							<th width="30%" >PRODUCTO</th>
							<th width="20%" align="right" style="text-align: right;">CANTIDAD</th>
							<th width="20%" align="right" style="text-align: right;">VENTA TOTAL CON IVA</th>
							<th align="right" style="text-align: right;"><strong>PORCENTAJE</strong></th>
						</thead>
						<tbody>
							<template v-for='articulo in reporteArticulos'> 
								<tr>
									<td width="30%" aign="left" style="text-align: left;">@{{articulo.cProDescripcion}}</td>
									<td width="20%" align="right" style="text-align: right;">@{{formateardosDecimales(articulo.cantidadProductos)}}</td>
									<td width="20%" align="right" style="text-align: right;">@{{formatearMoneda(articulo.precio)}}</td>
									<td width="20%" align="right" style="text-align: right;">@{{calcularProcentaje(articulo.precio)}}%</td>
								</tr>
							</template>
							<template v-for='totales in totalesArticulos'>
								<tr style="font-style: bold">
									<td width="20%" align="right" style="text-align: right;"><strong>TOTALES</strong></td>
									<td width="20%" align="right" style="text-align: right;"><strong>@{{formateardosDecimales(totales.cantidadProductos)}}</strong></td>
									<td width="20%" align="right" style="text-align: right;"><strong>@{{formatearMoneda(totales.precio)}}</strong></td>
									<td width="20%" align="right" style="text-align: right;"><strong>100%</strong></td>
								</tr>
							</template>
						</tbody>
					</table>
				</div>
			</div>
		</template>
		<template v-else>
			<template v-if="isLoading">
				<p class="text-center"> 
					<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
					<span>Cargando...</span>
				</p>
			</template>
			<template v-else>	
				<p class="text-center"> 
					<small>Complete los campos para mostrar un reporte</small>
				</p>
			</template>
		</template>	
	</div>
</div>
@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-tables-2.min.js"></script>
<script src="/vuejs/app-reportePlatillo.js"></script>
@endsection
