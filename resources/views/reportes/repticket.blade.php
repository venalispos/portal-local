@extends('Master')
@section('titulo','Reportes por ticket')
@section('content')
<div id="reporteporticket">
	<div class="row">
		<div class="col-xs-12 ">
			<form @submit.prevent="generarReporte()" >
				<div class="box box-primary">
					<div class="box-header with-border text-center">
						<h3 class="box-title">Filtros de búsqueda</h3>
					</div>

					<div class="box-body">
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaDesde">Desde *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaDesde" v-model="fechaDesde" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="fechaHasta">Hasta *</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input name="fechaHasta" v-model="fechaHasta" type="date" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<br>
							<button type="submit" class="btn btn-primary btn-sm " @click='statusReporte(0);' id="BuscarSmezcla">Mostrar reporte</button>
							<button type="submit" class="btn btn-danger btn-sm " @click='statusReporte(1);' id="Buscarcmezcla" style="display: none;"><strong>Mostrar reporte</strong></button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 ">
			<div class="box box-primary">
				<div class="box-header with-border ">
					<h3 class="box-title">Reporte</h3>
					<div class="row">
						<template v-if='mostrarbotonDescarga'>
							<template v-if='botonPresionadoTicket == 1'>
								<div class="col-md-4">
									<a class="btn btn-app btn" style="background-color: red;color: white" @click='descargarPDF()'>
										<i style="color: white;" class="fa fa-edit"></i> PDF
									</a>
									<a class="btn btn-app" style="background-color: red;color: white" @click='GenerarExcelTicket()'>
										<i  style="color: white;" class="fa fa-edit"></i> EXCEL
									</a>
								</div>
							</template>
							<template v-if='botonPresionadoTicket == 0'>
								<div class="col-md-4">
									<a class="btn btn-app " @click='descargarPDF()'>
										<i class="fa fa-edit"></i> PDF
									</a>
									<a class="btn btn-app" @click='GenerarExcelTicket()'>
										<i class="fa fa-edit"></i> EXCEL
									</a>
								</div>
							</template>
						</template>
					</div>
				</div>

				<div class="box-body">
					<div class="col-xs-12">
						{{--
							<v-client-table :data="reporte_resultados" :columns="columnas" :options="options">
							</v-client-table>
							--}}
							<template v-if="reporte_resultados">
								<template v-if="botonPresionadoTicket==1">
									<div class="table-responsive">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<th class="text-center">
														<template v-if="selected_filtro == 'Ticket'">
															<a href="#" @click="aplicarFiltro('Ticket')"><u style="color: #337ab7;">Ticket</u> <i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a>

														</template>
														<template v-else>
															<a href="#" @click="aplicarFiltro('Ticket')" style="color:black">Ticket <i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a>
														</template>
													</th>
													<th class="text-center">Fecha</th>
													<th class="text-center" style="min-width: 125px">Mesa</th>
													<th align="right" style="text-align: right;">Subtotal</th>
													<th align="right" style="text-align: right;">IVA</th>
													<th align="right" style="text-align: right;">Total</th>
												</tr>
											</thead>
											<tbody>
												<template v-if="isLoading == 0">

													<tr v-for="reporte in reporte_resultados">
														<td align="center"> @{{ reporte.cHisMesTicket }} </td>
														<td align="center"> @{{ reporte.cHisMesFechaRep }} </td>
														<td align="center"> @{{ reporte.cHisMesNombre }} </td>
														<td align="right"> @{{ formatearMoneda(reporte.cHisMesSubTotal) }} </td>
														<td align="right"> @{{ formatearMoneda(reporte.cHisMesTotalImpuesto1) }}</td>
														<td align="right"> @{{ formatearMoneda(reporte.cHisMesTotal) }} </td>
													</tr>
													<tr style="font-weight: bold">
														<td align="right" colspan="3">Totales</td>
														<td  align="right">@{{formatearMoneda(subtotalticket)}}</td>
														<td  align="right">@{{formatearMoneda(totalImpuesto)}}</td>
														<td align="right">@{{formatearMoneda(totalTotales)}}</td>
													</tr>
												</template>
												<template v-else-if="isLoading == 1">
													<tr>
														<td colspan="10">
															<p class="text-center">
																<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
																<span>Cargando...</span>
															</p>
														</td>
													</tr>
												</template>
											</tbody>
										</table>
									</div>
								</template>

								<template v-if="botonPresionadoTicket==0">
									<div class="table-responsive">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<th class="text-center">
														<template v-if="selected_filtro == 'Ticket'">
															<a href="#" @click="aplicarFiltro('Ticket')"><u style="color: #337ab7;">Ticket</u> <i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a>

														</template>
														<template v-else>
															<a href="#" @click="aplicarFiltro('Ticket')" style="color:black">Ticket <i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a>
														</template>
													</th>
													<th class="text-center">Fecha</th>
													<th class="text-center">Hora entrada</th>
													<th class="text-center">Hora salida</th>
													<th class="text-center" style="min-width: 125px">Mesa</th>
													<th class="text-center" style="min-width: 125px">Mesero</th>
													<th align="right" style="text-align: right;">Subtotal</th>
													<th align="right" style="text-align: right;">IVA</th>
													<th align="right" style="text-align: right;">Total</th>
												</tr>
											</thead>
											<tbody>
												<template v-if="isLoading == 0">

													<tr v-for="reporte in reporte_resultados">
														<td align="center"> @{{ reporte.cHisMesTicket }} </td>
														<td align="center"> @{{ reporte.cHisMesFechaRep }} </td>
														<td align="center"> @{{ formatearHora(reporte.cHisMesFechaEntrada) }} </td>
														<td align="center"> @{{ formatearHora(reporte.cHisMesFechaSalida) }} </td>
														<td align="center"> @{{ reporte.cHisMesNombre }} </td>
														<td align="center"> @{{ reporte.cHisMesCajeroNombre }} </td>
														<td align="right"> @{{ formatearMoneda(reporte.cHisMesSubTotal) }} </td>
														<td align="right"> @{{ formatearMoneda(reporte.cHisMesTotalImpuesto1) }}</td>
														<td align="right"> @{{ formatearMoneda(reporte.cHisMesTotal) }} </td>
													</tr>
													<tr style="font-weight: bold">
														<td align="right" colspan="6">Totales</td>
														<td  align="right">@{{formatearMoneda(subtotalticket)}}</td>
														<td  align="right">@{{formatearMoneda(totalImpuesto)}}</td>
														<td align="right">@{{formatearMoneda(totalTotales)}}</td>
													</tr>
												</template>
												<template v-else-if="isLoading == 1">
													<tr>
														<td colspan="10">
															<p class="text-center">
																<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
																<span>Cargando...</span>
															</p>
														</td>
													</tr>
												</template>
											</tbody>
										</table>
									</div>
								</template>
							</template>
							<template v-else>
								<template v-if="isLoading">
									<p class="text-center">
										<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
										<span>Cargando...</span>
									</p>
								</template>
								<template v-else>
									<p class="text-center">
										<small>Complete los campos para mostrar un reporte</small>
									</p>
								</template>
							</template>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection
	@section('scripts')
	<script src="/js/axios.min.js"></script>
	<script src="/js/vue.js"></script>
	<script src="/vuejs/vue-tables-2.min.js"></script>
	<script src="/vuejs/app-repticket.js"></script>
	@endsection
