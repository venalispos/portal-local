<div class="modal fade" id="eliminarDevolucion" tabindex="-1" role="dialog" style="overflow-y: auto;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click='FunLimpiarGasto()'>&times;</button>
					<h4>Eliminar Devolucion</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" v-if='devolucionSelected'>
          ¿Desea realmente eliminar <strong>@{{devolucionSelected.cCorDetComentario}} </strong> de las devoluciones?
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-pull-right" @click ="FunEliminarDevolucion()">Enviar</button>
        </div>
    </div>
  </div>
</div>
