<div class="modal fade" id="modificarDevolucion" tabindex="-1" role="dialog" style="overflow-y: auto;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
					<h4>Modificar Devolucion</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <template v-if='devolucionSelected'>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="">DESCRIPCIÓN</label>
                  <input type="text" class="form-control"  v-model='devolucionSelected.cCorDetComentario' name="" value="">
                </div>
                <div class="form-group">
                  <label for="">CANTIDAD</label>
                  <input type="number" class="form-control"  v-model='devolucionSelected.cCorDetCantidad' name="" value="">
                </div>
              </div>
            </template>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-pull-right" @click ="FunGuardarDevolucionExistente()">Enviar</button>
        </div>
    </div>
  </div>
</div>
