<div class="modal fade" id="finalizarCorte" tabindex="-1" role="dialog" style="overflow-y: auto;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
					<h4>Finalizar corte </h4>
      </div>
      <div class="modal-body" v-if='dataTotalIngresado'>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for=""> Atención</label>
            </div>
            <div class="form-group">
            <!--  <small>Al marcar esta casilla, certifico que he realizado deposito en tómbola de seguridad, correcto y completo, correspondiente al día y la hora del corte de caja de la sucursal, según los protocolos establecidos por grupo beh y el proveedor para la recolección de valores, los números de folios de papeletas, bolsas de traslado y ficha bancaria han sido escaneadas previamente, siguiendo el consecutivo de los mismos para ser enviados de manera confidencial al departamento de finanzas.</small>-->
            <small>¿Estás seguro de finalizar el corte?</small>
            </div>
            <div class="form-group">
            <!--<p><small>@{{ FuncalcularDiferencia(dataTotalFormasPago,dataTotalIngresado,dataTotaDevoluciones[0]['cCorDetCantidad'],dataTotalGasto[0]['cCorDetCantidad'])  }} </small> </p>-->

            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-pull-right" @click ="FunFinalizarCorte()">Enviar</button>
        </div>
    </div>
  </div>
</div>
