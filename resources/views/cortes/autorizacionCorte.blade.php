<div class="modal fade" id="autorizarCorte" tabindex="-1" role="dialog" style="overflow-y: auto;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
					<h4>Guardar corte </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for=""> ¿Estás seguro de guardar el corte?</label>
            </div>
            <div class="form-group">
              <small>Al guardar ya no podrás modificar las cantidades ingresadas.</small>
            </div>
            <div class="form-group">
            <!--<p><small>@{{ FuncalcularDiferencia(dataTotalFormasPago,dataTotalIngresado,dataTotaDevoluciones[0]['cCorDetCantidad'],dataTotalGasto[0]['cCorDetCantidad'])  }} </small> </p>-->

            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-pull-right" @click ="FunGuardarCorte()">Si</button>
        </div>
    </div>
  </div>
</div>
