<div class="modal fade" id="VerCorte" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" @click='dataLimpiar'>&times;</button>
				<h4 class="modal-title" style="font-weight: bold;">Ver corte</h4>
			</div>
      <div class="modal-body">
        <template v-for="tipopago in tipoPago">
          <template v-if="tipopago.cidtipopago==1&&tipopago.cestatus==1">
            <h4>Terminal Banamex</h4>
            <table class="table table-striped">
              <thead>
								<tr>
									<th style="text-align:center">No. de terminal</th>
									<th style="text-align:center">Subtotal</th>
								</tr>
							</thead>
              <tbody>
                <template v-for="cortedetalle in corteDetalle">
                  <tr v-if="cortedetalle.cidtipopago==1">
                    <td align="center">@{{cortedetalle.subtipopago.cnombresubtipopago}}</td>
										<td align="center">$ @{{cortedetalle.ctotal}}</td>
                  </tr>
                </template>
                <tr>
										<td align="right" style="font-weight: bold;">Total</td>
										<td align="center">$ @{{totalBan}}</td>
								</tr>
              </tbody>
            </table>
          </template>
          <template v-if="tipopago.cidtipopago==2&&tipopago.cestatus==1">
            <hr>
            <h4>Terminal Scotiabank</h4>
            <table class="table table-striped">
              <thead>
								<tr>
									<th style="text-align:center">No. de terminal</th>
									<th style="text-align:center">Subtotal</th>
								</tr>
							</thead>
              <tbody>
                <template v-for="cortedetalle in corteDetalle">
                  <tr v-if="cortedetalle.cidtipopago==2">
                    <td align="center">@{{cortedetalle.subtipopago.cnombresubtipopago}}</td>
										<td align="center">$ @{{cortedetalle.ctotal}}</td>
                  </tr>
                </template>
                <tr>
										<td align="right" style="font-weight: bold;">Total</td>
										<td align="center">$ @{{totalSco}}</td>
								</tr>
              </tbody>
            </table>
          </template>
          <template v-if="tipopago.cidtipopago==3&&tipopago.cestatus==1">
            <hr>
            <h4>Moneda Nacional</h4>
            <table class="table table-striped">
              <thead>
								<tr>
                  <th style="text-align:center">Tipo</th>
									<th style="text-align:center">Cantidad</th>
									<th style="text-align:center">Subtotal</th>
								</tr>
							</thead>
              <tbody>
                <template v-for="cortedetalle in corteDetalle">
                  <tr v-if="cortedetalle.cidtipopago==3">
                    <td align="center">$ @{{cortedetalle.cdenominacion}}</td>
										<td align="center">$ @{{cortedetalle.ccantidad}}</td>
                    <td align="center">$ @{{cortedetalle.ctotal}}</td>
                  </tr>
                </template>
                <tr>
									<td colspan="2" align="right" style="font-weight: bold;">Total</td>
									<td align="center">$ @{{totalNac}}</td>
								</tr>
              </tbody>
            </table>
          </template>
          <template v-if="tipopago.cidtipopago==4&&tipopago.cestatus==1">
            <hr>
            <h4>Cheque ó transferencia</h4>
            <table class="table table-striped">
              <thead>
								<tr>
									<th style="text-align:center">No. de referencia</th>
									<th style="text-align:center">Subtotal</th>
								</tr>
							</thead>
              <tbody>
                <template v-for="cortedetalle in corteDetalle">
                  <tr v-if="cortedetalle.cidtipopago==4">
                    <td align="center">@{{cortedetalle.cnoreferencia}}</td>
										<td align="center">$ @{{cortedetalle.ctotal}}</td>
                  </tr>
                </template>
              </tbody>
            </table>
          </template>
          <template v-if="tipopago.cidtipopago==5&&tipopago.cestatus==1">
            <hr>
            <h4>Dólares</h4>
            <table class="table table-striped">
              <thead>
								<tr>
                  <th style="text-align:center">Tipo</th>
									<th style="text-align:center">Cantidad</th>
									<th style="text-align:center">Subtotal</th>
								</tr>
							</thead>
              <tbody>
                <template v-for="cortedetalle in corteDetalle">
                  <tr v-if="cortedetalle.cidtipopago==5">
                    <td align="center">$ @{{cortedetalle.cdenominacion}}</td>
										<td align="center">$ @{{cortedetalle.ccantidad}}</td>
                    <td align="center">$ @{{cortedetalle.ctotal}}</td>
                  </tr>
                </template>
                <tr>
									<td colspan="2" align="right" style="font-weight: bold;">Total Dólares</td>
									<td align="center">$ @{{totalDol}}</td>
								</tr>
                <tr>
									<td colspan="2" align="right" style="font-weight: bold;">Tipo cambio</td>
									<td align="center">$ @{{tipoCambio}}</td>
								</tr>
                <tr>
									<td colspan="2" align="right" style="font-weight: bold;">Total Mxn</td>
									<td align="center">$ @{{dolaresCambio}}</td>
								</tr>
              </tbody>
            </table>
          </template>
          <template v-if="tipopago.cidtipopago==6&&tipopago.cestatus==1">
            <hr>
            <h4>Aplicación móvil</h4>
            <table class="table table-striped">
              <thead>
								<tr>
                  <td align="right" style="font-weight: bold;">Subtotal</td>
                  <template v-for="cortedetalle in corteDetalle">
                    <td v-if="cortedetalle.cidtipopago==6" align="center">$ @{{cortedetalle.ctotal}}</td>
                  </template>
								</tr>
							</thead>
              <tbody>
                <template v-for="cortedetalle in corteDetalle">
                  <tr v-if="cortedetalle.cidtipopago==6">
                    <td align="right" style="font-weight: bold;">Total</td>
									  <td align="center">$ @{{cortedetalle.ctotal}}</td>
                  </tr>
                </template>
              </tbody>
            </table>
          </template>
          <template v-if="tipopago.cidtipopago==7&&tipopago.cestatus==1">
            <hr>
            <h4>UBER</h4>
            <table class="table table-striped">
              <thead>
								<tr>
                  <td align="right" style="font-weight: bold;">Subtotal</td>
                  <template v-for="cortedetalle in corteDetalle">
                    <td v-if="cortedetalle.cidtipopago==7" align="center">$ @{{cortedetalle.ctotal}}</td>
                  </template>
								</tr>
							</thead>
              <tbody>
                <template v-for="cortedetalle in corteDetalle">
                  <tr v-if="cortedetalle.cidtipopago==7">
                    <td align="right" style="font-weight: bold;">Total</td>
									  <td align="center">$ @{{cortedetalle.ctotal}}</td>
                  </tr>
                </template>
              </tbody>
            </table>
          </template>
          <template v-if="tipopago.cidtipopago==8&&tipopago.cestatus==1">
            <h4>Terminal American Express</h4>
            <table class="table table-striped">
              <thead>
								<tr>
									<th style="text-align:center">No. de terminal</th>
									<th style="text-align:center">Subtotal</th>
								</tr>
							</thead>
              <tbody>
                <template v-for="cortedetalle in corteDetalle">
                  <tr v-if="cortedetalle.cidtipopago==8">
                    <td align="center">@{{cortedetalle.subtipopago.cnombresubtipopago}}</td>
										<td align="center">$ @{{cortedetalle.ctotal}}</td>
                  </tr>
                </template>
                <tr>
										<td align="right" style="font-weight: bold;">Total</td>
										<td align="center">$ @{{totalAmex}}</td>
								</tr>
              </tbody>
            </table>
          </template>
        </template>
        <hr>
        <table class="table table-striped">
          <tbody>
            <tr>
							<td align="right" style="font-weight: bold;">Total Corte</td>
							<td align="right">$ @{{total}}</td>
						</tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
