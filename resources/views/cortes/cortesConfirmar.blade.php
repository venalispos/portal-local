<form @submit.prevent="agregarCorte">
  {!! csrf_field() !!}
  <div class="modal fade" id='confirmarCorte' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 style="font-weight: bold;">Confirmación</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label>¿Estás seguro que deseas enviar el corte?</label><br>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
