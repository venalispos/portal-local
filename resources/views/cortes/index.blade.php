@extends('Master')
@section('titulo','Cortes')
@section('content')
<style>
  @font-face {
    font-family: 'lucida-console';
    src: url('/fonts/lucida-console.ttf') format('truetype');
  }

  a {
    text-decoration: none;
  }

  .main-wrap {
    background: #000;
    text-align: center;
  }

  .main-wrap h1 {
    color: #fff;
    margin-top: 50px;
    margin-bottom: 100px;
  }

  .col-md-3 {
    display: block;
    float: left;
    margin: 1% 0 1% 1.6%;
    background-color: white;
    padding: 50px 0;
  }

  .col:first-of-type {
    margin-left: 0;
  }


  /* ALL LOADERS */

  .loader {
    width: 100px;
    height: 100px;
    border-radius: 100%;
    position: relative;
    margin-left: 300%;
  }

  /* LOADER 5 */

  #loader-5 span {
    display: block;
    position: absolute;
    left: calc(50% - 20px);
    top: calc(50% - 20px);
    width: 20px;
    height: 20px;
    background-color: green;
  }

  #loader-5 span:nth-child(2) {
    animation: moveanimation1 1s ease-in-out infinite;
  }

  #loader-5 span:nth-child(3) {
    animation: moveanimation2 1s ease-in-out infinite;
  }

  #loader-5 span:nth-child(4) {
    animation: moveanimation3 1s ease-in-out infinite;
  }

  @keyframes moveanimation1 {

    0%,
    100% {
      -webkit-transform: translateX(0px);
      -ms-transform: translateX(0px);
      -o-transform: translateX(0px);
      transform: translateX(0px);
    }

    75% {
      -webkit-transform: translateX(30px);
      -ms-transform: translateX(30px);
      -o-transform: translateX(30px);
      transform: translateX(30px);
    }
  }

  @keyframes moveanimation2 {

    0%,
    100% {
      -webkit-transform: translateY(0px);
      -ms-transform: translateY(0px);
      -o-transform: translateY(0px);
      transform: translateY(0px);
    }

    75% {
      -webkit-transform: translateY(30px);
      -ms-transform: translateY(30px);
      -o-transform: translateY(30px);
      transform: translateY(30px);
    }
  }

  @keyframes moveanimation3 {

    0%,
    100% {
      -webkit-transform: translate(0px, 0px);
      -ms-transform: translate(0px, 0px);
      -o-transform: translate(0px, 0px);
      transform: translate(0px, 0px);
    }

    75% {
      -webkit-transform: translate(30px, 30px);
      -ms-transform: translate(30px, 30px);
      -o-transform: translate(30px, 30px);
      transform: translate(30px, 30px);
    }
  }

  }
</style>

<div id="cortesPrincipal">
	<div class="row">
      <div class="col-md-2">
        <div class="form-group">
          <label for="">Buscar fecha</label>
          <input type="date" name="" class="form-control" v-model='fecha' value="">
          <button type="button" @click="FunObtenerInformacionCortes()" class="btn btn-primary btn-block" name="button"> <i class=" fa fa-search"></i> Buscar</button>
        </div>
      </div>
    </div>
    <template v-if='dataFechaAceptada == 0 && corteRealizadoConsulta == 0'>
      <div class="col-md-9">
        <p>No puedes realizar el corte en la fecha seleccionada</p>
      </div>
    </div>
  </template>
  <template v-if='dataMesasAbiertas == 0 && corteRealizadoConsulta == 0'>
      <div class="col-md-9">
        <p>Tienes mesas abiertas, no puedes realizar el corte por el momento</p>
      </div>
    </div>
  </template>

  <template v-if="(dataFechaAceptada == 1 && dataMesasAbiertas == 1 ) || (dataFechaAceptada == 1 && dataMesasAbiertas == 0) && corteRealizadoConsulta != 0">
    <div class="row">
      <div class="col-md-2">
        <div class="form-group" width='100px'>
          <div v-if='finalizarCorte == "1"'>
            <a class="btn btn-app btn-block" @click='FunDescargarArqueoVenta()'>
              <i class="fa fa-edit"></i> <small>ARQUEO DE VENTA</small>
            </a>
          </div>
          <div v-if='finalizarCorte == "1"'>
            <a class="btn btn-app  btn-block" @click='FunDescargarCorteCaja()'>
              <i class="fa fa-edit"></i> <small>CORTE DE CAJA DIARIO</small>
            </a>
          </div>
        </div>
        <div class="form-group">
          <div v-if='finalizarCorte == "1"'>
            <a class="btn btn-app  btn-block" @click='FunDescargarDolar()'>
              <i class="fa fa-edit"></i> <small>CORTE DE REPORTE DOLAR</small>
            </a>
          </div>
        </div>
        <template v-if='guardarCorte == 1 && configuracion.cConPorcentajeTiraX == 0 || finalizarCorte == "1" && configuracion.cConPorcentajeTiraX == 0'>
          <div class="form-group">
            <div>
              <a class="btn btn-app  btn-block" @click='FunDescargarbanamex()'>
                <i class="fa fa-edit"></i> <small>FICHA DE DEPÓSITO</small>
              </a>
            </div>
          </div>
        </template>
      </div>
      <template v-if='finalizarCorte == "0" && !inicio'>
        <template v-if='guardarCorte == 0'>
          <div class="col-sm-9 pull-center">
            <div class="form-group col-sm-4">
              <input type="text" name="num_papeleta" class="form-control" v-model='num_papeleta' placeholder="# de papeleta">
            </div>
            <div class="form-group col-sm-4">
              <input type="text" name="num_bolsa" class="form-control" v-model='num_bolsa' placeholder="# de bolsa">
            </div>
            <div class="form-group col-sm-4">
              <div class="form-group">
                <div>
                  <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Al marcar esta casilla, certifico que he realizado deposito en tómbola de seguridad, correcto y completo, correspondiente al día y la hora del corte de caja de la sucursal, según los protocolos establecidos por grupo beh y el proveedor para la recolección de valores, los números de folios de papeletas, bolsas de traslado y ficha bancaria han sido escaneadas previamente, siguiendo el consecutivo de los mismos para ser enviados de manera confidencial al departamento de finanzas."  @click='FunModalFinalizarCorte()'>
                    <i class="fa fa-check"></i> <small>FINALIZAR CORTE</small>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </template>
      </template>
      <div class="col-md-9">
        <template v-if='dataInformacionCortes'>
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <th>Forma de pago</th>
                <template v-if='configuracion.cConForzarCierre == 0 || finalizarCorte == "1"'>
                  <th style="text-align:right">POS</th>
                </template>
                <th style="text-align:right">Físico</th>
                <template v-if='configuracion.cConForzarCierre == 0 || finalizarCorte == "1"'>
                  <th style="text-align:right">Diferencia</th>
                </template>
                <th v-if='finalizarCorte == "0" && guardarCorte == 0'>Acciones</th>
              </thead>
              <tbody>
                <template v-for='data in dataInformacionCortes'>
                  <tr>
                    <td>@{{data.cHisForDes}}</td>
                    <template v-if='configuracion.cConForzarCierre == 0 || finalizarCorte == "1"'>
                      <th v-if='finalizarCorte == "0"' style="text-align:right;"> <label for="">@{{FunFormatearMoneda(data.cHisForCant)}} </label> </th>
                      <th v-else style="text-align:right;"> <label for="">@{{FunFormatearMoneda(data.cHisForCant)}} </label></th>
                    </template>
                    <th align='right' style="text-align:right;"> @{{FunFormatearMoneda(data.cHisCorteIngresado)}} </th>
                    <template v-if='configuracion.cConForzarCierre == 0 || finalizarCorte == "1"'>
                    <th align='right' style="text-align:right" v-if='(data.cHisCorteIngresado -data.cHisForCant ) >= 0'>@{{FunFormatearMoneda(data.cHisCorteIngresado -data.cHisForCant)}}</th>

                    <th align='right' style="text-align:right;color:red;" v-if='(data.cHisCorteIngresado -data.cHisForCant ) < 0'>@{{FunFormatearMoneda(data.cHisCorteIngresado -data.cHisForCant ) }}</th>
                    </template>
                    <th align='right' style="text-align:right" v-if=' dataCorteFinalizado &&dataCorteFinalizado[0]["cCorDetFinalizado"] == "0" && guardarCorte == 0'>
                      <button type="button" class="btn btn-primary" @click='FunObtenerItemsAbrirModal(data)' name="button"> <i class="fa fa-edit"></i> </button>
                    </th>

                  </tr>
                </template>
              </tbody>
              <tr>
                <th align='right' style="text-align:right" colspan="1"><strong>TOTAL</strong> </th>
                <template v-if='configuracion.cConForzarCierre == 0 || finalizarCorte == "1"'>
                <th align='right' style="text-align:right" v-if='dataTotalFormasPago'>@{{FunFormatearMoneda(dataTotalFormasPago)}} </th>
                <th align='right' style="text-align:right" v-else>$0.00 </th>
                </template>
                <th align='right' style="text-align:right" v-if='dataTotalIngresado'>@{{FunFormatearMoneda(dataTotalIngresado)}} </th>
                <th align='right' style="text-align:right" v-else>$0.00 </th>
                <template v-if='configuracion.cConForzarCierre == 0 || finalizarCorte == "1"'>
                <th align='right' style="text-align:right" v-if='dataTotalIngresado'>@{{FunFormatearMoneda(dataTotalIngresado -dataTotalFormasPago)}} </th>
                <th align='right' style="text-align:right" v-else>@{{FunFormatearMoneda(dataTotalFormasPago)}} </th>
                </template>
              </tr>
            </table>
          </div>
        </template>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <button v-if=' finalizarCorte == "0" && !inicio' type="button" class="btn btn-primary" @click='FunAgregarGasto()' name="button"> <i class="fa fa-add-o"></i> Agregar gastos</button>
        </div>
        <div class="table-responsive" v-if='dataInformacionCortes '>
          <table class="table table-bordered">
            <thead>
              <th>Gasto</th>
              <th>Importe</th>
              <th v-if='finalizarCorte == "0"'>Acciones</th>
            </thead>
            <tbody>
              <template v-if='dataGastos' v-for='gastos in dataGastos'>
                <tr>
                  <td>@{{gastos.cCorDetComentario}}</td>
                  <td>@{{FunFormatearMoneda(gastos.cCorDetCantidad)}}</td>
                  <td v-if='finalizarCorte == "0"'>
                    <button type="button" class="btn btn-primary" @click='FunMostrarModalModificar(gastos)' name="button"> <i class="fa fa-edit"></i> </button>
                    <button type="button" class="btn btn-primary" @click='FunEliminarGastoModal(gastos)' name="button"> <i class="fa fa-trash"></i> </button>
                  </td>

                </tr>
              </template>
              <tr>
                <th>TOTALES</th>
                <template v-if="dataTotalGasto">
                  <th>@{{FunFormatearMoneda(dataTotalGasto[0]['cCorDetCantidad'])}}</th>
                </template>
                <template v-else>
                  <th>$0.00</th>
                </template>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </template>



  @include('cortes.modificarCortes')
  @include('cortes.modificarGasto')
  @include('cortes.agregarGasto')
  @include('cortes.eliminarFormaPago')
  @include('cortes.autorizacionCorte')
  @include('cortes.finalizarCorte')
  @include('cortes.agregarDevolucion')
  @include('cortes.modificarDevolucion')
  @include('cortes.eliminarDevolucion')
</div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/cortes/cortes.js"></script>
@endsection
