<!DOCTYPE html>

<?php $total=0;
$totalBilletes=0;
 ?>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body style="font-family:Courier;">
    <div class="">
      <table width="100%">
      @foreach($dataGPDF['corte']['categoriasagrupadores'] as $categoriasAgrupadores)
    <?php   $total=0; ?>
      <tr>
        <th colspan="3">
          {{$categoriasAgrupadores["cCatAgForDescripcion"]}}
        </th>
      </tr>
      <tr>

        @if($categoriasAgrupadores["cCatAgForParidad"] ==1)
          <td >DESCRIPCION</td>
        <td align='right' style="text-align:right;">TIPO DE CAMBIO</td>
        @else
        <td colspan="2" style="text-align:left;">DESCRIPCION</td>
        @endif
        <td align='right' style="text-align:right;">SUBTOTAL</td>
      </tr>
          @foreach($dataGPDF['corte']['formaspago'] as $formaspago)
              @if($formaspago["cHisFolioAgrupador"] == $categoriasAgrupadores["cCatAgForId"])
              <tr>

                @if($categoriasAgrupadores["cCatAgForParidad"] ==1)
                <td> {{$formaspago["cHisForDes"]}}</td>
                  @if($formaspago["cHisParidad"] >1)
                  <td align='right' style="text-align:right;"><?php echo number_format(floatval($formaspago["cHisCorteIngresado"])/floatval($formaspago["cHisParidad"]),2)."  X  "; ?> {{ number_format($formaspago["cHisParidad"],2)}}</td>
                  @else
                    <td align='right' style="text-align:right;">--</td>
                  @endif
                @else
                <td  colspan="2" style="text-align:left;">
                {{$formaspago["cHisForDes"]}}
              </td>
                @endif
                <th  align='right' style="text-align:right;">

                  <?php   $total +=$formaspago["cHisCorteIngresado"]; echo number_format($formaspago["cHisCorteIngresado"],2);  ?>
                </th>
              </tr>
              @endif

          @endforeach
          <tr>
            <td colspan="3"><hr> </td>
          </tr>
          <tr>
            <th style="text-align:left;" colspan="2">
              TOTAL
            </th>
            <th align='right' style="text-align:right;"><?php echo number_format($total,2);  ?></th>
          </tr>
          <tr>
            <th colspan="3"><hr> </th>
          </tr>
          <?php $total=0; ?>
      @endforeach
      <tr>
        <th colspan="3">
          GASTOS
        </th>
      </tr>
      <tr>
        <td>DESCRIPCION</td>
        <td colspan="2" align='right' style="text-align:right;">SUBTOTAL</td>
      </tr>
      <?php  $total=0; ?>
      @foreach($dataGPDF["corte"]["gastos"] as $gasto)
      <tr>
        <td>{{$gasto["cCorDetComentario"]}}</td>
        <?php  $total += $gasto["cCorDetCantidad"]; ?>
        <th colspan="2" align='right' style="text-align:right;"><?php  echo number_format($gasto["cCorDetCantidad"],2) ?></th>
      </tr>
      @endforeach
      <tr>
        <th style="text-align:left;" colspan="2">TOTAL</th>
        <th align='right' style="text-align:right;"><?php  echo  number_format($total,2);; ?></th>
      </tr>
      <tr>
        <th colspan="3"><hr> </th>
      </tr>

      <tr>
        <th colspan="3"><hr> </th>
      </tr>
      </table>
    </div>
  </body>

  <div align='right' style="text-align:right;">
    <table  align='right' style="text-align:right;" width='100%'>
        <tr>
          <th width='60%'>TOTAL </th>
          <td  width='40%'> <?php echo  number_format(round($dataGPDF['corte']['totalIngresado'],2),2); ?> </td>
        </tr>

        <tr>
          <th width='60%'> VENTA</th>
          <td  width='40%'> <?php echo  number_format(round($dataGPDF['corte']['totalVenta'],2),2); ?></td>
        </tr>
        <tr>
          <?php
          $totalTotal= (floatval($dataGPDF['corte']['totalIngresado'])+floatval($dataGPDF['corte']['totalGasto'][0]['cCorDetCantidad'])+floatval($dataGPDF['corte']['totalDevoluciones'][0]['cCorDetCantidad']))-(floatval($dataGPDF['corte']['totalVenta']));
           ?>
          <th width='60%'>SOBRA/FALTA</th>
          <td  width='40%'> <strong> <?php echo number_format($totalTotal,2); ?></strong> </td>
        </tr>
    </table>
    <br><br><br> <br><br><br><br>
    <br><br><br> <br><br><br><br>
    <div align='center' style="text-align:center;">
      <p>{{Auth()->user()->cUsuNombre }} {{Auth()->user()->cUsuApellido}} </p>
      <br><p>_______________________________</p>
    </div>
  </div>
</html>
