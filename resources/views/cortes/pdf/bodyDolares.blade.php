<?php
 $totalBilletes=0;
  $totalBilletesDolares=0;
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body style="font-family:Courier">
    <div class="">
      @foreach($dataGPDF['corte']['formaspago'] as $formaspago)
      <?php $totalBilletes=0; ?>
      @if($formaspago['cHisForId'] == 5)
      <h4><small>TIPO DE CORTE</small> {{$formaspago['cHisForDes']}}</h4>
      <table width='60%'>
        <tr>
          <th width='20%' align='left' style="text-align:left;" > CANTIDAD </th>
          <th width='20%' align='center' style="text-align:center;" >DESCRIPCIÓN</th>
          <th width='40%' align='right' style="text-align:right;" >BILLETES </th>
          <th width='40%' align='center' style="text-align:center;" > TIPO DE CAMBIO </th>
          <th width='40%' align='right' style="text-align:right;" >TOTAL</th>
        </tr>
        @foreach($dataGPDF['item'] as $items)
          @if($items['cFormaPagoId'] == $formaspago['cHisForId'])
          <tr>
            <td align='left' style="text-align:left;" ><?php echo $items['cCorForValor'] ?>  </td>

            <td  align='center' style="text-align:center;" >{{$items['cCorForDescripcion']}}</td>
            <td  width='40%' align='right' style="text-align:right;"><?php echo number_format(floatval($items['cCorForValorItem'])*floatval($items['cCorForValor']),2) ?><strong> </strong> </td>
            <td  align='center' style="text-align:center;" ><?php echo number_format(floatval($items['cCorDetParidad']),2) ?><strong> </strong> </td>

            <td  align='right' style="text-align:right;" ><?php echo number_format(floatval($items['cCorDetParidad']*$items['cCorForValor'])*floatval($items['cCorForValorItem']),2) ?> </td>
          </tr>
          <?php $totalBilletes += $items['cCorForValor']; ?>
          <?php $totalBilletesDolares += floatval($items['cCorForValorItem'])*floatval($items['cCorForValor']) ?>
          @endif
          @endforeach
          <tr>
            <td colspan="7"><hr>  </td>
          </tr>
          <tr>
              <th align='left' style="text-align:left;"></th>
              <th></th>
                <th width='40%' align='right' style="text-align:right;"> <?php echo  number_format(round(floatval($totalBilletesDolares),2),2); ?></th>
              <th></th>
              <th  width='40%' align='right' style="text-align:right;" ><?php echo  number_format(round(floatval($formaspago['cHisCorteIngresado']),2),2); ?> </th>
      </table>
      <br>
      @endif
      @endforeach
    </div>
    <br><br><br> <br><br><br><br>
    <br><br><br> <br><br><br><br>
    <div align='center' style="text-align:center;">
      <p>{{Auth()->user()->cUsuNombre }} {{Auth()->user()->cUsuApellido}} </p>
      <br><p>_______________________________</p>
    </div>
  </body>
</html>
