<?php $totalBilletes=0; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body style="font-family:Courier">
    <div class="">
      @foreach($dataGPDF['corte']['formaspago'] as $formaspago)
      <?php $totalBilletes=0;  ?>
      <h4><small>TIPO DE CORTE</small> {{$formaspago['cHisForDes']}}</h4>
      <table width='60%'>
        <tr>
          @if($formaspago['cHisForId'] == 3 || $formaspago['cHisForId'] == 5)
            <th width='10%' align='left' style="text-align:left;" > CANTIDAD</th>
          @endif

          <th width='40%' align='center' style="text-align:center;" >DESCRIPCIÓN</th>
          @if($formaspago['cHisForId'] == 5)
          <th width='40%' align='center' style="text-align:center;" >TIPO DE CAMBIO</th>
          @endif
          <th width='40%' align='right' style="text-align:right;" >TOTAL</th>
        </tr>
        @foreach($dataGPDF['item'] as $items)
          @if($items['cFormaPagoId'] == $formaspago['cHisForId'])
          <tr>
            @if($formaspago['cHisForId'] == 3 || $formaspago['cHisForId'] == 5)
              <td  align='left' style="text-align:left;" ><?php echo number_format(floatval($items['cCorForValor']),0) ?> </td>
            @endif
            <td  align='center' style="text-align:center;" >{{$items['cCorForDescripcion']}}</td>
            @if($items['cFormaPagoId'] == 5)
            <td align='center' style="text-align:center;" ><?php echo number_format(floatval($items['cCorDetParidad']),2) ?> </td>
            @endif
            <td width='60%' align='right' style="text-align:right;" ><?php echo number_format(floatval($items['cCorForValor'])*floatval($items['cCorForValorItem'])*floatval($items['cCorDetParidad']),2) ?> </td>
          </tr>
          <?php $totalBilletes += $items['cCorForValor']; ?>
          @endif
          @endforeach
          <tr>
            @if($formaspago['cHisForId'] == 3 || $formaspago['cHisForId'] == 5)
            <th align='left' colspan="1" style="text-align:left;"></th>
            @endif
              @if( $formaspago['cHisForId'] == 5)
              <td></td>
            @endif
  <td></td>
              <th   align='right' style="text-align:right;" ><?php
               $resultado = floor(($formaspago['cHisCorteIngresado']*1000))/1000;
               echo number_format($resultado,2);
               ?> </th>
          </tr>
      </table>
      <br>
      @endforeach
    </div>
    <br><br>
    <div align='right' style="text-align:right;">
      <table  align='right' style="text-align:right;" width='100%'>

          <tr>
            <th width='60%'>TOTAL</th>
            <td  width='40%'> <?php echo  number_format(round($dataGPDF['corte']['totalIngresado'],2),2); ?> </td>
          </tr>

          <tr>
            <th width='60%'>VENTA</th>
            <td  width='40%'> <?php echo  number_format(round($dataGPDF['corte']['totalVenta'],2),2); ?></td>
          </tr>
          <tr>
            <?php
            $totalTotal= (floatval($dataGPDF['corte']['totalIngresado'])+floatval($dataGPDF['corte']['totalGasto'][0]['cCorDetCantidad']))-round($dataGPDF['corte']['totalVenta'],2);
             ?>
            <th width='60%'>SOBRA/FALTA</th>
            <td  width='40%'> <strong> <?php echo number_format( round($totalTotal,2),2); ?></strong> </td>
          </tr>
      </table>
      <br><br><br> <br><br><br><br>
      <br><br><br> <br><br><br><br>
      <div align='center' style="text-align:center;">
        <p>{{Auth()->user()->cUsuNombre }} {{Auth()->user()->cUsuApellido}} </p>
        <br><p>_______________________________</p>
      </div>
    </div>
  </body>
</html>
