
<!DOCTYPE html>
<html lang="en">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<head>
		<style>
			#cabecera{
				background-color:#969696;
				height:80px;
			}
      #contenido{
        background-color:#969696;
				height:400px;
      }
			#imagen{
				height:50px;
				width: 140px;
			}
			#celda{
				margin-left:18px;
        margin-top: 12px;
  			width:18%;
        float: left;
			}
      #documentos{
        float:left;
        margin-left: 30px;
        margin-top: 18px;

      }
      #casilla{
        background-color:#ffffff;
        float:left;
        width:4%;
        height:36%;
        margin-top: 24px;
        margin-left: 10px;
      }
      #deposito{
        float:left;
        margin-top: 8px;
        margin-left: 100px;
      }
      #mx{
        float:left;
        margin-left: 100px;
        margin-top: 18px;
      }
      #dlls{
        float:left;
        margin-left: 10px;
        margin-top: 18px;
      }
      #contenedor{
        background-color: #ffffff;
        height: 65px;
        width: 848px;
        margin-left: 15px;

      }
      #nombre{
        margin: 20px;
        padding: 20px;
        width: 25%;
      }
      #empresa{
        border: 2px solid #969696;
        width: 75%;
      }
      #sucursal{
        border: 2px solid #969696;
					right: 26px;
      }
      #blanco{
        width: 1%;
					right: 26px;
      }
      #referencia{
        border: 2px solid #969696;
        width: 3%;
        height: 5%;
      }
      #numeros{
        background-color: #ffffff;
        height: 35px;
        width: 848px;
        margin-left: 15px;
					right: 26px;
      }
      #titulos{
        background-color: #ffffff;
        height: 25px;
				position: absolute;
			 	right: 26px;
				left: : 26px;
        width: 848px;
        margin-left: 15px;
      }
      #importe{
        background-color: white;
        height: 270px;
        width: 400px;
        margin-left: 15px;
        float: left;
      }
      #alfa{

        float: right;
        background-color: #ffffff;
					position: absolute;
        height: 35px;
				top:223px ;
					right: 26px;
        width: 474px;
        margin-left: 0px;

      }
      #ref_alfa{
        float: right;
				position: absolute;
				right: 26px;
			top:255px ;
        background-color: #ffffff;
        height: 45px;
        width: 474px;

        margin-left: 1px;
      }
      #caja_dep{

        float: right;
        background-color: #ffffff;
				position: absolute;
		right: 26px;
			top:285px ;
        height: 23px;
        width: 474px;
        margin-left: 1px;
      }
      #caja_formas{
        float: right;
        background-color: #ffffff;
				position: absolute;
				right: 26px;
			top:308px ;
        height: 70px;
        width: 474px;
        margin-left: 1px;
      }
      #txt_dep{
        float: right;
        background-color: #ffffff;
				position: absolute;
				right: 26px;
			top:375px ;
        height: 95px;
        width: 474px;
        margin-left: 1px;
      }
      #txt_importe{
				position: absolute;
				right: 26px;
				top:412px ;
        float: right;
        background-color: #ffffff;
        height: 20px;
        width: 474px;
        margin-left: 1px;
      }
      <?php

      $count_total=3;
      $total  =3;
       ?>
		</style>
	</head>
	<body>
		<br>
		<div id="cabecera">
      <div id="celda">
        <img src="https://admin.grupobeh.com/img/logo_banamex.png" alt="banamex" id="imagen">
      </div>
      <div id="documentos">
        <p style="font-family:Courier; font-size:13px">No. DOCUMENTOS</p>
      </div>
      <div id="casilla">
      </div>
      <div id="casilla" style="margin-left:5px;">
      </div>
      <div id="deposito">
        <p style="font-family:Courier; font-size:20px"><strong>DÉPOSITO</strong></p>
      </div>
      <div id="mx">
        <p style="font-family:Courier; font-size:13px">M.X</p>
      </div>
      <div id="casilla">
        <p style="font-family:Courier; font-size:20px; margin:6px;"><strong>X</strong></p>
      </div>
      <div id="dlls">
        <p style="font-family:Courier; font-size:13px">DLLS</p>
      </div>
      <div id="casilla">
      </div>
		</div>
    <div id="contenido">
      <div id="contenedor">
        <table width="100%">
          <tr>
            <td style="font-family:Courier; font-size:12px" id="nombre">NOMBRE DEL CLIENTE</td>
            <td style="font-family:Courier; font-size:16px" id="empresa" align="center"><strong>OPERADORA POCKETS METRO, SA DE CV</strong></td>
          </tr>
        </table>
      </div>
      <div id="numeros">
        <table width="100%">
          <tr>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>9</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>2</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>9</strong></td>
            <td style="font-family:Courier; font-size:16px" align="center" id="blanco">  </td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>3</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>6</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>7</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>6</strong></td>
            <td style="font-family:Courier; font-size:16px" id="sucursal" align="center"><strong>0</strong></td>
            <td style="font-family:Courier; font-size:16px" align="center" id="blanco">  </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
          </tr>
        </table>
      </div>
      <div id="titulos">
        <table width="80%">
          <tr>
            <td style="font-family:Courier; font-size:12px;">No. DE SUCURSAL</td>
            <td style="font-family:Courier; font-size:12px;" align="center">No. DE CUENTA DE CHEQUES</td>
            <td align='right' style="font-family:Courier; font-size:12px;" align="right">No. DE REFERENCIA NUMERICA</td>
          </tr>
        </table>
      </div>
      <div id="importe">
        <table width="100%">
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
            <td style="font-family:Courier; font-size:16px;" align="center" id="blanco"><strong>DOCUMENTO</strong></td>
            <td style="font-family:Courier; font-size:16px;" align="center" id="blanco"><strong>IMPORTE</strong></td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
          </tr>
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
          </tr>
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
          </tr>
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
          </tr>
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
          </tr>
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
          </tr>
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
          </tr>
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
          </tr>
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia">000000000000</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco">0</td>
          </tr>
        </table>
				<br><br> <br><br> <br><br> <br><br> <br><br> <br><br>
      </div>
      <div id="alfa">
        <table width="100%">
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
          </tr>
        </table>
      </div>
      <div id="ref_alfa">
        <table width="100%">
          <tr>
            <td style="font-family:Courier; font-size:12px;">REFERENCIA ALFANUMERICA</td>
          </tr>
        </table>
      </div>
      <div id="caja_dep">
        <p style="font-family:Courier; font-size:16px; margin-top:-1%;"><strong>FORMA DE DEPOSITO </strong><span style="font-family:Courier; font-size:12px;">(REGISTRE UNA FORMA POR CADA OPCION)</span></p>
      </div>
      <div id="caja_formas">
        <table width="100%">
          <tr>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:12px;" align="center" id="blanco"> CHEQUES BANAMEX </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:12px;" align="center" id="blanco"> CHEQUES OTROS BANCOS </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco"> 0 </td>
            <td style="font-family:Courier; font-size:16px;" align="center" id="referencia"><strong>X</strong></td>
            <td style="font-family:Courier; font-size:12px;" align="center" id="blanco">EFECTIVO</td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="blanco"> 0 </td>
            <td style="font-family:Courier; font-size:16px; color:#FFFFFF" align="center" id="referencia"> 0 </td>
            <td style="font-family:Courier; font-size:12px;" align="center" id="blanco"> OTRO </td>
            <td style="font-family:Courier; font-size:16px;" align="center" id="blanco"> ____________ </td>
          </tr>
        </table>
      </div>
      <div id="txt_dep">
        <p style="font-family:Courier; font-size:16px; margin-top:-1%; margin-left:20%;"><strong>IMPORTE DEL DEPOSITO:</strong></p>
      </div>
      <div id="txt_importe">
        <table width="100%">
					<?php
						$arr1	=null;
						$counted= 0;

					  ?>
						@foreach($dataGPDF['corte']['formaspago'] as $formapago)
						@if($formapago['cHisForId']  == 3)
							<?php
							$formapago['cHisCorteIngresado']=number_format(round($formapago['cHisCorteIngresado'],2),2);
							$formapago['cHisCorteIngresado'] =str_replace(".","",$formapago['cHisCorteIngresado']);
							$formapago['cHisCorteIngresado'] =str_replace(",","",$formapago['cHisCorteIngresado']);
							 $counted= strlen($formapago['cHisCorteIngresado']);
							 $discounted = 11;
							 $limiteText = 11;
							 $lenghtText = 0;
							$arr1 = str_split($formapago['cHisCorteIngresado']);
							 ?>
						@endif
						@endforeach
            <tr>
						 <td style="font-family:Courier; font-size:40px;" align="center" id="blanco"><strong>$</strong></td>
							<?php
					for ($i=0; $i < $limiteText; $i++) {
						if ($discounted > $counted) {
							echo '<td style="font-family:Courier; font-size:20px; color:black" align="center" id="referencia"><strong> 0</strong> </td>';
						}else{
							if ($discounted < 3) {
								echo '<td style="font-family:Courier; font-size:20px; background-color:#969696;" align="center" id="referencia"><strong>'.$arr1[$lenghtText].'</strong></td>';
							}else{
							echo '<td style="font-family:Courier; font-size:20px; color:black" align="center" id="referencia"><strong> '.$arr1[$lenghtText].'</strong> </td>';
							}
							$lenghtText ++;
						}
						$discounted=intval($discounted)-1;
					}
							 ?>
            </tr>

        </table>
      </div>
    </div>
	</body>
</html>
