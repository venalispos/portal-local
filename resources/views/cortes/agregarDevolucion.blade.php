<div class="modal fade" id="agregarDevolucion" tabindex="-1" role="dialog" style="overflow-y: auto;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click='FunLimpiarGasto()'>&times;</button>
					<h4>Agregar Devolucion</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="">Ingresa una descripción y/o número de ticket de venta cancelada</label>
              <input type="text" v-model='devolucionAgregar.descripcion' class="form-control" name="" value="">
            </div>
            <div class="form-group">
              <label for="">CANTIDAD</label>
              <input type="number" class="form-control" v-model='devolucionAgregar.gasto' step="0-9" name="" value="">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-pull-right" @click ="FunGuardarDevolucion()">Enviar</button>
        </div>
    </div>
  </div>
</div>
