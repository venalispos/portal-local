<div class="modal fade" id="agregarGasto" tabindex="-1" role="dialog" style="overflow-y: auto;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click='FunLimpiarGasto()'>&times;</button>
					<h4>Agregar Gasto</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="">DESCRIPCION</label>
              <input type="text" v-model='gastoAgregar.descripcion' class="form-control" name="" value="">
            </div>
            <div class="form-group">
              <label for="">CANTIDAD</label>
              <input type="number" class="form-control" v-model='gastoAgregar.gasto' step="0-9" name="" value="">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-pull-right" @click ="FunGuardarGasto()">Enviar</button>
        </div>
    </div>
  </div>
</div>
