<div class="modal fade" id="idModificarCorte" tabindex="-1" role="dialog" style="overflow-y: auto;" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
					<h4>Modificar corte </h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <template v-if='formaPagoSelected'>
                <template v-for='data in dataInformacionItem'>
                <template v-if='data.cFormaPagoId == formaPagoSelected.cHisForId'>
                  <div class="col-md-6">
                      <div class="form-group" align='right' style="text-align:right!IMPORTANT;">
                        <label  >@{{data.cCorForDescripcion}}</label>
                      </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <input type="Numeric" class="form-control" v-model='data.cCorForValor' name="" value="">
                    </div>
                  </div>
                </template>
                </template>
            </template>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-pull-right" @click ="FunGuardarCantidadItemCerrarModal()">Enviar</button>
        </div>
    </div>
  </div>
</div>
