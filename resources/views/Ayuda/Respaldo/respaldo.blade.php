@extends('Master')
@section('titulo','Resumen')
@section('content')
<div id="respaldosBasededatos">
	<div  align="center">
		<div class="form-group">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>RESPALDO</h3>


					</div>
					<div class="icon">
						<i class="fa fa-save"></i>
					</div>
					<a @click='generarRespaldo' class="small-box-footer">Presione Aqui <i class="fa fa-download"></i></a> 
					<template v-if="loading">
								<p class="text-center"> 
									<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><br>
									<span>Cargando...</span>
								</p>
							</template>
				</div>
			</div>	
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-respaldo.js"></script>
@endsection
