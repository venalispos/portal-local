<div class="modal fade" id="AgregarCorte" tabindex="-1" role="dialog" style="overflow-y: auto;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click='dataLimpiar'>&times;</button>
					<h4>Ingresar pre cierre</h4>
      </div>
      <div class="modal-body">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <template v-for="tipopago in tipoPago">
                  <li class="active" v-if="tipopago.cidtipopago==1&&tipopago.cestatus==1"><a href="#banamex" data-toggle="tab">@{{tipopago.cnombretipopago}}</a></li>
                  <li v-if="tipopago.cidtipopago==2&&tipopago.cestatus==1"><a href="#scotiabank" data-toggle="tab">@{{tipopago.cnombretipopago}}</a></li>
                  <li v-if="tipopago.cidtipopago==3&&tipopago.cestatus==1"><a href="#nacional" data-toggle="tab">@{{tipopago.cnombretipopago}}</a></li>
                  <li v-if="tipopago.cidtipopago==4&&tipopago.cestatus==1"><a href="#cheque" data-toggle="tab">@{{tipopago.cnombretipopago}}</a></li>
                  <li v-if="tipopago.cidtipopago==5&&tipopago.cestatus==1"><a href="#dolares" data-toggle="tab">@{{tipopago.cnombretipopago}}</a></li>
                  <li v-if="tipopago.cidtipopago==6&&tipopago.cestatus==1"><a href="#aplicacion" data-toggle="tab">@{{tipopago.cnombretipopago}}</a></li>
                  <li v-if="tipopago.cidtipopago==7&&tipopago.cestatus==1"><a href="#uber" data-toggle="tab">@{{tipopago.cnombretipopago}}</a></li>
                  <li v-if="tipopago.cidtipopago==8&&tipopago.cestatus==1"><a href="#amex" data-toggle="tab">@{{tipopago.cnombretipopago}}</a></li>
              </template>
              <li><a href="#gasto" data-toggle="tab">Gasto</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="banamex">
              <div class="row">
                  <div class="col-md-12">
										<div class="form-group">
											<br><label style="font-size: 16px;">Terminal Banamex</label>
										</div>
									</div>
                  <div class="col-md-6">
										<div class="form-group">
											<label style="font-weight: normal;">Terminal:</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label style="font-weight: normal;">Total:</label>
										</div>
									</div>
                  <template v-for="subtipopago in subtipoPago">
                    <template v-if="subtipopago.cidsubtipopago==1&&subtipopago.cestatus==1&&subtipopago.cidtipopago==1">
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" v-model="subtipopago.cnombresubtipopago" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" class="form-control" v-model="bterminal1.ctotal" @keyup='totalBanamex'>
                        </div>
                      </div>
                    </template>
                    <template v-if="subtipopago.cidsubtipopago==2&&subtipopago.cestatus==1&&subtipopago.cidtipopago==1">
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" v-model="subtipopago.cnombresubtipopago" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" class="form-control" v-model="bterminal2.ctotal" @keyup='totalBanamex'>
                        </div>
                      </div>
                    </template>
                  </template>
                  <div class="col-md-6">
                    <div class="form-group" align="right">
                      <label>Total:</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                             <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="banamexTotal" readonly>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div id="scotiabank" class="tab-pane">
              <div class="row">
                  <div class="col-md-12">
										<div class="form-group">
											<br><label style="font-size: 16px;">Terminal Scotiabank</label>
										</div>
									</div>
                  <div class="col-md-6">
										<div class="form-group">
											<label style="font-weight: normal;">Terminal:</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label style="font-weight: normal;">Total:</label>
										</div>
									</div>
                  <template v-for="subtipopago in subtipoPago">
                    <template v-if="subtipopago.cidsubtipopago==3&&subtipopago.cestatus==1&&subtipopago.cidtipopago==2">
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" v-model="subtipopago.cnombresubtipopago" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="number" class="form-control" v-model="sterminal1.ctotal" @keyup='totalScotiabank'>
                        </div>
                      </div>
                    </template>
                    <template v-if="subtipopago.cidsubtipopago==4&&subtipopago.cestatus==1&&subtipopago.cidtipopago==2">
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" v-model="subtipopago.cnombresubtipopago" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="number" class="form-control" v-model="sterminal2.ctotal" @keyup='totalScotiabank'>
                        </div>
                      </div>
                    </template>
                  </template>
                  <div class="col-md-6">
                    <div class="form-group" align="right">
                      <label>Total:</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                             <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="scotiabankTotal" readonly>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div id="nacional" class="tab-pane">
              <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <br><label style="font-size: 16px;">Moneda Nacional</label>
                    </div>
                  </div>
                  <div class="col-md-4">
										<div class="form-group">
											<label style="font-weight: normal;">Tipo de billete/moneda:</label>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label style="font-weight: normal;">Cantidad:</label>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label style="font-weight: normal;">Total:</label>
										</div>
									</div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="billetes1.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valorb1" class="form-control" v-model="billetes2.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valorb1" class="form-control" v-model="billetes3.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valorb1" class="form-control" v-model="billetes4.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valorb1" class="form-control" v-model="billetes5.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valorb1" class="form-control" v-model="billetes6.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas1.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas2.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas3.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas4.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas5.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas6.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas7.cdenominacion" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="billetes1.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="billetes2.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="billetes3.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="billetes4.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="billetes5.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="billetes6.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="monedas1.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="monedas2.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="monedas3.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="monedas4.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="monedas5.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="monedas6.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="monedas7.ccantidad" class="form-control" @keyup='totalNacional'>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="billetes1.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="billetes2.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="billetes3.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="billetes4.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="billetes5.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="billetes6.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas1.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas2.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas3.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas4.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas5.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas6.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="monedas7.ctotal" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="form-group" align="right">
                      <label>Total:</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                             <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="nacionalTotal" readonly>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div id="cheque" class="tab-pane">
              <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
											<label style="font-size: 16px;">Cheque o Transferencia</label>
										</div>
                  </div>
                  <div class="col-md-6">
										<div class="form-group">
											<label style="font-weight: normal;">No. de referencia:</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label style="font-weight: normal;">Total:</label>
										</div>
									</div>
                  <div class="col-md-6">
										<div class="form-group">
											<input type="text" v-model="cheque.cnoreferencia" class="form-control">
										</div>
									</div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="totalnum" class="form-control" v-model="cheque.ctotal">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group" align="right">
                      <label>Total:</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="cheque.ctotal" readonly>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div id="dolares" class="tab-pane">
              <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
											<label style="font-size: 16px;">Dolares</label>
										</div>
                  </div>
                  <div class="col-md-4">
										<div class="form-group">
											<label style="font-weight: normal;">Tipo de dolar:</label>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label style="font-weight: normal;">Cantidad:</label>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label style="font-weight: normal;">Total:</label>
										</div>
									</div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares1.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares2.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares3.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares4.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares5.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares6.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares7.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares8.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares9.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares10.cdenominacion" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="valord1" class="form-control" v-model="dolares11.cdenominacion" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares1.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares2.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares3.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares4.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares5.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares6.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares7.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares8.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares9.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares10.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="text" v-model="dolares11.ccantidad" class="form-control" @keyup='totalDolares'>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares1.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares2.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares3.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares4.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares5.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares6.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares7.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares8.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares9.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares10.ctotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolares11.ctotal" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="form-group" align="right">
                      <label>Total DLLS:</label>
                    </div>
                    <div class="form-group" align="right">
                      <label>Tipo Cambio:</label>
                    </div>
                    <div class="form-group" align="right">
                      <label>Total MXN:</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                             <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolaresTotal" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                             <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="tipoCambio" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                             <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="dolaresCambio" readonly>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div id="aplicacion" class="tab-pane">
              <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <br><label style="font-size: 16px;">Aplicación Móvil</label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label style="font-weight: normal;">Total:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="aplicacion.ctotal">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group" align="right">
                      <label>Total:</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="aplicacion.ctotal" readonly>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div id="uber" class="tab-pane">
              <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <br><label style="font-size: 16px;">UBER Eats</label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label style="font-weight: normal;">Total:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="uber.ctotal">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group" align="right">
                      <label>Total:</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="uber.ctotal" readonly>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div id="amex" class="tab-pane">
              <div class="row">
                  <div class="col-md-12">
										<div class="form-group">
											<label style="font-size: 16px;">Terminal American Express</label>
										</div>
									</div>
                  <div class="col-md-6">
										<div class="form-group">
											<label style="font-weight: normal;">No. de terminal:</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label style="font-weight: normal;">Total:</label>
										</div>
									</div>
                  <template v-for="subtipopago in subtipoPago">
                    <template v-if="subtipopago.cidsubtipopago==5&&subtipopago.cestatus==1&&subtipopago.cidtipopago==8">
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" v-model="subtipopago.cnombresubtipopago" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="number" class="form-control" v-model="amexterminal1.ctotal" @keyup='totalAmexpress'>
                        </div>
                      </div>
                    </template>
                    <template v-if="subtipopago.cidsubtipopago==6&&subtipopago.cestatus==1&&subtipopago.cidtipopago==8">
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" v-model="subtipopago.cnombresubtipopago" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="number" class="form-control" v-model="amexterminal2.ctotal" @keyup='totalAmexpress'>
                        </div>
                      </div>
                    </template>
                  </template>
                  <div class="col-md-6">
                    <div class="form-group" align="right">
                      <label>Total:</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-addon">
                             <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" class="form-control" v-model="amexpressTotal" readonly>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div id="gasto" class="tab-pane">
              <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
											<label style="font-size: 16px;">Gastos</label>
										</div>
                    <div class="form-group">
      								<label style="font-weight: normal;">Descripción:</label>
      								<textarea class="form-control" v-model="precierre.cdescripciongasto" rows="3"></textarea>
      							</div>
                    <div class="form-group">
                      <label style="font-weight: normal;">Total:</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-dollar"></i>
                        </div>
                        <input type="text" name="cantgasto" class="form-control" v-model="precierre.ctotalgasto">
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-pull-right" @click ="abrirModalConfirmar">Enviar</button>
        </div>
    </div>
  </div>
</div>
