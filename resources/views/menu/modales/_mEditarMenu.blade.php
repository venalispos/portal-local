<form  @submit.prevent="actualizarMenu()">
    {{ csrf_field() }}
    <div class="modal fade" id="editarM" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" @click='reiniciarNuevoMenu()'>&times;</button>
                    <h4>AGREGAR MENÚ</h4>
                    <div id="mensaje_validacion" class="alert alert-warning" role="alert">Complete todos los campos para añadir menú.</div>
                </div >
                <div class="modal-body">
                    
                    <div class="row" >
                        <div class="col-md-4" align="left">
                            <label>Folio</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="number" disabled="" required='true' v-model='nuevo_menu.Folio' placeholder="Folio" class="form-control" name="">
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                    	<div class="col-md-4" align="left">
                    		<label>Orden</label>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
                    			<input type="number" required='true' v-model='nuevo_menu.id' placeholder="Orden" class="form-control" name="">
                    		</div>
                    	</div>
                    </div>
                    <div class="row" >
                    	<div class="col-md-4" align="left">
                    		<label>Nombre</label>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
                    			<input type="text" required='true' v-model='nuevo_menu.nombre' placeholder="Nombre" class="form-control" name="">
                    		</div>
                    	</div>
                    </div>
                    <div class="row" >
                    	<div class="col-md-4" align="left">
                    		<label>Folio Padre</label>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
                    			<input type="number" required='true' v-model='nuevo_menu.folPadre' placeholder="Folio Padre" class="form-control" name="">
                    		</div>
                    	</div>
                    </div>
                    <div class="row" >
                    	<div class="col-md-4" align="left">
                    		<label>URL</label>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
                    			<input type="text" required='true' v-model='nuevo_menu.URL' placeholder="URL" class="form-control" name="">
                    		</div>
                    	</div>
                    </div>
                    <div class="row" >
                    	<div class="col-md-4" align="left">
                    		<label>Nodos hijos</label>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
                    			<input type="number" required='true'  v-model='nuevo_menu.nodHijos' placeholder="Modos Hijos" class="form-control" maxlength="1" name="">
                    			<span class="help-block">0 = NO / 1 = Si</span>
                    		</div>
                    	</div>
                    </div>
                    <div class="row" >
                    	<div class="col-md-4" align="left">
                    		<label>Nuevo</label>  
                    	</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
                    			<input type="text" required='true' v-model='nuevo_menu.nuevo' placeholder="Nuevo" class="form-control" name="">
                    			<span class="help-block">0 = NO / 1 = Si</span>
                    		</div>
                    	</div>
                    </div>
                    <div class="row" >
                    	<div class="col-md-4" align="left">
                    		<label>Nombre icono</label>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
                    			<input type="text" required='true' v-model='nuevo_menu.nombreIcono' placeholder="ICono" class="form-control" name="">
                    			<span class="help-block">Ejemplo. fa fa-circle-o</span>
                    		</div>
                    	</div>
                    </div>
              </div>
              <div class="modal-footer">
               <div class="pull-right">
                <button class="btn btn-success" type="submit"   :disabled='disabledButton' >Guardar</button>
            </div>
        </div>
    </div>
</div>
</div>
</form>