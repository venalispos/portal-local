<form  @submit.prevent="eliminarMenu()">
	{{ csrf_field() }}
	<div class="modal fade" id="deleteMenu" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" @click='reiniciarNuevoMenu()'>&times;</button>
					<h4>¿Desea eliminar este elemento del Menú?</h4>
				</div >
				<div class="modal-body">
					<div class="form-group">
						<button class="btn btn-danger btn pull-right " >Eliminar</button>
					</div>
				</div>
				<div class="modal-footer">
					
				</div>
			</div>
		</div>
	</div>
</form>