@extends('Master')
@section('titulo','Configuracion del Menú')
@section('content')
<div id="configuracionmenu">
	<div class="row">
		<div class="col-md-12" align="right">
			<div class="form-group">
				<button class="btn btn-primary" @click='mAgregar()'>Agregar Menú</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped dataTable">
					<thead>
						<th align="center" style="text-align: center">ID</th>
						<th align="center" style="text-align: center">Orden</th>
						<th align="center" style="text-align: center">Nombre</th>
						<th align="center" style="text-align: center">Folio <br> Padre</th>
						<th align="center" style="text-align: center">Ruta</th>
						<th align="center" style="text-align: center">Nodos <br> Hijos</th>
						<th align="center" style="text-align: center">Nuevo</th>
						<th align="center" style="text-align: center">Icono</th>
						<th align="center" style="text-align: center">Acciones</th>
					</thead>
					<tbody>
						<template v-for='menu in menuarray'>
							<tr v-if='menu.cMenHijos==1' style="font-style: bold;">
								<td align="center" style="text-align: center"><strong>@{{menu.cMenFolio}} </strong></td>
								<td align="center" style="text-align: center"><strong>@{{menu.cMenOrden}} </strong></td>
								<td align="center" style="text-align: center"><strong>@{{menu.cMenDescripcion}} </strong></td>
								<td align="center" style="text-align: center"><strong>@{{menu.cMenFolPadre}} </strong></td>
								<td align="center" style="text-align: center">@{{menu.cMenURL}} </strong></td>
								<td align="center" style="text-align: center"><strong>@{{menu.cMenHijos}}</strong></td>
								<td align="center" style="text-align: center"><strong>0 </strong></td>
								<td align="center" style="text-align: center"><strong>@{{menu.cMenIcono}} </strong></td>
								<td class="text-center">
									<button class="btn btn-default" id="1" name="Agregar" data-toggle="tooltip" @click='editarMenu(menu)'  title="" data-original-title="Editar">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="btn btn-default" id="1" name="editar" data-toggle="tooltip" title="" @click='confirmacionDeleteMen(menu.cMenFolio)' data-original-title="Editar">
										<i class="fa fa-trash"></i>
									</button>
								</td>
							</tr>
							<tr v-else>
								<td align="center" style="text-align: center">@{{menu.cMenFolio}}</td>
								<td align="center" style="text-align: center">@{{menu.cMenOrden}}</td>
								<td align="center" style="text-align: center">@{{menu.cMenDescripcion}}</td>
								<td align="center" style="text-align: center">@{{menu.cMenFolPadre}}</td>
								<td align="center" style="text-align: center">@{{menu.cMenURL}}</td>
								<td align="center" style="text-align: center">@{{menu.cMenHijos}}</td>
								<td align="center" style="text-align: center">0</td>
								<td align="center" style="text-align: center">@{{menu.cMenIcono}}</td>
								<td class="text-center">
									<button class="btn btn-default" id="1" name="Agregar" data-toggle="tooltip" @click='editarMenu(menu)'  title="" data-original-title="Editar">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="btn btn-default" id="1" name="editar" data-toggle="tooltip" title="" @click='confirmacionDeleteMen(menu.cMenFolio)' data-original-title="Editar">
										<i class="fa fa-trash"></i>
									</button>
								</td>
							</tr>
						</template>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@include('menu/modales/_mAgregar')
	@include('menu/modales/_mEditarMenu')
	@include('menu/modales/_mElimiarMenu')
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-menu.js"></script>
@endsection
