<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="css/app.css">
</head>
<body>
<div>
	Bienvenido {{auth()->user()->name}}
</div>
<div class="col-md-3">

	<form method="post" action="{{route('logout')}}">
		{!!csrf_field()!!}
		<button class="btn btn-danger">Cerrar session</button>
	</form>
</div>
</body>
</html>
