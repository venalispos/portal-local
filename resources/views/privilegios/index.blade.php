@extends('Master')
@section('titulo','Privilegios')
@section('content')
<div id="Privilegio">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-9">
				<div class="form-group">
					<form @submit.prevent="buscarUsuario()">
					<label for="">Ingrese un usuario</label>
					<input type="text" v-model='indicios' class="form-control">
					</form>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="">Opciones</label>
					<select name="" id="" @change="" class="form-control">
						<option value="" disabled="true" selected="">SELECCIONE UNA OPCION</option>
						<option value="">Administrar privilegios</option>
						<option value="">Reiniciar contraseña</option>
					</select>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection 
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-privilegios.js"></script>	
@endsection
