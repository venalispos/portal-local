@extends('Master')
@section('titulo','Escaneo Ticket')
@section('content')
<div id="registro">
    <div class="row">
        <div class="col-md-12">
            <form @submit.prevent="buscarTicket">
                {!!csrf_field()!!}
                <div class="box box-primary">
                    <div class="box-header with-border text-center">
                        Escanea el Ticket
                    </div>
                    </button>
                    <div class="box-body">
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="number" name="" value="" class="form-control" v-model="Buscar.Ticket" placeholder="Ticket">
                            </div>
                        </div>
                        <div class="col-md-3">
                          <template v-if="Cargando === 0">
                            <div class="form-group">
                                <button type="submit" name="button" class="btn btn-primary pull-right btn col-md-12">
                                    <span class="fa fa-search"></span> Buscar
                                </button>
                            </div>
                          </template>
                          <template v-else>
                            <div class="overlay">
                            <button type="submit" name="button" class="btn btn-primary pull-right btn col-md-12" disabled>
                                <i class="fa fa-refresh fa-spin"></i> Cargando...
                            </button>
                            </div>
                          </template>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row margin">
        <div class="row">
          <template v-if="Estatus">
            <div class="col-md-12">
              <template v-if="Estatus === 1">
                <template v-if="Escaneos === 1">
                  <div class="alert alert-success alert-dismissible">
                      <h3><i class="fa fa-check"></i> Ticket registrado correctamente</h3>
                      Cantidad de clientes: @{{Mesa.cHisMesCantClientes}}
                  </div>
                </template>
                <template v-else>
                  <div class="alert alert-danger alert-dismissible">
                      <h3><i class="fa fa-ban"></i> Ticket escaneado</h3>
                      Cantidad de veces escaneado: @{{Escaneos}}
                  </div>
                </template>
              </template>
              <template v-else>
                <div class="alert alert-warning alert-dismissible">
                    <h3><i class="fa fa-exclamation-triangle"></i> Ticket no encontrado</h3>
                    Cantidad de veces escaneado: @{{Escaneos}}
                </div>
              </template>
              </template>

            </div>
            <div class="col-md-12 table-responsive">
              <template v-if="Productos">
                <table class="table table-bordered table-striped dataTable">
                    <thead>
                        <th style="text-align:center">Codigo</th>
                        <th style="text-align:center">Producto</th>
                        <th style="text-align:center">Precio</th>
                        <th style="text-align:center">Cantidad</th>
                        <th style="text-align:center">Total</th>
                    </thead>
                    <tbody>
                            <tr v-for="Producto in Productos">
                                <td align="right">
                                    @{{Producto.cHisProCodigo}}</td>
                                <td align="center">
                                    @{{Producto.cHisProDesGeneral}}</td>
                                <td align="right">$@{{formatoPrecio(Producto.cHisproPrecioUni)}}</td>
                                <td align="right">
                                    @{{Producto.cHisProCantidad}}</td>
                                <td align="right">$@{{formatoPrecio(Producto.cHisProPrecio)}}</td>
                            </tr>
                            <tr>
                                <td align="right"><b>TOTAL</b> </td>
                                <td align="right"></td>
                                <td align="right"></td>
                                <td align="right"></td>
                                <td align="right"><b>$@{{formatoPrecio(Mesa.cHisMesTotal)}}</b></td>
                            </tr>
                    </tbody>
                </table>
                </template>
            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/ticket/registro.js"></script>
@endsection
