<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="#" class="no logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>{{$textosTitulos[0]['cPorTitTextoAbreviado']}}</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">{{$textosTitulos[0]['cPorTitTextoMenu']}}</span>
  </a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" >
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Navbar Right Menu -->
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">

        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="/img/userempty.png" class="user-image" alt="User Image"/>
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">{{auth()->user()->cUsuNombre}} {{auth()->user()->cUsuApellido}}</span>
            </a>
            <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                    <img src="/img/userempty.png" class="img-circle" alt="User Image" />
                    <p>
                        {{auth()->user()->cUsuNombre}} {{auth()->user()->cUsuApellido}}
                    </p>
                </li>
                <!-- Menu Body -->
                <!-- Menu Footer-->
            </li>
            <li class="user-footer">
                <div class="pull-right">
                    <form method="post" action="{{route('logout')}}">
                  	{!!csrf_field()!!}
                    <button class="btn btn-default btn-flat">Cerrar session</button>
                    </form>
                </div>
            </li>
        </ul>
    </li>
</ul>
</div>
</nav>
</header>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box box-default box-solid">
        <div class="box-header with-border">
          <section class="content-header">

              <h1>
                @yield('titulo')
            </h1>
        </section>
    </div>
    <div class="box-body">
      <section class="content">
    <!-- /.content -->@yield('content')
    </section>
    <section class="">
       @yield('scripts')
    </section>
    </div>
<!-- /.box-body -->
</div>



</div>
