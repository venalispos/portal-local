﻿@extends('Master')
@section('titulo','Cierre de caja')
@section('content')
<style>
.dgcAlert {top: 0;position: absolute;width: 100%;display: block;height: 1000px; background: url(http://www.dgcmedia.es/recursosExternos/fondoAlert.png) repeat; text-align:center; opacity:0; display:none; z-index:999999999999999;}
.dgcAlert .dgcVentana{width: 500px; background: white;min-height: 150px;position: relative;margin: 0 auto;color: black;padding: 10px;border-radius: 10px;}
.dgcAlert .dgcVentana .dgcCerrar {height: 25px;width: 25px;float: right; cursor:pointer; background: url(http://www.dgcmedia.es/recursosExternos/cerrarAlert.jpg) no-repeat center center;}
.dgcAlert .dgcVentana .dgcMensaje { margin: 0 auto; padding-top: 45px; text-align: center; width: 400px;font-size: 20px;}
.dgcAlert .dgcVentana .dgcAceptar{background:#09C; bottom:20px; display: inline-block; font-size: 12px; font-weight: bold; height: 24px; line-height: 24px; padding-left: 5px; padding-right: 5px;text-align: center; text-transform: uppercase; width: 75px;cursor: pointer; color:#FFF; margin-top:50px;}

tr:hover{
	background-color: silver;
	color: white;
}
</style>
<div id="cierre">
	@if($mensaje['mensaje'] =='1')
	<div class="row">
		<div class="col-lg-12" align="center" style="text-align: center;">
			<span style="text-align: center;margin: auto;" align="center">EL CIERRE <strong>NO</strong> ESTÁ DISPONIBLE</span>
		</div>
	</div>
	@else
	<div class="row">
		@if($mensaje['mesas'] =='1')
		<div class="col-lg-12" align="center" style="text-align: center;">
			<span><small style="text-align: center;margin: auto;" align="center">Te recordamos que para poder realizar el corte. necesitas cerras todas las mesas abiertas</small></span>
		</div>
		@else
		<div class="row">
			<div class="col-lg-12" style="text-align: center;">

				<div class="form-group "  id='respuesta'>

				</div>
			</div>

		</div>
		@endif
	</div>
	@endif

</div>

@endsection

@section('scripts')
	<script>
		function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
    	if(filter(tempValue)=== false){
    		return false;
    	}else{
    		return true;
    	}
    }else{
    	if(key == 8 || key == 13 || key == 46 || key == 0) {
    		return true;
    	}else{
    		return false;
    	}
    }
}
function filter(__val__){
	var preg = /^([0-9]+\.?[0-9]{0,2})$/;
	if(preg.test(__val__) === true){
		return true;
	}else{
		return false;
	}

}
</script>
<script>
	var _token="{{ csrf_token() }}";
</script>
<script>
	function openForm() {
		document.getElementById("myForm").style.display = "block";
	}

	function closeForm() {
		document.getElementById("myForm").style.display = "none";
	}
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>

	$(document).ready(function() {


			var mesasAbiertas = "<?php print_r($mensaje['mesas'])?>";
			if (mesasAbiertas==0) {
				var mensajes="<?php print_r($mensaje['mensaje'])?>";
				var fechaActual="<?php print_r($mensaje['fecha'])?>";
				var fechaSiguiente="<?php print_r($mensaje['fechaSiguiente'])?>";
				var mensaje="¿SEGURO QUE DESEA REALIZAR EL CIERRE DEL DÍA "+fechaActual+"?";
				var mensajeCuerpo="Recuerda que no podrás hacer más movimientos con la fecha "+fechaActual+" y a partir del cierre comenzarás a trabajar con la fecha "+fechaSiguiente;
				if (mensajes==0) {
					var dgcTiempo=500
				var ventanaCS='<div class="dgcAlert"><div class="dgcVentana"><div class="dgcCerrar"></div><div class="dgcMensaje">'+mensaje+'<br> <span><small>'+mensajeCuerpo+' </small></span><br><div class="dgcAceptar">Aceptar</div></div></div></div>';
				$('body').append(ventanaCS);
				var alVentana=$('.dgcVentana').height();
				var alNav=$(window).height();
				var supNav=$(window).scrollTop();
				$('.dgcAlert').css('height',$(document).height());
				$('.dgcVentana').css('top',((alNav-alVentana)/2+supNav-100)+'px');
				$('.dgcAlert').css('display','block');
				$('.dgcAlert').animate({opacity:1},dgcTiempo);
        $('.dgcAceptar').one('click', function(e) {
					e.preventDefault();
					$('.dgcAlert').animate({opacity:0},dgcTiempo);
					setTimeout("$('.dgcAlert').remove()",dgcTiempo);
				});
				$('.dgcCerrar').click(function(e) {
					location.href="/";
				});
        $('.dgcAceptar').one('click', function(e) {
          e.preventDefault();
          generarMetodos(fechaSiguiente);
        });
				}
			}



	});
</script>
<script>
	function generarMetodos(fechaSiguiente){
		actualizarAsistencia(fechaSiguiente);
	}
	function actualizarBitacora(fechaSiguiente){
		$.ajax({
		url: '/cierre/generarBitaroca',
		type:'POST',
		datatype:'html',
		data:{_token:_token},
	})
	.done(function(respuesta){
		if (respuesta==1) {
		$("#respuesta").html("<label style='color:red'>El cierre ya se realizó</label>");
	}else{
		$("#respuesta").html("<label style='color:green'>Se ha generado la bitacora correctamente</label>");
		actualizarAsistencia(fechaSiguiente);
		}
	})
	.fail(function(){
		$("#respuesta").html("<label style='color:red'>No se ha generado la bitacora correctamente</label>");
	})
	}

	function actualizarAsistencia(fechaSiguiente){
		$.ajax({
		url: '/cierre/generarAsistencia',
		type:'POST',
		datatype:'html',
		data:{_token:_token},
	})
	.done(function(respuesta){
		$("#respuesta").append("<br> <label style='color:green'>Proceso de cierre realizado</label>");
		//generarRespaldo(fechaSiguiente);
	})
	.fail(function(){
		$("#respuesta").append("<br> <label style='color:red'>No se ha generado la asistencia correctamente</label>");
	})
	}

	function generarRespaldo(fechaSiguiente){
		$.ajax({
		url: '/cierre/generarRespadlo',
		type:'POST',
		datatype:'html',
		data:{_token:_token},
	})
	.done(function(respuesta){
	$("#respuesta").append("<br>  <label style='color:green'>Se ha generado el respaldo correctamente</label>");
			SincronizarCorte(fechaSiguiente);
	})
	.fail(function(){
		$("#respuesta").append("<br>  <label style='color:red'>No se ha generado el respaldo</label>");
	})
	}

	function SincronizarCorte(fechaSiguiente){
		$.ajax({
		url: '/cierre/generarSincroCorte',
		type:'POST',
		datatype:'html',
			data:{_token:_token,fecha:fechaSiguiente},
	})
	.done(function(respuesta){
		$("#respuesta").append("<br> <label style='color:green'>Se ha generado el corte correctamente</label> ");
		generarNuevafecha(fechaSiguiente);
	})
	.fail(function(){
		$("#respuesta").append("<br> <label style='color:red'>No se ha generado el corte correctamente</label> ");
	})
	}

	function generarNuevafecha(fechaSiguiente){
		console.log(fechaSiguiente);
		$.ajax({
		url: '/cierre/generarFechaTrabajo',
		type:'POST',
		datatype:'html',

		data:{_token:_token,fecha:fechaSiguiente},
	})
	.done(function(respuesta){
		$("#respuesta").append("<br> <label style='color:green'>Se ha cambiado la fecha de trabajo correctamente a "+fechaSiguiente+"</label> ");
		sincronizarEmpleados(fechaSiguiente);
	})
	.fail(function(){
				$("#respuesta").append("<br> <label style='color:red'>Se ha cambiado la fecha de trabajo correctamente a "+fechaSiguiente+"</label> ");
	})
	}

	function sincronizarEmpleados(fechaSiguiente){
		$.ajax({
		url: '/cierre/generarSincroEmpleados',
		type:'POST',
		datatype:'html',
		data:{_token:_token},
	})
	.done(function(respuesta){

	})
	.fail(function(){
		console.log("error");
	})
	}


</script>
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-cierrePOSX.js"></script>
@endsection
