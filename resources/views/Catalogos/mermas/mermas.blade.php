@extends('Master')
@section('titulo','Waste catalog')
@section('content')
<div id="mermas">
  <div class="row">
    <div class=" col-md-4">
      <div class = "box box-primary">
        <div class="box-header text-center">
          Choose date of waste
        </div>
        <div class="box-body">
            {{csrf_field()}}
            <div class="form-group">
              <label for="">Date</label>
              <input type="date" name="date_waste" value="" class = "form-control" v-model = "date_search">
            </div>
            <div class="box-footer">
              <button @click = "obtener_mermas()" class = "btn btn-primary btn-sm btn-block">
                <span class = "fa fa-search"></span> Search</button>
            </div>
        </div>
      </div>
    </div>
  <div class="col-md-8">
      <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Actions</h3>
          </div>
          <br>
          <a @click = "ver_agregar_merma" class="btn btn-app " color = "tomato">
              <i class="fa fa-plus"></i> Add waste
          </a>
<template v-if = "lista_mermas != 0">
      <div class="box box-primary">
        <div class=" box-header text-center">
          Waste catalog
        </div>
        <table class = "table table-stripped table-hover">
          <thead>
            <th style = "text-align:center">Folio</th>
            <th style = "text-align:center">Category</th>
            <th style = "text-align:center">Quantity</th>
          </thead>
          <tbody>
            <tr v-for = "merma in lista_mermas">
              <td align = "center">@{{merma.cFolio}}</td>
              <td align = "center">@{{merma.cCatDescripcion}}</td>
              <td align = "center">@{{merma.cCantidad}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</template>
<template v-else>
  <p align = "center" style = "font-size:12px">There are no records to show</p>
</template>
    @include('Catalogos.mermas.agregar_merma')
</div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/mermas.js"></script>
@endsection
