
  {{ csrf_field() }}
  <div class="modal fade" id='agregar_merma' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click ="">&times;</button>
          <h4 style="font-weight: bold;">New waste</h4>
        </div>
        <div class="modal-body">
          <div class="box box-primary">
            <div class="box-header text-center">
              Add fields
            </div>
            <div class="box-body">
              <div class="form-group">
                <label for="">Category</label>
                <select class="form-control" name="" v-model = "selected_categoria">
                  <option v-for = "categoria in lista_categorias" :value = "categoria.cCatCodigo" >@{{categoria.cCatDescripcion}}</option>
                </select>
              </div>
              <div class="form-group">
                <label for="">Quantity</label>
                <input type="number" name="" value="" class = 'form-control' v-model = "waste_quantity">
              </div>
              <div class="form-group">
                <label for="">Date</label>
                <input type="text" name="" value="" class = 'form-control' v-model = "date_waste" disabled>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button @click  = "agregar_merma()" class="btn btn-success">Send</button>
        </div>
      </div>
    </div>
  </div>
