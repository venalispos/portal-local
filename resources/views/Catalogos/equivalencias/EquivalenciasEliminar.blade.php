<form @submit.prevent="eliminarEquivalencias">
  {!!csrf_field()!!}
  <div class="modal fade" id='EliminarEquivalencias' data-backdrop="static" data-keyboard="false" aria-hidden="false" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModalEliminar()">&times;</button>
          <h4 style="font-weight: bold;">Eliminar equivalencia</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label>¿Estás seguro de querer eliminar la equivalencia: <span style="font-style: italic;">@{{prodPrimario.cProPriDescripcion}} y @{{producto.cProDescripcion}}</span> ?</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger">Eliminar</button>
        </div>
      </div>
    </div>
  </div>
</form>
