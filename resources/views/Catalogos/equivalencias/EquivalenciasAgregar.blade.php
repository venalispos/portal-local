<form @submit.prevent="agregarEquivalencias">
  {!!csrf_field()!!}
  <div class="modal fade" id='AgregarEquivalencia' data-backdrop="static" data-keyboard="false" aria-hidden="false" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal()">&times;</button>
          <h4 style="font-weight: bold;">Nueva equivalencia</h4>
        </div>
        <div class="modal-body">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Producto Primario</label>
                     <multiselect
                        placeholder="Seleccionar producto primario..."
                        select-label="Presiona enter para seleccionar"
                        v-model = 'prodPrimario'
                        :options = 'listaProdPrimarios'
                        label = 'cProPriDescripcion'>
                    </multiselect>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Producto</label>
                     <multiselect
                        placeholder="Seleccionar producto..."
                        v-model = 'producto'
                        :options = 'listaProductos'
                        :custom-label = 'getNombreProdCodigo'>
                    </multiselect>
                  </div>
                </div>
                <div class="col-md-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Equivalencia</label>
                    <input class="form-control" type="number" step="0.01" min="0.01" pattern="^[0-9]+" placeholder="ej: 300" v-model="equivalencia.cEquivalencia" required>
                  </div>
                </div>
                <div class="col-md-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Detalle</label>
                    <input class="form-control" type="text" v-model='prodPrimario.cProPriDetEquivalencia' disabled>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-success">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
