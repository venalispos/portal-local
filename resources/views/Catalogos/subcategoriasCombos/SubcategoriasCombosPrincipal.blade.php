@extends('Master')
@section('titulo','Catálogo subcategorías combos')
@section('content')
  <div id="subcategoriasCombos">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="ModalAgregarComSubcategoria" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar subcategoría combo</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table id="table_subcategoriasCombos" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center"># ID</th>
              <th style="text-align:center">Descripción</th>
              <th style="text-align:center">Categoría</th>
              <th style="text-align:center">Acciones</th>
            </thead>
            <tbody>
              <template v-for="subcategoriascombo in SubcategoriasComb">
                <tr>
                  <td align="center">@{{subcategoriascombo.cComSubCatFolio}}</td>
                  <td align="center">@{{subcategoriascombo.cComSubCatDescripcion}}</td>
                  <td align="center">@{{subcategoriascombo.categoria.cComCatDesc}}</td>
                  <td align="center">
                    <div class="btn-group">
                      <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" @click="ModalEditarComSubcategoria(subcategoriascombo)"><span class="fa fa-edit"></span> Editar</a></li>
                        <li><a href="#" @click="ModalEliminarComSubcategoria(subcategoriascombo)"><span class="fa fa-times"></span> Eliminar</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('Catalogos.subcategoriasCombos.SubcategoriasCombosEliminar')
    @include('Catalogos.subcategoriasCombos.SubcategoriasCombosEditar')
    @include('Catalogos.subcategoriasCombos.SubcategoriasCombosAgregar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/subcategoriaCombos.js"></script>
@endsection
