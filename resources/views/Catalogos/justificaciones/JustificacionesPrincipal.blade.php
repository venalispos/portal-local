@extends('Master')
@section('titulo','Justificaciones')
@section('styles')
    <link rel="stylesheet" href="/css/vue-multiselect.min.css">
@endsection
@section('content')
  <div id="Justificaciones">
    <div class="row-margin">
      <div class="row">
        <div class="col-sm-12">
          <button type='button' @click ="abrirModal()" class = 'btn btn-success pull-right'>
            <span class = 'fa fa-plus'></span> Agregar justificación
          </button>
        </div>
      </div>
      <br>
      <div class="box">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-striped dataTable">
                <thead>
                  <tr>
                    <th scope="col"> ID </th>
                    <th scope="col"> Categoría </th>
                    <th scope="col"> Descripción </th>
                    <th scope="col"> Acciones </th>
                  </tr>
                </thead>
                <tbody>
                    <template v-if="justificaciones != 0">
                      <tr v-for='justificacion in justificaciones'>
                        <td>@{{ justificacion.cJusID }}</td>
                          <td v-if="justificacion.cJusCategoria == 1"> Cancelación </td>
                          <td v-if="justificacion.cJusCategoria == 2"> Cortesía </td>
                          <td v-if="justificacion.cJusCategoria == 3"> Descuentos </td>
                        <td>@{{ justificacion.cJusDescripcion }}</td>
                        <td align="center">
                          <div class="btn-group">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                              <li><a href="#" @click="abrirModalEditar(justificacion)"><span class="fa fa-edit"></span> Editar</a></li>
                              <li><a href="#" @click="abrirModalEliminar(justificacion)"><span class="fa fa-times"></span> Eliminar</a></li>
                            </ul>
                          </div></td>
                      </tr>
                    </template>
                    <template v-else>
                      <td colspan="4">
                        <p class="text-center"> No hay datos que mostrar. </p>
                      </td>
                    </template>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    @include('Catalogos.Justificaciones.JustificacionesAgregar')
    @include('Catalogos.Justificaciones.DescuentosAgregar')
    @include('Catalogos.Justificaciones.JustificacionesEditar')
    @include('Catalogos.Justificaciones.DescuentosEditar')
    @include('Catalogos.Justificaciones.JustificacionesEliminar')
    @include('Catalogos.Justificaciones.DescuentosEliminar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-multiselect.js"></script>
<script src="/vuejs/catalogos/justificaciones.js"></script>
@endsection
