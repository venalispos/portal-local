<form @submit.prevent="eliminarDescuento()">
  {!!csrf_field()!!}
  <div class="modal fade" id='EliminarDescuento' data-backdrop="static" data-keyboard="false" aria-hidden="false" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModalEliminarDesc()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 style="font-weight: bold;">Eliminar descuento</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label>¿Estás seguro de querer eliminar el descuento: <span style="font-style: italic;">@{{ fillDescuento.cDescDescripcion }} </span> ?</label>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" @click ="cerrarModalEliminarDesc()"> Cerrar </button>
          <button type="submit" class="btn btn-danger"> Eliminar </button>
        </div>
      </div>
    </div>
  </div>
</form>
