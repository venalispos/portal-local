<form @submit.prevent="eliminarJustificacion()">
  {!!csrf_field()!!}
  <div class="modal fade" id='EliminarJustificacion' data-backdrop="static" data-keyboard="false" aria-hidden="false" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModalEliminar()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 style="font-weight: bold;">Eliminar justificación</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label>¿Estás seguro de querer eliminar la justificación: <span style="font-style: italic;">@{{ fillJustificacion.cJusDescripcion }} </span> ?</label>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" @click ="cerrarModalEliminar()"> Cerrar </button>
          <button type="submit" class="btn btn-danger"> Eliminar </button>
        </div>
      </div>
    </div>
  </div>
</form>
