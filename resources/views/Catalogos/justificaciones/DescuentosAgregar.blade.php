<form @submit.prevent="agregarDescuento()">
  {!!csrf_field()!!}
  <div id="AgregarDescuento" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModalDescuento()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 style="font-weight: bold;">Nuevo descuento</h4>
        </div>

        <div class="modal-body">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label"> ID </label>
                    <input class="form-control" type="text" v-model="descuento.cDescIdPadre">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label"> Descripción </label>
                    <input class="form-control" type="text" v-model="descuento.cDescDescripcion">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label"> Cantidad </label>
                    <input class="form-control" type="number" step="0.1" min="0.1" pattern="^[0-9]+" v-model="descuento.cDescCantidad">
                  </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" @click ="cerrarModalDescuento()"> Cerrar </button>
              <button type="submit" class="btn btn-success"> Guardar </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
