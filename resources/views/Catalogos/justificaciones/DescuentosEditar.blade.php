<form @submit.prevent="editarDescuento()">
  {!!csrf_field()!!}
  <div id="EditarDescuento" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModalEditarDesc()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 style="font-weight: bold;">Editar descuento</h4>
        </div>

        <div class="modal-body">
          <div class="box box-warning">
            <div class="box-body">
              <template v-id="fillDescuento != null" >
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label"> ID </label>
                      <input class="form-control" type="text" v-model="fillDescuento.cDescIdPadre" disabled>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label"> Descripción </label>
                      <input class="form-control" type="text" v-model="fillDescuento.cDescDescripcion">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label"> Cantidad </label>
                      <input class="form-control" type="number" step="0.1" min="0.1" pattern="^[0-9]+" v-model="fillDescuento.cDescCantidad">
                    </div>
                  </div>
                </div>
              </template>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" @click ="cerrarModalEditarDesc()"> Cerrar </button>
              <button type="submit" class="btn btn-warning"> Guardar </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
