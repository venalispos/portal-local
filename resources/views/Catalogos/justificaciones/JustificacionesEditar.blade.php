<form @submit.prevent="editarJustificacion()">
  {!!csrf_field()!!}
  <div id="EditarJustificacion" class="modal fade" role="dialog" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModalEditar()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 style="font-weight: bold;"> Editar justificacion </h4>
        </div>
        <div class="modal-body">
          <div class="box box-warning">
            <div class="box-body">
              <template v-if="fillJustificacion != null">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label"> Categoría </label>
                        <select class="form-control" v-model="fillJustificacion.cJusCategoria" required>
                          <option value="0" disabled> Seleccionar </option>
                          <option value="1"> Cancelación </option>
                          <option value="2"> Cortesía </option>
                          <option value="3"> Descuentos </option>
                        </select>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label"> Descripción </label>
                      <input class="form-control" type="text" v-model="fillJustificacion.cJusDescripcion" required>
                    </div>
                  </div>
                </div>
                <div class="panel-group">
                <div class="panel panel-default" v-for="descuento in fillJustificacion.descuentos">
                   <div class="panel-heading">Descuentos Autorizados</div>
                   <div class="panel-body" >
                     <template v-if="descuento != 0">
                       <div class="col-md-6">
                         <div class="form-group">
                           <label class="control-label"> Descripción </label>
                           <input class="form-control" type="text" v-model="descuento.cDescDescripcion" disabled>
                         </div>
                       </div>
                       <div class="col-md-3">
                         <div class="form-group">
                           <label class="control-label"> Cantidad </label>
                           <input class="form-control" type="number" step="0.1" min="0.1" pattern="^[0-9]+" v-model="descuento.cDescCantidad" disabled>
                         </div>
                       </div>
                       <div class="col-md-3">
                         <div class="form-group">
                           <label class="control-label"> Acciones </label> <br>
                           <button type='button' @click ="abrirModalEditarDesc(descuento)" class = 'btn btn-primary'>
                             <span class = 'fa fa-edit'></span>
                           </button>
                           <button type='button' @click ="abrirModalEliminarDesc(descuento)" class = 'btn btn-primary'>
                             <span class = 'fa fa-times'></span>
                           </button>
                         </div>
                       </div>
                     </template>
                     <template v-else>
                       <p class="text-center"> No hay datos que mostrar. </p>
                     </template>
                     </div>
                   </div>
                </div>
              </div>
              </template>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" @click ="cerrarModalEditar()"> Cerrar </button>
              <button type="submit" class="btn btn-warning"> Actualizar </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
