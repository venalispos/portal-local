<form @submit.prevent="agregarJustificacion()">
  {!!csrf_field()!!}
  <div id="AgregarJustificacion" class="modal fade" role="dialog" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 style="font-weight: bold;">Nueva justificación</h4>
        </div>

        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <button type='button' @click ="abrirModalDescuento()" class = 'btn btn-success pull-right'>
                <span class = 'fa fa-plus'></span> Agregar descuentos
              </button>
            </div>
          </div>
          <br>
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label"> ID </label>
                    <input class="form-control" type="text" v-model="justificacion.cJusID">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label"> Categoría </label>
                    <select class="form-control" v-model="justificacion.cJusCategoria" required>
                      <option value="0" disabled> Seleccionar </option>
                      <option value="1"> Cancelación </option>
                      <option value="2"> Cortesía </option>
                      <option value="3"> Descuentos </option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label"> Descripción </label>
                    <input class="form-control" type="text" v-model="justificacion.cJusDescripcion">
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" @click ="cerrarModal()"> Cerrar </button>
              <button type="submit" class="btn btn-success"> Guardar </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
