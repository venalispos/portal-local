 @extends('Master')
 @section('titulo','CatÃ¡logo productos')
 @section('content')
 <style>
 #btn {
 	/* Color del texo */

 	color: #0099CC;
 	/* Eliminar color de fondo */

 	background: transparent;
 	/* Grosor del borde, estilo de lÃ­nea y color */

 	border: 2px solid #0099CC;
 	/* AÃ±adir esquinas curvadas */

 	border-radius: 6px;
 	/* Poner texto en mayÃºsculas */

 	border: none;
 	padding: 16px 32px;
 	text-align: center;
 	display: inline-block;
 	font-size: 16px;
 	margin: 4px 2px;
 	-webkit-transition-duration: 0.4s; /* Safari */
 	transition-duration: 0.4s;
 	cursor: pointer;
 	text-decoration: none;
 	text-transform: uppercase;
 }
 /* Al poner el curso encima (hover) */

 #btn1:hover {
 	background-color: #008CBA;
 	color: white;
 }
</style>
<div id="catalogoProductos">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<button id="btn"  @click='mNuevoProductos()'>Nuevo <i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table id="example" class="table table-striped table-bordered" style="width:100%">
					<thead>
						<th>Codigo</th>
						<th>Descripcion</th>
						<th>Costo</th>
						<th>Precio</th>
						<th>Catagoria</th>
						<th>Subcategoria</th>
						<th>Acciones</th>
					</thead>
					<tbody>
						<template v-for='producto in arrProducts'>
							<tr>
								<td>@{{producto.cProCodigo}}</td>
								<td>@{{producto.cProDescripcion}}</td>
								<td>@{{producto.cProCosto}}</td>
								<td>@{{producto.cProPrecio}}</td>
								<template v-for='categorias in arrCategorias' v-if='categorias.cCatCodigo == producto.cProCategoria'>
								<td align="center" v-if="categorias!=null">@{{categorias.cCatDescripcion}}</td>
                			 	<td v-else></td>
								</template>
								<template v-for='subcategorias in arrSubCategorias' v-if='subcategorias.cSCatCodigo == producto.cProSubCategoria'>
								<td align="center" v-if="botonproducto.producto!=null">@{{subcategorias.cSCatDescripcion}}</td>
                  					<td align="center" v-else></td>
								</template>
								<td align="right">
									<div class="btn-group "><button type="button" data-toggle="dropdown" aria-expanded="false" class="btn btn-secondary dropdown-toggle">MÃ¡s
										<span class="caret"></span></button>
										<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="#" @click='mEditarProducto(producto)'><span class="fa fa-pencil"></span> Editar </a></li>
											<li><a href="#"><span class="fa fa-trash-o"></span> Eliminar </a></li>
										</ul>
									</div>
								</td>
							</tr>
						</template>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@include('Catalogos.productos.modales.mEditarProducto')
</div>
@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-catalogoProductos.js"></script>
<SCRIPT LANGUAGE="JavaScript">
function NumCheck(e, field) {
  key = e.keyCode ? e.keyCode : e.which
  // backspace
  if (key == 8) return true
  // 0-9
  if (key > 47 && key < 58) {
    if (field.value == "") return true
    regexp = /.[0-9]{2}$/
    return !(regexp.test(field.value))
  }
  // .
  if (key == 46) {
    if (field.value == "") return false
    regexp = /^[0-9]+$/
    return regexp.test(field.value)
  }
  // other key
  return false

}

</script>
<script>
	function validaNumericos(event) {
		if(event.charCode >= 48 && event.charCode <= 57){
			return true;
		}
		return false;
	}
</script>
@endsection
