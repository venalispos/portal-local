<form @submit.prevent="ProductoAgregar">
  {!!csrf_field()!!}
  <div class="modal fade" id='AgregarProducto' data-backdrop="static" data-keyboard="false" aria-hidden="false" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('AgregarProducto')">&times;</button>
          <h4 style="font-weight: bold;">Nuevo producto</h4>
        </div>
        <div class="modal-body">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Código</label>
                    <input class="form-control" v-model="DatosProducto.cProCodigo" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Intercódigo</label>
                    <input class="form-control" v-model="DatosProducto.cProInterCodigo">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Descripción</label>
                    <input class="form-control" v-model="DatosProducto.cProDescripcion" @keyup="FormatearDescMayusculas" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Descripción botón</label>
                    <input class="form-control" v-model="DatosProducto.cProDesBoton" @keyup="FormatearDescBMayusculas" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Descripción ticket</label>
                    <input class="form-control" v-model="DatosProducto.cProDesTicket" @keyup="FormatearDescTMayusculas" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Precio</label>
                    <div class="input-group">
  								    <div class="input-group-addon">
  										  <i class="fa fa-dollar"></i>
  										</div>
  										<input type="number" class="form-control" v-model="DatosProducto.cProPrecio" step="0.0001" required>
  				          </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Categoría</label>
                    <select class="form-control" v-model="DatosProducto.cProCategoria" required>
                      <option value="0" disabled>SELECCIONAR</option>
                      <option v-for="categoria in Categorias" :value="categoria.cCatCodigo">@{{categoria.cCatDescripcion}}</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Clasificación</label>
                    <select class="form-control" v-model="DatosProducto.cProClasificacion" required>
                      <option value="0" disabled>SELECCIONAR</option>
                      <option v-for="clasificaciones in Clasificacion" :value="clasificaciones.cClaFolio">@{{clasificaciones.cClaDescripcion}}</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Impuestos</label>
                  </div>
                  <template v-if="Configuracion.cConImpuesto1Activo=='1'">
                    <div class="form-check">
                      <label class="form-check-label">@{{Configuracion.cConImpuesto1Nombre}}</label>
                      <input class="form-check-input" type="checkbox" v-model="DatosProducto.cProImpuesto1" style="float:right;">
                    </div>
                  </template>
                  <template v-else>
                    <div class="form-check">
                      <label class="form-check-label" style="color:#ffffff;">Impuesto 1</label>
                    </div>
                  </template>
                  <template v-if="Configuracion.cConImpuesto2Activo=='1'">
                    <div class="form-check">
                      <label class="form-check-label">@{{Configuracion.cConImpuesto2Nombre}}</label>
                      <input class="form-check-input" type="checkbox" v-model="DatosProducto.cProImpuesto2" style="float:right;">
                    </div>
                  </template>
                  <template v-else>
                    <div class="form-check">
                      <label class="form-check-label" style="color:#ffffff;">Impuesto 2</label>
                    </div>
                  </template>
                  <template v-if="Configuracion.cConImpuesto3Activo=='1'">
                    <div class="form-check">
                      <label class="form-check-label">@{{Configuracion.cConImpuesto3Nombre}}</label>
                      <input class="form-check-input" type="checkbox" v-model="DatosProducto.cProImpuesto3" style="float:right;">
                    </div>
                  </template>
                  <template v-else>
                    <div class="form-check">
                      <label class="form-check-label" style="color:#ffffff;">Impuesto 3</label>
                    </div>
                  </template>
                  <template v-if="Configuracion.cConImpuesto4Activo=='1'">
                    <div class="form-check">
                      <label class="form-check-label">@{{Configuracion.cConImpuesto4Nombre}}</label>
                      <input class="form-check-input" type="checkbox" v-model="DatosProducto.cProImpuesto4" style="float:right;">
                    </div>
                  </template>
                  <template v-else>
                    <div class="form-check">
                      <label class="form-check-label" style="color:#ffffff;">Impuesto 4</label>
                    </div>
                  </template><br>
                  <div class="form-group">
                    <label class="control-label">Categoría modificador</label>
                    <select class="form-control" v-model="DatosProducto.cProMod" required>
                      <option value="0" disabled>SELECCIONAR</option>
                      <option v-for="modificador in CatModificadores" :value="modificador">@{{modificador}}</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <template v-if="parseFloat(DatosProducto.cProCodigo)!=0 && DatosProducto.cProDesBoton!=''">
                      <button type="button" class="btn btn-light col-md-12" style="margin-top: 9%; font-weight:bold;" @click ="ModalBotonDescB">Vista previa botón</button>
                    </template>
                    <template v-else>
                      <button type="button" class="btn btn-light col-md-12" style="margin-top: 9%;" disabled>Vista previa botón</button>
                    </template>
                  </div>
                  <div class="form-group">
                    <template v-if="parseFloat(DatosProducto.cProCodigo)!=0 && DatosProducto.cProDesTicket!=''">
                      <button type="button" class="btn btn-light col-md-12" style="margin-top: 17%; font-weight:bold;" @click="ModalBotonDescT">Vista previa ticket</button>
                    </template>
                    <template v-else>
                      <button type="button" class="btn btn-light col-md-12" style="margin-top: 17%;" disabled>Vista previa ticket</button>
                    </template>
                  </div>
                  <div class="form-group" style="margin-top: 57%;">
                    <label class="control-label">Costo</label>
                    <div class="input-group">
  								    <div class="input-group-addon">
  										  <i class="fa fa-dollar"></i>
  										</div>
  										<input type="number" class="form-control" v-model="DatosProducto.cProCosto" required>
  				          </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Subcategoría</label>
                    <select class="form-control" v-model="DatosProducto.cProSubCategoria" required>
                      <option value="0" disabled>SELECCIONAR</option>
                      <option v-for="subcategoria in SubCategorias" :value="subcategoria.cSCatCodigo" v-if="subcategoria.cSCatPadre == DatosProducto.cProCategoria">@{{subcategoria.cSCatDescripcion}}</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Imprimir</label>
                    <select class="form-control" v-model="DatosProducto.cProImprime" required>
                      <option value="0">NO SE IMPRIME</option>
                      <option value="1">BARRA</option>
                      <option value="2">COCINA</option>
                      <option value="3">AMBOS</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Configuración</label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">ACTIVO</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosProducto.cProActivo" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">VISIBLE</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosProducto.cProVisible" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">MODIFICADOR AUTOMÁTICO</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosProducto.cProAutoModi" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">COMBO</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosProducto.cCombo" style="float:right;">
                  </div><br>
                  <div class="form-group">
                    <label class="control-label">Cantidad modificador</label>
                    <input type="number" class="form-control" v-model="DatosProducto.cProModCant">
                  </div>
                </div>
                <template v-if="DatosProducto.cCombo==true">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label">Precio</label>
                      <div class="input-group">
    								    <div class="input-group-addon">
    										  <i class="fa fa-dollar"></i>
    										</div>
    										<input type="number" class="form-control" v-model="DatosProducto.cProComPrecio">
    				          </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Categoría #1</label>
                      <select class="form-control" v-model="DatosProducto.cComCat1">
                        <option value="0" disabled>SELECCIONAR</option>
                        <option v-for="categoria in ComCategorias" :value="categoria.cComCatFolio">@{{categoria.cComCatDesc}}</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Categoría #2</label>
                      <select class="form-control" v-model="DatosProducto.cComCat2">
                        <option value="0" disabled>SELECCIONAR</option>
                        <option v-for="categoria in ComCategorias" :value="categoria.cComCatFolio">@{{categoria.cComCatDesc}}</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Categoría #3</label>
                      <select class="form-control" v-model="DatosProducto.cComCat3">
                        <option value="0" disabled>SELECCIONAR</option>
                        <option v-for="categoria in ComCategorias" :value="categoria.cComCatFolio">@{{categoria.cComCatDesc}}</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Categoría #4</label>
                      <select class="form-control" v-model="DatosProducto.cComCat4">
                        <option value="0" disabled>SELECCIONAR</option>
                        <option v-for="categoria in ComCategorias" :value="categoria.cComCatFolio">@{{categoria.cComCatDesc}}</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Cantidad #1</label>
                      <input type="number" min="0" max="100" class="form-control" v-model="DatosProducto.cComCat1Cant">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Cantidad #2</label>
                      <input type="number" min="0" max="100" class="form-control" v-model="DatosProducto.cComCat2Cant">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Cantidad #3</label>
                      <input type="number" min="0" max="100" class="form-control" v-model="DatosProducto.cComCat3Cant">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Cantidad #4</label>
                      <input type="number" min="0" max="100" class="form-control" v-model="DatosProducto.cComCat4Cant">
                    </div>
                  </div>
                </template>
                <div class="col-md-6">
                  <div class="form-group ">
                    <label class="control-label">Cantidad puntos</label>
                    <input type="number" class="form-control" v-model="DatosProducto.cProPuntos">
                  </div>
                  <div class="form-group">
                    <label class="control-label">Fecha de inicio</label>
                    <input type="date" class="form-control" v-model="DatosProducto.cProFechaInicio">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Fracción</label>
                    <select class="form-control" v-model="DatosProducto.cProFraccion">
                      <option value="0">NO</option>
                      <option value="1">SI</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Fecha de fin</label>
                    <input type="date" class="form-control" v-model="DatosProducto.cProFechaFin">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <th style="text-align:center">Día</th>
                        <th style="text-align:center">Activo</th>
                        <th style="text-align:center">Precio</th>
                        <th style="text-align:center">Hora inicio</th>
                        <th style="text-align:center">Hora fin</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td align="center">LUNES</td>
                          <td align="center"><input class="form-check-input" type="checkbox" v-model="DatosProducto.A1"></td>
                          <td align="center"><input type="number" class="form-control" v-model="DatosProducto.P1"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.I1"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.F1"></td>
                        </tr>
                        <tr>
                          <td align="center">MARTES</td>
                          <td align="center"><input class="form-check-input" type="checkbox" v-model="DatosProducto.A2"></td>
                          <td><input type="number" class="form-control" v-model="DatosProducto.P2"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.I2"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.F2"></td>
                        </tr>
                        <tr>
                          <td align="center">MIÉRCOLES</td>
                          <td align="center"><input class="form-check-input" type="checkbox" v-model="DatosProducto.A3"></td>
                          <td><input type="number" class="form-control" v-model="DatosProducto.P3"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.I3"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.F3"></td>
                        </tr>
                        <tr>
                          <td align="center">JUEVES</td>
                          <td align="center"><input class="form-check-input" type="checkbox" v-model="DatosProducto.A4"></td>
                          <td><input type="number" class="form-control" v-model="DatosProducto.P4"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.I4"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.F4"></td>
                        </tr>
                        <tr>
                          <td align="center">VIERNES</td>
                          <td align="center"><input class="form-check-input" type="checkbox" v-model="DatosProducto.A5"></td>
                          <td><input type="number" class="form-control" v-model="DatosProducto.P5"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.I5"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.F5"></td>
                        </tr>
                        <tr>
                          <td align="center">SÁBADO</td>
                          <td align="center"><input class="form-check-input" type="checkbox" v-model="DatosProducto.A6"></td>
                          <td><input type="number" class="form-control" v-model="DatosProducto.P6"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.I6"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.F6"></td>
                        </tr>
                        <tr>
                          <td align="center">DOMINGO</td>
                          <td align="center"><input class="form-check-input" type="checkbox" v-model="DatosProducto.A0"></td>
                          <td><input type="number" class="form-control" v-model="DatosProducto.A0"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.I0"></td>
                          <td align="center"><input type="time" class="form-control" v-model="DatosProducto.F0"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
