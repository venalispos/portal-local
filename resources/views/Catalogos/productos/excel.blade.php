<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Document</title>
</head>
<body>
	<br>
	<div>
		<table  width="100%">
			<tr style="font-family:Courier;font-size:14px;">
				<th width="15%" style="text-align: center;font-family:Courier;font-size:14px;"> <strong>Código</strong></th>
				<th width="15%" style="text-align: center;font-family:Courier;font-size:14px;"><strong>Intercódigo</strong></th>
				<th width="15%" style="text-align: center;font-family:Courier;font-size:14px;"><strong>Descripción</strong></th>
				<th width="15%" style="text-align: center;font-family:Courier;font-size:14px;"><strong>Precio</strong></th>
				<th width="15%" style="text-align: center;font-family:Courier;font-size:14px;"><strong>Categoría</strong></th>
				<th width="15%" style="text-align: center;font-family:Courier;font-size:14px;"><strong>Subcategoría</strong></th>
        <th width="15%" style="text-align: center;font-family:Courier;font-size:14px;"><strong>Estatus</strong></th>
			</tr>
			@foreach($Productos as $Producto)
			<tr style="font-family:Courier;font-size:14px;">
				<td>{{$Producto['cProFolio']}}</td>
				<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo $Producto['cProInterCodigo'] @endphp</td>
				<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo $Producto['cProDescripcion'] @endphp</td>
				<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo number_format($Producto['cProPrecio'],2) @endphp</td>
				<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo $Producto['categoria']['cCatDescripcion'] @endphp</td>
				<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;">@php echo $Producto['sub_categoria']['cSCatDescripcion'] @endphp</td>
				@if($Producto['cProActivo'] == 1)
				<td width="15%" style="text-align: right;font-family:Courier;font-size:14px;">ACTIVO</td>
        @else
        <td width="15%" style="text-align: right;font-family:Courier;font-size:14px;">INACTIVO</td>
				@endif
			</tr>
			@endforeach
		</table>
	</div>
</body>
</html>
