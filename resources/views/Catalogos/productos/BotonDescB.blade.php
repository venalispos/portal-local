<div class="modal fade" id="VerBotonDescB" tabindex="-1" role="dialog" aria-labelledby="VerBotonDescBTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" @click ="">&times;</button>
           <h4 style="font-weight: bold;">Vista previa botón</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div style="background-color: #ff7f27; width:245px; height:62px; margin: 0 auto;">
              <span style="font-family:'lucida-console'; color:white; font-size:22px; margin-left: 6px;">@{{DatosProducto.cProDesBoton}}</span>
            </div>
            <div style="background-color: #ff7f27; width:245px; height:18px; margin: 0 auto;">
              <span style="font-family:'lucida-console'; color:white; font-size:16px; margin-left: 8px;">@{{DatosProducto.cProCodigo}}</span>
              <span style="font-family:'lucida-console'; color:white; font-size:16px; margin-left: 88px;">$ @{{Precio}}</span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
