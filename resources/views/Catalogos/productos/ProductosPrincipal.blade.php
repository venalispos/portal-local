@extends('Master')
@section('titulo','Catálogo productos')
@section('content')
<style>
  @font-face {
  font-family: 'lucida-console';
  src: url('/fonts/lucida-console.ttf') format('truetype');
  }

  a{
    text-decoration: none;
  }

  .main-wrap {
      background: #000;
          text-align: center;
  }
  .main-wrap h1 {
          color: #fff;
              margin-top: 50px;
      margin-bottom: 100px;
  }
  .col-md-3 {
  	display: block;
  	float:left;
  	margin: 1% 0 1% 1.6%;
  	  background-color: white;
    padding: 50px 0;
  }

  .col:first-of-type {
    margin-left: 0;
  }


  /* ALL LOADERS */

  .loader{
    width: 100px;
    height: 100px;
    border-radius: 100%;
    position: relative;
    margin-left: 300%;
  }

  /* LOADER 5 */

  #loader-5 span{
    display: block;
    position: absolute;
    left: calc(50% - 20px);
    top: calc(50% - 20px);
    width: 20px;
    height: 20px;
    background-color: green;
  }

  #loader-5 span:nth-child(2){
    animation: moveanimation1 1s ease-in-out infinite;
  }

  #loader-5 span:nth-child(3){
    animation: moveanimation2 1s ease-in-out infinite;
  }

  #loader-5 span:nth-child(4){
    animation: moveanimation3 1s ease-in-out infinite;
  }

  @keyframes moveanimation1{
    0%, 100%{
      -webkit-transform: translateX(0px);
      -ms-transform: translateX(0px);
      -o-transform: translateX(0px);
      transform: translateX(0px);
    }

    75%{
      -webkit-transform: translateX(30px);
      -ms-transform: translateX(30px);
      -o-transform: translateX(30px);
      transform: translateX(30px);
    }
  }

  @keyframes moveanimation2{
    0%, 100%{
      -webkit-transform: translateY(0px);
      -ms-transform: translateY(0px);
      -o-transform: translateY(0px);
      transform: translateY(0px);
    }

    75%{
      -webkit-transform: translateY(30px);
      -ms-transform: translateY(30px);
      -o-transform: translateY(30px);
      transform: translateY(30px);
    }
  }

  @keyframes moveanimation3{
    0%, 100%{
      -webkit-transform: translate(0px, 0px);
      -ms-transform: translate(0px, 0px);
      -o-transform: translate(0px, 0px);
      transform: translate(0px, 0px);
    }

    75%{
      -webkit-transform: translate(30px, 30px);
      -ms-transform: translate(30px, 30px);
      -o-transform: translate(30px, 30px);
      transform: translate(30px, 30px);
    }
  }

  }

</style>
<div id="catalogoProductos">
  <div class="row margin">
    <div class="row">
      <div class="col-md-6">
        <button type='button' @click ="abrirModal('AgregarProducto',0)" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar producto</button>
      </div>
      <div class="col-md-6">
        <button type='button' @click ="descargarExcel()" class = 'btn btn-success pull-left'><span class = 'fa fa-arrow-circle-down'></span> Descargar</button>
      </div>
      <div class="col-md-12 table-responsive">
        <table id="table_productos" class="table table-bordered table-striped dataTable">
          <thead>
            <th style="text-align:center">Código</th>
            <th style="text-align:center">Intercódigo</th>
            <th style="text-align:center">Descripción</th>
            <th style="text-align:center">Precio</th>
            <th style="text-align:center">Categoría</th>
            <th style="text-align:center">Subcategoría</th>
            <th style="text-align:center">Estatus</th>
            <th style="text-align:center">Acciones</th>
          </thead>
          <tbody>
            <template v-if="Cargando === 1">
              <div class="col-md-12">
                <div class="loader" id="loader-5">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
              </div>
            </template>
            <template v-for="Producto in Productos">
              <tr>
                <td align="center">@{{Producto.cProCodigo}}</td>
                <td align="center">@{{Producto.cProInterCodigo}}</td>
                <td align="center">@{{Producto.cProDescripcion}}</td>
                <td align="center">$ @{{Producto.cProPrecio}}</td>
                <td align="center" v-if="Producto.categoria!=null">@{{Producto.categoria.cCatDescripcion}}</td>
                <td align="center" v-else></td>
                <td align="center" v-if="Producto.sub_categoria!=null">@{{Producto.sub_categoria.cSCatDescripcion}}</td>
                <td align="center" v-else></td>
                <td align="center" v-if="Producto.cProActivo==1"><small class="label label-success">Activo</small></td>
                <td align="center" v-if="Producto.cProActivo==0"><small class="label label-danger">Inactivo</small></td>
                <td align="center"><button class="btn btn-warning" @click="abrirModal('EditarProducto',Producto)"><i class="fa fa-pencil" aria-hidden="true"></i></button></td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @include('Catalogos.productos.ProductosAgregar')
  @include('Catalogos.productos.ProductosEditar')
  @include('Catalogos.productos.BotonDescB')
  @include('Catalogos.productos.BotonDescT')
</div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/productos.js"></script>
@endsection
