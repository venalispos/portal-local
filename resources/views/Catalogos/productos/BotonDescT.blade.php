<div class="modal fade" id="VerBotonDescT" tabindex="-1" role="dialog" aria-labelledby="VerBotonDescBTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" @click ="">&times;</button>
           <h4 style="font-weight: bold;">Vista previa ticket</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div style="width:80mm; height: 30mm; margin: 0 auto; font-family:'lucida-console';">
              <span>====================================</span><br><br>
              <table>
                <tr style="font-size:15px;">
                  <template v-for="descripcion in DescTicket">
                    <td align="center" v-if="descripcion!=''">@{{descripcion}}</td>
                    <td align="center" v-if="descripcion==''">&nbsp;</td>
                  </template>
                </tr>
              </table><br>
              <span>====================================</span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
