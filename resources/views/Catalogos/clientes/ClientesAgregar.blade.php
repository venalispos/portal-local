<form @submit.prevent="agregarCliente">
  {{ csrf_field() }}
  <div class="modal fade" id="agregarClientesModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click ="eliminarData">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" style="font-weight: bold;">Nuevo Cliente</h4>
        </div>
        <div class="modal-body">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Código:</label>
                    <input class="form-control" v-model="dataCliente.ccodigocliente" placeholder="02895" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Código admintotal:</label>
                    <input class="form-control" v-model="dataCliente.ccodigoadmintotal" placeholder="02895" required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Fecha alta:</label>
                    <input type="date" class="form-control" v-model="dataCliente.cfechaalta" required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Nombre:</label>
                    <input class="form-control" v-model="dataCliente.cnombrecliente" placeholder="Nombre completo" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">RFC:</label>
                    <input class="form-control" v-model="dataCliente.crfccliente" placeholder="VECJ880326 XXX" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">CURP:</label>
                    <input class="form-control" v-model="dataCliente.ccurpcliente" placeholder="VEGA780615HDFLML08" required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Domicilio comercial:</label>
                    <input class="form-control" v-model="dataCliente.cdomiciliocomercial" placeholder="Domicilio completo" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Representante Legal:</label>
                    <input class="form-control" v-model="dataCliente.crepesentantelegal" placeholder="Nombre representante legal">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Estatus:</label>
                    <select class="form-control" v-model="dataCliente.cestatuscliente" required>
                      <option value="0">Inactivo</option>
                      <option value="1">Activo</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Ciudad:</label>
                    <input class="form-control" v-model="dataCliente.cciudadcliente" placeholder="Ciudad Juárez" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Código Postal:</label>
                    <input class="form-control" v-model="dataCliente.ccodigopostal" placeholder="75632" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Email:</label>
                    <input class="form-control" v-model="dataCliente.ccorreoelectronico" placeholder="you@yourdomain.com">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Estado:</label>
                    <input class="form-control" v-model="dataCliente.cestadocliente" placeholder="Chihuahua" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">País:</label>
                    <input class="form-control" v-model="dataCliente.cpaiscliente" placeholder="México" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Teléfono:</label>
                    <input class="form-control" v-model="dataCliente.ctelefonocliente" placeholder="123-245-4512" required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Notas:</label>
                    <textarea  rows="3" cols="80" class="form-control" v-model="dataCliente.cnotascliente" placeholder="Puedes escribir tus notas aquí"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success">Guardar cambios</button>
        </div>
      </div>
    </div>
  </div>
</form>
