<div class="modal fade" id="verClientesModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click ="eliminarData">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" style="font-weight: bold;">Ver Cliente</h4>
      </div>
      <div class="modal-body">
        <div class="box box-primary">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12 table-responsive">
                <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th>Código:</th>
                        <td>@{{dataCliente.ccodigocliente}}</td>
                        <th>Código admintotal:</th>
                        <td>@{{dataCliente.ccodigoadmintotal}}</td>
                      </tr>
                      <tr>
                        <th colspan="1">Fecha alta:</th>
                        <td colspan="3">@{{dataCliente.cfechaalta}}</td>
                      </tr>
                      <tr>
                        <th colspan="1">Nombre:</th>
                        <td colspan="3">@{{dataCliente.cnombrecliente}}</td>
                      </tr>
                      <tr>
                        <th>RFC:</th>
                        <td>@{{dataCliente.crfccliente}}</td>
                        <th>CURP:</th>
                        <td>@{{dataCliente.ccurpcliente}}</td>
                      </tr>
                      <tr>
                        <th colspan="1">Dirección:</th>
                        <td colspan="3">@{{dataCliente.cdomiciliocomercial}}</td>
                      </tr>
                      <tr>
                        <th>Representante legal:</th>
                        <td>@{{dataCliente.crepesentantelegal}}</td>
                        <th>Estatus:</th>
                        <td v-if="dataCliente.cestatuscliente==0"><small class="label label-danger">Inactivo</small></td>
                        <td v-if="dataCliente.cestatuscliente==1"><small class="label label-success">Activo</small></td>
                      </tr>
                      <template v-if="dataCliente.cestatuscliente==0">
                        <tr>
                          <th colspan="1">Fecha baja:</th>
                          <td colspan="3">@{{dataCliente.cfechainactivo}}</td>
                        </tr>
                      </template>
                      <tr>
                        <th>Ciudad:</th>
                        <td>@{{dataCliente.cciudadcliente}}</td>
                        <th>Estado:</th>
                        <td>@{{dataCliente.cestadocliente}}</td>
                      </tr>
                      <tr>
                        <th>Código postal:</th>
                        <td>@{{dataCliente.ccodigopostal}}</td>
                        <th>País:</th>
                        <td>@{{dataCliente.cpaiscliente}}</td>
                      </tr>
                      <tr>
                        <th>Email:</th>
                        <td>@{{dataCliente.ccorreoelectronico}}</td>
                        <th>Teléfono:</th>
                        <td>@{{dataCliente.ctelefonocliente}}</td>
                      </tr>
                      <tr>
                        <th colspan="4">Notas:</th>
                      </tr>
                      <tr>
                        <td colspan="4">@{{dataCliente.cnotascliente}}</td>
                      </tr>
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
