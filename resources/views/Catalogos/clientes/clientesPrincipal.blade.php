@extends('Master')
@section('titulo','Catálogo clientes')

@section('content')
<div id="clientesPrincipal">
  <div class="row margin">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
            <button type='button' @click ="abrirModalAgregarCliente" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar Cliente</button>
        </div>
      </div>
      <div class="col-md-12">
        <hr style="border-top: 1px solid #EAEAEA;">
        <div class="table-responsive">
          <table id="clientesTabla" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center">ID</th>
              <th style="text-align:center">Nombre</th>
              <th style="text-align:center">Teléfono</th>
              <th style="text-align:center">Estatus</th>
              <th style="text-align:center">Acciones</th>
            </thead>
            <tbody>
              <template v-for="cliente in clientes">
                <tr>
                  <td align="center">@{{cliente.cidcliente}}</td>
                  <td align="center">@{{cliente.cnombrecliente}}</td>
                  <td align="center">@{{cliente.ctelefonocliente}}</td>
                  <td align="center" v-if="cliente.cestatuscliente==0"><small class="label label-danger">Inactivo</small></td>
                  <td align="center" v-if="cliente.cestatuscliente==1"><small class="label label-success">Activo</small></td>
                  <td align="center">
                    <div class="btn-group">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a href="#" @click="abrirModalVerCliente(cliente)"><span class="fa fa-eye"></span> Ver</a></li>
                          <li><a href="#" @click="abrirModalEditarCliente(cliente.cidcliente)"><span class="fa fa-pencil"></span> Editar</a></li>
                          <li><a href="#" @click="abrirModalEliminarCliente(cliente)"><span class="fa fa-trash"></span> Eliminar</a></li>
                        </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @include("Catalogos/clientes/ClientesAgregar")
  @include("Catalogos/clientes/clientesEditar")
  @include("Catalogos/clientes/clientesEliminar")
  @include("Catalogos/clientes/clientesVer")
</div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/clientes.js?ver=1.1.2"></script>
@endsection
