<form @submit.prevent="eliminarClientes">
  {{ csrf_field() }}
  <div class="modal fade" id="eliminarClientesModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click ="eliminarData">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" style="font-weight: bold;">Eliminar Cliente</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <label>¿Estas seguro de eliminar el cliente: <span style="font-style: italic;">@{{dataCliente.cnombrecliente}}</span> ?</label>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-danger">Guardar cambios</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
