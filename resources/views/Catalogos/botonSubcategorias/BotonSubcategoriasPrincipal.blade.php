@extends('Master')
@section('titulo','Botones subcategorías')
@section('content')
  <div id="botonesSubcategorias">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="abrirModal('AgregarBotonSubcategorias', 0)" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar botón categorías</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table id="table_botonsubcategorias" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center">No. folio</th>
              <th style="text-align:center">Descripción</th>
              <th style="text-align:center">Categoría</th>
              <th style="text-align:center">Acciones</th>
            </thead>
            <tbody>
              <template v-for="botonsubcategoria in BotonSubCategorias">
                <tr>
                  <td align="center">@{{botonsubcategoria.cBotSubCatFolio}}</td>
                  <td align="center">@{{botonsubcategoria.cBotSubCatDescripcion}}</td>
                  <td align="center" v-if="botonsubcategoria.bot_categoria!=null">@{{botonsubcategoria.bot_categoria.cBotCatDescripcion}}</td>
                  <td v-else></td>
                  <td align="center">
                    <div class="btn-group">
                      <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" @click="abrirModal('EditarBotonSubcategorias', botonsubcategoria)"><span class="fa fa-edit"></span> Editar</a></li>
                        <li><a href="#" @click="abrirModal('EliminarBotonSubcategorias', botonsubcategoria)"><span class="fa fa-times"></span> Eliminar</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
      @include('Catalogos.botonSubcategorias.BotonSubcategoriasAgregar')
      @include('Catalogos.botonSubcategorias.BotonSubcategoriasEditar')
      @include('Catalogos.botonSubcategorias.BotonSubcategoriasEliminar')
    </div>

  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/botonSubcategorias.js"></script>
@endsection
