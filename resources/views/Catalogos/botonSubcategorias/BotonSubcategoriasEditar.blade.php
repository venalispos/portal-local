<form @submit.prevent="editarBotonSubcategoria">
  {{ csrf_field() }}
  <div class="modal fade" id='EditarBotonSubcategorias' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('EditarBotonSubcategorias')">&times;</button>
          <h4 style="font-weight: bold;">Editar botón subcategorías</h4>
        </div>
        <div class="modal-body">
          <div class="box box-warning">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Descripción</label>
                    <input class="form-control" v-model="datosBotonSubCategorias.cBotSubCatDescripcion" @keyup="FormatearMayusculas" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Categoría</label>
                    <select class="form-control" v-model="datosBotonSubCategorias.cBotSubCatIdPadre" required>
                      <option v-for="botoncategorias in BotonCategorias" :value="botoncategorias.cBotCatFolio">@{{botoncategorias.cBotCatDescripcion}}</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-warning">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
