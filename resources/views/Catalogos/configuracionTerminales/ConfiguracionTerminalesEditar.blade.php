<form @submit.prevent="editarConfigTerminal">
  {{ csrf_field() }}
  <div class="modal fade" id='EditarConfigTerminal' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('EditarConfigTerminal')">&times;</button>
          <h4 style="font-weight: bold;">Editar configuración terminal No. </h4>
        </div>
        <div class="modal-body">
          <div class="box box-warning">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Serie</label>
                    <input class="form-control" v-model="datosTerminales.cTerSerie" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Impresora Barra</label>
                    <input class="form-control" v-model="datosTerminales.cTerImpBarra" @keyup="FormatearBMayusculas" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Impresora Tickets</label>
                    <input class="form-control" v-model="datosTerminales.cTerImpTickets" @keyup="FormatearTMayusculas" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">ID TeamViewer</label>
                    <input class="form-control" v-model="datosTerminales.cTerIdTeamViewer" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Impresora Cocina</label>
                    <input class="form-control" v-model="datosTerminales.cTerImpCocina" @keyup="FormatearCMayusculas" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Liquida</label>
                    <select class="form-control" v-model="datosTerminales.cTerLiquida" required>
                      <option value="0">NO</option>
                      <option value="1">SI</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-warning">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
