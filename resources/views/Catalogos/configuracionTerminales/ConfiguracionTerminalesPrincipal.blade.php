@extends('Master')
@section('titulo','Configuración Terminales')
@section('content')
  <div id="configuracionTerminales">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="abrirModal('AgregarConfigTerminal', 0)" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar configuración terminales</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table id="table_configterminales" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center">Serie</th>
              <th style="text-align:center">ID Teamviewer</th>
              <th style="text-align:center">Impresora Barra</th>
              <th style="text-align:center">Impresora Cocina</th>
              <th style="text-align:center">Impresora Tickets</th>
              <th style="text-align:center">Liquida</th>
              <th style="text-align:center">Acciones</th>
            </thead>
            <tbody>
              <template v-for="terminal in Terminales">
                <tr>
                  <td align="center">@{{terminal.cTerSerie}}</td>
                  <td align="center">@{{terminal.cTerIdTeamViewer}}</td>
                  <td align="center">@{{terminal.cTerImpBarra}}</td>
                  <td align="center">@{{terminal.cTerImpCocina}}</td>
                  <td align="center">@{{terminal.cTerImpTickets}}</td>
                  <td align="center" v-if="terminal.cTerLiquida==1"><small class="label label-success">Activo</small></td>
                  <td align="center" v-if="terminal.cTerLiquida==0"><small class="label label-danger">Inactivo</small></td>
                  <td align="center">
                    <div class="btn-group">
                      <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" @click="abrirModal('EditarConfigTerminal', terminal)"><span class="fa fa-edit"></span> Editar</a></li>
                        <li><a href="#" @click="abrirModal('EliminarConfigTerminal', terminal)"><span class="fa fa-times"></span> Eliminar</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('Catalogos.configuracionTerminales.ConfiguracionTerminalesAgregar')
    @include('Catalogos.configuracionTerminales.ConfiguracionTerminalesEditar')
    @include('Catalogos.configuracionTerminales.ConfiguracionTerminalesEliminar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/configuracionTerminales.js"></script>
@endsection
