<form @submit.prevent="editarProductoPrimario">
  {!!csrf_field()!!}
  <div class="modal fade" id='EditarProductoPrimario' data-backdrop="static" data-keyboard="false" aria-hidden="false" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModalEditarProdPrimario()">&times;</button>
          <h4 style="font-weight: bold;">Editar producto primario</h4>
        </div>
        <div class="modal-body">
          <div class="box box-warning">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Producto Primario</label>
                    <!--<select class="form-control" v-model="productoPrimario.cProPriFolio" required>
                      <option v-for="producto in listaProdPrimarios" :value="producto.cProPriFolio">@{{producto.cProPriDescripcion}}</option>
                    </select>-->
                    <input class="form-control" v-model="productoPrimario.cProPriDescripcion" required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Detalle Equivalencia</label>
                    <input class="form-control" v-model="productoPrimario.cProPriDetEquivalencia" required>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-warning">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
