<form @submit.prevent="agregarProductoPrimario">
  {!!csrf_field()!!}
  <div class="modal fade" id='AgregarProductoPrimario' data-backdrop="static" data-keyboard="false" aria-hidden="false" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal()">&times;</button>
          <h4 style="font-weight: bold;">Nuevo producto primario</h4>
        </div>
        <div class="modal-body">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Producto Primario</label>
                    <!-- <select class="form-control" v-model="nuevoProductoPrimario.cProPriCodigo" required>
                      <option selected disabled value = -1>Seleccione...</option>
                      <option v-for="producto in listaProductos" :value="producto.cProCodigo">@{{producto.cProDescripcion}}</option>
                    </select>
                    <multiselect
                    placeholder="Seleccionar producto ..."
                    v-model = 'seleccionarProducto'
                    :options = 'listaProductos'
                    label = 'cProPriCodigo'
                    :custom-label = 'getNombreProdPriFolio'>
                  </multiselect> -->
                  <input class="form-control" type="text" placeholder="ej: Arroz" v-model="productoPrimario.cProPriDescripcion">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Detalle Equivalencia</label>
                    <input class="form-control" type="text" placeholder="ej: Kilos" v-model="productoPrimario.cProPriDetEquivalencia" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-success">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
