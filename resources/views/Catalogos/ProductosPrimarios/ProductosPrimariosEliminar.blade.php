<form @submit.prevent="eliminarProductoPrimario">
  {!!csrf_field()!!}
  <div class="modal fade" id='EliminarProductoPrimario' data-backdrop="static" data-keyboard="false" aria-hidden="false" style="overflow-y: scroll;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModalEliminarProdPrimario()">&times;</button>
          <h4 style="font-weight: bold;">Eliminar producto primario</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label>¿Estás seguro de querer eliminar el producto primario: <span style="font-style: italic;">@{{productoPrimario.cProPriDescripcion}}</span> ?</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger">Eliminar</button>
        </div>
      </div>
    </div>
  </div>
</form>
