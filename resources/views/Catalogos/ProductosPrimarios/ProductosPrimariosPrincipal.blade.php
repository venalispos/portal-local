@extends('Master')
@section('titulo','Productos Primarios')
@section('styles')
    <link rel="stylesheet" href="/css/vue-multiselect.min.css">
@endsection
@section('content')
  <div id="ProductosPrimarios">
    <div class="row-margin">
      <div class="row">
        <div class="col-sm-12">
          <button type='button' @click ="abrirModal()" class = 'btn btn-success pull-right'>
            <span class = 'fa fa-plus'></span> Agregar producto primario
          </button>
        </div>
      </div>
      <br>
      <div class="box">
        <div class="box-body">
          <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
              <div class="col-sm-6">
                <div class="dataTables_length">
                  <label>Mostrar
                    <select class="btn btn-secondary btn-sm dropdown-toggle"  v-model="numeroRegistros" @change="obtenerDatos">
                      <option value='10'>10</option>
                      <option value='25'>25</option>
                      <option value='50'>50</option>
                      <option value='100'>100</option>
                    </select> registros</label>
                  </div>
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-striped dataTable">
                <thead>
                  <tr>
                    <th scope="col">Código</th>
                    <th scope="col">Producto Primario</th>
                    <th scope="col">Detalle Equivalencia</th>
                    <th scope="col">Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <template v-for='producto in listaProdPrimarios'>
                    <template v-if="producto">
                      <tr>
                        <td>@{{producto.cProPriFolio}}</td>
                        <td>@{{producto.cProPriDescripcion}}</td>
                        <td>@{{producto.cProPriDetEquivalencia}}</td>
                        <td align="center">
                          <div class="btn-group">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                              <li><a href="#" @click="abrirModalEditarProdPrimario(producto)"><span class="fa fa-edit"></span> Editar</a></li>
                              <li><a href="#" @click="abrirModalEliminarProdPrimario(producto)"><span class="fa fa-times"></span> Eliminar</a></li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                    </template>
    						  </template>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <div class="dataTables_info" id="table_productos_info" role="status" aria-live="polite">
                Mostrando registros del <span>@{{ pagination.from }}</span> al <span>@{{ pagination.to }}</span> de un total de <span>@{{ pagination.total }}</span> registros
              </div>
            </div>
            <div class="col-md-7">
              <nav aria-label="Page navigation example">
                <ul class="pagination pagination-sm pull-right">
                  <li class="page-item" v-if="pagination.current_page > 1">
                    <a href="" @click.prevent="changePage(pagination.current_page - 1)">
                      <span>Atrás</span>
                    </a>
                  </li>
                  <li class="page-item" v-for="page in pagesNumber" :class="[ page == isActived ? 'active' : '' ]">
                    <a href="" @click.prevent="changePage(page)">
                      <span>@{{ page }}</span>
                    </a>
                  </li>
                  <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                    <a href="" @click.prevent="changePage(pagination.current_page + 1)">
                      <span>Siguiente</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    @include('Catalogos.ProductosPrimarios.ProductosPrimariosAgregar')
    @include('Catalogos.ProductosPrimarios.ProductosPrimariosEditar')
    @include('Catalogos.ProductosPrimarios.ProductosPrimariosEliminar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/vue-multiselect.js"></script> {{-- Add-on para las busquedas en los select. --}}
<script src="/vuejs/catalogos/productosPrimarios.js"></script>
@endsection
