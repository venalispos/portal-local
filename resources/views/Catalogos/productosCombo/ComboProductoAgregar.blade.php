<form @submit.prevent="agregarProductoCombos">
  {{ csrf_field() }}
  <div class="modal fade" id='AgregarComProductos' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('AgregarComProductos')">&times;</button>
          <h4 style="font-weight: bold;">Nuevo botón combos</h4>
        </div>
        <div class="modal-body">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Producto</label>
                    <select class="form-control" v-model="dataComProductos.cComProdIdProd" required>
                      <option v-for="productos in Productos" :value="productos.cProCodigo">@{{productos.cProDescripcion}}</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Subcategoría</label>
                    <select class="form-control" v-model="dataComProductos.cComProdIdPadre" required>
                      <option v-for="subcategorias in ComSubCategorias" :value="subcategorias.cComSubCatFolio">@{{subcategorias.cComSubCatDescripcion}}</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-success">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
