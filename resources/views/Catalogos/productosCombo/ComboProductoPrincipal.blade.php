@extends('Master')
@section('titulo','Combo productos')
@section('content')
  <div id="botonesCombos">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="abrirModal('AgregarComProductos', 0)" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar producto combo</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table id="table_productoscombo" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center">No. folio</th>
              <th style="text-align:center">Producto</th>
              <th style="text-align:center">Subcategoría</th>
              <th style="text-align:center">Acciones</th>
            </thead>
            <tbody>
              <template v-for="comboprod in ComProductos">
                <tr>
                  <td align="center">@{{comboprod.cComProdFolio}}</td>
                  <td align="center" v-if="comboprod.producto!=null">@{{comboprod.producto.cProDescripcion}}</td>
                  <td align="center" v-else></td>
                  <td align="center" v-if="comboprod.sub_categoria!=null">@{{comboprod.sub_categoria.cComSubCatDescripcion}}</td>
                  <td align="center" v-else></td>
                  <td align="center">
                    <div class="btn-group">
                      <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" @click="abrirModal('EditarComProductos', comboprod)"><span class="fa fa-edit"></span> Editar</a></li>
                        <li><a href="#" @click="abrirModal('EliminarComProducto', comboprod)"><span class="fa fa-times"></span> Eliminar</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('Catalogos.productosCombo.ComboProductoAgregar')
    @include('Catalogos.productosCombo.ComboProductoEditar')
    @include('Catalogos.productosCombo.ComboProductoEliminar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/productosCombos.js"></script>
@endsection
