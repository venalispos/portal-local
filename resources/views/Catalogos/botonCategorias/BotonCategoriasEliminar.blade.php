<form @submit.prevent="eliminarBotonCategorias">
  {{ csrf_field() }}
  <div class="modal fade" id='EliminarBotonCategorias' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click ="">&times;</button>
          <h4 style="font-weight: bold;">Eliminar botón categoría</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label>¿Estás seguro de querer eliminar la categoría: <span style="font-style: italic;">@{{datosBotonCategorias.cBotCatDescripcion}}</span> ?</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
