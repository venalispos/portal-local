@extends('Master')
@section('titulo','Botones categorías')
@section('content')
  <div id="botonesCategorias">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="abrirModal('AgregarBotonCategoria', 0)" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar botón categorías</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table id="table_botoncategorias" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center">No. folio</th>
              <th style="text-align:center">Descripción</th>
              <th style="text-align:center">Acciones</th>
            </thead>
            <tbody>
              <template v-for="botoncategorias in BotonCategorias">
                <tr>
                  <td align="center">@{{botoncategorias.cBotCatFolio}}</td>
                  <td align="center">@{{botoncategorias.cBotCatDescripcion}}</td>
                  <td align="center">
                    <div class="btn-group">
                      <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" @click="abrirModal('EditarBotonCategoria', botoncategorias)"><span class="fa fa-edit"></span> Editar</a></li>
                        <li><a href="#" @click="abrirModal('EliminarBotonCategorias', botoncategorias)"><span class="fa fa-times"></span> Eliminar</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('Catalogos.botonCategorias.BotonCategoriasAgregar')
    @include('Catalogos.botonCategorias.BotonCategoriasEditar')
    @include('Catalogos.botonCategorias.BotonCategoriasEliminar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/botonCategorias.js"></script>
@endsection
