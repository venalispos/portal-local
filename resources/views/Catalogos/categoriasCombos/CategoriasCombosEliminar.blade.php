<form @submit.prevent="eliminarCatCombo">
  {{ csrf_field() }}
  <div class="modal fade" id='EliminarCatCombo' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('EliminarCatCombo')">&times;</button>
          <h4 style="font-weight: bold;">Eliminar categoría combo</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label>¿Estás seguro de querer eliminar la categoría combo: <span style="font-style: italic;">@{{DatosCatCombo.cComCatDesc}}</span> ?</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
