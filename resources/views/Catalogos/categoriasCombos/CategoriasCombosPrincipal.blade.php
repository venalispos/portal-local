@extends('Master')
@section('titulo','Catálogo categorías combos')
@section('content')
  <div id="categoriasCombos">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="ModalAgregarComCategoria" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar categoría combo</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table id="table_categoriasCombos" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center"># ID</th>
              <th style="text-align:center">Descripción</th>
              <th style="text-align:center">Acciones</th>
            </thead>
            <tbody>
              <template v-for="catcombos in CategoriasComb">
                <tr>
                  <td align="center">@{{catcombos.cComCatFolio}}</td>
                  <td align="center">@{{catcombos.cComCatDesc}}</td>
                  <td align="center">
                    <div class="btn-group">
                      <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" @click="ModalEditarComCategoria(catcombos)"><span class="fa fa-edit"></span> Editar</a></li>
                        <li><a href="#" @click="ModalEliminarComCategoria(catcombos)"><span class="fa fa-times"></span> Eliminar</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('Catalogos.categoriasCombos.CategoriasCombosAgregar')
    @include('Catalogos.categoriasCombos.CategoriasCombosEditar')
    @include('Catalogos.categoriasCombos.CategoriasCombosEliminar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/categoriasCombos.js"></script>
@endsection
