<form @submit.prevent="editarConImpuestos">
  {{ csrf_field() }}
  <div class="modal fade" id='AgregarConImpuestos' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('AgregarConImpuestos')">&times;</button>
          <h4 style="font-weight: bold;">Editar configuración impuestos</h4>
        </div>
        <div class="modal-body">
          <div class="box box-warning">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h3 class="box-title" style="font-weight:bold; font-size: 18px;text-align: left;">Impuesto No.1</h3><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Estatus</label>
                    <select class="form-control" v-model="DatosConfigImpuestos.cConImpuesto1Activo">
                      <option value="0">Inactivo</option>
                      <option value="1">Activo</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label class="control-label">Cantidad</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-percent"></i>
                      </div>
                      <input class="form-control" v-model="DatosConfigImpuestos.cConImpuesto1Cantidad" required>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Nombre</label>
                    <input type="text" class="form-control" v-model="DatosConfigImpuestos.cConImpuesto1Nombre" @keyup="FormatearMayusculas1">
                  </div>
                  <div class="form-group">
                    <label class="control-label">Tipo</label>
                    <select class="form-control" v-model="DatosConfigImpuestos.cConImpuesto1Tipo">
                      <option value="0">Ninguno</option>
                      <option value="1">Impuestos incluidos en precio</option>
                      <option value="2">Impuestos No incluidos en precio</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <h3 class="box-title" style="font-weight:bold; font-size: 18px;text-align: left;">Impuesto No.2</h3><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Estatus</label>
                    <select class="form-control" v-model="DatosConfigImpuestos.cConImpuesto2Activo">
                      <option value="0">Inactivo</option>
                      <option value="1">Activo</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Cantidad</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-percent"></i>
                      </div>
                      <input type="number" class="form-control" v-model="DatosConfigImpuestos.cConImpuesto2Cantidad">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Nombre</label>
                    <input type="text" class="form-control" v-model="DatosConfigImpuestos.cConImpuesto2Nombre" @keyup="FormatearMayusculas2">
                  </div>
                  <div class="form-group">
                    <label class="control-label">Tipo</label>
                    <select class="form-control" v-model="DatosConfigImpuestos.cConImpuesto2Tipo">
                      <option value="0">Ninguno</option>
                      <option value="1">Impuestos incluidos en precio</option>
                      <option value="2">Impuestos No incluidos en precio</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <h3 class="box-title" style="font-weight:bold; font-size: 18px;text-align: left;">Impuesto No.3</h3><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Estatus</label>
                    <select class="form-control" v-model="DatosConfigImpuestos.cConImpuesto3Activo">
                      <option value="0">Inactivo</option>
                      <option value="1">Activo</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Cantidad</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-percent"></i>
                      </div>
                    <input type="number" class="form-control" v-model="DatosConfigImpuestos.cConImpuesto3Cantidad">
                  </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Nombre</label>
                    <input type="text" class="form-control" v-model="DatosConfigImpuestos.cConImpuesto3Nombre" @keyup="FormatearMayusculas3">
                  </div>
                  <div class="form-group">
                    <label class="control-label">Tipo</label>
                    <select class="form-control" v-model="DatosConfigImpuestos.cConImpuesto3Tipo">
                      <option value="0">Ninguno</option>
                      <option value="1">Impuestos incluidos en precio</option>
                      <option value="2">Impuestos No incluidos en precio</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <h3 class="box-title" style="font-weight:bold; font-size: 18px;text-align: left;">Impuesto No.4</h3><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Estatus</label>
                    <select class="form-control" v-model="DatosConfigImpuestos.cConImpuesto4Activo">
                      <option value="0">Inactivo</option>
                      <option value="1">Activo</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Cantidad</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-percent"></i>
                      </div>
                    <input type="number" class="form-control" v-model="DatosConfigImpuestos.cConImpuesto4Cantidad">
                  </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Nombre</label>
                    <input type="text" class="form-control" v-model="DatosConfigImpuestos.cConImpuesto4Nombre" @keyup="FormatearMayusculas4">
                  </div>
                  <div class="form-group">
                    <label class="control-label">Tipo</label>
                    <select class="form-control" v-model="DatosConfigImpuestos.cConImpuesto4Tipo">
                      <option value="0">Ninguno</option>
                      <option value="1">Impuestos incluidos en precio</option>
                      <option value="2">Impuestos No incluidos en precio</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-warning">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
