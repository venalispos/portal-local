@extends('Master')
@section('titulo','Configuración Impuestos')
@section('content')
  <div id="configuracionImpuestos">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="abrirModal('AgregarConImpuestos')" class = 'btn btn-warning pull-right'><span class = 'fa fa-pencil'></span> Editar configuración impuestos</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center;">No. Impuesto</th>
              <th style="text-align:center">Nombre</th>
              <th style="text-align:center">Cantidad</th>
              <th style="text-align:center">Tipo</th>
              <th style="text-align:center">Estatus</th>
            </thead>
            <tbody>
              <tr>
                <td align="center" style="font-weight:bold;">1</td>
                <td align="center">@{{DatosConfigImpuestos.cConImpuesto1Nombre}}</td>
                <td align="center">% @{{ DatosConfigImpuestos.cConImpuesto1Cantidad}}</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto1Tipo==0"></td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto1Tipo==1">Impuestos incluidos en precio</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto1Tipo==2">Impuestos No incluidos en precio</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto1Activo==1"><small class="label label-success">Activo</small></td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto1Activo==0"><small class="label label-danger">Inactivo</small></td>
              </tr>
              <tr>
                <td align="center" style="font-weight:bold;">2</td>
                <td align="center">@{{DatosConfigImpuestos.cConImpuesto2Nombre}}</td>
                <td align="center">% @{{ DatosConfigImpuestos.cConImpuesto2Cantidad}}</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto2Tipo==0"></td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto2Tipo==1">Impuestos incluidos en precio</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto2Tipo==2">Impuestos No incluidos en precio</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto2Activo==1"><small class="label label-success">Activo</small></td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto2Activo==0"><small class="label label-danger">Inactivo</small></td>
              </tr>
              <tr>
                <td align="center" style="font-weight:bold;">3</td>
                <td align="center">@{{DatosConfigImpuestos.cConImpuesto3Nombre}}</td>
                <td align="center">% @{{ DatosConfigImpuestos.cConImpuesto3Cantidad}}</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto3Tipo==0"></td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto3Tipo==1">Impuestos incluidos en precio</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto3Tipo==2">Impuestos No incluidos en precio</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto3Activo==1"><small class="label label-success">Activo</small></td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto3Activo==0"><small class="label label-danger">Inactivo</small></td>
              </tr>
              <tr>
                <td align="center" style="font-weight:bold;">4</td>
                <td align="center">@{{DatosConfigImpuestos.cConImpuesto4Nombre}}</td>
                <td align="center">% @{{ DatosConfigImpuestos.cConImpuesto4Cantidad}}</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto4Tipo==0"></td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto4Tipo==1">Impuestos incluidos en precio</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto4Tipo==2">Impuestos No incluidos en precio</td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto4Activo==1"><small class="label label-success">Activo</small></td>
                <td align="center" v-if="DatosConfigImpuestos.cConImpuesto4Activo==0"><small class="label label-danger">Inactivo</small></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('Catalogos.configuracionImpuestos.ConfiguracionImpuestosAgregar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/configuracionImpuestos.js"></script>
@endsection
