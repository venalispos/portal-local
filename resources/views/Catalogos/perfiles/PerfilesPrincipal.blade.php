@extends('Master')
@section('titulo','Catálogo perfiles')
@section('content')
  <div id="catalogoPerfiles">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="abrirModal('AgregarPerfil', 0)" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar perfil</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table id="table_catalogoperfiles" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center">No. folio</th>
              <th style="text-align:center">Descripción</th>
              <th style="text-align:center">Acciones</th>
            </thead>
            <tbody>
              <template v-for="perfil in Perfiles">
                <tr>
                  <td align="center">@{{perfil.cPerFolio}}</td>
                  <td align="center">@{{perfil.cPerDesc}}</td>
                  <td align="center">
                    <div class="btn-group">
                      <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" @click="abrirModal('VerPerfiles', perfil)"><span class="fa fa-eye"></span> Ver</a></li>
                        <li><a href="#" @click="abrirModal('EditarPerfil', perfil)"><span class="fa fa-edit"></span> Editar</a></li>
                        <li><a href="#" @click="abrirModal('EliminarPerfil', perfil)"><span class="fa fa-times"></span> Eliminar</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include("Catalogos/perfiles/PerfilesVer")
    @include("Catalogos/perfiles/PerfilesAgregar")
    @include("Catalogos/perfiles/PerfilesEditar")
    @include("Catalogos/perfiles/PerfilesEliminar")
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/Perfiles.js"></script>
@endsection
