<form @submit.prevent="agregarPerfil">
  {!! csrf_field() !!}
  <div class="modal fade" id='AgregarPerfil' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModalN('AgregarPerfil')">&times;</button>
          <h4 style="font-weight: bold;">Nuevo perfil</h4>
        </div>
        <div class="modal-body">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Descripción</label>
                    <input class="form-control" v-model="datosPerfil.cPerDesc" @keyup="FormatearDescMayusculas" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-check">
                    <label class="form-check-label">Clave de acceso</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAccesoClave" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Liquida</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerLiquida" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Recibe cuentas</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerRecibeCuentas" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autorización extra</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutExtra" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autorización transferencia</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutTransfer" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autorización Retiros</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutRetiros" style="float:right;">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-check">
                    <label class="form-check-label">Reimpresión ticket</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerReImpTicket" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autoriza salida</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutorizaSalida" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Servicio</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerServicio" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autorización dividir</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutDividir" style="float:right;">
                  </div>
                </div>
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Acciones</h4><br>
                </div>
                <div class="col-md-12">
                  <template v-for="acciones in Acciones">
                    <div class="col-md-6">
                      <label class="form-check-label">@{{acciones.cAccDes}}</label>
                      <input class="form-check-input" type="checkbox" v-model='AccionesSeleccionadas' :value="acciones.cAccId" style="float:right;">
                    </div>
                  </template>
                </div>
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Inventario</h4><br>
                </div>
                <div class="col-md-12">

                    <div class="col-md-6">
                      <div class="form-group">
                          <label class="form-check-label">AGREGAR </label>
                        <input type="checkbox" v-model='InventarioPermisos' :value="1" style="float:right;">
                      </div>
                      </div>
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-check-label">MODIFICAR </label>
                        <input type="checkbox" v-model='InventarioPermisos' :value="2" style="float:right;">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                      <label class="form-check-label">ELIMINAR </label>
                        <input type="checkbox" v-model='InventarioPermisos' :value="3" style="float:right;">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-check">
                      <label class="form-check-label">REPORTES </label>
                        <input type="checkbox" class="form-check-input"  v-model='InventarioPermisos' :value="4" style="float:right;">
                      </div>
                    </div>
                </div>
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Accesos del portal</h4><br>
                </div>
                <div class="col-md-12">
                  <template v-if='menu'>
                      <template v-for='men in menu'>
                          <div class="col-md-6">
                              <div class="form-check">
                              <label ><template v-if='men.cMenFolPadre == 0'> (Master)</template> - @{{men.cMenDescripcion}} <small> <template v-if='men.cMenFolPadre == 0'> (Master)</template>  </small> </label>
                                <input type="checkbox" class="form-check-input"  v-model='permisosPortalSeleccionados' :value="men.cMenFolio" style="float:right;">
                              </div>
                          </div>
                      </template>
                  </template>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
