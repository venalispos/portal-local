<form @submit.prevent="editarPerfil">
  {!! csrf_field()!!}
  <div class="modal fade" id='EditarPerfil' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('EditarPerfil')">&times;</button>
          <h4 style="font-weight: bold;">Editar perfil</h4>
        </div>
        <div class="modal-body">
          <div class="box box-warning">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label">Descripción</label>
                    <input class="form-control" v-model="datosPerfil.cPerDesc" @keyup="FormatearDescMayusculas" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-check">
                    <label class="form-check-label">Clave de acceso</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAccesoClave" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Liquida</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerLiquida" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Recibe cuentas</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerRecibeCuentas" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autorización extra</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutExtra" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autorización transferencia</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutTransfer" style="float:right;">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-check">
                    <label class="form-check-label">Reimpresión ticket</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerReImpTicket" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autoriza salida</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutorizaSalida" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Servicio</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerServicio" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autorización dividir</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutDividir" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">Autorización Retiros</label>
                    <input class="form-check-input" type="checkbox" v-model="datosPerfil.cPerAutRetiros" style="float:right;">
                  </div>
                </div>
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Acciones</h4><br>
                </div>
                <div class="col-md-12">
                  <template v-for="acciones in datosPerfil.Permisos">
                    <div class="col-md-6">
                      <label class="form-check-label">@{{acciones.descripcion}}</label>
                      <input class="form-check-input" type="checkbox" :value='acciones.Codigo' v-model="AccionesSeleccionadas"  style="float:right;">
                    </div>
                  </template>
                </div>
                <div class="col-md-12">
                    <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;"> Accesos en el portal</h4><br>
                </div>
                <template v-if='menu' v-for='menue in menu'>
                  <div class="col-md-6">
                    <div class="form-check">
                      <label class="form-check-label">@{{menue.cMenDescripcion}}</label>
                      <input class="form-check-input" type="checkbox" :value='menue.cMenFolio' v-model="permisosPortalSeleccionados" style="float:right;">
                    </div>
                  </div>
                </template>
                <div class="col-md-12">
                    <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;"> Inventario</h4><br>
                </div>
                <template v-if='datosInventario' v-for='inventario in datosInventario'>
                  <div class="col-md-6">
                    <div class="form-check">
                      <label class="form-check-label">@{{inventario.descripcion}}</label>
                      <input class="form-check-input" type="checkbox" :value='inventario.valor' v-model="InventarioPermisos" style="float:right;">
                    </div>
                  </div>
                </template>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-warning">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
