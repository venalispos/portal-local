<div class="modal fade" id='VerPerfiles' data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <template v-if="datosPerfil!=null">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('VerPerfiles')">&times;</button>
           <h4 style="font-weight: bold;">Detalle de perfil: <span style="font-style: italic;">@{{datosPerfil.cPerDesc}}</span></h4>
        </div>
        <div class="modal-body">
          <div class="box box-primary">
            <div class="box-body">
              <div class="row">
                <table class="table table-striped">
                  <thead>
                     <th style="text-align:center; font-size: 18px;" colspan="4">Información general</th>
                   </thead>
                   <tbody>
                     <tr>
                       <th>Clave acceso</th>
                       <td v-if="datosPerfil.cPerAccesoClave==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerAccesoClave==0"><small class="label label-danger">Inactivo</small></td>
                       <th>Reimpresión ticket</th>
                       <td v-if="datosPerfil.cPerReImpTicket==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerReImpTicket==0"><small class="label label-danger">Inactivo</small></td>
                     </tr>
                     <tr>
                       <th>Liquida</th>
                       <td v-if="datosPerfil.cPerLiquida==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerLiquida==0"><small class="label label-danger">Inactivo</small></td>
                       <th>Autoriza salida</th>
                       <td v-if="datosPerfil.cPerAutorizaSalida==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerAutorizaSalida==0"><small class="label label-danger">Inactivo</small></td>
                     </tr>
                     <tr>
                       <th>Recibe cuentas</th>
                       <td v-if="datosPerfil.cPerRecibeCuentas==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerRecibeCuentas==0"><small class="label label-danger">Inactivo</small></td>
                       <th>Servicio</th>
                       <td v-if="datosPerfil.cPerServicio==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerServicio==0"><small class="label label-danger">Inactivo</small></td>
                     </tr>
                     <tr>
                       <th>Autorización extra</th>
                       <td v-if="datosPerfil.cPerAutExtra==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerAutExtra==0"><small class="label label-danger">Inactivo</small></td>
                       <th>Autorización dividir</th>
                       <td v-if="datosPerfil.cPerAutDividir==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerAutDividir==0"><small class="label label-danger">Inactivo</small></td>
                     </tr>
                     <tr>
                       <th>Autorización transferir</th>
                       <td v-if="datosPerfil.cPerAutTransfer==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerAutTransfer==0"><small class="label label-danger">Inactivo</small></td>
                       <th>Autorización Retiros</th>
                       <td v-if="datosPerfil.cPerAutRetiros==1"><small class="label label-success">Activo</small></td>
                       <td v-if="datosPerfil.cPerAutRetiros==0"><small class="label label-danger">Inactivo</small></td>
                       <th></th>
                       <td></td>
                     </tr>
                     <tr>
                       <th style="text-align:center; font-size: 18px;" colspan="4">Lista de acciones</th>
                     </tr>
                     <template v-for="permisos in datosPerfil.Permisos">
                       <tr>
                         <td align="center" v-if="permisos.activo==0"><small class="label label-success">✓</small></td>
                         <td align="center" v-else><small class="label label-danger">X</small></td>
                         <th colspan="3">@{{permisos.descripcion}}</th>
                       </tr>
                     </template>
                     <tr>

                     </tr>
                     <tr>
                       <th style="text-align:center; font-size: 18px;" colspan="4">Lista de Accesos en el portal</th>
                     </tr>
                     <template v-for="menue in menu">
                       <tr>
                         <template v-for='permiso in permisosPortalSeleccionados'>
                           <template v-if='menue.cMenFolio == permiso'>
                            @{{FunCambiarActivo(menue)}}
                           </template>
                         </template>
                         <td align="center" v-if="menue.activo==1"><small class="label label-success">✓</small></td>
                         <td align="center" v-else><small class="label label-danger">X</small></td>
                         <th colspan="3">@{{menue.cMenDescripcion}}</th>
                       </tr>
                     </template>
                     <tr>

                     </tr>
                     <tr>

                     </tr>
                     <tr>
                       <th style="text-align:center; font-size: 18px;" colspan="4">Inventario</th>
                     </tr>
                    <template v-for='inventario in datosInventario'>
                      <tr>
                        <template v-for='invPermiso in InventarioPermisos'>
                          <template v-if='inventario.valor == invPermiso'>
                           @{{FunCambiarActivo(inventario)}}
                          </template>
                        </template>
                        <td align="center" v-if="inventario.activo==1"><small class="label label-success">✓</small></td>
                        <td align="center" v-else><small class="label label-danger">X</small></td>
                          <th colspan="3">@{{inventario.descripcion}}</th>
                      </tr>
                    </template>
                   </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </template>
  </div>
</div>
