<form @submit.prevent="eliminarPerfiles">
  {!! csrf_field() !!}
  <div class="modal fade" id='EliminarPerfil' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('EliminarPerfil')">&times;</button>
          <h4 style="font-weight: bold;">Eliminar perfil</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label>¿Estás seguro de querer eliminar el perfil: <span style="font-style: italic;">@{{datosPerfil.cPerDesc}}</span> ?</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
