<form @submit.prevent="editarConNube">
  {{ csrf_field() }}
  <div class="modal fade" id='EditarConfigNube' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('EditarConfigNube')">&times;</button>
          <h4 style="font-weight: bold;">Editar configuración nube</h4>
        </div>
        <div class="modal-body">
          <div class="box box-warning">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-12">
                  <div  class="form-group">
                    <label class="control-label">Sincronización</label>
                    <select class="form-control" v-model="DatosConfigNube.cBDSincroActiva" required>
                      <option value="0">INACTIVO</option>
                      <option value="1">ACTIVO</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">App pedidos</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDAppPedidos" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Bitácora</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDBitacora" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Puestos</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDPuestos" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Registrar facturas</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDRegistrarFacturas" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Contenido mesas abiertas</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDContenidoMesasAbiertasNube" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Productos</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDProductosNube" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Admin Total</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDAdminTotal" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Empleados</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDEmpleados" style="float:right;">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">App checkit</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDAppCheckIt" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Indicadores</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDIndicadores" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Productos Local</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDProductosNubeToLocal" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Mesas abiertas</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDMesasAbiertasNube" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Mesas</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDMesasNube" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Registro de asistencia</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDRegAsistencia" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Sincronizar corte</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cBDSincronizarCorte" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Querys</label>
                    <input class="form-check-input" type="checkbox" value="1" v-model="DatosConfigNube.cDBQUerys" style="float:right;">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-warning">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
