<form @submit.prevent="agregarConfigNube">
  {{ csrf_field() }}
  <div class="modal fade" id='AgregarConfigNube' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('AgregarConfigNube')">&times;</button>
          <h4 style="font-weight: bold;">Nueva configuración nube</h4>
        </div>
        <div class="modal-body">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Host</label>
                    <input class="form-control" v-model="DatosConfigNube.cBDHost" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Sucursal</label>
                    <input class="form-control" v-model="DatosConfigNube.cIDTienda" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Nombre Base de Datos</label>
                    <input class="form-control" v-model="DatosConfigNube.cBDName" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Centro de Costos Admin Total</label>
                    <input class="form-control" v-model="DatosConfigNube.cDbAdminTotalAlmacen" required>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">App pedidos</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDAppPedidos" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Bitácora</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDBitacora" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Puestos</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDPuestos" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Registrar facturas</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDRegistrarFacturas" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Contenido mesas abiertas</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDContenidoMesasAbiertasNube" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Productos</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDProductosNube" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Admin Total</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDAdminTotal" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Empleados</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDEmpleados" style="float:right;">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Usuario</label>
                    <input class="form-control" v-model="DatosConfigNube.cBDUser" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Contraseña</label>
                    <input class="form-control" v-model="DatosConfigNube.cBDPass" required>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Host Admin Total</label>
                    <input class="form-control" v-model="DatosConfigNube.cDBHostAdminTotal" required>
                  </div>
                  <div  class="form-group">
                    <label class="control-label">Sincronización</label>
                    <select class="form-control" v-model="DatosConfigNube.cBDSincroActiva" required>
                      <option value="0">INACTIVO</option>
                      <option value="1">ACTIVO</option>
                    </select>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">App checkit</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDAppCheckIt" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Indicadores</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDIndicadores" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Productos Local</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDProductosNubeToLocal" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Mesas abiertas</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDMesasAbiertasNube" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Mesas</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDMesasNube" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Registro de asistencia</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDRegAsistencia" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Sincronizar corte</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cBDSincronizarCorte" style="float:right;">
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">Querys</label>
                    <input class="form-check-input" type="checkbox" v-model="DatosConfigNube.cDBQUerys" style="float:right;">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
