@extends('Master')
@section('titulo','Catálogo configuración nube')
@section('content')
<div id="configuracionNube">
  <div class="row margin">
    <div class="row">
      <template v-if="DatosConfigNube==null">
        <div class="col-md-12">
          <button type='button' @click ="abrirModal('AgregarConfigNube')" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar configuración nube</button>
        </div>
      </template>
      <template v-else>
        <div class="col-md-12">
          <button type='button' @click ="abrirModal('EditarConfigNube')" class = 'btn btn-warning pull-right'><span class = 'fa fa-pencil'></span> Editar configuración nube</button>
        </div>
      </template>
      <div class="col-md-12">
        <hr>
      </div>
      <template  v-if="DatosConfigNube!=null">
        <div class="col-md-12 table-responsive">
            <table class="table table-bordered table-striped">
              <tbody>
                <tr>
                  <th>Sincronización</th>
                  <td align="center" v-if="DatosConfigNube.cBDSincroActiva==1"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDSincroActiva==0"><span class="label label-danger">Inactivo</span></td>
                  <th>App pedidos</th>
                  <td align="center" v-if="DatosConfigNube.cBDAppPedidos==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDAppPedidos==false"><span class="label label-danger">Inactivo</span></td>
                </tr>
                <tr>
                  <th>App checkit</th>
                  <td align="center" v-if="DatosConfigNube.cBDAppCheckIt==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDAppCheckIt==false"><span class="label label-danger">Inactivo</span></h4></td>
                  <th>Bitácora</th>
                  <td align="center" v-if="DatosConfigNube.cBDBitacora==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDBitacora==false"><span class="label label-danger">Inactivo</span></td>
                </tr>
                <tr>
                  <th>Indicadores</th>
                  <td align="center" v-if="DatosConfigNube.cBDIndicadores==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDIndicadores==false"><span class="label label-danger">Inactivo</span></td>
                  <th>Puestos</th>
                  <td align="center" v-if="DatosConfigNube.cBDPuestos==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDPuestos==false"><span class="label label-danger">Inactivo</span></td>
                </tr>
                <tr>
                  <th>Productos Local</th>
                  <td align="center" v-if="DatosConfigNube.cBDProductosNubeToLocal==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDProductosNubeToLocal==false"><span class="label label-danger">Inactivo</span></td>
                  <th>Registrar facturas</th>
                  <td align="center" v-if="DatosConfigNube.cBDRegistrarFacturas==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDRegistrarFacturas==false"><span class="label label-danger">Inactivo</span></td>
                </tr>
                <tr>
                  <th>Mesas abiertas</th>
                  <td align="center" v-if="DatosConfigNube.cBDMesasAbiertasNube==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDMesasAbiertasNube==false"><span class="label label-danger">Inactivo</span></td>
                  <th>Contenido mesas abiertas</th>
                  <td align="center" v-if="DatosConfigNube.cBDContenidoMesasAbiertasNube==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDContenidoMesasAbiertasNube==false"><span class="label label-danger">Inactivo</span></td>
                </tr>
                <tr>
                  <th>Mesas</th>
                  <td align="center" v-if="DatosConfigNube.cBDMesasNube==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDMesasNube==false"><span class="label label-danger">Inactivo</span></td>
                  <th>Productos</th>
                  <td align="center" v-if="DatosConfigNube.cBDProductosNube==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDProductosNube==false"><span class="label label-danger">Inactivo</span></td>
                </tr>
                <tr>
                  <th>Registro asistencia</th>
                  <td align="center" v-if="DatosConfigNube.cBDRegAsistencia==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDRegAsistencia==false"><span class="label label-danger">Inactivo</span></td>
                  <th>Admin Total</th>
                  <td align="center" v-if="DatosConfigNube.cBDAdminTotal==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDAdminTotal==false"><span class="label label-danger">Inactivo</span></td>
                </tr>
                <tr>
                  <th>Sincronizar corte</th>
                  <td align="center" v-if="DatosConfigNube.cBDSincronizarCorte==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDSincronizarCorte==false"><span class="label label-danger">Inactivo</span></td>
                  <th>Empleados</th>
                  <td align="center" v-if="DatosConfigNube.cBDEmpleados==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cBDEmpleados==false"><span class="label label-danger">Inactivo</span></td>
                </tr>
                <tr>
                  <th>Querys</th>
                  <td align="center" v-if="DatosConfigNube.cDBQUerys==true"><span class="label label-success">Activo</span></td>
                  <td align="center" v-if="DatosConfigNube.cDBQUerys==false"><span class="label label-danger">Inactivo</span></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
        </div>
      </template>
    </div>
  </div>
  @include('Catalogos.configuracionNube.ConfiguracionNubeAgregar')
  @include('Catalogos.configuracionNube.ConfiguracionNubeEditar')
</div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/configuracionNube.js"></script>
@endsection
