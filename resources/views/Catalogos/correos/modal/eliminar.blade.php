
<form  @submit.prevent="FunEliminarCorreo()">
	{!! csrf_field() !!}
	<div class="modal fade" id="mEliminarCorreo" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
					<h4>ELIMINAR CORREO</h4>
				</div >
				<div class="modal-body">
          <template v-if='correoSelected'>
            <div class="form-group">
              <label for="">¿Estas Seguro de eliminar este correo ?</label>
              <small >@{{correoSelected.cConCorCorreo}}</small>
            </div>

          </template>

				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn btn-primary" type="submit" >Eliminar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
