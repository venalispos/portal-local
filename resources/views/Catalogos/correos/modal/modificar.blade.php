<form  @submit.prevent="FunModificarCorreos()">
	{!! csrf_field() !!}
	<div class="modal fade" id="mActualizarCorreo" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
					<h4>EDITAR CORREO</h4>
				</div >
				<div class="modal-body">
          <template v-if='correoSelected'>
            <div class="form-group">
              <label for=""> Correo Actual</label>
              <input type="text" disabled v-model='correoSelected.cConCorCorreo' class="form-control" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Nuevo Correo</label>
              <input type="email" required v-model='correoSelected.correo' class="form-control" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Seleccione un status</label>
              <select class="form-control" v-model='correoSelected.cConCorActivo'>
                  <option :value="0">INACTIVO</option>
                  <option :value="1">ACTIVO</option>
              </select>
            </div>
          </template>

				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn btn-success" type="submit" >Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
