<form  @submit.prevent="FunAgregarNuevoCorreoCerrarModal()">
	{!! csrf_field() !!}
	<div class="modal fade" id="mNuevoCorreo" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
					<h4>NUEVO CORREO</h4>
				</div >
				<div class="modal-body">
          <template v-if='nuevoCorreo'>
            <div class="form-group">
              <label for="">Ingrese el correo</label>
              <input type="email" required v-model='nuevoCorreo.correo' class="form-control" name="" value="">
            </div>
            <div class="form-group">
              <label for="">Seleccione un status</label>
              <select class="form-control" v-model='nuevoCorreo.estatus'>
                  <option :value="0">INACTIVO</option>
                  <option :value="1">ACTIVO</option>
              </select>
            </div>
          </template>

				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn btn-success" type="submit" >Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
