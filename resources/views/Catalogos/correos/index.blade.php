@extends('Master')
@section('titulo','Configuracion correos')

@section('content')
<div id="correos">
  <div class="row">
    <div class="col-lg-12">
      <button type="button" class="btn btn-primary btn pull-right" @click='FunModalNuevoCorreo()' name="button">  <i class="fa fa-add"></i> Nuevo Correo</button>
    </div>
  </div>
<div class="row">
  <div class="col-lg-12">
    <template v-if='dataCorreos'>
      <div class="table-responsive">
        <table class="table table-hover table-bordered">
          <thead>
            <th>Correo</th>
            <th>estatus</th>
            <th>opciones</th>
          </thead>
          <tbody>
            <template v-for='correo in dataCorreos'>
              <tr>
                  <td>@{{correo.cConCorCorreo}}</td>
                  <td v-if='correo.cConCorActivo == 0'>Desactivado</td>
                  <td v-if='correo.cConCorActivo == 1'>Activo</td>
                  <td><button type="button" class="btn btn-primary" @click='FunAbrirModalActualizar(correo)' name="button"> <i class="fa fa-edit"> </i> </button> |<button @click='FunModalEliminarCorreo(correo)' type="button" class="btn btn-danger" name="button"> <i class="fa fa-trash"> </i> </button>  </td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
    </template>
  </div>
</div>
@include('catalogos.correos.modal.agregar')
@include('catalogos.correos.modal.modificar')
@include('catalogos.correos.modal.eliminar')
</div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/correos.js"></script>
@endsection
