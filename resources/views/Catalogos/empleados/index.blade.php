﻿ @extends('Master')
 @section('titulo','Employees')
 @section('content')
 <style>
 /* Estilos de etiqueta*/


 th {
 	/*background-color: #0099CC;*/
 	border-bottom: 1px solid rgb(210, 220, 250);
 	color: black;
 }

 td {
 	color: rgba(100, 100, 100, 60);
 }


 /*Pseudo-clases*/

 tr:hover {
 	background-color: #EFE7EA;
 }

#btn {
/* Color del texo */

color: #0099CC;
/* Eliminar color de fondo */

background: transparent;
/* Grosor del borde, estilo de línea y color */

border: 2px solid #0099CC;
/* Añadir esquinas curvadas */

border-radius: 6px;
/* Poner texto en mayúsculas */

border: none;
      padding: 16px 32px;
      text-align: center;
      display: inline-block;
      font-size: 16px;
      margin: 4px 2px;
      -webkit-transition-duration: 0.4s; /* Safari */
      transition-duration: 0.4s;
      cursor: pointer;
      text-decoration: none;
      text-transform: uppercase;
}
/* Al poner el curso encima (hover) */

#btn1:hover {
      background-color: #008CBA;
      color: white;
 }
#eliminarButton:hover{
	color: orange;
}
</style>
<div id="catalogoempleados">
	<div class="row">
		<div class="col-lg-12">
			<button id="btn"  @click='mNuevoEMpleado()'>NEW <i class="fa fa-plus"></i></button>
			<div class="table-responsive">
			<table id="example" class="table table-striped table-bordered" style="width:100%">
					<thead>
						<tr>
							<th> ID</th>
							<th>NAME</th>
              <th>ROLE</th>
              <th>ACCESS</th>
              <th>STATUS</th>
							<th>ACCIONES</th>
						</tr>
					</thead>
					<tbody>
						<template v-for='usuario in usuarios'>
							<tr>
							<td>@{{usuario.cUsuCodigo}}</td>
							<td>@{{usuario.cUsuNombre}} @{{usuario.cUsuApellido}}</td>
              <td>@{{usuario.cUsuPuesto1Desc}}</td>
              <td>@{{usuario.perfil.cPerDesc}}</td>
              <td v-if="usuario.cUsuActivo==1"><small class="label label-success" >Active</small></td>
              <td v-if="usuario.cUsuActivo==0"><small class="label label-danger" > Inactive</small></td>
							<td ><button @click='mEditarUsuario(usuario)'  id='eliminarButton' class="btn btn-primary">EDIT</button></td>
						</tr>
						</template>

					</tbody>
				</table>
			</div>
		</div>
	</div>
	@include('Catalogos.empleados.modales.m_Editar')
	@include('Catalogos.empleados.modales.m_NuevoEmpleado')
	@include('Catalogos.empleados.modales.m_eliminarconfirmarempleado')
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/app-catalogoempleados.js"></script>
<script>
	function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;
}
</script>
@endsection
