﻿<form  @submit.prevent="agregarNuevoEmpleado()">
	{!! csrf_field() !!}
	<div class="modal fade" id="mNuevoUsuario" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" @click='reiniciarNuevoEmpleado()'>&times;</button>
					<h4>NEW USER</h4>
				</div >
				<div class="modal-body">
					<template  >
						<div class="form-group">
							<label for="">ID</label>
							<input type="text" required="" class="form-control" v-model='nuevoEmpleadoSel.cUsuCodigo'  required="">
						</div>
						<div class="form-group">
							<label for="">NAME</label>
							<input type="text" required="" class="form-control"  v-model='nuevoEmpleadoSel.cUsuNombre' required="">
						</div>
						<div class="form-group">
							<label for="">LAST NAME</label>
							<input type="text" required="" class="form-control"  v-model='nuevoEmpleadoSel.cUsuApellido' required="">
						</div>
						<div class="form-group">
							<label for="">ACTIVE POS</label>
							<select name="" id="" class="form-control"  v-model='nuevoEmpleadoSel.cUsuActivo' required="">
								<option value="0">NO</option>
								<option value="1">YES</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">PASSWORD POS</label>
							<input type="password" class="form-control" v-model='nuevoEmpleadoSel.cUsuClave' onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
							<small>THE PASSWORD IS ONLY NUMBER</small>
						</div>
						<div class="form-group">
							<label for="">ROLE 1</label>
							<select name="" id="" v-model='nuevoEmpleadoSel.cUsuPuesto1Perfil' class="form-control" required="">
								<template v-for='perfil in perfiles'>
									<option :value="perfil"> @{{perfil.cPerDesc}}</option>
								</template>
							</select>
						</div>
						<div class="form-group">
							<label for="">STATUS ROLE 1</label>
							<select name="" id="" class="form-control"  v-model='nuevoEmpleadoSel.cUsuPuesto1Activo' required="">
								<option value="0">INACTIVE</option>
								<option value="1">ACTIVE</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">ROLE 2</label>
							<select name="" id="" v-model='nuevoEmpleadoSel.cUsuPuesto2Perfil' class="form-control">
								<template v-for='perfil in perfiles'>
									<option :value="perfil"> @{{perfil.cPerDesc}}</option>
								</template>
							</select>
						</div>
						<div class="form-group">
							<label for="">STATUS ROLE 2</label>
							<select name="" id="" class="form-control"  v-model='nuevoEmpleadoSel.cUsuPuesto2Activo'>
								<option selected value="0">INACTIVE</option>
								<option value="1">ACTIVE</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">ROLE 3</label>
							<select name="" id="" v-model='nuevoEmpleadoSel.cUsuPuesto3Perfil' class="form-control">
								<template v-for='perfil in perfiles'>
									<option :value="perfil"> @{{perfil.cPerDesc}}</option>
								</template>
							</select>
						</div>
						<div class="form-group">
							<label for="">STATUS ROLE 3</label>
							<select name="" id="" class="form-control"  v-model='nuevoEmpleadoSel.cUsuPuesto3Activo'>
								<option selected value="0">INACTIVE</option>
								<option value="1">ACTIVE</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">DNI</label>
							<input type="text" class="form-control" v-model='nuevoEmpleadoSel.cUsuNumSeguro' required>
						</div>
                        <div class="form-group">
                            <label for="">BANK ACCOUNT NUMBER</label>
                            <input type="text" class="form-control" v-model='nuevoEmpleadoSel.cUsuCuentaBancaria' required>
                        </div>
                        <div class="form-group">
                            <label for="">ROUTING NUMBER</label>
                            <input type="text" class="form-control" v-model='nuevoEmpleadoSel.cUsuNumRuta' required>
                        </div>
						<div class="form-group">
							<label for="">ADDRESS</label>
							<input type="text" class="form-control" v-model='nuevoEmpleadoSel.cUsuDireccion' required="">
						</div>
						<div class="form-group">
							<label for="">PHONE</label>
							<input type="text" class="form-control" v-model='nuevoEmpleadoSel.cUsuTelefono' required="">
						</div>
						<div class="form-group">
							<label for="">BIRTHDAY</label>
							<input type="date" class="form-control" data-date-format="YYYY-MMMM-DD" v-model='nuevoEmpleadoSel.cUsuFechNac' required="">
						</div>
						<div class="form-group">
							<label for="">ACCEPT COVID</label>
							<select name="" class="form-control" id="" v-model='nuevoEmpleadoSel.cUsuAutoCovid' required="">
								<option :value="0">NO</option>
								<option :value="1">YES</option>
							</select>
						</div>
						 <div class="form-group">
							<label for=""> ACCESS TO PORTAL</label>
							<select name="" id="" class="form-control" v-model='nuevoEmpleadoSel.cUsuAccesoAdmin'>
								<option value="0">NO</option>
								<option value="1">YES</option>
							</select>
						</div>
					 	<div class="form-group">
							<label for="">PASSWORD RESET</label>
							<select name="" class="form-control" id="" v-model='nuevoEmpleadoSel.tUsuReiniciar' required="">
								<option :value="0">NO</option>
								<option :value="1">YES</option>
							</select>
						</div>

					</template>
				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn btn-success" type="submit" >SAVE</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
