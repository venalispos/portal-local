<form  @submit.prevent="eliminarEmpleados()">
	{!! csrf_field() !!}
	<div class="modal fade" id="Idempleadoeliminarmodal" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" @click='reiniciarNuevoEmpleado()'>&times;</button>

				</div >
				<div class="modal-body">
				<template v-if='usuarioSeleccionado'>
						<h4>¿Desea eliminar a <strong> @{{usuarioSeleccionado.cUsuNombre}} @{{usuarioSeleccionado.cUsuApellido}}</strong>?</h4>
				</template>
				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn btn-primary"  type="submit" >Eliminar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
