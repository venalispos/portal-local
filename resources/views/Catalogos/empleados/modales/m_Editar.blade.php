<form  @submit.prevent="actualizarUsuario()">
	{!! csrf_field() !!}
	<div class="modal fade" id="mEditarUsuario" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
					<h4>EDIT USER</h4>
				</div >
				<div class="modal-body">
					<template v-if='status == 0 && usuarioSeleccionado != null' >
						<div class="form-group">
							<label for="">ID</label>
							<input type="text" required="" class="form-control" v-model='usuarioSeleccionado.cUsuCodigo' >
						</div>
						<div class="form-group">
							<label for="">NAME</label>
							<input type="text" required="" class="form-control"  v-model='usuarioSeleccionado.cUsuNombre'>
						</div>
						<div class="form-group">
							<label for="">LAST NAME</label>
							<input type="text" required="" class="form-control"  v-model='usuarioSeleccionado.cUsuApellido'>
						</div>
						<div class="form-group">
							<label for="">ACTIVE POS</label>
							<select name="" id="" class="form-control"  v-model='usuarioSeleccionado.cUsuActivo'>
								<option value="0">NO</option>
								<option value="1">YES</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">PASSWORD POS</label>
							<input type="password" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" v-model='usuarioSeleccionado.cUsuClave'  >
							<small>THE PASSWORD IS ONLY NUMBER</small>
						</div>
						<div class="form-group">
							<label for="">ROLE 1</label>
							<input type="text" required="" class="form-control" v-model='usuarioSeleccionado.cUsuPuesto1Desc' >
						</div>
						<div class="form-group">
							<label for="">ROLE 1</label>
							<select name="" id="" v-model='usuarioSeleccionado.cUsuPuesto1Perfil' class="form-control">
								<template v-for='perfil in perfiles'>
									<option :value="perfil.cPerId"> @{{perfil.cPerDesc}}</option>
								</template>
							</select>
						</div>
						<div class="form-group">
							<label for="">STATUS ROLE 1</label>
							<select name="" id="" class="form-control"  v-model='usuarioSeleccionado.cUsuPuesto1Activo'>
								<option value="0">Inactivo</option>
								<option value="1">Activo</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">ROLE 2</label>
							<input type="text" class="form-control" v-model='usuarioSeleccionado.cUsuPuesto2Desc' >
						</div>
						<div class="form-group">
							<label for="">ROLE 2</label>
							<select name="" id="" v-model='usuarioSeleccionado.cUsuPuesto2Perfil' class="form-control">
								<template v-for='perfil in perfiles'>
									<option :value="perfil.cPerId"> @{{perfil.cPerDesc}}</option>
								</template>
							</select>
						</div>
						<div class="form-group">
							<label for="">STATUS ROLE 2</label>
							<select name="" id="" class="form-control"  v-model='usuarioSeleccionado.cUsuPuesto2Activo'>
								<option selected value="0">Inactivo</option>
								<option value="1">Activo</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">ROLE 3</label>
							<input type="text" class="form-control" v-model='usuarioSeleccionado.cUsuPuesto3Desc' >
						</div>
						<div class="form-group">
							<label for="">ROLE 3</label>
							<select name="" id="" v-model='usuarioSeleccionado.cUsuPuesto3Perfil' class="form-control">
								<template v-for='perfil in perfiles'>
									<option :value="perfil.cPerId"> @{{perfil.cPerDesc}}</option>
								</template>
							</select>
						</div>
						<div class="form-group">
							<label for="">STATUS ROLE 3</label>
							<select name="" id="" class="form-control"  v-model='usuarioSeleccionado.cUsuPuesto3Activo'>
								<option selected value="0">Inactivo</option>
								<option value="1">Activo</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">DNI</label>
							<input type="text" class="form-control" v-model='usuarioSeleccionado.cUsuNumSeguro' required>
						</div>
                        <div class="form-group">
                            <label for="">BANK ACCOUNT NUMBER</label>
                            <input type="text" class="form-control" v-model='usuarioSeleccionado.cUsuCuentaBancaria' required>
                        </div>
                        <div class="form-group">
                            <label for="">ROUTING NUMBER</label>
                            <input type="text" class="form-control" v-model='usuarioSeleccionado.cUsuNumRuta' required>
                        </div>
						<div class="form-group">
							<label for="">ADDRESS</label>
							<input type="text" class="form-control" v-model='usuarioSeleccionado.cUsuDireccion' required="">
						</div>
						<div class="form-group">
							<label for="">PHONE</label>
							<input type="text" class="form-control" v-model='usuarioSeleccionado.cUsuTelefono' required="">
						</div>
						<div class="form-group">
							<label for="">BIRTHDAY</label>
							<input type="date" class="form-control" data-date-format="YYYY-MMMM-DD" v-model='usuarioSeleccionado.cUsuFechNac'>
						</div>
						<div class="form-group">
							<label for="">COVID</label>
							<select name="" class="form-control" id="" v-model='usuarioSeleccionado.cUsuAutoCovid' required="">
								<option :value="0">NO</option>
								<option :value="1">YES</option>
							</select>
						</div>
						<div class="form-group">
							<label for=""> ACCESS TO PORAL</label>
							<select name="" id="" class="form-control" v-model='usuarioSeleccionado.cUsuAccesoAdmin'>
								<option value="0">NO</option>
								<option value="1">YES</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">PASSWORD RESET</label>
							<select name="" class="form-control" id="" v-model='usuarioSeleccionado.tUsuReiniciar'>
								<option :value="0">NO</option>
								<option :value="1">YES</option>
							</select>
						</div>

					</template>
				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button class="btn btn-success" type="submit" >SAVE</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
