<form @submit.prevent="editarSubcategoria">
  {{ csrf_field() }}
  <div class="modal fade" id='EditarSubcategoria' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" @click ="">&times;</button>
          <h4 style="font-weight: bold;">Editar subcategoría</h4>
        </div>
        <div class="modal-body">
          <div class="box box-warning">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Código</label>
                    <input type="number" class="form-control" v-model="DatosSubCategoria.cSCatCodigo" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <label class="control-label">Categoría</label>
                    <select class="form-control" v-model="DatosSubCategoria.cSCatPadre" required>
                      <option v-for="Categoria in Categorias" :value="Categoria.cCatCodigo">@{{Categoria.cCatDescripcion}}</option>
                    </select>
                </div>
                <div class="col-md-12">
                  <label class="control-label">Descripción</label>
                  <input class="form-control" v-model="DatosSubCategoria.cSCatDescripcion" @keyup="FormatearDescMayusculas" required>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-warning">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
