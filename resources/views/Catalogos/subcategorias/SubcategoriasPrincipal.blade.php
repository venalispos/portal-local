@extends('Master')
@section('titulo','Catálogo subcategorías')
@section('content')
  <div id="catalogoSubcategorias">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="ModalAgregarSubcategoria" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar subcategoría</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table id="table_subcategorias" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center">Código</th>
              <th style="text-align:center">Descripción</th>
              <th style="text-align:center">Categoría</th>
              <th style="text-align:center">Acciones</th>
            </thead>
            <tbody>
              <template v-for="subcategoria in Subcategorias">
                <tr>
                  <td align="center">@{{subcategoria.cSCatCodigo}}</td>
                  <td align="center">@{{subcategoria.cSCatDescripcion}}</td>
                  <td align="center">@{{subcategoria.categoria.cCatDescripcion}}</td>
                  <td align="center">
                    <div class="btn-group">
                      <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" @click="VerModalEditar(subcategoria)"><span class="fa fa-edit"></span> Editar</a></li>
                        <li><a href="#" @click="VerModalEliminar(subcategoria)"><span class="fa fa-times"></span> Eliminar</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('Catalogos.subcategorias.SubcategoriasAgregar')
    @include('Catalogos.subcategorias.SubcategoriasEditar')
    @include('Catalogos.subcategorias.SubcategoriasEliminar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/subcategorias.js"></script>
@endsection
