@extends('Master')
@section('titulo','Botones productos')
@section('content')
  <div id="botonesProducto">
    <div class="row margin">
      <div class="row">
        <div class="col-md-12">
          <button type='button' @click ="abrirModal('AgregarBotonProductos', 0)" class = 'btn btn-success pull-right'><span class = 'fa fa-plus'></span> Agregar botón productos</button>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="col-md-12 table-responsive">
          <table id="table_botonproductos" class="table table-bordered table-striped dataTable">
            <thead>
              <th style="text-align:center">No. folio</th>
              <th style="text-align:center">Producto</th>
              <th style="text-align:center">Subcategoría</th>
              <th style="text-align:center">Acciones</th>

            </thead>
            <tbody>
              <template v-for="botonproducto in BotonProductos">
                <tr>
                  <td align="center">@{{botonproducto.cBotProFolio}}</td>
                  <td align="center" v-if="botonproducto.producto!=null">@{{botonproducto.producto.cProDescripcion}}</td>
                  <td align="center" v-else></td>
                  <td align="center" v-if="botonproducto.sub_categoria!=null">@{{botonproducto.sub_categoria.cBotSubCatDescripcion}}</td>
                  <td align="center" v-else></td>
                  <td align="center">
                    <div class="btn-group">
                      <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" style="height:30px;"><span class="caret"></span></button>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#" @click="abrirModal('EditarBotonProductos', botonproducto)"><span class="fa fa-edit"></span> Editar</a></li>
                        <li><a href="#" @click="abrirModal('EliminarBotonProducto', botonproducto)"><span class="fa fa-times"></span> Eliminar</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('Catalogos.botonProductos.BotonProductosAgregar')
    @include('Catalogos.botonProductos.BotonProductoEditar')
    @include('Catalogos.botonProductos.BotonProductoEliminar')
  </div>
@endsection

@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/catalogos/botonProductos.js"></script>
@endsection
