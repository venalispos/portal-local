<form @submit.prevent="editarBotonProductos">
  {!!csrf_field()!!}
  <div class="modal fade" id='EditarBotonProductos' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('EditarBotonProductos')">&times;</button>
          <h4 style="font-weight: bold;">Editar botón productos</h4>
        </div>
        <div class="modal-body">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="box-title" style="font-weight:bold; font-size: 18px;text-align: center;">Información general</h4><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Producto</label>
                    <select class="form-control" v-model="datosBotonProducto.cBotProIdProd" required>
                      <option v-for="producto in Productos" :value="producto.cProCodigo">@{{producto.cProDescripcion}}</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Subcategoría</label>
                    <select class="form-control" v-model="datosBotonProducto.cBotProIdPadre" required>
                      <option v-for="subcategorias in BotSubCategorias" :value="subcategorias.cBotSubId">@{{subcategorias.cBotSubCatDescripcion}}</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
