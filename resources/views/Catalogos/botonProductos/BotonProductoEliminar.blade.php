<form @submit.prevent="eliminarBotonProducto">
  {{ csrf_field() }}
  <div class="modal fade" id='EliminarBotonProducto' data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" @click ="cerrarModal('EliminarBotonProducto')">&times;</button>
          <h4 style="font-weight: bold;">Eliminar botón producto</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <label v-if="datosBotonProducto.producto!=null">¿Estás seguro de querer eliminar el botón: <span style="font-style: italic;">@{{datosBotonProducto.producto.cProDescripcion}}</span> ??</label>
              <label v-else></label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>
