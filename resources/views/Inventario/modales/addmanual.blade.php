<form  @submit.prevent="">
	{!! csrf_field() !!}
	<div class="modal fade" id="modalAgregarManual" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
						PRODUCTO MANUAL
					</div >
					<div class="modal-body">
						<div class="panel panel-primary">
					<div class="panel-heading">
            SELECCIONE UN PRODUCTO
					</div>
					<div class="panel-body">
						<div class="form-group">
              <input type="text" v-model='producto_manual' name="" value="" class="form-control">
						</div>
					</div>
				</div>
					</div>
					<div class="modal-footer">
						<div class="pull-right">
							<button class="btn btn-primary" type="submit" >Guardar</button>
						</div>
					</div>
			</div>
		</div>
	</div>
</form>
