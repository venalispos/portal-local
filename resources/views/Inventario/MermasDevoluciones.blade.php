@extends('Master')
@section('titulo','Mermas y devoluciones')
@section('content')
<div id="Inventario">
<div class="row">
<template v-if='Insertar == 1'>
  <div class="col-lg-3">
    <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Código</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">
                    <div class="form-group">
                        <form  @submit.prevent="FunEnfocarCantidad">
                            <label for="" title="1- Cantidad * código  ----  2-Código">CÓDIGO</label>
                            <input type="text" autocomplete="false" required class="form-control" v-model='codigoProducto' value="">
                        </form>
                    </div>
                    <form @submit.prevent='FunEnfocarSelect()'>
                      <div class="form-group">
                        <label for="">Cantidad</label>
                        <input type="text" id='cantidadP' onkeypress="return filterFloat(event,this);" class="form-control" v-model='cantidadProducto' name="" value="">
                      </div>
                    </form>

                      <div class="form-group">
                        <label >SELECCIONE UNA OPCIÓN</label>
                        <select id='seleccionable' name="" required class="form-control" @change="FunEnfocarMotivo" v-model='tipoDevolucion'>
                          <option :value="2">MERMA</option>
                          <option :value="3">DEVOLUCIÓN</option>
                        </select>
                      </div>
                      <form>
                        <div class="form-group">
                          <label>INGRESE UN MOTIVO</label>

                          <div class="form-group">
                            <input type="text" required  id='motivosSalida'  class="form-control" v-model='motivoSalida' name="" value="">
                          </div>
                        </div>

                      <div class="form-group">
                          <button type="submit" class="btn btn-primary" @click.prevent="FunAgregarProducto(codigoProducto,cantidadProducto)">Ingresar</button>
                      </div>
                    </form>
                  </div>
                  <div class="box-footer">

                  </div>

  </div>
</template>
  <div class="col-lg-9">
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <th>Código</th>
          <th>Descripción</th>
          <th>Cantidad</th>
        </thead>
        <tbody>
          <template v-if='productosInicialDiario'>
          <template v-for='inventario in productosInicialDiario'>
            <tr>
              <td>@{{inventario.cProInterCodigo}}</td>
              <td>@{{inventario.cProDesTicket}}</td>
              <td>@{{inventario.cantidad}}</td>
              <td v-if='inventario.tInvIniStatus == 2'>Merma</td>
              <td v-if='inventario.tInvIniStatus == 3'>Devoluciones</td>
              <td>
                <td>
                  <template v-if='dataHabilitar == 0'>
                      <template v-if='modificar == 2'>
                        <button type="button" class="btn btn-primary" name="button" @click='FunAbrirModalModificar(inventario)' > <i class="fa fa-edit"></i> </button>
                      </template>
                      <template v-if='eliminar == 3'>
                        <button type="button" class="btn btn-primary" name="button" @click='FunAbrirModalEliminar(inventario)'><i class="fa fa-trash"></i> </button>
                      </template>
                  </template>
              </td>
            </tr>
          </template>
          </template>
        </tbody>
      </table>
    </div>
  </div>
</div>
@include('Inventario.modales.modalModificar')
@include('Inventario.modales.modalEliminar')
</div>
@endsection
@section('scripts')

<script>
  function filterFloat(evt,input){
  // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
  var key = window.Event ? evt.which : evt.keyCode;
  var chark = String.fromCharCode(key);
  var tempValue = input.value+chark;
  if(key >= 48 && key <= 57){
    if(filter(tempValue)=== false){
      return false;
    }else{
      return true;
    }
  }else{
    if(key == 8 || key == 13 || key == 46 || key == 0) {
      return true;
    }else{
      return false;
    }
  }
}
function filter(__val__){
var preg = /^([0-9]+\.?[0-9]{0,2})$/;
if(preg.test(__val__) === true){
  return true;
}else{
  return false;
}

}
</script>
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/Inventario/MermasDevoluciones.js"></script>
@endsection
