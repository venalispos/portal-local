@extends('Master')
@section('titulo','Reporte de Inventario')
@section('content')

<div id="Inventario">
  <div class="row">
      <template v-if='reporte == 4'>
  <div class="col-lg-3">
      <form @submit.prevent='FunBuscarInventario()'>
          <div class="row">
            <div >
              <div class="form-group">
                <label>Seleccione una fecha para ver  detalles del inventario </label>
                <input type="date" class="form-control" v-model='fechaReporteInventario' name="">
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-block">BUSCAR <i class="fa fa-search"></i></button>
              </div>
            </div>

          </div>
        </form>
        <div class="form-group">
          <template >
          <button class="btn btn-app " @click='FunGenerarPDF()'> <i class="fa fa-save"></i>Descargar PDF</button>
          </template>
          <template >
          <button class="btn btn-app " @click='FunGenerarExcel()'> <i class="fa fa-save"></i> Descargar EXCEL</button>
          </template>
          <button type="button" class="btn btn-app" @click='FunGenerarProductosAdmintotalAPI()' name="button"><i class='fa fa-send' ></i> Enviar a admintotal</button>
          <button type="button" class="btn btn-app" @click='FunGenerarArchivosEnviarCorreo' name="button"><i class='fa fa-envelope-o' ></i> Enviar reportes</button>
        </div>
  </div>
  <template v-if='reporteInventario'>
    <div class="col-lg-9">
      <div class="table-responsive">
        <table class="table table-responsive">
          <thead>
            <th>Código</th>
            <th>Platillo</th>
            <th>Precio U.</th>
            <th>Inicial</th>
            <th>Entrada</th>
            <th>Devolución</th>
            <th>Merma</th>
            <th>Inventario final</th>
            <th>Cantidad Productos Vendidos</th>
            <th>Venta real</th>
            <th>Cantidad platillos capturados</th>
            <th align='right' style="text-align:right">Venta teórica</th>
            <th align='right' style='text-align:right;'>Cantidad de platillos</th>
            <th align='right' style='text-align:right;'>Monto total</th>
          </thead>
          <tbody>
            <template v-if='reporteInventario'>
              <template v-for='inventario in  reporteInventario'>
              <tr>
                <td>@{{inventario.cProInterCodigo}}</td>
                <td>@{{inventario.cProDesTicket}}</td>
                <td>@{{FunFormatearModena(inventario.cProPrecio)}}</td>
                <td>@{{inventario.cProInicial}}</td>
                <td>@{{inventario.cProentrada}}</td>
                <td>@{{inventario.cProSalidaD}}</td>
                <td>@{{inventario.cProSalidaM}}</td>
                <td> @{{inventario.cProFinal}}</td>
                <td> @{{FunRealizarInventarioFinal(inventario.cProInicial,inventario.cProentrada, inventario.cProSalidaM,inventario.cProSalidaD,inventario.cProFinal)}} </td>
                <td >@{{FunFormatearModena(FunRealizarInventarioFinal(inventario.cProInicial,inventario.cProentrada, inventario.cProSalidaM,inventario.cProSalidaD,inventario.cProFinal)*inventario.cProPrecio)}}</td>
                  <td>@{{inventario.cProVenta}} </td>
                <td  align='right'>@{{FunFormatearModena(inventario.cProVenta *inventario.cProPrecio)}}</td>
                <td align='right'>@{{inventario.cProVenta - FunRealizarInventarioFinal(inventario.cProInicial,inventario.cProentrada, inventario.cProSalidaM,inventario.cProSalidaD,inventario.cProFinal)}}</td>
                <td align='right'>@{{FunFormatearModena((inventario.cProVenta - FunRealizarInventarioFinal(inventario.cProInicial,inventario.cProentrada, inventario.cProSalidaM,inventario.cProSalidaD,inventario.cProFinal)) * inventario.cProPrecio) }}</td>
              </tr>
              </template>
              <tr>
                <th colspan="3"></th>
                <th >@{{totalInicial}}</th>
                <th >@{{totalEntrada}}</th>
                <th >@{{totalDevolucion}}</th>
                <th >@{{totalMerma}}</th>
                <th >@{{totalFinal}}</th>
                <th >@{{totalVentaRealCantidad}}</th>
                <th  align='right' style="text-align:right;">@{{FunFormatearModena(totalVentaRealTeorica)}} </th>
                <th>@{{ totalVentaReal}}</th>
                <th align='right' style='text-align:right;'>@{{FunFormatearModena(totalventa)}}</th>
                  <th align='right' style="text-align:right;">@{{totalVentaReal -totalVentaRealCantidad}}</th>
                  <th align='right'>@{{FunFormatearModena(totalMonto)}}</th>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
    </div>
  </template>

</template>
<template v-else>
  <div align='center'>
    <label for="" >NO SE PUEDE MOSTRAR LA INFORMACIÓN</label>
  </div>
</template>
</div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/Inventario/InventarioReporte.js"></script>
@endsection
