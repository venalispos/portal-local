<?php
$fecha="";
$fecha = date('d/m/Y H:m');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table width="100%" style="font-family:Courier;">
          <tr>
            <th  align="center" colspan="3" style=" font-family:Courier;font-style: bold; font-size:27px; text-align: center; ">
              @php print_r($info_encabezado['empresaSeleccionada'][0]['cConEmpresa']); @endphp
            </th>
          </tr>
            <tr>
              <td align="left" width="180" style="font-family:Courier; font-size:20px" >
                @php print_r($info_encabezado['empresaSeleccionada'][0]['cConSucursal']); @endphp
              </td>
              <td  align="center" style="font-family:Courier; font-size:26px; text-align: center; ">
                INVENTARIO DIARIO
              </td>
              <td style="font-family:Courier ; font-size:18px"  width="180"> {{$fecha}}</td>
            </tr>
            <tr>
              <td></td>
              <td align="center" style="font-family:Courier ; font-size:26px;" >
                 Fecha Inventario {{$info_encabezado['fecha']}}</td>
              <td>
              </td>
            </tr>
            <tr >
              <td colspan="3" style="padding-bottom: 15px;"><hr></td>
            </tr>
        </table>
        <hr>
        <br>
        <table width='100%'  style="font-family:COURIER;">
          <tr style="border:solid 5px green;">
            <th width='5%' alig='left' style="text-align:left;">Código|</th>
            <th width='5%' alig='left' style="text-align:left;">Platillo|</th>
            <th width='5%' align='right'>Precio U.|</th>
            <th width='5%' align='right' >Inicial|</th>
            <th width='5%' align='right'>Entrada|</th>
            <th width='5%' align='right'>Devolución|</th>
            <th width='5%' align='right'>Merma|</th>
            <th width='5%' align='right'> final|</th>
            <th width='5%' align='right'>Productos <br> Vendidos|</th>
            <th width='5%' align='right'>Venta real|</th>
            <th width='5%' align='right'>p. capturados|</th>
            <th width='5%' align='right'>Venta teórica|</th>
            <th width='5%' align='right'>Dif. <br> plat.|</th>
            <th width='5%' align='right'>Monto total|</th>
          </tr>
          	@foreach($dataGPDF['data']['datosInventario'] as $data)
              <tr >
                <td width='5%'>{{$data['cProInterCodigo']}}</td>
                <td width='5%'>{{$data['cProDesTicket']}}</td>
                <td width='5%' align='right'> <?php echo number_format($data['cProPrecio'],2); ?></td>
                <td width='5%' align='right'>{{$data['cProInicial']}}</td>
                <td width='5%' align='right'>{{$data['cProentrada']}}</td>
                <td width='5%' align='right'>{{$data['cProSalidaD']}}</td>
                <td width='5%' align='right'>{{$data['cProSalidaM']}}</td>
                  <td width='5%' align='right'> {{$data['cProFinal']}}</td>
                <td width='5%' align='right'><?php $resultadoInventarioFinal= ($data['cProInicial']+ $data['cProentrada'])-$data['cProSalidaD']-$data['cProSalidaM']-$data['cProFinal']; echo $resultadoInventarioFinal;  ?></td>
                  <td width='5%' align='right'><?php $resultadoInventarioFinal= number_format((($data['cProInicial']+ $data['cProentrada'])-$data['cProSalidaD']-$data['cProSalidaM']-$data['cProFinal'])*$data['cProPrecio'] ,2); echo $resultadoInventarioFinal;  ?></td>
                  <td width='5%' align='right'>{{$data['cProVenta']}}</td>
                    <td width='5%' align='right'><?php $resultadoInventarioFinal1= number_format($data['cProPrecio'] * $data['cProVenta'] ,2);  echo $resultadoInventarioFinal1; ?></td>
                    <?php $result=($data['cProVenta']-($data['cProInicial']+$data['cProentrada']-$data['cProSalidaD']-$data['cProSalidaM']-$data['cProFinal']))*$data['cProPrecio'] ?>
                    <td width='5%' align='right'>  <?php echo floatval($data['cProVenta'])-(floatval($data['cProInicial'])+floatval($data['cProentrada'])-floatval($data['cProSalidaD'])-floatval($data['cProSalidaM'])-floatval($data['cProFinal'])); ?></td>
                      <td width='5%' align='right'><?php echo number_format($result,2); ?></td>
              </tr>
        	@endforeach
          <tr>
            <th colspan="3"></th>
              <th  align='right' style="text-align:right;"> <?php echo $dataGPDF['data']['totalInicial']; ?> </th>
              <th  align='right' style="text-align:right;"> <?php echo $dataGPDF['data']['totalEntrada']; ?> </th>
              <th  align='right' style="text-align:right;"> <?php echo $dataGPDF['data']['totalDevolucion']; ?> </th>
              <th  align='right' style="text-align:right;"> <?php echo $dataGPDF['data']['totalMerma']; ?> </th>
              <th  align='right' style="text-align:right;"> <?php echo $dataGPDF['data']['totalFinal']; ?> </th>
              <th  align='right' style="text-align:right;"> <?php echo $dataGPDF['data']['totalventaCantidad']; ?> </th>
              <th  align='right' style="text-align:right;"> <?php echo number_format($dataGPDF['data']['totalventateoricacantidad'],2); ?> </th>
              <th  align='right' style="text-align:right;"> <?php echo $dataGPDF['data']['totalVentareal']; ?> </th>
              <th  align='right' style="text-align:right;"> <?php echo number_format($dataGPDF['data']['totalventa'],2); ?> </th>
              <th  align='right' style="text-align:right;">{{$dataGPDF['data']['totalVentareal'] -  $dataGPDF['data']['totalventaCantidad'] }} </th>
              <th  align='right' style="text-align:right;"> <?php echo number_format($dataGPDF['data']['totalventa']-$dataGPDF['data']['totalventateoricacantidad'],2); ?> </th>

          </tr>
        </table>
  </body>
</html>
