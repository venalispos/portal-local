@extends('Master')
@section('titulo','Realizar traspaso')
@section('styles')
<link rel="stylesheet" href="/css/toastr.css">
<link rel="stylesheet" href="/css/vue-multiselect.min.css">
@endsection
@section('content')
<div id="peticionTraspaso">
<div class="row">
  <div class="col-lg-3">
    <form @submit.prevent="FunEnviarDatosControlador">
      <div class="form-group">
        <label for="">Seleccione un centro de costos</label>
        <select class="form-control" required v-model='productoSelected.CentroCosto'>
          <template v-for='centros in centroCostos'>
            <option :value="centros.cCeCoId">@{{centros.cCeCoDescripcion}}</option>
          </template>
        </select>
      </div>
      <div class="form-group">
        <template v-if='productosdata'>
          <label for="">Seleccione un producto</label>
          <multiselect
                               placeholder="Seleccionar producto ..."
                               label="cProDescripcion" track-by="cProInterCodigo"
                               v-model="productoSelected.producto"
                               open-direction="bottom"
                               required="true"
                               :options="productosdata"
                               :multiple="false"
                               :close-on-select="true"
                               :limit="3"
                               select-label="Agregar Producto"
                               selected-label="Ya agregado"
                               deselect-label="Quitar">
                             </multiselect>
        </template>
      </div>
      <div class="form-group">
        <label for="">Cantidad</label>
        <input type="number" v-model='productoSelected.cantidad' required class="form-control">
      </div>
      <div class="form-group">
        <button type="submit"  class="btn btn-primary btn-block" name="button"><i class="fa fa-search"></i> Buscar</button>
      </div>
    </form>
  </div>
  <div class="col-lg-9">
    <div class="table-responsive">
      <template v-if='productosSinFinalizar'>

        <table class="table table-hover table-border">
          <thead>
            <th>Centro costos a enviar</th>
            <th>Producto</th>
            <th>Cantidad</th>
            <th colspan="1">Opciones</th>
          </thead>
          <tbody>
            <template v-for='productos in productosSinFinalizar'>
              <tr>
                <td>@{{productos.cHistrasCentroCosto}}</td>
                <td>@{{productos.cHistrasCodigoProducto}}</td>
                <td>@{{productos.cHistrasCantidad}}</td>
                <td><button type="button" @click='FunModalEliminar(productos)' class="btn btn-primary" name="button"><i class="fa fa-trash"></i> </button> </td>
              </tr>
            </template>
          </tbody>
        </table>
        <button type="button" class="btn btn-rpimary" @click='FunActualizarFinalizarRegistros' name="button"> Finalizar</button>
      </template>

    </div>
  </div>
</div>
@include('Inventario.traspaso.modals.confirmarEliminar')
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/vuejs/vue-multiselect.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/Inventario/traspasos/peticion.js"></script>
@endsection
