<form  @submit.prevent="FunEliminarRegistro()">

 <div class="modal fade" id="meliminar" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog modal-md">
     <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" @click=''>&times;</button>
           <h4>¿Desea realmente eliminar el registro seleccionado?</h4>
         </div >
         <div class="modal-body">
           <h3><strong></strong></h3>
         </div>
         <div class="modal-footer">
           <div class="pull-right">
             <button class="btn btn-warning" type="submit" >Si, deseo eliminar</button>
           </div>
         </div>
     </div>
   </div>
 </div>
</form>
