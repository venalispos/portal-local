<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table  width='100%'>
      <hr>
      <label for="">ENTRADAS</label>
      <hr>
      <tr>
        <th align='left'>FECHA</th>
        <th align='right'>CANTIDAD</th>
        <th align='left'>CODIGO</th>
        <th align='left'>DESCRIPCION</th>
      </tr>
      @foreach($dataGPDF['data']['entradas'] as $entradas)
      <tr>
        <td align='left'>{{$entradas['cInvFechaRegistro']}}</td>
        <td align='right'>{{$entradas['cInvCantidad']}}</td>
        <td align='left'>{{$entradas['cProInterCodigo']}}</td>
        <td align='left'>{{$entradas['cProDesTicket']}}</td>
      </tr>
      @endforeach
    </table>
    <br>
    <table  width='100%'>
      <hr>
      <label for="">SALIDAS MERMAS</label>
      <hr>
      <tr>
        <th align='left'>FECHA</th>
        <th align='right'>CANTIDAD</th>
        <th align='left'>CODIGO</th>
        <th align='left'>DESCRIPCION</th>
      </tr>
      @foreach($dataGPDF['data']['entradas'] as $entradas)
      <tr>
        <td align='left'>{{$entradas['cInvFechaRegistro']}}</td>
        <td align='right'>{{$entradas['cInvCantidad']}}</td>
        <td align='left'>{{$entradas['cProInterCodigo']}}</td>
        <td align='left'>{{$entradas['cProDesTicket']}}</td>
      </tr>
      @endforeach
    </table>
    <br>
    <table  width='100%'>
      <hr>
      <label for="">SALIDAS DEVOLUCIONES</label>
      <hr>
      <tr>
        <th align='left'>FECHA</th>
        <th align='right'>CANTIDAD</th>
        <th align='left'>CODIGO</th>
        <th align='left'>DESCRIPCION</th>
      </tr>
      @foreach($dataGPDF['data']['entradas'] as $entradas)
      <tr>
        <td align='left'>{{$entradas['cInvFechaRegistro']}}</td>
        <td align='right'>{{$entradas['cInvCantidad']}}</td>
        <td align='left'>{{$entradas['cProInterCodigo']}}</td>
        <td align='left'>{{$entradas['cProDesTicket']}}</td>
      </tr>
      @endforeach
    </table>
    <br>
    <table  width='100%'>
      <hr>
      <label for="">INVENTARIO FINAL</label>
      <hr>
      <tr>
        <th align='left'>FECHA</th>
        <th align='right'>CANTIDAD</th>
        <th align='left'>CODIGO</th>
        <th align='left'>DESCRIPCION</th>
      </tr>
      @foreach($dataGPDF['data']['entradas'] as $entradas)
      <tr>
        <td align='left'>{{$entradas['cInvFechaRegistro']}}</td>
        <td align='right'>{{$entradas['cInvCantidad']}}</td>
        <td align='left'>{{$entradas['cProInterCodigo']}}</td>
        <td align='left'>{{$entradas['cProDesTicket']}}</td>
      </tr>
      @endforeach
    </table>
  </body>
</html>
