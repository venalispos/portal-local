@extends('Master')
@section('titulo','Reporte de movimientos físicos')
@section('content')

<div id="Inventario">
  <div class="row">
      <template v-if='reporte == 4'>
            <div class="col-lg-3">
                <form @submit.prevent='FunObtenerInventarioFisico()'>
                    <div class="row">
                      <div >
                        <div class="form-group">
                          <label>Seleccione una fecha para ver  detalles de los movimientos</label>
                          <input type="date" class="form-control" v-model='dfechaReporte' name="">
                        </div>
                        <div class="form-group">
                          <button class="btn btn-primary btn-block">BUSCAR <i class="fa fa-search"></i></button>
                        </div>
                      </div>

                    </div>
                  </form>
                  <div class="form-group">
                    <template >
                    <button class="btn btn-app " @click='FunGenerarPDF()'> <i class="fa fa-save"></i>Descargar PDF</button>
                    </template>
                    <template >
                    <button class="btn btn-app " @click='FunGenerarExcel()'> <i class="fa fa-save"></i> Descargar EXCEL</button>
                    </template>

                  </div>
            </div>
            <div class="col-lg-9">
              <template v-if='aEntradas'>
                        <div class="form-group">
                          <label class="form-control"> ENTRADAS DIARIAS</label>
                        </div>
                    <div class="form-group">
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <th>FECHA</th>
                              <th>CANTIDAD</th>
                              <th>CÓDIGO</th>
                              <th>DESCRIPCIÓN</th>
                          </thead>
                          <tbody>
                            <template v-for='entradas in aEntradas'>
                              <tr>
                                <th>@{{entradas.cInvFechaRegistro}}</th>
                                <td>@{{entradas.cInvCantidad}}</td>
                                <td>@{{entradas.cProInterCodigo}}</td>
                                <td>@{{entradas.cProDesTicket}}</td>
                              </tr>
                            </template>
                          </tbody>
                        </table>
                      </div>
                      </div>
              </template>
              <template v-if='aMermas'>
                        <div class="form-group">
                          <label class="form-control"> SALIDAS</label>
                        </div>
                    <div class="form-group">
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <th>FECHA</th>
                              <th>CANTIDAD</th>
                              <th>CÓDIGO</th>
                                <th>DESCRIPCIÓN</th>
                          </thead>
                          <tbody>
                            <template v-for='mermas in aMermas'>
                              <tr>
                                <th>@{{mermas.cInvFechaRegistro}}</th>
                                <td>@{{mermas.cInvCantidad}}</td>
                                <td>@{{mermas.cProInterCodigo}}</td>
                                <td>@{{mermas.cProDesTicket}}</td>
                              </tr>
                            </template>
                          </tbody>
                        </table>
                      </div>
                      </div>
              </template>
              <template v-if='aDevoluciones'>
                        <div class="form-group">
                          <label class="form-control"> SALIDAS DEVOLUCIONES</label>
                        </div>
                    <div class="form-group">
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <th>FECHA</th>
                              <th>CANTIDAD</th>
                              <th>CÓDIGO</th>
                                <th>DESCRIPCIÓN</th>
                          </thead>
                          <tbody>
                            <template v-for='devolucion in aDevoluciones'>
                              <tr>
                                <th>@{{devolucion.cInvFechaRegistro}}</th>
                                <td>@{{devolucion.cInvCantidad}}</td>
                                <td>@{{devolucion.cProInterCodigo}}</td>
                                <td>@{{devolucion.cProDesTicket}}</td>
                              </tr>
                            </template>
                          </tbody>
                        </table>
                      </div>
                      </div>
              </template>
              <template v-if='aFinal'>
                        <div class="form-group">
                          <label class="form-control"> INVENTARIO FINAL CAPTURADO</label>
                        </div>
                    <div class="form-group">
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <th>FECHA</th>
                              <th>CANTIDAD</th>
                              <th>CÓDIGO</th>
                                <th>DESCRIPCIÓN</th>
                          </thead>
                          <tbody>
                            <template v-for='final in aFinal'>
                              <tr>
                                <th>@{{final.cInvFechaRegistro}}</th>
                                <td>@{{final.cInvCantidad}}</td>
                                <td>@{{final.cProInterCodigo}}</td>
                                <td>@{{final.cProDesTicket}}</td>
                              </tr>
                            </template>
                          </tbody>
                        </table>
                      </div>
                      </div>
              </template>
            </div>
      </template>
      <template v-else>
    <div align='center'>
      <label for="" >NO SE PUEDE MOSTRAR LA INFORMACIÓN</label>
    </div>
  </template>
</div>
</div>

@endsection
@section('scripts')
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/Inventario/InventarioMovimientosFisicos.js"></script>
@endsection
