@extends('Master')
@section('titulo','Entradas')
@section('content')
<div id="Inventario">
  <template v-if='Insertar == 1'>
  <div class="row">
    <div class="col-lg-12">
      <!-- <div class="contenedor">
        <button class="btn btn Default" @click='FunModalManual'>
          <span>+</span>
        </button>
      </div> -->
    </div>
  </div>
</template>
<div class="row">
    <template v-if='Insertar == 1'>
  <div class="col-lg-3">
    <div class="box box-success" id='boxCaptura'>
                <div class="box-header with-border">
                  <h3 class="box-title">Código</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                  <div class="box-body">
                      <form  @submit.prevent="FunEnfocarCantidad()">
                          <div class="form-group">
                            <label for="" title="1- Cantidad * código  ----  2-Código">CÓDIGO</label>
                            <input type="text"  id='codigoP' autocomplete="false" required class="form-control" v-model='codigoProducto' value="">
                          </div>
                      </form>
                      <form @submit.prevent="FunAgregarProducto(codigoProducto,cantidadProducto)">
                            <div class="form-group">
                              <label for="">Cantidad</label>
                              <input type="text" onkeypress="return filterFloat(event,this);" name='cantidadP' autocomplete="false"  id='cantidadP' required class="form-control" v-model='cantidadProducto' value="">
                            </div>
                              <div class="form-group">
                                <button type="submit" name='ingresar' class="btn btn-primary">Ingresar</button>
                              </div>
                      </form>
                  </div>
                  <div class="box-footer">

                  </div>

              </div>
  </div>
</template>
  <div class="col-lg-9">
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <th>Código</th>
          <th>Descripción</th>
          <th>Cantidad</th>
          <th></th>
        </thead>
        <tbody>
          <template v-if='productosInicialDiario'>
          <template v-for='inventario in productosInicialDiario'>
            <tr>
              <td>@{{inventario.cProInterCodigo}}</td>
              <td>@{{inventario.cProDesTicket}}</td>
              <td>@{{inventario.cantidad}}</td>
              <td><template v-if='dataHabilitar == 0'>
                <template  v-if='modificar == 2'>
                  <button type="button" class="btn btn-primary" name="button" @click='FunAbrirModalModificar(inventario)' > <i class="fa fa-edit"></i> </button>
                </template>
                <template v-if='eliminar == 3'>
                    <button type="button" class="btn btn-primary" name="button" @click='FunAbrirModalEliminar(inventario)'><i class="fa fa-trash"></i> </button>
                </template>

              </template>
              </td>
            </tr>
          </template>
          </template>
        </tbody>
      </table>
    </div>
  </div>
</div>
@include('Inventario.modales.modalModificar')
@include('Inventario.modales.modalEliminar')
@include('Inventario.modales.addmanual')
</div>
@endsection
@section('scripts')
<script>
  function filterFloat(evt,input){
  // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
  var key = window.Event ? evt.which : evt.keyCode;
  var chark = String.fromCharCode(key);
  var tempValue = input.value+chark;
  if(key >= 48 && key <= 57){
    if(filter(tempValue)=== false){
      return false;
    }else{
      return true;
    }
  }else{
    if(key == 8 || key == 13 || key == 46 || key == 0) {
      return true;
    }else{
      return false;
    }
  }
}
function filter(__val__){
var preg = /^([0-9]+\.?[0-9]{0,2})$/;
if(preg.test(__val__) === true){
  return true;
}else{
  return false;
}

}
</script>
<script src="/js/axios.min.js"></script>
<script src="/js/vue.js"></script>
<script src="/vuejs/Inventario/inventarioInicialDiario.js"></script>
@endsection
