<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
  </div>
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">MENU</li>
    <?php

    $permisos=auth()->user()->cUsuPuesto1Perfil;
    $perfilPermisos=DB::table('tPerfiles')->select('cPerAccesPortal')->where('cPerId',$permisos)->get()->toArray();
      $perfilPermisos=json_decode(json_encode($perfilPermisos),true);
    $array = explode(",", $perfilPermisos[0]['cPerAccesPortal']);
    $GLOBALS['menu_datos']=array();
    $GLOBALS['datosmostrar']=null;

      $enlace_actual = $_SERVER['REQUEST_URI'];//'http://'.$_SERVER['HTTP_HOST'].
      $gFolio = DB::table('tMenu')
      ->select('cMenFolio')
      ->where('cMenURL',$enlace_actual)
      ->get();
      $Folio=json_decode(json_encode($gFolio),true);


    $content = DB::table('tMenu')
    ->orderBy('cMenOrden')
    ->whereIn('cMenFolio',[$array][0])
    ->get();
    $empresaSel=json_decode(json_encode($content),true);

    foreach ($empresaSel as $key ) {
     $GLOBALS['menu_datos'][]=$key;
   }
   Menupadre();
   echo $GLOBALS['datosmostrar'];

   function Menupadre(){
    foreach ($GLOBALS['menu_datos']as $menue) {
      if ($menue['cMenFolPadre']==0 ) {
        if ($menue['cMenHijos']) {
          hijos($menue);
        }else{
          $GLOBALS['datosmostrar'].= " <li ><a href='".$menue['cMenURL']."'><i class='fa  fa-home'></i><span>".$menue['cMenDescripcion']."</span></a></li>";
        }
      }
    }
  }

  function hijos($menupadre){
    $menumaster=$menupadre;
    $GLOBALS['datosmostrar'].='<li class="treeview">
    <a href="#">
    <i class="fa fa-files-o"></i> <span>'.$menumaster['cMenDescripcion'].'</span>
    <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
    <ul class="treeview-menu">';
    foreach ($GLOBALS['menu_datos']as $menue) {
      if ($menue['cMenFolPadre']==$menumaster['cMenFolio']) {
        if ($menue['cMenHijos']==1) {
          hijos($menue);
        }else
        {
        //  $GLOBALS['datosmostrar'].='<li><a href="'.$menue['cMenURL'].'/'.$menue['cVersion'].'" ><i class="'.$menue['cMenIcono'].'"></i>'.$menue['cMenDescripcion'].'</a></li>';
            $GLOBALS['datosmostrar'].='<li><a href="'.$menue['cMenURL'].'" ><i class="'.$menue['cMenIcono'].'"></i>'.$menue['cMenDescripcion'].'</a></li>';
        }
      }
    }
    $GLOBALS['datosmostrar'].="</ul></li>";

  }

  ?>
</ul>
</section>
