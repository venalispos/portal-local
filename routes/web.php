<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/','Auth\LoginController@showLoginForm')->name('/');


// Route::get('/', function () {
//     // session_start();
//     // if(isset($_SESSION['cEmpFolio'])){
//     //     return redirect('inicio.php');
//     // } else {
//     //    // return redirect('login.php');
//     // 	echo "string";
//     // }
// });

Route::get('/','Auth\LoginController@showLoginForm')->name('/');
Route::post('login','Auth\LoginController@login')->name('login');
Route::post('logout','Auth\LoginController@logout')->name('logout');
Route::get('reiniciar','Auth\LoginController@FunReiniciarPassword')->name('reiniciar');
Route::post('reiniciarPass','Auth\LoginController@FunModificarPassword')->name('reiniciarPass');
Route::get('home','DashboardController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
//Ayuda
Route::get('respaldo','RespaldoController@index')->name('respaldo');
Route::get('respaldo/generar','RespaldoController@generarRespaldo')->name('respaldo');
Route::get('respaldo/generar1','RespaldoController@generarRespaldo1')->name('respaldo');
Route::get('prueba/fechas/reporte/top','ReportesController@obtenerTop')->name('top');

//reportes
Route::get('reportes/ticket','ReportesController@indexticket')->name('reporteticket');
Route::get('reportes/productos','ReportesController@indexproductos')->name('reporteProductos');
Route::get('reportes/Resumen','ResumenController@indexResumen')->name('reporteResumen');;
Route::post('reportes/ticketGenerar','ReportesController@generarReporteTickets');
Route::post('reportes/generarPDFIticket','ReportesController@descargarPDFticket');
Route::post('reportes/productosGenerar','ReportesController@genRepProductos');
Route::post('reportes/generarPDFProd','ReportesController@pdfProductos');
Route::post('reportes/excelProductos','ReportesController@excelProductos');
Route::post('reportes/excelTicket','ReportesController@excelTicket');
Route::post('reportes/generarResumenrespuesta','ResumenController@generarResumenN');
Route::post('reportes/generarResumenPDF','ResumenController@generarResumenPDF');
Route::post('reportes/generarResumenExcel','ResumenController@generarResumenExcel');
Route::get('reportes/desgloseTP','ReportesController@indexDeglose');
Route::post('reportes/desgloseTP/generar','ReportesController@generarDesglose');
Route::post('reportes/desgloseTP/generarPDF/en','ReportesController@generarPDFDesgloseEN');
Route::post('reportes/desgloseTP/generarPDF','ReportesController@generarPDF');
Route::post('reportes/desgloseTP/generarEXCEL','ReportesController@generarDesgloseEXCEL');
Route::get('reportes/descuentos','ReportesController@indexDescuentos')->name('reporteDescuentos');
Route::post('reportes/descuentos/generar','ReportesController@generarDescuentos');
Route::get('reportes/Articulos','ReportesController@indexArticulo')->name('reporteArticulo');
Route::post('reportes/obtenerArticulos','ReportesController@obtenerArticulos');
Route::post('reportes/obtenerArticulosPDF','ReportesController@obtenerArticulosPDF');
Route::post('reportes/obtenerArticulosEXCEL','ReportesController@obtenerArticulosEXCEL');
Route::get('reportes/platillo','ReportesController@indexPlatillo')->name('reportePlatillo');;
Route::post('reportes/platillo/generar','ReportesController@obtenerreportePlatillosGen');
Route::get('reportes/platillo/Categorias','ReportesController@obtenerCategorias');
Route::post('reportes/platillo/PDF','ReportesController@generarPDFplatillos');
Route::post('reportes/platillo/EXCEL','ReportesController@generarEXCELplatillos');

//catalogo clientes
Route::get('/catalogos/clientes/principal', 'ClientesController@clientesPrincipal');
Route::get('/catalogos/clientes/obtenerClientes', 'ClientesController@clientesObtener');
Route::post('/catalogos/clientes/agregar', 'ClientesController@clientesAgregar');
Route::post('/catalogos/clientes/dataeditar', 'ClientesController@clientesDataEditar');
Route::post('/catalogos/clientes/editar', 'ClientesController@clientesEditar');
Route::post('/catalogos/clientes/eliminar', 'ClientesController@clientesEliminar');

//Sales report
Route::get('reportes/sales_report', 'SalesReportController@index');;
Route::post('reportes/sales_report/ventas', 'SalesReportController@obtener_ventas');
Route::post('reportes/sales_report/pdf', 'SalesReportController@pdf_sales_report')->name('sales_report_pdf');

//Product mix report
//Route::get('reportes/product_mix/{Version}', 'ProductMixReportController@index');
Route::get('reportes/product_mix', 'ProductMixReportController@index');
Route::post('reportes/product_mix/ventas', 'ProductMixReportController@obtener_ventas');
Route::post('reportes/product_mix/pdf', 'ProductMixReportController@pdf_product_mix')->name('sales_report_pdf');


//Reporte por empleadosIndex
Route::get('/reportes/reportexemp', 'ReportesController@verVentasEmpleado');;
Route::post('/reportes/empleado/generar', 'ReportesController@generarReporteEmpleado');



//Reporte Corte
Route::get('/reportes/corte', 'ReportesController@indexCorte');
Route::post('/reportes/corte/generar', 'ReportesController@generarReporteCorte');
Route::post('/reportes/corte/generar/pdf', 'ReportesController@generarPDFCorte');
Route::post('/reportes/corte/generar/excel', 'ReportesController@generarExcelCorte');

///Configuracion

Route::get('configuracion/obtener','Configuracion\ConfiguracionController@obtenerConfiguracion');

//menu
Route::get('menu/configuracion','MenuController@indexmenu')->name('menu');
Route::get('menu/configuracion/lista','MenuController@obtenerMenu')->name('getMenu');
Route::post('menu/nuevoItem','MenuController@nuevoMenu')->name('addMenu');
Route::post('menu/ActualizarMenu','MenuController@actualizarMenu')->name('MenuActualizar');
Route::post('menu/EliminarMenu','MenuController@eliminarMenu')->name('menuEliminar');

//privilegios
Route::get('Privilegios','PrivilegiosController@index')->name('privilegios');
Route::post('Privilegios/usuario','PrivilegiosController@buscarusuarios')->name('usuariosbuscar');

//Catalogos
Route::get('catalogo/empleadosIndex','CatalogoController@indexEmpleados');
Route::get('catalogo/getUserRoute','CatalogoController@getUser');
Route::get('catalogo/obtenerusuarios','CatalogoController@getUser');
Route::post('catalogo/shareEmploye','CatalogoController@addNewEmploye');
Route::post('catalogo/eliminarEmpleado','CatalogoController@eliminarEmpleado');
Route::post('catalogo/updateEmployee','CatalogoController@updateEmployee');
Route::get('catalogo/productoIndex','CatalogoController@IndexProductos');
Route::get('catalogo/getProductos','CatalogoController@getProductos');

//Catalogo de productos
Route::get('catalogo/productos/principal','Catalogos\ProductosController@VistaProductos')->name('productos');
Route::get('catalogo/productos/reporte','Catalogos\ProductosController@VistaProductosReporte')->name('productos.reporte');
Route::get('catalogo/productos/obtenerProductos','Catalogos\ProductosController@obtenerProductos');
Route::get('catalogo/productos/obtenerDatos','Catalogos\ProductosController@obtenerDatos');
Route::post('catalogo/productos/agregarProducto','Catalogos\ProductosController@agregarProducto');
Route::post('catalogo/productos/generarExcel','Catalogos\ProductosController@generarExcel');
Route::post('catalogo/productos/editarProducto','Catalogos\ProductosController@editarProducto');

//Catalogo de impuestos
Route::get('catalogo/configuracionImpuestos/principal','Configuracion\ImpuestosController@VistaImpuestos')->name('impuestos');
Route::get('configuracion/impuestos/obtenerImpuestos','Configuracion\ImpuestosController@obtenerImpuestos');
Route::get('configuracion/impuestos/obtenerDatos','Configuracion\ImpuestosController@obtenerDatos');
Route::post('configuracion/impuestos/agregarImpuesto','Configuracion\ImpuestosController@agregarImpuesto');
Route::post('configuracion/impuestos/editarImpuesto','Configuracion\ImpuestosController@editarImpuesto');

//Catalogo de mermas
Route::get('catalogos/mermas', 'Catalogos\MermasController@index');;
Route::get('catalogo/mermas/categorias', 'Catalogos\MermasController@obtener_categorias');
Route::post('catalogos/mermas/obtener_mermas', 'Catalogos\MermasController@obtener_mermas');
Route::post('catalogos/mermas/agregar_merma', 'Catalogos\MermasController@agregar_merma');
Route::get('catalogos/mermas/obtener_fecha', 'Catalogos\MermasController@obtener_fecha');

//cortes

Route::get('/cortes/cortesPrincipal/obtenerData', 'CortesController@cortesData');
Route::post('/cortes/cortesPrincipal/agregarCorte', 'CortesController@cortesAgregar');
Route::post('/cortes/cortesPrincipal/dataCorte', 'CortesController@cortesVer');

//cortes new
Route::get('/cortes/cortesprincipal', 'CorteController@Index');
Route::post('/cortes/cortesPrincipal/obtenerCortes', 'CorteController@FunObtenerCorte');
Route::post('/cortes/cortesPrincipal/obtenerItems', 'CorteController@FunObtenerItemFormaPago');
Route::post('/cortes/cortesPrincipal/GuardarCantidadItem', 'CorteController@FunGuardarItemCantidad');
Route::post('/cortes/cortesPrincipal/guardarGastoModificado', 'CorteController@FunGuardarGastoModificado');
Route::post('/cortes/cortesPrincipal/guardarDevolucion', 'CorteController@FunGuardarDevolucionModificado');
Route::post('/cortes/cortesPrincipal/guardarGasto', 'CorteController@FunGuardarGasto');
Route::post('/cortes/cortesPrincipal/guardarDevolucionAgregar', 'CorteController@FunGuardarDevolucion');
Route::post('/cortes/cortesPrincipal/eliminarGasto', 'CorteController@FunEliminarGasto');
Route::post('/cortes/cortesPrincipal/finalizarCorte', 'CorteController@FunFinalizarCorte');
Route::post('/cortes/cortesPrincipal/guardarCorte', 'CorteController@FunguardarCorte');
Route::post('/cortes/cortesPrincipal/verEstatusCorte', 'CorteController@FunVerEstatus');
Route::post('/cortes/cortesPrincipal/arqueoVentaPDF', 'CorteController@FunRealizarDescargaPDFVenta');
Route::post('/cortes/cortesPrincipal/corteCajaPDf', 'CorteController@FunRealizarDescargaPDFCorteCaja');
Route::post('/cortes/cortesPrincipal/corteDescargaBanamex', 'CorteController@FunRealizarDescargaBanamex');
Route::post('/cortes/cortesPrincipal/corteDescargarDolar', 'CorteController@FunRealizarDolarPDf');

//Precierre
Route::get('/precierre/precierreprincipal', 'PrecierreController@precierrePrincipal');
Route::get('/precierre/precierrePrincipal/obtenerPrecierres', 'PrecierreController@precierreObtener');
Route::get('/precierre/precierrePrincipal/obtenerData', 'PrecierreController@precierreData');
Route::post('/precierre/precierrePrincipal/agregarPrecierre', 'PrecierreController@precierreAgregar');
Route::post('/precierre/precierrePrincipal/dataPrecierre', 'PrecierreController@precierreVer');

//Catalogo de categorias
Route::get('catalogo/categorias/obtenerCategorias','Catalogos\CategoriasController@obtenerCategorias');
Route::get('catalogo/categorias/principal', 'Catalogos\CategoriasController@VistaCategorias');;
Route::post('catalogo/categorias/agregarCategoria', 'Catalogos\CategoriasController@agregarCategorias');
Route::post('catalogo/categorias/editarCategoria','Catalogos\CategoriasController@editarCategorias');
Route::post('catalogo/categorias/eliminarCategorias','Catalogos\CategoriasController@eliminarCategorias');

//Catalogo de terminales
Route::get('catalogo/configuracionTerminales/obtenerTerminales','Catalogos\TerminalesController@obtenerTerminales');
Route::get('catalogo/configuracionTerminales/principal', 'Catalogos\TerminalesController@VistaTerminales');;
Route::post('catalogo/configuracionTerminales/agregarConfigTerminal', 'Catalogos\TerminalesController@agregarConfigTerminal');
Route::post('catalogo/configuracionTerminales/editarConfigTerminal','Catalogos\TerminalesController@editarConfigTerminal');
Route::post('catalogo/configuracionTerminales/eliminarConfigTerminal','Catalogos\TerminalesController@eliminarConfigTerminal');

//Catalogo de boton subcategorias
Route::get('catalogo/botonSubcategorias/obtener','Catalogos\BotSubCategoriasController@obtener');
Route::get('catalogo/botonSubcategorias/principal', 'Catalogos\BotSubCategoriasController@VistaBotSubCategorias');;
Route::post('catalogo/botonSubcategorias/agregarBotonSubCategorias', 'Catalogos\BotSubCategoriasController@agregarBotonSubCategorias');
Route::post('catalogo/botonSubcategorias/editarBotonSubCategorias','Catalogos\BotSubCategoriasController@editarBotonSubCategorias');
Route::post('catalogo/botonSubcategorias/eliminarBotonSubCategorias','Catalogos\BotSubCategoriasController@eliminarBotonSubCategorias');

//Catalogo de boton categorias
Route::get('catalogo/botonCategorias/obtenerBotCategorias','Catalogos\BotCategoriasController@obtenerBotCategorias');
Route::get('catalogo/botonCategorias/principal', 'Catalogos\BotCategoriasController@VistaBotCategorias');;
Route::post('catalogo/botonCategorias/agregarBotonCategorias', 'Catalogos\BotCategoriasController@agregarBotonCategorias');
Route::post('catalogo/botonCategorias/editarBotonCategorias','Catalogos\BotCategoriasController@editarBotonCategorias');
Route::post('catalogo/botonCategorias/eliminarBotonCategorias','Catalogos\BotCategoriasController@eliminarBotonCategorias');

//Catalogo de boton productos
Route::get('catalogo/botonProductos/obtenerBotonProductos','Catalogos\BotProductosController@obtenerBotonProductos');
Route::get('catalogo/botonProductos/principal', 'Catalogos\BotProductosController@VistaBotProductos');;
Route::post('catalogo/botonProductos/agregarBotonProductos', 'Catalogos\BotProductosController@agregarBotonProductos');
Route::post('catalogo/botonProductos/editarBotonProductos','Catalogos\BotProductosController@editarBotonProductos');
Route::post('catalogo/botonProductos/eliminarBotonProductos','Catalogos\BotProductosController@eliminarBotonProductos');

//Catalogo de perfiles
Route::get('catalogo/perfiles/obtenerPerfiles','Catalogos\PerfilesController@obtenerPerfiles');
Route::get('catalogo/perfiles/principal', 'Catalogos\PerfilesController@VistaPerfiles');;
Route::post('catalogo/perfiles/agregarPerfil', 'Catalogos\PerfilesController@agregarPerfiles');
Route::post('catalogo/perfiles/editarPerfil','Catalogos\PerfilesController@editarPerfiles');
Route::post('catalogo/perfiles/eliminarPerfil','Catalogos\PerfilesController@eliminarPerfil');

//Catalogo de Combos
Route::get('catalogo/comcategorias/obtenerComCategorias','Catalogos\ComCategoriasController@obtenerComCategorias');
Route::get('catalogo/comcategorias/principal','Catalogos\ComCategoriasController@VistaComCategorias');
Route::post('catalogo/comcategorias/agregarComCategorias','Catalogos\ComCategoriasController@agregarComCategorias');
Route::post('catalogo/comcategorias/editarComCategorias','Catalogos\ComCategoriasController@editarComCategorias');
Route::post('catalogo/comcategorias/eliminarComCategorias','Catalogos\ComCategoriasController@eliminarComCategorias');

//Catalogo combo Subcategoria
Route::get('catalogo/comsubcategorias/obtenerComSubCategorias','Catalogos\ComSubCategoriasController@obtenerComSubCategorias');
Route::get('catalogo/comsubcategorias/principal','Catalogos\ComSubCategoriasController@VistaComSubCategorias');;
Route::post('catalogo/comsubcategorias/agregarComSubcategorias','Catalogos\ComSubCategoriasController@agregarComSubcategorias');;
Route::post('catalogo/comsubcategorias/editarComSubcategorias','Catalogos\ComSubCategoriasController@editarComSubcategorias');
Route::post('catalogo/comsubcategorias/eliminarComSubcategorias','Catalogos\ComSubCategoriasController@eliminarComSubcategorias');

//Catalogo combo productos
Route::get('catalogo/comboProductos/obtenerComProductos','Catalogos\ComProductosController@obtenerComProductos');
Route::get('catalogo/comboProductos/principal','Catalogos\ComProductosController@VistaComProductosCategorias');;
Route::post('catalogo/comboProductos/agregarComProductos','Catalogos\ComProductosController@agregarComProductos');
Route::post('catalogo/comboProductos/editarComProductos','Catalogos\ComProductosController@editarComProductos');
Route::post('catalogo/comboProductos/eliminarComProductos','Catalogos\ComProductosController@eliminarComProductos');

//Catalogo de clasificacion
Route::get('catalogo/configuracionNube/principal','Catalogos\ClasificacionController@obtenerClasificacion');
Route::get('catalogo/configuracionNube/vistaprincipal','Catalogos\ConfiguracionNubeController@VistaConfigNube');
Route::get('catalogo/configuracionNube/obtenerConfigNube','Catalogos\ConfiguracionNubeController@obtenerConfigNube');
Route::post('catalogo/configuracionNube/agregarConfigNube','Catalogos\ConfiguracionNubeController@agregarConfigNube');
Route::post('catalogo/configuracionNube/editarConfigNube','Catalogos\ConfiguracionNubeController@editarConfigNube');

//Catalogo de subcategorias
Route::get('catalogo/subcategorias/obtenerSubCategorias','Catalogos\SubCategoriasController@obtenerSubCategorias');
Route::get('catalogo/subcategorias/principal','Catalogos\SubCategoriasController@VistaSubCategorias');;
Route::post('catalogo/subcategorias/agregarSubcategoria', 'Catalogos\SubCategoriasController@agregarSubcategorias');
Route::post('catalogo/subcategorias/editarSubcategoria','Catalogos\SubCategoriasController@editarSubCategorias');
Route::post('catalogo/subcategorias/eliminarSubcategoria','Catalogos\SubCategoriasController@eliminarSubcategorias');

//Catalogo de ticket
Route::get('catalogo/ticket/obtenerTicket','Catalogos\TicketController@obtenerTicket');

//correo
 Route::get('correo/index','CorreoController@index');
 Route::post('correo/enviar','CorreoController@sedEmail');

 //cierre turno
Route::get('cierre/index','CierreController@indexCierre');
 Route::post('cierre/ActualizarAsistencia','CierreController@updateBitacoraAsistenciaCorte');
Route::post('cierre/generarBitaroca','CierreController@updateBitacora');
Route::post('cierre/generarAsistencia','CierreController@updateAsistencia');
Route::post('cierre/generarFechaTrabajo','CierreController@updateNuevaFeechatrabajo');
Route::post('cierre/generarRespadlo','CierreController@generarRespaldo');
Route::post('cierre/generarSincroEmpleados','CierreController@generarSincroEmpleados');
Route::post('cierre/generarSincroCorte','CierreController@FungenerarSincroCorte');
Route::get('cierre/getFormasPago','CierreController@getFormasdepago');

 //Inventario
Route::get('Inventario/Index','InventarioController@ValidarOInsertarProductos');
Route::get('Inventario/Inventario/reporteDiario','InventarioController@IndexReporteDiario');
Route::get('Inventario/verificarInventarioVacio','InventarioController@FuncionVerificarRegistrosInventario');
Route::post('Inventario/Buscarproducto','InventarioController@FuncionBuscarProducto');
Route::post('Inventario/guardar/producto','InventarioController@FuncionGuardarProductosInventario');
Route::get('Inventario/mostrar/inventario/productos/agregadosenfecha','InventarioController@FuncionmostrarPedidosAgregados');
Route::get('Inventario/mostrar/inventario/productos/agregadosenfechaSalida','InventarioController@FuncionmostrarPedidosAgregadosSalidas');
Route::post('Inventario/modificar/producto/cantidad','InventarioController@FuncionModificarCantidadProducto');
Route::post('Inventario/modificar/producto/cantidadSalida','InventarioController@FuncionModificarCantidadProductoSalida');
Route::post('Inventario/eliminar/producto','InventarioController@FuncionEliminarProducto');
Route::post('Inventario/eliminar/productoSalida','InventarioController@FuncionEliminarProductoSalida');
Route::post('Inventario/buscar/productos/detalles','InventarioController@FuncionBuscarDetallesProductos');
Route::get('Inventario/index/capturarSalidas','InventarioController@IndexCapturarSalidas');
Route::post('Inventario/guardar/producto/salida','InventarioController@FuncionAgregarsalida');
Route::post('Inventario/guardar/productos/totales','InventarioController@FuncionGuardarTotales');
Route::post('Inventario/generar/pdf/reporte/inventario','InventarioController@FuncionGenerarReporteInventario');
Route::get('Inventario/Salidas/empleado/Index','InventarioController@indexCapturarSalidasporEmpleado');

 //Inventario acumulable de Roberto
 Route::post('Inventario/mostrar/inventario/productos/usuario/agregadosenfechaSalida','InventarioController@FuncionAgregarsalidaPorUsuario');
 Route::get('Inventario/guardar/producto/usuario/salida','InventarioController@FuncionmostrarPedidosAgregadosSalidasPorUsuario');
 Route::post('Inventario/finalizar/Inventario/caputar/inicial','InventarioController@FuncionFinalizarCapturaInicial');
 Route::get('Inventario/Verificar/Inventario/Capturado/fechaactual','InventarioController@FuncionVerificarInventarioCapturadoInicialEntradas');
 Route::post('Inventario/finalizar/Inventario/MermasDevoluciones/capturadas/fechaactual','InventarioController@FuncionVerificarInventarioCapturadoMermasEntradas');
 Route::get('Inventario/verificar/Inventario/MermasDevoluciones/capturadas/fechaactual','InventarioController@FuncionVerificarInventarioCapturadoMermasDevolucion');
 Route::get('Inventario/verificar/Inventario/Capturado/capturadas/fechaactual','InventarioController@FuncionFinalizarCapturadoSalidasUsuario');
 Route::post('Inventario/finalizar/Inventario/InventarioFinal/capturadas/fechaactual','InventarioController@FuncionInventarioFinalUsuario');
 Route::get('Inventario/verificar/Inventario/InventarioFinal/capturadas/fechaactual','InventarioController@FuncionVerificarInventarioFinalporUsuario');
 Route::get('Inventario/verificar/Inventario/reportes/capturar/finalizados','InventarioController@FuncionVerificarCapturarFinalizado');


 //Inventario modificaciones
 Route::get('inventario/Inicial/index','InventariosController@FunMostrarInicial');
 Route::get('inventario/Salida/index','InventariosController@FunMostrarSalidas');
 Route::get('inventario/Final/index','InventariosController@FunMostrarInventarioFinal');
 Route::get('inventario/ReporteInventario/index','InventariosController@FunMostrarReporteInventario');
 Route::post('inventario/Inicial/agregarProductos','InventariosController@FunAgregarProductos');
 Route::post('inventario/Inicial/ObtenerProductoInventarioDiario','InventariosController@FunProductosPorTipo');
 Route::post('inventario/Inicial/GuardarProductoModificado','InventariosController@FunModificarProducto');
 Route::post('inventario/Inicial/eliminarProducto','InventariosController@FunEliminarProducto');
 Route::post('inventario/Inicial/BuscarReporteInventario','InventariosController@FunBuscarReporteInventario');
 Route::post('inventario/Inicial/BuscarReporteInventarioPDF','InventariosController@FunBuscarReporteInventarioPDF');
 Route::post('inventario/Inicial/FunBuscarReporteInventarioEXCEL','InventariosController@FunBuscarReporteInventarioEXCEL');
 Route::post('inventario/Inicial/BuscarReporteInventarioGuardarTotales','InventariosController@FunBuscarReporteInventarioGuardarTotales');
 Route::post('inventario/Inicial/GuardarTotales','InventariosController@FunGuardarTotales');
 Route::get('inventario/Reportes/Generar/Index','InventariosController@FunGenerarReportesIndex');
 Route::post('inventario/Reportes/Generar/porTipo','InventariosController@FunGenerarReportePorTipo');
 Route::post('inventario/Inicial/reporte/PDF/movimientos','InventariosController@FunGenerarReporteMovimientosPDF');
 Route::get('inventario/privilegios/get','InventariosController@FunPrivilegios');
 Route::post('inventario/prepararDatos/envioadmintotal','InventariosController@PrepararAPIAdmintotal');
 Route::get('inventario/verificar/envioadmintotal','InventariosController@FunVerificarEnvioadmintotal');
 Route::post('inventario/GenerarEnviar/porCorreo','InventariosController@FunGenerarArchivosEnviarCorreo');


 Route::get('Inventario/requestTransaction','InventariosController@FunvistaPeticionesTraspaso');
 Route::get('Inventario/realizarTransaction','TraspasosController@FunVistaRealizarTraspaso');
 Route::get('Inventario/realizarTransaction','TraspasosController@FunVistaRealizarTraspaso');

 //Traspasos.
  Route::get('traspasos/get/centrosCostos','TraspasosController@FunObtenerCentrosCosto');
  Route::post('traspasos/post/dataAPIV','TraspasosController@FunGuardarPeticionCentroCostos');
  Route::get('traspasos/get/dataAPIV','TraspasosController@FunBuscarPeticionesSinFinalizar');
  Route::post('traspasos/post/dataAPIV/delete','TraspasosController@FuneliminarRegistro');
  Route::post('traspasos/post/dataAPIV/update','TraspasosController@FunFinalizarRegistros');

  //Correos
Route::get('correos/index','CorreosController@FunMostrarIndex');
Route::get('correos/getCorreos','CorreosController@FunObtenerCorreos');
Route::post('correos/NuevoCorreo','CorreosController@FunAgregarCorreo');
Route::post('correos/ModificarCorreos','CorreosController@FunActualizarCorreos');
Route::post('correos/EliminarCorreo','CorreosController@FunBorrarCorreos');

// Productos Primarios
Route::get('Catalogos/ProductosPrimarios/index','Catalogos\ProductosPrimariosController@VistaProductosPrimarios');
Route::post('Catalogos/ProductosPrimarios/agregarProductosPrimarios','Catalogos\ProductosPrimariosController@AgregarProductosPrimarios');
Route::get('Catalogos/ProductosPrimarios/obtenerDatos','Catalogos\ProductosPrimariosController@listaProdPrimarios');
Route::post('Catalogos/ProductosPrimarios/editarProductosPrimarios','Catalogos\ProductosPrimariosController@EditarProductosPrimarios');
Route::post('Catalogos/ProductosPrimarios/eliminarProductosPrimarios','Catalogos\ProductosPrimariosController@EliminarProductosPrimarios');

// Equivalencias
Route::get('Catalogos/Equivalencias/index','Catalogos\EquivalenciasController@VistaEquivalencias');
Route::get('Catalogos/Equivalencias/obtenerProductos','Catalogos\EquivalenciasController@ObtenerProductos');
Route::get('Catalogos/Equivalencias/obtenerProdPrimarios','Catalogos\EquivalenciasController@ObtenerProdPrimarios');
Route::post('Catalogos/Equivalencias/agregarEquivalencias','Catalogos\EquivalenciasController@AgregarEquivalencias');
Route::get('Catalogos/Equivalencias/obtenerEquivalencias','Catalogos\EquivalenciasController@ListaEquivalencias');
Route::post('Catalogos/Equivalencias/editarEquivalencias','Catalogos\EquivalenciasController@EditarEquivalencias');
Route::post('Catalogos/Equivalencias/eliminarEquivalencias','Catalogos\EquivalenciasController@EliminarEquivalencias');
Route::post('Catalogos/Equivalencias/generarExcel','Catalogos\EquivalenciasController@generarExcel');

// Reporte Equivalencias
Route::get('reportes/equivalencias', 'EquivalenciasReportController@index');
Route::get('reportes/equivalencias/obtenerProdPrimarios','EquivalenciasReportController@ObtenerProdPrimarios');
Route::post('reportes/equivalencias/generar', 'EquivalenciasReportController@GenerarReporte');

//Registro Ticket

Route::get('ticket/registro','RegistroTicketController@VistaTicket');
Route::post('ticket/registro/buscar','RegistroTicketController@buscarTicket');

// Justificaciones
Route::get('catalogos/justificaciones','Catalogos\JustificacionesController@VistaJustificaciones');
Route::get('catalogos/justificaciones/obtenerJustificaciones','Catalogos\JustificacionesController@ObtenerJustificaciones');
Route::post('catalogos/justificaciones/agregarJustificacion','Catalogos\JustificacionesController@AgregarJustificacion');
Route::post('catalogos/justificaciones/editarJustificacion','Catalogos\JustificacionesController@EditarJustificacion');
Route::post('catalogos/justificaciones/eliminarJustificacion','Catalogos\JustificacionesController@EliminarJustificacion');

Route::post('catalogos/justificaciones/agregarDescuento','Catalogos\JustificacionesController@AgregarDescuento');
Route::post('catalogos/justificaciones/editarDescuento','Catalogos\JustificacionesController@EditarDescuento');
Route::post('catalogos/justificaciones/eliminarDescuento','Catalogos\JustificacionesController@EliminarDescuento');

/* Rastrillo 2.0 */
Route::get('reportes/rastrillo', 'RastrilloController@index');
Route::get('reportes/rastrillo/catformaspago', 'RastrilloController@showCatFormasPago');
Route::post('reportes/rastrillo/generar','RastrilloController@generateReport');
Route::post('reportes/rastrillo/actualizar/{historialMesas}', 'RastrilloController@updateHistorialMesas');
Route::post('reportes/rastrillo/pdf','RastrilloController@exportPDF');
Route::post('reportes/rastrillo/resumen/pdf','RastrilloController@reporteResumenPDF');
Route::post('reportes/rastrillo/resumen/excel','RastrilloController@reporteResumenExcel');
});
