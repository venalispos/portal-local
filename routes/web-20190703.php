<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/','Auth\LoginController@showLoginForm')->middleware('guest')->name('/'); 


// Route::get('/', function () {
//     // session_start(); 
//     // if(isset($_SESSION['cEmpFolio'])){
//     //     return redirect('inicio.php');
//     // } else { 
//     //    // return redirect('login.php');
//     // 	echo "string";
//     // }
// });

Route::get('/','Auth\LoginController@showLoginForm')->middleware('guest')->name('/');
Route::post('login','Auth\LoginController@login')->name('login');
Route::post('logout','Auth\LoginController@logout')->name('logout');

Route::get('home','DashboardController@index')->name('home');
Route::get('reiniciar','DashboardController@reiniciarPassword')->name('reiniciar');

//Ayuda
Route::get('respaldo','RespaldoController@index')->name('respaldo'); 
Route::get('respaldo/generar','RespaldoController@generarRespaldo')->name('respaldo');
Route::get('respaldo/generar1','RespaldoController@generarRespaldo1')->name('respaldo');

//reportes  

Route::get('reportes/ticket','ReportesController@indexticket')->name('reporteticket');
Route::get('reportes/productos','ReportesController@indexproductos')->name('reporteProductos');
Route::get('reportes/Resumen','ResumenController@indexResumen')->name('reporteResumen');
Route::post('reportes/ticketGenerar','ReportesController@generarReporteTickets');
Route::post('reportes/generarPDFIticket','ReportesController@descargarPDFticket');
Route::post('reportes/productosGenerar','ReportesController@genRepProductos');
Route::post('reportes/generarPDFProd','ReportesController@pdfProductos');
Route::post('reportes/excelProductos','ReportesController@excelProductos');
Route::post('reportes/excelTicket','ReportesController@excelTicket'); 
Route::post('reportes/generarResumenrespuesta','ResumenController@generarResumenN');
Route::post('reportes/generarResumenPDF','ResumenController@generarResumenPDF');
Route::post('reportes/generarResumenExcel','ResumenController@generarResumenExcel'); 
Route::get('reportes/desgloseTP','ReportesController@indexDeglose');
Route::post('reportes/desgloseTP/generar','ReportesController@generarDesglose');
Route::post('reportes/desgloseTP/generarPDF','ReportesController@generarPDF');
Route::post('reportes/desgloseTP/generarEXCEL','ReportesController@generarDesgloseEXCEL');
Route::get('reportes/descuentos','ReportesController@indexDescuentos')->name('reporteDescuentos');
Route::post('reportes/descuentos/generar','ReportesController@generarDescuentos');

Route::get('reportes/Articulos','ReportesController@indexArticulo')->name('reporteArticulo');
Route::post('reportes/obtenerArticulos','ReportesController@obtenerArticulos');
Route::post('reportes/obtenerArticulosPDF','ReportesController@obtenerArticulosPDF');
Route::post('reportes/obtenerArticulosEXCEL','ReportesController@obtenerArticulosEXCEL');
Route::get('reportes/platillo','ReportesController@indexPlatillo')->name('reportePlatillo'); 
Route::post('reportes/platillo/generar','ReportesController@obtenerreportePlatillosGen');
Route::get('reportes/platillo/Categorias','ReportesController@obtenerCategorias'); 
Route::post('reportes/platillo/PDF','ReportesController@generarPDFplatillos');
Route::post('reportes/platillo/EXCEL','ReportesController@generarEXCELplatillos'); 



//menu
Route::get('menu/configuracion','MenuController@indexmenu')->name('menu');
Route::get('menu/configuracion/lista','MenuController@obtenerMenu')->name('getMenu');
Route::post('menu/nuevoItem','MenuController@nuevoMenu')->name('addMenu');
Route::post('menu/ActualizarMenu','MenuController@actualizarMenu')->name('MenuActualizar');
Route::post('menu/EliminarMenu','MenuController@eliminarMenu')->name('menuEliminar');

//privilegios
Route::get('Privilegios','PrivilegiosController@index')->name('privilegios');
Route::post('Privilegios/usuario','PrivilegiosController@buscarusuarios')->name('usuariosbuscar');

//Catalogos

	//empleados
	Route::get('catalogo/empleadosIndex','CatalogoController@indexEmpleados');
	Route::get('catalogo/getUserRoute','CatalogoController@getUser'); 
	Route::post('catalogo/shareEmploye','CatalogoController@addNewEmploye'); 
	Route::post('catalogo/updateEmployee','CatalogoController@updateEmployee'); 


	//Productos
	Route::get('catalogo/productoIndex','CatalogoController@IndexProductos'); 
	Route::get('catalogo/getProductos','CatalogoController@getProductos');  
	Route::post('catalogo/Validar_codigo_producto','CatalogoController@FunValidaProducto');  
	Route::post('producto/productobycategoriasubcategoria','InventarioController@getProductosPorCategoriaSubcategoria');  
	Route::get('producto/filtradoporinventario','InventarioController@getproductosfiltradoporinventario');  


//correo

 Route::get('correo/index','CorreoController@index'); 
 Route::post('correo/enviar','CorreoController@sedEmail');  

 //cierre turno

 Route::get('cierre/index','CierreController@indexCierre');
 Route::post('cierre/ActualizarAsistencia','CierreController@updateBitacoraAsistenciaCorte'); 
 Route::get('cierre/getFormasPago','CierreController@getFormasdepago'); 


 //PerfilesController

 Route::get('Perfiles/index','PerfilesController@indexPerfiles');

 //Inventario
 Route::get('Inventario/home','InventarioController@homeInventarioDiario');
 Route::get('Inventario/reporteDiario','InventarioController@indexreporteDiario');
 Route::post('Inventario/agregarproductosInventario','InventarioController@agregarproductoaInventario');
 Route::post('Inventario/eliminarproductodeInventario','InventarioController@deleteItemInventario');
 Route::get('Inventario/finalizarInventarioDiario','InventarioController@FinalizarInventarioDiario');
 Route::get('Inventario/verificarStatus','InventarioController@verificarInventarioDiarioFinalizado');
 Route::get('Inventario/obtenerreporteproductoinventario','InventarioController@obtenerProductosenInventario');
 Route::post('Inventario/obtenerreporteproductoinventarioPDF','InventarioController@obtenerProductosenInventarioPDF');
 Route::post('Inventario/obtenerreporteproductoinventarioEXCEL','InventarioController@obtenerProductosenInventarioEXCEL');
