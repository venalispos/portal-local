<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtipopago extends Model
{
    //
    protected $table = 'tSubtipopago'; //Nombre de la tabla
    protected $primaryKey = 'cidsubtipopago'; //Campo de la llave primaria
    public $timestamps  = false; //Desactiva registros de actualización/creación
    protected $fillable = ['cidtipopago', 'cnombresubtipopago', 'cestatus'];
    protected $casts = [
      'cidtipopago' => 'int',
      'cestatus' => 'int',
    ];

    public function Tipopago(){
      return $this-> hasOne('App\Tipopago', 'cidtipopago', 'cidtipopago');
    }
}
