<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merma extends Model
{
  protected $table = "tMerma";
	protected $primaryKey = 'cFolio';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cCategoria', 'cCantidad', 'cFechaRep'];
}
