<?php

namespace App\Configuracion;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
  protected $table = "tPerfiles";
	protected $primaryKey = 'cPerFolio';
  public $timestamps = false;
  protected $fillable = ['cPerId','cPerDesc','cPerAccesoClave','cPerAccion','cPerCategorias','cPerSubCategorias','cPerProductos','cPerReImpTicket','cPerAccesPortal','cPerLiquida','cPerAutorizaSalida','cPerRecibeCuentas','cPerServicio','cPerAutExtra','cPerAutDividir','cPerAutTransfer'];
  protected $casts = [
    'cConImpuesto1Cantidad' => 'float',
    'cConImpuesto2Cantidad' => 'float',
    'cConImpuesto3Cantidad' => 'float',
    'cConImpuesto4Cantidad' => 'float',
    ];
}
