<?php

namespace App\Configuracion;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
  protected $table = "tConfiguracion";
	protected $primaryKey = 'cConFolio';
  public $timestamps = false;
  protected $fillable = ['cConFechaVal','cConCorreo','cConPassCorreo','cConCorreoNotif1','cConCorreoNotif2','cConCorreoNotif3','cConCorreoNomina','cConNomTopeActivo','cConNomTopeHoras','cConNomLimiteJornada','cConDolarActivo','cConDolarParidad','cConPuntosActivo','cConPuntosPorcentaje','cConPuntoPago','cConLenguaje','cConVersionPOS','cConVersionAdmin','cConVersionSincro','cConBDTarjeta','cConBDNube','cConEmpresa','cConSucursal','cConEmpresaNominas','cConImpuesto1Activo','cConImpuesto1Nombre','cConImpuesto1Cantidad','cConImpuesto1Tipo','cConImpuesto2Activo','cConImpuesto2Nombre','cConImpuesto2Cantidad','cConImpuesto2Tipo','cConImpuesto3Activo','cConImpuesto3Nombre','cConImpuesto3Cantidad','cConImpuesto3Tipo','cConImpuesto4Activo','cConImpuesto4Nombre','cConImpuesto4Cantidad','cConImpuesto4Tipo','cConAutoRenta','cConReimpresion','cConMonoCuenta','cConSoloNumeroMesa','cConCostoPorcentaje','cConTicketCierre','cConTicketQR','cConFacturamos','cConModUsuarios','cConPedirCierre','cConPedirFondo','cConPorcentaje','cConVersionCheckit','cConAutorizaSalida','cConBanderaRespaldo','cConLlenarInventarioDiaro','cConMensajeImprimir','cConPorcentajeTiraX','cConBiometrico','cConTiempoBloqueo'];
  protected $casts = [
    'cConImpuesto1Cantidad' => 'float',
    'cConImpuesto2Cantidad' => 'float',
    'cConImpuesto3Cantidad' => 'float',
    'cConImpuesto4Cantidad' => 'float',
    ];
}
