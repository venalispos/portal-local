<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clasificacion extends Model
{
	protected $table = "tClasificacion";
	protected $primaryKey = 'cClaFolio';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cClaCodigo','cClaDescripcion'];
}
