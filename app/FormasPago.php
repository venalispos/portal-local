<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormasPago extends Model
{
    protected $table = "tCategoriaFormasPago";
	protected $primaryKey = 'cCatForFolio';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cCatForId', 'cCatForDesc', 'cCatForActivo', 'cCarForParidad', 'cCarForReportes', 'cCatForFolioAgrupador'];
}
      