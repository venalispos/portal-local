<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class HistorialTraspasos extends Model
{
  protected $table = "tHisTraspasos";
protected $primaryKey = 'cHistrasFolio';
public $timestamps = false;
public $fillable = ['cHistrasCodigoProducto','cHistrasCentroCosto','cHistrasTipo','cHistrasFechaRep','cHistrasFinalizado','cHistrasCantidad'];
}
