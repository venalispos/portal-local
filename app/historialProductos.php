<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class historialProductos extends Model
{
    protected $table = "tHisProductos";
    protected $primaryKey = 'cHisProFolio';
    protected $fillable = [
        'cHisProTicket',
        'cHisProEmpNum',
        'cHisProEmpNom',
        'cHisProItem',
        'cHisProModificador',
        'cHisProOrden',
        'cHisProComboCat',
        'cHisProCodigo',
        'cHisProAgrupador',
        'cHisProDesGeneral',
        'cHisProDesTicket',
        'cHisProCantidad',
        'cHisProPrecioUni',
        'cHisProPrecio',
        'cHisProCosto',
        'cHisProImpT1',
        'cHisProImp1',
        'cHisProImpT2',
        'cHisProImp2',
        'cHisProImpT3',
        'cHisProImp3',
        'cHisProImpT4',
        'cHisProImp4',
        'cHisProSubTotal',
        'cHisProCategoria',
        'cHisProSubCategoria',
        'cHisProClasificacion',
        'cHisProPuntos',
        'cHisProFechaHoraEntrada',
        'cHisProCortesia',
        'cHisProCancel',
        'cHisProDescuentos',
        'cHisProDescCombo',
        'cHisProAutoriza',
        'cHisProSerieCap',
        'cHisProSerieLiq',
        'cHisProVisible',
        'cHisProFechaHoraSalida',
        'cHisProCajeroNum',
        'cHisProCajeroNom',
        'cHisProFechaRep',
        'cHisProNumLealtad',
        'cHisProSincro',
        'cHisProTurno',
        'cHisProTOGO',
        'cHisProPlataforma',
    ];

    protected $casts = [
        'cHisProTicket' => 'integer',
        'cHisProEmpNum' => 'integer',
        'cHisProItem' => 'integer',
        'cHisProModificador' => 'integer',
        'cHisProOrden' => 'integer',
        'cHisProComboCat' => 'integer',
        'cHisProCodigo' => 'integer',
        'cHisProAgrupador' => 'integer',
        'cHisProCantidad' => 'float',
        'cHisProPrecioUni' => 'float',
        'cHisProPrecio' => 'float',
        'cHisProCosto' => 'float',
        'cHisProImpT1' => 'integer',
        'cHisProImp1' => 'float',
        'cHisProImpT2' => 'integer',
        'cHisProImp2' => 'float',
        'cHisProImpT3' => 'integer',
        'cHisProImp4' => 'float',
        'cHisProSubTotal' => 'float',
        'cHisProCategoria' => 'integer',
        'cHisProSubCategoria' => 'integer',
        'cHisProClasificacion' => 'integer',
        'cHisProPuntos' => 'integer',
        'cHisProCortesia' => 'float',
        'cHisProCancel' => 'float',
        'cHisProDescuentos' => 'float',
        'cHisProDescCombo' => 'float',
        'cHisProVisible' => 'integer',
        'cHisProCajeroNum' => 'integer',
        'cHisProFechaRep' => 'integer',
        'cHisProSincro' => 'integer',
        'cHisProTurno' => 'integer',
        'cHisProTOGO' => 'integer',
        'cHisProPlataforma' => 'integer',
    ];

    public function Mesa()
    {
      return $this->hasOne(historialMesas::class, 'cHisMesTicket', 'cHisProTicket');
    }

    public function categorias(){
        return $this->hasOne(Categorias::class, 'cCatCodigo', 'cHisProCategoria' );
    }
}

/*

,
 */
