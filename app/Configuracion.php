<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
	protected $table = "tConfiguracion";
	protected $primaryKey = 'cConFolio';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cConFechaVal', 'cConCorreo', 'cConPassCorreo', 'cConCorreoNotif1', 'cConCorreoNotif2', 'cConCorreoNotif3', 'cConCorreoNomina', 'cConNomTopeActivo', 'cConNomTopeHoras', 'cConNomLimiteJornada',
                         'cConDolarActivo', 'cConDolarParidad', 'cConPuntosActivo', 'cConPuntosPorcentaje', 'cConPuntoPago', 'cConLenguaje', 'cConVersionPOS', 'cConVersionAdmin', 'cConVersionSincro', 'cConBDTarjeta', 'cConBDNube', 'cConEmpresa',
                         'cConSucursal', 'cConEmpresaNominas', 'cConImpuesto1Activo', 'cConImpuesto1Nombre', 'cConImpuesto1Cantidad', 'cConImpuesto1Tipo', 'cConImpuesto2Activo', 'cConImpuesto2Nombre', 'cConImpuesto2Cantidad', 'cConImpuesto2Tipo',
                         'cConImpuesto3Activo', 'cConImpuesto3Nombre', 'cConImpuesto3Cantidad', 'cConImpuesto3Tipo', 'cConImpuesto4Activo', 'cConImpuesto4Nombre', 'cConImpuesto4Cantidad', 'cConImpuesto4Tipo', 'cConAutoRenta', 'cConReimpresion',
                        'cConMonoCuenta', 'cConSoloNumeroMesa', 'cConCostoPorcentaje', 'cConTicketCierre', 'cConFacturamos', 'cConTicketQR', 'cConCatEmpleado', 'cConModUsuarios', 'cConPedirCierre', 'cConPedirFondo', 'cConBanderaRespaldo', 'cConLlenarInventarioDiaro',
                        'cConMensajeImprimir', 'cConPorcentajeTiraX', 'cConPorcentaje', 'cConVersionCheckit', 'cConAutorizaSalida', 'cConTicketQR', 'cConBiometrico', 'cConTiempoBloqueo', 'cConInventarioInicial', 'cConTicketAsistencia', 'cConAutorizaDividir',
                    'cConAutorizaTransfer', 'cConAutorizaExtra', 'cConSalidaTipObligatorio', 'cConPortalIdioma', 'cConMensajeFactura', 'cConFacturaAutomatica', 'cConTipoCorreo', 'cConConfirmaLiquida', 'cConRegaloFlexible', 'cConNotiRetiroParcial',
                         'cConIdentificadorAdmintotal', 'cConRevisaEntradaInventario', 'cConMensajeTicket', 'cConSincronizaProductos', 'cConPantallaCliente', 'cConPantCliAncho', 'cConPantCliAlto', 'cConPantCliPosX', 'cConPantCliPosY', 'cConImpComandas',
                         'cConSegmentosCant', 'cConSincronizarcliente','cConInventarioCodigoInterno','cConBanderaCierre'

];
    }
