<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class historialMesas extends Model
{
    protected $table = "tHisMesas";
    protected $primaryKey = 'cHisMesFolio';
    public $timestamps = false;

    protected $fillable = [
        'cHisMesTicket',
        'cHisMesNombre',
        'cHisMesSegmento',
        'cHisMesEmpleadoFolio',
        'cHisMesEmpleadoNombre',
        'cHisMesCantClientes',
        'cHisMesSerieCap',
        'cHisMesSerieLiq',
        'cHisMesFechaEntrada',
        'cHisMesFechaSalida',
        'cHisMesFechaRep',
        'cHisMesCajeroFolio',
        'cHisMesCajeroNombre',
        'cHisMesSubTotal',
        'cHisMesTotalImpuesto1',
        'cHisMesTotalImpuesto2',
        'cHisMesTotalImpuesto3',
        'cHisMesTotalImpuesto4',
        'cHisMesTotal',
        'cHisMesFacturado',
        'cHisMesFactSoli',
        'cHisMesNumLealtad',
        'cHisMesSincro',
        'cHisMesSincroInterfaz',
        'cHisMesCodigoFacturacion',
        'cHisMesTurno',
        'cHisMesCodCliente',
        'cHisMesTOGO',
        'cHisMesPlataforma',
        'cHisMesVisible',
        'cHisMesPagado',
        'cHisMesDiferido',
    ];


    protected $casts = [
        'cHisMesTicket' => 'integer',
        'cHisMesSegmento' => 'integer',
        'cHisMesEmpleadoFolio' => 'integer',
        'cHisMesCantClientes' => 'integer',
        'cHisMesFechaRep' => 'integer',
        'cHisMesCajeroFolio' => 'integer',
        'cHisMesSubTotal' => 'float',
        'cHisMesTotalImpuesto1' => 'float',
        'cHisMesTotalImpuesto2' => 'float',
        'cHisMesTotalImpuesto3' => 'float',
        'cHisMesTotalImpuesto4' => 'float',
        'cHisMesTotal' => 'float',
        'cHisMesPuntos' => 'float',
        'cHisMesFacturado' => 'integer',
        'cHisMesFactSoli' => 'integer',
        'cHisMesSincro' => 'integer',
        'cHisMesSincroInterfaz' => 'integer',
        'cHisMesTurno' => 'integer',
        'cHisMesCodCliente' => 'integer',
        'cHisMesTOGO' => 'integer',
        'cHisMesPlataforma' => 'integer',
        'cHisMesVisible' => 'integer',
        'cHisMesPagado' => 'integer',
        'cHisMesDiferido' => 'integer',
    ];

    public function formaspago() {
        return $this->hasMany(HistorialFormasPagos::class, 'cHisForTicket', 'cHisMesTicket');
    }
}
