<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    protected $table = "tRegAsistencia";
	protected $primaryKey = 'cRegFolio';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cRegIdEmp', 'cRegNombre', 'cRegFechaRep', 'cRegFechaEntrada', 'cRegFechaIniDes', 'cRegFechaFinDes', 'cRegFechaSalida','cRegBandera', 'cRegHorasJornada', 'cRegHorasDescanso', 'cRegHorasTrabajadas','cRegPuesto', 'cRegPerfil', 'cRegTips', 'cRegCCTips', 'cRegExtras', 'cRegSincro'];
}
