<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cortes extends Model
{
  protected $table = 'tCortes';
  protected $primaryKey = 'cCorFolio';
  protected $dates = [
        'cCorFecha',
    ];

  protected $casts = [
        'cCorTotalVenta' => 'float',
        'cCorTipoCambio' => 'float',
        'cCorIniTicket' => 'integer',
        'cCorFinTicket' => 'integer',
    ];
  protected $fillable =[
    'cCorFecha',
    'cCorTotalVenta',
    'cCorTipoCambio',
    'cCorIniTicket',
    'cCorFinTicket',
  ];
}
