<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialFormasPagos extends Model
{
    protected $table = "tHisFormaPago";
    protected $primaryKey = 'cHisForFolio';
    protected $fillable = [
        'cHisForId',
        'cHisForDes',
        'cHisForCant',
        'cHisForTipo',
        'cHisForTicket',
        'cHisForIdEmp',
        'cHisForNomEmp',
        'cHisForIdIngresa',
        'cHisForNomIngresa',
        'cHisForFechaIngreso',
        'cHisForFechaRep',
        'cHisForSincro',
        'cHisForTurno',
        'cHisForBanRegio',
        'cHisForTipoTarjeta',
        'cHisForDiferido',
    ];

    protected $casts = [
        'cHisForId' => 'integer',
        'cHisForCant' => 'float',
        'cHisForTipo' => 'integer',
        'cHisForTicket' => 'integer',
        'cHisForIdEmp' => 'integer',
        'cHisForIdIngresa' => 'integer',
        'cHisForFechaRep' => 'integer',
        'cHisForSincro' => 'integer',
        'cHisForTurno' => 'integer',
        'cHisForTipoTarjeta' => 'integer',
        'cHisForDiferido' => 'integer',
    ];

    public function Mesa()
    {
      return $this->hasOne(historialMesas::class, 'cHisMesTicket', 'cHisForTicket');
    }

    public function categoriaformapago(){
        return $this->hasOne(categoriasFormaPago::class, 'cCatForId', 'cHisForId');
    }
}
