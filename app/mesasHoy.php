<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mesasHoy extends Model
{
     protected $table = "tHoyMesas";
	protected $primaryKey = 'cHoyMesTicket';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
protected $fillable = ['cHoyMesNombre', 'cHoyMesSegmento', 'cHoyMesEmpleadoFolio', 'cHoyMesEmpleadoNombre', 'cHoyMesFechaEntrada', 'cHoyMesCantClientes', 'cHoyMesSerie', 'cHoyMesEstatus', 'cHoyMesTransfer','cHoyMesImpresa', 'cHoyMesServicio', 'cHoyMesAuto'];
}
