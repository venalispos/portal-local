<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalleprecierre extends Model
{
    //
    protected $table = 'tDetalleprecierre'; //Nombre de la tabla
    protected $primaryKey = 'ciddetalleprecierre'; //Campo de la llave primaria
    public $timestamps  = false; //Desactiva registros de actualización/creación
    protected $fillable = ['cidtipopago', 'cdenominacion', 'ccantidad', 'ctotal', 'cidsubtipopago', 'cnoreferencia', 'cidprecierre'];
    protected $casts = [
      'cidtipopago' => 'int',
      'cdenominacion' => 'float',
      'ccantidad' => 'int',
      'ctotal' => 'float',
      'cidsubtipopago' => 'int',
      'cidprecierre' => 'int',
    ];

    public function Tipopago(){
      return $this-> hasOne('App\Tipopago', 'cidtipopago', 'cidtipopago');
    }
    public function Subtipopago(){
      return $this-> hasOne('App\Subtipopago', 'cidsubtipopago', 'cidsubtipopago');
    }
    public function Precierre(){
      return $this-> hasOne('App\Precierre', 'cidprecierre', 'cidprecierre');
    }
}
