<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categoriasFormaPago extends Model
{
    protected $table = "tCategoriaFormasPago";
    protected $primaryKey = 'cCatForFolio';
    public $timestamps = false;
    protected $fillable = [
        'cCatForId',
        'cCatForDesc',
        'cCatForActivo',
        'cCarForParidad',
        'cCarForReportes',
        'cCatForFolioAgrupador',
        'cCatForTipoTarjeta',
        'cCatForFacturarAdmintotal',
        'cCatForAgrupadorCorte',
        'cCatForSincroniza',
        'cCatForPlataforma',
        'cCatForDiferido'
    ];

    protected $casts = [
        'cCatForId' => 'integer',
        'cCatForActivo' => 'integer',
        'cCarForParidad' => 'float',
        'cCarForReportes' => 'integer',
        'cCatForFolioAgrupador' => 'integer',
        'cCatForTipoTarjeta' => 'integer',
        'cCatForFacturarAdmintotal' => 'integer',
        'cCatForAgrupadorCorte' => 'integer',
        'cCatForSincroniza' => 'integer',
        'cCatForPlataforma' => 'integer',
    ];


    public function agrupadorformapago(){
        return $this->hasOne(AgrupadorFormaPago::class, 'cAgForId', 'cCatForAgrupadorCorte');
    }


    

}
