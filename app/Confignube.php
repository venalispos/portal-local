<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confignube extends Model
{
    //
    protected $table = 'tConfigNube'; //Nombre de la tabla
    protected $primaryKey = 'cFolio'; //Campo de la llave primaria
    public $timestamps  = false; //Desactiva registros de actualización/creación
    protected $fillable = ['cBDSincroActiva', 'cBDHost', 'cBDUser', 'cBDPass', 'cIDTienda', 'cBDName', 'cDBHostAdminTotal', 'cDbAdminTotalAlmacen',
    'cBDAppPedidos', 'cBDAppCheckIt', 'cBDBitacora', 'cBDIndicadores', 'cBDPuestos', 'cBDProductosNubeToLocal', 'cBDRegistrarFacturas', 'cBDMesasAbiertasNube',
    'cBDContenidoMesasAbiertasNube', 'cBDMesasNube', 'cBDProductosNube', 'cBDRegAsistencia', 'cBDAdminTotal', 'cBDSincronizarCorte', 'cBDEmpleados',
    'cDBQUerys', 'cDBFormasPago', 'cDBDescuentos', 'cDBNotificaciones', 'cBDTerminales'];

}
