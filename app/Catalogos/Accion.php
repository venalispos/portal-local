<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Accion extends Model
{
  protected $table = "tAccion";
	protected $primaryKey = 'cAccFolio';
  public $timestamps = false;
  protected $fillable= ['cAccId', 'cAccDes', 'cAccIcono', 'cAccTexto', 'cAccNotas', 'cAccOrden'];
}
