<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
  protected $table = "tTicket";
	protected $primaryKey = 'cTicFolio';
  protected $fillable= ['cTicAncho', 'cTicCantCaracteres', 'cTicLetra', 'cTicPropSugerida', 'cTicMensajeA1', 'cTicMensajeA2', 'cTicMensajeA3', 'cTicMensajeA4', 'cTicMensajeA5', 'cTicMensajeA6', 'cTicMensajeA7', 'cTicMensajeA8', 'cTicMensajeB1', 'cTicMensajeB2', 'cTicMensajeB3', 'cTicMensajeB4', 'cTicMensajeB5', 'cTicMensajeE1', 'cTicMensajeE2', 'cTicMensajeE3', 'cTicCantCantidad', 'cTicCantPrecio'];
}
