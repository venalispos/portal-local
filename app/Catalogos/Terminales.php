<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Terminales extends Model
{
  protected $table = "tTerminales";
	protected $primaryKey = 'cTerFolio';
  public $timestamps = false;
  protected $fillable= ['cTerSerie','cTerImpBarra','cTerImpCocina','cTerImpTickets','cTerLiquida','cTerIdTeamViewer','cTerDescargas'];
}
