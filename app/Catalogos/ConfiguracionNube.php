<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionNube extends Model
{
  protected $table = "tConfigNube";
	protected $primaryKey = 'cFolio';
  public $timestamps = false;
  protected $fillable= ['cBDSincroActiva','cBDHost', 'cBDUser', 'cBDPass', 'cIDTienda', 'cBDName', 'cDBHostAdminTotal',
                        'cDbAdminTotalAlmacen', 'BDAppPedidos', 'cBDAppCheckIt', 'cBDBitacora', 'cBDIndicadores',
                      'cBDPuestos', 'cBDProductosNubeToLocal', 'cBDRegistrarFacturas', 'cBDMesasAbiertasNube',
                      'cBDContenidoMesasAbiertasNube', 'cBDMesasNube', 'cBDProductosNube', 'cBDRegAsistencia',
                      'cBDAdminTotal', 'cBDSincronizarCorte', 'cBDEmpleados', 'cDBQUerys'];
}
