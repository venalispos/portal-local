<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class BotSubCategorias extends Model
{
  protected $table = "tBotSubCategorias";
	protected $primaryKey = 'cBotSubCatFolio';
  public $timestamps = false;
  protected $fillable= ['cBotSubCatDescripcion','cBotSubCatIdPadre'];

  public function BotCategoria() {
    return $this -> hasOne('App\Catalogos\BotCategorias', 'cBotCatFolio','cBotSubCatIdPadre');
  }
}
