<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class DescuentosAutorizados extends Model
{
  protected $table = "tDescuentosAutorizados";
	protected $primaryKey = 'cDescFolio';
  public $timestamps = false;
  protected $fillable= ['cDescIdPadre', 'cDescDescripcion', 'cDescCantidad'];
  protected $casts = [
    'cDescIdPadre' => 'int',
    'cDescCantidad' => 'float',
  ];
}
