<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class ProductosPrimarios extends Model
{
  protected $table = "tProductosPrimarios";
	protected $primaryKey = 'cProPriFolio';
  public $timestamps = false;
  protected $fillable= ['cProPriDescripcion', 'cProPriDetEquivalencia'];
  protected $casts = [
    // 'ProPriCodigo' => 'int',
  ];

/* public function Producto() {
  return $this -> hasOne('App\Catalogos\Productos', 'cProCodigo','cProPriCodigo');
} */

}
