<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Justificaciones extends Model
{
  protected $table = "tJustificaciones";
	protected $primaryKey = 'cJusFolio';
  public $timestamps = false;
  protected $fillable= ['cJusID', 'cJusCategoria', 'cJusDescripcion'];
  protected $casts = [
    'cJusID' => 'int',
    'cJusCategoria' => 'int',
  ];

  public function Descuentos() {
    return $this -> hasMany('App\Catalogos\DescuentosAutorizados', 'cDescIdPadre','cJusID');
  }

}
