<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class BotProductos extends Model
{
  protected $table = "tBotProductos";
	protected $primaryKey = 'cBotProFolio';
  public $timestamps = false;
  protected $fillable= ['cBotProIdPadre','cBotProIdProd'];

  public function Producto() {
    return $this -> hasOne('App\Catalogos\Productos', 'cProCodigo','cBotProIdProd')->select(array('cProCodigo', 'cProDescripcion'));
  }
  public function SubCategoria() {
    return $this -> hasOne('App\Catalogos\BotSubCategorias', 'cBotSubCatFolio','cBotProIdPadre')->select(array('cBotSubCatFolio', 'cBotSubCatDescripcion'));
  }

}
