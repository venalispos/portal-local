<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Clasificacion extends Model
{
  protected $table = "tClasificacion";
	protected $primaryKey = 'cClaFolio';
  protected $fillable= ['cClaCodigo', 'cClaDescripcion'];
}
