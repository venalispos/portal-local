<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class ComProductos extends Model
{
  protected $table = "tComProductos";
	protected $primaryKey = 'cComProdFolio';
  public $timestamps = false;
  protected $fillable= ['cComProdIdPadre','cComProdIdProd'];

  public function Producto() {
    return $this -> hasOne('App\Catalogos\Productos', 'cProCodigo','cComProdIdProd')->select(array('cProCodigo', 'cProDescripcion'));
  }
  public function SubCategoria() {
    return $this -> hasOne('App\Catalogos\ComSubCategorias', 'cComSubCatFolio','cComProdIdPadre')->select(array('cComSubCatFolio', 'cComSubCatDescripcion'));
  }
}
