<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Perfiles extends Model
{
  protected $table = "tPerfiles";
	protected $primaryKey = 'cPerFolio';
  public $timestamps = false;
  protected $fillable= ['cPerId', 'cPerDesc', 'cPerAccesoClave', 'cPerAccion', 'cPerCategorias', 'cPerSubCategorias', 'cPerProductos', 'cPerReImpTicket', 'cPerAccesPortal', 'cPerLiquida', 'cPerAutorizaSalida', 'cPerRecibeCuentas', 'cPerServicio', 'cPerAutExtra', 'cPerAutDividir', 'cPerAutTransfer','cPerInventario'];

  protected $casts = [
    'cPerAutorizaSalida' => 'int',
    'cPerLiquida' => 'int',
    'cPerReImpTicket' => 'int',
    'cPerRecibeCuentas' => 'int',
    'cPerServicio' => 'int',
    'cPerAccesoClave' => 'int',
    'cPerAutDividir' => 'int',
    'cPerAutExtra' => 'int',
    'cPerAutTransfer' => 'int',
    'cPerAutorizaSalida' => 'int',
    'cPerCategorias' => 'string',
    'cPerSubCategorias' => 'string',
    'cPerProductos' => 'string',
    ];
}
