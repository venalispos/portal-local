<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class BotCategorias extends Model
{
  protected $table = "tBotCategorias";
	protected $primaryKey = 'cBotCatFolio';
  public $timestamps = false;
  protected $fillable= ['cBotCatDescripcion'];
}
