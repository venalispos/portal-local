<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class ComSubCategorias extends Model
{
  protected $table = "tComSubCategorias";
  public $timestamps = false;
	protected $primaryKey = 'cComSubCatFolio';
  protected $fillable= ['cComSubCatDescripcion','cComSubCatIdPadre'];

  public function Categoria() {
    return $this -> hasOne('App\Catalogos\ComCategorias', 'cComCatFolio','cComSubCatIdPadre');
  }
}
