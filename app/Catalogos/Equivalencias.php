<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Equivalencias extends Model
{
  protected $table = "tEquivalencias";
	protected $primaryKey = 'cEquivaFolio';
  public $timestamps = false;
  protected $fillable= ['cFolioProPri', 'cCodigoProd', 'cEquivalencia'];
  protected $casts = [
    'cCodigoProd' => 'int',
    'cFolioProPri' => 'int',
    'cEquivalencia' => 'float',
  ];

  public function Producto() {
    return $this -> hasOne('App\Catalogos\Productos', 'cProCodigo','cCodigoProd');
  }

  public function ProductoPrimario() {
    return $this -> hasOne('App\Catalogos\ProductosPrimarios', 'cProPriFolio','cFolioProPri');
  }

}
