<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class ComCategorias extends Model
{
  protected $table = "tComCategorias";
	protected $primaryKey = 'cComCatFolio';
  public $timestamps = false;
  protected $fillable= ['cComCatDesc'];
}
