<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class SubCategorias extends Model
{
  protected $table = "tSubCategorias";
	protected $primaryKey = 'cSCatFolio';
  public $timestamps = false;
  protected $fillable= ['cSCatDescripcion','cSCatPadre','cSCatCodigo'];

  public function Categoria() {
    return $this -> hasOne('App\Catalogos\Categorias', 'cCatCodigo','cSCatPadre');
  }
}
