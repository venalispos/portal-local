<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
  protected $table = "tCategorias";
  public $timestamps = false;
	protected $primaryKey = 'cCatFolio';
  protected $fillable= ['cCatCodigo', 'cCatDescripcion'];
}
