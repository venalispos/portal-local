<?php

namespace App\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
  protected $table = "tProductos";
	protected $primaryKey = 'cProFolio';
  public $timestamps = false;
  protected $fillable= ['cProInterCodigo','cProCodigo','cProDescripcion','cProDesBoton','cProDesTicket','cProPrecio','cProCosto','cProImpuesto1','cProImpuesto2','cProImpuesto3','cProImpuesto4','cProCategoria','cProSubCategoria','cProClasificacion','cProImprime','cProActivo','cProVisible','cProAutoModi','cProMod','cProModCant','cCombo','cProComPrecio','cComCat1','cComCat1Cant','cComCat2','cComCat2Cant','cComCat3','cComCat3Cant','cProPuntos','cProFechaInicio','cProFechaFin','cProFraccion','A0','P0','I0','F0','A1','P1','I1','F1','A2','P2','I2','F2','A3','P3','I3','F3','A4','P4','I4','F4','A5','P5','I5','F5','A6','P6','I6','F6','cComCat4','cComCat4Cant'];
  protected $casts = [
    'cProCosto' => 'float',
    'cProComPrecio' => 'float',
    'cProImpuesto1' => 'int',
    'cProImpuesto2' => 'int',
    'cProImpuesto3' => 'int',
    'cProImpuesto4' => 'int',
    'cProDescripcion' => 'varchar',
    'A0' => 'int',
    'A1' => 'int',
    'A2' => 'int',
    'A3' => 'int',
    'A4' => 'int',
    'A5' => 'int',
    'A6' => 'int',
    'P0' => 'float',
    'P1' => 'float',
    'P2' => 'float',
    'P3' => 'float',
    'P4' => 'float',
    'P5' => 'float',
    'P6' => 'float',
    'cProMod' => 'int',
    'cProVisible' => 'int',
    'cProSubCategoria' => 'int',
    'cProActivo' => 'int',
    'cProAutoModi' => 'int',
    'cProCategoria' => 'int',
    'cComCat1' => 'int',
    'cComCat2' => 'int',
    'cComCat3' => 'int',
    'cComCat4' => 'int',
    'cCombo' => 'int',
    ];
public function Categoria() {
  return $this -> hasOne('App\Catalogos\Categorias', 'cCatCodigo','cProCategoria');
}
public function SubCategoria() {
  return $this -> hasOne('App\Catalogos\SubCategorias', 'cSCatCodigo','cProSubCategoria');
}

}
