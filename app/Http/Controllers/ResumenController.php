<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\historialMesas;
use App\HistorialFormasPagos;
use App\categoriasFormaPago;
use DB;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\UsersExport;

class ResumenController extends Controller
{
	public function indexResumen()
	{
		return view('reportes/reporteResumen');
	}


	public function generarResumenN(Request $request)
	{
		$oHisMesas = null;
		$fInicio = str_replace("-", "", $request['inicio']);
		$fTermino = str_replace("-", "", $request['termino']);
		$igual = app(Configuracion\ConfiguracionController::class)
			->obtenerCorteFecha($fInicio, $fTermino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {


			$HistorialMesa = historialMesas::selectRaw('COALESCE(SUM(cHisMesTotal), 0) as Efectivo, COALESCE(SUM(cHisMesSubTotal), 0) as Subtotal, COALESCE(SUM(cHisMesTotalImpuesto1), 0) as Impuesto, COALESCE(SUM(cHisMesTotalImpuesto2), 0) as Impuesto2, COALESCE(SUM(cHisMesTotalImpuesto3), 0) as Impuesto3, COALESCE(SUM(cHisMesTotalImpuesto4), 0) as Impuesto4')
				->whereBetween('cHisMesFechaRep', [$fInicio, $fTermino])->first();

			$CatFormasPago = categoriasFormaPago::select('cCatForId', 'cCatForDesc')
				->where('cCatForActivo', 1)
				->where('cCarForReportes', 1)
				->get();

			$tMonto = 0;
			foreach ($CatFormasPago as $formapago) {
				$formapago->Historial = HistorialFormasPagos::selectRaw('COALESCE(SUM(cHisForCant), 0) as monto')
					->whereBetween('cHisForFechaRep', [$fInicio, $fTermino])
					->where('cHisForId', $formapago->cCatForId)->first();
				$tMonto += $formapago->Historial->monto;
			}


			 $Efectivo = $HistorialMesa->Efectivo - $tMonto;


			$tIva = null;
			$tSubtotal = null;
			$tCortesias = null;
			$tDescuentos = null;
			$tCancelaciones = null;
			$tMonto = null;
			$tTotales = null;


			$oHisMesas = DB::raw("SELECT cHisProSerieLiq, cCatDescripcion,SUM(tHisProductos.cHisProSubTotal)as Subtotal,SUM(tHisProductos.cHisProCortesia)as Cortesias,SUM(tHisProductos.cHisProDescuentos)as Descuento,SUM(tHisProductos.cHisProCancel)as Cancelaciones,SUM(cHisProImp1) as Iva  FROM tHisProductos  INNER JOIN tCategorias ON  cHisProCategoria = cCatCodigo  AND cHisProFechaRep BETWEEN '" . $fInicio . "' AND '" . $fTermino . "' group By cHisProSerieLiq,cCatDescripcion");

			$oHisMesas = db::select($oHisMesas);

			$oHisMesas = json_decode(json_encode($oHisMesas), true);


			$oHisMesasTotales = DB::raw("SELECT SUM(tHisProductos.cHisProSubTotal)as Subtotal,SUM(tHisProductos.cHisProCortesia)as Cortesias,SUM(tHisProductos.cHisProDescuentos)as Descuento,SUM(tHisProductos.cHisProCancel)as Cancelaciones,SUM(cHisProImp1) as Iva  FROM tHisProductos INNER JOIN tCategorias ON cHisProCategoria =cCatCodigo  AND cHisProFechaRep BETWEEN '" . $fInicio . "' AND '" . $fTermino . "' ");
			$oHisMesasTotales = db::select($oHisMesasTotales);

			$oHisMesasTotales = json_decode(json_encode($oHisMesasTotales), true);


			foreach ($oHisMesasTotales as $key) {
				$tIva += floatval($key['Iva']);
				$tSubtotal += floatval($key['Subtotal']);
				$tCortesias += floatval($key['Cortesias']);
				$tDescuentos += floatval($key['Descuento']);
				$tCancelaciones += floatval($key['Cancelaciones']);
			}


			$oHisPagos = HistorialFormasPagos::selectRaw('SUM(cHisForCant)as monto,cCatForDesc')
				->Join('tCategoriaFormasPago', 'cHisForId', 'tCategoriaFormasPago.cCatForId')
				->whereBetween('cHisForFechaRep', [$fInicio, $fTermino])
				->where('cCarForReportes', '1')
				->groupBy('cCatForDesc')
				->get();

			$oHisPagosTotal = HistorialFormasPagos::selectRaw('SUM(cHisForCant)as monto')
				->Join('tCategoriaFormasPago', 'cHisForId', 'tCategoriaFormasPago.cCatForId')
				->whereBetween('cHisForFechaRep', [$fInicio, $fTermino])
				->where('cCarForReportes', '1')
				->get()
				->toArray();


			foreach ($oHisPagosTotal as $key) {
				$tMonto += floatval($key['monto']);
			}

			if ($oHisPagosTotal	== []) {
				$oHisMesas = null;
			}
			$totalR = null;
			$TotalRes = DB::raw("SELECT cCatDescripcion,(Iva+Subtotal)as Total,cHisProSerieLiq,cHisProSerieLiq
					FROM(SELECT tCategorias.cCatDescripcion,cHisProSerieLiq,
					tCategorias.cCatFolio,
					SUM(tHisProductos.cHisProSubTotal)as Subtotal,
					SUM(cHisProImp1) as Iva
					from tHisProductos			INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria
					where cHisProFechaRep BETWEEN '" . $fInicio . "' and '" . $fTermino . "'
					group By cHisProSerieLiq, cHisProCategoria,tCategorias.cCatDescripcion,tCategorias.cCatFolio) AS TABLA1");
			$totalR = db::select($TotalRes);

			$totalR = json_decode(json_encode($totalR), true);
			foreach ($totalR as $key) {
				$tTotales += $key['Total'];
			}
			return [
				'Efectivo' => $Efectivo, 
				'HistorialCategorias' => $oHisMesas,
				'HistorialFormasPagos' => $oHisPagos,
				'Iva' => $tIva,
				'Subtotal' => $tSubtotal,
				'Cortesias' => $tCortesias,
				'Descuentos' => $tDescuentos,
				'Cancelaciones' => $tCancelaciones,
				'Monto' => $tMonto + $Efectivo,
				'totalResumen' => $totalR,
				'totales' => $tTotales
			];
		}
	}
	public function generarResumenPDF(Request $request)
	{
		$oHisMesas = null;
		$fInicio = str_replace("-", "", $request['inicio']);
		$fTermino = str_replace("-", "", $request['termino']);

		$igual = app(Configuracion\ConfiguracionController::class)
			->obtenerCorteFecha($fInicio, $fTermino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
			$tIva = null;
			$tSubtotal = null;
			$tCortesias = null;
			$tDescuentos = null;
			$tCancelaciones = null;
			$tMonto = null;
			$tTotales = null;


			$HistorialMesa = historialMesas::selectRaw('COALESCE(SUM(cHisMesTotal), 0) as Efectivo, COALESCE(SUM(cHisMesSubTotal), 0) as Subtotal, COALESCE(SUM(cHisMesTotalImpuesto1), 0) as Impuesto, COALESCE(SUM(cHisMesTotalImpuesto2), 0) as Impuesto2, COALESCE(SUM(cHisMesTotalImpuesto3), 0) as Impuesto3, COALESCE(SUM(cHisMesTotalImpuesto4), 0) as Impuesto4')
				->whereBetween('cHisMesFechaRep', [$fInicio, $fTermino])->first();

			$CatFormasPago = categoriasFormaPago::select('cCatForId', 'cCatForDesc')
				->where('cCatForActivo', 1)
				->where('cCarForReportes', 1)
				->get();

			$cMonto = 0;
			foreach ($CatFormasPago as $formapago) {
				$formapago->Historial = HistorialFormasPagos::selectRaw('COALESCE(SUM(cHisForCant), 0) as monto')
					->whereBetween('cHisForFechaRep', [$fInicio, $fTermino])
					->where('cHisForId', $formapago->cCatForId)->first();
				$cMonto += $formapago->Historial->monto;
			}


			 $Efectivo = $HistorialMesa->Efectivo - $cMonto;


			$oHisMesas = DB::raw("SELECT cHisProSerieLiq,cCatDescripcion,SUM(tHisProductos.cHisProSubTotal)as Subtotal,SUM(tHisProductos.cHisProCortesia)as Cortesias,SUM(tHisProductos.cHisProDescuentos)as Descuento,SUM(tHisProductos.cHisProCancel)as Cancelaciones,SUM(cHisProImp1) as Iva  FROM tHisProductos  INNER JOIN tCategorias ON  cHisProCategoria = cCatCodigo  AND cHisProFechaRep BETWEEN '" . $fInicio . "' AND '" . $fTermino . "' group By cHisProSerieLiq,cCatDescripcion");

			$oHisMesas = db::select($oHisMesas);

			$oHisMesas = json_decode(json_encode($oHisMesas), true);


			$oHisMesasTotales = DB::raw("SELECT SUM(tHisProductos.cHisProSubTotal)as Subtotal,SUM(tHisProductos.cHisProCortesia)as Cortesias,SUM(tHisProductos.cHisProDescuentos)as Descuento,SUM(tHisProductos.cHisProCancel)as Cancelaciones,SUM(cHisProImp1) as Iva  FROM tHisProductos INNER JOIN tCategorias ON cHisProCategoria =cCatCodigo  AND cHisProFechaRep BETWEEN '" . $fInicio . "' AND '" . $fTermino . "'");

			$oHisMesasTotales = db::select($oHisMesasTotales);

			$oHisMesasTotales = json_decode(json_encode($oHisMesasTotales), true);


			foreach ($oHisMesasTotales as $key) {
				$tIva += floatval($key['Iva']);
				$tSubtotal += floatval($key['Subtotal']);
				$tCortesias += floatval($key['Cortesias']);
				$tDescuentos += floatval($key['Descuento']);
				$tCancelaciones += floatval($key['Cancelaciones']);
			}


			$oHisPagos = HistorialFormasPagos::selectRaw('SUM(cHisForCant)as monto,cCatForDesc')
				->Join('tCategoriaFormasPago', 'cHisForId', 'tCategoriaFormasPago.cCatForId')
				->whereBetween('cHisForFechaRep', [$fInicio, $fTermino])
				->where('cCarForReportes', '1')
				->groupBy('cCatForDesc')
				->get();

			$oHisPagosTotal = HistorialFormasPagos::selectRaw('SUM(cHisForCant)as monto')
				->Join('tCategoriaFormasPago', 'cHisForId', 'tCategoriaFormasPago.cCatForId')
				->whereBetween('cHisForFechaRep', [$fInicio, $fTermino])
				->where('cCarForReportes', '1')
				->get()
				->toArray();


			foreach ($oHisPagosTotal as $key) {
				$tMonto += floatval($key['monto']);
			}

			if ($oHisPagosTotal	== []) {
				$oHisMesas = null;
			}
			$totalR = null;
			$TotalRes = DB::raw("SELECT cCatDescripcion,(Iva+Subtotal)as Total,cHisProSerieLiq
					FROM(SELECT tCategorias.cCatDescripcion,cHisProSerieLiq,
					tCategorias.cCatFolio,
					SUM(tHisProductos.cHisProSubTotal)as Subtotal,
					SUM(cHisProImp1) as Iva
					from tHisProductos			INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria
					where cHisProFechaRep BETWEEN '" . $fInicio . "' and '" . $fTermino . "'
					group by cHisProSerieLiq,cHisProCategoria,tCategorias.cCatDescripcion,tCategorias.cCatFolio) AS TABLA1");
			$totalR = db::select($TotalRes);

			$totalR = json_decode(json_encode($totalR), true);
			foreach ($totalR as $key) {
				$tTotales += $key['Total'];
			}

			$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
			$empresaSel = db::select($datosempresa);
			$empresaSel = json_decode(json_encode($empresaSel), true);

			$info_encabezado = [
				'fecha' => $request->inicio . " - " . $request->termino,
				'empresaSeleccionada' => $empresaSel
			];

			$dataGPDF = [
				'Efectivo' => $Efectivo, 
				'HistorialCategorias' => $oHisMesas,
				'HistorialFormasPagos' => $oHisPagos,
				'Iva' => $tIva,
				'Subtotal' => $tSubtotal,
				'Cortesias' => $tCortesias,
				'Descuentos' => $tDescuentos,
				'Cancelaciones' => $tCancelaciones,
				'Monto' => $tMonto + $Efectivo ,
				'totalResumen' => $totalR,
				'totales' => $tTotales
			];



			$header = view()->make('/reportes/reporteResumenPDF/header')->with('datosencabezado', $info_encabezado)->render();
			$pdf = PDF::loadview('/reportes/reporteResumenPDF/body', compact('dataGPDF'));
			$pdf->setOption('header-html', $header)
				->setOption('header-right', 'PAGINA [page] DE [toPage]')
				->setOption('header-font-name', 'Courier')
				->setOption('header-font-size', '8')
				->setOption('margin-top', 38)
				->setOption('margin-bottom', 10)
				->setOption('margin-left', 10)
				->setOption('margin-right', 10);
			return $pdf->download('reporte.pdf');
		}
	}

	public function generarResumenExcel(Request $request)
	{
		$oHisMesas = null;
		$fInicio = str_replace("-", "", $request['inicio']);
		$fTermino = str_replace("-", "", $request['termino']);

		$igual = app(Configuracion\ConfiguracionController::class)
			->obtenerCorteFecha($fInicio, $fTermino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
			$tIva = null;
			$tSubtotal = null;
			$tCortesias = null;
			$tDescuentos = null;
			$tCancelaciones = null;
			$tMonto = null;
			$tTotales = null;


			$HistorialMesa = historialMesas::selectRaw('COALESCE(SUM(cHisMesTotal), 0) as Efectivo, COALESCE(SUM(cHisMesSubTotal), 0) as Subtotal, COALESCE(SUM(cHisMesTotalImpuesto1), 0) as Impuesto, COALESCE(SUM(cHisMesTotalImpuesto2), 0) as Impuesto2, COALESCE(SUM(cHisMesTotalImpuesto3), 0) as Impuesto3, COALESCE(SUM(cHisMesTotalImpuesto4), 0) as Impuesto4')
				->whereBetween('cHisMesFechaRep', [$fInicio, $fTermino])->first();

			$CatFormasPago = categoriasFormaPago::select('cCatForId', 'cCatForDesc')
				->where('cCatForActivo', 1)
				->where('cCarForReportes', 1)
				->get();

			$tMonto = 0;
			foreach ($CatFormasPago as $formapago) {
				$formapago->Historial = HistorialFormasPagos::selectRaw('COALESCE(SUM(cHisForCant), 0) as monto')
					->whereBetween('cHisForFechaRep', [$fInicio, $fTermino])
					->where('cHisForId', $formapago->cCatForId)->first();
				$tMonto += $formapago->Historial->monto;
			}


			 $Efectivo = $HistorialMesa->Efectivo - $tMonto;

			$oHisMesas = DB::raw("SELECT cHisProSerieLiq,cCatDescripcion,SUM(tHisProductos.cHisProSubTotal)as Subtotal,SUM(tHisProductos.cHisProCortesia)as Cortesias,SUM(tHisProductos.cHisProDescuentos)as Descuento,SUM(tHisProductos.cHisProCancel)as Cancelaciones,SUM(cHisProImp1) as Iva  FROM tHisProductos  INNER JOIN tCategorias ON  cHisProCategoria = cCatCodigo  AND cHisProFechaRep BETWEEN '" . $fInicio . "' AND '" . $fTermino . "' group By cHisProSerieLiq,cCatDescripcion");

			$oHisMesas = db::select($oHisMesas);

			$oHisMesas = json_decode(json_encode($oHisMesas), true);


			$oHisMesasTotales = DB::raw("SELECT SUM(tHisProductos.cHisProSubTotal)as Subtotal,SUM(tHisProductos.cHisProCortesia)as Cortesias,SUM(tHisProductos.cHisProDescuentos)as Descuento,SUM(tHisProductos.cHisProCancel)as Cancelaciones,SUM(cHisProImp1) as Iva  FROM tHisProductos INNER JOIN tCategorias ON cHisProCategoria =cCatCodigo  AND cHisProFechaRep BETWEEN '" . $fInicio . "' AND '" . $fTermino . "'");

			$oHisMesasTotales = db::select($oHisMesasTotales);

			$oHisMesasTotales = json_decode(json_encode($oHisMesasTotales), true);


			foreach ($oHisMesasTotales as $key) {
				$tIva += floatval($key['Iva']);
				$tSubtotal += floatval($key['Subtotal']);
				$tCortesias += floatval($key['Cortesias']);
				$tDescuentos += floatval($key['Descuento']);
				$tCancelaciones += floatval($key['Cancelaciones']);
			}


			$oHisPagos = HistorialFormasPagos::selectRaw('SUM(cHisForCant)as monto,cCatForDesc')
				->Join('tCategoriaFormasPago', 'cHisForId', 'tCategoriaFormasPago.cCatForId')
				->whereBetween('cHisForFechaRep', [$fInicio, $fTermino])
				->where('cCarForReportes', '1')
				->groupBy('cCatForDesc')
				->get();

			$oHisPagosTotal = HistorialFormasPagos::selectRaw('SUM(cHisForCant)as monto')
				->Join('tCategoriaFormasPago', 'cHisForId', 'tCategoriaFormasPago.cCatForId')
				->whereBetween('cHisForFechaRep', [$fInicio, $fTermino])
				->where('cCarForReportes', '1')
				->get()
				->toArray();


			foreach ($oHisPagosTotal as $key) {
				$tMonto += floatval($key['monto']);
			}

			if ($oHisPagosTotal	== []) {
				$oHisMesas = null;
			}
			$totalR = null;
			$TotalRes = DB::raw("SELECT cCatDescripcion,(Iva+Subtotal)as Total,cHisProSerieLiq
					FROM(SELECT tCategorias.cCatDescripcion,cHisProSerieLiq,
					tCategorias.cCatFolio,
					SUM(tHisProductos.cHisProSubTotal)as Subtotal,
					SUM(cHisProImp1) as Iva
					from tHisProductos			INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria
					where cHisProFechaRep BETWEEN '" . $fInicio . "' and '" . $fTermino . "'
					group by cHisProSerieLiq,cHisProCategoria,tCategorias.cCatDescripcion,tCategorias.cCatFolio) AS TABLA1");
			$totalR = db::select($TotalRes);

			$totalR = json_decode(json_encode($totalR), true);
			foreach ($totalR as $key) {
				$tTotales += $key['Total'];
			}

			$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
			$empresaSel = db::select($datosempresa);
			$empresaSel = json_decode(json_encode($empresaSel), true);

			$info_encabezado = [
				'fecha' => $request->inicio . " - " . $request->termino,
				'empresaSeleccionada' => $empresaSel
			];

			$dataGPDF = [
				'Efectivo' => $Efectivo, 
				'HistorialCategorias' => $oHisMesas,
				'HistorialFormasPagos' => $oHisPagos,
				'Iva' => $tIva,
				'Subtotal' => $tSubtotal,
				'Cortesias' => $tCortesias,
				'Descuentos' => $tDescuentos,
				'Cancelaciones' => $tCancelaciones,
				'Monto' => $tMonto,
				'totalResumen' => $totalR,
				'totales' => $tTotales
			];



			Excel::create('ventas', function ($excel) use (
				$info_encabezado,
				$dataGPDF
			) {

				$excel->sheet('ventas', function ($sheet) use (
					$info_encabezado,
					$dataGPDF
				) {

					$sheet->loadView("reportes.reporteExcelResumen.reporteExcelResumen")
						->with(compact(
							'info_encabezado',
							'dataGPDF'
						));
				});
			})->download('csv');
		}
	}
}
