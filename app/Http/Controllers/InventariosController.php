<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\InventarioDiario;
use App\Configuracion;
use App\Productos;
use App\historialProductos;
use App\Perfiles;
use App\Correos;
use App\TotaCorreoslesInventario;
use PDF;
use Mail;
use Maatwebsite\Excel\Facades\Excel;

class InventariosController extends Controller
{


    public function FunMostrarInicial(){
      return view("Inventario.inicial");
    }

    public function verificarHorario()
    {
      $Configuracion = Configuracion::select('cConHorarioInventario')->first();
      $date = Carbon::now();
      $Hora = $date->toTimeString();
      if (!$Configuracion->cConHorarioInventario) {
        return 1;
      }
      if ($Hora < $Configuracion->cConHorarioInventario ) {
        return 1;
      }
      return 0;
    }

    public function FunPrivilegios(){

      $_perfil_1 = auth()->user()->cUsuPuesto1Perfil;
      $_perfil_1_Activo = auth()->user()->cUsuPuesto1Activo;
      $_perfil_2 = auth()->user()->cUsuPuesto2Perfil;
      $_perfil_2_Activo = auth()->user()->cUsuPuesto2Activo;
      $_perfil_3 = auth()->user()->cUsuPuesto3Perfil;
      $_perfil_3_Activo = auth()->user()->cUsuPuesto3Activo;
      $_perfilesActivos = NULL;
      if($_perfil_1_Activo == 1)
        $_perfilesActivos = $_perfilesActivos.$_perfil_1.",";
      if($_perfil_2_Activo == 1)
        $_perfilesActivos = $_perfilesActivos.$_perfil_2.",";
      if($_perfil_3_Activo == 1)
        $_perfilesActivos = $_perfilesActivos.$_perfil_3.",";

      $_perfilesActuales = explode(",", $_perfilesActivos);

      $Acciones=Perfiles::select('cPerInventario')->whereIn('cPerId',$_perfilesActuales)->get()->toArray();
      $AccionesInventarioString=NULL;
      foreach($Acciones AS $key){
        $AccionesInventarioString.=$key['cPerInventario'].",";
      }
      $AccionesInventarioString = substr($AccionesInventarioString,0,-1);
      $AccionesInventarioString = explode(",", $AccionesInventarioString);
    $AccionesInventarioString = array_unique($AccionesInventarioString);
    return $AccionesInventarioString;
    }

    public function FunAgregarProductos(Request $request){
      $boExisteInicial= false;
      $boExisteEntrada= false;
      $cantidad=$request->cantidad;
      $codigo=$request->codigo;
      $date = Carbon::now();
      $Fecha = Carbon::now();
  		$dateFormat=$Fecha->format('Y-m-d h:m:s');
      $date = $date->format('Ymd');
      $usuario= auth()->user()->cUsuCodigo;
      $productos =Productos::select('cProCodigo','cProPrecio')->where('cProinterCodigo',$codigo)->first();
      if ($productos == []) {
        return 1;
      }
      else
      {
      if ($request->tipo != 1 ) {
      $existeInicial =DB::raw("SELECT ISNULL((SELECT cInvVentasTotal FROM tInventarioDiarioTotales WHERE  cInvTFechaRep = (
		  SELECT MAX(cInvTFechaRep)
		  FROM tInventarioDiarioTotales WHERE cInvTFechaRep <".$date.")  AND cInvTCodigo=".$productos->cProCodigo."),0) AS 'cInvVentasTotal';");
        $existeInicial=db::select($existeInicial);
        $existeInicial=json_decode(json_encode($existeInicial),true);
        if ($existeInicial == []) {
          $boExisteInicial=false;
        }else{
          $boExisteInicial=true;
        }
        $existeentrada=InventarioDiario::where('cInvCodigoProducto',$productos->cProCodigo)->where('cInvFechaRep',$date)->get()->toArray();
        if ($existeentrada == []) {
          $boExisteEntrada =false;
        }else{
          $boExisteEntrada =true;
        }
        if ($boExisteEntrada ==false && $boExisteInicial == false) {
          return 3;
        }else{
          $limitarProductos=DB::raw("

		IF EXISTS (SELECT SUM(cInvCantidad) FROM tInventarioDiario WHERE  cInvFechaRep =".$date."  AND cInvCodigoProducto=".$productos->cProCodigo." GROUP BY cInvStatuS)
		  BEGIN
		     select (inicial+entrada)-(mermas+devoluciones+final) AS 'cantidad' from (
		  SELECT inicial,SUM(entrada) as 'entrada',SUM(mermas) as 'mermas',SUM(devoluciones) as 'devoluciones',SUM(final) as 'final' FROM (
		  SELECT  CASE WHEN cInvStatus= 1 then SUM(cantidad) ELSE  0 END AS 'entrada',
		  CASE WHEN cInvStatus= 2 then SUM(cantidad) ELSE  0 END AS 'mermas',
		  CASE WHEN cInvStatus= 3 then SUM(cantidad) ELSE  0 END AS 'devoluciones',
		  CASE WHEN cInvStatus= 4 then SUM(cantidad) ELSE  0 END AS 'final',inicial FROM (

		  SELECT ISNULL((SELECT cInvTInicial FROM tInventarioDiarioTotales WHERE  cInvTFechaRep = (
		  SELECT MAX(cInvTFechaRep)

		  FROM tInventarioDiarioTotales WHERE cInvTFechaRep <=".$date.")  AND cInvTCodigo=".$productos->cProCodigo."),0) AS 'inicial',ISNULL(SUM(cInvCantidad),0) AS 'cantidad',cInvStatus
		  FROM tInventarioDiario WHERE cInvCodigoProducto=".$productos->cProCodigo."  AND cInvFechaRep=".$date."  GROUP BY cInvStatus,cInvCodigoProducto
          )AS TB1 group by cInvStatus,inicial)AS TB2 group by inicial)AS TB3

		   END ELSE BEGIN
		   	SELECT ISNULL((SELECT cInvTInicial FROM tInventarioDiarioTotales WHERE  cInvTFechaRep = (
		  SELECT MAX(cInvTFechaRep)
		  FROM tInventarioDiarioTotales WHERE cInvTFechaRep <=".$date.")  AND cInvTCodigo=".$productos->cProCodigo."),0) AS 'cantidad'
		    END

");

          $limitarProductos=db::select($limitarProductos);
          $limitarProductos=json_decode(json_encode($limitarProductos),true);
          if ($limitarProductos != []) {
            if ($limitarProductos[0]["cantidad"] >= $request->cantidad) {
            //  InventarioDiario::create(['cInvFechaRep'=>$date,'cInvCodigoProducto'=>$productos[0]['cProCodigo'],'cInvCantidad'=>$request->cantidad,'tInvIniCostoPiezaProd'=>$productos[0]['cProPrecio'],'cInvCodigoUsuario'=>$usuario,'cInvFechaRegistro'=>$dateFormat,'cInvStatus'=>$request->tipo,'cInvComentario'=>$request->comentario]);
              $Fecha = Carbon::now();
             $formatoFecha=$Fecha->format('Y-m-d h:m:s');

              $insert = new InventarioDiario;

              $insert->cInvCantidad = $request->cantidad;
              $insert->cInvCodigoProducto = $productos['cProCodigo'];
              $insert->cInvCodigoUsuario = $usuario;
              $insert->cInvComentario = $request->comentario;
              $insert->cInvFechaRegistro = $formatoFecha;
              $insert->cInvFechaRep = $date;
              $insert->cInvStatus = $request->tipo;
              $insert->tInvIniCostoPiezaProd = $productos['cProPrecio'];

              $insert->save();

              return 0;
            }else{
              return 4;
            }
          }else{
              return 4;
          }

          return 0;
        }
      }
      else{
        $permitir =  $this->verificarHorario();
        if (!$permitir) {
          return 1;
        }
        $Fecha = Carbon::now();
    		$formatoFecha=$Fecha->format('Y-m-d h:m:s');

        $insert = new InventarioDiario;

        $insert->cInvCantidad = $request->cantidad;
        $insert->cInvCodigoProducto = $productos['cProCodigo'];
        $insert->cInvCodigoUsuario = $usuario;
        $insert->cInvComentario = $request->comentario;
        $insert->cInvFechaRegistro = $formatoFecha;
        $insert->cInvFechaRep = $date;
        $insert->cInvStatus = $request->tipo;
        $insert->tInvIniCostoPiezaProd = $productos['cProPrecio'];

        $insert->save();
        //$insert = InventarioDiario::create(['cInvFechaRep'=>$date,'cInvCodigoProducto'=>$productos[0]['cProCodigo'],'cInvCantidad'=>$request->cantidad,'tInvIniCostoPiezaProd'=>$productos[0]['cProPrecio'],'cInvCodigoUsuario'=>$usuario,'cInvFechaRegistro'=>$dateFormat,'cInvStatus'=>$request->tipo,'cInvComentario'=>$request->comentario]);

        return 0;
        }
        }
    }

    public function FunProductosPorTipo( Request $request){
      $date = Carbon::now();
  		$dateFormat=$date->format('Y-m-d h:m:s');
      $date = $date->format('Ymd');
      $dataTipo= array();
      $dataTipo=explode(",", $request->tipo);
      $InventarioEntradas= InventarioDiario::selectRaw('cProInterCodigo,cProDesTicket,SUM(cInvCantidad) as cantidad,0 as nuevaCantidad,cInvStatus')
      ->where('cInvFechaRep',$date)
      ->Join('tproductos','cProCodigo','cInvCodigoProducto')
      ->whereIn('cInvStatus',$dataTipo)
      ->groupBy('cProInterCodigo','cProDesTicket','cInvStatus')
      ->get()
      ->toArray();
      return $InventarioEntradas;
    }

    public function FunModificarProducto(Request $request){
      if ($request['tipo']!=4) {
        $permitir =  $this->verificarHorario();
        if (!$permitir) {
          return 1;
        }
      }
      $date = Carbon::now();
  		$dateFormat=$date->format('Y-m-d h:m:s');
      $date = $date->format('Ymd');
      $usuario= auth()->user()->cUsuCodigo;
      $productos =Productos::where('cProinterCodigo',$request['producto']['cProInterCodigo'] )->get()->toArray();
      InventarioDiario::where('cInvFechaRep',$date)->where('cInvCodigoProducto',$productos[0]['cProCodigo'])->where('cInvStatus',$request['producto']['cInvStatus'])->delete();
      $insert = new InventarioDiario;

      $insert->cInvCantidad = $request['producto']['nuevaCantidad'];
      $insert->cInvCodigoProducto = $productos[0]['cProCodigo'];
      $insert->cInvCodigoUsuario = $usuario;
      $insert->cInvFechaRegistro = $dateFormat;
      $insert->cInvFechaRep = $date;
      $insert->cInvStatus = $request->tipo;
      $insert->tInvIniCostoPiezaProd = $productos[0]['cProPrecio'];

      $insert->save();
    }

    public function FunEliminarProducto(Request $request){
      if ($request['tipo']!=4) {
        $permitir =  $this->verificarHorario();
        if (!$permitir) {
          return 1;
        }
      }
      $date = Carbon::now();
      $dateFormat=$date->format('Y-m-d h:m:s');
      $date = $date->format('Ymd');
      $usuario= auth()->user()->cUsuCodigo;
      $productos =Productos::where('cProinterCodigo',$request['producto']['cProInterCodigo'] )->get()->toArray();
      InventarioDiario::where('cInvFechaRep',$date)->where('cInvCodigoProducto',$productos[0]['cProCodigo'])->where('cInvStatus',$request['producto']['cInvStatus'])->delete();
    }

    public function FunMostrarSalidas(){
      return view("Inventario.MermasDevoluciones");
    }

    public function FunMostrarInventarioFinal(){
      return view("Inventario.InventarioFinal");
    }

    public function FunMostrarReporteInventario(){
      return view("Inventario.Index");
    }

    public function FunBuscarReporteInventario(Request $request){

      $fecha=$request['fecha'];
      $date =carbon:: parse($fecha)->format('Ymd');
      $totalventaTeorica =0;
      $totalventaTeoricaCantidad =0;
      $totalventaReal =0;
      $totalventaRealCantidad =0;
      $totalDiferenciaPlatillosMonto=0;
      $dateAnterior=0;
      $informacionInventario=null;
      $dataProductos=null;
      $datosInventario=array();
      $totalEntrada = null;
      $totalMermas = null;
      $totalDevoluciones = null;
      $totalFinal = null;
      $totalInicial = null;
      $vSComprobarFecha=InventarioDiario::selectRaw("MAX(cInvFechaRep)	 AS 'fechaRep'")->where('cInvFechaRep','<',$date)->get()->toArray();
      if ($vSComprobarFecha[0]['fechaRep'] ==[]) {

        $dataProductos=Productos::selectRaw("DISTINCT  0 as cProInicial,cProCodigo,cProInterCodigo,cProDesTicket,0 as cProFinal, 0 as cProentrada,0 as cProSalidaD,0 as cProSalidaM,0 as cProVenta,cProPrecio")
        ->Join('tInventarioDiario','cInvCodigoProducto','cProCodigo')
        ->where('cCombo',0)
        ->get()->toArray();
        $dataInventarioDiario= InventarioDiario::where('cInvFechaRep',$date)->get()->toArray();
        $VentasCapturadas=historialProductos::selectRaw("SUM(cHisProCantidad) as cHisProCantidad,cProInterCodigo")
        ->where('cHisProFechaRep',$date)
        ->groupBy('cHisProCodigo','cProInterCodigo')
        ->Join('tProductos','cHisProCodigo','cProCodigo')->get()->toArray();

        foreach ($dataProductos as $key ) {
          foreach ($dataInventarioDiario as $inventario) {
          if ($key['cProCodigo'] == $inventario['cInvCodigoProducto']) {
            if ($inventario['cInvStatus'] == "1") {
              $key['cProentrada'] += floatval($inventario['cInvCantidad']);
            }else
            if ($inventario['cInvStatus'] == "2") {
              $key['cProSalidaM'] += floatval($inventario['cInvCantidad']);
            }else
            if ($inventario['cInvStatus'] == "3") {
              $key['cProSalidaD'] += floatval($inventario['cInvCantidad']);
            }else
            if ($inventario['cInvStatus'] == "4") {
              $key['cProFinal'] += floatval($inventario['cInvCantidad']);
            }
          }
          }
          $totalEntrada += $key['cProentrada'];
          $totalMermas += $key['cProSalidaM'];
          $totalDevoluciones += $key['cProSalidaD'];
          $totalFinal += $key['cProFinal'];
          $totalInicial += 0;
          $totalventaReal +=(floatval($key['cProentrada'])-floatval($key['cProSalidaM'])-floatval($key['cProSalidaD'])-floatval($key['cProFinal']))*$key['cProPrecio'];
          $totalventaRealCantidad +=(floatval($key['cProentrada'])-floatval($key['cProSalidaM'])-floatval($key['cProSalidaD'])-floatval($key['cProFinal']));
          foreach ($VentasCapturadas as $ventas) {
            if ($key['cProInterCodigo'] ==$ventas['cProInterCodigo']   ) {
              $key['cProVenta'] += $ventas['cHisProCantidad'];
                $totalventaTeorica += floatval($ventas['cHisProCantidad'])*floatval($key['cProPrecio']);
                $totalventaTeoricaCantidad += floatval($ventas['cHisProCantidad']);

            }
          }
          $totalDiferenciaPlatillosMonto += (floatval($key['cProVenta'])-(floatval($key['cProentrada'] + floatval(  $key['cProInicial'] ))-floatval($key['cProSalidaM'])-floatval($key['cProSalidaD'])-floatval($key['cProFinal'])))*$key['cProPrecio'];

        //  $totalDiferenciaPlatillosMonto += floatval($key['cProVenta'])-(floatval($key['cProentrada'])-floatval($key['cProSalidaM'])-floatval($key['cProSalidaD'])-floatval($key['cProFinal']))*$key['cProPrecio'];
          array_push($datosInventario, $key);
        }

      }else{

          $dateAnterior=$vSComprobarFecha[0]['fechaRep'];
    			$totalVentaDiaAnterio=DB::raw("SELECT cInvTCodigo as codigoProducto,(cInvVentasTotal) AS 'total' FROM  tInventarioDiarioTotales WHERE cInvTFechaRep=".$dateAnterior." AND cInvVentasTotal >0 group by cInvTCodigo,cInvTInicial,cInvTEntradas,cInvTSalidasMermas,cInvTSalidasDevoluciones,cInvVentasTotal;");
          $_vDatosDiaAnterior=db::select($totalVentaDiaAnterio);
          $_vDatosDiaAnterior=json_decode(json_encode($_vDatosDiaAnterior),true);

          $dataProductos =   DB::raw("SELECT DISTINCT  0 as cProInicial,cProCodigo,cProInterCodigo,cProDesTicket,0 as cProFinal, 0 as cProentrada,0 as cProSalidaD,0 as cProSalidaM,0 as cProVenta,cProPrecio FROM tProductos JOIN tInventarioDiario ON cProCodigo =cInvCodigoProducto WHERE cCombo= 0 AND cProCodigo IN((SELECT cInvTCodigo FROM tInventarioDiarioTotales WHERE cInvTFechaRep=".$dateAnterior." AND  cInvVentasTotal > 0)) OR cProCodigo IN((SELECT cInvCodigoProducto FROM tInventarioDiario WHERE cInvFechaRep > ".$dateAnterior.")) ");
          $dataProductos=db::select($dataProductos);
          $dataProductos=json_decode(json_encode($dataProductos),true);
            $dataInventarioDiario= InventarioDiario::where('cInvFechaRep',$date)->get()->toArray();
            $VentasCapturadas=historialProductos::selectRaw("SUM(cHisProCantidad) as cHisProCantidad,cProInterCodigo")
            ->where('cHisProFechaRep',$date)
            ->groupBy('cHisProCodigo','cProInterCodigo')
            ->Join('tProductos','cHisProCodigo','cProCodigo')->get()->toArray();

            foreach ($dataProductos as $key ) {

              foreach ($dataInventarioDiario as $inventario) {
                if ($key['cProCodigo'] == $inventario['cInvCodigoProducto']) {
                    if ($inventario['cInvStatus'] == "1") {
                      $key['cProentrada'] += floatval($inventario['cInvCantidad']);
                    }else
                    if ($inventario['cInvStatus'] == "2") {
                      $key['cProSalidaM'] += floatval($inventario['cInvCantidad']);
                    }else
                    if ($inventario['cInvStatus'] == "3") {
                      $key['cProSalidaD'] += floatval($inventario['cInvCantidad']);
                    }else
                    if ($inventario['cInvStatus'] == "4") {
                      $key['cProFinal'] += floatval($inventario['cInvCantidad']);
                    }
                }

              }
              $totalEntrada += $key['cProentrada'];
              $totalMermas += $key['cProSalidaM'];
              $totalDevoluciones += $key['cProSalidaD'];
              $totalFinal += $key['cProFinal'];

              $totalventaReal +=(floatval($key['cProentrada'])-floatval($key['cProSalidaM'])-floatval($key['cProSalidaD'])-floatval($key['cProFinal']))*$key['cProPrecio'];
              $totalventaRealCantidad +=(floatval($key['cProentrada'])-floatval($key['cProSalidaM'])-floatval($key['cProSalidaD'])-floatval($key['cProFinal']));
              foreach ($VentasCapturadas as $ventas) {
                if ($key['cProInterCodigo'] ==$ventas['cProInterCodigo']   ) {
                  $key['cProVenta'] += floatval($ventas['cHisProCantidad']);
                  $totalventaTeorica += floatval($ventas['cHisProCantidad'])*floatval($key['cProPrecio']);
                  $totalventaTeoricaCantidad += floatval($ventas['cHisProCantidad']);
                }
              }

              foreach ($_vDatosDiaAnterior as $invTo) {
                if ($key['cProCodigo'] == $invTo['codigoProducto']) {
                  $key['cProInicial'] += floatval($invTo['total']);
                  $totalventaReal += floatval( $key['cProInicial'])*floatval($key['cProPrecio']);
                  $totalventaRealCantidad +=  $key['cProInicial'];
                }
              }
                $totalInicial += $key['cProInicial'];
              $totalDiferenciaPlatillosMonto += (floatval($key['cProVenta'])-(floatval($key['cProentrada'] + floatval(  $key['cProInicial'] ))-floatval($key['cProSalidaM'])-floatval($key['cProSalidaD'])-floatval($key['cProFinal'])))*$key['cProPrecio'];
              array_push($datosInventario, $key);
            }
      }
     return ['datosInventario'=>$datosInventario,'totalventa'=>$totalventaTeorica,'totalVentareal'=> $totalventaTeoricaCantidad ,'totalventaCantidad'=>$totalventaRealCantidad,'totalventateoricacantidad'=>$totalventaReal,'montototal'=>$totalDiferenciaPlatillosMonto,'totalInicial'=>$totalInicial,'totalEntrada'=>$totalEntrada,'totalMerma'=>$totalMermas,'totalDevolucion'=>$totalDevoluciones,'totalFinal'=>$totalFinal];
    }

    public function FunGuardarTotales(Request $request){
      $productos=$request->data;
          $fecha=$request['fecha'];
      $date = Carbon::parse($fecha)->format('Ymd');
        $eliminarRegistros="DELETE tInventarioDiarioTotales WHERE cInvTFechaRep =".$date;
        $Queryinsert="INSERT INTO tInventarioDiarioTotales (cInvTFechaRep,cInvTInicial,cInvTCodigo,cInvTEntradas,cInvTSalidasMermas,cInvTSalidasDevoluciones,cInvVentasTotal,cInvTDiferencias )VALUES";
        foreach($productos as $key)
        {
          $VentaCapturada=((floatval($key['cProInicial'])+floatval($key['cProentrada']))-floatval($key['cProSalidaD'])-floatval($key['cProSalidaM'])-floatval($key['cProFinal'])) ;
          $ventaTotal =floatval($VentaCapturada) - floatval($VentaCapturada);
          $ventaTotalPrecio =  (floatval($key['cProVenta'])-floatval($VentaCapturada)) * floatval($key['cProPrecio']);
          if ($ventaTotalPrecio < 0 ) {
            $ventaTotalPrecio = $ventaTotalPrecio*-1;
          }
          if ($key['cProVenta'] < $VentaCapturada ) {
          $Queryinsert=$Queryinsert."(".$date.",".$key['cProInicial'].",'".$key['cProCodigo']."',".$key['cProentrada'].",".$key['cProSalidaM'].",".$key['cProSalidaD'].",".$key['cProFinal'].",".$ventaTotalPrecio."),";
        }else{
            $Queryinsert=$Queryinsert."(".$date.",".$key['cProInicial'].",'".$key['cProCodigo']."',".$key['cProentrada'].",".$key['cProSalidaM'].",".$key['cProSalidaD'].",".$key['cProFinal'].",0),";
        }

        }


        $Queryinsert=substr($Queryinsert,0,-1);
    		DB::statement($eliminarRegistros);
    		DB::statement($Queryinsert);


  }

  public function FunBuscarReporteInventarioPDF( Request $request) {
    $fecha=$request['fecha'];
    $date =carbon:: parse($fecha)->format('Ymd');
    		$datosempresa=DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
    		$empresaSel=db::select($datosempresa);
    		$empresaSel=json_decode(json_encode($empresaSel),true);
    		$info_encabezado=['fecha'=>$fecha,'empresaSeleccionada'=>$empresaSel];
        $dataGPDF= ['data'=>$this->FunBuscarReporteInventario($request)];
    		$header=view()->make('/Inventario/PDF/header')->with('datosencabezado',$info_encabezado)->render();
    		$pdf=PDF::loadview('/Inventario/PDF/body',compact('dataGPDF'));
    		$pdf->setOption('header-html',$header)
    		->setOption('header-right','PAGINA [page] DE [toPage]')
    		->setOption('header-font-name','Courier')
    		->setOption('header-font-size','8')
    		->setOption('margin-top',38)
    		->setOption('margin-bottom',10)
    		->setOption('margin-left',10)
    		->setOption('orientation','Landscape')
    		->setOption('margin-right',10);
    		return $pdf->download('reporteProductosInventario.pdf');
  }

    public function FunBuscarReporteInventarioEXCEL(Request $request ){
          $fecha=$request['fecha'];
      		$datosempresa=DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
      		$empresaSel=db::select($datosempresa);
      		$empresaSel=json_decode(json_encode($empresaSel),true);
      		$info_encabezado=['fecha'=>$fecha,'empresaSeleccionada'=>$empresaSel];
          $dataGPDF= ['data'=>$this->FunBuscarReporteInventario($request)];
          $header=view()->make('/Inventario/PDF/header')->with('datosencabezado',$info_encabezado)->render();

          Excel::create('ventas', function($excel)use (
      			$info_encabezado,
      			$dataGPDF
      		){

      			$excel->sheet('ventas', function($sheet)use(
      				$info_encabezado,
      				$dataGPDF
      			){

      				$sheet->loadView("/Inventario/Excel/index")
      				->with(compact(
      					'info_encabezado',
      					'dataGPDF'
      				));


      			});
      		})->download('csv');
    }

    public function FunGenerarReportesIndex(Request $request){
      return view('Inventario.reportes.index');
    }

    public function FunGenerarReportePorTipo(Request $request){
        $date = str_replace("-","",$request->fecha);
        $historialEntradas= InventarioDiario::join('tProductos','cProCodigo','cInvCodigoProducto')->where('cInvFechaRep',$date)->where('cInvStatus',1)->get()->toArray();
        $historialSalidasM= InventarioDiario::join('tProductos','cProCodigo','cInvCodigoProducto')->where('cInvFechaRep',$date)->where('cInvStatus',2)->get()->toArray();
        $historialSalidasD= InventarioDiario::join('tProductos','cProCodigo','cInvCodigoProducto')->where('cInvFechaRep',$date)->where('cInvStatus',3)->get()->toArray();
        $historialInventarioFinal= InventarioDiario::join('tProductos','cProCodigo','cInvCodigoProducto')->where('cInvFechaRep',$date)->where('cInvStatus',4)->get()->toArray();
        return ['entradas'=>$historialEntradas,'devoluciones'=>$historialSalidasD,'mermas'=>$historialSalidasM,'final'=>$historialInventarioFinal];
    }
    public function FunGenerarReporteMovimientosPDF(Request $request){
          $fecha=$request['fecha'];
          $date =carbon:: parse($fecha)->format('Ymd');
          $datosempresa=DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
          $empresaSel=db::select($datosempresa);
          $empresaSel=json_decode(json_encode($empresaSel),true);
          $info_encabezado=['fecha'=>$fecha,'empresaSeleccionada'=>$empresaSel];
          $dataGPDF= ['data'=>$this->FunGenerarReportePorTipo($request)];
          $header=view()->make('/Inventario/reportes/PDF/header')->with('datosencabezado',$info_encabezado)->render();
          $pdf=PDF::loadview('/Inventario/reportes/PDF/body',compact('dataGPDF'));
          $pdf->setOption('header-html',$header)
          ->setOption('header-right','PAGINA [page] DE [toPage]')
          ->setOption('header-font-name','Courier')
          ->setOption('header-font-size','8')
          ->setOption('margin-top',38)
          ->setOption('margin-bottom',10)
          ->setOption('margin-left',10)
          ->setOption('orientation','Landscape')
          ->setOption('margin-right',10);
          return $pdf->download('reporteProductosInventario.pdf');
    }
    public function FunvistaPeticionesTraspaso(){
      return view('Inventario.traspaso.indexPeticion');
    }

    public function PrepararAPIAdmintotal(Request $request ){
      $fecha=$request['fecha'];
      $date =carbon:: parse($fecha)->format('Ymd');
      $datospreparados=DB::raw("SELECT * FROM tInventarioDiario WHERE cInvFinalizado=1  AND cInvFechaRep =".$date);
      $datospreparados=db::select($datospreparados);
      $datospreparados=json_decode(json_encode($datospreparados),true);
      if ($datospreparados == []) {
        $query = "UPDATE tInventarioDiario SET cInvFinalizado=1,cInvSincro=0 WHERE cInvFechaRep =".$date;
        DB::statement($query);
        $activarBandera= "UPDATE tConfigNube SET cDBPtAdmintotal=1";
        DB::statement($activarBandera);
        return "finalizado";
      }else{
        echo "ya se finalizÃ³";
      }
    }

    public function FunVerificarEnvioadmintotal(Request $request){
      $date =carbon::now()->format('Ymd');
      $inventarioSincronizado =InventarioDiario::where('cInvFechaRep',$date)->where('cInvFinalizado',1)->first();
      if ($inventarioSincronizado == []) {
        return 0;
      }else{

        return 1;
      }

    }

    public function FunGenerarArchivosEnviarCorreo(Request $request){
          $archivoExcel=null;
          $fecha=$request['fecha'];
          $date =carbon:: parse($fecha)->format('Ymd');
          $datosempresa=DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
          $empresaSel=db::select($datosempresa);
          $empresaSel=json_decode(json_encode($empresaSel),true);
          $info_encabezado=['fecha'=>$fecha,'empresaSeleccionada'=>$empresaSel];
          $dataGPDF= ['data'=>$this->FunBuscarReporteInventario($request)];


          $header=view()->make('/Inventario/PDF/header')->with('datosencabezado',$info_encabezado)->render();
          $pdf=PDF::loadview('/Inventario/PDF/body',compact('dataGPDF'));
          $pdf->setOption('header-html',$header)
          ->setOption('header-right','PAGINA [page] DE [toPage]')
          ->setOption('header-font-name','Courier')
          ->setOption('header-font-size','8')
          ->setOption('margin-top',38)
          ->setOption('margin-bottom',10)
          ->setOption('margin-left',10)
          ->setOption('orientation','Landscape')
          ->setOption('margin-right',10);


          $excelinv = Excel::create('InventarioDiarioExcel', function($excel)use (
            $info_encabezado,
            $dataGPDF
          ){

            $excel->sheet('InventarioDiarioExcel', function($sheet)use(
              $info_encabezado,
              $dataGPDF
            ){

              $sheet->loadView("/Inventario/Excel/index")
              ->with(compact(
                'info_encabezado',
                'dataGPDF'
              ));


            });
          });//->download('csv');

          $correos =Correos::select('cConCorCorreo')->get()->toArray();
          $correosFormat=array();
            foreach ($correos as $key ) {
              $correosFormat[]=$key['cConCorCorreo'];
            }

            $data["email"] =$correosFormat;
            $data["client_name"]="Elkin Vazquez";

            $data["subject"]="REPOTE DE INVENTARIO".$fecha;
            try{
            Mail::send('correos.inventarioDiario.index', $data, function($message)use($data,$pdf,$excelinv) {
            $message->to($data["email"], $data["client_name"])
            ->subject($data["subject"])
            ->attachData($pdf->output(), "InventarioDiarioPDF.pdf")
            ->attach($excelinv->store("csv",false,true)['full']);

            });
        }catch(JWTException $exception){
            $this->serverstatuscode = "0";
            $this->serverstatusdes = $exception->getMessage();
        }

    }
}
