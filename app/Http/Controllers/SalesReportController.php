<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuracion;
use App\ParteDia;
use App\FormasPago;
use Dompdf\Dompdf;
use App\Categorias;
use App\Merma;
use DB;
use PDF;
use App\historialMesas;
use App\historialProductos;
use App\historialFormasPagos;
use App\Cortes;
use Carbon\Carbon;

class SalesReportController extends Controller
{

  public function index(){

    return view('reportes/reporte_ventas/sales_report');
  }

  public function obtener_ventas(Request $request){
    $FechaIni = str_replace("-","",$request->date_since);
    $FechaFin = str_replace("-", "", $request->date_to);

    return $this->obtenerCategorias($FechaIni,$FechaFin);
  }

  public function obtenerCategorias($FechaIni,$FechaFin){
    $TotalFinal = 0;
    $PrecioFinal = 0;
    $SubtotalFinal = 0;
    $MermaFinal = 0;
    $CortesiasFinal = 0;
    $CancelacionesFinal = 0;
    $Impuestos1Final = 0;
    $Impuestos2Final = 0;
    $Impuestos3Final = 0;
    $Impuestos4Final = 0;
    $CantidadFinal = 0;
    $IndexHoraT = 0;
    $TotalFinal = 0;
    $TotalUber = 0;
    $TotalTotalHorarios = 0;
    $HoraSInCategoriaTotal = 0;
    $Categorias = Categorias::get();
    $ProductosSInCategoriaTotal = 0;
    $CategoriasExistentes = array();
    $TotalHorarios = array();
    $Apps = array();
    $impuesto1 = Configuracion::select('cConImpuesto1Nombre','cConImpuesto1Cantidad')->where('cConImpuesto1Activo', 1)->first();
    $impuesto2 = Configuracion::select('cConImpuesto2Nombre','cConImpuesto2Cantidad')->where('cConImpuesto2Activo', 1)->first();
    $impuesto3 = Configuracion::select('cConImpuesto3Nombre','cConImpuesto3Cantidad')->where('cConImpuesto3Activo', 1)->first();
    $impuesto4 = Configuracion::select('cConImpuesto4Nombre','cConImpuesto4Cantidad')->where('cConImpuesto4Activo', 1)->first();
    $Horarios = ParteDia::get();

    foreach($Categorias as $Categoria){
      $CategoriasExistentes[] = $Categoria->cCatCodigo;
      if ($Categoria->cCatFee != 0) {
        $Totales = $this->obtenerTotales($FechaIni,$FechaFin,$Categoria->cCatCodigo);

        $app['Codigo'] = $Categoria->cCatCodigo;
        $app['Descripcion'] = $Categoria->cCatDescripcion;
        $app['Fee'] = $Totales['Subtotal'] * -$Categoria->cCatFee;
        $app['Total'] = $Totales['Total'] + $app['Fee'];
        $app['Subtotal'] = $Totales['Subtotal'];
        $app['Impuesto1'] = $Totales['Impuesto1'];
        $app['Impuesto2'] = $Totales['Impuesto2'];
        $app['Impuesto3'] = $Totales['Impuesto3'];
        $app['Impuesto4'] = $Totales['Impuesto4'];

        array_push($Apps, $app);
      }
      $Categoria->Totales = $this->obtenerTotales($FechaIni,$FechaFin,$Categoria->cCatCodigo);
      $Categoria->Hora = $this->obtenerHora($FechaIni,$FechaFin,$Categoria->cCatCodigo);

      foreach ($Categoria->Hora['Horarios'] as $index => $Horario) {
        if ( ! isset($TotalHorarios[$index])) {
         $TotalHorarios[$index] = 0;
      }
      $TotalHorarios[$index] = $TotalHorarios[$index] + $Horario['Total'];

    }

      $TotalFinal = $TotalFinal + $Categoria->Totales['Total'];
      $SubtotalFinal = $SubtotalFinal + $Categoria->Totales['Subtotal'];
      $CortesiasFinal = $CortesiasFinal + $Categoria->Totales['Cortesias'];
      $CancelacionesFinal = $CancelacionesFinal + $Categoria->Totales['Cancelaciones'];
      $Impuestos1Final = $Impuestos1Final + $Categoria->Totales['Impuesto1'];
      $Impuestos2Final = $Impuestos2Final + $Categoria->Totales['Impuesto2'];
      $Impuestos3Final = $Impuestos3Final + $Categoria->Totales['Impuesto3'];
      $Impuestos4Final = $Impuestos4Final + $Categoria->Totales['Impuesto4'];
      $MermaFinal = $MermaFinal + $Categoria->Totales['Merma'];
    }

    $ProductosSInCategoria = $this->obtenerProductosSinCategoria($FechaIni,$FechaFin,$CategoriasExistentes);

    if ($ProductosSInCategoria) {
      $ProductosSInCategoriaTotal = $this->obtenerTotalesSinCategoria($FechaIni,$FechaFin,$CategoriasExistentes);
      $HoraSInCategoriaTotal = $this->obtenerHoraSinCategoria($FechaIni,$FechaFin,$CategoriasExistentes);
      $TotalFinal = $TotalFinal + $ProductosSInCategoriaTotal['Total'];
      $SubtotalFinal = $SubtotalFinal + $ProductosSInCategoriaTotal['Subtotal'];
      $CortesiasFinal = $CortesiasFinal + $ProductosSInCategoriaTotal['Cortesias'];
      $CancelacionesFinal = $CancelacionesFinal + $ProductosSInCategoriaTotal['Cancelaciones'];
      $Impuestos1Final = $Impuestos1Final + $ProductosSInCategoriaTotal['Impuesto1'];
      $Impuestos2Final = $Impuestos2Final + $ProductosSInCategoriaTotal['Impuesto2'];
      $Impuestos3Final = $Impuestos3Final + $ProductosSInCategoriaTotal['Impuesto3'];
      $Impuestos4Final = $Impuestos4Final + $ProductosSInCategoriaTotal['Impuesto4'];
      $MermaFinal = $MermaFinal + $ProductosSInCategoriaTotal['Merma'];

      foreach ($HoraSInCategoriaTotal['Horarios'] as $index => $Horario) {
        if ( ! isset($TotalHorarios[$index])) {
         $TotalHorarios[$index] = 0;
         $IndexHoraT = $index;
      }
      $TotalHorarios[$index] = $TotalHorarios[$index] + $Horario['Total'];
    }
    }
    foreach ($TotalHorarios as $index => $Horario) {
       $TotalTotalHorarios = $TotalTotalHorarios +$Horario;
    }
    $FormaPagos = $this->obtenerFormasPago($FechaIni,$FechaFin);
    $HoraMesas = $this->obtenerHoraMesas($FechaIni,$FechaFin);
    return ['Total' =>$TotalFinal,'Categorias' =>$Categorias,'SinCategoria' =>$ProductosSInCategoria,'TotalesSinCategorias' =>$ProductosSInCategoriaTotal
            ,'Impuesto1' =>$impuesto1
            ,'Impuesto2' =>$impuesto2
            ,'Impuesto3' =>$impuesto3
            ,'Impuesto4' =>$impuesto4
            ,'SubtotalFinal' => $SubtotalFinal
            ,'Impuestos1Final' => $Impuestos1Final
            ,'Impuestos2Final' => $Impuestos2Final
            ,'Impuestos3Final' => $Impuestos3Final
            ,'Impuestos4Final' => $Impuestos4Final
            ,'CanselacionesFinal' => $CancelacionesFinal
            ,'CortesiasFinal' => $CortesiasFinal
            ,'TotalHorarios' => $TotalHorarios
            ,'TotalHorarios' => $TotalHorarios
            ,'TotalFinalHorario' => $TotalTotalHorarios
            ,'FormaPagos' => $FormaPagos
            ,'HoraMesa' => $HoraMesas
            ,'MermaFinal' => $MermaFinal
            ,'Horarios' => $Horarios
            ,'HoraSInCategoriaTotal' => $HoraSInCategoriaTotal
            ,'Apps' => $Apps];
          }

  public function obtenerTotales($FechaIni,$FechaFin,$Categoria){
    $ProductosTotal = historialProductos::where('cHisProCategoria',$Categoria)
    ->where('cHisProCodigo', '<>',1005)
    ->whereBetween('cHisProFechaRep', [$FechaIni, $FechaFin])
    ->orderBy('cHisProCodigo');

    $MermaTotal = Merma::where('cCategoria',$Categoria)
    ->whereBetween('cFechaRep', [$FechaIni, $FechaFin])
    ->sum('cCantidad');

    $SubtotalTotal = $ProductosTotal->sum('cHisProSubTotal');
    $CortesiasTotal = $ProductosTotal->sum('cHisProCortesia');
    $CancelacionesTotal = $ProductosTotal->sum('cHisProCancel');
    $Impuesto1 = $ProductosTotal->sum('cHisProImp1');
    $Impuesto2 = $ProductosTotal->sum('cHisProImp2');
    $Impuesto3 = $ProductosTotal->sum('cHisProImp3');
    $Impuesto4 = $ProductosTotal->sum('cHisProImp4');
    $TotalTotal = $SubtotalTotal+$Impuesto1+$Impuesto2+$Impuesto3+$Impuesto4+$CancelacionesTotal+$CortesiasTotal+$MermaTotal;

    return ['Total' =>$TotalTotal,'Impuesto1' =>$Impuesto1,'Impuesto2' =>$Impuesto2,'Impuesto3' =>$Impuesto3,
    'Impuesto4' =>$Impuesto4,'Subtotal' =>$SubtotalTotal,
    'Cortesias' =>$CortesiasTotal,'Cancelaciones' =>$CancelacionesTotal, 'Merma' => $MermaTotal];
  }

  public function obtenerTotalesSinCategoria($FechaIni,$FechaFin,$Categoria){
    $ProductosTotal = historialProductos::whereNotIn('cHisProCategoria',$Categoria)
    ->where('cHisProCodigo', '<>',1005)
    ->whereBetween('cHisProFechaRep', [$FechaIni, $FechaFin])
    ->orderBy('cHisProCodigo');

    $MermaTotal = Merma::where('cCategoria',$Categoria)
    ->whereBetween('cFechaRep', [$FechaIni, $FechaFin])
    ->sum('cCantidad');

    $SubtotalTotal = $ProductosTotal->sum('cHisProSubTotal');
    $CortesiasTotal = $ProductosTotal->sum('cHisProCortesia');
    $CancelacionesTotal = $ProductosTotal->sum('cHisProCancel');
    $Impuesto1 = $ProductosTotal->sum('cHisProImp1');
    $Impuesto2 = $ProductosTotal->sum('cHisProImp2');
    $Impuesto3 = $ProductosTotal->sum('cHisProImp3');
    $Impuesto4 = $ProductosTotal->sum('cHisProImp4');
    $TotalTotal = $SubtotalTotal+$Impuesto1+$Impuesto2+$Impuesto3+$Impuesto4+$CancelacionesTotal+$CortesiasTotal;

    return ['Total' =>$TotalTotal,'Impuesto1' =>$Impuesto1,'Impuesto2' =>$Impuesto2,'Impuesto3' =>$Impuesto3,
    'Impuesto4' =>$Impuesto4,'Subtotal' =>$SubtotalTotal,
    'Cortesias' =>$CortesiasTotal,'Cancelaciones' =>$CancelacionesTotal, 'Merma' => $MermaTotal];
  }

  public function obtenerProductosSinCategoria($FechaIni,$FechaFin,$Categorias){
    $Productos = historialProductos::select(DB::raw("SUM(cHisProSubTotal) as Subtotal,SUM(cHisProImp1) as Impuesto1,SUM(cHisProImp2) as Impuesto2,SUM(cHisProImp3) as Impuesto3,SUM(cHisProImp4) as Impuesto4,SUM(cHisProCantidad) as Cantidad,SUM(cHisProSubTotal+cHisProImp1+cHisProImp2+cHisProImp3+cHisProImp4) as Total" ), 'cHisProCodigo','cHisProPrecio','cHisProDesGeneral')
    ->whereNotIn('cHisProCategoria',$Categorias)
    ->where('cHisProCodigo', '<>',1005)
    ->where('cHisProCancel', 0)
    ->whereBetween('cHisProFechaRep', [$FechaIni, $FechaFin])
    ->groupBy('tHisProductos.cHisProCodigo','cHisProPrecio','cHisProDesGeneral')
    ->orderBy('cHisProCodigo')
    ->first();

    return $Productos;
  }

  public function obtenerFormasPago($FechaIni,$FechaFin){
    $TotalTotal = 0;
    $FormasDePago = FormasPago::where('cCarForReportes', 1)
    ->where('cCatForActivo', 1)
    ->get();

    $TotalCantidad = 0;
    $TotalSubtotal = 0;
    $TotapPropina = 0;
    $TotalCargo = 0;
    $TotalFinal = 0;
    foreach($FormasDePago as $index => $FormaPago){
      $Pago = historialFormasPagos::whereBetween('cHisForFechaRep', [$FechaIni, $FechaFin])
      ->where('cHisForId', $FormaPago->cCatForId);

      $FormaPago->Cargo = historialFormasPagos::whereBetween('cHisForFechaRep', [$FechaIni, $FechaFin])
      ->where('cHisForId', $FormaPago->cCatForId)
      ->where('cHisForTipo',2)
      ->sum('cHisForCant');

      $FormaPago->Propina = historialFormasPagos::whereBetween('cHisForFechaRep', [$FechaIni, $FechaFin])
      ->where('cHisForId', $FormaPago->cCatForId)
      ->where('cHisForTipo',3)
      ->sum('cHisForCant');

      $FormaPago->Cantidad = $Pago->where('cHisForTipo',1)->count();
      $FormaPago->Subtotal = $Pago->sum('cHisForCant');
      $FormaPago->Total = $FormaPago->Subtotal+$FormaPago->Propina+$FormaPago->Cargo;

      $TotalCantidad = $TotalCantidad+$FormaPago->Cantidad;
      $TotalSubtotal = $TotalSubtotal+$FormaPago->Subtotal;
      $TotapPropina = $TotapPropina+$FormaPago->Propina;
      $TotalCargo = $TotalCargo+$FormaPago->Cargo;
      $TotalFinal = $TotalFinal+$FormaPago->Total;
    }

    return ['FormaDePago' =>$FormasDePago
            ,'TotalFinal' =>$TotalFinal
            ,'TotalCantidad' =>$TotalCantidad
            ,'TotalSubtotal' =>$TotalSubtotal
            ,'TotalCargo' =>$TotalCargo
            ,'TotapPropina' =>$TotapPropina];
  }

  public function obtenerHora($FechaIni,$FechaFin,$Categoria){
    $TotalTotal = 0;
    $Productos = historialProductos::select(DB::raw('SUM(cHisProSubTotal) as SubTotal, DATEPART(hh, cHisProFechaHoraEntrada) as hora'))
    ->where('cHisProCategoria',$Categoria)
    ->where('cHisProCodigo', '<>',1005)
    ->where('cHisProCancel', 0)
    ->whereBetween('cHisProFechaRep', [$FechaIni, $FechaFin])
    ->groupBy('cHisProFechaHoraEntrada')
    ->get();

    $Horarios = ParteDia::get();
    foreach($Horarios as $index => $Horario){
      $Total = 0;

      foreach($Productos as $index => $Producto){
        if ($Horario->cParteDiaFin >24) {
          if ($Producto->hora >= $Horario->cParteDiaInicio) {
            $Total = $Total + $Producto->SubTotal;
          }
          elseif ($Producto->hora < 5) {
            $Total = $Total + $Producto->SubTotal;
          }
        } else {
            if ($Producto->hora >= $Horario->cParteDiaInicio  && $Producto->hora <= $Horario->cParteDiaFin) {
              $Total = $Total + $Producto->SubTotal;
          }
        }
      }
      $Horario->Total = $Total;
      $TotalTotal = $TotalTotal + $Total;
    }

    return ['Total' =>$TotalTotal,'Horarios' =>$Horarios];
  }

  public function obtenerHoraSinCategoria($FechaIni,$FechaFin,$Categorias){
    $TotalTotal = 0;
    $Productos = historialProductos::select(DB::raw('SUM(cHisProSubTotal) as SubTotal, DATEPART(hh, cHisProFechaHoraEntrada) as hora'))
    ->whereNotIn('cHisProCategoria',$Categorias)
    ->where('cHisProCodigo', '<>',1005)
    ->where('cHisProCancel', 0)
    ->whereBetween('cHisProFechaRep', [$FechaIni, $FechaFin])
    ->groupBy('cHisProFechaHoraEntrada')
    ->get();

    $Horarios = ParteDia::get();
    foreach($Horarios as $index => $Horario){
      $Total = 0;
      foreach($Productos as $index => $Producto){
        if ($Horario->cParteDiaFin >24) {
          if ($Producto->hora >= $Horario->cParteDiaInicio) {
            $Total = $Total + $Producto->SubTotal;
          }
          elseif ($Producto->hora < 5) {
            $Total = $Total + $Producto->SubTotal;
          }
        } else {
            if ($Producto->hora >= $Horario->cParteDiaInicio  && $Producto->hora <= $Horario->cParteDiaFin) {
              $Total = $Total + $Producto->SubTotal;
          }
        }
      }
      $Horario->Total = $Total;
      $TotalTotal = $TotalTotal + $Total;
    }

    return ['Total' =>$TotalTotal,'Horarios' =>$Horarios];
  }

  public function obtenerHoraMesas($FechaIni,$FechaFin){
    $FinalClientes = 0;
    $FinalTicket = 0;
    $Mesas = historialMesas::select(DB::raw('SUM(cHisMesCantClientes) as Cantidad,COUNT(cHisMesTicket) as Ticket, DATEPART(hh, cHisMesFechaEntrada) as hora'))
    ->whereBetween('cHisMesFechaRep', [$FechaIni, $FechaFin])
    ->groupBy('cHisMesFechaEntrada')
    ->get();

    $Horarios = ParteDia::get();
    foreach($Horarios as $index => $Horario){
      $Total = 0;

      foreach($Mesas as $index => $Mesa){
        if ($Horario->cParteDiaFin >24) {
          if ($Mesa->hora >= $Horario->cParteDiaInicio) {
            $Horario->TotalClientes = $Horario->TotalClientes + $Mesa->Cantidad;
            $Horario->TotalTicket = $Horario->TotalTicket + $Mesa->Ticket;
          }
          elseif ($Mesa->hora < 5) {
            $Horario->TotalClientes = $Horario->TotalClientes + $Mesa->Cantidad;
            $Horario->TotalTicket = $Horario->TotalTicket + $Mesa->Ticket;
          }
        } else {
            if ($Mesa->hora >= $Horario->cParteDiaInicio  && $Mesa->hora <= $Horario->cParteDiaFin) {
              $Horario->TotalClientes = $Horario->TotalClientes + $Mesa->Cantidad;
              $Horario->TotalTicket = $Horario->TotalTicket + $Mesa->Ticket;
          }
        }
      }
      $FinalClientes = $FinalClientes + $Horario->TotalClientes;
      $FinalTicket = $FinalTicket + $Horario->TotalTicket;
    }

    return ['TotalClientes' =>$FinalClientes,'TotalTicket' =>$FinalTicket,'Mesas' =>$Horarios];
  }
  public function pdf_sales_report(Request $request){

    $FechaIni = str_replace("-","",$request->date_since);
    $FechaFin = str_replace("-", "", $request->date_to);

    $reportes =  $this->obtenerCategorias($FechaIni,$FechaFin);

    $date_since = date_create($request->date_since);
    $date_to = date_create($request->date_to);

  $date_to_format = date_format($date_to, 'm/d/Y');
  $date_since_format = date_format($date_since, 'm/d/Y');

  $sucursal = Configuracion::select('cConSucursal')->first();
  $configuracion = Configuracion::first()->toArray();


  $info_encabezado = ['fecha' => $date_since_format." - ".$date_to_format, 'sucursal' => $sucursal['cConSucursal'], 'configuracion' => $configuracion];


$nombre_archivo = 'Sales report ('.$date_since_format." - ".$date_to_format.')';
$header = view()->make('/reportes/reporte_ventas/sales_report_pdfheader')->with('info_encabezado', $info_encabezado)->render();
$pdf = PDF::loadview('/reportes/reporte_ventas/sales_report_bodypdf', compact('reportes'));
$pdf->setOption('header-html', $header)
->setOption('header-right','PAGE [page] OF [toPage]')
->setOption('header-font-name','Courier')
->setOption('header-font-size','8')
->setOption('margin-top',38)
->setOption('margin-bottom',10)
->setOption('margin-left',10)
->setOption('margin-right',10);
return $pdf->download($nombre_archivo.'pdf');


  }

}
