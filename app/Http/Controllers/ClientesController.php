<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Cliente;

class ClientesController extends Controller
{
  public function clientesPrincipal(){
    return view('Catalogos/clientes/clientesPrincipal');
  }
  public function clientesObtener(){
    $clientes = Cliente::get();
    $clientes = json_decode($clientes,true);
    return ['clientes' => $clientes];
  }
  public function clientesAgregar(Request $request){
    $addCliente = $request->all();
    Cliente::create($addCliente);
  }
  public function clientesDataEditar(Request $request){
    $idclientes = $request->all();
    $dataCliente = Cliente::where('cidcliente', $idclientes['idcliente'])->first();
    $dataCliente = json_decode($dataCliente,true);
    return ['clientes' => $dataCliente];
  }
  public function clientesEditar(Request $request){
    $editarClientes = $request->all();
    Cliente::where('cidcliente', $editarClientes['cidcliente'])->update([
      'cnombrecliente' => $editarClientes['cnombrecliente'],
      'crfccliente' => $editarClientes['crfccliente'],
      'ccurpcliente' => $editarClientes['ccurpcliente'],
      'cdomiciliocomercial' => $editarClientes['cdomiciliocomercial'],
      'crepesentantelegal' => $editarClientes['crepesentantelegal'],
      'cciudadcliente' => $editarClientes['cciudadcliente'],
      'cestadocliente' => $editarClientes['cestadocliente'],
      'ccodigopostal' => $editarClientes['ccodigopostal'],
      'cpaiscliente' => $editarClientes['cpaiscliente'],
      'ccorreoelectronico' => $editarClientes['ccorreoelectronico'],
      'ctelefonocliente' => $editarClientes['ctelefonocliente'],
      'cnotascliente' => $editarClientes['cnotascliente'],
      'cestatuscliente' => $editarClientes['cestatuscliente'],
      'cfechaalta' => $editarClientes['cfechaalta'],
      'cfechainactivo' => $editarClientes['cfechainactivo'],
      'ccodigocliente' => $editarClientes['ccodigocliente'],
      'ccodigoadmintotal' => $editarClientes['ccodigoadmintotal'],
    ]);
  }
  public function clientesEliminar(Request $request){
    $eliminarCliente = $request->all();
    Cliente::where('cidcliente', $eliminarCliente['cidcliente'])->delete();
  }
}
