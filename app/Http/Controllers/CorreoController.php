<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail; //Importante incluir la clase Mail, que será la encargada del envío

class CorreoController extends Controller
{
    public function index(){
    	return view('correo.index');
    }
    public function sedEmail(Request $request){
        $subject = "Asunto del correo";
        $for="elkinrvazquez@gmail.com";
        Mail::send('email',"elkin vazquez", function($msj) use($subject,$for){
            $msj->from("tucorreo@gmail.com","NombreQueApareceráComoEmisor");
            $msj->subject($subject);
            $msj->to($for);
        });
        return redirect()->back();
    }
}
