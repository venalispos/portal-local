<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{

    public function indexmenu(){
    	return view('menu.configuracionmenu');
    }

    public function obtenerMenu(){

    	$listamenu=Menu::orderBy('cMenOrden')->get();
    	return ['listamenu'=>$listamenu];
    }
    public function nuevoMenu(Request $request){
     Menu::create(['cMenFolPadre'=>$request['folPadre'],'cMenURL'=>$request['URL'],'cMenOrden'=>$request['id'],'cMenDescripcion'=>$request['nombre'],'cMenIcono'=>$request['nombreIcono'],'cMenHijos'=>$request['nodHijos']]);
    }
    public function actualizarMenu(Request $request){
         Menu::where('cMenFolio',$request['Folio'])->update(['cMenFolPadre'=>$request['folPadre'],'cMenURL'=>$request['URL'],'cMenOrden'=>$request['id'],'cMenDescripcion'=>$request['nombre'],'cMenIcono'=>$request['nombreIcono'],'cMenHijos'=>$request['nodHijos']]);
    }
    public function eliminarMenu(Request $request){
        Menu::where('cMenFolio',$request['Folio'])->delete();
    }
}
