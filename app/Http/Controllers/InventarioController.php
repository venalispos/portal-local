<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\InventarioDiario;
use App\Configuracion;
use App\Productos;
use App\ConfiguracionInventario;
use App\TotalesInventario;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

class InventarioController extends Controller
{


	public function ValidarOInsertarProductos(){


		return view("InventarioDiario.InventarioAcumulable.IndexInventarioDiario");

	}


	public function FuncionBuscarProducto(Request $request){

		$vrCodigoProducto=$request['codigo'];
		$vADatosProducto=Productos::selectRaw(" *, 1 as cProCantidad")->where('cProInterCodigo',$vrCodigoProducto)->get()->toArray();
		if ($vADatosProducto == []) {
			$vADatosProducto=null;
		}
		return $vADatosProducto;
	}



	public function FuncionGuardarProductosInventario(Request $request){


		$date = Carbon::now();
		$dateFormat=$date->format('Y-m-d h:m:s');
		$date = $date->format('Ymd');
		$usuario= auth()->user()->cUsuFolio;

		InventarioDiario::create(['tInvIniFechaRep'=>$date,'tInvIniFolioProd'=>$request['producto']['cProInterCodigo'],'tInvIniValorMinimo'=>$request['producto']['cProCantidad'],'tInvIniCostoPiezaProd'=>0,'tInvIniFolioUsuario'=>$usuario,'tInvIniFechaRegistro'=>$dateFormat,'tInvIniStatus'=>1]);


	}


	public function FuncionmostrarPedidosAgregados(){

		$date = Carbon::now();
		$date = $date->format('Ymd');
		$VDatosInventario=InventarioDiario::selectRaw('cProInterCodigo,cProDesTicket,SUM(tInvIniValorMinimo) as cantidad')->where('tInvIniFechaRep',$date)->where('tInvIniStatus',1)->where('tInvIniValorMinimo','>',0)->Join('tProductos','tInvIniFolioProd','cProInterCodigo')->groupBy('cProInterCodigo','cProDesTicket')->get()->toArray();
		return['datosInventario'=>$VDatosInventario];

	}


	public function FuncionModificarCantidadProducto(Request $request){

		$date = Carbon::now();
		$dateFormat=$date->format('Y-m-d h:m:s');
		$date = $date->format('Ymd');
		$usuario= auth()->user()->cUsuFolio;

		InventarioDiario::where('tInvIniFechaRep',$date)->where('tInvIniStatus',1)->where('tInvIniFolioProd',$request['cProInterCodigo'])->delete();
		InventarioDiario::create(['tInvIniFechaRep'=>$date,
			'tInvIniFolioProd'=>$request['cProInterCodigo'],
			'tInvIniValorMinimo'=>$request['nuevaCantidad'],
			'tInvIniFolioUsuario'=>$usuario,
			'tInvIniFechaRegistro'=>$dateFormat,
			'tInvIniStatus'=>1]);
	}



	public function FuncionEliminarProducto(Request $request){
		$date = Carbon::now();
		$dateFormat=$date->format('Y-m-d h:m:s');
		$date = $date->format('Ymd');
		InventarioDiario::where('tInvIniFechaRep',$date)->where('tInvIniStatus',1)->where('tInvIniFolioProd',$request['cProInterCodigo'])->delete();
	}



	public function IndexReporteDiario(){

		return view('InventarioDiario.InventarioAcumulable.IndexReporteInventario');

	}



	public function FuncionBuscarDetallesProductos(Request $request){

		$fecha=$request['fecha'];
		$date =carbon:: parse($fecha)->format('Ymd');

		$dateAnterior=0;
		$informacionInventario=null;

		$vSComprobarFecha=InventarioDiario::selectRaw("MAX(tInvIniFechaRep)	 AS 'fechaRep'")->where('tInvIniFechaRep','<',$date)->get()->toArray();
		if ($vSComprobarFecha[0]['fechaRep'] ==[]) {

			$buscarInformacionUnaFecha = DB::raw("SELECT * FROM (SELECT 0 as Inicial,codigo,cProDesTicket,SUM(ENTRADA) ENTRADA,SUM(SALIDAM) SALIDAM,SUM(SALIDAD) SALIDAD,SUM(SALIDADUSUARIO) as SALIDADUSUARIO,cProPrecio FROM (
				SELECT codigo,cProPrecio,cProDesTicket,CASE WHEN statusES=1 OR statusES=0 THEN cantidad ELSE 0 END  AS 'ENTRADA',CASE WHEN statusES=2 THEN cantidad ELSE 0 END  AS 'SALIDAM',CASE WHEN statusES=3 THEN cantidad ELSE 0 END  AS 'SALIDAD',CASE WHEN statusES=4 THEN cantidad ELSE 0 END  AS 'SALIDADUSUARIO'  FROM (
				SELECT tInvIniFolioProd AS 'codigo',SUM(tInvIniValorMinimo) AS 'cantidad',tInvIniStatus AS 'statusES',cProDesTicket,cProPrecio FROM tInventarioDiario  INNER JOIN tProductos ON cProInterCodigo=tInvIniFolioProd
				WHERE tInvIniFechaRep = ".$date." GROUP BY tInvIniFolioProd,tInvIniStatus,cProDesTicket,cProPrecio) AS table1) as TB11 GROUP BY codigo,cProDesTicket,cProPrecio) AS TB111
				FULL  JOIN
				(SELECT cProInterCodigo,SUM(cHisProCantidad) as cantidadventa FROM tHisProductos INNER JOIN tProductos ON cProCodigo=cHisProCodigo WHERE cHisProFechaRep= ".$date." GROUP BY cHisProCodigo,cProInterCodigo) AS TB2 ON TB111.codigo=TB2.cProInterCodigo");


			$informacionInventario=db::select($buscarInformacionUnaFecha);
			$informacionInventario=json_decode(json_encode($informacionInventario),true);


		}else{


			$dateAnterior=$vSComprobarFecha[0]['fechaRep'];

			$totalVentaDiaAnterio=DB::raw("SELECT cITProCodigo as codigoProducto,(cITVentasSistema) AS 'total' FROM  tInventarioDiarioTotales WHERE cITFechaRep=".$dateAnterior." group by cITProCodigo,cITInicial,cITEntradas,cITSalidasMermas,cITSalidasDevoluciones,cITSalidasDevoluciones,cITVentasSistema;");

			$_vDatosDiaAnterior=db::select($totalVentaDiaAnterio);
			$_vDatosDiaAnterior=json_decode(json_encode($_vDatosDiaAnterior),true);


			$buscarInformacionUnaFecha=DB::raw("SELECT * FROM (SELECT 0 as Inicial,codigo,cProDesTicket,SUM(ENTRADA) ENTRADA,SUM(SALIDAM) SALIDAM,SUM(SALIDAD) SALIDAD,SUM(SALIDADUSUARIO) AS SALIDADUSUARIO,cProPrecio
			FROM (SELECT cProPrecio,codigo,cProDesTicket,CASE WHEN statusES=1 OR statusES=0 THEN cantidad ELSE 0 END  AS 'ENTRADA',CASE WHEN statusES=2 THEN cantidad ELSE 0 END  AS 'SALIDAM',CASE WHEN statusES=3 THEN cantidad ELSE 0 END  AS 'SALIDAD',CASE WHEN statusES=4 THEN cantidad ELSE 0 END  AS 'SALIDADUSUARIO'
			  FROM (
				SELECT tInvIniFolioProd AS 'codigo',SUM(tInvIniValorMinimo) AS 'cantidad',tInvIniStatus AS 'statusES',cProDesTicket,cProPrecio FROM tInventarioDiario  INNER JOIN tProductos ON cProInterCodigo=tInvIniFolioProd
				WHERE tInvIniFechaRep = ".$date." GROUP BY tInvIniFolioProd,tInvIniStatus,cProDesTicket,cProPrecio) AS table1) as TB11 GROUP BY codigo,cProDesTicket,cProPrecio) AS TB111
				FULL  JOIN
				(SELECT cProInterCodigo,SUM(cHisProCantidad) as cantidadventa FROM tHisProductos INNER JOIN tProductos ON cProCodigo=cHisProCodigo WHERE cHisProFechaRep= ".$date." GROUP BY cHisProCodigo,cProInterCodigo) AS TB2 ON TB111.codigo=TB2.cProInterCodigo");

			$informacionInventario=db::select($buscarInformacionUnaFecha);
			$informacionInventario=json_decode(json_encode($informacionInventario),true);


			for ($i=0; $i < sizeof($informacionInventario) ; $i++) {

				for ($j=0; $j < sizeof($_vDatosDiaAnterior); $j++) {

					if ($_vDatosDiaAnterior[$j]['codigoProducto'] == $informacionInventario[$i]['codigo']) {

						if ($_vDatosDiaAnterior[$j]['total'] == NULL) {
							$informacionInventario[$i]['Inicial'] = 0;
						}else{
							$informacionInventario[$i]['Inicial'] = $_vDatosDiaAnterior[$j]['total'];

						}

					}
				}
			}




		}

		$InventarioRealizado=DB::Raw("SELECT MAX(cITFechaRep) as fecharep FROM tInventarioDiarioTotales WHERE cITFechaRep=".$date);
		$InventarioRealizado=db::select($InventarioRealizado);
		$InventarioRealizado=json_decode(json_encode($InventarioRealizado),true);

		$status=0;
		if ($InventarioRealizado[0]["fecharep"] ==$date) {
			$status=1;
		}

		$MostrarMensaje=0;
		if ($informacionInventario == [] ) {
			$MostrarMensaje=1;
		}

		return ['informacionInventario'=>$informacionInventario,'status'=>$status,'mensaje'=>$MostrarMensaje];

	}



	public function IndexCapturarSalidas(){
		return view("InventarioDiario.InventarioAcumulable.IndexCapturarSalidas");
	}




	public function FuncionAgregarsalida(Request $request){
		$date = Carbon::now();
		$dateFormat=$date->format('Y-m-d h:m:s');
		$date = $date->format('Ymd');
		$usuario= auth()->user()->cUsuFolio;
		if ($request['seleccion'] == 0) {

			InventarioDiario::create(['tInvIniFechaRep'=>$date,'tInvIniFolioProd'=>$request['producto']['cProInterCodigo'],'tInvIniValorMinimo'=>$request['producto']['cProCantidad'],'tInvIniCostoPiezaProd'=>0,'tInvIniFolioUsuario'=>$usuario,'tInvIniFechaRegistro'=>$dateFormat,'tInvIniStatus'=>2]);

		}else if ($request['seleccion'] == 1) {

			InventarioDiario::create(['tInvIniFechaRep'=>$date,'tInvIniFolioProd'=>$request['producto']['cProInterCodigo'],'tInvIniValorMinimo'=>$request['producto']['cProCantidad'],'tInvIniCostoPiezaProd'=>0,'tInvIniFolioUsuario'=>$usuario,'tInvIniFechaRegistro'=>$dateFormat,'tInvIniStatus'=>3]);

		}

	}



	public function FuncionmostrarPedidosAgregadosSalidas(){

		$date = Carbon::now();
		$date = $date->format('Ymd');
		$VDatosInventario=InventarioDiario::selectRaw('cProInterCodigo,cProDesTicket,SUM(tInvIniValorMinimo) as cantidad,tInvIniStatus')->where('tInvIniFechaRep',$date)->whereIn('tInvIniStatus', [2, 3])->where('tInvIniValorMinimo','>',0)->Join('tProductos','tInvIniFolioProd','cProInterCodigo')->groupBy('cProInterCodigo','cProDesTicket','tInvIniStatus')->get()->toArray();

		if ($VDatosInventario ==[]) {
			$VDatosInventario=null;
		}
		return['datosInventario'=>$VDatosInventario];

	}



	public Function FuncionEliminarProductoSalida(Request $request){

		$date = Carbon::now();
		$dateFormat=$date->format('Y-m-d h:m:s');
		$date = $date->format('Ymd');
		InventarioDiario::where('tInvIniFechaRep',$date)->where('tInvIniStatus',$request['tInvIniStatus'])->where('tInvIniFolioProd',$request['cProInterCodigo'])->delete();
	}



	public function FuncionModificarCantidadProductoSalida(Request $request){

		$date = Carbon::now();
		$dateFormat=$date->format('Y-m-d h:m:s');
		$date = $date->format('Ymd');
		$usuario= auth()->user()->cUsuFolio;

		InventarioDiario::where('tInvIniFechaRep',$date)->where('tInvIniStatus',$request['tInvIniStatus'])->where('tInvIniFolioProd',$request['cProInterCodigo'])->delete();
		InventarioDiario::create(['tInvIniFechaRep'=>$date,
			'tInvIniFolioProd'=>$request['cProInterCodigo'],
			'tInvIniValorMinimo'=>$request['nuevaCantidad'],
			'tInvIniFolioUsuario'=>$usuario,
			'tInvIniFechaRegistro'=>$dateFormat,
			'tInvIniStatus'=>$request['tInvIniStatus']]);


	}


	public function FuncionGuardarTotales(Request $request){

		$_productosTotales=$request['productos'];

		$date = Carbon::now();
		$date = $date->format('Ymd');

		$eliminarRegistros="DELETE tInventarioDiarioTotales WHERE cITFechaRep=".$date;
		$Queryinsert="INSERT INTO tInventarioDiarioTotales (cITFechaRep,cITInicial,cITProCodigo,cITEntradas,cITSalidasMermas,cITSalidasDevoluciones,cITVentasSistema)VALUES";

		foreach ($_productosTotales as $key ) {
			if ($key['Inicial'] == NULL) {
				$key['Inicial']=0;
			}
			if ($key['ENTRADA'] == NULL) {
				$key['ENTRADA']=0;
			}
			if ($key['SALIDAM'] == NULL) {
				$key['SALIDAM']=0;
			}
			if ($key['SALIDAD'] == NULL) {
				$key['SALIDAD']=0;
			}
			if ($key['cantidadventa'] == NULL) {
				$key['cantidadventa']=0;
			}

			$Queryinsert=$Queryinsert."(".$date.",".$key['Inicial'].",'".$key['codigo']."',".$key['ENTRADA'].",".$key['SALIDAM'].",".$key['SALIDAD'].",".$key['SALIDADUSUARIO']."),";
		}

		$Queryinsert=substr($Queryinsert,0,-1);

		echo $Queryinsert;

		DB::statement($eliminarRegistros);
		DB::statement($Queryinsert);

	}

//Descargar pdf
	public function FuncionGenerarReporteInventario(Request $request){
		$fecha=$request['fecha'];
		$date =carbon:: parse($fecha)->format('Ymd');

		$dateAnterior=0;
		$informacionInventario=null;

		$vSComprobarFecha=InventarioDiario::selectRaw("MAX(tInvIniFechaRep)	 AS 'fechaRep'")->where('tInvIniFechaRep','<',$date)->get()->toArray();
		if ($vSComprobarFecha[0]['fechaRep'] ==[]) {

			$buscarInformacionUnaFecha = DB::raw("SELECT * FROM (SELECT 0 as Inicial,codigo,cProDesTicket,SUM(ENTRADA) ENTRADA,SUM(SALIDAM) SALIDAM,SUM(SALIDAD) SALIDAD,SUM(SALIDADUSUARIO) as SALIDADUSUARIO,cProPrecio FROM (
				SELECT codigo,cProPrecio,cProDesTicket,CASE WHEN statusES=1 OR statusES=0 THEN cantidad ELSE 0 END  AS 'ENTRADA',CASE WHEN statusES=2 THEN cantidad ELSE 0 END  AS 'SALIDAM',CASE WHEN statusES=3 THEN cantidad ELSE 0 END  AS 'SALIDAD',CASE WHEN statusES=4 THEN cantidad ELSE 0 END  AS 'SALIDADUSUARIO'  FROM (
				SELECT tInvIniFolioProd AS 'codigo',SUM(tInvIniValorMinimo) AS 'cantidad',tInvIniStatus AS 'statusES',cProDesTicket,cProPrecio FROM tInventarioDiario  INNER JOIN tProductos ON cProInterCodigo=tInvIniFolioProd
				WHERE tInvIniFechaRep = ".$date." GROUP BY tInvIniFolioProd,tInvIniStatus,cProDesTicket,cProPrecio) AS table1) as TB11 GROUP BY codigo,cProDesTicket,cProPrecio) AS TB111
				FULL  JOIN
				(SELECT cProInterCodigo,SUM(cHisProCantidad) as cantidadventa FROM tHisProductos INNER JOIN tProductos ON cProCodigo=cHisProCodigo WHERE cHisProFechaRep= ".$date." GROUP BY cHisProCodigo,cProInterCodigo) AS TB2 ON TB111.codigo=TB2.cProInterCodigo");


			$informacionInventario=db::select($buscarInformacionUnaFecha);
			$informacionInventario=json_decode(json_encode($informacionInventario),true);


		}else{


			$dateAnterior=$vSComprobarFecha[0]['fechaRep'];

			$totalVentaDiaAnterio=DB::raw("SELECT cITProCodigo as codigoProducto,(cITVentasSistema) AS 'total' FROM  tInventarioDiarioTotales WHERE cITFechaRep=".$dateAnterior." group by cITProCodigo,cITInicial,cITEntradas,cITSalidasMermas,cITSalidasDevoluciones,cITSalidasDevoluciones,cITVentasSistema;");

			$_vDatosDiaAnterior=db::select($totalVentaDiaAnterio);
			$_vDatosDiaAnterior=json_decode(json_encode($_vDatosDiaAnterior),true);


			$buscarInformacionUnaFecha=DB::raw("SELECT * FROM (SELECT 0 as Inicial,codigo,cProDesTicket,SUM(ENTRADA) ENTRADA,SUM(SALIDAM) SALIDAM,SUM(SALIDAD) SALIDAD,SUM(SALIDADUSUARIO) AS SALIDADUSUARIO,cProPrecio
			FROM (SELECT cProPrecio,codigo,cProDesTicket,CASE WHEN statusES=1 OR statusES=0 THEN cantidad ELSE 0 END  AS 'ENTRADA',CASE WHEN statusES=2 THEN cantidad ELSE 0 END  AS 'SALIDAM',CASE WHEN statusES=3 THEN cantidad ELSE 0 END  AS 'SALIDAD',CASE WHEN statusES=4 THEN cantidad ELSE 0 END  AS 'SALIDADUSUARIO'
				FROM (
				SELECT tInvIniFolioProd AS 'codigo',SUM(tInvIniValorMinimo) AS 'cantidad',tInvIniStatus AS 'statusES',cProDesTicket,cProPrecio FROM tInventarioDiario  INNER JOIN tProductos ON cProInterCodigo=tInvIniFolioProd
				WHERE tInvIniFechaRep = ".$date." GROUP BY tInvIniFolioProd,tInvIniStatus,cProDesTicket,cProPrecio) AS table1) as TB11 GROUP BY codigo,cProDesTicket,cProPrecio) AS TB111
				FULL  JOIN
				(SELECT cProInterCodigo,SUM(cHisProCantidad) as cantidadventa FROM tHisProductos INNER JOIN tProductos ON cProCodigo=cHisProCodigo WHERE cHisProFechaRep= ".$date." GROUP BY cHisProCodigo,cProInterCodigo) AS TB2 ON TB111.codigo=TB2.cProInterCodigo");

			$informacionInventario=db::select($buscarInformacionUnaFecha);
			$informacionInventario=json_decode(json_encode($informacionInventario),true);


			for ($i=0; $i < sizeof($informacionInventario) ; $i++) {

				for ($j=0; $j < sizeof($_vDatosDiaAnterior); $j++) {

					if ($_vDatosDiaAnterior[$j]['codigoProducto'] == $informacionInventario[$i]['codigo']) {

						if ($_vDatosDiaAnterior[$j]['total'] == NULL) {
							$informacionInventario[$i]['Inicial'] = 0;
						}else{
							$informacionInventario[$i]['Inicial'] = $_vDatosDiaAnterior[$j]['total'];

						}

					}
				}
			}




		}

		$InventarioRealizado=DB::Raw("SELECT MAX(cITFechaRep) as fecharep FROM tInventarioDiarioTotales WHERE cITFechaRep=".$date);
		$InventarioRealizado=db::select($InventarioRealizado);
		$InventarioRealizado=json_decode(json_encode($InventarioRealizado),true);

		$status=0;
		if ($InventarioRealizado[0]["fecharep"] ==$date) {
			$status=1;
		}

		$MostrarMensaje=0;
		if ($informacionInventario == [] ) {
			$MostrarMensaje=1;
		}

		$datosempresa=DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
		$empresaSel=db::select($datosempresa);
		$empresaSel=json_decode(json_encode($empresaSel),true);


		$info_encabezado=['fecha'=>$request->fecha,'empresaSeleccionada'=>$empresaSel];
		$dataGPDF=['informacionInventario'=>$informacionInventario,'status'=>$status];

		$header=view()->make('/InventarioDiario/InventarioAcumulable/PDF/header')->with('datosencabezado',$info_encabezado)->render();
		$pdf=PDF::loadview('/InventarioDiario/InventarioAcumulable/PDF/body',compact('dataGPDF'));
		$pdf->setOption('header-html',$header)
		->setOption('header-right','PAGINA [page] DE [toPage]')
		->setOption('header-font-name','Courier')
		->setOption('header-font-size','8')
		->setOption('margin-top',38)
		->setOption('margin-bottom',10)
		->setOption('margin-left',10)
		->setOption('margin-right',10)
		->setOption('orientation', 'Landscape');
		return $pdf->download('reporte.pdf');



	}


	public function indexCapturarSalidasporEmpleado(){
		return view('InventarioDiario.InventarioAcumulable.indexCapturarSalidasporEmpleado');
	}


	public function FuncionAgregarsalidaPorUsuario(Request $request){
		$date = Carbon::now();
		$dateFormat=$date->format('Y-m-d h:m:s');
		$date = $date->format('Ymd');
		$usuario= auth()->user()->cUsuFolio;

		InventarioDiario::create(['tInvIniFechaRep'=>$date,'tInvIniFolioProd'=>$request['producto']['cProInterCodigo'],'tInvIniValorMinimo'=>$request['producto']['cProCantidad'],'tInvIniCostoPiezaProd'=>0,'tInvIniFolioUsuario'=>$usuario,'tInvIniFechaRegistro'=>$dateFormat,'tInvIniStatus'=>4]);



	}



	public function FuncionmostrarPedidosAgregadosSalidasPorUsuario(){

		$date = Carbon::now();
		$date = $date->format('Ymd');
		$VDatosInventario=InventarioDiario::selectRaw('cProInterCodigo,cProDesTicket,SUM(tInvIniValorMinimo) as cantidad,tInvIniStatus')->where('tInvIniFechaRep',$date)->whereIn('tInvIniStatus', [4])->where('tInvIniValorMinimo','>',0)->Join('tProductos','tInvIniFolioProd','cProInterCodigo')->groupBy('cProInterCodigo','cProDesTicket','tInvIniStatus')->get()->toArray();
		return['datosInventario'=>$VDatosInventario];

	}


	//Verificar el status de las entradas de pedidos
	public function FuncionVerificarInventarioCapturadoInicialEntradas(){
		$date = Carbon::now();
		$date = $date->format('Ymd');
		$InventarioEntradasCapturadas=InventarioDiario::where('tInvIniFechaRep',$date)->whereIn('tInvIniStatus',[0,1])->where('tInvIniFinalizado',1)->get()->toArray();
		if ($InventarioEntradasCapturadas == []) {
			return 0;
		}else{
			return 1;
		}
	}

	//Cambiar status para finalizar las entradas de pedidos
	public function FuncionFinalizarCapturaInicial(Request $request){

		$date = Carbon::now();
		$date = $date->format('Ymd');
		$productos=$request['data'];
		$productosNoAgregar=null;
		$VAproductos=null;
		if ($productosNoAgregar == []) {
			$VAproductos=Productos::select("cProInterCodigo")->get()->toArray();
		}else{

			foreach ($productos as $key ) {
				$productosNoAgregar[]=$key['cProInterCodigo'];
			}

			$VAproductos=Productos::select("cProInterCodigo")->whereNotIn('cProInterCodigo',$productosNoAgregar)->get()->toArray();
		}

		$queryInsertarInventario="INSERT INTO tInventarioDiario (tInvIniFechaRep,tInvIniFolioProd,tInvIniValorMinimo,tInvIniCostoPiezaProd,tInvIniFolioUsuario,tInvIniFinalizado,tInvIniStatus)VALUES";


		foreach ($VAproductos as $key) {
			$queryInsertarInventario =$queryInsertarInventario."(".$date.",'".$key['cProInterCodigo']."',0,0,0,1,1),";
		}

		$queryInsertarInventario=substr($queryInsertarInventario,0,-1);
		DB::statement($queryInsertarInventario);



	}

	//Verificar status de finalizar mermas y devoluciones

	public function FuncionVerificarInventarioCapturadoMermasDevolucion(){
		$date = Carbon::now();
		$date = $date->format('Ymd');
		$InventarioEntradasCapturadas=InventarioDiario::where('tInvIniFechaRep',$date)->whereIn('tInvIniStatus',[2,3])->where('tInvIniFinalizado',1)->get()->toArray();
		if ($InventarioEntradasCapturadas == []) {
			return 0;
		}else{
			return 1;
		}
	}

	//Cambiar status de las mermas y devoluciones
	public function FuncionVerificarInventarioCapturadoMermasEntradas(Request $request){

		$date = Carbon::now();
		$date = $date->format('Ymd');
		$productos=$request['data'];
		$productosNoAgregar=null;
		$VAproductos=null;
		if ($productosNoAgregar == []) {
			$VAproductos=Productos::select("cProInterCodigo")->get()->toArray();
		}else{

			foreach ($productos as $key ) {
				$productosNoAgregar[]=$key['cProInterCodigo'];
			}

			$VAproductos=Productos::select("cProInterCodigo")->whereNotIn('cProInterCodigo',$productosNoAgregar)->get()->toArray();
		}

		$queryInsertarInventario="INSERT INTO tInventarioDiario (tInvIniFechaRep,tInvIniFolioProd,tInvIniValorMinimo,tInvIniCostoPiezaProd,tInvIniFolioUsuario,tInvIniFinalizado,tInvIniStatus)VALUES";


		foreach ($VAproductos as $key) {
			$queryInsertarInventario =$queryInsertarInventario."(".$date.",'".$key['cProInterCodigo']."',0,0,0,1,2),";
		}

		$queryInsertarInventario=substr($queryInsertarInventario,0,-1);
		DB::statement($queryInsertarInventario);
	}


 	//Cambiar status de inventario Final

	public function FuncionInventarioFinalUsuario(Request $request){

		$date = Carbon::now();
		$date = $date->format('Ymd');
		$productos=$request['data'];
		$productosNoAgregar=null;
		$VAproductos=null;
		if ($productosNoAgregar == []) {
			$VAproductos=Productos::select("cProInterCodigo")->get()->toArray();
		}else{

			foreach ($productos as $key ) {
				$productosNoAgregar[]=$key['cProInterCodigo'];
			}

			$VAproductos=Productos::select("cProInterCodigo")->whereNotIn('cProInterCodigo',$productosNoAgregar)->get()->toArray();
		}

		$queryInsertarInventario="INSERT INTO tInventarioDiario (tInvIniFechaRep,tInvIniFolioProd,tInvIniValorMinimo,tInvIniCostoPiezaProd,tInvIniFolioUsuario,tInvIniFinalizado,tInvIniStatus)VALUES";


		foreach ($VAproductos as $key) {
			$queryInsertarInventario =$queryInsertarInventario."(".$date.",'".$key['cProInterCodigo']."',0,0,0,1,4),";
		}

		$queryInsertarInventario=substr($queryInsertarInventario,0,-1);
		DB::statement($queryInsertarInventario);
	}



	public function FuncionVerificarInventarioFinalporUsuario(){
		$date = Carbon::now();
		$date = $date->format('Ymd');
		$InventarioEntradasCapturadas=InventarioDiario::where('tInvIniFechaRep',$date)->whereIn('tInvIniStatus',[4])->where('tInvIniFinalizado',1)->get()->toArray();
		if ($InventarioEntradasCapturadas == []) {
			return 0;
		}else{
			return 1;
		}
	}

	public function FuncionVerificarCapturarFinalizado(){
		$date = Carbon::now();
		$date = $date->format('Ymd');
		$InventarioEntradasCapturadas=InventarioDiario::where('tInvIniFechaRep',$date)->whereIn('tInvIniStatus',[1])->where('tInvIniFinalizado',1)->get()->toArray();
		$InventarioSalidasCapturadas=InventarioDiario::where('tInvIniFechaRep',$date)->whereIn('tInvIniStatus',[2,3])->where('tInvIniFinalizado',1)->get()->toArray();
		$InventarioReporteFinalCapturadas=InventarioDiario::where('tInvIniFechaRep',$date)->whereIn('tInvIniStatus',[4])->where('tInvIniFinalizado',1)->get()->toArray();
		//print_r($InventarioEntradasCapturadas);
		//print_r($InventarioSalidasCapturadas);

		$InventarioReporteo=0;

		if ($InventarioEntradasCapturadas == []) {
			$InventarioReporteo=1;
		}
		if ($InventarioSalidasCapturadas == []) {
			$InventarioReporteo=1;
		}
		if ($InventarioReporteFinalCapturadas == []) {
			$InventarioReporteo=1;
		}

		return $InventarioReporteo;
	}






//Aqui estÃ¡ los mÃ©todos que podramos rescatar

	public function getProductosPorCategoriaSubcategoria(Request $request){

		$arregloproductoseninventario ="";

		$date = Carbon::now();
		$date = $date->format('Ymd');

		$complemetoQuery = "";
		$categoria= $request['categoria'];
		$subcategoria= $request['subcategoria'];

		if ($categoria != 0) {
			$complemetoQuery = $complemetoQuery." AND cproCategoria=".$categoria;
		}
		if ($subcategoria != 0) {
			$complemetoQuery = $complemetoQuery." AND cProSubCategoria=".$subcategoria;
		}


		$productosInventario=DB::Raw("SELECT * FROM tInventarioDiario INNER JOIN tproductos ON cProFolio=tInvIniFolioProd  WHERE tInvIniFechaRep =".$date);
		$productosInventario=db::select($productosInventario);
		$productosInventario=json_decode(json_encode($productosInventario),true);


		if ($productosInventario != []) {
			foreach ($productosInventario as $key) {
				$arregloproductoseninventario=$arregloproductoseninventario.$key['tInvIniFolioProd'].",";
			}
			$arregloproductoseninventario = substr($arregloproductoseninventario, 0, -1);

			$complemetoQuery =$complemetoQuery." AND cProFolio NOT IN(".$arregloproductoseninventario.")";
		}

		$productos = DB::raw("SELECT *,0 as cantidad FROM tProductos WHERE cProActivo =1 ".$complemetoQuery);

		$productos=db::select($productos);
		$productos=json_decode(json_encode($productos),true);

		return ['productos'=>$productos,'productosinventario'=>$productosInventario];
	}

	public function getproductosfiltradoporinventario(){

		$arregloproductoseninventario ="";
		$date = Carbon::now();
		$date = $date->format('Ymd');


		$productosInventario=DB::Raw("SELECT * FROM tInventarioDiario INNER JOIN tproductos ON cProFolio=tInvIniFolioProd  WHERE tInvIniFechaRep =".$date);
		$productosInventario=db::select($productosInventario);
		$productosInventario=json_decode(json_encode($productosInventario),true);

		if ($productosInventario == []) {

			$productos = DB::raw("SELECT *,0 as cantidad FROM tProductos WHERE cProActivo =1");
			$productos=db::select($productos);
			$productos=json_decode(json_encode($productos),true);
			return ['productos'=>$productos,'productosInventario'=>$productosInventario];

		}else{

			foreach ($productosInventario as $key) {
				$arregloproductoseninventario=$arregloproductoseninventario.$key['tInvIniFolioProd'].",";
			}
			$arregloproductoseninventario = substr($arregloproductoseninventario, 0, -1);

			$productos = DB::raw("SELECT *,0 as cantidad FROM tProductos WHERE cProActivo =1 AND cProFolio NOT IN(".$arregloproductoseninventario.")");
			$productos=db::select($productos);
			$productos=json_decode(json_encode($productos),true);
			return ['productos'=>$productos,'productosInventario'=>$productosInventario];
		}

	}

	public function agregarproductoaInventario(Request $request){
		$date = Carbon::now();
		$dateFormat=$date->format('Y-m-d h:m:s');
		$date = $date->format('Ymd');
		$usuario= auth()->user()->cUsuFolio;

		InventarioDiario::create(['tInvIniFechaRep'=>$date,'tInvIniFolioProd'=>$request['producto']['cProFolio'],'tInvIniValorMinimo'=>$request['producto']['cantidad'],'tInvIniCostoPiezaProd'=>0,'tInvIniFolioUsuario'=>$usuario,'tInvIniFechaRegistro'=>$dateFormat]);
	}

	public function deleteItemInventario(Request $request){
		InventarioDiario::where('tInvIniFolio',$request['producto']['tInvIniFolio'])->delete();
	}

	public function FinalizarInventarioDiario(){

		$date = Carbon::now();
		$date = $date->format('Ymd');
		InventarioDiario::where('tInvIniFechaRep',$date)->update(['tInvIniStatus'=>1]);
	}

	public function verificarInventarioDiarioFinalizado(){
		$date = Carbon::now();
		$date = $date->format('Ymd');
		$statusInventario = InventarioDiario::where('tInvIniFechaRep',$date)->where('tInvIniStatus',1)->get()->toArray();
		if ($statusInventario == []) {
			return 1;
		}else{
			return 0;
		}
	}



	public function obtenerProductosenInventario(){


		$date = Carbon::now();
		$date = $date->format('Ymd');

		$productoReporte=DB::raw("SELECT tInvIniFolioProd,cProInterCodigo,cProFolio,cHisProCodigo,cProDesTicket,CASE WHEN cHisProCantidad IS NULL THEN 0 ELSE cHisProCantidad END AS 'cHisProCantidad',CASE WHEN tInvIniValorMinimo IS NULL THEN 0 ELSE tInvIniValorMinimo END AS 'tInvIniValorMinimo',tInvIniValorMinimo-cHisProCantidad as 'DIFERENCIA',CASE WHEN cHisProCantidad<tInvIniValorMinimo THEN 1 ELSE 0 END AS 'NECESITA'  FROM(
			SELECT cHisProCodigo,SUM(cHisProCantidad) AS cHisProCantidad,cHisProDesTicket FROM tHisProductos WHERE cHisProFechaRep = ".$date."  GROUP BY cHisProCodigo,cHisProDesTicket)AS T1
			FULL OUTER JOIN(SELECT * FROM tProductos )AS T2
			ON T1.cHisProCodigo=T2.cProCodigo FULL OUTER JOIN
			(SELECT * FROM tInventarioDiario  WHERE tInvIniFechaRep =".$date.") AS T3 ON T2.cProFolio = T3.tInvIniFolioProd");

		$productoReporte=db::select($productoReporte);
		$productoReporte=json_decode(json_encode($productoReporte),true);
		return $productoReporte;
	}

	public function obtenerProductosenInventarioPDF(){
		$date = Carbon::now();
		$dateComplete = $date->format('Y-m-d');
		$date = $date->format('Ymd');

		$productoReporte=DB::raw("SELECT tInvIniFolioProd,cProInterCodigo,cProFolio,cHisProCodigo,cProDesTicket,CASE WHEN cHisProCantidad IS NULL THEN 0 ELSE cHisProCantidad END AS 'cHisProCantidad',CASE WHEN tInvIniValorMinimo IS NULL THEN 0 ELSE tInvIniValorMinimo END AS 'tInvIniValorMinimo',tInvIniValorMinimo-cHisProCantidad as 'DIFERENCIA',CASE WHEN cHisProCantidad<tInvIniValorMinimo THEN 1 ELSE 0 END AS 'NECESITA'  FROM(
			SELECT cHisProCodigo,SUM(cHisProCantidad) AS cHisProCantidad,cHisProDesTicket FROM tHisProductos WHERE cHisProFechaRep = ".$date."  GROUP BY cHisProCodigo,cHisProDesTicket)AS T1
			FULL OUTER JOIN(SELECT * FROM tProductos )AS T2
			ON T1.cHisProCodigo=T2.cProCodigo FULL OUTER JOIN
			(SELECT * FROM tInventarioDiario  WHERE tInvIniFechaRep =".$date.") AS T3 ON T2.cProFolio = T3.tInvIniFolioProd");

		$productoReporte=db::select($productoReporte);
		$productoReporte=json_decode(json_encode($productoReporte),true);

		$datosempresa=DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
		$empresaSel=db::select($datosempresa);
		$empresaSel=json_decode(json_encode($empresaSel),true);

		$dataGPDF= ['productos'=>$productoReporte];

		$info_encabezado=['fecha'=>$dateComplete,'empresaSeleccionada'=>$empresaSel];


		$header=view()->make('/InventarioDiario/header')->with('datosencabezado',$info_encabezado)->render();
		$pdf=PDF::loadview('/InventarioDiario/body',compact('dataGPDF'));
		$pdf->setOption('header-html',$header)
		->setOption('header-right','PAGINA [page] DE [toPage]')
		->setOption('header-font-name','Courier')
		->setOption('header-font-size','8')
		->setOption('margin-top',38)
		->setOption('margin-bottom',10)
		->setOption('margin-left',10)
		->setOption('margin-right',10);
		return $pdf->download('reporteProductosInventario.pdf');
//		return $productoReporte;
	}

	public function obtenerProductosenInventarioEXCEL(){
		$date = Carbon::now();
		$dateComplete = $date->format('Y-m-d');
		$date = $date->format('Ymd');

		$productoReporte=DB::raw("SELECT tInvIniFolioProd,cProInterCodigo,cProFolio,cHisProCodigo,cProDesTicket,CASE WHEN cHisProCantidad IS NULL THEN 0 ELSE cHisProCantidad END AS 'cHisProCantidad',CASE WHEN tInvIniValorMinimo IS NULL THEN 0 ELSE tInvIniValorMinimo END AS 'tInvIniValorMinimo',tInvIniValorMinimo-cHisProCantidad as 'DIFERENCIA',CASE WHEN cHisProCantidad<tInvIniValorMinimo THEN 1 ELSE 0 END AS 'NECESITA'  FROM(
			SELECT cHisProCodigo,SUM(cHisProCantidad) AS cHisProCantidad,cHisProDesTicket FROM tHisProductos WHERE cHisProFechaRep = ".$date."  GROUP BY cHisProCodigo,cHisProDesTicket)AS T1
			FULL OUTER JOIN(SELECT * FROM tProductos )AS T2
			ON T1.cHisProCodigo=T2.cProCodigo FULL OUTER JOIN
			(SELECT * FROM tInventarioDiario  WHERE tInvIniFechaRep = ".$date.") AS T3 ON T2.cProFolio = T3.tInvIniFolioProd");

		$productoReporte=db::select($productoReporte);
		$productoReporte=json_decode(json_encode($productoReporte),true);

		$datosempresa=DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
		$empresaSel=db::select($datosempresa);
		$empresaSel=json_decode(json_encode($empresaSel),true);

		$dataGPDF= ['productos'=>$productoReporte];

		$info_encabezado=['fecha'=>$dateComplete,'empresaSeleccionada'=>$empresaSel];


		Excel::create('ventas', function($excel)use (
			$info_encabezado,
			$dataGPDF
		){

			$excel->sheet('ventas', function($sheet)use(
				$info_encabezado,
				$dataGPDF
			){

				$sheet->loadView("/InventarioDiario/indexEXCEL")
				->with(compact(
					'info_encabezado',
					'dataGPDF'
				));


			});
		})->download('csv');


//		return $productoReporte;
	}


	public function FuncionVerificarRegistrosInventario(Request $request){
		{

			$VerificarRegistrosTablaInventario= InventarioAcumulable::get()->toArray();
			$date = Carbon::now();
			$date = $date->format('Ymd');


			if ($VerificarRegistrosTablaInventario == []) {
				$queryInsertarInventario="INSERT INTO tInventario (cInvFechaRep,cInvCodProd,cInvCant,cInvCantAcumulada,cInvTipoMov)VALUES";

				try {
					$VAproductos=Productos::select("cProCodigo")->get()->toArray();
					foreach ($VAproductos as $key) {
						$queryInsertarInventario =$queryInsertarInventario."(".$date.",'".$key['cProCodigo']."',0,0,0),";

					}

					$queryInsertarInventario=substr($queryInsertarInventario,0,-1);
					DB::statement($queryInsertarInventario);


				} catch (Exception $e) {

				}

			}else{

			}


		}
	}




}
