<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuracion;
use App\mesasHoy;
use App\Asistencia;
use App\FormasPago;
use DB;
use App\bitacora;
use App\historialMesas;
use App\Cortes;
use Carbon\Carbon;

class CierreController extends Controller
{


    public function indexCierre(){

    	$permisoMesas= null;
    	
        $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();
        $MesActuales=mesasHoy::get()->toArray();
        if ($MesActuales == []) {
        	$permisoMesas=0;
        }else{
        	$permisoMesas=1; 
        }

        date_default_timezone_set('America/Chihuahua'); 
        $date=Carbon::now()->format('Ymd');

        if ($Configuracion[0]['cConFechaVal'] == $date) {

        	$autorizacion="1";

        }else{ 
        	$autorizacion="0"; 
        } 

        $mensaje=['mensaje'=>$autorizacion,'mesas'=>$permisoMesas];
        return view('Cierreturno.index')->with('mensaje',$mensaje);

    }

    public function updateBitacora(Request $request){



     date_default_timezone_set('America/Chihuahua'); 
     $date=Carbon::now()->subDays(1)->format('Ymd');

     $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();


     $mesas=historialMesas::where('cHisMesFechaRep',$Configuracion[0]['cConFechaVal'])->selectRaw('CAST(SUM(cHisMesTotal) as decimal(16,2)) as total,CAST(SUM(cHisMesSubTotal) as decimal(16,2)) as subtotal')->get()->toArray();

     $bitacoracheck=bitacora::where('cBitFechaRep',$Configuracion[0]['cConFechaVal'])->get()->toArray();
     if ($bitacoracheck == []) {
       bitacora::create(['cBitFechaRep'=>$Configuracion[0]['cConFechaVal'],'cBitTotalVenta'=>$mesas[0]['total'],'cBitSubTotalVenta'=>$mesas[0]['subtotal'],'cBitLlenado'=>0,'cBitSincro'=>0]);   
   }

} 

public function updateAsistencia(){
   $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();

   Asistencia::where('cRegFechaRep',$Configuracion[0]['cConFechaVal'])->update(['cRegBandera'=>5]);
   $newdate=Carbon::now()->format('Ymd');
   $dateFechaVal=Carbon::parse($Configuracion[0]['cConFechaVal'])->format('Y-m-d');

   $_buscarEmpleadosConChecadas=Asistencia::select('cRegIdEmp')->where('cRegFechaRep',$Configuracion[0]['cConFechaVal'])->get()->toArray();
   $empleadosChecados=NULL;

   if ($_buscarEmpleadosConChecadas == []) {
         DB::statement("INSERT INTO tRegAsistencia  (cRegIdEmp,cRegNombre,cRegFechaRep,cRegFechaEntrada,cRegBandera,cRegPuesto,cRegPerfil,cRegSincro,cRegHorasJornada,cRegHorasDescanso)SELECT cUsuCodigo,cUsuApellido+' '+cUsuNombre,'".$Configuracion[0]['cConFechaVal']."','".$dateFechaVal."',5,cUsuPuesto1Desc,cUsuPuesto1Perfil,0,0,0  FROM tUsuarios  WHERE cUsuActivo = 1");
   }else{
       foreach ($_buscarEmpleadosConChecadas as $key ) {
         $empleadosChecados = $empleadosChecados.$key['cRegIdEmp'].","; 
     }

     $empleadosChecados = substr($empleadosChecados,0,-1);

     DB::statement("INSERT INTO tRegAsistencia  (cRegIdEmp,cRegNombre,cRegFechaRep,cRegFechaEntrada,cRegBandera,cRegPuesto,cRegPerfil,cRegSincro,cRegHorasJornada,cRegHorasDescanso)SELECT cUsuCodigo,cUsuApellido+' '+cUsuNombre,'".$Configuracion[0]['cConFechaVal']."','".$dateFechaVal."',5,cUsuPuesto1Desc,cUsuPuesto1Perfil,0,0,0  FROM tUsuarios  WHERE cUsuCodigo NOT IN (".$empleadosChecados.") AND cUsuActivo = 1");
 }
}



public function updateNuevaFeechatrabajo (){
  $newdate=Carbon::now()->format('Ymd');
  DB::statement("update tConfiguracion  SET cConFechaVal ='".$newdate."'");

}

public function generarRespaldo(){
  DB::statement("update tConfiguracion  SET cConBanderaRespaldo =1");
}

public function generarSincroEmpleados(){
    DB::statement("update tConfigNube  SET cBDEmpleados =1");
}



public function updateCortes(){

    $FORMASPAGO=FormasPago::select('cCarForParidad')->where('cCatForFolio',3)->get()->toArray();
    $CONFIGURACION=Configuracion::select('cConFechaVal')->get()->toArray();
    $MESAS= historialMesas::selectRaw('SUM(chisMesTotal) as total')->where('cHisMesFechaRep',$CONFIGURACION[0]['cConFechaVal'])->get()->toArray();

    $FIRST=historialMesas::select('cHisMesTicket')->where('cHisMesFechaRep',$CONFIGURACION[0]['cConFechaVal'])->first()->get()->toArray();
    $LAST=historialMesas::select('cHisMesTicket')->where('cHisMesFechaRep',$CONFIGURACION[0]['cConFechaVal'])->latest('cHisMesFolio')->limit(1)->get()->toArray();
    
    $dateFechaVal=Carbon::parse($CONFIGURACION[0]['cConFechaVal'])->format('Y-m-d');
    Cortes::create(['cCorFecha'=>$dateFechaVal,
        'cCorTotalVenta'=>$MESAS[0]['total'],
        'cCorTipoCambio'=>$FORMASPAGO[0]['cCarForParidad'],
        'cCorIniTicket'=>$FIRST[0]['cHisMesTicket'], 
        'cCorFinTicket'=>$LAST[0]['cHisMesTicket']]);






}




//    public function respadoSQL(){
// $fecha=Carbon::now()->format('Ymd');    
//         $pathFile="'C:\Users\Elkin\Dropbox/respaldosPOSX'";
//         $descargarBakup=DB::unprepared(DB::raw("BACKUP DATABASE CRRTS2 TO DISK = ".$pathFile." ")); //CRRTS2
//    }

public function getFormasdepago(){

    $formas=FormasPago::get()->toArray();
    $pivote=array();

    foreach ($formas as $key ) {
        $pivote[]=array(
            'cCatForFolio'=>$key['cCatForFolio'],
            'cCatForId'=>$key['cCatForId'],
            'cCatForDesc'=>$key['cCatForDesc'],
            'cCatForActivo'=>$key['cCatForActivo'],
            'cCarForParidad'=>$key['cCarForParidad'],
            'cCarForReportes'=>$key['cCarForReportes'],
            'cCatForFolioAgrupador'=>$key['cCatForFolioAgrupador'],
            'cCatCantidad'=>null,
            'cCatEstatus'=>0
        );
    }

    
    return['formas'=>$pivote];
}

}
