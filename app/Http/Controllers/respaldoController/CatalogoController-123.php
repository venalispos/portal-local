<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Configuracion;
use App\Perfiles;
use App\SubCategorias;
use App\Categorias;
use App\Productos;
use Carbon\Carbon;
use App\Clasificacion;

class CatalogoController extends Controller
{
	public function indexEmpleados(){
		return view ('catalogos.empleados.index');
	}

	public function IndexProductos(){
		return view('catalogos.productos.index');
	}
	public function getUser(){



		$configuracion=Configuracion::select('cConModUsuarios')->get()->toArray();

		$configuracion=$configuracion[0]['cConModUsuarios'];
		$usuarios=null;
		if ($configuracion == 0) {
			$usuarios = User::select('cUsuFolio','cUsuCodigo','cUsuClave','cUsuAccesoAdmin','cUsuNombre','tUsuReiniciar')->get()->toArray();
		}else{
			$usuarios = User::select('cUsuFolio','cUsuCodigo','cUsuNombre', 'cUsuApellido', 'cUsuActivo', 'cUsuClave', 'cUsuPuesto1Activo', 'cUsuPuesto1Desc','cUsuPuesto1Perfil', 'cUsuPuesto2Activo', 'cUsuPuesto2Desc', 'cUsuPuesto2Perfil','cUsuPuesto3Activo', 'cUsuPuesto3Desc','cUsuPuesto3Perfil', 'cUsuNumSeguro', 'cUsuDireccion', 'cUsuTelefono', 'cUsuFechNac', 'cUsuAccesoAdmin', 'cUsuPermisosPortal','tUsuReiniciar')->get()->toArray();
		}
		$perfiles= Perfiles::get()->toArray();

		return ['usuarios'=>$usuarios,'activo'=>$configuracion,'perfiles'=>$perfiles];
	}

	public function addNewEmploye(Request $request){
		$USUARIOEXISTE=NULL;
		$UsuarioId=User::where('cUsuCodigo',$request['cUsuCodigo'])->get()->toArray();


		if($UsuarioId== []){
			if ($request['tUsuReiniciar'] == 0) {

			User::create(['cUsuCodigo'=>$request['cUsuCodigo'],
			'cUsuNombre'=>$request['cUsuNombre'],
			'cUsuApellido'=>$request['cUsuApellido'],
			'cUsuActivo'=>$request['cUsuActivo'],
			'cUsuClave'=>$request['cUsuClave'],
			'cUsuPuesto1Activo'=>$request['cUsuPuesto1Activo'],
			'cUsuPuesto1Desc'=>'NA',
			'cUsuPuesto1Perfil'=>$request['cUsuPuesto1Perfil'],
			'cUsuPuesto2Activo'=>$request['cUsuPuesto2Activo'],
			'cUsuPuesto2Desc'=>$request['cUsuPuesto2Desc'],
			'cUsuPuesto2Perfil'=>$request['cUsuPuesto2Perfil'],
			'cUsuPuesto3Activo'=>$request['cUsuPuesto3Activo'],
			'cUsuPuesto3Desc'=>$request['cUsuPuesto3Desc'],
			'cUsuPuesto3Perfil'=>$request['cUsuPuesto3Perfil'],
			'cUsuNumSeguro'=>$request['cUsuNumSeguro'],
			'cUsuDireccion'=>$request['cUsuDireccion'],
			'cUsuTelefono'=>$request['cUsuTelefono'],
			'cUsuFechNac'=>$request['cUsuFechNac'],
			'cUsuAccesoAdmin'=>$request['cUsuAccesoAdmin'],
			'cUsuAdminPass'=>0,
			'cUsuPermisosPortal'=>$request['cUsuPermisosPortal'],
			'tUsuReiniciar'=>$request['tUsuReiniciar']]);

		}else{


			$tocken="IV5cUfVbvBiWLXgNVSozih0zS3nHTQvM9a25vkGIK1HoK8MTHGtSikkIexlr";
			User::create(['cUsuCodigo'=>$request['cUsuCodigo'],
			'cUsuNombre'=>$request['cUsuNombre'],
			'cUsuApellido'=>$request['cUsuApellido'],
			'cUsuActivo'=>$request['cUsuActivo'],
			'cUsuClave'=>$request['cUsuClave'],
			'cUsuPuesto1Activo'=>$request['cUsuPuesto1Activo'],
			'cUsuPuesto1Desc'=>'NA',
			'cUsuPuesto1Perfil'=>$request['cUsuPuesto1Perfil'],
			'cUsuPuesto2Activo'=>$request['cUsuPuesto2Activo'],
			'cUsuPuesto2Desc'=>$request['cUsuPuesto2Desc'],
			'cUsuPuesto2Perfil'=>$request['cUsuPuesto2Perfil'],
			'cUsuPuesto3Activo'=>$request['cUsuPuesto3Activo'],
			'cUsuPuesto3Desc'=>$request['cUsuPuesto3Desc'],
			'cUsuPuesto3Perfil'=>$request['cUsuPuesto3Perfil'],
			'cUsuNumSeguro'=>$request['cUsuNumSeguro'],
			'cUsuDireccion'=>$request['cUsuDireccion'],
			'cUsuTelefono'=>$request['cUsuTelefono'],
			'cUsuFechNac'=>$request['cUsuFechNac'],
			'cUsuAccesoAdmin'=>$request['cUsuAccesoAdmin'],
			'cUsuAdminPass'=>$temporal,
			'remember_token'=>$tocken,
			'cUsuPermisosPortal'=>$request['cUsuPermisosPortal'],
			'tUsuReiniciar'=>$request['tUsuReiniciar']]);

		}
		}else{
			$USUARIOEXISTE	= 1;
		}

		return ['existe'=>$USUARIOEXISTE];
	}

	public function updateEmployee(Request $request){


		User::where('cUsuFolio',$request['cUsuFolio'])->update(['cUsuCodigo'=>$request['cUsuCodigo'],
			'cUsuNombre'=>$request['cUsuNombre'],
			'cUsuApellido'=>$request['cUsuApellido'],
			'cUsuActivo'=>$request['cUsuActivo'],
			'cUsuClave'=>$request['cUsuClave'],
			'cUsuPuesto1Activo'=>$request['cUsuPuesto1Activo'],
			'cUsuPuesto1Desc'=>$request['cUsuPuesto1Desc'],
			'cUsuPuesto1Perfil'=>$request['cUsuPuesto1Perfil'],
			'cUsuPuesto2Activo'=>$request['cUsuPuesto2Activo'],
			'cUsuPuesto2Desc'=>$request['cUsuPuesto2Desc'],
			'cUsuPuesto2Perfil'=>$request['cUsuPuesto2Perfil'],
			'cUsuPuesto3Activo'=>$request['cUsuPuesto3Activo'],
			'cUsuPuesto3Desc'=>$request['cUsuPuesto3Desc'],
			'cUsuPuesto3Perfil'=>$request['cUsuPuesto3Perfil'],
			'cUsuNumSeguro'=>$request['cUsuNumSeguro'],
			'cUsuDireccion'=>$request['cUsuDireccion'],
			'cUsuTelefono'=>$request['cUsuTelefono'],
			'cUsuFechNac'=>$request['cUsuFechNac'],
			'cUsuAccesoAdmin'=>$request['cUsuAccesoAdmin'],
			'cUsuPermisosPortal'=>$request['cUsuPermisosPortal'],
			'tUsuReiniciar'=>$request['tUsuReiniciar']]);
	}

	public function getProductos(){
		$productos=Productos::get()->toArray();
		$categorias=Categorias::get()->toArray();
		$subcategorias=SubCategorias::get()->toArray();
		$configuracion=Configuracion::get()->toArray();
		$clasificacion=Clasificacion::get()->toArray();
		return ['productos'=>$productos,'categorias'=>$categorias,'subcategorias'=>$subcategorias,'configuracion'=>$configuracion,'clasificacion'=>$clasificacion];
	}


	public function FunValidaProducto(Request $request){
		$_return_data=['datos'=>2];
		return ['datos'=>$_return_data];

	}
}
