<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Configuracion; 
use App\Perfiles;
use App\SubCategorias;
use App\Categorias; 
use App\Productos;
use Carbon\Carbon;  
use App\Clasificacion;    
use DB;  

class CatalogoController extends Controller
{ 
	public function indexEmpleados(){
		return view ('catalogos.empleados.index');
	} 

	public function IndexProductos(){
		return view('catalogos.productos.index');
	}
	public function getUser(){



		$configuracion=Configuracion::select('cConModUsuarios')->get()->toArray();
		$configuracion=$configuracion[0]['cConModUsuarios'];
		$usuarios=null;

		$usuarios = User::select('cUsuFolio','cUsuCodigo','cUsuNombre', 'cUsuApellido', 'cUsuActivo', 'cUsuClave', 'cUsuPuesto1Activo', 'cUsuPuesto1Desc','cUsuPuesto1Perfil', 'cUsuPuesto2Activo', 'cUsuPuesto2Desc', 'cUsuPuesto2Perfil','cUsuPuesto3Activo', 'cUsuPuesto3Desc','cUsuPuesto3Perfil', 'cUsuNumSeguro', 'cUsuDireccion', 'cUsuTelefono', 'cUsuFechNac', 'cUsuAccesoAdmin', 'cUsuPermisosPortal','tUsuReiniciar')->get()->toArray();
		
		$perfiles= Perfiles::get()->toArray();

		return ['usuarios'=>$usuarios,'activo'=>$configuracion,'perfiles'=>$perfiles]; 
	} 

	public function addNewEmploye(Request $request){

$USUARIOEXISTE =0;
		$UsuarioId=User::where('cUsuPortalWebUser',$request['cUsuPortalWebUser'])->where('cUsuPortalWebUser',"<>",NULL)->get()->toArray();
		
		if($UsuarioId== []){
			$query="INSERT INTO tUsuarios (cUsuCodigo, cUsuNombre, cUsuApellido,cUsuActivo,cUsuClave, cUsuPuesto1Activo, cUsuPuesto1Desc, cUsuPuesto1Perfil, cUsuPuesto2Activo, cUsuPuesto2Desc, cUsuPuesto2Perfil,cUsuPuesto3Activo,cUsuPuesto3Desc, cUsuPuesto3Perfil, cUsuNumSeguro, cUsuDireccion, cUsuTelefono, cUsuFechNac, cUsuAccesoAdmin, cUsuAdminPass, remember_token,tUsuReiniciar,cUsuPortalWebUser)VALUES(".$request['cUsuCodigo'].",'".$request['cUsuNombre']."','".$request['cUsuApellido']."'";
			if ($request['cUsuActivo'] ==1) {
				$query=$query.",1,".$request['cUsuClave']."";
			}else{
				$query=$query.",0,0";
			}
			if ($request['cUsuPuesto1Activo'] ==1) {
				$query=$query.",1,'".$request['cUsuPuesto1Perfil']['cPerDesc']."',".$request['cUsuPuesto1Perfil']['cPerId'];
			}else{
				$query=$query.",0,'',0";
			}

			if ($request['cUsuPuesto2Activo'] ==1) {
				$query=$query.",1,'".$request['cUsuPuesto2Perfil']['cPerDesc']."',".$request['cUsuPuesto2Perfil']['cPerId'];
			}else{
				$query=$query.",0,'',0";
			}

			if ($request['cUsuPuesto3Activo'] ==1) {
				$query=$query.",1,'".$request['cUsuPuesto3Perfil']['cPerDesc']."',".$request['cUsuPuesto3Perfil']['cPerId'];
			}else{
				$query=$query.",0,'',0";
			}
			$query=$query.",'".$request['cUsuNumSeguro']."','".$request['cUsuDireccion']."','".$request['cUsuTelefono']."','".$request['cUsuFechNac']."'";

			$temporal= bcrypt('temporal');
			$tocken="IV5cUfVbvBiWLXgNVSozih0zS3nHTQvM9a25vkGIK1HoK8MTHGtSikkIexlr";
			if ($request['cUsuAccesoAdmin'] ==1 ) {
				$query=$query.",1,'".$temporal."','".$tocken."',1,'".$request['cUsuPortalWebUser']."')";
			}else{
				$query=$query.",0,'','',0,'')";
			}

	DB::statement($query);

		}else{
			$USUARIOEXISTE	= 1;
		}

		return ['existe'=>$USUARIOEXISTE];

	} 

	public function updateEmployee(Request $request){


		User::where('cUsuFolio',$request['cUsuFolio'])->update(['cUsuCodigo'=>$request['cUsuCodigo'],
			'cUsuNombre'=>$request['cUsuNombre'],
			'cUsuApellido'=>$request['cUsuApellido'],  
			'cUsuActivo'=>$request['cUsuActivo'],
			'cUsuClave'=>$request['cUsuClave'],
			'cUsuPuesto1Activo'=>$request['cUsuPuesto1Activo'],
			'cUsuPuesto1Desc'=>$request['cUsuPuesto1Desc'],
			'cUsuPuesto1Perfil'=>$request['cUsuPuesto1Perfil'],
			'cUsuPuesto2Activo'=>$request['cUsuPuesto2Activo'],
			'cUsuPuesto2Desc'=>$request['cUsuPuesto2Desc'],
			'cUsuPuesto2Perfil'=>$request['cUsuPuesto2Perfil'],
			'cUsuPuesto3Activo'=>$request['cUsuPuesto3Activo'],
			'cUsuPuesto3Desc'=>$request['cUsuPuesto3Desc'],
			'cUsuPuesto3Perfil'=>$request['cUsuPuesto3Perfil'],
			'cUsuNumSeguro'=>$request['cUsuNumSeguro'],
			'cUsuDireccion'=>$request['cUsuDireccion'],
			'cUsuTelefono'=>$request['cUsuTelefono'],
			'cUsuFechNac'=>$request['cUsuFechNac'],
			'cUsuAccesoAdmin'=>$request['cUsuAccesoAdmin'],
			'cUsuPermisosPortal'=>$request['cUsuPermisosPortal'],
			'tUsuReiniciar'=>$request['tUsuReiniciar']]);
	}

	public function getProductos(){
		$productos=Productos::selectRAW("*, 0 as cantidad")->get()->toArray();
		$categorias=Categorias::get()->toArray();
		$subcategorias=SubCategorias::get()->toArray();
		$configuracion=Configuracion::get()->toArray();
		$clasificacion=Clasificacion::get()->toArray();
		return ['productos'=>$productos,'categorias'=>$categorias,'subcategorias'=>$subcategorias,'configuracion'=>$configuracion,'clasificacion'=>$clasificacion];
	}
}
