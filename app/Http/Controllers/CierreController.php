<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuracion;
use App\mesasHoy;
use App\Asistencia;
use App\FormasPago;
use DB;
use App\bitacora;
use App\historialMesas;
use App\Cortes;
use App\categoriasFormaPago;
use App\HistorialFormasPagos;
use Carbon\Carbon;

class CierreController extends Controller
{

public function __construct(){
        // $this->middleware('auth');
        // $configuracion=Configuracion::select('cConPortalIdioma')->get()->toArray();
        // \App::setLocale($configuracion[0]['cConPortalIdioma']);

    }
    public function indexCierre(){

        $permisoMesas= null;

        $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();
        $MesActuales=mesasHoy::get()->toArray();
        if ($MesActuales == []) {
            $permisoMesas=0;
        }else{
            $permisoMesas=1;
        }
        $FechaActual=Carbon::parse($Configuracion[0]['cConFechaVal'])->format("Y-m-d");
        $fechaSiguiente=Carbon::now()->format("Y-m-d");

        $date=Carbon::now()->format('Ymd');

        if ($Configuracion[0]['cConFechaVal'] == $date) {

            $autorizacion="1";

        }else{
            $autorizacion="0";
        }

        $mensaje=['mensaje'=>$autorizacion,'mesas'=>$permisoMesas,'fecha'=>$FechaActual,'fechaSiguiente'=>$fechaSiguiente];
        return view('Cierreturno.index')->with('mensaje',$mensaje);

    }

    public function updateBitacora(Request $request){
     $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();
     if ($Configuracion[0]['cConFechaVal'] == Carbon::now()->format('Ymd')) {
       return 1;
     }else{

         $mesas=historialMesas::where('cHisMesFechaRep',$Configuracion[0]['cConFechaVal'])->selectRaw('CAST(SUM(cHisMesTotal) as decimal(16,2)) as total,CAST(SUM(cHisMesSubTotal) as decimal(16,2)) as subtotal')->get()->toArray();

         $bitacoracheck=bitacora::where('cBitFechaRep',$Configuracion[0]['cConFechaVal'])->get()->toArray();
         if ($bitacoracheck == []) {
           $MezclaX="SELECT SUM(cHisMesTotal) as'cHisMestotal' FROM (SELECT TB4.cHisMesTicket,cHisMesNombre,EFECTIVO,OTRA,cHisMesFactSoli,cHisMesFechaRep,CASE WHEN  EFECTIVO>0 AND OTRA= 0 AND cHisMesFactSoli=0  THEN (cHisMesTotalImpuesto1) ELSE cHisMesTotalImpuesto1  END AS 'cHisMesTotalImpuesto1',CASE WHEN   EFECTIVO>0 AND OTRA= 0  AND cHisMesFactSoli=0 THEN (cHisMesTotal*(cConPorcentajeTiraX*0.01)) ELSE cHisMesTotal  END AS 'cHisMesTotal',CASE WHEN   EFECTIVO>0 AND OTRA= 0  AND cHisMesFactSoli =0 THEN (cHisMesSubTotal*(cConPorcentajeTiraX*0.01)) ELSE cHisMesSubTotal  END AS 'cHisMesSubTotal'    FROM (SELECT cHisMesTicket,SUM(efectivo) AS 'EFECTIVO',SUM(otra) AS 'OTRA' FROM(				SELECT DISTINCT COUNT(cHisForTicket) AS 'CONTADOR' ,cHisMesTicket,CASE WHEN cHisForId=1 THEN (1)ELSE 0 END AS 'efectivo',				CASE WHEN cHisForId NOT IN (1) THEN (1)ELSE 0 END AS 'otra' FROM 				(SELECT * FROM tHisFormaPago INNER JOIN tHisMesas ON cHisMesTicket = cHisForTicket)AS TB1 group by cHisMesTicket,cHisForId)AS TB3 group by TB3.cHisMesTicket)AS TB4  FULL JOIN (SELECT cHisMesTicket,cHisMesNombre,cHisMesFechaRep,cHisMesSubTotal,cHisMesTotalImpuesto1,cHisMesTotal,cHisMesFactSoli,cConPorcentajeTiraX  FROM tHisMesas CROSS JOIN tConfiguracion  where cHisMesFechaRep     between ".$Configuracion[0]['cConFechaVal']." and ".$Configuracion[0]['cConFechaVal'].")AS TB5 ON TB4.cHisMesTicket =TB5.cHisMesTicket WHERE TB5.cHisMesTicket IS NOT NULL) AS TBF";
           $bitacoraData=DB::raw($MezclaX);
           	$bitacoraData=db::select($bitacoraData);
            $bitacoraData=json_decode(json_encode($bitacoraData),true);

            if ($bitacoraData[0]['cHisMestotal'] == null) {
              $bitacoraData[0]['cHisMestotal'] =0;
            }
           bitacora::create(['cBitFechaRep'=>$Configuracion[0]['cConFechaVal'],'cBitTotalVenta'=>$mesas[0]['total'],'cBitSubTotalVenta'=>$mesas[0]['subtotal'],'cBitLlenado'=>0,'cBitSincro'=>0,'cBitParcial'=>$bitacoraData[0]['cHisMestotal']]);
       }
   }

}

public function updateAsistencia(){
  $Configuracion=Configuracion::first();
  $Configuracion->cConBanderaCierre=1;
  $Configuracion->save();
  return 1;
}



public function updateNuevaFeechatrabajo (Request $request){
  $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();
  if ($Configuracion[0]['cConFechaVal'] == Carbon::now()->format('Ymd')) {
    echo "La fecha es la misma y no es valida";
  }else{
    echo $request['fecha'];
   $newdate=Carbon::now()->format('Ymd');
   DB::statement("update tConfiguracion  SET cConFechaVal ='".$newdate."'");
 }
}

public function generarRespaldo(){
  $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();
  if ($Configuracion[0]['cConFechaVal'] == Carbon::now()->format('Ymd')) {
        echo "La fecha es la misma y no es valida";
  }else{
        DB::statement("update tConfiguracion  SET cConBanderaRespaldo =1");
      }
}

public function generarSincroEmpleados(){
  $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();
  if ($Configuracion[0]['cConFechaVal'] == Carbon::now()->format('Ymd')) {
    echo "La fecha es la misma y no es valida";
  }else{
      DB::statement("update tConfigNube  SET cBDEmpleados =1");
    }
}

public function FungenerarSincroCorte(Request $request){
  $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();
  if ($Configuracion[0]['cConFechaVal'] == Carbon::now()->format('Ymd')) {
    echo "La fecha es la misma y no es valida";
  }else{
          $cantidadSinEfectivo = 0;
           $fechaCierre= $request['fecha'];
           $cierre= array();
           $informacionInsertar= array();
           $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();
          $formaspago = categoriasFormaPago::selectRaw('*, 0 as cantidad')->get()->toArray();
          $formaspagoAgrupado =categoriasFormaPago::selectRaw('COUNT(cCarForParidad),0 as cantidad,cCarForParidad,cCatForAgrupadorCorte')
          ->groupBy('cCatForAgrupadorCorte','cCarForParidad')->get()->toArray();
          $historialformasPago = HistorialFormasPagos::selectRaw(" SUM(cHisForCant)as 'cantidad',cHisForId ")->where('cHisForFechaRep',$Configuracion[0]['cConFechaVal'])->whereNotIn('cHisForId',[1])->groupBy('cHisForId')->get()->toArray();
            foreach ($formaspago as $key ) {
              foreach ($historialformasPago as $historial) {
                if ($key['cCatForId'] == $historial['cHisForId']) {
                  $key['cantidad'] += $historial['cantidad'];
                }
              }
              $cantidadSinEfectivo +=$key['cantidad'];
              array_push($cierre,$key);
            }
            $query="SELECT SUM(cHisProPrecio)as cantidad FROM tHisProductos WHERE cHisProFechaRep=".$Configuracion[0]['cConFechaVal']."";
            $ventaDiaria=DB::raw($query);
             $ventaDiaria=db::select($ventaDiaria);
             $ventaDiaria=json_decode(json_encode($ventaDiaria),true);
             $cantidadEfectivo = floatval($ventaDiaria[0]['cantidad'])-floatval($cantidadSinEfectivo);
             foreach ($formaspagoAgrupado as $key ) {
               foreach ($cierre as $temporal) {
                 if ($key['cCatForAgrupadorCorte'] == $temporal['cCatForAgrupadorCorte']) {
                   $key['cantidad'] += $temporal['cantidad'];
                 } if ($key['cCatForAgrupadorCorte'] == 3) {
                    $key['cantidad'] = $cantidadEfectivo;
                 }
               }
               array_push($informacionInsertar,$key);
             }
             $queryInsert="INSERT INTO tHisPagosAgrupados( cHisPagAgrupador, cHisPagMonto, cHisPagFechaRep, cHisPagParidad)VALUES";
             foreach ($informacionInsertar as $key) {
               $queryInsert .="(".$key['cCatForAgrupadorCorte'].",".$key['cantidad'].",".$Configuracion[0]['cConFechaVal'].",".$key['cCarForParidad']."),";
             }
             $queryInsert = substr($queryInsert, 0, -1);
             $query="SELECT * FROM tHisPagosAgrupados WHERE cHisPagFechaRep=".$Configuracion[0]['cConFechaVal']."";
             $diacapturado=DB::raw($query);
              $diacapturado=db::select($diacapturado);
              $diacapturado=json_decode(json_encode($diacapturado),true);
              if ($diacapturado  == []) {
              DB::statement($queryInsert);
              }
        }
}



public function updateCortes(){

  $Configuracion=Configuracion::select('cConFechaVal')->get()->toArray();
  if ($Configuracion[0]['cConFechaVal'] == Carbon::now()->format('Ymd')) {
    echo "La fecha es la misma y no es valida";
  }else{

          $FORMASPAGO=FormasPago::select('cCarForParidad')->where('cCatForFolio',3)->get()->toArray();
          $CONFIGURACION=Configuracion::select('cConFechaVal')->get()->toArray();
          $MESAS= historialMesas::selectRaw('SUM(chisMesTotal) as total')->where('cHisMesFechaRep',$CONFIGURACION[0]['cConFechaVal'])->get()->toArray();

          $FIRST=historialMesas::select('cHisMesTicket')->where('cHisMesFechaRep',$CONFIGURACION[0]['cConFechaVal'])->first()->get()->toArray();
          $LAST=historialMesas::select('cHisMesTicket')->where('cHisMesFechaRep',$CONFIGURACION[0]['cConFechaVal'])->latest('cHisMesFolio')->limit(1)->get()->toArray();

          $dateFechaVal=Carbon::parse($CONFIGURACION[0]['cConFechaVal'])->format('Y-m-d');
          Cortes::create(['cCorFecha'=>$dateFechaVal,
              'cCorTotalVenta'=>$MESAS[0]['total'],
              'cCorTipoCambio'=>$FORMASPAGO[0]['cCarForParidad'],
              'cCorIniTicket'=>$FIRST[0]['cHisMesTicket'],
              'cCorFinTicket'=>$LAST[0]['cHisMesTicket']]);

      }




}




//    public function respadoSQL(){
// $fecha=Carbon::now()->format('Ymd');
//         $pathFile="'C:\Users\Elkin\Dropbox/respaldosPOSX'";
//         $descargarBakup=DB::unprepared(DB::raw("BACKUP DATABASE CRRTS2 TO DISK = ".$pathFile." ")); //CRRTS2
//    }

public function getFormasdepago(){

    $formas=FormasPago::get()->toArray();
    $pivote=array();

    foreach ($formas as $key ) {
        $pivote[]=array(
            'cCatForFolio'=>$key['cCatForFolio'],
            'cCatForId'=>$key['cCatForId'],
            'cCatForDesc'=>$key['cCatForDesc'],
            'cCatForActivo'=>$key['cCatForActivo'],
            'cCarForParidad'=>$key['cCarForParidad'],
            'cCarForReportes'=>$key['cCarForReportes'],
            'cCatForFolioAgrupador'=>$key['cCatForFolioAgrupador'],
            'cCatCantidad'=>null,
            'cCatEstatus'=>0
        );
    }


    return['formas'=>$pivote];
}

}
