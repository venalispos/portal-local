<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Cortes;
use App\Confignube;
use App\Tipopago;
use App\Subtipopago;
use App\Configuracion;
use App\Detallecorte;

class CortesController extends Controller
{
  public function cortesPrincipal(){
    return view('cortes/cortesPrincipal');
  }
  public function cortesObtener(){
    $cortes = Cortes::get();
    $cortes = json_decode($cortes,true);
    return ['cortes' => $cortes];
  }
  public function cortesData(){
    $confignube = Confignube::first();
    $confignube = json_decode($confignube,true);

    $tipopago = Tipopago::get();
    $tipopago = json_decode($tipopago,true);

    $subtipopago = Subtipopago::get();
    $subtipopago = json_decode($subtipopago,true);

    $configuracion = Configuracion::first();
    $configuracion = json_decode($configuracion,true);
    return ['confignube' => $confignube, 'tipopago'=>$tipopago, 'subtipopago'=>$subtipopago, 'configuracion'=>$configuracion];
  }
  public function cortesVer(Request $request){
    $idcorte = $request->all();
    $cortedetalle = Detallecorte::with('Subtipopago')->where('cidcorte', $idcorte['idcorte'])->get();
    $cortedetalle = json_decode($cortedetalle,true);
    return ['cortedetalle' => $cortedetalle];
  }
  public function cortesAgregar(Request $request){
    $corte = $request->input('corte');
    $bterminal1 = $request->input('bterminal1');
    $bterminal2 = $request->input('bterminal2');
    $sterminal1 = $request->input('sterminal1');
    $sterminal2 = $request->input('sterminal2');
    $billetes1 = $request->input('billetes1');
    $billetes2 = $request->input('billetes2');
    $billetes3 = $request->input('billetes3');
    $billetes4 = $request->input('billetes4');
    $billetes5 = $request->input('billetes5');
    $monedas1 = $request->input('monedas1');
    $monedas2 = $request->input('monedas2');
    $monedas3 = $request->input('monedas3');
    $monedas4 = $request->input('monedas4');
    $monedas5 = $request->input('monedas5');
    $monedas6 = $request->input('monedas6');
    $monedas7 = $request->input('monedas7');
    $cheque = $request->input('cheque');
    $dolares1 = $request->input('dolares1');
    $dolares2 = $request->input('dolares2');
    $dolares3 = $request->input('dolares3');
    $dolares4 = $request->input('dolares4');
    $dolares5 = $request->input('dolares5');
    $dolares6 = $request->input('dolares6');
    $dolares7 = $request->input('dolares7');
    $dolares8 = $request->input('dolares8');
    $dolares9 = $request->input('dolares9');
    $dolares10 = $request->input('dolares10');
    $dolares11 = $request->input('dolares11');
    $aplicacion = $request->input('aplicacion');
    $uber = $request->input('uber');
    $amexterminal1 = $request->input('amexterminal1');
    $amexterminal2 = $request->input('amexterminal2');

    $agregarCorte = Cortes::create([
      'cfechacorte'=>$corte['cfechacorte'],
      'cidsucursal'=>$corte['cidsucursal'],
      'ctotalcorte'=>$corte['ctotalcorte'],
      'cestatuscorte'=>1,
      'cdescripciongasto'=>$corte['cdescripciongasto'],
      'ctotalgasto'=>$corte['ctotalgasto'],
      'cusuariocorte'=>$corte['cusuariocorte'],
    ]);

    $idcorte=$agregarCorte->cidcorte;
    Detallecorte::create([
         'cidtipopago' => $bterminal1['cidtipopago'],
         'cdenominacion' => $bterminal1['cdenominacion'],
         'ccantidad' => $bterminal1['ccantidad'],
         'ctotal' => $bterminal1['ctotal'],
         'cidsubtipopago' => $bterminal1['cidsubtipopago'],
         'cnoreferencia' => $bterminal1['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $bterminal2['cidtipopago'],
         'cdenominacion' => $bterminal2['cdenominacion'],
         'ccantidad' => $bterminal2['ccantidad'],
         'ctotal' => $bterminal2['ctotal'],
         'cidsubtipopago' => $bterminal2['cidsubtipopago'],
         'cnoreferencia' => $bterminal2['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $sterminal1['cidtipopago'],
         'cdenominacion' => $sterminal1['cdenominacion'],
         'ccantidad' => $sterminal1['ccantidad'],
         'ctotal' => $sterminal1['ctotal'],
         'cidsubtipopago' => $sterminal1['cidsubtipopago'],
         'cnoreferencia' => $sterminal1['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

    Detallecorte::create([
         'cidtipopago' => $sterminal2['cidtipopago'],
         'cdenominacion' => $sterminal2['cdenominacion'],
         'ccantidad' => $sterminal2['ccantidad'],
         'ctotal' => $sterminal2['ctotal'],
         'cidsubtipopago' => $sterminal2['cidsubtipopago'],
         'cnoreferencia' => $sterminal2['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

    Detallecorte::create([
         'cidtipopago' => $billetes1['cidtipopago'],
         'cdenominacion' => $billetes1['cdenominacion'],
         'ccantidad' => $billetes1['ccantidad'],
         'ctotal' => $billetes1['ctotal'],
         'cidsubtipopago' => $billetes1['cidsubtipopago'],
         'cnoreferencia' => $billetes1['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

    Detallecorte::create([
         'cidtipopago' => $billetes2['cidtipopago'],
         'cdenominacion' => $billetes2['cdenominacion'],
         'ccantidad' => $billetes2['ccantidad'],
         'ctotal' => $billetes2['ctotal'],
         'cidsubtipopago' => $billetes2['cidsubtipopago'],
         'cnoreferencia' => $billetes2['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

    Detallecorte::create([
         'cidtipopago' => $billetes3['cidtipopago'],
         'cdenominacion' => $billetes3['cdenominacion'],
         'ccantidad' => $billetes3['ccantidad'],
         'ctotal' => $billetes3['ctotal'],
         'cidsubtipopago' => $billetes3['cidsubtipopago'],
         'cnoreferencia' => $billetes3['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

    Detallecorte::create([
         'cidtipopago' => $billetes4['cidtipopago'],
         'cdenominacion' => $billetes4['cdenominacion'],
         'ccantidad' => $billetes4['ccantidad'],
         'ctotal' => $billetes4['ctotal'],
         'cidsubtipopago' => $billetes4['cidsubtipopago'],
         'cnoreferencia' => $billetes4['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $billetes5['cidtipopago'],
         'cdenominacion' => $billetes5['cdenominacion'],
         'ccantidad' => $billetes5['ccantidad'],
         'ctotal' => $billetes5['ctotal'],
         'cidsubtipopago' => $billetes5['cidsubtipopago'],
         'cnoreferencia' => $billetes5['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $monedas1['cidtipopago'],
         'cdenominacion' => $monedas1['cdenominacion'],
         'ccantidad' => $monedas1['ccantidad'],
         'ctotal' => $monedas1['ctotal'],
         'cidsubtipopago' => $monedas1['cidsubtipopago'],
         'cnoreferencia' => $monedas1['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $monedas2['cidtipopago'],
         'cdenominacion' => $monedas2['cdenominacion'],
         'ccantidad' => $monedas2['ccantidad'],
         'ctotal' => $monedas2['ctotal'],
         'cidsubtipopago' => $monedas2['cidsubtipopago'],
         'cnoreferencia' => $monedas2['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $monedas3['cidtipopago'],
         'cdenominacion' => $monedas3['cdenominacion'],
         'ccantidad' => $monedas3['ccantidad'],
         'ctotal' => $monedas3['ctotal'],
         'cidsubtipopago' => $monedas3['cidsubtipopago'],
         'cnoreferencia' => $monedas3['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $monedas4['cidtipopago'],
         'cdenominacion' => $monedas4['cdenominacion'],
         'ccantidad' => $monedas4['ccantidad'],
         'ctotal' => $monedas4['ctotal'],
         'cidsubtipopago' => $monedas4['cidsubtipopago'],
         'cnoreferencia' => $monedas4['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $monedas5['cidtipopago'],
         'cdenominacion' => $monedas5['cdenominacion'],
         'ccantidad' => $monedas5['ccantidad'],
         'ctotal' => $monedas5['ctotal'],
         'cidsubtipopago' => $monedas5['cidsubtipopago'],
         'cnoreferencia' => $monedas5['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $monedas6['cidtipopago'],
         'cdenominacion' => $monedas6['cdenominacion'],
         'ccantidad' => $monedas6['ccantidad'],
         'ctotal' => $monedas6['ctotal'],
         'cidsubtipopago' => $monedas6['cidsubtipopago'],
         'cnoreferencia' => $monedas6['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $monedas7['cidtipopago'],
         'cdenominacion' => $monedas7['cdenominacion'],
         'ccantidad' => $monedas7['ccantidad'],
         'ctotal' => $monedas7['ctotal'],
         'cidsubtipopago' => $monedas7['cidsubtipopago'],
         'cnoreferencia' => $monedas7['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $cheque['cidtipopago'],
         'cdenominacion' => $cheque['cdenominacion'],
         'ccantidad' => $cheque['ccantidad'],
         'ctotal' => $cheque['ctotal'],
         'cidsubtipopago' => $cheque['cidsubtipopago'],
         'cnoreferencia' => $cheque['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares1['cidtipopago'],
         'cdenominacion' => $dolares1['cdenominacion'],
         'ccantidad' => $dolares1['ccantidad'],
         'ctotal' => $dolares1['ctotal'],
         'cidsubtipopago' => $dolares1['cidsubtipopago'],
         'cnoreferencia' => $dolares1['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares2['cidtipopago'],
         'cdenominacion' => $dolares2['cdenominacion'],
         'ccantidad' => $dolares2['ccantidad'],
         'ctotal' => $dolares2['ctotal'],
         'cidsubtipopago' => $dolares2['cidsubtipopago'],
         'cnoreferencia' => $dolares2['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares3['cidtipopago'],
         'cdenominacion' => $dolares3['cdenominacion'],
         'ccantidad' => $dolares3['ccantidad'],
         'ctotal' => $dolares3['ctotal'],
         'cidsubtipopago' => $dolares3['cidsubtipopago'],
         'cnoreferencia' => $dolares3['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares4['cidtipopago'],
         'cdenominacion' => $dolares4['cdenominacion'],
         'ccantidad' => $dolares4['ccantidad'],
         'ctotal' => $dolares4['ctotal'],
         'cidsubtipopago' => $dolares4['cidsubtipopago'],
         'cnoreferencia' => $dolares4['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares5['cidtipopago'],
         'cdenominacion' => $dolares5['cdenominacion'],
         'ccantidad' => $dolares5['ccantidad'],
         'ctotal' => $dolares5['ctotal'],
         'cidsubtipopago' => $dolares5['cidsubtipopago'],
         'cnoreferencia' => $dolares5['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares6['cidtipopago'],
         'cdenominacion' => $dolares6['cdenominacion'],
         'ccantidad' => $dolares6['ccantidad'],
         'ctotal' => $dolares6['ctotal'],
         'cidsubtipopago' => $dolares6['cidsubtipopago'],
         'cnoreferencia' => $dolares6['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares7['cidtipopago'],
         'cdenominacion' => $dolares7['cdenominacion'],
         'ccantidad' => $dolares7['ccantidad'],
         'ctotal' => $dolares7['ctotal'],
         'cidsubtipopago' => $dolares7['cidsubtipopago'],
         'cnoreferencia' => $dolares7['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares8['cidtipopago'],
         'cdenominacion' => $dolares8['cdenominacion'],
         'ccantidad' => $dolares8['ccantidad'],
         'ctotal' => $dolares8['ctotal'],
         'cidsubtipopago' => $dolares8['cidsubtipopago'],
         'cnoreferencia' => $dolares8['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares9['cidtipopago'],
         'cdenominacion' => $dolares9['cdenominacion'],
         'ccantidad' => $dolares9['ccantidad'],
         'ctotal' => $dolares9['ctotal'],
         'cidsubtipopago' => $dolares9['cidsubtipopago'],
         'cnoreferencia' => $dolares9['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares10['cidtipopago'],
         'cdenominacion' => $dolares10['cdenominacion'],
         'ccantidad' => $dolares10['ccantidad'],
         'ctotal' => $dolares10['ctotal'],
         'cidsubtipopago' => $dolares10['cidsubtipopago'],
         'cnoreferencia' => $dolares10['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $dolares11['cidtipopago'],
         'cdenominacion' => $dolares11['cdenominacion'],
         'ccantidad' => $dolares11['ccantidad'],
         'ctotal' => $dolares11['ctotal'],
         'cidsubtipopago' => $dolares11['cidsubtipopago'],
         'cnoreferencia' => $dolares11['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $aplicacion['cidtipopago'],
         'cdenominacion' => $aplicacion['cdenominacion'],
         'ccantidad' => $aplicacion['ccantidad'],
         'ctotal' => $aplicacion['ctotal'],
         'cidsubtipopago' => $aplicacion['cidsubtipopago'],
         'cnoreferencia' => $aplicacion['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

    Detallecorte::create([
         'cidtipopago' => $uber['cidtipopago'],
         'cdenominacion' => $uber['cdenominacion'],
         'ccantidad' => $uber['ccantidad'],
         'ctotal' => $uber['ctotal'],
         'cidsubtipopago' => $uber['cidsubtipopago'],
         'cnoreferencia' => $uber['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $amexterminal1['cidtipopago'],
         'cdenominacion' => $amexterminal1['cdenominacion'],
         'ccantidad' => $amexterminal1['ccantidad'],
         'ctotal' => $amexterminal1['ctotal'],
         'cidsubtipopago' => $amexterminal1['cidsubtipopago'],
         'cnoreferencia' => $amexterminal1['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);

     Detallecorte::create([
         'cidtipopago' => $amexterminal2['cidtipopago'],
         'cdenominacion' => $amexterminal2['cdenominacion'],
         'ccantidad' => $amexterminal2['ccantidad'],
         'ctotal' => $amexterminal2['ctotal'],
         'cidsubtipopago' => $amexterminal2['cidsubtipopago'],
         'cnoreferencia' => $amexterminal2['cnoreferencia'],
         'cidcorte' => $idcorte,
     ]);
  }
}
