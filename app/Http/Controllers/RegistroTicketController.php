<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\HistorialMesas;
use App\Configuracion;
use App\historialProductos;
use App\RegTicket;

class RegistroTicketController extends Controller
{
	public function VistaTicket(){
		return view("Tickets.index");
	}

	public function buscarTicket(Request $request){
		$Ticket=$request['Ticket'];
		$Fecha = Configuracion::select('cConFechaVal')->first();

		$this->guardarTicket($Ticket,$Fecha['cConFechaVal']);
		$Escaneos=RegTicket::where('cRegTicketMesa',$Ticket)->count();
		$Mesa=$this->consultarMesa($Ticket,$Fecha['cConFechaVal']);
		if ($Mesa) {
				$Productos=$this->consultarProductos($Ticket,$Fecha['cConFechaVal']);
				return ['Estatus' => 1, 'Mesa' => $Mesa, 'Productos' => $Productos, 'Escaneos' => $Escaneos];
		}else {
			return ['Estatus' => 2, 'Escaneos' => $Escaneos];
		}

	}

	public function guardarTicket($ticket,$FechaSistema){
		$Fecha = Carbon::now();
		$formatoFecha=$Fecha->format('Y-m-d h:m:s');
		$usuario= auth()->user()->cUsuFolio;
		$GuardarTicket = RegTicket::create(['cRegTicketFechaRep'=>$FechaSistema,'cRegTicketTiempo'=>$formatoFecha,'cRegTicketUsuario'=>$usuario,'cRegTicketMesa'=>$ticket]);
	}

	public function consultarMesa($Ticket,$FechaSistema){
		$Mesa=HistorialMesas::select('cHisMesTicket', 'cHisMesCantClientes','cHisMesTotal')
		->where('cHisMesFechaRep',$FechaSistema)
		->where('cHisMesTicket',$Ticket)->first();

		return $Mesa;
	}

	public function consultarProductos($Ticket,$FechaSistema){
		$Productos=historialProductos::select('cHisProTicket', 'cHisProCodigo', 'cHisProDesGeneral', 'cHisproPrecioUni', 'cHisProCantidad', 'cHisProPrecio')
		->where('cHisProFechaRep',$FechaSistema)
		->where('cHisProTicket',$Ticket)->get();

		return $Productos;
	}
}
