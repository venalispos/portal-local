<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\BotProductos;
use App\Catalogos\Productos;
use App\Catalogos\BotSubCategorias;
use App\Http\Controllers\Controller;

class BotProductosController extends Controller
{
  public function VistaBotProductos()
  	{
  		    return view('Catalogos\botonProductos\BotonProductosPrincipal');
  	}
  public function obtenerBotonProductos()
    {
      $BotProductos= BotProductos::with('Producto','SubCategoria')->get();
      $Productos= Productos::select('cProCodigo', 'cProDescripcion')->get();
      $BotSubCategorias= BotSubCategorias::get();

      $BotProductos = json_decode($BotProductos,true);
      $Productos = json_decode($Productos,true);
      $BotSubCategorias = json_decode($BotSubCategorias,true);
      return ['BotProductos' => $BotProductos, 'Productos' => $Productos, 'BotSubCategorias' => $BotSubCategorias];
    }
    public function agregarBotonProductos(Request $request){
      $datosBotProductos = $request->all();
      $BotProductos = BotProductos::create($datosBotProductos);
      $IdBotoProducto = $BotProductos->cBotProFolio;

      BotProductos::where('cBotProFolio', $IdBotoProducto)->update(['cBotProId' => $IdBotoProducto]);
    }

    public function editarBotonProductos(Request $request){
      $datosBotProductos = $request->all();
      $Folio = $datosBotProductos['cBotProFolio'];
      $datosBotProductos = \array_splice($datosBotProductos,1,1);
      $editarCategoria= BotProductos::where('cBotProFolio', $Folio)
          ->update($datosBotProductos);
    }

    public function eliminarBotonProductos (Request $request){
      $datosBotProductos = $request->all();
      $deletedRows = BotProductos::where('cBotProFolio', $datosBotProductos['cBotProFolio'])->delete();
    }
}
