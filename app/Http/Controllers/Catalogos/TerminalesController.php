<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\Terminales;
use App\Http\Controllers\Controller;

class TerminalesController extends Controller
{
  public function VistaTerminales()
  	{
  		    return view('Catalogos/configuracionTerminales/ConfiguracionTerminalesPrincipal');
  	}
  public function obtenerTerminales()
    {
      $Terminales= Terminales::get();
      $Terminales = json_decode($Terminales,true);
      return ['Terminales' => $Terminales];
    }
    public function agregarConfigTerminal(Request $request){
      $datosTerminal = $request->all();
      Terminales::create($datosTerminal);
    }

    public function editarConfigTerminal(Request $request){
      $datosTerminal = $request->all();
      $Folio = $datosTerminal['cTerFolio'];
      $datosTerminal = \array_splice($datosTerminal,1,7);
      $editarCategoria= Terminales::where('cTerFolio', $Folio)
          ->update($datosTerminal);
    }

    public function eliminarConfigTerminal(Request $request){
      $datosTerminal = $request->all();
      $deletedRows = Terminales::where('cTerFolio', $datosTerminal['cTerFolio'])->delete();
    }
}
