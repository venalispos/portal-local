<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\BotSubCategorias;
use App\Catalogos\BotCategorias;
use App\Http\Controllers\Controller;

class BotSubCategoriasController extends Controller
{
  public function VistaBotSubCategorias()
  	{
  		    return view('Catalogos/botonSubcategorias/BotonSubcategoriasPrincipal');
  	}
  public function obtener()
    {
      $BotSubCategorias= BotSubCategorias::with('BotCategoria')->get();
      $BotCategorias= BotCategorias::get();
      
      $BotCategorias = json_decode($BotCategorias,true);
      $BotSubCategorias = json_decode($BotSubCategorias,true);
      return ['BotSubCategorias' => $BotSubCategorias, 'BotCategorias' => $BotCategorias];
    }
    public function agregarBotonSubCategorias(Request $request){
      $datosBotSubCategorias = $request->all();
      BotSubCategorias::create($datosBotSubCategorias);
    }

    public function editarBotonSubCategorias(Request $request){
      $datosBotSubCategorias = $request->all();
      $Folio = $datosBotSubCategorias['cBotSubCatFolio'];
      $datosBotSubCategorias = \array_splice($datosBotSubCategorias,1,2);
      $editarCategoria= BotSubCategorias::where('cBotSubCatFolio', $Folio)
          ->update($datosBotSubCategorias);
    }

    public function eliminarBotonSubCategorias(Request $request){
      $datosBotSubCategorias = $request->all();
      $deletedRows = BotSubCategorias::where('cBotSubCatFolio', $datosBotSubCategorias['cBotSubCatFolio'])->delete();
    }
}
