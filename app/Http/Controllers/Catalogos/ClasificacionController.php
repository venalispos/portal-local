<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\Clasificacion;
use App\Http\Controllers\Controller;

class ClasificacionController extends Controller
{
  public function VistaClasificacion()
  	{
  		    return view('Catalogos/clasificacion/ClasificacionPrincipal');
  	}
    public function obtenerClasificacion()
      {
        $Clasificacion= Clasificacion::get();
        $Clasificacion = json_decode($Clasificacion,true);
        return ['Clasificacion' => $Clasificacion];
      }
}
