<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\ComSubCategorias;
use App\Http\Controllers\Controller;

class ComSubCategoriasController extends Controller
{
  public function VistaComSubCategorias(){
	    return view('Catalogos/subcategoriasCombos/SubCategoriasCombosPrincipal');
  	}

  public function obtenerComSubCategorias(){
      $ComSubCategorias= ComSubCategorias::with('Categoria')->get();
      $ComSubCategorias = json_decode($ComSubCategorias,true);
      return ['ComSubCategorias' => $ComSubCategorias];
  }

  public function agregarComSubcategorias(Request $request){
    $datosComSubCategoria = $request->all();
    ComSubCategorias::create($datosComSubCategoria);
  }

  public function editarComSubcategorias(Request $request){
    $datosComSubCategoria = $request->all();
    $Folio = $datosComSubCategoria['cComSubCatFolio'];
    $datosComSubCategoria = \array_splice($datosComSubCategoria,1,2);
    $editarCategoria= ComSubCategorias::where('cComSubCatFolio', $Folio)
        ->update($datosComSubCategoria);
  }

  public function eliminarComSubcategorias(Request $request){
    $datosComSubCategoria = $request->all();
    $deletedRows = ComSubCategorias::where('cComSubCatFolio', $datosComSubCategoria['cComSubCatFolio'])->delete();
  }
}
