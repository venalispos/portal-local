<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\Perfiles;
use App\Catalogos\Accion;
use App\Http\Controllers\Controller;

class PerfilesController extends Controller
{
  public function VistaPerfiles()
  	{
  		    return view('Catalogos/perfiles/PerfilesPrincipal');
  	}
    public function obtenerPerfiles(){
        $Perfiles= Perfiles::get();
        $Acciones= Accion::get();


        foreach ($Perfiles as $index => $Perfil) {
          $Permisos=[];
          $PerfilA =  [];
          $Permisos =explode(",", $Perfil->cPerAccion);
          foreach ($Acciones as $index => $Accion) {
            $PerfilP =  new \stdClass();
            $PerfilP->id = $Accion->cAccFolio;
            $PerfilP->Codigo = $Accion->cAccId;
            $PerfilP->descripcion = $Accion->cAccNotas;
            $PerfilP->activo = 0;
            foreach ($Permisos as $index => $Permiso) {
              if($Accion->cAccId==$Permiso){
                $PerfilP->activo = 1;
                break;
              }
            }
            array_push($PerfilA,$PerfilP);
          }
          $Perfil->Permisos = $PerfilA;
        }

        $Acciones = json_decode($Acciones,true);
        $Perfiles = json_decode($Perfiles,true);
        return ['Perfiles' => $Perfiles, 'Acciones' => $Acciones];
    }

    public function agregarPerfiles(Request $request){

      $datosPerfiles = $request['data'];
      $datosInventario = $request['inventario'];
      $datosAcciones = $request['acciones'];
      $datosPortal = $request['portal'];

      $accionesQuitadas =Accion::select('cAccId')->whereNotIn('cAccId',$datosAcciones)->get()->toArray();
      $acciones=array();
      foreach ($accionesQuitadas  as $key) {
        $acciones[] =$key['cAccId'];
      }
       $datosPerfiles['cPerAccion'] = implode(',',$acciones);

       $datosPerfiles['cPerProductos'] = " ";
       $datosPerfiles['cPerCategorias'] = " ";
       $datosPerfiles['cPerSubCategorias'] = " ";
       $datosPerfiles['cPerAccesPortal'] = implode(',',$datosPortal);
       $datosPerfiles['cPerInventario'] = implode(',',$datosInventario);
       $crearPerfil = Perfiles::create($datosPerfiles);
       $id = $crearPerfil->cPerFolio;

       Perfiles::where('cPerFolio',$id)->update(['cPerId'=>$id]);
    }

    public function editarPerfiles(Request $request){
      $datosPerfiles = $request['data'];
      $datosInventario = $request['inventario'];
      $datosAcciones = $request['acciones'];
      $datosPortal = $request['portal'];
      $Folio = $datosPerfiles['cPerFolio'];

      $accionesQuitadas =Accion::select('cAccId')->whereNotIn('cAccId',$datosAcciones)->get()->toArray();
      $acciones=array();
      foreach ($accionesQuitadas  as $key) {
        $acciones[] =$key['cAccId'];
      }

      Perfiles::where('cPerFolio',$Folio)->update([
      'cPerAccesoClave'=>$datosPerfiles['cPerAccesoClave'],
      'cPerAccion'=>','.implode(',',$acciones),
      'cPerInventario'=>implode(',',$datosInventario),
      'cPerAccesPortal'=>implode(',',$datosPortal),
      'cPerReImpTicket'=>$datosPerfiles['cPerReImpTicket'],
      'cPerLiquida'=>$datosPerfiles['cPerLiquida'],
      'cPerAutorizaSalida'=>$datosPerfiles['cPerAutorizaSalida'],
      'cPerRecibeCuentas'=>$datosPerfiles['cPerRecibeCuentas'],
      'cPerServicio'=>$datosPerfiles['cPerServicio'],
      'cPerAutExtra'=>$datosPerfiles['cPerAutExtra'],
      'cPerAutDividir'=>$datosPerfiles['cPerAutDividir'],
      'cPerAutTransfer'=>$datosPerfiles['cPerAutTransfer'],
      'cPerAutRetiros'=>$datosPerfiles['cPerAutRetiros']]);

    }

    public function eliminarPerfil(Request $request){
      $datosPerfil = $request->all();
      $deletedRows = Perfiles::where('cPerFolio', $datosPerfil['cPerFolio'])->delete();
    }

    public function FunObtenerDatosPerfil(Request $request){
      print_r($request);
    }
}
