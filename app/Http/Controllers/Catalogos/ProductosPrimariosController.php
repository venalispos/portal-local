<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\ProductosPrimarios;
use App\Catalogos\Productos;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\UsersExport;

class ProductosPrimariosController extends Controller
{
  public function VistaProductosPrimarios()
  	{
  		    return view('Catalogos/ProductosPrimarios/ProductosPrimariosPrincipal');
  	}

    public function AgregarProductosPrimarios(Request $request) {
      $datosProducto = $request->all();
      $result = $this->ValidarProdPrimario($datosProducto['data']['cProPriDescripcion'],0);

      if($result === 0) {
         $agregarProdPrimario = ProductosPrimarios::create($datosProducto['data']);
      }
      return $result;
    }

    public function ObtenerDatos() {
      $ProductosPrimarios = ProductosPrimarios::get();
      $ProductosPrimarios = json_decode($ProductosPrimarios, true);
      return $ProductosPrimarios;
    }

    public function EditarProductosPrimarios(Request $request) {
      $datosProducto = $request->all();
      $result = $this->ValidarProdPrimario($datosProducto['data']['cProPriDescripcion'], $datosProducto['id']);

      if($result === 0) {
         $editarProducto = ProductosPrimarios::where('cProPriFolio', $datosProducto['id'])->update($datosProducto['data']);
      }
      return $result;
    }

    public function EliminarProductosPrimarios(Request $request) {
      $datosProducto= $request->all();
      $eliminarProducto = ProductosPrimarios::where('cProPriFolio', $datosProducto['id'])->delete();
    }

    public function ValidarProdPrimario($prodPrimario, $id) {
      $encontrado = 1;
      $validarProdPrimario = ProductosPrimarios::where('cProPriDescripcion', $prodPrimario)->first();

      if($validarProdPrimario==null || $id === $validarProdPrimario['cProPriFolio']) {
        $encontrado = 0;
      }
      return $encontrado;
    }

    public function listaProdPrimarios(Request $request) {

      $ProductosPrimarios = ProductosPrimarios::orderBy('cProPriDescripcion');

      $ProductosPrimarios = $ProductosPrimarios->paginate($request->registros,  ['*'], 'page', $request->page);
      return  [
        'paginate' => [
          'total'        => $ProductosPrimarios->total(),
          'current_page' => $ProductosPrimarios->currentPage(),
          'per_page'     => $ProductosPrimarios->perPage(),
          'last_page'    => $ProductosPrimarios->lastPage(),
          'from'         => $ProductosPrimarios->firstItem(),
          'to'           => $ProductosPrimarios->lastItem(),
        ],
        'listaProdPrimarios' => $ProductosPrimarios
      ];


      }

}
