<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\ComCategorias;
use App\Http\Controllers\Controller;

class ComCategoriasController extends Controller
{
  public function VistaComCategorias()
  	{
  		    return view('Catalogos/categoriasCombos/CategoriasCombosPrincipal');
  	}
    public function obtenerComCategorias(){
        $ComCategorias= ComCategorias::get();
        $ComCategorias = json_decode($ComCategorias,true);
        return ['ComCategorias' => $ComCategorias];
    }

    public function agregarComCategorias(Request $request){
      $datosComCategoria = $request->all();
      ComCategorias::create($datosComCategoria);
    }

    public function editarComCategorias(Request $request){
      $datosComCategoria = $request->all();
      $Folio = $datosComCategoria['cComCatFolio'];
      $datosComCategoria = \array_splice($datosComCategoria,1,1);
      $editarCategoria= ComCategorias::where('cComCatFolio', $Folio)
          ->update($datosComCategoria);
    }

    public function eliminarComCategorias(Request $request){
      $datosComCategoria = $request->all();
      $deletedRows = ComCategorias::where('cComCatFolio', $datosComCategoria['cComCatFolio'])->delete();
    }
}
