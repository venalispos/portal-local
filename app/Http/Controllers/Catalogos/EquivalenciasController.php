<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\Equivalencias;
use App\Catalogos\Productos;
use App\Catalogos\ProductosPrimarios;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\UsersExport;

class EquivalenciasController extends Controller {

    public function VistaEquivalencias() {
      return view('Catalogos/equivalencias/EquivalenciasPrincipal');
  	}

    public function ObtenerProductos() {
      $Productos = Productos::select('cProCodigo', 'cProDescripcion')->get()->toArray();
      return $Productos;
    }

    public function ObtenerProdPrimarios() {
      $ProdPrimarios = ProductosPrimarios::get()->toArray();
      return $ProdPrimarios;
    }

    public function AgregarEquivalencias(Request $request) {
      $datosEquivalencia = $request->all();
      $agregarEquivalencia = Equivalencias::create($datosEquivalencia);
    }

    // Se dejo de utilizar por listaEquivalencias con paginado
    public function ObtenerEquivalencias() {
      $Equivalencias = Equivalencias::with('Producto', 'ProductoPrimario')->get()->toArray();
      return $Equivalencias;
    }

    public function EditarEquivalencias(Request $request) {
      $datosEquivalencia = $request->all();
      print_r($datosEquivalencia);
      $editarEquivalencia = Equivalencias::where('cEquivaFolio', $datosEquivalencia['id'])->update($datosEquivalencia['data']);
    }

    public function EliminarEquivalencias(Request $request) {
      $datosEquivalencia = $request->all();
      $eliminarEquivalencia = Equivalencias::where('cEquivaFolio', $datosEquivalencia['id'])->delete();
    }

    public function ListaEquivalencias(Request $request) {
      $Equivalencias = Equivalencias::with('Producto', 'ProductoPrimario');

      $Equivalencias = $Equivalencias->paginate($request->registros,  ['*'], 'page', $request->page);
      return  [
        'paginate' => [
        'total'        => $Equivalencias->total(),
        'current_page' => $Equivalencias->currentPage(),
        'per_page'     => $Equivalencias->perPage(),
        'last_page'    => $Equivalencias->lastPage(),
        'from'         => $Equivalencias->firstItem(),
        'to'           => $Equivalencias->lastItem(),
        ],
          'ListaEquivalencias' => $Equivalencias
      ];
    }

    public function generarExcel( Request $request){

      $Equivalencias = Equivalencias::with('Producto', 'ProductoPrimario')->get();
      $Equivalencias = json_decode($Equivalencias,true);

  		Excel::create('ventas', function($excel)use (
  	                $Equivalencias
  	            ){

  	            $excel->sheet('ventas', function($sheet)use(
  	                $Equivalencias
  	            ){
  	                $sheet->loadView("Catalogos.equivalencias.excel")
  	                      ->with(compact(
  	                        'Equivalencias'
  	                      ));


  	            });
  	        })->download('csv');

  	}

}
