<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\Categorias;
use App\Configuracion;
use App\Merma;
use App\Http\Controllers\Controller;
use DB;

class MermasController extends Controller
{
  public function index(){
    return view('catalogos/mermas/mermas');
  }

  public function obtener_categorias(){

    $lista_categorias = Categorias::all();

    return response()->json($lista_categorias);

  }

  public function obtener_fecha(){

    $date_config = Configuracion::first();

    $newDateFormat = date('d/m/Y', strtotime($date_config['cConFechaVal']));

    return $newDateFormat;
  }

  public function obtener_mermas(Request $request){

    $data_search = $request->all();

    $date_waste = str_replace("-","",$request->date_waste);

    $configuration = Configuracion::first();

    $configuration_date = $configuration['cConFechaVal'];

    $string_query = "SELECT cFolio, cCatDescripcion, cCantidad, cFechaRep
                    FROM tMerma, tCategorias
                    WHERE cCategoria = cCatCodigo AND cFechaRep = '".$date_waste."' ";

    $waste_list = DB::select($string_query);

    $waste_list_array = json_decode(json_encode($waste_list), true);

    return response()->json($waste_list_array);

  }

  public function agregar_merma(Request $request){

    $data_add = $request->all();

    $configuration = Configuracion::first();

    $configuration_date = $configuration['cConFechaVal'];

    $merma = Merma::where('cFechaRep', $configuration_date)->where('cCategoria', $data_add['cCategoria'])->first();

    $mesagge_response = '';

    if($merma){
      $response = 0;
      return response()->json(['response' => $response]);

    }elseif(!$merma){
      $response = 1;

      Merma::create([
        'cCantidad' => $data_add['cCantidad'],
        'cCategoria' => $data_add['cCategoria'],
        'cFechaRep' => $configuration_date
      ]);

      return response()->json(['response' => $response]);
    }


  }
}
