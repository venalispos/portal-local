<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\BotCategorias;
use App\Http\Controllers\Controller;

class BotCategoriasController extends Controller
{
  public function VistaBotCategorias()
  	{
  		    return view('Catalogos/botonCategorias/BotonCategoriasPrincipal');
  	}
  public function obtenerBotCategorias()
    {
      $BotCategorias= BotCategorias::get();
      $BotCategorias = json_decode($BotCategorias,true);
      return ['BotCategorias' => $BotCategorias];
    }
    public function agregarBotonCategorias(Request $request){
      $datosBotCategorias = $request->all();
      BotCategorias::create($datosBotCategorias);
    }

    public function editarBotonCategorias(Request $request){
      $datosBotCategorias = $request->all();
      $Folio = $datosBotCategorias['cBotCatFolio'];
      $datosBotCategorias = \array_splice($datosBotCategorias,1,1);
      $editarCategoria= BotCategorias::where('cBotCatFolio', $Folio)
          ->update($datosBotCategorias);
    }

    public function eliminarBotonCategorias (Request $request){
      $datosBotCategorias = $request->all();
      $deletedRows = BotCategorias::where('cBotCatFolio', $datosBotCategorias['cBotCatFolio'])->delete();
    }
}
