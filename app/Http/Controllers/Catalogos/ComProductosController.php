<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\ComProductos;
use App\Catalogos\Productos;
use App\Catalogos\ComSubCategorias;
use App\Http\Controllers\Controller;

class ComProductosController extends Controller
{
  public function VistaComProductosCategorias()
  	{
  		    return view('Catalogos/productosCombo/ComboProductoPrincipal');
  	}
  public function obtenerComProductos()
    {
      $ComProductos= ComProductos::with('Producto','SubCategoria')->get();
      $Productos= Productos::select('cProCodigo', 'cProDescripcion')->get();
      $ComSubCategorias= ComSubCategorias::get();

      $ComProductos = json_decode($ComProductos,true);
      $Productos = json_decode($Productos,true);
      $ComSubCategorias = json_decode($ComSubCategorias,true);
      return ['ComProductos' => $ComProductos, 'Productos' => $Productos, 'ComSubCategorias' => $ComSubCategorias];
    }

  public function agregarComProductos(Request $request){
    $datosComProducto = $request->all();
    ComProductos::create($datosComProducto);
  }

  public function editarComProductos(Request $request){
    $datosComProducto = $request->all();
    $Folio = $datosComProducto['cComProdFolio'];
    $datosComProducto = \array_splice($datosComProducto,1,2);
    $editarProducto= ComProductos::where('cComProdFolio', $Folio)
        ->update($datosComProducto);
  }

  public function eliminarComProductos(Request $request){
    $datosComProducto = $request->all();
    $deletedRows = ComProductos::where('cComProdFolio', $datosComProducto['cComProdFolio'])->delete();
  }
}
