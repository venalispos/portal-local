<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\ConfiguracionNube;
use App\Http\Controllers\Controller;

class ConfiguracionNubeController extends Controller
{
  public function VistaConfigNube(){
    return view('Catalogos/configuracionNube/ConfiguracionNubePrincipal');
  	}
  public function obtenerConfigNube(){
    $ConfigNube= ConfiguracionNube::first();
    $ConfigNube = json_decode($ConfigNube,true);
    return ['ConfigNube' => $ConfigNube];
  }
  public function agregarConfigNube(Request $request){
    $datosConNube = $request->all();
    ConfiguracionNube::create($datosConNube);
  }
  public function editarConfigNube(Request $request){
    $datosConNube = $request->all();
    $Folio = $datosConNube['cFolio'];
    $datosConNube = \array_splice($datosConNube,1,25);
    $editarCategoria= ConfiguracionNube::where('cFolio', $Folio)
       ->update($datosConNube);
  }
}
