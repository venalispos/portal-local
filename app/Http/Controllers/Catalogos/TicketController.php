<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\Ticket;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{
  public function VistaTicket()
  	{
  		    return view('Catalogos/ticket/TicketPrincipal');
  	}
    public function obtenerTicket()
      {
        $Ticket= Ticket::first();
        $Ticket = json_decode($Ticket,true);
        return ['Ticket' => $Ticket];
      }
}
