<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\Categorias;
use App\Catalogos\SubCategorias;
use App\Http\Controllers\Controller;

class CategoriasController extends Controller
{
  public function VistaCategorias()
  	{
  		    return view('Catalogos/categoria/CategoriasPrincipal');
  	}
  public function obtenerCategorias()
    {
      $Categorias= Categorias::get();
      $Categorias = json_decode($Categorias,true);
      return ['Categorias' => $Categorias];
    }
  public function editarCategorias(Request $request)
    {
      $datosCategoria = $request->all();
      $Folio = $datosCategoria['cCatFolio'];
      $Codigo = $datosCategoria['cCatCodigo'];
      $datosCategoria = \array_splice($datosCategoria,1,2);
      $consulta = $this->comprobarCodigo($Codigo);
      $consultaSubcategorias = $this->comprobarSubcategoria($consulta['cCatCodigo']);
      if ($consulta === null || $consulta['cCatFolio']===$Folio) {
        $consultaFolio = $this->comprobarFolio($Folio);
        $consultaSubcategorias = $this->comprobarSubcategoria($consultaFolio['cCatCodigo']);

        if($consultaSubcategorias === null) {
          $editarCategoria= Categorias::where('cCatFolio', $Folio)
              ->update($datosCategoria);
          return 0;
        }
        else{
          return 2;
        }
      }
      else{
          return 1;
        }
    }

  public function eliminarCategorias(Request $request)
    {
      $datosCategoria = $request->all();
      $consultaFolio = $this->comprobarFolio($datosCategoria['cCatFolio']);
      $datosSubCategoria = $this->comprobarSubcategoria($consultaFolio['cCatCodigo']);
      if($datosSubCategoria===null){
        $deletedRows = Categorias::where('cCatFolio', $datosCategoria['cCatFolio'])->delete();
      }
      else{
        return 2;
      }
    }
  public function agregarCategorias(Request $request)
    {
      $datosCategoria = $request->all();
      if ($this->comprobarCodigo($datosCategoria['cCatCodigo']) ===null) {
        Categorias::create($datosCategoria);
        return 0;
      }
      else{
        return 1;
      }
    }

  public function comprobarCodigo($Codigo)
      {
        $Categoria = Categorias::where('cCatCodigo', $Codigo)->first();
        return $Categoria;
      }

  public function comprobarSubcategoria($Codigo)
      {
        $Subcategoria = SubCategorias::where('cSCatPadre', $Codigo)->first();
        return $Subcategoria;
      }

  public function comprobarFolio($Folio)
      {
        $Categoria = Categorias::where('cCatFolio', $Folio)->first();
        return $Categoria;
      }
}
