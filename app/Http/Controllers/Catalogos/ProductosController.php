<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\Productos;
use App\Catalogos\Ticket;
use App\Catalogos\Categorias;
use App\Catalogos\Clasificacion;
use App\Catalogos\ComCategorias;
use App\Catalogos\ComProductos;
use App\Catalogos\ComSubProductos;
use App\Catalogos\ComSubCategorias;
use App\Catalogos\SubCategorias;
use App\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\UsersExport;

class ProductosController extends Controller
{
  public function VistaProductos()
  	{
  		    return view('Catalogos/productos/ProductosPrincipal');
  	}
  public function VistaProductosReporte()
  	{
  		    return view('Catalogos/productos/ProductosReporte');
  	}

  public function obtenerProductos()
    {
      $Productos= Productos::with('Categoria', 'SubCategoria')->get();
      $Productos = json_decode($Productos,true);
      return ['Productos' => $Productos];
    }

  public function agregarProducto(Request $request)
      {
        $datosProducto = $request->all();
        $Producto = Productos::where('cProCodigo', $datosProducto['cProCodigo'])->first();
        if ($this->comprarCodigoProducto($datosProducto['cProCodigo']) ===null) {
          $query = Productos::create($datosProducto);
          return 0;
        }
        else{
          return 1;
        }
      }

  public function comprarCodigoProducto($Codigo){
    $Producto = Productos::where('cProCodigo', $Codigo)->first();
    return $Producto;
  }

  public function eliminarPuntos($Datos){
    $Datos = str_replace(':', '', $Datos);
    return $Datos;
  }

  public function editarProducto(Request $request)
    {
      $datosProducto= $request->all();
      $Folio = $datosProducto['cProFolio'];
      $datosProducto = \array_splice($datosProducto,1,62);
      $consulta = $this->comprarCodigoProducto($datosProducto['cProCodigo']);
      if ($consulta === null || $consulta['cProFolio']===$Folio) {
          $editarProducto= Productos::where('cProFolio', $Folio)
              ->update($datosProducto);
          return 0;
      }
      else{
        return 1;
      }
    }

  public function eliminarProductos(Request $request)
    {
      $datosProducto= $request->all();
      $deletedRows = Productos::where('cCatCodigo', $datosCategoria['cCatCodigo'])->delete();
    }

  public function obtenerDatos(){
    $Ticket= Ticket::select('cTicCantCantidad','cTicCantPrecio','cTicCantCaracteres','cTicLetra')->first();
    $Ticket = json_decode($Ticket,true);

    $Configuracion= Configuracion::select('cConImpuesto1Activo','cConImpuesto1Nombre','cConImpuesto1Cantidad','cConImpuesto1Tipo','cConImpuesto2Activo','cConImpuesto2Nombre','cConImpuesto2Cantidad','cConImpuesto2Tipo','cConImpuesto3Activo','cConImpuesto3Nombre','cConImpuesto3Cantidad','cConImpuesto3Tipo','cConImpuesto4Activo','cConImpuesto4Nombre','cConImpuesto4Cantidad','cConImpuesto4Tipo')->first();
    $Configuracion = json_decode($Configuracion,true);

    $Categorias= Categorias::get();
    $Categorias = json_decode($Categorias,true);

    $SubCategorias= SubCategorias::get();
    $SubCategorias = json_decode($SubCategorias,true);

    $Clasificacion= Clasificacion::get();
    $Clasificacion = json_decode($Clasificacion,true);

    $ComCategorias= ComCategorias::get();
    $ComCategorias = json_decode($ComCategorias,true);

    $ComProductos= ComProductos::get();
    $ComProductos = json_decode($ComProductos,true);

    $ComSubCategorias= ComSubCategorias::get();
    $ComSubCategorias = json_decode($ComSubCategorias,true);
    return ['Ticket' => $Ticket, 'Configuracion' => $Configuracion, 'Categorias' => $Categorias, 'SubCategorias' => $SubCategorias, 'Clasificacion' => $Clasificacion, 'ComCategorias' => $ComCategorias, 'ComSubCategorias' => $ComSubCategorias, 'ComProductos' => $ComProductos];
  }

  public function generarExcel( Request $request){

    $Productos= Productos::with('Categoria', 'SubCategoria')->get();
    $Productos = json_decode($Productos,true);

		Excel::create('ventas', function($excel)use (
	                $Productos
	            ){

	            $excel->sheet('ventas', function($sheet)use(
	                $Productos
	            ){
	                $sheet->loadView("Catalogos.productos.excel")
	                      ->with(compact(
	                        'Productos'
	                      ));


	            });
	        })->download('csv');

	}
}
