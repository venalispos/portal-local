<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Catalogos\Justificaciones;
use App\Catalogos\DescuentosAutorizados;
use App\Http\Controllers\Controller;

class JustificacionesController extends Controller {

  public function VistaJustificaciones() {
    return view('Catalogos/justificaciones/JustificacionesPrincipal');
  }

  public function ObtenerJustificaciones() {
    $Justificaciones = Justificaciones::with('Descuentos')->get()->toArray();
    return $Justificaciones;
  }

  public function AgregarJustificacion(Request $request) {
    $Justificacion = $request['justificacion'];
    Justificaciones::create($Justificacion);
    return $Justificacion;
  }

  public function EditarJustificacion(Request $request) {
    $id = $Justificacion['cJusFolio'];
    unset($Justificacion['cJusFolio']);
    unset($Justificacion['descuentos']);

    Justificaciones::where('cJusFolio', $id)->update($Justificacion);
    return $Justificacion;
  }

  public function EliminarJustificacion(Request $request) {
    $Justificacion = $request['justificacion'];
    $id = $Justificacion['cJusFolio'];
    unset($Justificacion['cJusFolio']);
    unset($Justificacion['descuentos']);
    Justificaciones::where('cJusFolio', $id)->delete($Justificacion);
    return $Justificacion;
  }

  public function AgregarDescuento(Request $request) {
    $Descuento = $request['descuento'];
    DescuentosAutorizados::create($Descuento);
    return $Descuento;
  }

  public function EditarDescuento(Request $request) {
    $Descuento = $request['descuento'];
    $id = $Descuento['cDescFolio'];
    unset($Descuento['cDescFolio']);
    DescuentosAutorizados::where('cDescFolio', $id)->update($Descuento);
    return $Descuento;
  }

  public function EliminarDescuento(Request $request) {
    $Descuento = $request['descuento'];
    $id = $Descuento['cDescFolio'];
    unset($Descuento['cDescFolio']);
    DescuentosAutorizados::where('cDescFolio', $id)->delete($Descuento);
    return $Descuento;
  }

}
