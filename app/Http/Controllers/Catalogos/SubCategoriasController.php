<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Catalogos\SubCategorias;
use App\Http\Controllers\Controller;
use App\Catalogos\Productos;

class SubCategoriasController extends Controller
{
  public function VistaSubCategorias()
  	{
  		    return view('Catalogos/subcategorias/SubcategoriasPrincipal');
  	}
    public function obtenerSubCategorias()
    {
        $SubCategorias= SubCategorias::with('Categoria')->get();
        $SubCategorias = json_decode($SubCategorias,true);
        return ['SubCategorias' => $SubCategorias];
    }
    public function agregarSubcategorias(Request $request){
      $datosSubCategoria = $request->all();
      if ($this->comprobarCodigo($datosSubCategoria['cSCatCodigo']) ===null) {
        SubCategorias::create($datosSubCategoria);
      }
      else{
        return 1;
      }
    }

    public function editarSubCategorias(Request $request){
      $datosSubCategoria = $request->all();
      $Codigo = $datosSubCategoria['cSCatCodigo'];
      $Folio = $datosSubCategoria['cSCatFolio'];
      $datosSubCategoria = \array_splice($datosSubCategoria,1,3);
      $consulta = $this->comprobarCodigo($Codigo);
      $consultaProductos = $this->comprobarProductos($consulta['cSCatCodigo']);

      if ($consulta === null || $consulta['cSCatFolio']===$Folio) {
        $consultaFolio = $this->comprobarFolio($Folio);
        $consultaProductos = $this->comprobarProductos($consultaFolio['cSCatCodigo']);

        if($consultaProductos === null) {
          $editarSubCategoria= SubCategorias::where('cSCatFolio', $Folio)
              ->update($datosSubCategoria);
          return 0;
        }
        else{
          return 2;
        }
      }
      else{
        return 1;
      }
    }

    public function eliminarSubcategorias(Request $request){
      $datosSubCategoria = $request->all();
      $consultaFolio = $this->comprobarFolio($datosSubCategoria['cSCatFolio']);
      $consultaProductos = $this->comprobarProductos($consultaFolio['cSCatCodigo']);
      if($consultaProductos===null){
        $deletedRows = SubCategorias::where('cSCatFolio', $datosSubCategoria['cSCatFolio'])->delete();
      }
      else{
        return 2;
      }
    }

    public function comprobarCodigo($Codigo)
        {
          $SubCategoria = SubCategorias::where('cSCatCodigo', $Codigo)->first();
          return $SubCategoria;
        }

    public function comprobarFolio($Folio)
        {
          $SubCategoria = SubCategorias::where('cSCatFolio', $Folio)->first();
          return $SubCategoria;
        }

    public function comprobarProductos($Codigo)
        {
          $Producto = Productos::where('cProSubCategoria', $Codigo)->first();
          return $Producto;
        }
}
