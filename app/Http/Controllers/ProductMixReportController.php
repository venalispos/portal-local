<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuracion;
use App\mesasHoy;
use App\FormasPago;
use Dompdf\Dompdf;
use App\Categorias;
use DB;
use PDF;
use App\historialMesas;
use App\historialProductos;
use App\Cortes;
use Carbon\Carbon;

class ProductMixReportController extends Controller
{

  public function index(){

  //  return view('reportes/product_mix/product_mix')->with('Version', $Version);
  return view('reportes/product_mix/product_mix');
  }

  public function obtener_ventasc(Request $request){
    $Categorias = Categorias::get();
    return $Categorias;
  }

  public function obtener_ventas(Request $request){
    $FechaIni = str_replace("-","",$request->date_since);
    $FechaFin = str_replace("-", "", $request->date_to);

    return $this->obtenerCategorias($FechaIni,$FechaFin);
  }

  public function obtenerCategorias($FechaIni,$FechaFin){
    $TotalFinal = 0;
    $PrecioFinal = 0;
    $SubtotalFinal = 0;
    $Impuestos1Final = 0;
    $Impuestos2Final = 0;
    $Impuestos3Final = 0;
    $Impuestos4Final = 0;
    $CantidadFinal = 0;
    $Categorias = Categorias::get();
    $ProductosSInCategoriaTotal = 0;
    $CategoriasExistentes = array();
    $impuesto1 = Configuracion::select('cConImpuesto1Nombre')->where('cConImpuesto1Activo', 1)->first();
    $impuesto2 = Configuracion::select('cConImpuesto2Nombre')->where('cConImpuesto2Activo', 1)->first();
    $impuesto3 = Configuracion::select('cConImpuesto3Nombre')->where('cConImpuesto3Activo', 1)->first();
    $impuesto4 = Configuracion::select('cConImpuesto4Nombre')->where('cConImpuesto4Activo', 1)->first();

    foreach($Categorias as $Categoria){
      $CategoriasExistentes[] = $Categoria->cCatCodigo;
      $Categoria->Productos = $this->obtenerProductos($FechaIni,$FechaFin,$Categoria->cCatCodigo);
      $Categoria->Totales = $this->obtenerTotales($FechaIni,$FechaFin,$Categoria->cCatCodigo);
      $TotalFinal = $TotalFinal + $Categoria->Totales['Total'];
      $PrecioFinal = $PrecioFinal + $Categoria->Totales['Precio'];
      $CantidadFinal = $CantidadFinal + $Categoria->Totales['Cantidad'];
      $SubtotalFinal = $SubtotalFinal + $Categoria->Totales['Subtotal'];
      $Impuestos1Final = $Impuestos1Final + $Categoria->Totales['Impuesto1'];
      $Impuestos2Final = $Impuestos2Final + $Categoria->Totales['Impuesto2'];
      $Impuestos3Final = $Impuestos3Final + $Categoria->Totales['Impuesto3'];
      $Impuestos4Final = $Impuestos4Final + $Categoria->Totales['Impuesto4'];
    }

    $ProductosSInCategoria = $this->obtenerProductosSinCategoria($FechaIni,$FechaFin,$CategoriasExistentes);

    if (count($ProductosSInCategoria) > 0) {
      $ProductosSInCategoriaTotal = $this->obtenerTotalesSinCategoria($FechaIni,$FechaFin,$CategoriasExistentes);
      $TotalFinal = $TotalFinal + $ProductosSInCategoriaTotal['Total'];
      $PrecioFinal = $PrecioFinal + $ProductosSInCategoriaTotal['Precio'];
      $CantidadFinal = $CantidadFinal + $ProductosSInCategoriaTotal['Cantidad'];
      $SubtotalFinal = $SubtotalFinal + $ProductosSInCategoriaTotal['Subtotal'];
      $Impuestos1Final = $Impuestos1Final + $ProductosSInCategoriaTotal['Impuesto1'];
      $Impuestos2Final = $Impuestos2Final + $ProductosSInCategoriaTotal['Impuesto2'];
      $Impuestos3Final = $Impuestos3Final + $ProductosSInCategoriaTotal['Impuesto3'];
      $Impuestos4Final = $Impuestos4Final + $ProductosSInCategoriaTotal['Impuesto4'];
    }

    return ['Total' =>$TotalFinal,'Categorias' =>$Categorias,'SinCategoria' =>$ProductosSInCategoria,'TotalesSinCategorias' =>$ProductosSInCategoriaTotal
            ,'Impuesto1' =>$impuesto1
            ,'Impuesto2' =>$impuesto2
            ,'Impuesto3' =>$impuesto3
            ,'Impuesto4' =>$impuesto4
            ,'CantidadFinal' => $CantidadFinal
            ,'PrecioFinal' => $PrecioFinal
            ,'SubtotalFinal' => $SubtotalFinal
            ,'Impuestos1Final' => $Impuestos1Final
            ,'Impuestos2Final' => $Impuestos2Final
            ,'Impuestos3Final' => $Impuestos3Final
            ,'Impuestos4Final' => $Impuestos4Final];
  }

  public function obtenerProductos($FechaIni,$FechaFin,$Categoria){
    $Productos = historialProductos::select(DB::raw("SUM(cHisProSubTotal) as Subtotal,SUM(cHisProImp1) as Impuesto1,SUM(cHisProImp2) as Impuesto2,SUM(cHisProImp3) as Impuesto3,SUM(cHisProImp4) as Impuesto4,SUM(cHisProCantidad) as Cantidad,SUM(cHisProSubTotal+cHisProImp1+cHisProImp2+cHisProImp3+cHisProImp4) as Total" ), 'cHisProCodigo','cHisProPrecio','cHisProDesGeneral')
    ->where('cHisProCategoria',$Categoria)
    ->where('cHisProCodigo', '<>',1005)
    ->where('cHisProCancel', 0)
    ->whereBetween('cHisProFechaRep', [$FechaIni, $FechaFin])
    ->groupBy('tHisProductos.cHisProCodigo','cHisProPrecio','cHisProDesGeneral')
    ->orderBy('cHisProCodigo')
    ->get();

    return $Productos;
  }

  public function obtenerProductosSinCategoria($FechaIni,$FechaFin,$Categorias){
    $Productos = historialProductos::select(DB::raw("SUM(cHisProSubTotal) as Subtotal,SUM(cHisProImp1) as Impuesto1,SUM(cHisProImp2) as Impuesto2,SUM(cHisProImp3) as Impuesto3,SUM(cHisProImp4) as Impuesto4,SUM(cHisProCantidad) as Cantidad,SUM(cHisProSubTotal+cHisProImp1+cHisProImp2+cHisProImp3+cHisProImp4) as Total" ), 'cHisProCodigo','cHisProPrecio','cHisProDesGeneral')
    ->whereNotIn('cHisProCategoria',$Categorias)
    ->where('cHisProCodigo', '<>',1005)
    ->where('cHisProCancel', 0)
    ->whereBetween('cHisProFechaRep', [$FechaIni, $FechaFin])
    ->groupBy('tHisProductos.cHisProCodigo','cHisProPrecio','cHisProDesGeneral')
    ->orderBy('cHisProCodigo')
    ->get();

    return $Productos;
  }

  public function obtenerTotalesSinCategoria($FechaIni,$FechaFin,$Categoria){
    $ProductosTotal = historialProductos::whereNotIn('cHisProCategoria',$Categoria)
    ->whereBetween('cHisProFechaRep', [$FechaIni, $FechaFin])
    ->where('cHisProCodigo', '<>',1005)
    ->where('cHisProCancel', 0)
    ->orderBy('cHisProCodigo');

    $CantidadTotal = $ProductosTotal->sum('cHisProCantidad');
    $PrecioTotal = $ProductosTotal->avg('cHisProPrecio');
    $SubtotalTotal = $ProductosTotal->sum('cHisProSubTotal');
    $Impuesto1 = $ProductosTotal->sum('cHisProImp1');
    $Impuesto2 = $ProductosTotal->sum('cHisProImp2');
    $Impuesto3 = $ProductosTotal->sum('cHisProImp3');
    $Impuesto4 = $ProductosTotal->sum('cHisProImp4');
    $TotalTotal = $SubtotalTotal+$Impuesto1+$Impuesto2+$Impuesto3+$Impuesto4;

    return ['Total' =>$TotalTotal,'Impuesto1' =>$Impuesto1,'Impuesto2' =>$Impuesto2,'Impuesto3' =>$Impuesto3,'Impuesto4' =>$Impuesto4,'Subtotal' =>$SubtotalTotal,'Precio' =>$PrecioTotal,'Cantidad' =>$CantidadTotal];
  }

  public function obtenerTotales($FechaIni,$FechaFin,$Categoria){
    $ProductosTotal = historialProductos::where('cHisProCategoria',$Categoria)
    ->whereBetween('cHisProFechaRep', [$FechaIni, $FechaFin])
    ->where('cHisProCodigo', '<>',1005)
    ->where('cHisProCancel', 0)
    ->orderBy('cHisProCodigo');

    $CantidadTotal = $ProductosTotal->sum('cHisProCantidad');
    $PrecioTotal = $ProductosTotal->avg('cHisProPrecio');
    $SubtotalTotal = $ProductosTotal->sum('cHisProSubTotal');
    $Impuesto1 = $ProductosTotal->sum('cHisProImp1');
    $Impuesto2 = $ProductosTotal->sum('cHisProImp2');
    $Impuesto3 = $ProductosTotal->sum('cHisProImp3');
    $Impuesto4 = $ProductosTotal->sum('cHisProImp4');
    $TotalTotal = $SubtotalTotal+$Impuesto1+$Impuesto2+$Impuesto3+$Impuesto4;

    return ['Total' =>$TotalTotal,'Impuesto1' =>$Impuesto1,'Impuesto2' =>$Impuesto2,'Impuesto3' =>$Impuesto3,'Impuesto4' =>$Impuesto4,'Subtotal' =>$SubtotalTotal,'Precio' =>$PrecioTotal,'Cantidad' =>$CantidadTotal];
  }

  public function pdf_product_mix(Request $request){
    $FechaIni = str_replace("-","",$request->date_since);
    $FechaFin = str_replace("-", "", $request->date_to);

    $reportes =  $this->obtenerCategorias($FechaIni,$FechaFin);
   $config_idioma = Configuracion::select('cConLenguaje')->first();
   $formato_fecha = DB::table('tIdiomas')
                       ->select('gFormatoFecha')
                       ->where('cIdiId', 'LIKE'. '%'.$config_idioma['cConLenguaje'].'%')
                       ->first();

     $date_since = date_create($request->date_since);
     $date_to = date_create($request->date_to);

     $date_to_format = date_format($date_to, 'm/d/Y');
     $date_since_format = date_format($date_since, 'm/d/Y');

     $sucursal = Configuracion::select('cConSucursal')->first();

     $info_encabezado = ['fecha' => $date_since_format." - ".$date_to_format, 'sucursal' => $sucursal['cConSucursal']];


   $nombre_archivo = 'Sales report ('.$date_since_format." - ".$date_to_format.')';
   $header = view()->make('/reportes/product_mix/pdf_report/product_mix_pdfheader')->with('info_encabezado', $info_encabezado)->render();
   $pdf = PDF::loadview('/reportes/product_mix/pdf_report/product_mix_pdfbody', compact('reportes'));
   $pdf->setOption('header-html', $header)
   ->setOption('header-right','PAGE [page] OF [toPage]')
   ->setOption('header-font-name','Courier')
   ->setOption('header-font-size','8')
   ->setOption('margin-top',38)
   ->setOption('margin-bottom',10)
   ->setOption('margin-left',10)
   ->setOption('margin-right',10);
   return $pdf->download('Product mix.pdf');
  }

}
