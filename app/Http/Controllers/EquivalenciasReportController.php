<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dompdf\Dompdf;
use DB;
use PDF;
use Carbon\Carbon;
use App\Catalogos\ProductosPrimarios;
use App\Catalogos\Productos;
use App\Catalogos\Equivalencias;
use App\historialProductos;

class EquivalenciasReportController extends Controller {

  public function index() {
    return view('reportes/equivalencias/equivalencias');
  }

  public function ObtenerProdPrimarios() {
    $ProdPrimarios = ProductosPrimarios::orderBy('cProPriDescripcion')->get();
    return $ProdPrimarios;
  }

  public function ObtenerEquivalencias() {
    $Equivalencias = Equivalencias::get();
    return $Equivalencias;
  }

  public function GenerarReporte(Request $request) {
    $fechaDesde = str_replace("-","",$request->fechaDesde);
    $fechaHasta = str_replace("-","",$request->fechaHasta);
    $prodPrimario = $request->prodPrimarioSelected;
    $arrayProductos = array();
    // print_r( $fechaDesde);
    foreach ($prodPrimario as $key => $product) {
      $primario['Producto'] = $this->obtenerProductos($fechaDesde, $fechaHasta, $product['cProPriFolio']);
      $primario['Descripcion'] = $product['cProPriDescripcion'];
      $primario['TotalFinal'] = 0;

      if ($primario['Producto'] != 0) {
        foreach ($primario['Producto'] as $key => $Producto) {
          $primario['TotalFinal'] = $primario['TotalFinal'] + $Producto['Total'];
        }
      }
      array_push($arrayProductos, $primario);
    }

    return $arrayProductos;

  }

  public function obtenerProductos($fechaDesde, $fechaHasta, $prodPrimario) {
    $Productos = historialProductos::select(DB::raw('cCodigoProd, cProDescripcion, cProPriDescripcion, cEquivalencia, cProPriDetEquivalencia, SUM(cHisProCantidad) AS Cantidad, (SUM(cHisProCantidad))*cEquivalencia AS Total'))
    ->join('tEquivalencias', 'tEquivalencias.cCodigoProd', '=', 'tHisProductos.cHisProCodigo')
    ->join('tProductos', 'tProductos.cProCodigo', '=', 'tEquivalencias.cCodigoProd')
    ->join('tProductosPrimarios', 'tProductosPrimarios.cProPriFolio', '=', 'tEquivalencias.cFolioProPri')
    ->where('tEquivalencias.cFolioProPri', $prodPrimario)
    ->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
    ->groupBy('cHisProCodigo','tEquivalencias.cFolioProPri','cProPriDescripcion', 'cEquivalencia', 'cProPriDetEquivalencia', 'cProDescripcion', 'cCodigoProd')
    ->get()->toArray();

    if ( ! isset($Productos[0])) {
       return 0;
    } else {
      return $Productos;
    }

  }

}
