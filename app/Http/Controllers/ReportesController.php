<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dompdf\Dompdf;
use PDF;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\UsersExport;
use App\historialMesas;
use App\HistorialFormasPagos;
use App\historialProductos;
use App\categoriasFormaPago;
use App\AgrupadorFormaPago;
use App\CategoriaAgrupadorFormaPago;
use App\Productos;
use App\Categorias;
use App\SubCategorias;
use App\Bitacora;
use Illuminate\Support\Facades\DB;

class ReportesController extends Controller
{


	public function indexCorte()
	{
		return view('reportes/reporteCorte/index');
	}

	public function verVentasEmpleado(){
		return view('reportes.reporte_ventaempleado.venta_empleado');
	}

	public function generarReporteEmpleado(Request $request){
		$FechaInicio = new Carbon($request->fechaDesde);
		$FechaFin = new Carbon($request->fechaHasta);
		$fechaDesde = date_format($FechaInicio, 'Ymd');
		$fechaHasta = date_format($FechaFin, 'Ymd');

		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($fechaDesde, $fechaHasta);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
			  	$reporte_ventas = DB::select("SELECT cHisProEmpNum,cHisProEmpNom,
						SUM(cHisProSubTotal) AS Subtotal, SUM(cHisProPrecio) AS Total FROM tHisProductos
						WHERE cHisProFechaRep BETWEEN '".$fechaDesde."' AND '".$fechaHasta."' GROUP BY cHisProEmpNum,cHisProEmpNom ORDER BY Total DESC");
			  	return [
			  		'informacion' => [
			  			'fechaDesde' => $request->fechaDesde,
			  			'fechaHasta' => $request->fechaHasta
			  		],
			  		'reporte' => $reporte_ventas
			  	];
				}
	}

	public function generarExcelCorte(Request $request)
	{
		$FechaInicio = new Carbon($request->fechaDesde);
		$FechaFin = new Carbon($request->fechaHasta);

		$fechaDesde = date_format($FechaInicio, 'Ymd');
		$fechaHasta = date_format($FechaFin, 'Ymd');


		$totalesysubtotales = historialProductos::whereHas('Mesa', function ($query) {
			$query->where('cHisMesVisible', '=',  1);
		})->selectRaw('sum(cHisProSubTotal) as Subtotal, sum(cHisProImp1) as Impuesto, sum(cHisProSubTotal)+sum(cHisProImp1) as Total')
			->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
			->get();


		$cantidadclientes = historialMesas::selectRaw('sum(cHisMesCantClientes) as CantidadClientes')
			->whereBetween('cHisMesFechaRep', [$fechaDesde, $fechaHasta])
			->first();

		$cortesiasxdescuentos = historialProductos::selectRaw('cHisProCategoria , sum(cHisProCortesia) as Cortesias, sum(cHisProDescuentos) as Descuentos')
			->groupBy('cHisProCategoria')
			->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
			->with(['categorias:cCatFolio,cCatCodigo,cCatDescripcion'])
			->get();

		$sumacortesias = historialProductos::selectRaw('sum(cHisProCortesia) as Cortesias')
			->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
			->first();

		$sumadescuentos = historialProductos::selectRaw('sum(cHisProDescuentos) as Descuentos')
			->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
			->first();
		$Historial = HistorialFormasPagos::selectRaw('cHisForId, sum(cHisForCant) as Cantidad')
			->with(['categoriaformapago:cCatForFolio,cCatForId,cCatForDesc,cCatForAgrupadorCorte'])
			->groupBy('cHisForId')
			->whereNotIn('cHisForId', [1, 4])
			->whereBetween('cHisForFechaRep', [$fechaDesde, $fechaHasta])
			->get();

		$agrupadores = AgrupadorFormaPago::select('cAgForId', 'cAgForDescripcion', 'cAgCategoria')
			->with(['categoriaagrupadorformapago:cCatAgForId,cCatAgForDescripcion'])
			->get();

		$agr = array();


		foreach ($agrupadores as $agrupador) {
			$agrupador->suma = $this->sumarHistorial($agrupador['cAgForId'], $Historial);
			array_push($agr, $agrupador);
		}

		$catagr = CategoriaAgrupadorFormaPago::select('cCatAgForId', 'cCatAgForDescripcion')->get();


		$caja = array();

		foreach ($catagr as $categoriaagrupador) {
			$categoriaagrupador->suma = $this->sumarHistoriaCatAgrupador($categoriaagrupador['cCatAgForId'], $agr);
			array_push($caja, $categoriaagrupador);
		}


		$totalcaja = 0;

		foreach ($caja as $box) {
			$totalcaja += $box->suma;
		}

		unset($caja[0]);

		$totalformapago = 0;

		foreach ($Historial as $hist) {
			$totalformapago += $hist->Cantidad;
		}

		$nuevoefectivo = historialMesas::selectRaw('sum(cHisMesTotal) as CantidadEfectivo')
			->whereBetween('cHisMesFechaRep', [$fechaDesde, $fechaHasta])
			->where('cHisMesVisible', 1)
			->first();

		$totalefectivo = $nuevoefectivo->CantidadEfectivo - $totalformapago;

		$totalcaja = $totalcaja + $totalefectivo;
		$totalformapago = $totalformapago + $totalefectivo;

		$titulo = "Reporte Corte";
		$info_encabezado = ['titulo' => $titulo, 'fecha' => $request->fechaDesde . " - " . $request->fechaHasta];

		$dataGPDF = [
			'fechainicio' => $request->fechaDesde,
			'fechafinal' => $request->fechaHasta,
			'efectivo' => $totalefectivo,
			'caja' => $caja,
			'formapago' => $Historial,
			'totalcaja' => $totalcaja,
			'totalformapago' => $totalformapago,
			'comensales' => $cantidadclientes,
			'deducciones' => $cortesiasxdescuentos,
			'totalcortesias' => $sumacortesias,
			'totaldescuentos' => $sumadescuentos,
			'totalesysubtotales' => $totalesysubtotales
		];

		Excel::create('ventas', function ($excel) use (
			$info_encabezado,
			$dataGPDF
		) {

			$excel->sheet('ventas', function ($sheet) use (
				$info_encabezado,
				$dataGPDF
			) {

				$sheet->loadView("reportes.reporteCorteExcel.body")
					->with(compact(
						'info_encabezado',
						'dataGPDF'
					));
			});
		})->download('csv');
	}

	public function generarPDFCorte(Request $request)
	{
		$FechaInicio = new Carbon($request->fechaDesde);
		$FechaFin = new Carbon($request->fechaHasta);

		$fechaDesde = date_format($FechaInicio, 'Ymd');
		$fechaHasta = date_format($FechaFin, 'Ymd');

		$totalesysubtotales = historialProductos::whereHas('Mesa', function ($query) {
			$query->where('cHisMesVisible', '=',  1);
		})->selectRaw('sum(cHisProSubTotal) as Subtotal, sum(cHisProImp1) as Impuesto, sum(cHisProSubTotal)+sum(cHisProImp1) as Total')
			->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
			->get();

		$cantidadclientes = historialMesas::selectRaw('sum(cHisMesCantClientes) as CantidadClientes')
			->whereBetween('cHisMesFechaRep', [$fechaDesde, $fechaHasta])
			->first();

		$cortesiasxdescuentos = historialProductos::selectRaw('cHisProCategoria , sum(cHisProCortesia) as Cortesias, sum(cHisProDescuentos) as Descuentos')
			->groupBy('cHisProCategoria')
			->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
			->with(['categorias:cCatFolio,cCatCodigo,cCatDescripcion'])
			->get();

		$sumacortesias = historialProductos::selectRaw('sum(cHisProCortesia) as Cortesias')
			->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
			->first();

		$sumadescuentos = historialProductos::selectRaw('sum(cHisProDescuentos) as Descuentos')
			->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
			->first();
		$Historial = HistorialFormasPagos::selectRaw('cHisForId, sum(cHisForCant) as Cantidad')
			->with(['categoriaformapago:cCatForFolio,cCatForId,cCatForDesc,cCatForAgrupadorCorte'])
			->groupBy('cHisForId')
			->whereNotIn('cHisForId', [1, 4])
			->whereBetween('cHisForFechaRep', [$fechaDesde, $fechaHasta])
			->get();

		$agrupadores = AgrupadorFormaPago::select('cAgForId', 'cAgForDescripcion', 'cAgCategoria')
			->with(['categoriaagrupadorformapago:cCatAgForId,cCatAgForDescripcion'])
			->get();

		$agr = array();


		foreach ($agrupadores as $agrupador) {
			$agrupador->suma = $this->sumarHistorial($agrupador['cAgForId'], $Historial);
			array_push($agr, $agrupador);
		}

		$catagr = CategoriaAgrupadorFormaPago::select('cCatAgForId', 'cCatAgForDescripcion')->get();


		$caja = array();

		foreach ($catagr as $categoriaagrupador) {
			$categoriaagrupador->suma = $this->sumarHistoriaCatAgrupador($categoriaagrupador['cCatAgForId'], $agr);
			array_push($caja, $categoriaagrupador);
		}


		$totalcaja = 0;

		foreach ($caja as $box) {
			$totalcaja += $box->suma;
		}

		unset($caja[0]);

		$totalformapago = 0;

		foreach ($Historial as $hist) {
			$totalformapago += $hist->Cantidad;
		}

		$nuevoefectivo = historialMesas::selectRaw('sum(cHisMesTotal) as CantidadEfectivo')
			->whereBetween('cHisMesFechaRep', [$fechaDesde, $fechaHasta])
			->where('cHisMesVisible', 1)
			->first();

		$totalefectivo = $nuevoefectivo->CantidadEfectivo - $totalformapago;

		$totalcaja = $totalcaja + $totalefectivo;
		$totalformapago = $totalformapago + $totalefectivo;

		$titulo = "Reporte Corte";
		$info_encabezado = ['titulo' => $titulo, 'fecha' => $request->fechaDesde . " - " . $request->fechaHasta];

		$dataGPDF = [
			'fechainicio' => $request->fechaDesde,
			'fechafinal' => $request->fechaHasta,
			'efectivo' => $totalefectivo,
			'caja' => $caja,
			'formapago' => $Historial,
			'totalcaja' => $totalcaja,
			'totalformapago' => $totalformapago,
			'comensales' => $cantidadclientes,
			'deducciones' => $cortesiasxdescuentos,
			'totalcortesias' => $sumacortesias,
			'totaldescuentos' => $sumadescuentos,
			'totalesysubtotales' => $totalesysubtotales
		];


		$header = view()->make('/reportes/reporteCortePdf/header')->with('datosencabezado', $info_encabezado)->render();
		$pdf = PDF::loadview('/reportes/reporteCortePdf/body', compact('dataGPDF'));
		$pdf->setOption('header-html', $header)
			->setOption('header-right', 'PAGINA [page] DE [toPage]')
			->setOption('header-font-name', 'Courier')
			->setOption('header-font-size', '8')
			->setOption('margin-top', 38)
			->setOption('margin-bottom', 10)
			->setOption('margin-left', 10)
			->setOption('margin-right', 10);
		return $pdf->download('reportecorte.pdf');
	}

	public function sumarHistorial($iterador, $historial)
	{
		$suma = 0;
		foreach ($historial as $hist) {
			if ($hist['categoriaformapago']->cCatForAgrupadorCorte == $iterador) {
				$suma += $hist->Cantidad;
			}
		}
		return $suma;
	}


	public function sumarHistoriaCatAgrupador($iterador, $historial)
	{
		$suma = 0;
		foreach ($historial as $hist) {
			foreach ($hist['categoriaagrupadorformapago'] as $h) {
				if ($h['cCatAgForId']  == $iterador) {
					$suma += $hist->suma;
				}
			}
		}
		return $suma;
	}

	public function generarReporteCorte(Request $request)
	{

		$FechaInicio = new Carbon($request->fechaDesde);
		$FechaFin = new Carbon($request->fechaHasta);

		$fechaDesde = date_format($FechaInicio, 'Ymd');
		$fechaHasta = date_format($FechaFin, 'Ymd');
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($fechaDesde, $fechaHasta);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
			$totalesysubtotales = historialProductos::whereHas('Mesa', function ($query) {
				$query->where('cHisMesVisible', '=',  1);
			})->selectRaw('sum(cHisProSubTotal) as Subtotal, sum(cHisProImp1) as Impuesto, sum(cHisProSubTotal)+sum(cHisProImp1) as Total')
				->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
				->get();



			$cantidadclientes = historialMesas::selectRaw('sum(cHisMesCantClientes) as CantidadClientes')
				->whereBetween('cHisMesFechaRep', [$fechaDesde, $fechaHasta])
				->first();

			$cortesiasxdescuentos = historialProductos::selectRaw('cHisProCategoria , sum(cHisProCortesia) as Cortesias, sum(cHisProDescuentos) as Descuentos')
				->groupBy('cHisProCategoria')
				->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
				->with(['categorias:cCatFolio,cCatCodigo,cCatDescripcion'])
				->get();

			$sumacortesias = historialProductos::selectRaw('sum(cHisProCortesia) as Cortesias')
				->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
				->first();

			$sumadescuentos = historialProductos::selectRaw('sum(cHisProDescuentos) as Descuentos')
				->whereBetween('cHisProFechaRep', [$fechaDesde, $fechaHasta])
				->first();


			$Historial = HistorialFormasPagos::whereHas('Mesa', function ($query) {
				$query->where('cHisMesVisible', '=',  1);
			})->selectRaw('cHisForId, sum(cHisForCant) as Cantidad')
				->with(['categoriaformapago:cCatForFolio,cCatForId,cCatForDesc,cCatForAgrupadorCorte'])
				->groupBy('cHisForId')
				->whereNotIn('cHisForId', [1, 4])
				->whereBetween('cHisForFechaRep', [$fechaDesde, $fechaHasta])
				->get();


			$agrupadores = AgrupadorFormaPago::select('cAgForId', 'cAgForDescripcion', 'cAgCategoria')
				->with(['categoriaagrupadorformapago:cCatAgForId,cCatAgForDescripcion'])
				->get();

			$agr = array();


			foreach ($agrupadores as $agrupador) {
				$agrupador->suma = $this->sumarHistorial($agrupador['cAgForId'], $Historial);
				array_push($agr, $agrupador);
			}

			$catagr = CategoriaAgrupadorFormaPago::select('cCatAgForId', 'cCatAgForDescripcion')->get();


			$caja = array();

			foreach ($catagr as $categoriaagrupador) {
				$categoriaagrupador->suma = $this->sumarHistoriaCatAgrupador($categoriaagrupador['cCatAgForId'], $agr);
				array_push($caja, $categoriaagrupador);
			}


			$totalcaja = 0;

			foreach ($caja as $box) {
				$totalcaja += $box->suma;
			}

			unset($caja[0]);



			$totalformapago = 0;

			foreach ($Historial as $hist) {
				$totalformapago += $hist->Cantidad;
			}


			$nuevoefectivo = historialMesas::selectRaw('sum(cHisMesTotal) as CantidadEfectivo')
				->whereBetween('cHisMesFechaRep', [$fechaDesde, $fechaHasta])
				->where('cHisMesVisible', 1)
				->first();

			$totalefectivo = $nuevoefectivo->CantidadEfectivo - $totalformapago;

			$totalcaja = $totalcaja + $totalefectivo;
			$totalformapago = $totalformapago + $totalefectivo;
			return [
				'fechainicio' => $request->fechaDesde,
				'fechafinal' => $request->fechaHasta,
				'efectivo' => $totalefectivo,
				'caja' => $caja,
				'formapago' => $Historial,
				'totalcaja' => $totalcaja,
				'totalformapago' => $totalformapago,
				'comensales' => $cantidadclientes,
				'deducciones' => $cortesiasxdescuentos,
				'totalcortesias' => $sumacortesias,
				'totaldescuentos' => $sumadescuentos,
				'totalesysubtotales' => $totalesysubtotales
			];
		}
	}



	public function indexticket()
	{
		return view('reportes/repticket');
	}

	public function indexproductos()
	{
		return view('reportes/repproductos');
	}

	public function obtenerTop()
	{
		$productos = historialProductos::select('cHisProCodigo', 'cHisProFechaRep', 'cHisProPrecio')->whereBetween('cHisProFechaRep', [20210101, 20210131])->groupBy('cHisProCodigo', 'cHisProFechaRep', 'cHisProPrecio')->get();
		$reporte = historialProductos::select('cHisProFechaRep', 'cHisProDesGeneral', 'cHisProCodigo', 'cHisProCantidad', 'cHisProPrecio')->limit(5)->groupBy('cHisProCodigo')->sum('cHisProCantidad');
		return ['Productos' => $productos, 'reporte' => $reporte];
	}

	public function agregarCero($numero)
	{
		if (strlen($numero) < 2) {
			$numero = '0' . $numero;
		}
		return $numero;
	}
	public function generarMes($Mes, $Ano)
	{
		$Mes = $this->agregarCero($Mes);
		-$ContadorDia = array();
		$PromedioDia = array();
		$TotalMes = 0;
		$TotalPorcentaje = 0;
		$MesCarbon = new Carbon($Ano . '-' . $Mes);
		$DiaF = $MesCarbon->daysInMonth;
		$FechaInicio = new Carbon($Ano . '-' . $Mes . '-' . '1');
		$FechaFin = new Carbon($Ano . '-' . $Mes . '-' . $DiaF);
		$DiasMes = array();
		$Porcentajes = 0;

		while ($FechaInicio->lte($FechaFin)) {
			$dia['DiaSemana'] = $FechaInicio->dayOfWeekIso - 1;
			$dia['Dia'] = $this->agregarCero($FechaInicio->day);
			$dia['SemanaMes'] = $Semana;

			$dia['cCantidad'] = $this->obtenerAjuste($Ano . $Mes . $dia['Dia'], $Sucursal, $tipo);
			$dia['Total'] = $dia['Venta'] + $dia['cCantidad'];
			$TotalDia[$dia['DiaSemana']] = $TotalDia[$dia['DiaSemana']] + $dia['Total'];
			$ContadorDia[$dia['DiaSemana']] = $ContadorDia[$dia['DiaSemana']] + 1;
			$PromedioDia[$dia['DiaSemana']] = $TotalDia[$dia['DiaSemana']] / $ContadorDia[$dia['DiaSemana']];
			$TotalSemana[$Semana] = $TotalSemana[$Semana] + $dia['Total'];

			if ($dia['DiaSemana'] == 6) {
				$Semana = $Semana + 1;
			}
			$TotalMes = $TotalMes + $dia['Total'];
			array_push($DiasMes, $dia);
			$FechaInicio->addDay();
		}
		return [
			'Semana' => $DiasMes, 'Ano' => $Ano, 'Sucursal' => $Sucursal, 'TotalSemanas' => $Semana, 'Mes' => $Mes, 'TotalDia' => $TotalDia, 'TotalSemana' => $TotalSemana, 'TotalMes' => $TotalMes, 'ContadorDias' => $ContadorDia, 'TotalDias' => $DiaF,
			'PromedioDia' => $PromedioDia, 'Porcentajes' => $Porcentajes
		];
	}

	public function generarReporteTickets(Request $request)
	{

		$fechaDesde = str_replace("-", "", $request->fechaDesde);
		$fechaHasta = str_replace("-", "", $request->fechaHasta);

		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($fechaDesde, $fechaHasta);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {

		$reporte_parcial = null;
		if ($request['reporte'] == 0) {
			$reporte_parcial = DB::raw("SELECT cHisMesTicket,cHisMesNombre,cHisMesFechaRep,cHisMesSubTotal,cHisMesTotalImpuesto1,cHisMesTotal,cHisMesCajeroNombre,cHisMesCantClientes,cHisMesFechaEntrada,cHisMesFechaSalida FROM tHisMesas  where cHisMesFechaRep  between '" . $fechaDesde . "' and '" . $fechaHasta . "' ORDER BY cHisMesTicket ASC");
		} else {
			$reporte_parcial = DB::raw("SELECT TB4.cHisMesTicket,cHisMesNombre,EFECTIVO,OTRA,cHisMesFactSoli,cHisMesFechaRep,cHisMesTotal AS TOTAL ,
						CASE WHEN EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
							THEN
								CASE WHEN cHisMesTotal > (cConMinimoMezcla * 100/cConPorcentajeTiraX)
									THEN (cHisMesTotalImpuesto1*(cConPorcentajeTiraX*0.01))
									ELSE
										CASE WHEN cHisMesTotal >= cConMinimoMezcla
										THEN cConMinimoMezcla*cConImpuesto1Cantidad
										ELSE cHisMesTotalImpuesto1
										END
									END
							ELSE cHisMesTotalImpuesto1
						END AS 'cHisMesTotalImpuesto1',
						CASE WHEN EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
							THEN
								CASE WHEN cHisMesTotal > (cConMinimoMezcla * 100/cConPorcentajeTiraX)
									THEN (cHisMesTotal*(cConPorcentajeTiraX*0.01))
									ELSE
										CASE WHEN cHisMesTotal >= cConMinimoMezcla
										THEN cConMinimoMezcla
										ELSE cHisMesTotal
										END
									END
							ELSE cHisMesTotal
							END AS 'cHisMesTotal',
						CASE WHEN  EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
							THEN
								CASE WHEN EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
								THEN
									CASE WHEN cHisMesTotal > (cConMinimoMezcla * 100/cConPorcentajeTiraX)
										THEN (cHisMesSubTotal*(cConPorcentajeTiraX*0.01))
										ELSE
											CASE WHEN cHisMesTotal >= cConMinimoMezcla
											THEN cConMinimoMezcla - (cConMinimoMezcla*cConImpuesto1Cantidad)
											ELSE cHisMesTotal
											END
										END
									END
								ELSE cHisMesSubTotal
								END AS 'cHisMesSubTotal'
							FROM
				(SELECT cHisMesTicket,SUM(efectivo) AS 'EFECTIVO',SUM(otra) AS 'OTRA' FROM(
				SELECT DISTINCT COUNT(cHisForTicket) AS 'CONTADOR' ,cHisMesTicket,CASE WHEN cHisForId=1 THEN (1)ELSE 0 END AS 'efectivo',
				CASE WHEN cHisForId NOT IN (1)
					THEN (1)
					ELSE 0
					END AS 'otra' FROM
				(SELECT * FROM tHisFormaPago RIGHT JOIN tHisMesas ON cHisMesTicket = cHisForTicket)AS TB1 group by cHisMesTicket,cHisForId)AS TB3 group by TB3.cHisMesTicket)AS TB4  FULL JOIN
				(SELECT cHisMesTicket,cHisMesNombre,cHisMesFechaRep,cHisMesSubTotal,cHisMesTotalImpuesto1,cHisMesTotal,cHisMesFactSoli,cConPorcentajeTiraX,cConMinimoMezcla,cConImpuesto1Cantidad FROM tHisMesas CROSS JOIN tConfiguracion  where cHisMesFechaRep     between '" . $fechaDesde . "' and '" . $fechaHasta . "')AS TB5 ON TB4.cHisMesTicket =TB5.cHisMesTicket WHERE TB5.cHisMesTicket IS NOT NULL ORDER BY cHisMesTicket ASC");
		}


		$reporte_parcial = db::select($reporte_parcial);
		$totalTotales = 0;
		$totalSubtotal = 0;
		$totalIva = 0;
		foreach ($reporte_parcial as $key => $ticket) {
			$ticket->cHisMesFechaRep = $this->convertirFecha($ticket->cHisMesFechaRep);
			$totalTotales += $ticket->cHisMesTotal;
			$totalSubtotal += $ticket->cHisMesSubTotal;
			$totalIva += $ticket->cHisMesTotalImpuesto1;
		}

		//return response()->json($reporte);
		return [
			'reporte' => $reporte_parcial,
			'totalTotales' => $totalTotales,
			'subtotal' => $totalSubtotal,
			'impuesto' => $totalIva
		];
	}
	}

	public function descargarPDFticket(Request $request)
	{
		$fechaDesde = str_replace("-", "", $request->fechaDesde);
		$fechaHasta = str_replace("-", "", $request->fechaHasta);
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($fechaDesde, $fechaHasta);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
				$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
				$empresaSel = db::select($datosempresa);
				$empresaSel = json_decode(json_encode($empresaSel), true);


				if ($request['reporte'] == 0) {
					$reporte_parcial = DB::raw("SELECT cHisMesTicket,cHisMesNombre,cHisMesFechaRep,cHisMesSubTotal,cHisMesTotalImpuesto1,cHisMesTotal FROM tHisMesas  where cHisMesFechaRep  between '" . $fechaDesde . "' and '" . $fechaHasta . "' ORDER BY cHisMesTicket ASC");
				} else {
					$reporte_parcial = DB::raw("SELECT TB4.cHisMesTicket,cHisMesNombre,EFECTIVO,OTRA,cHisMesFactSoli,cHisMesFechaRep,cHisMesTotal AS TOTAL,
						CASE WHEN EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
							THEN
								CASE WHEN cHisMesTotal > (cConMinimoMezcla * 100/cConPorcentajeTiraX)
									THEN (cHisMesTotalImpuesto1*(cConPorcentajeTiraX*0.01))
									ELSE
										CASE WHEN cHisMesTotal >= cConMinimoMezcla
										THEN cConMinimoMezcla*cConImpuesto1Cantidad
										ELSE cHisMesTotalImpuesto1
										END
									END
							ELSE cHisMesTotalImpuesto1
						END AS 'cHisMesTotalImpuesto1',
						CASE WHEN EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
							THEN
								CASE WHEN cHisMesTotal > (cConMinimoMezcla * 100/cConPorcentajeTiraX)
									THEN (cHisMesTotal*(cConPorcentajeTiraX*0.01))
									ELSE
										CASE WHEN cHisMesTotal >= cConMinimoMezcla
										THEN cConMinimoMezcla
										ELSE cHisMesTotal
										END
									END
							ELSE cHisMesTotal
							END AS 'cHisMesTotal',
						CASE WHEN  EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
							THEN
								CASE WHEN EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
								THEN
									CASE WHEN cHisMesTotal > (cConMinimoMezcla * 100/cConPorcentajeTiraX)
										THEN (cHisMesSubTotal*(cConPorcentajeTiraX*0.01))
										ELSE
											CASE WHEN cHisMesTotal >= cConMinimoMezcla
											THEN cConMinimoMezcla - (cConMinimoMezcla*cConImpuesto1Cantidad)
											ELSE cHisMesTotal
											END
										END
									END
								ELSE cHisMesSubTotal
								END AS 'cHisMesSubTotal'
							FROM
						(SELECT cHisMesTicket,SUM(efectivo) AS 'EFECTIVO',SUM(otra) AS 'OTRA' FROM(
						SELECT DISTINCT COUNT(cHisForTicket) AS 'CONTADOR' ,cHisMesTicket,CASE WHEN cHisForId=1 THEN (1)ELSE 0 END AS 'efectivo',
						CASE WHEN cHisForId NOT IN (1)
							THEN (1)
							ELSE 0
							END AS 'otra' FROM
						(SELECT * FROM tHisFormaPago RIGHT JOIN tHisMesas ON cHisMesTicket = cHisForTicket)AS TB1 group by cHisMesTicket,cHisForId)AS TB3 group by TB3.cHisMesTicket)AS TB4  FULL JOIN
						(SELECT cHisMesTicket,cHisMesNombre,cHisMesFechaRep,cHisMesSubTotal,cHisMesTotalImpuesto1,cHisMesTotal,cHisMesFactSoli,cConPorcentajeTiraX,cConMinimoMezcla,cConImpuesto1Cantidad FROM tHisMesas CROSS JOIN tConfiguracion  where cHisMesFechaRep     between '" . $fechaDesde . "' and '" . $fechaHasta . "')AS TB5 ON TB4.cHisMesTicket =TB5.cHisMesTicket WHERE TB5.cHisMesTicket IS NOT NULL ORDER BY cHisMesTicket ASC");
				}


				$reporte_parcial = db::select($reporte_parcial);
				$totalTotales = 0;
				$totalSubtotal = 0;
				$totalIva = 0;
				foreach ($reporte_parcial as $key => $ticket) {
					$ticket->cHisMesFechaRep = $this->convertirFecha($ticket->cHisMesFechaRep);
					$totalTotales += $ticket->cHisMesTotal;
					$totalSubtotal += $ticket->cHisMesSubTotal;
					$totalIva += $ticket->cHisMesTotalImpuesto1;
				}
				//return response()->json($reporte);
				$titulo = "Reporte por Ticket";
				$info_encabezado = [
					'titulo' => $titulo, 'fecha' => $request->fechaDesde . " - " . $request->fechaHasta,
					'empresaSeleccionada' => $empresaSel
				];

				$dataGPDF = [
					'reporte' => $reporte_parcial,
					'totalTotales' => $totalTotales,
					'subtotal' => $totalSubtotal,
					'impuesto' => $totalIva
				];
				$header = view()->make('/reportes/reporteticketPDF/header')->with('datosencabezado', $info_encabezado)->render();
				$pdf = PDF::loadview('/reportes/reporteticketPDF/body', compact('dataGPDF'));
				$pdf->setOption('header-html', $header)
					->setOption('header-right', 'PAGINA [page] DE [toPage]')
					->setOption('header-font-name', 'Courier')
					->setOption('header-font-size', '8')
					->setOption('margin-top', 38)
					->setOption('margin-bottom', 10)
					->setOption('margin-left', 10)
					->setOption('margin-right', 10);
				return $pdf->download('reporte.pdf');
			}

	}

	public function genRepProductos(Request $request)
	{

		$FechaInicio = new Carbon($request->fechaDesde);
		$FechaFin = new Carbon($request->fechaHasta);

		$fechaDesde = date_format($FechaInicio, 'Ymd');
		$fechaHasta = date_format($FechaFin, 'Ymd');


		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($request->fechaDesde, $request->fechaHasta);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {

		$productos = DB::raw("Select cHisProSerieLiq, CONCAT(cCatDescripcion,' ',cHisProSerieLiq) AS COMPARAR,cProInterCodigo,cHisProCodigo,cProDescripcion,cCatDescripcion,SUM(cHisProSubTotal) as subtotal,SUM(cHisProImp1) as cHisProImp1,SUM(cHisProCantidad)as 	cantidad,(SUM(cHisProSubTotal)+SUM(cHisProImp1))as totalsuma FROM tHisProductos INNER JOIN tProductos ON tProductos.cProCodigo=tHisProductos.cHisProCodigo INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria WHERE cHisProFechaRep  between '" . $fechaDesde . "' and '" . $fechaHasta . "'  AND cHisProCancel=0 group by cHisProSerieLiq,cHisProCategoria,cHisProCodigo,cProDescripcion,cCatDescripcion,cProInterCodigo");
		$obtenerproductos = db::select($productos);
		$obtenerproductos = json_decode(json_encode($obtenerproductos), true);


		$totalesCategorias = DB::raw("Select cHisProSerieLiq,cHisProCategoria,SUM(cHisProSubTotal)+SUM(cHisProImp1) as total,SUM(cHisProSubTotal) as subtotal,SUM(cHisProImp1) as impuesto,COUNT(cHisProCantidad) as Cantidad,tCategorias.cCatDescripcion FROM tHisProductos INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria WHERE cHisProFechaRep between '" . $fechaDesde . "' and '" . $fechaHasta . "' group by cHisProSerieLiq,cHisProCategoria,cCatDescripcion");

		$totalesCategorias = db::select($totalesCategorias);
		$totalesCategoriasP = json_decode(json_encode($totalesCategorias), true);

		$Cajas = DB::raw("Select cHisProSerieLiq FROM tHisProductos WHERE cHisProFechaRep between '" . $fechaDesde . "' and '" . $fechaHasta . "' AND cHisProCancel=0 group by cHisProSerieLiq");
		$TotalFinal = null;
		$ListaCajas = db::select($Cajas);

		foreach ($ListaCajas as $Caja) {
			$cajasPivote = $Caja->cHisProSerieLiq;
			$Totalesdetotales = DB::raw("Select SUM(cHisProSubTotal)+SUM(cHisProImp1) as total,SUM(cHisProSubTotal) as subtotal,SUM(cHisProImp1) as impuesto,COUNT(cHisProCantidad) as Cantidad FROM tHisProductos INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria WHERE cHisProFechaRep between '" . $fechaDesde . "' and '" . $fechaHasta . "' AND cHisProCancel=0 AND cHisProSerieLiq = '$cajasPivote'");
			$TotalFinal[] = db::select($Totalesdetotales);
		}
		$TotalFinal = json_decode(json_encode($TotalFinal), true);

		$categoriasPivote = array();
		foreach ($obtenerproductos as $productoP) {
			$categoriasPivote[] = $productoP['cCatDescripcion'] . ' ' . $productoP['cHisProSerieLiq'];
		}
		$categoriasPivote = array_unique($categoriasPivote);
		$categoriasCompletas = array();
		$categoriasCompletas = array_values($categoriasPivote);

		$totalProductos = null;
		foreach ($obtenerproductos as $producto) {
			$totalProductos += $producto['totalsuma'];
		}

		return [
			'productos' => $obtenerproductos,
			'totales' => $totalProductos,
			'categorias' => $categoriasCompletas,
			'totalesCategorias' => $totalesCategoriasP,
			'ListaCajas' => $ListaCajas,
			'final' => $TotalFinal
		];
	}
	}

	public function pdfProductos(Request $request)
	{
		$fechaDesde = str_replace("-", "", $request->fechaDesde);
		$fechaHasta = str_replace("-", "", $request->fechaHasta);
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($fechaDesde, $fechaHasta);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
				$productos = DB::raw("Select cHisProSerieLiq, CONCAT(cCatDescripcion,' ',cHisProSerieLiq) AS COMPARAR,cProInterCodigo,cHisProCodigo,cProDescripcion,cCatDescripcion,SUM(cHisProSubTotal) as subtotal,SUM(cHisProImp1) as cHisProImp1,SUM(cHisProCantidad)as 	cantidad,(SUM(cHisProSubTotal)+SUM(cHisProImp1))as totalsuma FROM tHisProductos INNER JOIN tProductos ON tProductos.cProCodigo=tHisProductos.cHisProCodigo INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria WHERE cHisProFechaRep  between '" . $fechaDesde . "' and '" . $fechaHasta . "'  AND cHisProCancel=0 group by cHisProSerieLiq,cHisProCategoria,cHisProCodigo,cProDescripcion,cCatDescripcion,cProInterCodigo");
				$obtenerproductos = db::select($productos);
				$obtenerproductos = json_decode(json_encode($obtenerproductos), true);


				$totalesCategorias = DB::raw("Select cHisProSerieLiq,cHisProCategoria,SUM(cHisProSubTotal)+SUM(cHisProImp1) as total,SUM(cHisProSubTotal) as subtotal,SUM(cHisProImp1) as impuesto,COUNT(cHisProCantidad) as Cantidad,tCategorias.cCatDescripcion FROM tHisProductos INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria WHERE cHisProFechaRep between '" . $fechaDesde . "' and '" . $fechaHasta . "' group by cHisProSerieLiq,cHisProCategoria,cCatDescripcion");

				$totalesCategorias = db::select($totalesCategorias);
				$totalesCategoriasP = json_decode(json_encode($totalesCategorias), true);

				$Cajas = DB::raw("Select cHisProSerieLiq FROM tHisProductos WHERE cHisProFechaRep between '" . $fechaDesde . "' and '" . $fechaHasta . "' AND cHisProCancel=0 group by cHisProSerieLiq");

				$ListaCajas = db::select($Cajas);
				foreach ($ListaCajas as $Caja) {
					$cajasPivote = $Caja->cHisProSerieLiq;
					$Totalesdetotales = DB::raw("Select SUM(cHisProSubTotal)+SUM(cHisProImp1) as total,SUM(cHisProSubTotal) as subtotal,SUM(cHisProImp1) as impuesto,COUNT(cHisProCantidad) as Cantidad FROM tHisProductos INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria WHERE cHisProFechaRep between '" . $fechaDesde . "' and '" . $fechaHasta . "' AND cHisProCancel=0 AND cHisProSerieLiq = '$cajasPivote'");
					$TotalFinal[] = db::select($Totalesdetotales);
				}
				$TotalFinal = json_decode(json_encode($TotalFinal), true);
				$ListaCajas = json_decode(json_encode($ListaCajas), true);
				$categoriasPivote = array();
				foreach ($obtenerproductos as $productoP) {
					$categoriasPivote[] = $productoP['cCatDescripcion'] . ' ' . $productoP['cHisProSerieLiq'];
				}
				$categoriasPivote = array_unique($categoriasPivote);
				$categoriasCompletas = array();
				$categoriasCompletas = array_values($categoriasPivote);

				$totalProductos = null;
				foreach ($obtenerproductos as $producto) {
					$totalProductos += $producto['totalsuma'];
				}

				$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
				$empresaSel = db::select($datosempresa);
				$empresaSel = json_decode(json_encode($empresaSel), true);


				//return ['productos'=>$obtenerproductos,'totales'=>$totalProductos];

				$info_encabezado = [
					'fecha' => $request->fechaDesde . " - " . $request->fechaHasta,
					'empresaSeleccionada' => $empresaSel
				];
				$dataGPDF = [
					'productos' => $obtenerproductos,
					'totales' => $totalProductos,
					'categorias' => $categoriasCompletas,
					'totalesCategorias' => $totalesCategoriasP,
					'ListaCajas' => $ListaCajas,
					'final' => $TotalFinal
				];

				$header = view()->make('/reportes/reporteProductosPDF/header')->with('datosencabezado', $info_encabezado)->render();
				$pdf = PDF::loadview('/reportes/reporteProductosPDF/body', compact('dataGPDF'));
				$pdf->setOption('header-html', $header)
					->setOption('header-right', 'PAGINA [page] DE [toPage]')
					->setOption('header-font-name', 'Courier')
					->setOption('header-font-size', '8')
					->setOption('margin-top', 38)
					->setOption('margin-bottom', 10)
					->setOption('margin-left', 10)
					->setOption('margin-right', 10);
				return $pdf->download('reporte.pdf');
			}
	}

	public function excelProductos(Request $request)
	{
		$fechaDesde = str_replace("-", "", $request->fechaDesde);
		$fechaHasta = str_replace("-", "", $request->fechaHasta);
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($fechaDesde, $fechaHasta);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {

				$productos = DB::raw("Select cHisProSerieLiq, CONCAT(cCatDescripcion,' ',cHisProSerieLiq) AS COMPARAR,cProInterCodigo,cHisProCodigo,cProDescripcion,cCatDescripcion,SUM(cHisProSubTotal) as subtotal,SUM(cHisProImp1) as cHisProImp1,SUM(cHisProCantidad)as 	cantidad,(SUM(cHisProSubTotal)+SUM(cHisProImp1))as totalsuma FROM tHisProductos INNER JOIN tProductos ON tProductos.cProCodigo=tHisProductos.cHisProCodigo INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria WHERE cHisProFechaRep  between '" . $fechaDesde . "' and '" . $fechaHasta . "'  AND cHisProCancel=0 group by cHisProSerieLiq,cHisProCategoria,cHisProCodigo,cProDescripcion,cCatDescripcion,cProInterCodigo");
				$obtenerproductos = db::select($productos);
				$obtenerproductos = json_decode(json_encode($obtenerproductos), true);


				$totalesCategorias = DB::raw("Select cHisProSerieLiq,cHisProCategoria,SUM(cHisProSubTotal)+SUM(cHisProImp1) as total,SUM(cHisProSubTotal) as subtotal,SUM(cHisProImp1) as impuesto,COUNT(cHisProCantidad) as Cantidad,tCategorias.cCatDescripcion FROM tHisProductos INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria WHERE cHisProFechaRep between '" . $fechaDesde . "' and '" . $fechaHasta . "' group by cHisProSerieLiq,cHisProCategoria,cCatDescripcion");

				$totalesCategorias = db::select($totalesCategorias);
				$totalesCategoriasP = json_decode(json_encode($totalesCategorias), true);

				$Totalesdetotales = DB::raw("Select SUM(cHisProSubTotal)+SUM(cHisProImp1) as total,SUM(cHisProSubTotal) as subtotal,SUM(cHisProImp1) as impuesto,COUNT(cHisProCantidad) as Cantidad FROM tHisProductos INNER JOIN tCategorias ON tCategorias.cCatCodigo=tHisProductos.cHisProCategoria WHERE cHisProFechaRep between '" . $fechaDesde . "' and '" . $fechaHasta . "' AND cHisProCancel=0");

				$TotalFinal = db::select($Totalesdetotales);
				$TotalFinal = json_decode(json_encode($TotalFinal), true);

				$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
				$empresaSel = db::select($datosempresa);
				$empresaSel = json_decode(json_encode($empresaSel), true);

				$categoriasPivote = array();
				foreach ($obtenerproductos as $productoP) {
					$categoriasPivote[] = $productoP['cCatDescripcion'] . ' ' . $productoP['cHisProSerieLiq'];
				}
				$categoriasPivote = array_unique($categoriasPivote);
				$categoriasCompletas = array();
				$categoriasCompletas = $categoriasPivote;

				$totalProductos = null;
				foreach ($obtenerproductos as $producto) {
					$totalProductos += $producto['totalsuma'];
				}


				//return ['productos'=>$obtenerproductos,'totales'=>$totalProductos];

				$info_encabezado = [
					'fecha' => $request->fechaDesde . " - " . $request->fechaHasta,
					'empresaSeleccionada' => $empresaSel
				];
				$dataGPDF = [
					'productos' => $obtenerproductos,
					'totales' => $totalProductos,
					'categorias' => $categoriasCompletas,
					'totalesCategorias' => $totalesCategoriasP,
					'final' => $TotalFinal
				];


				Excel::create('ventas', function ($excel) use (
					$info_encabezado,
					$dataGPDF
				) {

					$excel->sheet('ventas', function ($sheet) use (
						$info_encabezado,
						$dataGPDF
					) {

						$sheet->loadView("reportes.reporteProductosExcel.reporteexcelproductos")
							->with(compact(
								'info_encabezado',
								'dataGPDF'
							));
					});
				})->download('csv');
			}
	}
	public function convertirFecha($fecha='')
	{
		$fecha = strval($fecha);
		$year = substr($fecha,0,4);
		$month = substr($fecha,4,2);
		$day = substr($fecha,6,2);
		return $year.'/'.$month.'/'.$day;
	}

	public function excelTicket(Request $request)
	{
		$fechaDesde = str_replace("-", "", $request->fechaDesde);
		$fechaHasta = str_replace("-", "", $request->fechaHasta);
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($fechaDesde, $fechaHasta);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
				$porcentajeMezcla = DB::table('tConfiguracion')
					->select('cConPorcentaje ')
					->get();
				$porcentajeMezcla = json_decode(json_encode($porcentajeMezcla), true);

				if ($request['reporte'] == 0) {
					$reporte_parcial = DB::raw("SELECT cHisMesTicket,cHisMesNombre,cHisMesFechaRep,cHisMesSubTotal,cHisMesTotalImpuesto1,cHisMesTotal FROM tHisMesas  where cHisMesFechaRep  between '" . $fechaDesde . "' and '" . $fechaHasta . "'");
				} else {
					$reporte_parcial = DB::raw("SELECT TB4.cHisMesTicket,cHisMesNombre,EFECTIVO,OTRA,cHisMesFactSoli,cHisMesFechaRep,cHisMesTotal AS TOTAL,
						CASE WHEN EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
							THEN
								CASE WHEN cHisMesTotal > (cConMinimoMezcla * 100/cConPorcentajeTiraX)
									THEN (cHisMesTotalImpuesto1*(cConPorcentajeTiraX*0.01))
									ELSE
										CASE WHEN cHisMesTotal >= cConMinimoMezcla
										THEN cConMinimoMezcla*cConImpuesto1Cantidad
										ELSE cHisMesTotalImpuesto1
										END
									END
							ELSE cHisMesTotalImpuesto1
						END AS 'cHisMesTotalImpuesto1',
						CASE WHEN EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
							THEN
								CASE WHEN cHisMesTotal > (cConMinimoMezcla * 100/cConPorcentajeTiraX)
									THEN (cHisMesTotal*(cConPorcentajeTiraX*0.01))
									ELSE
										CASE WHEN cHisMesTotal >= cConMinimoMezcla
										THEN cConMinimoMezcla
										ELSE cHisMesTotal
										END
									END
							ELSE cHisMesTotal
							END AS 'cHisMesTotal',
						CASE WHEN  EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
							THEN
								CASE WHEN EFECTIVO>0 AND OTRA=0 AND cHisMesFactSoli=0
								THEN
									CASE WHEN cHisMesTotal > (cConMinimoMezcla * 100/cConPorcentajeTiraX)
										THEN (cHisMesSubTotal*(cConPorcentajeTiraX*0.01))
										ELSE
											CASE WHEN cHisMesTotal >= cConMinimoMezcla
											THEN cConMinimoMezcla - (cConMinimoMezcla*cConImpuesto1Cantidad)
											ELSE cHisMesTotal
											END
										END
									END
								ELSE cHisMesSubTotal
								END AS 'cHisMesSubTotal'
							FROM
						(SELECT cHisMesTicket,SUM(efectivo) AS 'EFECTIVO',SUM(otra) AS 'OTRA' FROM(
						SELECT DISTINCT COUNT(cHisForTicket) AS 'CONTADOR' ,cHisMesTicket,CASE WHEN cHisForId=1 THEN (1)ELSE 0 END AS 'efectivo',
						CASE WHEN cHisForId NOT IN (1)
							THEN (1)
							ELSE 0
							END AS 'otra' FROM
						(SELECT * FROM tHisFormaPago RIGHT JOIN tHisMesas ON cHisMesTicket = cHisForTicket)AS TB1 group by cHisMesTicket,cHisForId)AS TB3 group by TB3.cHisMesTicket)AS TB4  FULL JOIN
						(SELECT cHisMesTicket,cHisMesNombre,cHisMesFechaRep,cHisMesSubTotal,cHisMesTotalImpuesto1,cHisMesTotal,cHisMesFactSoli,cConPorcentajeTiraX,cConMinimoMezcla,cConImpuesto1Cantidad FROM tHisMesas CROSS JOIN tConfiguracion  where cHisMesFechaRep     between '" . $fechaDesde . "' and '" . $fechaHasta . "')AS TB5 ON TB4.cHisMesTicket =TB5.cHisMesTicket WHERE TB5.cHisMesTicket IS NOT NULL ORDER BY cHisMesTicket ASC");
				}


				$reporte_parcial = db::select($reporte_parcial);
				$totalTotales = 0;
				$totalSubtotal = 0;
				$totalIva = 0;
				foreach ($reporte_parcial as $key => $ticket) {
					$ticket->cHisMesFechaRep = $this->convertirFecha($ticket->cHisMesFechaRep);
					$totalTotales += $ticket->cHisMesTotal;
					$totalSubtotal += $ticket->cHisMesSubTotal;
					$totalIva += $ticket->cHisMesTotalImpuesto1;
				}
				//return response()->json($reporte);


				$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
				$empresaSel = db::select($datosempresa);
				$empresaSel = json_decode(json_encode($empresaSel), true);


				$info_encabezado = [
					'fecha' => $request->fechaDesde . " - " . $request->fechaHasta,
					'empresaSeleccionada' => $empresaSel
				];
				$dataGPDF = [
					'reporte' => $reporte_parcial,
					'totalTotales' => $totalTotales,
					'subtotal' => $totalSubtotal,
					'impuesto' => $totalIva
				];



				Excel::create('ventas', function ($excel) use (
					$info_encabezado,
					$dataGPDF
				) {

					$excel->sheet('ventas', function ($sheet) use (
						$info_encabezado,
						$dataGPDF
					) {

						$sheet->loadView("reportes.reporteticketExcel.reporteticketExcel")
							->with(compact(
								'info_encabezado',
								'dataGPDF'
							));
					});
				})->download('csv');
			}
	}

	public function indexDeglose()
	{
		return view("reportes.ticketdesglosado");
	}

	public function generarDesglose(Request $request)
	{

		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);


		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($inicio, $termino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {


		$oHisMesas = historialMesas::whereBetween('cHisMesFechaRep', [$inicio, $termino])
			->get();

		$productosHis = historialProductos::selectRaw("FORMAT (getdate(), 'hh:mm:ss tt') as hora, cHisProCantidad, cHisProDesTicket,cHisProCodigo,cHisProSubTotal,cHisProImp1,cHisProTicket")->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->get();

		$totalesProductos = historialProductos::selectRaw("SUM(cHisProSubTotal) as Subtotal,SUM(cHisProImp1) as Impuesto,(SUM(cHisProSubTotal)+SUM(cHisProImp1)) as Total,cHisProTicket")->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cHisProTicket')
			->get();

		$oHisPagos = HistorialFormasPagos::whereBetween('cHisForFechaRep', [$inicio, $termino])
			->get();

		return ['mesas' => $oHisMesas, 'productos' => $productosHis, 'totalProductos' => $totalesProductos, 'formasdepago' => $oHisPagos];
		}
	}

    public function generarPDFDesgloseEN(Request $request)
	{
        $inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($inicio, $termino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
				$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
				$empresaSel = DB::select($datosempresa);
				$empresaSel = json_decode(json_encode($empresaSel), true);

				$oHisMesas = historialMesas::whereBetween('cHisMesFechaRep', [$inicio, $termino])
					->get();

				$productosHis = historialProductos::whereBetween('cHisProFechaRep', [$inicio, $termino])
					->get();

				$totalesProductos = historialProductos::selectRaw("SUM(cHisProSubTotal) as Subtotal
                    , SUM(cHisProImp1) as Impuesto
                    , SUM(cHisProImp2) as Impuesto2
                    , SUM(cHisProImp3) as Impuesto3
                    , SUM(cHisProImp4) as Impuesto4
                    , (SUM(cHisProSubTotal) + SUM(cHisProImp1) + SUM(cHisProImp2) + SUM(cHisProImp3) + SUM(cHisProImp4) ) as Total
                    , cHisProTicket")
                    ->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->groupBy('cHisProTicket')
					->get();

				$oHisPagos = HistorialFormasPagos::selectRaw("cHisForTicket, cHisForId, cHisForDes, SUM(cHisForCant) as cHisForCant")
                    ->whereBetween('cHisForFechaRep', [$inicio, $termino])
                    ->where('cHisForTipo', '=', 1)
                    ->groupBy('cHisForTicket', 'cHisForId', 'cHisForDes')
					->get();

				$info_encabezado = ['fecha' => $request->inicio . " - " . $request->termino, 'empresaSeleccionada' => $empresaSel];
				$dataGPDF = ['mesas' => $oHisMesas, 'productos' => $productosHis, 'totalProductos' => $totalesProductos, 'formasdepago' => $oHisPagos];

				$header = view()->make('/reportes/reporteDesglosePDF/en/header')->with('datosencabezado', $info_encabezado)->render();
				$pdf = PDF::loadview('/reportes/reporteDesglosePDF/en/body', compact('dataGPDF'));
				$pdf->setOption('header-html', $header)
					->setOption('header-right', 'PAGINA [page] DE [toPage]')
					->setOption('header-font-name', 'Courier')
					->setOption('header-font-size', '8')
					->setOption('margin-top', 38)
					->setOption('margin-bottom', 10)
					->setOption('margin-left', 10)
					->setOption('margin-right', 10);
				return $pdf->download('reporte.pdf');
			}
	}

	public function generarPDF(Request $request)
	{

		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($inicio, $termino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
				$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
				$empresaSel = db::select($datosempresa);
				$empresaSel = json_decode(json_encode($empresaSel), true);

				$oHisMesas = historialMesas::whereBetween('cHisMesFechaRep', [$inicio, $termino])
					->get();

				$productosHis = historialProductos::whereBetween('cHisProFechaRep', [$inicio, $termino])
					->get();

				$totalesProductos = historialProductos::selectRaw("SUM(cHisProSubTotal) as Subtotal,SUM(cHisProImp1) as Impuesto,(SUM(cHisProSubTotal)+SUM(cHisProImp1)) as Total,cHisProTicket")->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->groupBy('cHisProTicket')
					->get();

				$oHisPagos = HistorialFormasPagos::whereBetween('cHisForFechaRep', [$inicio, $termino])
					->get();


				$info_encabezado = ['fecha' => $request->inicio . " - " . $request->termino, 'empresaSeleccionada' => $empresaSel];
				$dataGPDF = ['mesas' => $oHisMesas, 'productos' => $productosHis, 'totalProductos' => $totalesProductos, 'formasdepago' => $oHisPagos];


				$header = view()->make('/reportes/reporteDesglosePDF/header')->with('datosencabezado', $info_encabezado)->render();
				$pdf = PDF::loadview('/reportes/reporteDesglosePDF/body', compact('dataGPDF'));
				$pdf->setOption('header-html', $header)
					->setOption('header-right', 'PAGINA [page] DE [toPage]')
					->setOption('header-font-name', 'Courier')
					->setOption('header-font-size', '8')
					->setOption('margin-top', 38)
					->setOption('margin-bottom', 10)
					->setOption('margin-left', 10)
					->setOption('margin-right', 10);
				return $pdf->download('reporte.pdf');
			}
	}

	public function generarDesgloseEXCEL(Request $request)
	{
		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($inicio, $termino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
				$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
				$empresaSel = db::select($datosempresa);
				$empresaSel = json_decode(json_encode($empresaSel), true);


				$oHisMesas = historialMesas::whereBetween('cHisMesFechaRep', [$inicio, $termino])
					->get();

				$productosHis = historialProductos::whereBetween('cHisProFechaRep', [$inicio, $termino])
					->get();

				$totalesProductos = historialProductos::selectRaw("SUM(cHisProSubTotal) as Subtotal,SUM(cHisProImp1) as Impuesto,(SUM(cHisProSubTotal)+SUM(cHisProImp1)) as Total,cHisProTicket")->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->groupBy('cHisProTicket')
					->get();

				$oHisPagos = HistorialFormasPagos::whereBetween('cHisForFechaRep', [$inicio, $termino])
					->get();


				$info_encabezado = ['fecha' => $request->inicio . " - " . $request->termino, 'empresaSeleccionada' => $empresaSel];
				$dataGPDF = ['mesas' => $oHisMesas, 'productos' => $productosHis, 'totalProductos' => $totalesProductos, 'formasdepago' => $oHisPagos];

				Excel::create('ventas', function ($excel) use (
					$info_encabezado,
					$dataGPDF
				) {

					$excel->sheet('ventas', function ($sheet) use (
						$info_encabezado,
						$dataGPDF
					) {

						$sheet->loadView("reportes.reporteDesgloseEXCEL.reportedesglose")
							->with(compact(
								'info_encabezado',
								'dataGPDF'
							));
					});
				})->download('csv');
			}
	}

	public function indexDescuentos()
	{
		return view("reportes.reportepordescuentos");
	}

	public function generarDescuentos(Request $request)
	{
		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);
		$productos = historialMesas::Join('tHisProductos', 'cHisProTicket', 'cHisMesTicket')->whereBetween('cHisMesFechaRep', [$inicio, $termino])->where('cHisProDescuentos', '<>', 0)->get()->toArray();
		if (!$productos) {
			$productos = null;
		}
		return ['productos' => $productos];
	}

	public function indexArticulo()
	{
		return view('reportes.repPlatillos');
	}

	public function obtenerArticulos(Request $request)
	{
		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);

		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($inicio, $termino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {

		$ventaPorArticulos = historialProductos::selectRaw("SUM(cHisProCantidad)as cantidadProductos,cProDescripcion,SUM(cHisProPrecio) as precio")
			->Join('tProductos', 'cHisProCodigo', 'cProCodigo')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->where('cHisProCancel', 0)
			->groupBy('cProDescripcion')
			->orderBy('precio', 'desc')->get()->toArray();

		$totalesArticulos = historialProductos::selectRaw("SUM(cHisProCantidad)as cantidadProductos,SUM(cHisProPrecio) as precio")
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->where('cHisProCancel', 0)
			->get()->toArray();
		if (!$totalesArticulos[0]['precio']) {
			$totalesArticulos = null;
		}

		return ['Articulos' => $ventaPorArticulos, 'totales' => $totalesArticulos];
	}
	}

	public function obtenerArticulosPDF(Request $request)
	{
		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($inicio, $termino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
				//Llamar empresa
				$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
				$empresaSel = db::select($datosempresa);
				$empresaSel = json_decode(json_encode($empresaSel), true);

				//logica para generar el reporte
				$ventaPorArticulos = historialProductos::selectRaw("SUM(cHisProCantidad)as cantidadProductos,cProDescripcion,SUM(cHisProPrecio) as precio")
					->Join('tProductos', 'cHisProCodigo', 'cProCodigo')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->where('cHisProCancel', 0)
					->groupBy('cProDescripcion')
					->orderBy('precio', 'desc')->get()->toArray();

				$totalesArticulos = historialProductos::selectRaw("SUM(cHisProCantidad)as cantidadProductos,SUM(cHisProPrecio) as precio")
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->where('cHisProCancel', 0)
					->get()->toArray();
				if (!$totalesArticulos[0]['precio']) {
					$totalesArticulos = null;
				}
				//variables a enviar a PDF
				$dataGPDF = ['Articulos' => $ventaPorArticulos, 'totales' => $totalesArticulos];
				$info_encabezado = ['fecha' => $request->inicio . " - " . $request->termino, 'empresaSeleccionada' => $empresaSel];

				//COnfiguracion del PDF
				$header = view()->make('/reportes/reporteArticulosPDF/header')->with('datosencabezado', $info_encabezado)->render();
				$pdf = PDF::loadview('/reportes/reporteArticulosPDF/body', compact('dataGPDF'));
				$pdf->setOption('header-html', $header)
					->setOption('header-right', 'PAGINA [page] DE [toPage]')
					->setOption('header-font-name', 'Courier')
					->setOption('header-font-size', '8')
					->setOption('margin-top', 38)
					->setOption('margin-bottom', 10)
					->setOption('margin-left', 10)
					->setOption('margin-right', 10);
				return $pdf->download('reporte.pdf');
			}
	}

	public function obtenerArticulosEXCEL(Request $request)
	{
		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);
		$igual = app(Configuracion\ConfiguracionController::class)
				->obtenerCorteFecha($inicio, $termino);
		if ($igual == 0) {
			return 0;
		} elseif ($igual == 1) {
				//Llamar empresa
				$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
				$empresaSel = db::select($datosempresa);
				$empresaSel = json_decode(json_encode($empresaSel), true);

				//logica para generar el reporte
				$ventaPorArticulos = historialProductos::selectRaw("SUM(cHisProCantidad)as cantidadProductos,cProDescripcion,SUM(cHisProPrecio) as precio")
					->Join('tProductos', 'cHisProCodigo', 'cProCodigo')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->where('cHisProCancel', 0)
					->groupBy('cProDescripcion')
					->orderBy('precio', 'desc')->get()->toArray();

				$totalesArticulos = historialProductos::selectRaw("SUM(cHisProCantidad)as cantidadProductos,SUM(cHisProPrecio) as precio")
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->where('cHisProCancel', 0)
					->get()->toArray();
				if (!$totalesArticulos[0]['precio']) {
					$totalesArticulos = null;
				}
				//variables a enviar a PDF
				$dataGPDF = ['Articulos' => $ventaPorArticulos, 'totales' => $totalesArticulos];
				$info_encabezado = ['fecha' => $request->inicio . " - " . $request->termino, 'empresaSeleccionada' => $empresaSel];

				Excel::create('ventas', function ($excel) use (
					$info_encabezado,
					$dataGPDF
				) {

					$excel->sheet('ventas', function ($sheet) use (
						$info_encabezado,
						$dataGPDF
					) {

						$sheet->loadView("reportes.reporteArticulosExcel.reporteArticulosExcel")
							->with(compact(
								'info_encabezado',
								'dataGPDF'
							));
					});
				})->download('csv');
			}
	}

	public function indexPlatillo()
	{
		return view('reportes.reporteporPlatillos');
	}

	public function obtenerreportePlatillosGen(Request $request)
	{
		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);


		$categoriasSel = array();
		$contadorCategorias = $request->categorias;
		if ($contadorCategorias) {
			foreach ($contadorCategorias as $key) {
				$categoriasSel[] = $key['cCatCodigo'];
			}
			if (count($contadorCategorias) == 0) {
				$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->orderBy('cCatDescripcion')->get();

				$productos = historialProductos::selectRaw('cHisProDesTicket,SUM(cHisProPrecio)as precio,SUM(cHisProCantidad) as cantidad,cHisProSubCategoria,cHisProCategoria')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->groupBy('cHisProCodigo', 'cHisProDesTicket', 'cHisProSubCategoria', 'cHisProCategoria')
					->get();

				$TotalGeneral = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->get();
			} else {
				$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->whereIn('cCatCodigo', $categoriasSel)
					->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->orderBy('cCatDescripcion')->get();

				$productos = historialProductos::selectRaw('cHisProDesTicket,SUM(cHisProPrecio)as precio,SUM(cHisProCantidad) as cantidad,cHisProSubCategoria,cHisProCategoria')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->whereIn('cHisProCategoria', $categoriasSel)
					->groupBy('cHisProCodigo', 'cHisProDesTicket', 'cHisProSubCategoria', 'cHisProCategoria')
					->get();

				$TotalGeneral = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->whereIn('cHisProCategoria', $categoriasSel)
					->get();
			}
		} else {
			$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
				->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
				->whereBetween('cHisProFechaRep', [$inicio, $termino])
				->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
				->orderBy('cCatDescripcion')->get();

			$productos = historialProductos::selectRaw('cHisProDesTicket,SUM(cHisProPrecio)as precio,SUM(cHisProCantidad) as cantidad,cHisProSubCategoria,cHisProCategoria')
				->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
				->whereBetween('cHisProFechaRep', [$inicio, $termino])
				->groupBy('cHisProCodigo', 'cHisProDesTicket', 'cHisProSubCategoria', 'cHisProCategoria')
				->get();

			$TotalGeneral = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad')
				->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
				->whereBetween('cHisProFechaRep', [$inicio, $termino])
				->get();
		}


		$subcategorias = SubCategorias::select('cSCatFolio', 'cSCatCodigo', 'cSCatDescripcion', 'cSCatPadre')
			->Join('tHisProductos', 'cHisProSubCategoria', 'cSCatCodigo')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cSCatFolio', 'cSCatCodigo', 'cSCatDescripcion', 'cSCatPadre')
			->get();


		$TotalCategorias = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad,cHisProCategoria as categoria')
			->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cHisProCategoria')
			->get();
		$TotalSubCategorias = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad,cHisProCategoria ,cHisProSubCategoria')
			->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cHisProSubCategoria', 'cHisProCategoria')
			->get();

		$productos = json_decode(json_encode($productos), true);
		if (!$productos) {
			$productos = null;
		}

		return ['categorias' => $categorias, 'subcategorias' => $subcategorias, 'productos' => $productos, 'totalgeneral' => $TotalGeneral, 'totalcategorias' => $TotalCategorias, 'totalsubcategorias' => $TotalSubCategorias];
	}

	public function obtenerCategorias()
	{
		$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
			->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
			->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
			->orderBy('cCatDescripcion')->get();
		return ['categorias' => $categorias];
	}

	public function generarPDFplatillos(Request $request)
	{
		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);
		$categoriasSel = array();
		$contadorCategorias = $request->categorias;
		if ($contadorCategorias) {
			foreach ($contadorCategorias as $key) {
				$categoriasSel[] = $key['cCatCodigo'];
			}
			if (count($contadorCategorias) == 0) {
				$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->orderBy('cCatDescripcion')->get();

				$productos = historialProductos::selectRaw('cHisProDesTicket,SUM(cHisProPrecio)as precio,SUM(cHisProCantidad) as cantidad,cHisProSubCategoria,cHisProCategoria')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->groupBy('cHisProCodigo', 'cHisProDesTicket', 'cHisProSubCategoria', 'cHisProCategoria')
					->get();

				$TotalGeneral = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->get();
			} else {
				$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->whereIn('cCatCodigo', $categoriasSel)
					->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->orderBy('cCatDescripcion')->get();

				$productos = historialProductos::selectRaw('cHisProDesTicket,SUM(cHisProPrecio)as precio,SUM(cHisProCantidad) as cantidad,cHisProSubCategoria,cHisProCategoria')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->whereIn('cHisProCategoria', $categoriasSel)
					->groupBy('cHisProCodigo', 'cHisProDesTicket', 'cHisProSubCategoria', 'cHisProCategoria')
					->get();

				$TotalGeneral = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->whereIn('cHisProCategoria', $categoriasSel)
					->get();
			}
		} else {
			$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
				->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
				->whereBetween('cHisProFechaRep', [$inicio, $termino])
				->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
				->orderBy('cCatDescripcion')->get();

			$productos = historialProductos::selectRaw('cHisProDesTicket,SUM(cHisProPrecio)as precio,SUM(cHisProCantidad) as cantidad,cHisProSubCategoria,cHisProCategoria')
				->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
				->whereBetween('cHisProFechaRep', [$inicio, $termino])
				->groupBy('cHisProCodigo', 'cHisProDesTicket', 'cHisProSubCategoria', 'cHisProCategoria')
				->get();

			$TotalGeneral = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad')
				->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
				->whereBetween('cHisProFechaRep', [$inicio, $termino])
				->get();
		}


		$subcategorias = SubCategorias::select('cSCatFolio', 'cSCatCodigo', 'cSCatDescripcion', 'cSCatPadre')
			->Join('tHisProductos', 'cHisProSubCategoria', 'cSCatCodigo')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cSCatFolio', 'cSCatCodigo', 'cSCatDescripcion', 'cSCatPadre')
			->get();


		$TotalCategorias = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad,cHisProCategoria as categoria')
			->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cHisProCategoria')
			->get();
		$TotalSubCategorias = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad,cHisProCategoria ,cHisProSubCategoria')
			->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cHisProSubCategoria', 'cHisProCategoria')
			->get();

		$productos = json_decode(json_encode($productos), true);
		if (!$productos) {
			$productos = null;
		}



		$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
		$empresaSel = db::select($datosempresa);
		$empresaSel = json_decode(json_encode($empresaSel), true);

		$dataGPDF = ['categorias' => $categorias, 'subcategorias' => $subcategorias, 'productos' => $productos, 'totalgeneral' => $TotalGeneral, 'totalcategorias' => $TotalCategorias, 'totalsubcategorias' => $TotalSubCategorias];

		$info_encabezado = ['fecha' => $request->inicio . " - " . $request->termino, 'empresaSeleccionada' => $empresaSel];


		$header = view()->make('/reportes/reportePlatillos/header')->with('datosencabezado', $info_encabezado)->render();
		$pdf = PDF::loadview('/reportes/reportePlatillos/body', compact('dataGPDF'));
		$pdf->setOption('header-html', $header)
			->setOption('header-right', 'PAGINA [page] DE [toPage]')
			->setOption('header-font-name', 'Courier')
			->setOption('header-font-size', '8')
			->setOption('margin-top', 38)
			->setOption('margin-bottom', 10)
			->setOption('margin-left', 10)
			->setOption('margin-right', 10);
		return $pdf->download('reporte.pdf');
	}

	public function generarEXCELplatillos(Request $request)
	{
		$inicio = str_replace("-", "", $request->inicio);
		$termino = str_replace("-", "", $request->termino);
		$categoriasSel = array();
		$contadorCategorias = $request->categorias;
		if ($contadorCategorias) {
			foreach ($contadorCategorias as $key) {
				$categoriasSel[] = $key['cCatCodigo'];
			}
			if (count($contadorCategorias) == 0) {
				$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->orderBy('cCatDescripcion')->get();

				$productos = historialProductos::selectRaw('cHisProDesTicket,SUM(cHisProPrecio)as precio,SUM(cHisProCantidad) as cantidad,cHisProSubCategoria,cHisProCategoria')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->groupBy('cHisProCodigo', 'cHisProDesTicket', 'cHisProSubCategoria', 'cHisProCategoria')
					->get();

				$TotalGeneral = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->get();
			} else {
				$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->whereIn('cCatCodigo', $categoriasSel)
					->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
					->orderBy('cCatDescripcion')->get();

				$productos = historialProductos::selectRaw('cHisProDesTicket,SUM(cHisProPrecio)as precio,SUM(cHisProCantidad) as cantidad,cHisProSubCategoria,cHisProCategoria')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->whereIn('cHisProCategoria', $categoriasSel)
					->groupBy('cHisProCodigo', 'cHisProDesTicket', 'cHisProSubCategoria', 'cHisProCategoria')
					->get();

				$TotalGeneral = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad')
					->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
					->whereBetween('cHisProFechaRep', [$inicio, $termino])
					->whereIn('cHisProCategoria', $categoriasSel)
					->get();
			}
		} else {
			$categorias = Categorias::select('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
				->Join('tHisProductos', 'cHisProCategoria', 'cCatCodigo')
				->whereBetween('cHisProFechaRep', [$inicio, $termino])
				->groupBy('cCatCodigo', 'cCatDescripcion', 'cCatFolio')
				->orderBy('cCatDescripcion')->get();

			$productos = historialProductos::selectRaw('cHisProDesTicket,SUM(cHisProPrecio)as precio,SUM(cHisProCantidad) as cantidad,cHisProSubCategoria,cHisProCategoria')
				->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
				->whereBetween('cHisProFechaRep', [$inicio, $termino])
				->groupBy('cHisProCodigo', 'cHisProDesTicket', 'cHisProSubCategoria', 'cHisProCategoria')
				->get();

			$TotalGeneral = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad')
				->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
				->whereBetween('cHisProFechaRep', [$inicio, $termino])
				->get();
		}


		$subcategorias = SubCategorias::select('cSCatFolio', 'cSCatCodigo', 'cSCatDescripcion', 'cSCatPadre')
			->Join('tHisProductos', 'cHisProSubCategoria', 'cSCatCodigo')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cSCatFolio', 'cSCatCodigo', 'cSCatDescripcion', 'cSCatPadre')
			->get();


		$TotalCategorias = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad,cHisProCategoria as categoria')
			->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cHisProCategoria')
			->get();
		$TotalSubCategorias = historialProductos::selectRaw('ROUND(SUM(cHisProCantidad),2) as cantidad,cHisProCategoria ,cHisProSubCategoria')
			->Join('tSubCategorias', 'cSCatCodigo', 'cHisProSubCategoria')
			->whereBetween('cHisProFechaRep', [$inicio, $termino])
			->groupBy('cHisProSubCategoria', 'cHisProCategoria')
			->get();

		$productos = json_decode(json_encode($productos), true);
		if (!$productos) {
			$productos = null;
		}





		$datosempresa = DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
		$empresaSel = db::select($datosempresa);
		$empresaSel = json_decode(json_encode($empresaSel), true);

		$dataGPDF = ['categorias' => $categorias, 'subcategorias' => $subcategorias, 'productos' => $productos, 'totalgeneral' => $TotalGeneral, 'totalcategorias' => $TotalCategorias, 'totalsubcategorias' => $TotalSubCategorias];

		$info_encabezado = ['fecha' => $request->inicio . " - " . $request->termino, 'empresaSeleccionada' => $empresaSel];


		Excel::create('ventas', function ($excel) use (
			$info_encabezado,
			$dataGPDF
		) {

			$excel->sheet('ventas', function ($sheet) use (
				$info_encabezado,
				$dataGPDF
			) {

				$sheet->loadView("reportes.reporteExcelPlatillos.reporteplatillo")
					->with(compact(
						'info_encabezado',
						'dataGPDF'
					));
			});
		})->download('csv');
	}
}
