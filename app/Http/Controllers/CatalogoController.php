<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Configuracion;
use App\Perfiles;
use App\SubCategorias;
use App\Categorias;
use App\Productos;
use Carbon\Carbon;
use App\Clasificacion;

class CatalogoController extends Controller
{
	public function indexEmpleados(){
		return view ('catalogos.empleados.index');
	}

	public function IndexProductos(){
		return view('catalogos.productos.index');
	}
	public function getUser(){
		$configuracion=Configuracion::select('cConModUsuarios')->get()->toArray();
		$cCovidActivo=Configuracion::select('cConEncCovid19')->get()->toArray();
		$configuracion=$configuracion[0]['cConModUsuarios'];
		$usuarios=null;

			$usuarios = User::with('Perfil')
			->select('cUsuAutoCovid',
            'cUsuFolio',
            'cUsuCodigo',
            'cUsuNombre',
            'cUsuApellido',
            'cUsuActivo',
            'cUsuClave',
            'cUsuPuesto1Activo',
            'cUsuPuesto1Desc',
            'cUsuPuesto1Perfil',
            'cUsuPuesto2Activo',
            'cUsuPuesto2Desc',
            'cUsuPuesto2Perfil',
            'cUsuPuesto3Activo',
            'cUsuPuesto3Desc',
            'cUsuPuesto3Perfil',
            'cUsuNumSeguro',
            'cUsuDireccion',
            'cUsuTelefono',
            'cUsuFechNac',
            'cUsuAccesoAdmin',
            'cUsuPermisosPortal',
            'tUsuReiniciar',
            'cUsuCuentaBancaria',
            'cUsuNumRuta')
			->where('cUsuActivo',1)
			->get()
			->toArray();

		$perfiles= Perfiles::get()->toArray();

		return ['usuarios'=>$usuarios,'activo'=>$configuracion,'perfiles'=>$perfiles,'campoCovid'=>$cCovidActivo[0]['cConEncCovid19']];
	}

	public function addNewEmploye(Request $request){
		//print_r($request->all());
		$USUARIOEXISTE =NULL;
		$UsuarioId=User::where('cUsuCodigo',$request['cUsuCodigo'])->get()->toArray();

		if($UsuarioId== []){
			if ($request['tUsuReiniciar'] == 0) {

			User::create(['cUsuCodigo'=>$request['cUsuCodigo'],
			'cUsuNombre'=>$request['cUsuNombre'],
			'cUsuApellido'=>$request['cUsuApellido'],
			'cUsuActivo'=>$request['cUsuActivo'],
			'cUsuClave'=>$request['cUsuClave'],
			'cUsuPuesto1Activo'=>$request['cUsuPuesto1Activo'],
			'cUsuPuesto1Desc'=>$request['cUsuPuesto1Perfil']['cPerDesc'],
			'cUsuPuesto1Perfil'=>$request['cUsuPuesto1Perfil']['cPerId'],
			'cUsuPuesto2Activo'=>$request['cUsuPuesto2Activo'],
			'cUsuPuesto2Desc'=>$request['cUsuPuesto2Perfil']['cPerDesc'],
			'cUsuPuesto2Perfil'=>$request['cUsuPuesto2Perfil']['cPerId'],
			'cUsuPuesto3Activo'=>$request['cUsuPuesto3Activo'],
			'cUsuPuesto3Desc'=>$request['cUsuPuesto3Perfil']['cPerDesc'],
			'cUsuPuesto3Perfil'=>$request['cUsuPuesto3Perfil']['cPerId'],
			'cUsuNumSeguro'=>$request['cUsuNumSeguro'],
			'cUsuDireccion'=>$request['cUsuDireccion'],
			'cUsuTelefono'=>$request['cUsuTelefono'],
			'cUsuFechNac'=>$request['cUsuFechNac'],
			'cUsuAccesoAdmin'=>$request['cUsuAccesoAdmin'],
			'cUsuAdminPass'=>0,
			'cUsuPermisosPortal'=>$request['cUsuPermisosPortal'],
			'cUsuAutoCovid'=>$request['cUsuAutoCovid'],
            'cUsuCuentaBancaria'=>$request['cUsuCuentaBancaria'],
            'cUsuNumRuta'=>$request['cUsuNumRuta'],
			'tUsuReiniciar'=>$request['tUsuReiniciar']]);

		}else{
			$temporal= bcrypt('temporal');
			$tocken="IV5cUfVbvBiWLXgNVSozih0zS3nHTQvM9a25vkGIK1HoK8MTHGtSikkIexlr";

			User::create(['cUsuCodigo'=>$request['cUsuCodigo'],
			'cUsuNombre'=>$request['cUsuNombre'],
			'cUsuApellido'=>$request['cUsuApellido'],
			'cUsuActivo'=>$request['cUsuActivo'],
			'cUsuClave'=>$request['cUsuClave'],
			'cUsuPuesto1Activo'=>$request['cUsuPuesto1Activo'],
			'cUsuPuesto1Desc'=>$request['cUsuPuesto1Perfil']['cPerDesc'],
			'cUsuPuesto1Perfil'=>$request['cUsuPuesto1Perfil']['cPerId'],
			'cUsuPuesto2Activo'=>$request['cUsuPuesto2Activo'],
			'cUsuPuesto2Desc'=>$request['cUsuPuesto2Perfil']['cPerDesc'],
			'cUsuPuesto2Perfil'=>$request['cUsuPuesto2Perfil']['cPerId'],
			'cUsuPuesto3Activo'=>$request['cUsuPuesto3Activo'],
			'cUsuPuesto3Desc'=>$request['cUsuPuesto3Perfil']['cPerDesc'],
			'cUsuPuesto3Perfil'=>$request['cUsuPuesto3Perfil']['cPerId'],
			'cUsuNumSeguro'=>$request['cUsuNumSeguro'],
			'cUsuDireccion'=>$request['cUsuDireccion'],
			'cUsuTelefono'=>$request['cUsuTelefono'],
			'cUsuFechNac'=>$request['cUsuFechNac'],
			'cUsuAccesoAdmin'=>$request['cUsuAccesoAdmin'],
			'cUsuAdminPass'=>$temporal,
			'remember_token'=>$tocken,
			'cUsuPermisosPortal'=>$request['cUsuPermisosPortal'],
            'cUsuCuentaBancaria'=>$request['cUsuCuentaBancaria'],
            'cUsuNumRuta'=>$request['cUsuNumRuta'],
			'cUsuAutoCovid'=>$request['cUsuAutoCovid'],
			'tUsuReiniciar'=>$request['tUsuReiniciar']]);

		}
		}else{
			$USUARIOEXISTE	= 1;
		}

		return ['existe'=>$USUARIOEXISTE];
	}

	public function updateEmployee(Request $request){
		$temporal= bcrypt('temporal');
		$tocken="IV5cUfVbvBiWLXgNVSozih0zS3nHTQvM9a25vkGIK1HoK8MTHGtSikkIexlr";
		if ($request['tUsuReiniciar'] == 1) {
			$request['cUsuAdminPass']=$temporal;
			$request['remember_token']=$tocken;
		}
		$Usuario = User::where('cUsuFolio',$request['cUsuFolio'])->first();

		$Usuario->cUsuCodigo=$request['cUsuCodigo'];
		$Usuario->cUsuNombre=$request['cUsuNombre'];
		$Usuario->cUsuApellido=$request['cUsuApellido'];
		$Usuario->cUsuActivo=$request['cUsuActivo'];
		$Usuario->cUsuClave=$request['cUsuClave'];
		$Usuario->cUsuPuesto1Activo=$request['cUsuPuesto1Activo'];
		$Usuario->cUsuPuesto1Desc=$request['cUsuPuesto1Desc'];
		$Usuario->cUsuPuesto1Perfil=$request['cUsuPuesto1Perfil'];
		$Usuario->cUsuPuesto2Activo=$request['cUsuPuesto2Activo'];
		$Usuario->cUsuPuesto2Desc=$request['cUsuPuesto2Desc'];
		$Usuario->cUsuPuesto2Perfil=$request['cUsuPuesto2Perfil'];
		$Usuario->cUsuPuesto3Activo=$request['cUsuPuesto3Activo'];
		$Usuario->cUsuPuesto3Desc=$request['cUsuPuesto3Desc'];
		$Usuario->cUsuPuesto3Perfil=$request['cUsuPuesto3Perfil'];
		$Usuario->cUsuNumSeguro=$request['cUsuNumSeguro'];
		$Usuario->cUsuDireccion=$request['cUsuDireccion'];
		$Usuario->cUsuTelefono=$request['cUsuTelefono'];
		$Usuario->cUsuFechNac=$request['cUsuFechNac'];
		$Usuario->cUsuAccesoAdmin=$request['cUsuAccesoAdmin'];
		$Usuario->cUsuPermisosPortal=$request['cUsuPermisosPortal'];
		$Usuario->cUsuAutoCovid=$request['cUsuAutoCovid'];
        $Usuario->cUsuCuentaBancaria=$request['cUsuCuentaBancaria'];
        $Usuario->cUsuSincronizar=1;
        $Usuario->cUsuNumRuta=$request['cUsuNumRuta'];

		if ($request['tUsuReiniciar'] == 1) {
			$Usuario->cUsuAdminPass=$temporal;
			$Usuario->remember_token=$tocken;
		}

		$Usuario->tUsuReiniciar=$request['tUsuReiniciar'];

		$Usuario->save();
	}

	public function getProductos(){
		$productos=Productos::get()->toArray();
		$categorias=Categorias::get()->toArray();
		$subcategorias=SubCategorias::get()->toArray();
		$configuracion=Configuracion::get()->toArray();
		$clasificacion=Clasificacion::get()->toArray();
		return ['productos'=>$productos,'categorias'=>$categorias,'subcategorias'=>$subcategorias,'configuracion'=>$configuracion,'clasificacion'=>$clasificacion];
	}

	public function eliminarEmpleado(Request $request){
		print_r($request->all());
		$NumEmpleado=$request['usuario']['cUsuFolio'];
		User::where('cUsuFolio',$NumEmpleado)->delete();
	}
}
