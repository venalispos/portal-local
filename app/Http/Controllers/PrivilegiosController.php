<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PrivilegiosController extends Controller
{


    public function index(){
    	return view("privilegios.index");
    }
    public function buscarusuarios(Request $request){
    	$usuarios=User::selectRaw("cUsuFolio,cUsuCodigo,cUsuNombre,cUsuApellido")
                        ->whereRaw("CONCAT(cUsuNombre,' ',cUsuApellido)LIKE '%".$request['indicio']."%'")
                        ->get()
                        ->toArray();
    	if (!$usuarios) {
    		$usuarios=null;
    	}
    	return ['resultado'=>$usuarios];
    }
}
