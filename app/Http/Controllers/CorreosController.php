<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Correos;

class CorreosController extends Controller
{

  public function FunMostrarIndex(){
    return view('catalogos.correos.index');
  }
  public function FunObtenerCorreos(){
    $correos= Correos::get()->toArray();
    return ['correos'=>$correos];
  }

  public function FunAgregarCorreo(Request $request){
    Correos::create(['cConCorCorreo'=>$request['data']['correo'],'cConCorActivo'=>$request['data']['estatus']]);

  }

  public function FunActualizarCorreos(Request $request){
    print_r($request->all());
Correos::where('cConCorFolio',$request['data']['cConCorFolio'])->update(['cConCorCorreo'=>$request['data']['correo']]);
  }
  public function FunBorrarCorreos(Request $request){
Correos::where('cConCorFolio',$request['data']['cConCorFolio'])->delete();
  }
}
