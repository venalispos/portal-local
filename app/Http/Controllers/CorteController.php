<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use App\Configuracion;
use Carbon\Carbon;

class CorteController extends Controller
{
    public function Index(){
      return view('cortes.index');
    }

    public function FunObtenerCorte(Request $request){


      $fecha=str_replace("-", "",$request['fecha']);
      $TotalVenta = DB::raw("SELECT SUM(cHisMesTotal) as 'total' FROM tHisMesas WHERE cHisMesFechaRep=$fecha;");

      $totalFormasPago =DB::raw("SELECT SUM(cHisForCant) AS 'cHisForCant' FROM tHisFormaPago INNER JOIN tCategoriaFormasPago on cCatForId=cHisForId inner join tAgrupadorFormasPago on cCatForAgrupadorCorte=cAgForId WHERE cHisForFechaRep=$fecha  AND cHisForId NOT IN(1) and cAgForActivo=1  ;");


      $formaspago = DB::raw("SELECT cAgForOrden,cHisParidad,cHisFolioAgrupador,cHisForId,cHisForDes,ROUND(Convert(Decimal(15,2),(cHisForCant),2),2) as 'cHisForCant',Convert(Decimal(15,2),(cHisParidad*cHisCorteIngresado),2	)
       as 'cHisCorteIngresado',cHisCorteIngresado as 'cHisCorteNoParidad' FROM (SELECT cAgForOrden,ISNULL((SELECT TOP(1) cCarForParidad FROM tCategoriaFormasPago WHERE cCatForAgrupadorCorte=cAgForId),0) AS 'cHisParidad',ISNULL((SELECT TOP(1) cCatForFolioAgrupador FROM tCategoriaFormasPago WHERE cCatForFolioAgrupador=cAgCategoria),0) AS 'cHisFolioAgrupador',cAgForId AS 'cHisForId',cAgForDescripcion AS 'cHisForDes',ISNULL((SELECT ROUND(SUM(cHisForCant),2) AS 'cHisForCant'
  FROM tHisFormaPago INNER JOIN tCategoriaFormasPago ON cHisForId=cCatForId WHERE cHisForFechaRep=$fecha AND cAgForId=cCatForAgrupadorCorte  AND cHisForId NOT IN(1)),0) as 'cHisForCant'
  ,ISNULL((SELECT SUM(cCorDetCantidad) as 'cCorDetCantidad'    FROM tCortesDetalle WHERE cCorDetFechaRep=$fecha AND cCorDetCatFormaPago=cAgForId  ),0) as 'cHisCorteIngresado'
    FROM tAgrupadorFormasPago  WHERE cAgForId NOT IN(3) AND cAgForActivo =1  ) AS TB order by cAgForOrden");


      $corteEfectivoManual=DB::raw("SELECT ISNULL(SUM(cCorDetCantidad),0) as 'cCorDetCantidad'  FROM tCortesDetalle WHERE cCorDetFechaRep=$fecha AND cCorDetCatFormaPago=3  ");
      $TotalVenta=db::select($TotalVenta);
      $TotalVenta=json_decode(json_encode($TotalVenta),true);
      $formaspago=db::select($formaspago);
      $formaspago=json_decode(json_encode($formaspago),true);
      $totalFormasPago=db::select($totalFormasPago);
      $totalFormasPago=json_decode(json_encode($totalFormasPago),true);

      $gastos =DB::raw("SELECT * FROM tCortesDetalle WHERE cCorDetCatFormaPago =15 AND cCorDetFechaRep = $fecha");
      $gastos=db::select($gastos);
      $gastos=json_decode(json_encode($gastos),true);

      $gastosTotales =DB::raw("SELECT ROUND(SUM(cCorDetCantidad),2) as 'cCorDetCantidad' FROM tCortesDetalle WHERE cCorDetCatFormaPago =15 AND cCorDetFechaRep = $fecha");
      $gastosTotales=db::select($gastosTotales);
      $gastosTotales=json_decode(json_encode($gastosTotales),true);

      $devoluciones =DB::raw("SELECT * FROM tCortesDetalle WHERE cCorDetCatFormaPago =16 AND cCorDetFechaRep = $fecha");
      $devoluciones=db::select($devoluciones);
      $devoluciones=json_decode(json_encode($devoluciones),true);

      $devolucionesTotales =DB::raw("SELECT SUM(cCorDetCantidad) as 'cCorDetCantidad' FROM tCortesDetalle WHERE cCorDetCatFormaPago =16 AND cCorDetFechaRep = $fecha");
      $devolucionesTotales=db::select($devolucionesTotales);
      $devolucionesTotales=json_decode(json_encode($devolucionesTotales),true);

      $corteEfectivoManual=db::select($corteEfectivoManual);
      $corteEfectivoManual=json_decode(json_encode($corteEfectivoManual),true);
      $formaspagoEfectivo= (  floatval($totalFormasPago[0]["cHisForCant"]) -floatval($TotalVenta[0]["total"]))*-1;
      $formaspagoEfectivo = ROUND($formaspagoEfectivo,2);
      $formaspago[] = array("cHisFolioAgrupador"=>1,"cHisForId"=>3 ,"cHisForCant"=>$formaspagoEfectivo,'cHisForDes'=>"Efectivo",'cHisCorteIngresado'=>$corteEfectivoManual[0]['cCorDetCantidad'],'cHisParidad'=>1,'cAgForOrden'=>0);
      $totalFormasPagoCantidad =0;
      $totalFormasPagoCantidadIngresada =0;
    foreach ($formaspago as $key ) {
      $totalFormasPagoCantidad +=floatval($key['cHisForCant']);
      $totalFormasPagoCantidadIngresada +=floatval($key['cHisCorteIngresado']);
    }

    $sCortefinalizado= DB::raw("IF EXISTS (SELECT TOP(1) 1 AS 'cCorDetFinalizado'  FROM tCortesDetalle WHERE cCorDetFinalizado = 1 AND cCorDetFechaRep = $fecha)select 1 AS 'cCorDetFinalizado' else select 0 AS 'cCorDetFinalizado' ");
    $sCortefinalizado=db::select($sCortefinalizado);
    $sCortefinalizado=json_decode(json_encode($sCortefinalizado),true);

    $sCategoriasAgrupadores= DB::raw("SELECT * FROM tCategoriasAgrupadorFormasPago");
    $sCategoriasAgrupadores=db::select($sCategoriasAgrupadores);
    $sCategoriasAgrupadores=json_decode(json_encode($sCategoriasAgrupadores),true);

    $sAgrupadores= DB::raw("SELECT * FROM tAgrupadorFormasPago");
    $sAgrupadores=db::select($sAgrupadores);
    $sAgrupadores=json_decode(json_encode($sAgrupadores),true);
    array_multisort($formaspago, SORT_ASC, $formaspago);

      $validarInfo = $this->revisarFecha($fecha);
      $permitirCorteFecha = $validarInfo['fecha'];
      $permitirCorte = $validarInfo['mesas'];


      return [
        'permitircorte'=>$permitirCorte,
        'permitircorteFecha'=>$permitirCorteFecha,
        'agrupadores'=>$sAgrupadores,
        'categoriasagrupadores'=>$sCategoriasAgrupadores,
        'formaspago'=>$formaspago,
        'gastos'=>$gastos,
        'totalGasto'=>$gastosTotales,
        'devoluciones'=>$devoluciones,
        'totalDevoluciones'=>$devolucionesTotales,
        'totalesFormasPago'=>$totalFormasPagoCantidad,
        'totalIngresado'=>$totalFormasPagoCantidadIngresada,
        'corteFinalizado'=>$sCortefinalizado,
        'totalVenta'=>$TotalVenta[0]["total"]];
    }

/*     public function revisarMesas($fecha='')
    {
      $permitirCorte= DB::raw("SELECT * FROM tHoyMesas WHERE CONVERT(varchar, cHoyMesFechaEntrada, 112)  =$fecha ;");
      $permitirCorte=db::select($permitirCorte);
      $permitirCorte=json_decode(json_encode($permitirCorte),true);
      if ($permitirCorte ==[]) {
        return 0;
      }else{
        return 1;
      }
    } */
    public function revisarMesas($fecha) {
        //$interval = $fechaRep->diffInDays($fecha);
      $permitirCorte= DB::raw("SELECT * FROM tHoyMesas");
      $permitirCorte=db::select($permitirCorte);
      //$permitirCorte=json_decode(json_encode($permitirCorte),true);
	    if(count($permitirCorte) == 0) {
        return true;
      }
      $fechaRep = DB::raw("SELECT cConFechaVal FROM tConfiguracion;");
      $fechaRep=db::select($fechaRep);
      $fechaRep=json_decode(json_encode($fechaRep),true);
      $fechaRep=$fechaRep[0]['cConFechaVal'];
      if($fecha < $fechaRep) {
        return true;
      }
      return false;
    }

    public function revisarFecha($fechaBusqueda='') {
      //obtener fecha rep de config y validar que sea la siguiente fecha
      $fechaBusqueda=str_replace("-", "",$fechaBusqueda);
      $fechaRep = DB::raw("SELECT cConFechaVal FROM tConfiguracion;");
      $fechaRep=db::select($fechaRep);
      $fechaRep=json_decode(json_encode($fechaRep),true);
      $fechaRep=$fechaRep[0]['cConFechaVal'];
      $validarFecha = 0;
      $mesasAbiertas = $this->revisarMesas($fechaBusqueda);

      if($fechaBusqueda <= $fechaRep) {
        $validarFecha = 1;
      }
      return ['mesas' => $mesasAbiertas, 'fecha' => $validarFecha];
    }

    public function FunObtenerItemFormaPago(Request $request){
      $fecha=str_replace("-", "",$request['fecha']);
      $itemFormaPago = DB::raw("SELECT *,ISNULL(CAST((SELECT TOP(1) cCorDetParidad FROM tCortesDetalle
                          WHERE cCorDetFechaRep =$fecha  AND cCorDetItemFormaPago=cCorForId ) AS float),(SELECT TOP(1)cCarForParidad  FROM tCategoriaFormasPago WHERE cFormaPagoId=cCatForAgrupadorCorte )) AS 'cCorDetParidad',ISNULL(CAST((SELECT SUM(cCorDetCantidad) FROM tCortesDetalle WHERE cCorDetFechaRep =$fecha  AND cCorDetItemFormaPago=cCorForId )/cCorForValorItem AS int),0) AS 'cCorForValor' FROM tCortesItemFormasPago WHERE cCorForActivo  = 1 AND cCorForVisible = 1  order by cCorForOrden");
      $itemFormaPago=db::select($itemFormaPago);
      $itemFormaPago=json_decode(json_encode($itemFormaPago),true);
      return $itemFormaPago;
    }

    public function FunGuardarItemCantidad(Request $request){
      $fecha=str_replace("-", "",$request['fecha']);
      print_r($request->formaPagoSelected);
      $sInsertarItem="INSERT INTO [dbo].[tCortesDetalle]([cCorDetFechaRep],[cCorDetCatFormaPago],[cCorDetCantidad],[cCorDetTipo],[cCorDetTurno],[cCorDetCaja],[cCorDetFechaHora],[cCorDetRealiza],[cCorDetItemFormaPago],[cCorDetFinalizado],[cCorDetParidad])VALUES";
        foreach ($request['item'] as $key ) {
          if ($key['cFormaPagoId'] ==$request['formaPagoSelected']['cHisForId'] ) {
            $totalCantidad = floatval($key['cCorForValorItem'])*floatval($key['cCorForValor']);
            $sInsertarItem .="($fecha,".$request['formaPagoSelected']['cHisForId'].",".$totalCantidad.",1,0,0,'  ". Carbon::now()->format('Y-m-d h:s:m')."',".auth()->user()->cUsuCodigo.",".$key['cCorForId'].",0,".$request['formaPagoSelected']['cHisParidad']."),";
          }
        }

        $sEliminarFormasPagoItem="DELETE FROM tCortesDetalle WHERE cCorDetCatFormaPago = ".$request['formaPagoSelected']['cHisForId']." AND cCorDetFechaRep = $fecha";
         DB::statement($sEliminarFormasPagoItem);
         $sInsertarItem=substr($sInsertarItem, 0, -1);
         DB::statement($sEliminarFormasPagoItem);
         DB::statement($sInsertarItem);
    }

    public function FunGuardarGastoModificado(Request $request){
        $gasto=$request->gasto;
        $sInsertarItem="UPDATE tCortesDetalle SET cCorDetComentario ='".$gasto['cCorDetComentario']."',cCorDetCantidad = ".$gasto['cCorDetCantidad']." WHERE cCorDetFolio =".$gasto['cCorDetFolio'];
           DB::statement($sInsertarItem);
    }

    public function FunGuardarDevolucionModificado(Request $request){
        $gasto=$request->gasto;
        $sInsertarItem="UPDATE tCortesDetalle SET cCorDetComentario ='".$gasto['cCorDetComentario']."',cCorDetCantidad = ".$gasto['cCorDetCantidad']." WHERE cCorDetFolio =".$gasto['cCorDetFolio'];
           DB::statement($sInsertarItem);
    }

    public function FunGuardarGasto(Request $request){
      $fecha=str_replace("-", "",$request['fecha']);
      $sInsertarItem="INSERT INTO [dbo].[tCortesDetalle]([cCorDetFechaRep],[cCorDetCatFormaPago],[cCorDetComentario],[cCorDetCantidad],[cCorDetTipo],[cCorDetTurno],[cCorDetCaja],[cCorDetFechaHora],[cCorDetRealiza],[cCorDetItemFormaPago],[cCorDetFinalizado])VALUES";
            $sInsertarItem .="($fecha,15,'".$request['agregarGasto']['descripcion']."',".$request['agregarGasto']['gasto'].",1,0,0,'". Carbon::now()->format('Y-m-d h:s:m')."',".auth()->user()->cUsuCodigo.",34,0)";
             DB::statement($sInsertarItem);
    }
    public function FunGuardarDevolucion(Request $request){
      $fecha=str_replace("-", "",$request['fecha']);
      $sInsertarItem="INSERT INTO [dbo].[tCortesDetalle]([cCorDetFechaRep],[cCorDetCatFormaPago],[cCorDetComentario],[cCorDetCantidad],[cCorDetTipo],[cCorDetTurno],[cCorDetCaja],[cCorDetFechaHora],[cCorDetRealiza],[cCorDetItemFormaPago],[cCorDetFinalizado])VALUES";
            $sInsertarItem .="($fecha,16,'".$request['agregarGasto']['descripcion']."',".$request['agregarGasto']['gasto'].",1,0,0,'". Carbon::now()->format('Y-m-d h:s:m')."',".auth()->user()->cUsuCodigo.",35,0)";
             DB::statement($sInsertarItem);
    }

    public function FunEliminarGasto(Request $request){
      $gasto=$request->gasto;
      $sInsertarItem="DELETE FROM tCortesDetalle WHERE cCorDetFolio =".$gasto['cCorDetFolio'];
         DB::statement($sInsertarItem);
    }

    public function FunVerEstatus(Request $request){
      $fecha=str_replace("-", "",$request['fecha']);
      $sCortefinalizado= DB::raw("IF EXISTS (SELECT TOP(1) 1 AS 'cCorDetFinalizado'  FROM tCortesDetalle WHERE cCorDetFinalizado = 1 AND cCorDetFechaRep = $fecha)select 1 AS 'cCorDetFinalizado' else select 0 AS 'cCorDetFinalizado' ");
      $sCortefinalizado=db::select($sCortefinalizado);
      $sCorteGuardado= DB::raw("IF EXISTS (SELECT TOP(1) 1 AS 'cCorDetFinalizado'  FROM tCortesDetalle WHERE cCorDetFechaRep = $fecha AND cCorDetFinalizado IN (1,2))select 1 AS 'cCorDetFinalizado' else select 0 AS 'cCorDetFinalizado' ");
      $sCorteGuardado=db::select($sCorteGuardado);
      return ['corteGuardado' => $sCorteGuardado[0], 'cosrteFinalizado' => $sCortefinalizado[0]];
    }
    public function FunguardarCorte(Request $request){
      $num_papeleta=$request['num_papeleta'];
      $num_bolsa=$request['num_bolsa'];
      $fecha=str_replace("-", "",$request['fecha']);
      $permitirCorte=$this->revisarMesas($fecha);
      if ($permitirCorte) {
        $sFinalizarCorte="UPDATE tCortesDetalle SET cCorDetNumBolsa='$num_bolsa', cCorDetFinalizado=2, cCorDetNumPapeleta='$num_papeleta' WHERE cCorDetFechaRep = $fecha";
           DB::statement($sFinalizarCorte);
        return 1;
      } else {
        return 0;
      }
    }

    public function FunFinalizarCorte(Request $request){
      $num_papeleta=$request['num_papeleta'];
      $num_bolsa=$request['num_bolsa'];
      $fecha=str_replace("-", "",$request['fecha']);
      $permitirCorte=$this->revisarMesas($fecha);
      if ($permitirCorte) {
        $sFinalizarCorte="UPDATE tCortesDetalle SET cCorDetFinalizado=1, cCorDetNumBolsa='$num_bolsa', cCorDetNumPapeleta='$num_papeleta' , cCorDetAutoriza = '".auth()->user()->cUsuNombre.' '.auth()->user()->cUsuApellido."' WHERE cCorDetFechaRep = $fecha";
        DB::statement($sFinalizarCorte);
        return 1;
      } else {
        return 0;
      }
    }

    public function FunObtenerSucursal(){
      $sSucursal="SELECT * FROM tConfiguracion";
      $sSucursal=db::select($sSucursal);
      $sSucursal=json_decode(json_encode($sSucursal),true);
      return $sSucursal;
    }

    public function FunRealizarDescargaPDFVenta(Request $request){
      $fecha=str_replace("-", "",$request['fecha']);
      $fecha = date("d-m-Y", strtotime($fecha));
      $info_encabezado=['titulo'=>"REPORTE ARQUEO VENTA",'fecha'=>$fecha,
      'empresaSeleccionada'=>$this->FunObtenerSucursal()];
      $dataGPDF=['corte'=>$this->FunObtenerCorte($request),'item'=>$this->FunObtenerItemFormaPago($request)];
      $header=view()->make('/cortes/PDF/header')->with('datosencabezado',$info_encabezado)->render();
      $pdf=PDF::loadview('/cortes/PDF/body',compact('dataGPDF'));
      $pdf->setOption('header-html',$header)
      ->setOption('header-right','PAGINA [page] DE [toPage]')
      ->setOption('header-font-name','Courier')
      ->setOption('header-font-size','8')
      ->setOption('margin-top',38)
      ->setOption('margin-bottom',10)
      ->setOption('margin-left',10)
      ->setOption('margin-right',10);
      return $pdf->download('reporte.pdf');
    }

    public function FunRealizarDescargaPDFCorteCaja(Request $request){
      $fecha=str_replace("-", "",$request['fecha']);
      $fecha = date("d-m-Y", strtotime($fecha));
      $info_encabezado=['titulo'=>"CORTE DE CAJA DIARIO",'fecha'=>$fecha,
      'empresaSeleccionada'=>$this->FunObtenerSucursal()];
      $dataGPDF=['corte'=>$this->FunObtenerCorte($request),'item'=>$this->FunObtenerItemFormaPago($request)];
      $header=view()->make('/cortes/PDF/header')->with('datosencabezado',$info_encabezado)->render();
      $pdf=PDF::loadview('/cortes/PDF/bodyCorteCajaDiario',compact('dataGPDF'));
      $pdf->setOption('header-html',$header)
      ->setOption('header-right','PAGINA [page] DE [toPage]')
      ->setOption('header-font-name','Courier')
      ->setOption('header-font-size','8')
      ->setOption('margin-top',38)
      ->setOption('margin-bottom',10)
      ->setOption('margin-left',10)
      ->setOption('margin-right',10);
      return $pdf->download('reporte.pdf');
    }

    public function FunRealizarDescargaBanamex(Request $request){
      $fecha=str_replace("-", "",$request['fecha']);
      $info_encabezado=['titulo'=>"Ficha deposito",'fecha'=>$fecha,
      'empresaSeleccionada'=>$this->FunObtenerSucursal()];
      $dataGPDF=['corte'=>$this->FunObtenerCorte($request),'item'=>$this->FunObtenerItemFormaPago($request)];
      $header=view()->make('/cortes/PDF/header')->with('datosencabezado',$info_encabezado)->render();
      $Empresa=Configuracion::select('cConEmpresaVenta')->first();
      if ($Empresa['cConEmpresaVenta'] == 4) { // IBR
        $pdf=PDF::loadview('/cortes/PDF/banamex',compact('dataGPDF'));
      }elseif ($Empresa['cConEmpresaVenta'] == 5) { // GPN
        $pdf=PDF::loadview('/cortes/PDF/banamexGPN',compact('dataGPDF'));
      }elseif ($Empresa['cConEmpresaVenta'] == 3) { // OPM
        $pdf=PDF::loadview('/cortes/PDF/banamexOPM',compact('dataGPDF'));
      }
      $pdf
      ->setOption('header-font-name','Courier')
      ->setOption('header-font-size','8')
      ->setOption('margin-top',38)
      ->setOption('margin-bottom',10)
      ->setOption('margin-left',10)
      ->setOption('margin-right',10);
      return $pdf->download('reporte.pdf');
    }
    public function FunRealizarDolarPDf(Request $request){
      $fecha=str_replace("-", "",$request['fecha']);
      $fecha = date("d-m-Y", strtotime($fecha));
      $info_encabezado=['titulo'=>"REPORTE DE DOLAR",'fecha'=>$fecha,
      'empresaSeleccionada'=>$this->FunObtenerSucursal()];
      $dataGPDF=['corte'=>$this->FunObtenerCorte($request),'item'=>$this->FunObtenerItemFormaPago($request)];
      $header=view()->make('/cortes/PDF/header')->with('datosencabezado',$info_encabezado)->render();
      $pdf=PDF::loadview('/cortes/PDF/bodyDolares',compact('dataGPDF'));
      $pdf->setOption('header-html',$header)
      ->setOption('header-right','PAGINA [page] DE [toPage]')
      ->setOption('header-font-name','Courier')
      ->setOption('header-font-size','8')
      ->setOption('margin-top',38)
      ->setOption('margin-bottom',10)
      ->setOption('margin-left',10)
      ->setOption('margin-right',10);
      return $pdf->download('reporte.pdf');

    }
}
