<?php

namespace App\Http\Controllers;

use App\categoriasFormaPago;
use App\Configuracion;
use App\historialMesas;
use App\Categorias;
use App\HistorialFormasPagos;
use App\historialProductos;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use PDF;
use DB;

class RastrilloController extends Controller
{
    public function index()
    {
        return view("reportes.rastrillo.index");
    }

    public function showHistorialMesas($fechaDesde, $fechaHasta, $formaPago)
    {

        $reporte = historialMesas::select('cHisMesFolio', 'cHisMesNombre', 'cHisMesTicket', 'cHisMesEmpleadoNombre', 'cHisMesFechaRep',
                                        'cHisMesSubTotal', 'cHisMesTotalImpuesto1', 'cHisMesTotal', 'cHisMesFacturado', 'cHisMesVisible');
        if ($formaPago != 0) {
            $reporte = $reporte->whereHas('formaspago', function ($query) use ($formaPago) {
                $query->where('cHisForId', $formaPago);
            });
        }
        $reporte = $reporte->whereBetween('cHisMesFechaRep', [$fechaDesde, $fechaHasta])
            ->with(['formaspago:cHisForFolio,cHisForId,cHisForDes,cHisForCant,cHisForTicket,cHisForFechaRep'])
            ->orderBy('cHisMesTicket')
            ->get();

        return $reporte;
    }

    public function generarResumen($fInicio, $fFin){
  		$fInicio=str_replace("-","",$fInicio);
  		$fTermino=str_replace("-","",$fFin);

  		$tIva=0;
  		$tSubtotal=0;
  		$tCortesias=0;
  		$tDescuentos=0;
  		$tCancelaciones=0;
  		$tMonto=0;
  		$tTotales=0;

  		$Categorias = Categorias::select('cCatCodigo','cCatDescripcion')->get();
  		foreach ($Categorias as $key => $value) {
  			$value->Historial = historialProductos::select(DB::raw('SUM(cHisProSubTotal) as Subtotal, SUM(cHisProCortesia) as Cortesia, SUM(cHisProDescuentos) as Descuento, SUM(cHisProCancel) as Cancelacion, SUM(cHisProImp1) as Impuestos'))
  			->whereBetween('cHisProFechaRep',[$fInicio,$fTermino])
  			->where('cHisProCategoria',$value->cCatCodigo)
  			->whereHas('Mesa', function ($q){
  					$q->where('cHisMesVisible', 1);
  			})->first();

  			$value->Historial->Total = $value->Historial->Subtotal + $value->Historial->Impuestos;

  			$tIva = $tIva + $value->Historial->Impuestos;
  			$tSubtotal = $tSubtotal + $value->Historial->Subtotal;
  			$tCortesias = $tCortesias + $value->Historial->Cortesia;
  			$tDescuentos = $tDescuentos + $value->Historial->Descuento;
  			$tCancelaciones = $tCancelaciones + $value->Historial->Cancelacion;
  			$tTotales = $tTotales + $value->Historial->Total;
  		}

  		$CatFormasPago = categoriasFormaPago::select('cCatForId','cCatForDesc')
  		->where('cCatForActivo',1)
  		->where('cCarForReportes',1)
  		->get();

  		foreach ($CatFormasPago as $key => $value) {
  			$value->Historial = HistorialFormasPagos::selectRaw('SUM(cHisForCant)as monto')
  			->whereBetween('cHisForFechaRep',[$fInicio,$fTermino])
  			->where('cHisForId',$value->cCatForId)
  			->whereHas('Mesa', function ($q){
  					$q->where('cHisMesVisible', 1);
  			})
  			->first();
  			$tMonto = $tMonto + $value->Historial->monto;
  		}

  		return ['HistorialCategorias' =>$Categorias,
  		'HistorialFormasPagos'=>$CatFormasPago,
  		'Iva'=>$tIva,
  		'Subtotal'=>$tSubtotal,
  		'Cortesias'=>$tCortesias,
  		'Descuentos'=>$tDescuentos,
  		'Cancelaciones'=>$tCancelaciones,
  		'Monto'=>$tMonto,
  		'totales'=>$tTotales];
  	}
    public function generateReport(Request $request)
    {
        $campos = $request->all();

        $fechaDesde = str_replace("-", "", $campos['fechaDesde']);
        $fechaHasta = str_replace("-", "", $campos['fechaHasta']);
        $formaPago = $campos['formaPago'];

        $reporte = $this->showHistorialMesas($fechaDesde, $fechaHasta, $formaPago);

        return $reporte;
    }

    public function showCatFormasPago(categoriasFormaPago $categoriasFormaPago)
    {
        $categoriasFormaPago = categoriasFormaPago::query()
            ->select('cCatForFolio', 'cCatForId', 'cCatForDesc')
            ->where('cCatForActivo', 1)
            ->get();
        return $categoriasFormaPago;
    }

    public function updateHistorialMesas(Request $request, historialMesas $historialMesas)
    {
        $historialMesas->fill($request->only([
            'cHisMesFacturado',
            'cHisMesVisible',
        ]));

        $historialMesas->save();
        return $historialMesas;
    }

    public function exportPDF(Request $request)
    {
        $campos = $request->all();

        $fechaDesde = str_replace("-", "", $campos['fechaDesde']);
        $fechaHasta = str_replace("-", "", $campos['fechaHasta']);
        $formaPago = $campos['formaPago'];

        $reporte = $this->showHistorialMesas($fechaDesde, $fechaHasta, $formaPago);

        $totales['Total'] = 0;
        $totales['Subtotal'] = 0;
        $totales['IVA'] = 0;

        foreach ($reporte as $ticket) {
            if ($ticket->cHisMesVisible == 1) {
                $totales['Total'] += $ticket->cHisMesTotal;
                $totales['Subtotal'] += $ticket->cHisMesSubTotal;
                $totales['IVA'] += $ticket->cHisMesTotalImpuesto1;
            }
        }

        $fechaDesde = Carbon::parse($fechaDesde)->format('Y-m-d');
        $fechaHasta = Carbon::parse($fechaHasta)->format('Y-m-d');

        $sucursal = Configuracion::select('cConSucursal', 'cConEmpresa')->first();

        $headerData = [
            'fecha' => $fechaDesde . " - " . $fechaHasta,
            'sucursal' => $sucursal->cConSucursal,
            'empresa' => $sucursal->cConEmpresa,
        ];

        $dataPDF = [
            'reporte' => $reporte,
            'subtotal' => $totales['Subtotal'],
            'iva' => $totales['IVA'],
            'total' => $totales['Total'],
        ];

        $archivo = 'Reporte (' . $fechaDesde . " - " . $fechaHasta . ')';
        $header = view()->make('reportes.rastrillo.pdf.header')->with('headerData', $headerData)->render();
        $pdf = PDF::loadview('reportes.rastrillo.pdf.body', compact('dataPDF'));
        $pdf->setOption('header-html', $header)
            ->setOption('header-right', 'PAGINA [page] DE [toPage]')
            ->setOption('header-font-name', 'Courier')
            ->setOption('header-font-size', '8')
            ->setOption('margin-top', 38)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-left', 10)
            ->setOption('margin-right', 10);
        return $pdf->download($archivo . 'pdf');
    }

    public function reporteResumenPDF(Request $request)
    {
    $dataGPDF = $this->generarResumen($request['fechaDesde'],$request['fechaHasta']);


		$datosempresa=DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
		$empresaSel=db::select($datosempresa);
		$empresaSel=json_decode(json_encode($empresaSel),true);

		$info_encabezado=['fecha'=>$request->fechaDesde." - ".$request->fechaHasta,
		'empresaSeleccionada'=>$empresaSel];

		$header=view()->make('/reportes/reporteResumenPDF/header')->with('datosencabezado',$info_encabezado)->render();
		$pdf=PDF::loadview('/reportes/reporteResumenPDF/bodyR',compact('dataGPDF'));
		$pdf->setOption('header-html',$header)
		->setOption('header-right','PAGINA [page] DE [toPage]')
		->setOption('header-font-name','Courier')
		->setOption('header-font-size','8')
		->setOption('margin-top',38)
		->setOption('margin-bottom',10)
		->setOption('margin-left',10)
		->setOption('margin-right',10);
		return $pdf->download('reporte.pdf');
    }

    public function reporteResumenExcel(Request $request)
    {
    $dataGPDF = $this->generarResumen($request['fechaDesde'],$request['fechaHasta']);


		$datosempresa=DB::raw("select cConEmpresa,cConSucursal from dbo.tConfiguracion where cConfolio =1");
		$empresaSel=db::select($datosempresa);
		$empresaSel=json_decode(json_encode($empresaSel),true);

		$info_encabezado=['fecha'=>$request->fechaDesde." - ".$request->fechaHasta,
		'empresaSeleccionada'=>$empresaSel];

    		Excel::create('ventas', function($excel)use (
    	                $info_encabezado,
    	                $dataGPDF
    	            ){

    	            $excel->sheet('ventas', function($sheet)use(
    	                $info_encabezado,
    	                $dataGPDF
    	            ){

    	                $sheet->loadView("reportes.reporteExcelResumen.reporteExcelResumenR")
    	                      ->with(compact(
    	                        'info_encabezado',
    	                        'dataGPDF'
    	                      ));


    	            });
    	        })->download('csv');
    }
}
