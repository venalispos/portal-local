<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
class LoginController extends Controller
{
   /* public function login(Request $request){
     $credenciales= $this->validate($request, [
         $this->username()=>'required|string',
         'cUsuAdminPass'=>'required|string'
     ]);
     // $password = bcrypt('123');
      //echo $password;
     // // //return $credenciales;


     if(Auth::attempt($credenciales)){
       if (auth()->user()->cUsuActivo == 1 ) {
         if (auth()->user()->tUsuReiniciar == 1 ) {
             return redirect()->route('reiniciar');
          }else{
          return redirect()->route('home');
          }
       }
     }

     return back()
     ->withErrors([$this->username()=> trans('auth.failed')])
     ->withInput(request([$this->username()]));
   } */

   public function login(Request $request){
    $credentials = $request->only('cUsuAdminPass', 'cUsuCodigo');

    // $password = bcrypt('123');
     //echo $password;
    // // //return $credenciales;


    if(Auth::attempt(['cUsuCodigo' => $credentials['cUsuCodigo'], 'password' => $credentials['cUsuAdminPass'], 'cUsuActivo' => 1])){
        if (auth()->user()->tUsuReiniciar == 1 ) {
            return redirect()->route('reiniciar');
         }else{
         return redirect()->route('home');
         }

    }

    return back()
    ->withErrors([$this->username()=> trans('auth.failed')])
    ->withInput(request([$this->username()]));
  }

   public function showLoginForm(){
    return view('auth.login');
   }

   public function logout(){

    Auth::logout();
    return redirect('/');
   }
   public function username(){
    return 'cUsuCodigo';
   }

   public function FunReiniciarPassword(){
     return view("reiniciarPassword.index");
   }

   public function FunModificarPassword(){
    $password =  bcrypt($_POST['pass1']);
     $usuarioFolio = auth()->user()->cUsuFolio;
     if ($_POST['pass1'] === $_POST['pass2']) {
       User::where('cUsuFolio',$usuarioFolio)->update(['cUsuAdminPass'=>$password,'tUsuReiniciar'=>0]);
        return redirect()->route('home');
     } else {
       return redirect('reiniciar');
     }


   }
}

/*
*/
