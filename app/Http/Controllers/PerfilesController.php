<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perfiles;

class PerfilesController extends Controller
{
	public function indexPerfiles(){

		return view("Catalogos.Perfiles.index");
	}

	public function solicitarPerfiles(){

		$perfiles =Perfiles::get()->toArray();

		return ['perfiles'=>$perfiles];
	}
}
