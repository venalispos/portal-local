<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HistorialTraspasos;
use App\centroCostos;
use Carbon\Carbon;

class TraspasosController extends Controller
{

  public function FunVistaRealizarTraspaso(){
    return view('Inventario.traspaso.indexrealizarTraspaso');
  }
    public function FunObtenerCentrosCosto(){
      $centros = centroCostos::get()->toArray();
      return $centros;
    }

    public function FunGuardarPeticionCentroCostos(Request $request){
      $sucursal=$request['data']['CentroCosto'];
      $producto=$request['data']['producto'];
      $cantidad=$request['data']['cantidad'];
      $now = Carbon::now()->format('Ymd');

      HistorialTraspasos::create(['cHistrasCodigoProducto'=>$producto['cProCodigo'],'cHistrasCentroCosto'=>$sucursal,'cHistrasTipo'=>0,'cHistrasFechaRep'=>$now,'cHistrasCantidad'=>$cantidad]);
    }

    public function FunBuscarPeticionesSinFinalizar(){
      $peticiones= HistorialTraspasos::where('cHistrasFinalizado',0)->where('cHistrasFinalizado',0)->get()->toArray();
      if ($peticiones == []) {
        $peticiones =null;
      }
      return $peticiones;
    }

    public function FuneliminarRegistro(Request $request){
      $registro = $request['registro'];
      HistorialTraspasos::where('cHistrasFolio',$registro['cHistrasFolio'])->delete();
    }

    public function FunFinalizarRegistros(Request $request){
      HistorialTraspasos::where('cHistrasFinalizado',0)->where('cHistrasTipo',0)->update(['cHistrasFinalizado'=>1]);

    }
}
