<?php

namespace App\Http\Controllers\Configuracion;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Configuracion\Configuracion;
use App\Http\Controllers\Controller;

class ImpuestosController extends Controller
{
  public function VistaImpuestos()
  	{
  		    return view('Catalogos/configuracionImpuestos/ConfiguraacionImpuestosPrincipal');
  	}

  public function obtenerImpuestos()
    {
      $Impuestos= Configuracion::select('cConFolio','cConImpuesto1Activo', 'cConImpuesto1Nombre', 'cConImpuesto1Cantidad', 'cConImpuesto1Tipo', 'cConImpuesto2Activo', 'cConImpuesto2Nombre', 'cConImpuesto2Cantidad', 'cConImpuesto2Tipo', 'cConImpuesto3Activo', 'cConImpuesto3Nombre', 'cConImpuesto3Cantidad', 'cConImpuesto3Tipo', 'cConImpuesto4Activo', 'cConImpuesto4Nombre', 'cConImpuesto4Cantidad', 'cConImpuesto4Tipo')->first();

      $Impuestos['cConImpuesto1Cantidad'] = $Impuestos['cConImpuesto1Cantidad'] *100;
      $Impuestos['cConImpuesto2Cantidad'] = $Impuestos['cConImpuesto2Cantidad'] *100;
      $Impuestos['cConImpuesto3Cantidad'] = $Impuestos['cConImpuesto3Cantidad'] *100;
      $Impuestos['cConImpuesto4Cantidad'] = $Impuestos['cConImpuesto4Cantidad'] *100;

      $Impuestos = json_decode($Impuestos,true);
      return ['Impuestos' => $Impuestos];
    }

  public function agregarImpuesto(Request $request)
    {
      $datosImpuestos = $request->all();
      Configuracion::create($datosImpuestos);
    }

  public function editarImpuesto(Request $request)
    {
      $datosImpuestos= $request->all();
      $Folio = $datosImpuestos['cConFolio'];
      $datosImpuestos['cConImpuesto1Cantidad'] = $datosImpuestos['cConImpuesto1Cantidad'] /100;
      $datosImpuestos['cConImpuesto2Cantidad'] = $datosImpuestos['cConImpuesto2Cantidad'] /100;
      $datosImpuestos['cConImpuesto3Cantidad'] = $datosImpuestos['cConImpuesto3Cantidad'] /100;
      $datosImpuestos['cConImpuesto4Cantidad'] = $datosImpuestos['cConImpuesto4Cantidad'] /100;
      $datosImpuestos = \array_splice($datosImpuestos,1,16);
      $editarImpuesto= Configuracion::where('cConFolio', $Folio)
          ->update($datosImpuestos);
    }
}
