<?php

namespace App\Http\Controllers\Configuracion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Configuracion;
Use App\Detallecorte;
Use Carbon\Carbon;
class ConfiguracionController extends Controller
{
    public function obtenerConfiguracion()
    {
        $conf = Configuracion::where('cConFolio', 1)->first();
        return $conf;
    }

    public function obtenerCorteFecha($fechainicio, $fechafinal){
        $configuracaion = Configuracion::first();

        $FechaInicio = new Carbon($fechainicio);
		    $FechaFin = new Carbon($fechafinal);

        $diff = $FechaInicio->diffInDays($FechaFin);
        $fechaDesde = date_format($FechaInicio, 'Ymd');
		    $fechaHasta = date_format($FechaFin, 'Ymd');
        if ($configuracaion->cConForzarCierre == 0) {
            return 1;
        }
        $cortes =  Detallecorte::select('cCorDetFechaRep')->where('cCorDetFinalizado',1)->whereBetween('cCorDetFechaRep', [$fechaDesde, $fechaHasta])->groupBY('cCorDetFechaRep')->get()->count();
        if ($diff+1 == $cortes) {
            return 1;
        }
        return 0;
    }
}
