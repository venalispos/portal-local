<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventarioDiario extends Model
{
    protected $table = "tInventarioDiario";
	protected $primaryKey = 'tInvIniFolio';
	public $timestamps = false;
	protected $fillable = [ 'tInvIniFechaRep', 'tInvIniFolioProd', 'tInvIniValorMinimo', 'tInvIniCostoPiezaProd', 'tInvIniFolioUsuario', 'tInvIniFechaRegistro','tInvIniStatus','tInvIniFinalizado'];
}
