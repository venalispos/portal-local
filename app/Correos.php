<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Correos extends Model
{
protected $table = "tConfiguracionCorreos";
protected $primaryKey = 'cConCorFolio';
public $timestamps = false;
protected $fillable = ['cConCorId','cConCorCorreo','cConCorActivo'];
}
