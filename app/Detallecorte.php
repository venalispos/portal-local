<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detallecorte extends Model
{
    //
    protected $table = 'tCortesDetalle'; //Nombre de la tabla
    protected $primaryKey = 'cCorDetFolio'; //Campo de la llave primaria
    public $timestamps  = false; //Desactiva registros de actualización/creación
    protected $fillable = ['cidtipopago', 'cdenominacion', 'ccantidad', 'ctotal', 'cidsubtipopago', 'cnoreferencia', 'cidcorte'];
    protected $casts = [
      'cidtipopago' => 'int',
      'cdenominacion' => 'float',
      'ccantidad' => 'int',
      'ctotal' => 'float',
      'cidsubtipopago' => 'int',
      'cidcorte' => 'int',
    ];

    public function Tipopago(){
      return $this-> hasOne('App\Tipopago', 'cidtipopago', 'cidtipopago');
    }
    public function Subtipopago(){
      return $this-> hasOne('App\Subtipopago', 'cidsubtipopago', 'cidsubtipopago');
    }
    public function Corte(){
      return $this-> hasOne('App\Corte', 'cidcorte', 'cidcorte');
    }
}
