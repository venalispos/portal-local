<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfiles extends Model
{
  protected $table = "tPerfiles";
	protected $primaryKey = 'cPerFolio';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cPerId', 'cPerDesc', 'cPerAccesoClave', 'cPerAccion', 'cPerCategorias', 'cPerSubCategorias', 'cPerProductos', 'cPerReImpTicket', 'cPerAccesPortal','cPerLiquida','cPerAutorizaSalida','cPerInventario'];
}
