<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgrupadorFormaPago extends Model
{
    protected $table = "tAgrupadorFormasPago";
    protected $primaryKey = 'cAgForFolio';
    public $timestamps = false;
    protected $fillable = [
        'cAgForId',
        'cAgForDescripcion',
        'cCatForActivo',
        'cAgForParidad',
        'cAgForOrden',
        'cAgForActivo',
        'cAgCategoria',
    ];

    protected $casts = [
        'cAgForId' => 'integer',
        'cCatForActivo' => 'integer',
        'cAgForParidad' => 'integer',
        'cAgForOrden' => 'integer',
        'cAgCategoria' => 'integer',
    ];


    public function categoriaagrupadorformapago(){
        return $this->hasMany(CategoriaAgrupadorFormaPago::class, 'cCatAgForId', 'cAgCategoria');
    }

}
