<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Usuarios as Authenticatable;

class Usuarios extends Authenticatable
{
      use Notifiable;

    protected $table = 'tUsuarios';

    protected $fillable = [
         'nombres', 'correo', 'contrasena',
    ];

    protected $hidden = [
        'contrasena',
    ];

    public function getAuthPassword()
    {
        return $this->contrasena;
    }
}
