<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class centroCostos extends Model
{
  protected $table = "tCentrosCosto";
protected $primaryKey = 'cCeCoFolio';
public $timestamps = false;
}
