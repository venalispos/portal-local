<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParteDia extends Model
{
     protected $table = "tParteDia";
    protected $primaryKey = 'cFolio';
    protected $fillable = ['cDescParteDia', 'cParteDiaInicio', 'cParteDiaFin'];
}
