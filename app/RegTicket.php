<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegTicket extends Model
{
     protected $table = "tRegTicket ";
    protected $primaryKey = 'cRegTicketFolio ';
    protected $fillable = ['cRegTicketMesa','cRegTicketUsuario','cRegTicketFechaRep','cRegTicketTiempo'];
    public $timestamps = false;
}
