<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventarioDiario extends Model
{
    protected $table = "tInventarioDiario";
	protected $primaryKey = 'cInvFolio';
	public $timestamps = false;
	protected $fillable = [
    'cInvFechaRep',
    'cInvCodigoProducto',
    'cInvCantidad',
    'tInvIniCostoPiezaProd',
    'cInvCodigoUsuario',
    'cInvFechaRegistro',
    'cInvStatus',
    'cInvFinalizado',
    'cInvComentario',
    'cInvProInterCodigo',
    'cInvPresentiacionProducto'
  ];


  protected $casts = [
        'tInvIniCostoPiezaProd' => 'float',
    ];

}
