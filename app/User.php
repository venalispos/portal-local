<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = "tUsuarios";
    protected $primaryKey = 'cUsuFolio';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cUsuCodigo',
        'cUsuNombre',
        'cUsuApellido',
        'cUsuActivo',
        'cUsuClave',
        'cUsuPuesto1Activo',
        'cUsuPuesto1Desc',
        'cUsuPuesto1Perfil',
        'cUsuPuesto2Activo',
        'cUsuPuesto2Desc',
        'cUsuPuesto2Perfil',
        'cUsuPuesto3Activo',
        'cUsuPuesto3Desc',
        'cUsuPuesto3Perfil',
        'cUsuFPData',
        'cUsuNumSeguro',
        'cUsuDireccion',
        'cUsuTelefono',
        'cUsuFechNac',
        'cUsuAccesoAdmin',
        'cUsuAdminPass',
        'remember_token',
        'cUsuPermisosPortal',
        'tUsuReiniciar',
        'cUsuAutoCovid',
        'cUsuCuentaBancaria',
        'cUsuSincronizar',
        'cUsuNumRuta'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'cUsuAdminPass',
        'remember_token',

    ];

     public function getAuthPassword()
    {
        return $this->cUsuAdminPass;
    }
    public function Perfil ()
     {
     	return $this->belongsTo('App\Perfiles', 'cUsuPuesto1Perfil','cPerId');
     }



}
