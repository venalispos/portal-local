<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class catProductos extends Model
{
    protected $table = "tCategorias";
    protected $primaryKey = 'cCatFolio';
    protected $fillable = ['cHisMesTicket','cHisMesNombre','cHisMesSegmento','cHisMesEmpleadoFolio','cHisMesEmpleadoNombre','cHisMesCantClientes',
						   'cHisMesSerieCap','cHisMesSerieLiq','cHisMesFechaEntrada','cHisMesFechaSalida','cHisMesFechaRep','cHisMesCajeroFolio',
						   'cHisMesCajeroNombre','cHisMesSubTotal','cHisMesTotalImpuesto1','cHisMesTotalImpuesto2','cHisMesTotalImpuesto3',
						   'cHisMesTotalImpuesto4','cHisMesTotal','cHisMesFacturado','cHisMesFactSoli','cHisMesNumLealtad','cHisMesSincro','cHisMesSincroInterfaz'];
}
 
 /*
 protected $table = "tHisMesas";
    
    protected $fillable = ['cHisMesTicket','cHisMesNombre','cHisMesSegmento','cHisMesEmpleadoFolio','cHisMesEmpleadoNombre','cHisMesCantClientes',
						   'cHisMesSerieCap','cHisMesSerieLiq','cHisMesFechaEntrada','cHisMesFechaSalida','cHisMesFechaRep','cHisMesCajeroFolio',
						   'cHisMesCajeroNombre','cHisMesSubTotal','cHisMesTotalImpuesto1','cHisMesTotalImpuesto2','cHisMesTotalImpuesto3',
						   'cHisMesTotalImpuesto4','cHisMesTotal','cHisMesFacturado','cHisMesFactSoli','cHisMesNumLealtad','cHisMesSincro','cHisMesSincroInterfaz'];



						   SELECT         , cCatCodigo, cCatDescripcion
FROM            
 */