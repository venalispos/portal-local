<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class unidadmedida extends Model
{
  protected $table = "tUnidadesMedida";
protected $primaryKey = 'cUnMeFolio';
public $timestamps = false;
}
