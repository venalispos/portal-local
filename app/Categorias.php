<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    protected $table = "tCategorias";
    protected $primaryKey = 'cCatFolio';
    protected $fillable = ['cCatCodigo', 'cCatDescripcion'];
} 

	
 