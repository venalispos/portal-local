<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $table = 'tClientes'; //Nombre de la tabla
    protected $primaryKey = 'cidcliente'; //Campo de la llave primaria
    public $timestamps  = false; //Desactiva registros de actualización/creación
    protected $fillable = ['cnombrecliente', 'crfccliente', 'ccurpcliente', 'cdomiciliocomercial', 'crepesentantelegal',
    'cciudadcliente', 'cestadocliente', 'ccodigopostal', 'cpaiscliente', 'ccorreoelectronico', 'ctelefonocliente', 'cnotascliente',
    'cestatuscliente', 'cfechaalta', 'cfechainactivo', 'ccodigocliente', 'ccodigoadmintotal'];

    protected $casts = [
    'cestatuscliente' => 'int',
    'ccodigocliente' => 'int',
  ];

}
