<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bitacora extends Model
{
     protected $table = "tBitacora";
	protected $primaryKey = 'cBitFolio';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cBitFechaRep', 'cBitTotalVenta', 'cBitSubTotalVenta', 'cBitSincro', 'cBitEspeciales', 'cBitTicketPromedio', 'cBitCantClientes', 'cBitCancelaciones', 'cBitTopEmpleados', 'cBitTopProductos', 'cBitDetalleDeVenta','cBitParcial'];
}
