<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
     protected $table = "tMenu";
    protected $primaryKey = 'cMenFolio';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cMenFolPadre','cMenOrden','cMenDescripcion','cMenURL','cMenIcono','cMenHijos'];
}
