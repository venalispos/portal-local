<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipopago extends Model
{
    //
    protected $table = 'tTipopago'; //Nombre de la tabla
    protected $primaryKey = 'cidtipopago'; //Campo de la llave primaria
    public $timestamps  = false; //Desactiva registros de actualización/creación
    protected $fillable = ['cnombretipopago', 'cestatus'];
    protected $casts = [
      'cestatus' => 'int',
    ];

}
