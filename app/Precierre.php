<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Precierre extends Model
{
  //
  protected $table = 'tPrecierre'; //Nombre de la tabla
  protected $primaryKey = 'cidprecierre'; //Campo de la llave primaria
  public $timestamps  = false; //Desactiva registros de actualización/creación
  protected $fillable = ['cfechaprecierre', 'cidsucursal', 'ctotalprecierre', 'cestatusprecierre', 'cdescripciongasto', 'ctotalgasto', 'cusuarioprecierre'];
  protected $casts = [
    'cidsucursal' => 'int',
    'ctotalprecierre' => 'float',
    'cestatusprecierre' => 'int',
    'ctotalgasto' => 'float',
    'cusuarioprecierre' => 'int',
  ];
}
