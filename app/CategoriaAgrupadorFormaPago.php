<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaAgrupadorFormaPago extends Model
{
    protected $table = "tCategoriasAgrupadorFormasPago";
    protected $primaryKey = 'cCatAgForFolio';
    public $timestamps = false;
    protected $fillable = [
        'cCatAgForId',
        'cCatAgForDescripcion',
        'cCatAgForParidad',
       
    ];

    protected $casts = [
        'cCatAgForId' => 'integer', 
        'cCatAgForParidad' => 'integer',
    ];
}
